@servers(['web' => 'devly@103.233.58.130'])

@setup
    $repository = 'git@gitlab.com:devly/offgridbazaar-laravel.git';
    $releases_dir = '/home/devly/ogb/releases';
    $app_dir = '/home/devly/ogb';
    $release = date('YmdHis');
    $branch = dev;
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    {{--run migrations--}}
    update_symlinks
    update_permissions
    db_seed_migrate
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} --branch={{ $branch }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    php7.0 /usr/local/bin/composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking public/storage with storage/app/public'
    php7.0 {{$new_release_dir}}/artisan storage:link

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('update_permissions')
    chmod 777 {{ $new_release_dir }}/bootstrap/cache
    chmod 777 {{ $app_dir }}/storage
@endtask

@task('db_seed_migrate')
    php7.0 {{ $new_release_dir }}/artisan migrate
    php7.0 {{ $new_release_dir }}/artisan db:seed
@endtask
