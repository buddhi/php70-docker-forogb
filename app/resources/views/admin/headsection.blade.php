<header>
    @if (session()->has('role_message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">X</span>
            </button>
            <strong>{!! session()->get('role_message') !!} !!</strong>
        </div>
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">X</span>
            </button>
            <strong>{!! session()->get('success') !!} !!</strong>
        </div>
    @endif
    <div class="mainheader container">
        <div class="col-sm-3">
            @guest
                <a href="{{url('/')}}">
                    <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                </a>
                @else
                    <a href="{{route('admin.project.list')}}">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                    </a>
                    @endguest
        </div>
        <div class="col-sm-5">
            <div class="stat small">
                <h4><i class="icon-project"></i> {{$project_count}}</h4>
                <small class="light">Projects</small>
            </div>
            <div class="stat small">
                <h4><i class="icon-investment"></i> $ {{round($received_investment/100000,0)}}K</h4>
                <small class="light">Investment</small>
            </div>
        </div>
        <div class="col-sm-4 text-right pull-right">
            <nav class="topright">
                @guest
                    <a href="{{url('/login')}}">Login</a> |
                    <a href="{{url('/register')}}">Signup</a>
                    @else
                        {{Auth::user()->username }}
                        |<strong>{{returnRole(session('role'))}}</strong>
                        |<a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                        @endguest
            </nav>
        </div>
    </div>
    <div class="container">
        <nav class="nav topnav">
            <a class="nav-link {{request()->is('admin/project') || request()->is('admin/project/*') ?'active':'' }}" href="{{route('admin.project.list')}}">Projects</a>
            <a class="nav-link {{request()->is('admin/meter*')?'active':'' }}" href="{{route('admin.meter.list')}}">Meters</a>
            <a class="nav-link {{request()->is('admin/project-plan*')?'active':'' }}" href="{{route('admin.project-plan.list')}}">Plans</a>
            <a class="nav-link {{request()->is('admin/user*') ?'active':'' }}" href="{{route('admin.user.list')}}">Users</a>
            <a class="nav-link {{request()->is('admin/credit-weight*') ?'active':'' }}" href="{{route('admin.credit.weight')}}">CreditScore Weight</a>
        </nav>
    </div>
</header>