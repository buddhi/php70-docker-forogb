@extends('admin.default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
@section('content')
    <div class="container">
        @if (session()->has('success_message'))
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session()->get('success_message') }}
            </div>
        @endif
        <div class="row col-md-12">
            <h3><strong>Investor Details of Project</strong></h3>
            <h4>Farmer name : {{$project->farmer_name}}</h4>
            <table class="table table-striped" id = "admin-table">
                <thead>
                <tr>
                    <th>Investor</th>
                    <th>investment_type</th>
                    <th>expected_irr</th>
                    <th>Term</th>
                    <th>interest_rate</th>
                    <th>Investment amount</th>
                    <th>Investment percent</th>
                    <th>Date</th>
                </tr>
                </thead>
                @if ($projectInvestments->count() > 0)
                    @foreach($projectInvestments as $row)
                        <tr>
                            <td>{{ $row->user->full_name}} </td>
                            <td>{{ investmentType($row->investment_type_id)}} </td>
                            <td>{{ isset($row->expected_irr)?:"--"}} </td>
                            <td>{{ isset($row->term)?:"--"}} </td>
                            <td>{{ isset($row->interest_rate)?:"--"}} </td>
                            <td>{{ $row->invest_amount}} </td>
                            <td>{{ round($row->investment_percent,5)}} </td>
                            <td>{{ $row->created_on}} </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
            </table>
            <p><strong>Note</strong>:Recent investments are shown first</p>
        </div>
    </div>
@endsection