@extends('admin.default')
@section('content')

<div class="container">
    <h2>Credit Score weights</h2>
    <p>We have the details weights of credit score.</p>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Credit Score</a></li>
        @if(isset($risk_collection) && count($risk_collection) > 0)
            @foreach($risk_collection as $risk)
                <li><a data-toggle="tab" href="#{{$risk->name}}">{{ ucfirst($risk->name)}}</a></li>
            @endforeach
        @endif
    </ul>

    @if(isset($risk_collection) && count($risk_collection) > 0)
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3>Weights</h3>
                <table class="table table-striped" style="border:1px solid #dedcdc">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Risk Weight</th>
                        </tr>
                    </thead>
                    @foreach($risk_collection as $risk)
                        <tr>
                            <td>{{$risk->id }}</td>
                            <td>{{$risk->name }}</td>
                            <td>{{$risk->risk_weight_factor }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

            @foreach($risk_collection as $risk)
                <div id="{{$risk->name}}" class="tab-pane fade">
                        <h3>{{ucfirst($risk->name)}}</h3>
                        <table class="table table-striped" style="border:1px solid #dedcdc">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Risk Weight</th>
                            </tr>
                            </thead>
                            @if(isset($risk->attributes) && count($risk->attributes) > 0)
                                @foreach($risk->attributes as $attribute)
                                    <tr>
                                        <td>{{$attribute->id }}</td>
                                        <td><a href="{{route('admin.credit.weight.attribute', $attribute->id)}}" style="color:#4c4a48; font-size:12px; text-decoration:underline" >{{ ucfirst(str_replace("_"," ",$attribute->name))}}</a></td>
                                        <td>{{$attribute->attribute_weight_factor }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3"> No data found</td>
                                </tr>
                            @endif
                        </table>
                    </div>
            @endforeach
        </div>
    @endif
</div>
@endsection
