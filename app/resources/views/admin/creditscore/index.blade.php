@extends('admin.default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        tr th{
            vertical-align:top !important;
        }
    </style>
@endsection
@section('content')
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success_message') }}
        </div>
    @endif

    <div class="container">
        <h2>{{$project->farmer_name}}</h2>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#credit-score">Credit Score</a></li>
            <li><a data-toggle="tab" href="#demographic">Demographic</a></li>
            <li><a data-toggle="tab" href="#financial"> Financial</a></li>
            <li><a data-toggle="tab" href="#agricultural">Agricultural</a></li>
            <li><a data-toggle="tab" href="#psychometric">Psychometric</a></li>
        </ul>

        <div class="tab-content">
            <div id="credit-score" class="tab-pane fade in active">
                <h3>Credit Score</h3>
                <table class="table table-striped" style="border:1px solid #dedcdc" id = "admin-table">
                    <thead>
                    <tr>
                        <th>Demographic</th>
                        <th>Financial</th>
                        <th>Agricultural</th>
                        <th>Psychometric</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tr>
                        <td>{{isset($credit_score_array['demographic']['component_risk'])?round($credit_score_array['demographic']['component_risk']* 100,2):null}}</td>
                        <td>{{isset($credit_score_array['financial']['component_risk'])?round($credit_score_array['financial']['component_risk']* 100,2) :null }} </td>
                        <td>{{isset($credit_score_array['agricultural']['component_risk'])?round($credit_score_array['agricultural']['component_risk']* 100,2) :null }} </td>
                        <td>{{isset($credit_score_array['psychometric']['component_risk'])?round($credit_score_array['psychometric']['component_risk']* 100,2) :null }} </td>
                        <td>{{ isset($credit_score_array['credit_score'])?$credit_score_array['credit_score']*100:null}}</td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="demographic" class="tab-pane fade">
                <h3>Demographic</h3>
                <table class="table table-striped" style="border:1px solid #dedcdc" id = "admin-table">
                    <thead>

                            <th>Age</th>
                            <th>Gender</th>
                            <th>Education</th>
                            <th>Certification</th>
                            <th>No of people in house</th>
                            <th>No of dependents</th>
                            <th>Component Risk Weight</th>
                            <th>Total component Weight</th>
                            <th>Percentage</th>

                    </thead>
                    <tr>
                        @if(isset($credit_score_array['demographic']['attribute_risk_array']))
                            @foreach( $credit_score_array['demographic']['attribute_risk_array'] as $key => $value)
                                <td>{{$value}}</td>
                            @endforeach
                        @else
                            @for($i =0;$i<6;$i++)
                                <td>unknown</td>
                            @endfor
                        @endif
                        <td>{{isset($credit_score_array['demographic']['component_risk_weight'])?$credit_score_array['demographic']['component_risk_weight']:null}}</td>
                        <td>{{isset($credit_score_array['demographic']['total_component_weight'])?$credit_score_array['demographic']['total_component_weight']:null}}</td>
                        <td>{{isset($credit_score_array['demographic']['component_risk'])?round($credit_score_array['demographic']['component_risk']* 100,2):null}}</td>
                    </tr>
                </table>
            </div>
            <div id="financial" class="tab-pane fade">
                <h3>Financial</h3>
                <div class="table-responsive">
                    <table class="table table-striped" style="border:1px solid #dedcdc;">
                    <thead>
                    <th>Revenues from water distribution</th>
                    <th>Additional income source</th>
                    <th>Additional salaried occupation</th>
                    <th>Monthly saving</th>
                    <th>Has bank</th>
                    <th>Percentage of debt on ebitha</th>,
                    <th>Recent large purchase through payment plan done</th>
                    <th>Future capital expenditure causing cash flow interruptions</th>
                    <th>Distance to nearest market</th>
                    <th>Land owned</th>
                    <th>Percentage of solar loan on expense</th>
                    <th>percentage of expenses on income</th>
                    <th>Component Risk Weight</th>
                    <th>Total component Weight</th>
                    <th>Percentage</th>
                    </thead>
                    <tr>
                        @if(isset($credit_score_array['financial']['attribute_risk_array']))
                            @foreach( $credit_score_array['financial']['attribute_risk_array'] as $key => $value)
                                <td>{{$value}}</td>
                            @endforeach
                        @else
                            @for($i =0;$i<12;$i++)
                                <td>unknown</td>
                            @endfor
                        @endif
                        <td>{{isset($credit_score_array['financial']['component_risk_weight'])?$credit_score_array['financial']['component_risk_weight']:null}}</td>
                        <td>{{isset($credit_score_array['financial']['total_component_weight'])?$credit_score_array['financial']['total_component_weight']:null}}</td>
                        <td>{{isset($credit_score_array['financial']['component_risk'])? round($credit_score_array['financial']['component_risk']* 100,2):null}}</td>
                    </tr>
                </table>
                </div>
            </div>
            <div id="agricultural" class="tab-pane fade">
                <h3>Agricultural</h3>
                <table class="table table-striped table-responsive" style="border:1px solid #dedcdc" id = "admin-table">
                    <thead>
                    <tr>
                        <td>Crop risk</td>
                        <td>Livestock risk</td>
                        <td>Crop diversity</td>
                        <td>Has cereal grains only</td>
                        <td>Crop Inventory</td>
                        <td>Livestock diversity</td>
                        <td>Component Risk Weight</td>
                        <td>Total component Weight</td>
                        <td>Percentage</td>
                    </tr>
                    </thead>
                    <tr>
                        @if(isset($credit_score_array['agricultural']['attribute_risk_array']))
                            @foreach( $credit_score_array['agricultural']['attribute_risk_array'] as $key => $value)
                                <td>{{$value}}</td>
                            @endforeach
                        @else
                            @for($i =0;$i<6;$i++)
                                <td>unknown</td>
                            @endfor
                        @endif
                        <td>{{isset($credit_score_array['agricultural']['component_risk_weight'])?$credit_score_array['agricultural']['component_risk_weight']:null}}</td>
                        <td>{{isset($credit_score_array['agricultural']['total_component_weight'])?$credit_score_array['agricultural']['total_component_weight']:null}}</td>
                        <td>{{isset($credit_score_array['agricultural']['component_risk'])?round($credit_score_array['agricultural']['component_risk']* 100,2):null}}</td>
                    </tr>
                </table>
            </div>

            <div id="psychometric" class="tab-pane fade">
                <h3>Psychometric</h3>
                <table class="table table-responsive" style="border:1px solid #dedcdc" id = "admin-table">
                    <thead>
                    <tr>
                        <td>Size fish</td>
                        <td>Neighbour trust</td>
                        <td>Harvest lost</td>
                        <td>village_meeting</td>
                        <td>Farming skill</td>
                        <td>Image recall exercise</td>
                        <td>Question timing</td>
                        <td>Answered all questions</td>
                        <td>equivocating_question</td>
                        <td>Component Risk Weight</td>
                        <td>Total component Weight</td>
                        <td>Percentage</td>
                    </tr>
                    </thead>
                    <tr>
                        @if(isset($credit_score_array['psychometric']['attribute_risk_array']))
                            @foreach( $credit_score_array['psychometric']['attribute_risk_array'] as $key => $value)
                                <td>{{$value}}</td>
                            @endforeach
                        @else
                            @for($i =0;$i<9;$i++)
                                <td>unknown</td>
                            @endfor
                        @endif
                        <td>{{isset($credit_score_array['psychometric']['component_risk_weight'])?$credit_score_array['psychometric']['component_risk_weight']:null}}</td>
                        <td>{{isset($credit_score_array['psychometric']['total_component_weight'])?$credit_score_array['psychometric']['total_component_weight']:null}}</td>
                        <td>{{isset($credit_score_array['psychometric']['component_risk'])?round($credit_score_array['psychometric']['component_risk']* 100,2):null}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection