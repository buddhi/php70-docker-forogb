@extends('admin.default')
@section('css')
    <style>
        th,td{
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    <h3> Categories of {{ucfirst(str_replace("_"," ",$attribute->name))}}<h3>
    <table class="table table-striped" style="border:1px solid #dedcdc" >
        <thead>
            <tr>
                <th>S No</th>
                <th>Name</th>
                <th>Risk Weight</th>
            </tr>
        </thead>
        @if(isset($attribute->categories) && count($attribute->categories)>0)
            @foreach($attribute->categories as $key=> $category)
                <tr>
                    <td>{{$key+1 }}</td>
                    <td>{{ucfirst(str_replace("_"," ",$category->name))}}</td>
                    <td>{{$category->category_weight_factor }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">No data found</td>
            </tr>
        @endif

    </table>
@endsection