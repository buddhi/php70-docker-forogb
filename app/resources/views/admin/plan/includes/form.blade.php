<div class="col-md-12">
    {!! Form::label('name', 'Name', ['class' => 'inline-label required']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder' => 'Enter the custom plan name']) !!}
    @if ($errors->has('name'))
        <span class="error">{{ $errors->first('name') }}</span>
    @endif
</div>

<div class="col-md-12">
    {!! Form::label('code', 'Code:', ['class' => 'inline-label required' ]) !!}
    {!! Form::text('code', null, ['class' => 'form-control','placeholder' => 'Enter the code ','required' =>'required']) !!}
    @if ($errors->has('code'))
        <span class="error">{{ $errors->first('code') }}</span>
    @endif
</div>


<div class="row">
    <div class="col-md-6">
        {!! Form::label('pump_size', 'Pump Size', ['class' => 'inline-label required' ]) !!}
        {!! Form::number('pump_size', null, ['class' => 'form-control','placeholder' => 'Enter the Pump Size','required' =>'required']) !!}
        @if ($errors->has('pump_size'))
            <span class="error">{{ $errors->first('pump_size') }}</span>
        @endif
    </div>

    <div class="col-md-6">
        {!! Form::label('cost', 'Cost', ['class' => 'inline-label' ]) !!}
        {!! Form::number('cost', null, ['class' => 'form-control','placeholder' => 'Enter the cost of plan','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('cost'))
            <span class="error">{{ $errors->first('cost') }}</span>
        @endif
    </div>


</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::label('daily_discharge', 'Daily Discharge', ['class' => 'inline-label' ]) !!}
        {!! Form::number('daily_discharge', null, ['class' => 'form-control','placeholder' => 'Enter daily discharge','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('daily_discharge'))
            <span class="error">{{ $errors->first('daily_discharge') }}</span>
        @endif
    </div>

    <div class="col-md-6">
        {!! Form::label('solar_pv_size', 'Solar Pv size', ['class' => 'inline-label' ]) !!}
        {!! Form::number('solar_pv_size', null, ['class' => 'form-control','placeholder' => 'Enter the solar pv size','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('solar_pv_size'))
            <span class="error">{{ $errors->first('solar_pv_size') }}</span>
        @endif
    </div>
</div>

@if(!$project->isAdvanceAmountPaid())
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('emi_amount', 'EMI amount', ['class' => 'inline-label' ]) !!}
            {!! Form::number('emi_amount', null, ['class' => 'form-control','placeholder' => 'Enter EMI amount','required' =>'required','step'=>'any']) !!}
            @if ($errors->has('emi_amount'))
                <span class="error">{{ $errors->first('emi_amount') }}</span>
            @endif
        </div>

        <div class="col-md-6">
            {!! Form::label('emi_count', 'Total number of EMI count:', ['class' => 'inline-label' ]) !!}
            {!! Form::number('emi_count', null, ['class' => 'form-control','placeholder' => 'Enter the EMI count','required' =>'required','step'=>'any']) !!}
            @if ($errors->has('emi_count'))
                <span class="error">{{ $errors->first('emi_count') }}</span>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {!! Form::label('advance_amount', 'Advance amount', ['class' => 'inline-label' ]) !!}
            {!! Form::number('advance_amount', null, ['class' => 'form-control','placeholder' => 'Enter advance amount','required' =>'required','step'=>'any']) !!}
            @if ($errors->has('advance_amount'))
                <span class="error">{{ $errors->first('advance_amount') }}</span>
            @endif
        </div>
        <div class="col-md-6">
            {!! Form::label('emi_start_date', 'EMI start date(mm/dd/yyyy)', ['class' => 'inline-label' ]) !!}
            {!! Form::date('emi_start_date',isset($project->emi_start_date)? Carbon\Carbon::parse($project->emi_start_date) :null, ['class' => 'form-control','placeholder' => 'Set the EMI start date']) !!}
            @if ($errors->has('emi_start_date'))
                <span class="error">{{ $errors->first('emi_start_date') }}</span>
            @endif
        </div>
    </div>
    <br/>
@else
    <div class="col-md-12">
        <div class="inline-label" style="font-weight: bold;font-size: 0.9em;display: block;color: #757575;">
            Emi Amount:{{displayUnitFormat('amount',$project->emi_amount)}}
        </div>
        <div class="inline-label" style="font-weight: bold;font-size: 0.9em;display: block;color: #757575;">
            Total number of emi defined for project:{{$project->emi_count}}
        </div>
        <div class="inline-label" style="font-weight: bold;font-size: 0.9em;display: block;color: #757575;">
            Advance amount: {{displayUnitFormat('amount',$project->advance_amount)}}
        </div>
        <div class="inline-label" style="font-weight: bold;font-size: 0.9em;display: block;color: #757575;">
            Advance paid: yes
        </div>
        <div class="inline-label" style="font-weight: bold;font-size: 0.9em;display: block;color: #757575;">
            Emi start date: {{$project->emi_start_date}}
        </div>
    </div>

@endif