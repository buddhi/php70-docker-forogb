@extends('admin.default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
@section('content')
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success_message') }}
        </div>
    @endif
    <div class="container">
    <div class="row col-md-12">
        <h3><strong>Project Plan List</strong></h3>
        <table class="table table-striped" style="border:1px solid #dedcdc" id = "admin-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Farmer Name</th>
                <th>Plan</th>
                <th>Status</th>
                <th>comments</th>
                <th>Action</th>
            </tr>
            </thead>
            @if (isset($projects) && sizeof($projects) > 0)
                @foreach($projects as $project)
                    <tr>
                        <td>{{ $project->id }}</td>
                        <td>{{ $project->farmer_name }}</td>
                        <td>{{ $project->name or 'No Plan Selected'}}</td>
                        <td>
                            <button class="btn btn-xs" style="padding:2px">{{$project->status}}</button>
                        </td>
                        @if($project->comments->first() && $project->comments->first()->comment)
                            <td>{!!
                                    (strlen($project->comments->first()->comment) < 20 ?
                                    $project->comments->first()->comment
                                    :substr($project->comments->first()->comment,0,20)."..... <a href = '#' data-toggle='modal' data-target = '#commentModal$project->id'>See More</a>")
                                    !!}

                                <div class="modal fade" id="commentModal{{$project->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                                <h4 class="modal-title">Developer Comment</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! $project->comments->first()->comment !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" style="padding:2px"
                                                        data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        @else
                            <td>No comments</td>
                        @endif

                        <td><a href="{{route('admin.project-plan.edit',$project->id)}}">
                                <button class="btn btn-xs btn-primary" style="padding:2px">Edit plan</button>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">No data found.</td>
                </tr>
            @endif
        </table>
    </div>
    </div>
@endsection