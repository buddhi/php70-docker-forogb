@extends('admin.default')
@section('content')

    <div class="form-container form">
        <div class="col-md-12 text-center">
            <h4>Editing plan for Project id: {{$project->id}}</h4>

        </div>
        <div class="col-md-12">
            {!! Form::model($project, ['route' => ['admin.project-plan.update', $project->id], 'class' => 'form-horizontal','enctype' => "multipart/form-data"]) !!}
            @include('admin.plan.includes.form')
            <div class="col-md-12">
                <button class="btn btn-info" id="submit" name="submit" type="submit" data-type="save">Update plans</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div>
@endsection


