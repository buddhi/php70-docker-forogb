<!-- layout.nunjucks -->
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="ui/"/>
    <meta charset="UTF-8">
    <title>Log In</title>
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">

</head>
<body>
<div class="text-center">
    @if (session()->has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert"></button>
            <strong>{!! session()->get('success') !!}</strong>
            <br>
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <strong>{!! session()->get('error') !!}</strong>
        </div>
    @endif
</div>

<div class="container">
    <div class="row">
        <div class="form-container form">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="form-logo">
                    </div>
                    <h1 class="page-title text-center">Welcome</h1>
                </div>
                <form class="form-horizontal" method="POST" action="{{ route('login') }}" id ="select-plan">
                    {{ csrf_field() }}
                    <div class="col-md-12 form-group">
                        <label for="email">Email Address</label>
                        <input class="form-control" type="text" name="email" value="{{old('email')}}" data-validetta="required,email">
                        @if ($errors->has('email'))
                            <span class="error">
                            <p>{{ $errors->first('email') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-12 form-group">
                        <span class="pull-right"><a href="{{url('/password/reset')}}">Forgot password?</a></span>
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" data-validetta="required">
                        @if ($errors->has('password'))
                            <span class="error">
                                <p>{{ $errors->first('password') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-12 form-group">
                        <button class="btn btn-success btn-block" type="submit">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{asset('offgrid-bazaar/js/bootstrap.min.js')}}"></script>
<script>
    $("#select-plan").validate({
        rules: {
            "comment": {
                required: true,
            }
        },
        onkeyup: function (element) {
            $(element).valid();
        },
        onfocusout: function (element) {
            $(element).valid();
        }
    });

</script>
</body>
</html>