<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">
    {{-- START CSS--}}
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/screen.css')}}" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/print.css')}}" media="print">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/ie.css')}}" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href=https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.1/css/simple-line-icons.css">
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/theme/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/alertify.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    {{--Necessary for create project so js is kept above--}}
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/9bb1aea1/jquery.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <title>Offgrid | Admin Panel</title>
    @yield('css')
    <style>
        .custom-btn{
            padding:2px;
            background-color: #e0dddd;
            color: #4a4848;
            margin:1.5px;
            padding: 4px;
            font-size: 11px;
        }
        .custom-btn:hover{
            background-color: #e3ddec80;
        }
        input[type=search] {
            border: 1px solid grey;
            border-radius: 2px;
            font-weight:normal;
        }
        div.dt-buttons{
            position: absolute;
            left: 0;
            bottom:0;
        }
    </style>

</head>

<body>
<div class="container" id="page">
    @include('admin.headsection')
    @yield('content')
</div>

{{--START JS--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('/offgrid-bazaar/js/alertify/alertify.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
@yield('js')
@php
    use App\OGB\Constants;
    use Illuminate\Support\Facades\Route;
    $route = Route::currentRouteName();
    $columns = returnColumnsToDownloadInTable($route);

    function returnColumnsToDownloadInTable($route){
        switch ($route){
            case "admin.project.list":
                return Constants::route_admin_project_list_columns;

            case "admin.meter.list":
                return Constants::route_admin_meter_list_columns;

            case "admin.user.list":
                 return Constants::route_admin_user_list_columns;
            default :
                return Constants::default_admin_list_columns;

        }
    }
@endphp
<script>
    $(document).ready( function () {
        $('#admin-table').DataTable({
            "language": {
                "search": "Search in table:"
            },
            "paging":   true,
            "ordering": true,
            "info":     false,
            stateSave: true,
            dom: 'lBfrtip',
            buttons: [
                {   extend: 'pdf',
                    footer: true,
                    exportOptions: {
                        columns: {!! json_encode($columns); !!}
                    }
                },
                {   extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns:{!! json_encode($columns); !!}
                    }
                },
                {   extend: 'print',
                    footer: true,
                    exportOptions: {
                        columns: {!! json_encode($columns); !!}
                    }
                }
            ]

         });
        $('#search_0').css('text-align', 'center');
    } );
</script>
{{--END JS--}}
</body>
</html>