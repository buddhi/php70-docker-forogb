@extends('admin.default')
@section('content')
    <div class="col-md-12 text-center">
        <h1>Edit user</h1>
    </div>
    <div class="form-container form">
        <div class="col-md-12">
            {!! Form::model($user, ['route' => ['admin.user.update', $user->user_id], 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"]) !!}
            {{csrf_field()}}
            {!! Form::hidden('user_id', $user->user_id) !!}

            <div class="col-md-12">
                {!! Form::label('full_name', 'Full name:', ['class' => 'inline-label required' ]) !!}
                {!! Form::text('full_name', null, ['class' => 'form-control','placeholder' => 'Enter the  full name of user','required' =>'required']) !!}
                @if ($errors->has('full_name'))
                    <span class="error">{{ $errors->first('full_name') }}</span>
                @endif
            </div>

            <div class="col-md-12">
                {!! Form::label('phone', 'Phone No', ['class' => 'inline-label required']) !!}
                {!! Form::text('phone', null, ['class' => 'form-control','placeholder' => 'Enter the phone admin']) !!}
                @if ($errors->has('phone'))
                    <span class="error">{{ $errors->first('phone_no') }}</span>
                @endif
            </div>
            <div class="col-md-12">
                {!! Form::label('address', 'Address', ['class' => 'inline-label required' ]) !!}
                {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Enter the address ','required' =>'required']) !!}
                @if ($errors->has('address'))
                    <span class="error">{{ $errors->first('address') }}</span>
                @endif
            </div>

            <div class="col-md-12">
                {!! Form::label('email', 'Email:', ['class' => 'inline-label required' ]) !!}
                {!! Form::text('email', null, ['class' => 'form-control','placeholder' => 'Enter the email of organisation-admin','required' =>'required']) !!}
                @if ($errors->has('email'))
                    <span class="error">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="col-md-12">
                <button class="btn btn-info" id="submit" name="submit" type="submit" data-type="save">Update User</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div>

@endsection
