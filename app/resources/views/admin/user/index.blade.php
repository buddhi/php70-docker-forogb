@extends('admin.default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
@section('content')
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success_message') }}
        </div>
    @endif
    <div class="container">
        <div class="row col-md-12">
            <h3><strong>Users</strong></h3>
            <table class="table table-striped" style="border:1px solid #dedcdc" id="admin-table">
                <thead>
                <tr>
                    <th colspan=9>
                        <select class="pull-right" onchange="javascript:filterProject(this)">
                            <option value="active" @if($filterStatus == 'active') selected="selected" @endif>Active
                            </option>
                            <option value="inactive" @if($filterStatus == 'inactive') selected="selected" @endif>
                                Inactive
                            </option>
                            <option value="all" @if($filterStatus == 'all')  selected="selected" @endif>All</option>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Email verification</th>
                    <th>Registered:</th>
                    <th>Status</th>
                    <th>Enable|Disable</th>
                    <th>Action</th>
                </tr>
                </thead>
                @if ($users->count() > 0)
                    @foreach($users as $user)
                        @if(in_array(\App\Enums\Role::ADMIN,$user->roles->pluck('role')->toArray()))
                            @continue;
                        @endif
                        <tr>
                            <td>
                                {{ $user->user_id }}
                            </td>
                            <td>
                                <b>{{ $user->full_name }}</b>

                                <p style="font-size: 12px">
                                    @if($user->roles->count()>0)
                                        @foreach($user->roles  as $key => $value)
                                            {{returnRole($value->role)}}
                                            @if($key != count($user->roles)-1),@endif
                                        @endforeach
                                    @endif
                                </p>

                            </td>
                            <td>
                                {{ $user->email }}
                                <br>
                                @if(isset($user->provider))
                                    ({{$user->provider}})
                                @endif

                            </td>
                            <td>
                                @if(!$user->verified)
                                    unverified
                                    @if(!$user->delete_flg)
                                        <a href="{{route('admin.user.resend-verification',$user->user_id)}}">
                                            <button class="btn btn-xs btn-success"
                                                    style="padding: 6px 3px; font-size:9px;line-height:2px;letter-spacing:1px;">
                                                Resend Verification email
                                            </button>
                                        </a>
                                    @endif
                                @else
                                    verified
                                @endif

                            </td>
                            <td>
                                {!! isset($user->created_on)? \Carbon\Carbon::parse($user->created_on)->toDateString():"--" !!}</td>
                            </td>
                            <td>{{$user->delete_flg? 'InActive': 'Active'}}</td>
                            <td>
                                @if(!$user->delete_flg)
                                    <a href="{{route('admin.user.disable',$user->user_id)}}">
                                        <button onclick="return confirm('Do you want to disable user?');"
                                                class="btn btn-xs btn-danger" style="padding:2px">Disable
                                        </button>
                                    </a>
                                @else
                                    <a href="{{route('admin.user.disable',$user->user_id)}}">
                                        <button onclick="return confirm('Do you want to enable user?');"
                                                class="btn btn-xs btn-primary" style="padding:2px">Enable
                                        </button>
                                    </a>
                                @endif
                            </td>
                            <td>
                                @if(!$user->delete_flg)
                                    <a href="{{route('admin.user.edit',$user->user_id)}}">Edit User</a>
                                @else
                                    --
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No data found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function filterProject(elm) {
            window.location = '/admin/user' + '?filter=' + elm.value;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('body').on('click', '#activate', function () {
                var $this = $(this);
                $.ajax({
                    method: 'POST',
                    url: '{{ route('admin.user.activate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        user_id: $this.attr('attr-id'),
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.error) {
                            alert(data.message);
                        } else {
                            var in_active_html = '<p>Activated</p>';
                            if (!data.status) {
                                $this.closest('td').html('').html(in_active_html);
                            }

                        }
                    }
                });

            });

        });

    </script>
@endsection