@extends('admin.default')
@php
$month = App\components\MonthUtility::$_MONTHS;
@endphp
@section('css')
    <style>
            #show_financial_institute{
                display: none;
            }
    </style>
@endsection
@section('content')
    <div class=" form-container-project form" >
        <div class="col-sm-12">
            <h1>Create new project({{__('app.create_new_project')}})</h1>
            <h4>
                Fill in the information about the project below.
                {{__('app.fill_in_the_information')}}
            </h4>
            <br>
        </div>
        <form action="{{route('admin.project.update', $project->id) }}" method="post" class="form-horizontal" id="project-form">
        <input type="hidden" name="_method" value="put">
        {{csrf_field()}}
        @include('includes.form')
        <button class="btn btn-success" id="submit-analysis" name="submit-analysis" type="submit" data-type="save" style="margin-left: 20px">Update</button>
        </form>
    </div>
@endsection
@section('js')
    @include('includes.project_form_script')
    @include('includes.project_form_validation')
@endsection