@extends('admin.default')
@php
   $conversionFactor = currencyConverterFactor("USD");
@endphp
@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style>
    .custom-btn{
        padding:2px;
        background-color: #e0dddd;
        color: #4a4848;
        margin:1.5px;
        padding: 4px;
        font-size: 11px;
    }
    .custom-btn:hover{
        background-color: #e3ddec80;
    }
    input[type="checkbox"] {
        height: 20px;
        width: 20px;
        margin-right: 10px;
    }


     body{
         margin: 0;
         padding: 0;
     }

    .clearfix{
        display: block;
        clear:both;
    }
    .pprogress{
        margin: 0;
        padding: 1px;
        background-color: #e9e9e9;
        border: 1px solid #a5baa4;
        box-sizing:border-box;
    }

    .pprogress li{
        float: left;
        list-style: none;
        width: 20px;
        height: 15px;
        border-left: 1px solid #658d64;
    }

    .pprogress li.active{
        background-color: #8cff89;
    }

    .pprogress li:first-child{
        border:none;
    }
    .dropdown-menu {
       
    left: -25px;
   
    float: left;
    min-width: 425px;
    border: 1px solid rgb(155, 155, 155);
    border-radius: 0px;
    -webkit-box-shadow: none;
    padding: 10px;
    
    }

    .checklist-row{
        padding: 10px;
        border-bottom: 1px solid #cdcdcd;
        margin: 0 0 7px 0;
    }
    .checklist-row .col-sm-10 {
        padding: 4px 0 0 0;
    }
    .progress-td {background:#f0ad4e; border: 1px solid rgba(3, 3, 3, 1); border-radius: 2px; height: 5px;}
    .progress-bar-custom {background: #5cb85c;}
</style>
@endsection
@section('content')
    <div class="container">
        @if (session()->has('success_message'))
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session()->get('success_message') }}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session()->get('error_message') }}
            </div>
        @endif

        @if(session()->has('error_message'))
            <div class="alert alert-danger">
                <strong>{!! session()->get('error_message') !!}</strong>
            </div>
        @endif

        <div class="row col-md-12">
            <h3><strong>Project List</strong></h3>

            <table class="table table-striped display" style="border:1px solid #dedcdc" id = "admin-table">
                <thead>
                <tr>
                    <th colspan="10">
                        <select class="pull-right" onchange="javascript:filterProject(this)">
                            <option value="active" @if($filterStatus == 'active') selected = "selected" @endif>Active</option>
                            <option value="inactive" @if($filterStatus == 'inactive') selected="selected" @endif>Inactive</option>
                            <option value="all" @if($filterStatus == 'all')  selected="selected" @endif>All</option>
                            @foreach(\App\Enums\ProjectStatus::ALL_PROJECT_STATUS as $status)
                            <option value="{{$status}}" @if($filterStatus == $status) selected="selected" @endif>{{ucfirst($status)}}</option>
                            @endforeach
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>Farmer Name</th>
                    <th>Created by</th>
                    <th>Status</th>
                    <th>Investor List</th>
                    <th>Files</th>
                    <th>PaymentId</th>
                    <th class = "text-center">Action</th>
                    <th>Enable|Disable</th>
                </tr>
                </thead>
                @if ($data['row']->count() > 0)
                    @foreach($data['row'] as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>
                                @if(strtolower($row->status)== 'operational' || strtolower($row->status)== 'error')
                                    <a href="{{route('meterdata_demo.show',$row->id)}}">{{$row->farmer_name }}</a>
                                @else
                                    {{$row->farmer_name}}
                                @endif
                            </td>
                            <td>{{ isset($row->user->full_name)? $row->user->full_name:'--' }}</td>
                            <td>
                            @if($row->status != 'operational' && $row->status != 'funding' && $row->status != 'error' && $row->status != 'overdue' && $row->status != 'plan')
                                    <ul class="pprogress" id = "ul-nearest-{{$row->id}}" style="width:{{20 * $row->totalTransitionStateCount($row->status)+ ($row->totalTransitionStateCount($row->status)+2)}}px">
                                        @for($i = 1;$i<= $row->totalTransitionStateCount($row->status);$i++)
                                            @if($i <= $row->currentTransitionStateCount($row->status))
                                                <li class="active"></li>
                                            @else
                                                <li class="inactive"></li>
                                            @endif
                                        @endfor
                                        <div class="clearfix"></div>
                                    </ul>
                                    <ul style="list-style: none; padding:0;min-width:70px;">
                                        <li class="dropdown this-works">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ucfirst($row->status)}}<b class="caret"></b></a>
                                            <div class="dropdown-menu dropdown-form">
                                                {!! Form::open(['route' => ['admin.state.update',$row->id],'enctype' => "multipart/form-data" ,'id' => 'form'.$row->id]) !!}
                                                 <div class="checklist-row">
                                                @foreach($row->getNextStatusCondition($row->status) as  $state)
                                                         <div class="row">
                                                            <div class="col-sm-2">
                                                                {!! Form::checkbox($state['id'],null, array_key_exists('value',$state), ['class'=>"check-box$row->id check-item" ,'attr-id' => "$row->id"]) !!}
                                                            </div>
                                                            <div class="col-sm-10 pt-4">
                                                                {{$state['description']}}
                                                            </div>
                                                        </div>
                                                @endforeach
                                                  </div>
                                                <button type="button" class="btn btn-default btnClose btn-xs">Close</button>
                                                <button  type="button" class="btn pull-right btn-xs" id ="update-state" attr-id = "{{$row->id}}" style="background-color: #e0dddd" di>
                                                @php
                                                    switch ($row->status) {
                                                        case 'new':
                                                            echo "Edit plan and approve project";
                                                            break;
                                                        case 'approved':
                                                            echo "Approve for funding";
                                                            break;
                                                        case 'funding':
                                                            echo "Approve for Funded";
                                                            break;
                                                        case 'funded':
                                                            echo "Installation Completed";
                                                            break;
                                                        case 'installed':
                                                            echo "Add meter and make operational";
                                                            break;
                                                        default:
                                                            echo "Update Project State";
                                                    }
                                                @endphp
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </li>
                                    </ul>
                                @elseif($row->status == 'funding')
                                    {{ucfirst($row->status)}}
                                    @if(isset($row->investment['investment_percent']))
                                        <div data-toggle="tooltip" data-placement="top" title="Remaining Amount: ${{number_format($row->getRemainingAmount()/$conversionFactor)}} ">
                                        <div class="progress-td">
                                            <div class="progress-bar progress-bar-custom" role="progressbar"
                                                 aria-valuenow="{{number_format($row->investment['investment_percent'])}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$row->investment['investment_percent']}}%;"></div>
                                        </div>
                                        <span style="color: #2a9227">${{number_format($row->investment['investment_amount']/$conversionFactor)}}</span>
                                            of ${{number_format($row->cost/$conversionFactor)}}
                                        </div>
                                    @endif
                                @else
                                    {{ucfirst($row->status)}}
                                @endif
                                @if(strtolower($row->status)== 'funded' || strtolower($row->status)== 'installed')
                                    @if(is_null($row->meter_id))
                                        <a href="{{route('admin.meter.create',$row->id)}}">
                                            <button class="btn btn-xs btn-success"
                                                    style="padding: 6px 3px; font-size:9px;line-height:2px;letter-spacing:1px;">
                                                Add Meter
                                            </button>
                                        </a>
                                    @endif
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.project.investor',$row->id)}}">Investors</a></td>
                            @if($row->uploadable()->count() >0)
                            <td>
                                {!!"<a href = '#' data-toggle='modal' data-target = '#uploadModal$row->id'>View files</a>"!!}
                                <div class="modal fade" id="uploadModal{{$row->id}}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                                <h4 class="modal-title">Uploaded files</h4>
                                            </div>
                                            <div class="modal-body">
                                                @foreach($row->uploadable as $upload)
                                                    <div class="row" style="padding:0px 20px">
                                                        {!! $upload->actual_file_name!!}
                                                        <a id ="downloadFile"  class="btn btn-sm btn-success pull-right" href="{{route('admin.project-files.download',['upload_id' =>$upload->id])}}">Download</a>
                                                    </div>
                                                    <br>
                                                @endforeach
                                                <div class="row" style="padding:0px 20px">
                                                    <a id ="downloadFileAll"  class="btn btn-sm btn-primary pull-right" href="{{route('admin.project-files.all.download',['project_id' =>$row->id])}}">Download All</a>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" style="padding:2px"
                                                        data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            @else
                                <td>---</td>
                            @endif
                            <td>
                                {{isset($row->payment_id)? $row->payment_id : '---'}}
                            </td>
                            <td>
                                <a href="{{route('admin.project-plan.edit',$row->id)}}"><button class="btn btn-xs btn-link custom-btn">Edit plan</button></a>
                                <a href="{{route('admin.project.edit',$row->id)}}"> <button class="btn btn-xs btn-link custom-btn" >Edit Project</button></a>
                                @if($row->status == 'operational')
                                    <a href="{{route( 'admin.meter.edit',$row->meter_id)}}"><button class="btn btn-xs btn-link custom-btn">Edit meter</button></a>
                                @endif
                                <a href="{{route( 'admin.credit',$row->id)}}"><button class="btn btn-xs btn-link custom-btn">CreditScore</button></a>
                                <a href="{{route( 'remark.index',$row->id)}}"><button class="btn btn-xs btn-link custom-btn">Remarks</button></a>
                            </td>
                            <td>
                                @if(!$row->delete_flg)
                                    <a href="{{route('admin.project.disable',$row->id)}}">
                                        <button onclick= "return confirm('Do you want to disable project?');" class="btn btn-xs btn-danger" style="padding:2px">Disable</button>
                                    </a>
                                @else
                                    <a href="{{route('admin.project.disable',$row->id)}}">
                                        <button onclick= "return confirm('Do you want to enable project?');" class="btn btn-xs btn-primary" style="padding:2px">Enable</button>
                                    </a>
                                @endif
                            </td>
                        </tr>

                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
            </table>
            <a class="btn btn-sm btn-default" style="background-color:#e9e9e9" href="{{route('admin.project.csv')}}">Download Survey data</a>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function filterProject(elm){
            window.location ='/admin/project'+'?filter='+elm.value;
        }
    </script>
    <script>

        $(document).ready(function () {
            $(".check-item").change(function() {
                const project_id  = $(this).attr('attr-id');
                const checkbox_id = 'ul-nearest-'+ project_id;
                var value = 0;
                if(this.checked) {
                    $( "#"+ checkbox_id).find('li.inactive:first').addClass('active').removeClass('inactive');
                    value= 1;
                }else{
                    $( "#"+ checkbox_id ).find( "li.active:last" ).removeClass( "active").addClass('inactive');
                }
                $.ajax({
                    method: 'POST',
                    url:'{{route('admin.transition.state.update')}}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        project_id:$(this).attr('attr-id'),
                        state_id:$(this).attr('name'),
                        value:value,
                    },
                    success: function (response) {
                        console.log("Error response");
                    }
                });
            });

        });
        $(document).ready(function () {
            $('body').on('click', '#update-state', function () {
                const project_id  = $(this).attr('attr-id');
                const class_name = 'check-box'+ project_id;
                const total_checkbox_number =  $("input."+class_name+":checkbox").length;
                const total_checked_checkbox =  $("input."+class_name+":checkbox:checked").length;
                const form_value = "form"+project_id;
                if(total_checkbox_number === total_checked_checkbox){
                    $("#"+form_value).submit()
                }
            })
        });
        $('.btnClose').click(function() {
            $(this).parents('.dropdown').find('.dropdown-toggle').dropdown('toggle')
        });
    </script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
