@extends('admin.default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
@section('content')
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success_message') }}
        </div>
    @endif
    <div class="container">
        <div class="row col-md-12">
            <h3><strong>Meter Data</strong></h3>
            <table class="table table-striped" style="border:1px solid #dedcdc" id = "admin-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Phone No</th>
                    <th>Hardware ID</th>
                    <th>Firmware Version</th>
                    <th>Type</th>
                    <th>status</th>
                    <th>Last Discharge</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                @if ($meters->count() > 0)
                    @foreach($meters as $meter)
                        <tr>
                            <td>{{ $meter->id }}</td>
                            <td>{{ $meter->phoneNo }}</td>
                            <td>{{ $meter->hardwareId}}</td>
                            <td>{{ $meter->firmwareVersion }}</td>
                            <td>{{ $meter->type }}</td>
                            <td>{{$meter->status? "Active":"Inactive"}}</td>
                            <td>{{ isset($meter->last_meter_data)? $meter->last_meter_data->toDateTimeString(). " (" .$meter->last_meter_data->diffForHumans(\Carbon\Carbon::now()).")" :"No meter data" }}</td>
                            <td>
                                <a class='btn btn-xs btn-link custom-btn' style="padding:2px" href="{{ route('admin.meter.edit',$meter->id) }}">Edit Meter</a>
                                @if(isset($meter->project_id))
                                    <a class='btn btn-link btn-xs custom-btn' style="padding:2px" href="{{ route('admin.project.edit',$meter->project_id) }}">Edit Project</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No data found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection