    <div class="col-md-12">
    {!! Form::label('phoneNo', 'Phone No', ['class' => 'inline-label required']) !!}
    {!! Form::number('phoneNo', null, ['class' => 'form-control','placeholder' => 'Enter the phone']) !!}
    @if ($errors->has('phoneNo'))
        <span class="error">{{ $errors->first('phoneNo') }}</span>
    @endif
</div>

<div class="col-md-12">
    {!! Form::label('hardwareId', 'Hardware Id:', ['class' => 'inline-label required' ]) !!}
    {!! Form::text('hardwareId', null, ['class' => 'form-control','placeholder' => 'Enter the hardware Id','required' =>'required']) !!}
    @if ($errors->has('hardwareId'))
        <span class="error">{{ $errors->first('hardwareId') }}</span>
    @endif
</div>
<div class="col-md-12">
    {!! Form::label('firmwareVersion', 'Firmware version', ['class' => 'inline-label required' ]) !!}
    {!! Form::text('firmwareVersion', null, ['class' => 'form-control','placeholder' => 'Enter the firmware version','required' =>'required']) !!}
    @if ($errors->has('firmwareVersion'))
        <span class="error">{{ $errors->first('firmwareVersion') }}</span>
    @endif
</div>
<div class="col-md-12">
    {!! Form::label('type', 'Type', ['class' => 'inline-label required' ]) !!}
    {!! Form::text('type', null, ['class' => 'form-control','placeholder' => 'Enter the type of meter','required' =>'required']) !!}
    @if ($errors->has('type'))
        <span class="error">{{ $errors->first('type') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('sys_eff1', 'System Efficiency when DC Power <= 250', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff1', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when Dc Power <= 250','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff1'))
            <span class="error">{{ $errors->first('sys_eff1') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff2', 'system efficiency when DC Power > 250 and <= 350', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff2', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 250 and <= 350','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff2'))
            <span class="error">{{ $errors->first('sys_eff2') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff3', 'System Efficiency when DC Power > 350 and <= 450', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff3', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 350 and <= 450','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff3'))
            <span class="error">{{ $errors->first('sys_eff3') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff4', 'System Efficiency when DC Power > 450 and <= 550', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff4', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 450 and <= 550','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff4'))
            <span class="error">{{ $errors->first('sys_eff4') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff5', 'System Efficiency when DC Power > 550 and <= 650', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff5', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 550 and <= 650','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff5'))
            <span class="error">{{ $errors->first('sys_eff5') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff6', 'System Efficiency when DC Power > 650 and <= 750', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff6', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 650 and <= 750','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff6'))
            <span class="error">{{ $errors->first('sys_eff6') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff7', 'System Efficiency when DC Power > 750 and <= 850', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff7', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 750 and <= 850','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff7'))
            <span class="error">{{ $errors->first('sys_eff7') }}</span>
        @endif
    </div>
    <div class="col-md-12">
        {!! Form::label('sys_eff8', 'System Efficiency when DC Power > 850', ['class' => 'inline-label' ]) !!}
        {!! Form::number('sys_eff8', null, ['class' => 'form-control','placeholder' => 'Enter the system efficiency when DC Power > 850','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('sys_eff8'))
            <span class="error">{{ $errors->first('sys_eff8') }}</span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('pump_efficiency', 'Pump efficiency', ['class' => 'inline-label' ]) !!}
        {!! Form::number('pump_efficiency', null, ['class' => 'form-control','placeholder' => 'Enter the pump efficiency of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('pump_efficiency'))
            <span class="error">{{ $errors->first('pump_efficiency') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('cd_efficiency', 'CD efficiency', ['class' => 'inline-label' ]) !!}
        {!! Form::number('cd_efficiency', null, ['class' => 'form-control','placeholder' => 'Enter the CD efficiency of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('cd_efficiency'))
            <span class="error">{{ $errors->first('cd_efficiency') }}</span>
        @endif
    </div>

    <div class="col-md-12">
        {!! Form::label('max_power', 'Max Power', ['class' => 'inline-label' ]) !!}
        {!! Form::number('max_power', null, ['class' => 'form-control','placeholder' => 'Enter the max power of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('max_power'))
            <span class="error">{{ $errors->first('max_power') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('frequency_multiplier', 'Frequency Multiplier', ['class' => 'inline-label' ]) !!}
        {!! Form::number('frequency_multiplier', null, ['class' => 'form-control','placeholder' => 'Enter the frequency multiplier of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('frequency_multiplier'))
            <span class="error">{{ $errors->first('frequency_multiplier') }}</span>
        @endif
    </div>

    <div class="col-md-12">
        {!! Form::label('rpm_multiplier', 'RPM multiplier', ['class' => 'inline-label' ]) !!}
        {!! Form::number('rpm_multiplier', null, ['class' => 'form-control','placeholder' => 'Enter the RPM multiplier of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('rpm_multiplier'))
            <span class="error">{{ $errors->first('rpm_multiplier') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('head', 'Head', ['class' => 'inline-label required' ]) !!}
        {!! Form::number('head', isset($head)? $head:null, ['class' => 'form-control','placeholder' => 'Enter the head of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('head'))
            <span class="error">{{ $errors->first('head') }}</span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::label('current_threshold', 'Current Threshold', ['class' => 'inline-label required' ]) !!}
        {!! Form::number('current_threshold', null, ['class' => 'form-control','placeholder' => 'Enter the current threshold of meter','required' =>'required','step'=>'any']) !!}
        @if ($errors->has('current_threshold'))
            <span class="error">{{ $errors->first('current_threshold') }}</span>
        @endif
    </div>
</div>

<div class="col-md-12">
    {!! Form::label('status', 'Status', ['class' => 'inline-label required']) !!}
    {!! Form::select('status', ['1' => 'Active', '0' => 'InActive'], null,['class' => 'form-control']) !!}
    @if ($errors->has('status'))
        <span class="error">{{ $errors->first('status') }}</span>
    @endif
</div>

<div class="col-md-12">
    {!! Form::label('meter_type', 'Meter Type', ['class' => 'inline-label required']) !!}
    {!! Form::select('meter_type', [\App\Enums\MeterType::AGRICULTURE_METER=> 'Agriculture Meter(Measures TH)',\App\Enums\MeterType::SMART_METER => ' Smart meter'], isset($meter->meter_type)? ($meter->meter_type == \App\Enums\MeterType::AGRICULTURE_METER)? \App\Enums\MeterType::AGRICULTURE_METER : \App\Enums\MeterType::SMART_METER : \App\Enums\MeterType::SMART_METER,['class' => 'form-control'])!!}
    @if ($errors->has('meter_type'))
        <span class="error">{{ $errors->first('meter_type') }}</span>
    @endif
</div>