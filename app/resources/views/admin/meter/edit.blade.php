@extends('admin.default')
@section('content')
    <div class="form-container form">
        <div class="col-md-12">
            {!! Form::model($meter, ['route' => ['admin.meter.update', $meter->id], 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"]) !!}
            {!! Form::hidden('id', $meter->id) !!}
            @include('admin.meter.includes.form')
            <div class="col-md-12">
                <button class="btn btn-info" id="submit" name="submit" type="submit" data-type="save">Update meter</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div>

@endsection

