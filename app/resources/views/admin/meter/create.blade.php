@extends('admin.default')
@section('content')
    <div class="form-container form">
        <div class="col-md-12">
            {!! Form::open(['route' => ['admin.meter.store',], 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"]) !!}
            {!! Form::hidden('farmer_project_id', $farmer_project_id) !!}
           @include('admin.meter.includes.form')
            <div class="col-md-12">
                <button class="btn btn-info" id="submit" name="submit" type="submit" data-type="save">Save meter</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div>
@endsection

