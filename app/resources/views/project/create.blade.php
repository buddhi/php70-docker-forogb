@extends('layouts.default')
@php
$month = App\components\MonthUtility::$_MONTHS;
@endphp
@section('title')
    Create Project
@endsection
@section('css')
    <style>
            #show_financial_institute{
                display: none;
            }

    </style>
@endsection
@section('content')
    <div class=" form-container-project form">
        <div class="col-sm-12 form-heading">
            <h1>Create new project <span class="sl-text">({{__('app.create_new_project')}})</span></h1>
            <h4>
              Fill in the information about the project below.
              <span class="sl-text">{{__('app.fill_in_the_information')}}</span>
            </h4>
            <br>
        </div>
    @if (isset($project))
    <form action="{{ route('project.update', $project->id) }}" method="post" class="form-horizontal" id="project-form">
        <input type="hidden" name="_method" value="put">
    @else
    <form action="{{route('project.store')}}" method="post" class="form-horizontal" id="project-form">
    @endif
        {{csrf_field()}}
       @include('includes.form')
        <div class="col-sm-12">
            <button class="btn btn-success" id="submit-analysis" name="submit-analysis" type="submit" data-type="save" value="submit-analysis">Submit for Analysis</button>
            @if(isset($project))
                @if(strtolower($project->status) !='plan')
                    <button class="btn btn-default"  type="submit" name="submit" value="submit">Save Draft</button>
                @endif
            @else
                <button class="btn btn-default"  type="submit" name="submit" value="submit">Save Draft</button>
            @endif
            <a class="btn btn-default pull-right" href="{{url('/project/list')}}">Cancel</a>
        </div>
    </form>
    </div>
@endsection
@section('js')
    @include('includes.project_form_script')
    @include('includes.project_form_validation')
@endsection