@extends('layouts.default')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/home_style.css')}}">
@endsection

@section('title')
     List project
@endsection
@section('content')
    <div class="container">
        <div class="col-md-6 col-lg-4 dashboard-hero hero-section nopadding">
            <div id="vue-app">
                <div class="nopadding dashboard-titlebar">
                    <div>
                        <div class="hero-heading left">Projects</div>

                        <a class="" role="" data-toggle="collapse" href="#collapseExample" aria-expanded="false"
                           aria-controls="collapseExample">
                            <i class="right medium-icon icon-filter"></i>
                        </a>
                        <div class="clearfix"></div>
                        @if(session('role') != 1)
                            <div class="collapse" id="collapseExample">
                                <div>
                                    <div class="hero-heading light">Filter Projects</div>
                                    <div class="field-form col-sm-10">
                                        <form action="">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Geography</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Size</option>
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Industry</option>
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Type</option>
                                                </select>
                                            </div>

                                            <div class="light">Show Only</div>
                                            <div class="checkbox">
                                                <input id="filter_all" type="radio" name="filter" @click="fetchProjects()"/>
                                                <label for="filter_all">All</label>
                                            </div>

                                            <div class="checkbox">
                                                <input id="filter_available" type="radio" name="filter" @click="filter('/filter','new')"/>
                                                <label for="filter_available">Available for Investment</label>
                                            </div>

                                            <div class="checkbox">
                                                <input id="filter_my_investments" type="radio" name="filter" @click="filter('/filter','funded')"/>
                                                <label for="filter_my_investments">My Investments</label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="collapse" id="collapseExample">
                                <div>
                                    <div class="field-form col-sm-10">
                                        <form action="">
                                            <div class="light"></div>
                                            <div>
                                                <div class="checkbox" style = "display:inline">
                                                    <input id="filter_all" type="radio" name="filter" @click="fetchProjects()"/>
                                                    <label for="filter_all" style = "padding-top:5px;padding-left:5px;">All</label>
                                                </div>

                                                <div class="checkbox" style = "display: inline;margin-left: 10px;">
                                                    <input id="filter_available" type="radio" name="filter" @click="filter('/filter','draft')"/>
                                                    <label for="filter_available" style = "padding-top:5px;padding-left:5px;">Draft</label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-12 nopadding">
                    <project :list = "projects"></project>
                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchProjects(pagination.prev_page_url)"
                                :disabled="!pagination.prev_page_url">
                            Previous
                        </button>
                        <span>Page @{{pagination.current_page}} of @{{pagination.last_page}}</span>
                        <button class="btn btn-default" @click="fetchProjects(pagination.next_page_url)"
                                :disabled="!pagination.next_page_url">Next
                        </button>
                    </div>
                    <div class="clearfix"></div>
                    @if(session('role') == App\Enums\Role::DEVELOPER)
                        <div class="cta_holder">
                            <a href="{{route('project.create')}}" class="btn btn-success btn-block">LIST YOUR PROJECT</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div id="map"></div>
    </div>
    <template id="project-template">
        <div class="projects">
            <div v-show="loading">Loading</div>
            <div v-for = "project in list">
                <a v-bind:href="$root.getUrl(project.status,project.id,{{session('role')}})">
                    <div class="single-project funding-available">
                        <div class="project-info left">
                            {{--<div class="name">{{$row->farmer_name}}--}}
                            <div class="name">@{{ project.farmer_name }}
                                <i class="icon-favourite project-fav"></i>
                            </div>
                            <div class="description">
                                <span class="budget" v-if="project.cost">
                                    {{displayUnitFormat('amount')}} @{{ project.cost/1000 }} K
                                </span>
                                <span class = "budget" v-else>{{displayUnitFormat('amount')}} 0</span>
                                <span class="peu" v-if="project.solar_pv_size">
                                    PEU: @{{ project.solar_pv_size/1000 }}
                                </span>
                                <span class = "peu" v-else>PEU: 0</span>
                            </div>
                        </div>
                        <div class="project-status right">
                            <div class="status new right">
                                @{{project.status}}
                            </div>
                            <div class="clearfix"></div>
                            <div class="description"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </template>
@endsection

@section('js')
    {{--axios--}}

    <script src="{{asset('/offgrid-bazaar/js/axios.min.js')}}"></script>
    {{--Vue CDN--}}
    <script src="{{asset('/offgrid-bazaar/js/vue.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    <script>
        Vue.component('project',{
            template : '#project-template',
            props : ['list','title','routeName','href','message','loading']
        });
        new Vue({
            el : '#vue-app',
            data : {
                projects : null,
                routeName : [],
                href : 'a',
                pagination: {},
                message : 'No data Found',
                loading: true,
                filter_option : null,
                last_page_url:null

            },
            mounted(){
                this.fetchProjects(this.fetchLastStateUrl());
            },
            methods: {
                fetchLastStateUrl:function(){
                    if (sessionStorage.getItem('last_pagination')) {
                        if(sessionStorage.getItem('last_pagination')){
                            this.pagination = JSON.parse(sessionStorage.getItem('last_pagination'));
                        }
                    }
                    if (typeof(this.pagination) === "undefined" && this.pagination === null ) {
                        return null
                    }
                    return '/all?page='+this.pagination.current_page;
                },

                fetchProjects: function(page_url){
                    let vm = this;
                    vm.loading = true;

                    page_url = page_url || '/all';
                    if(page_url === '/all'){
                        axios.get(page_url).then(function (response){
                            vm.makePagination(response.data);
                            vm.projects = response.data.data;
                            vm.loading = false
                        });
                    }else{
                        if(vm.filter_option != null){
                            this.filter(page_url,vm.filter_option);
                        }else{
                            axios.get(page_url).then(function (response){
                                vm.makePagination(response.data);
                                vm.projects = response.data.data;
                                vm.loading = false
                            });
                        }
                    }


                },
                filter: function(page_url,filter_option){
                    let vm = this;
                    vm.loading = true;
                    page_url = page_url || '/filter';
                    vm.filter_option = filter_option;
                    if(vm.filter_option === 'draft'){
                        vm.$http.get(page_url,{status:filter_option}).then(response => {
                            this.loading = true;
                            vm.projects = response.data;
                            vm.loading = false
                        });
                    }else{
                        vm.$http.get(page_url,{status:filter_option}).then(response => {
                            this.loading = true;
                            vm.makePagination(response.data);
                            vm.projects = response.data.data;
                            vm.loading = false
                        });
                    }
                },
                makePagination: function(data){
                    let pagination = {
                        current_page: data.current_page,
                        last_page: data.last_page,
                        next_page_url: data.next_page_url,
                        prev_page_url: data.prev_page_url
                    };
                    this.pagination = pagination;
                    this.savePagination()
                },
                getUrl : function(status,id,role){
                    status = status || 'draft';

                    switch (status.toLowerCase()) {
                        case 'plan':
                            return '/project/' + id + '/select-plan';
                        case 'draft':
                            return '/draft/' + id;
                        case 'operational' || 'error' || 'overdue' :
                            return '/meterdata/' + id;
                        default:
                            return '#';
                    }
                },
                savePagination() {
                    const parsed = JSON.stringify(this.pagination);
                    sessionStorage.setItem('last_pagination', parsed);
                }
            }
        });
    </script>

    <script src="{{asset('/offgrid-bazaar/js/bootstrap.min.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDa0mYwWMgHdH-ClgyhUWe8P7qvP4ATkEM&callback=initMap"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-marker-clusterer/1.0.0/markerclusterer_compiled.js"></script>
    <script>
        var iconBase = 'images/';
        var icons = {
            default: {
                icon: iconBase + 'default_map_pin.svg'
            },
            my_project: {
                icon: iconBase + 'my_project_map_pin.svg'
            }
        };

        var PROJECT_STATUS = {
            NEW: 1,
            FUNDED: 2,
            NOT_FUNDED: 3
        }

        var locations = [{
            "marker_tip": {
                "project_name": "Rubiya Shahi",
                "project_status": 5,
                "project_info": "Rs.210K PEU: 0.6W"
            }, "latlng": ["28.685244", "80.621591"], "type": "default"
        }, {
            "marker_tip": {
                "project_name": "Asar Sam Bajayak",
                "project_status": 5,
                "project_info": "Rs.376K PEU: 0.6W"
            }, "latlng": ["28.9872803", "80.1651854"], "type": "default"
        }, {
            "marker_tip": {"project_name": "Navin Joshi", "project_status": 5, "project_info": "Rs.210K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Fattu Tharu", "project_status": 5, "project_info": "Rs.376K PEU: 0.6W"},
            "latlng": ["28.2227572", "81.3289397"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Krishna Mahato", "project_status": 5, "project_info": "Rs.210K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {
                "project_name": "Laxmi Devi Chaudhary",
                "project_status": 5,
                "project_info": "Rs.376K PEU: 0.6W"
            }, "latlng": ["28.685244", "80.621591"], "type": "default"
        }, {
            "marker_tip": {"project_name": "Shyam Ram Malla", "project_status": 5, "project_info": "Rs.210K PEU: 0.6W"},
            "latlng": ["28.154453", "82.3234843"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Ravi Nath Panta", "project_status": 5, "project_info": "Rs.376K PEU: 0.6W"},
            "latlng": ["28.5099229", "81.1085744"],
            "type": "default"
        }, {
            "marker_tip": {
                "project_name": "Tej Kumari Sunar",
                "project_status": 5,
                "project_info": "Rs.376K PEU: 0.6W"
            }, "latlng": ["28.5776106", "81.6254283"], "type": "default"
        }, {
            "marker_tip": {"project_name": "Kalika Khatri", "project_status": 5, "project_info": "Rs.210K PEU: 0.6W"},
            "latlng": ["28.2227572", "81.3289397"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Sunil Ghimire", "project_status": 1, "project_info": "Rs.376K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "tyfghf", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Ananta Thakuri", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["27.1961191", "85.899783"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Ananta", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.9872803", "80.1651854"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Ananta Thakuri", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Ananta Thakuri", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "aa", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "ananta", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.5099229", "81.1085744"],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Dasulal Chaudhari", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.9872803", "80.1651854"],
            "type": "default"
        }, {
            "marker_tip": {
                "project_name": "Man Bahadur Chaudhary",
                "project_status": 1,
                "project_info": "Rs.0K PEU: 0.6W"
            }, "latlng": ["28.685244", "80.621591"], "type": "default"
        }, {
            "marker_tip": {
                "project_name": "pabitri devi sudin",
                "project_status": 1,
                "project_info": "Rs.0K PEU: 0.6W"
            }, "latlng": ["28.5099229", "81.1085744"], "type": "default"
        }, {
            "marker_tip": {"project_name": "kgkhg", "project_status": 1, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": [null, null],
            "type": "default"
        }, {
            "marker_tip": {"project_name": "Sam bdr", "project_status": 2, "project_info": "Rs.0K PEU: 0.6W"},
            "latlng": ["28.685244", "80.621591"],
            "type": "default"
        }];

        var addMapContent = function (map, marker, infowindow, template) {
            google.maps.event.addListener(marker, 'click', (function (marker) {
                return function () {
                    infowindow.setContent(template());
                    infowindow.open(map, marker);
                }
            })(marker));
        }

        var addMarkers = function (map, locations, options) {
            var bounds = new google.maps.LatLngBounds();
            var infowindow = new google.maps.InfoWindow();

            var marker, i, markers = [];

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].latlng[0], locations[i].latlng[1]),
                    map: map,
                    icon: icons[locations[i].type].icon
                });

                markers.push(marker);

                bounds.extend(marker.position);

                addMapContent(map, marker, infowindow, (function (message) {
                    return function () {
                        status = '';
                        if (message.project_status === PROJECT_STATUS.NEW) {
                            status = '<span class="status new">New</span>';
                        } else if (message.project_status === PROJECT_STATUS.FUNDED) {
                            status = '<span class="status funded">Funded</span>';
                        } else {
                            status = '<span class="status funded">Not Funded</span>';
                        }

                        return '<h1>' + message.project_name + '</h1>' +
                            '<div>' + status + '</div>' +
                            '<p>' + message.project_info + '</p>';
                    }
                })(locations[i].marker_tip));
            }
            // map.fitBounds(bounds);

            return markers;
        }

        var enableCluster = function (map, markers, options) {
            return new MarkerClusterer(map, markers, options);
        }

        var initMap = function () {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: 28.388670, lng: 84.105200},
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                // disableDefaultUI: true,
                disableDoubleClickZoom: true
            });
            var markers = addMarkers(map, locations);

            var markerCluster = enableCluster(map, markers,
                {imagePath: 'images/map_cluster'});

            map.panBy(-200, 0);
        }
    </script>
@endsection
