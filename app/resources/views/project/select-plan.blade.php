@extends('layouts.default')
@section('title')
    Select-Plan
@endsection
@section('css')
    <style>
        .custom-space{
            margin-top: 25px;
        }
    </style>
    <style>
        h2{
            font-weight: 400;
        }
        input[type="file"]{
            opacity: 0;
            width: 100%;
            height: 200px;
            position: absolute;
            cursor: pointer;
        }
        .filezone {
            outline: 2px dashed grey;
            outline-offset: -10px;
            background: #ccc;
            color: dimgray;
            padding: 10px 10px;
            min-height: 200px;
            position: relative;
            cursor: pointer;
            margin: 10px 0 0 0;
            width: 95%;
        }
        .filezone:hover {
            background: #c0c0c0;
        }

        .filezone p {
            font-size: 1.2em;
            text-align: center;
            padding: 50px 50px 50px 50px;
        }
        div.file-listing img{
            max-width: 90%;
        }

        div.file-listing{
            margin: auto;
            padding: 10px;
            border-bottom: 1px solid #ddd;
            width: 95%;
        }

        div.file-listing img{
            height: 100px;
        }
        div.success-container{
            text-align: center;
            color: green;
        }

        div.remove-container{
            text-align: center;
            float:right;
        }

        div.remove-container a{
            color: red;
            cursor: pointer;
        }

        a.submit-button{
            display: block;
            margin: auto;
            text-align: center;
            width: 200px;
            padding: 10px;
            text-transform: uppercase;
            background-color: #CCC;
            color: white;
            font-weight: bold;
            margin-top: 20px;
        }


        @media screen and (min-width: 769px){
            .select-plan-div > div:first-child{
               margin-left: 140px;
            }
        }
        @media screen and (max-width: 768px){
            .pricingtable{
                padding: 20px;
            }
        }
    </style>
@endsection
@section('content')
    @include('common.session_message')
    <div class="container" id ="app">
        <div class="row">
            <div class="col-sm-6">
                <h2>Water Requirement Analysis</h2>
                <p class="msg">We analyzed water requirement for your crops throughout the year. Based on that, we suggest the following solar- powered water pumping solutions. Select the solution that best suits you.
                    ({{__('app.select_plan_description')}})
                    </br>
                    @if(App::environment('development'))
                    This project has a <b> demographic risk of {{isset($credit_score)? round($credit_score *100,2):0}}%</b>
                    @endif
                </p>
            </div>
            <div class="col-sm-6">
                <div id="yw0">
                    <div>
                        <div v-if="loaded" v-cloak>
                            <Charts :water-quantity="waterQuantity" :key="waterQuantity"></Charts>
                        </div>
                        <div v-else>
                            Loading
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="container">
            <hr>
            @if ($errors->has('project_id'))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Please choose one plan and comment if you need customise plan
                </div>
            @endif
            <div class="col-sm-12 text-center">
                <br/>
                <h2>Select your Water Pumping Solution</h2>
                <p class="msg">
                    We analyzed water requirement for your crops throughout the year.
                    Based on that, we suggest the {{isset($selected_plan)?$selected_plan->name:' to choose any'}} solar-powered water pumping solution with a daily water discharge of {{isset($selected_plan)? $selected_plan->daily_discharge:'certian'}} litres .
                    You can also change the selection below to the solution that best suits you.
                    {{__('app.selection_start')}}
                    <b>{{isset($selected_plan)?$selected_plan->name:' to choose any'}}</b>
                    {{__('app.selection_end')}}
                </p>

                <br/>
            </div>
            <form id="select-plan-form" action="{{route('project.selectPlanStore')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input class="form-control" name="project_id" id="Project_id" type="hidden" value="{{$project->id}}" />
                <div class="row select-plan-div">
                    @foreach($plans as $plan)
                        <div class="col-sm-4 col-md-3 form-group">
                            <input type="radio" name="plan_id" id="plan_{{$plan->id}}" value= "{{$plan->id}}" class="checkbox-hide form-control" {{isset($selected_plan)? $plan->id == $selected_plan->id ? 'checked' : '':''}} data-cost="{{$plan->cost}}">
                            <label for="plan_{{$plan->id}}" class="block">
                                <div class="well pricingtable">
                                    <div class="checked-icon-holder">
                                        <i class="icon-done"></i>
                                    </div>
                                    <p class="text-uppercase">{{$plan->code}}</p>
                                    <h1>{{$plan->name}}</h1>
                                    <ul class="pricing">
                                        <li><span>Daily Discharge</span><span class="text-right">{{$plan->daily_discharge}} L</span></li>
                                        <li><span>Solav PV Size</span><span class="text-right">{{$plan->solar_pv_size}} W</span></li>
                                        <li><span>Water Pump Size</span><span class="text-right">{{$plan->pump_size}} hp</span></li>
                                    </ul>
                                    <h2>Rs.{{$plan->plan_cost}}</h2>
                                </div>
                            </label>
                        </div>
                    @endforeach
                </div>
                <br/>
                    <div class="col-sm-12">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="5" id="comment" name="comment" ></textarea>
                    </div>
                <br/>
                <div class="row"> 

                @foreach($upload_attributes as $attribute_key =>$attribute)
                    <div class="col-md-12">
                        <div class="col-md-8" style="margin-top: 25px;">
                            <div class="card">
                                <div class="card-header" style="font-size: 14px;">Add {{$attribute}}</div>
                                <div class="card-body">
                                    <upload-files :file-name="'{{$attribute_key}}'" :post_url="'project/file/upload/'"></upload-files>
                                </div>
                            </div>
                        </div>
                        @if(session()->has('file_upload_error'))
                            <span class="error"  style="color:red;">{!! session()->get('file_upload_error') !!}</span>
                        @endif
                    </div>
                @endforeach
                </div>
                <div class= "col-sm-12 custom-space">
                        <button  class="btn btn-success" @click="submitForm()">Submit for Financing</button>
                        <a class="btn btn-default" href="{{route('project.update', $project->id) }}">Go back and Update Info</a>
                        <a class="btn btn-default pull-right" href="{{url('/project/list')}}">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('/offgrid-bazaar/js/vue.min.js')}}"></script>
    <script src="{{asset('/offgrid-bazaar/js/highcharts.js')}}"></script>
    <!-- vue-highcharts should be load after Highcharts -->
    <script src="{{asset('/offgrid-bazaar/js/vue-highcharts.min.js')}}"></script>
    <script src = "{{asset('/offgrid-bazaar/js/axios.min.js')}}"></script>
    <script>
        const Charts = Vue.component('Charts', {
            props: [ 'waterQuantity'],
            template: `
            <div>
                <div id="thechart"></div>
            </div>
            `,
            data(){
                return{
                    chart:'column'
                }
            },
            methods:{
                redraw(){
                    this.chart.series[0].setData(this.aggregateDischarge,true);
                },
            },
            watch:{
                dailyDischarge(){this.redraw()},
            },
            mounted() {
                var highchartsOptions = {
                    chart: {
                        type: 'column',
                        renderTo: 'thechart',
                        height: 200,
                    },
                    credits: {
                        enabled: false
                    },

                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"]
                    },
                    yAxis: {
                        title: {
                            text: 'Liters'
                        },
                        tooltip: {
                            enabled: true,
                            pointFormat: this.value + 'l'
                        },

                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name : 'Water Requirement',
                        data: this.waterQuantity

                    }]
                }
                this.chart = new Highcharts.chart(highchartsOptions)
            }
        });
        const UploadFiles ={
            template:`
            <div class="container">
                <div class="row">
                <div class="large-12 medium-12 small-12 filezone">
                    <input type="file" :name="fileName" id="files" ref="files" multiple v-on:change="handleFiles(fileName)"/>
                    <p>Drop your files here <br>or click to search</p>
                </div>


                <div v-for="(file, key) in files"   class="file-listing clearfix">

                     <div v-if ="file.id >0  && file.file_name!== undefined">
                        <div v-if ="file.file_type != 'png' && file.file_type != 'jpg' && file.file_type != 'jpeg' && file.file_type != 'gif'">
                            <img class="preview" v-bind:src="'/offgrid-bazaar/images/generic.png'"/>
                            @{{file.actual_file_name}}
                        </div>
                        <div v-else>
                            <img class="preview" v-bind:src="relativePath(file.file_name,file.file_location)"/>
                            @{{file.actual_file_name}}
                        </div>

                    </div>
                    <div v-else>
                        <img class="preview" v-bind:ref="'preview'+parseInt(key)"/>
                        @{{file.name }}
                    </div>
                    <div class="remove-container">
                        <a class="remove" v-on:click="removeFile(key)">Remove</a>
                    </div>

                </div>

            </div>`,

            props: ['input_name', 'post_url','fileName'],
            data: function() {
                return {
                    files: [],
                    filename: null,
                }
            },
            mounted(){
                vm = this;
                this.fetchFiles({{$project->id}}).then(data => {
                    Object.entries(data.uploads).map(files => {
                        if(this.fileName === files[1].upload_attribute_key){
                            this.files.push(files[1]);
                        }

                    })
                });

            },
            methods: {
                fetchFiles(projectId){
                    return axios.get(`/project/file/get/${projectId}`).then(response => response.data);
                },
                removeFile(key){
                    this.deleteFile(key);
                    this.files.splice(key, 1);
                    this.getImagePreviews();
                },

                deleteFile(key){
                    var uploadId =this.files[key].id;
                    if (typeof uploadId == 'undefined') {
                        alert("file couldnot be deleted due to unknown reasons");
                    }
                    axios.get('/project/file/delete/'+uploadId)
                        .then(function (response){
                            if(response.status!= 200){
                                alert("file couldnot be deleted due to unknown reasons");
                            };
                        });
                },
                handleFiles(fileName) {
                    let uploadedFiles = this.$refs.files.files;
                    for(var i = 0; i < uploadedFiles.length; i++) {
                        if (!(/\.(jpe?g|png|gif|bmp|doc*|xls*|pdf|text)$/i.test(uploadedFiles[i].name))) {
                            alert("please upload the files with extension jpg,jpeg,png,bmp,doc,docx,xls,pdf,txt");
                            continue;
                        }
                        this.files.push(uploadedFiles[i]);
                    }
                    this.getImagePreviews();
                    this.submitFiles(fileName)
                },
                getImagePreviews(){
                    for( let i = 0; i < this.files.length; i++ ){
                        if (/\.(jpe?g|png|gif)$/i.test(this.files[i].name)) {
                            let reader = new FileReader();
                            reader.addEventListener("load", function(){
                                this.$refs['preview'+parseInt(i)][0].src = reader.result;
                            }.bind(this), false);
                            reader.readAsDataURL( this.files[i]);
                        }else{
                            this.$nextTick(function(){
                                this.$refs['preview'+parseInt(i)][0].src ="{{asset('/offgrid-bazaar/images/generic.png')}}";
                            });
                        }
                    }
                },
                submitFiles(fileName) {
                    for( let i = 0; i < this.files.length; i++ ){
                        if(this.files[i].id) {
                            continue;
                        }
                        let formData = new FormData();
                        formData.append(fileName, this.files[i]);
                        axios.post('/' + this.post_url + '{{$project->id}}',
                            formData,
                            {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }
                        ).then(function(data) {
                            this.files[i].id = data['data']['id'];
                            this.files.splice(i, 1, this.files[i]);
                        }.bind(this)).catch(function(data) {
                            console.log("error");
                        });
                    }
                },
                relativePath: function(name,location){
                    let app_url = window.location.hostname;
                    let port = window.location.port;
                    let return_url = '';
                    let splitted_location = location.split("/")
                    let splitted_length = splitted_location.length;
                    port === '' ? return_url = `http://${app_url}/storage/${splitted_location[splitted_length-2]}/${name}`: return_url = `http://${app_url}:${port}/storage/${splitted_location[splitted_length-2]}/${name}`;
                    return return_url;
                }

            }
        };
        new Vue({
            el: '#app',
            data: {
                water : null,
                loaded: false
            },
            mounted(){
                this.loadData().then(()=>{
                    this.loaded = true;
                });
            },
            computed:{
                waterQuantity(){


                    let waterQuantity = [];
                    let arr  = this.water;
                    let count = 0;
                    for(var i in arr)
                    {
                        waterQuantity.push(Math.round(arr[i]));
                    }
                    return waterQuantity

                },
            },
            methods : {
                loadData(){
                    let vm = this;
                    return axios.get('/project/api/fetch/{{$project->id}}')
                        .then(function (response) {
                            vm.water = response.data
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
            },
            components: { Charts,UploadFiles }
        })
    </script>
    {{--<script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/71993dc7/highcharts.src.js')}}"></script>--}}
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/71993dc7/modules/drilldown.src.js')}}/"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/71993dc7/modules/exporting.src.js')}}"></script>
    {{--<script type="text/javascript">--}}
        {{--jQuery(window).on('load',function() {--}}
            {{--Highcharts.setOptions([]); var chart = new Highcharts.Chart({'chart':{'renderTo':'yw0','type':'column'},'credits':{'enabled':false},'title':{'text':'Water\x20Requirement\x20Analysis'},'xAxis':{'crosshair':true,'categories':['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']},'yAxis':{'min':0,'title':{'text':'Water\x20Discharge\x20Ltrs\x2Fday'}},'tooltip':{'headerFormat':'\x3Cspan\x20style\x3D\x22font\x2Dsize\x3A10px\x22\x3E\x7Bpoint.key\x7D\x3C\x2Fspan\x3E\x3Ctable\x3E','pointFormat':'\x3Ctr\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22color\x3A\x7Bseries.color\x7D\x3Bpadding\x3A0\x22\x3E\x7Bseries.name\x7D\x3A\x20\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22padding\x3A0\x22\x3E\x3Cb\x3E\x7Bpoint.y\x3A.1f\x7D\x20ltrs\x2Fday\x3C\x2Fb\x3E\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x3C\x2Ftr\x3E','footerFormat':'\x3C\x2Ftable\x3E','shared':true,'useHTML':true},'plotOptions':{'column':{'pointPadding':0.2,'borderWidth':0}},'series':[{'name':'Water\x20Discharge','data':[0,0,0,0,0,0,0,0,0,0,0,0],'type':'column'},{'name':'Sulav','data':[240000,240000,240000,240000,240000,240000,240000,240000,240000,240000,240000,240000],'type':'line'},{'name':'Sahaj','data':[210000,210000,210000,210000,210000,210000,210000,210000,210000,210000,210000,210000],'type':'line'}]});--}}
        {{--});--}}
    {{--</script>--}}
@endsection