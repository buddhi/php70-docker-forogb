@extends('layouts.new-default')
@section('title') Invest in Offgrid Solar Projects @endsection
@section('metadata')
<meta property="og:title" content="Off Grid Bazar"/>
<meta property="og:url" content="offgridbazar.com" />
<meta property="og:description" content="This year, I am supporting awesome #impact projects by @GhamPowerproviding #solar to rural farmers in #Nepal! Join me to help more farmers. https://offgridbazaar.com #MakeaDifference #2019 #OffGridBaazar" />
@endsection
@section('content')
@include('common.hero-banner')
<project-list></project-list>
<share-news v-bind:paid="false" v-bind:showcontinue="false"></share-news>
@endsection
