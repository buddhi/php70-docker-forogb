@extends('layouts.new-default')
@section('content')
<section class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2>Awaiting account verification from admin</h2>
        </div>

        <div class="col-12">
            <div class="card text-center">
                <div class="card-content p-5">
                    <i class="icon ion-md-person text-success display-1"></i>
                    <p class="h3">Just one more step...</p>
                    <p>
                        Your account has been created  but there is one more step before you're ready to go.<br>
                        We will activate your account and send you an email. This usually takes less than 24 hours.</p>
                    <a href="{{route('index')}}"> <button class="btn btn-outline-secondary btn-lg">Okay</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection