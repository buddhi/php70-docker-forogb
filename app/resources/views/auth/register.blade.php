
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/screen.css')}}"
          media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/print.css')}}" media="print">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/ie.css')}}" media="screen, projection">
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/checkbox.css')}}">
    {{--<script src='https://www.google.com/recaptcha/api.js'></script>--}}
    <title>Register</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="form-container form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <a href="{{url('/')}}">
                                <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="form-logo">
                            </a>
                        </div>
                        <h1 class="page-title text-center">Sign Up As Developer</h1>
                    </div>
                    <div class="form">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <input type="text" name="user-form" value="user-form" hidden>
                            <input type="hidden" name="role[]" value="1">

                            <div class="col-md-12">
                                <label class="inline-label required" for="full_name">Full Name
                                    <span class="required">*</span></label>
                                <input class="form-control" placeholder="Full Name" name="full_name" id="User_full_name" type="text" maxlength="255" />
                                {{--<label id="full_name" class="error" for="full_name">This field is required.</label>--}}

                                @if ($errors->has('full_name'))
                                    <span class="error">
                                        <p>{{ $errors->first('full_name') }}</p>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <label class="inline-label" for="User_address">Address</label>
                                <input class="form-control" name="address" placeholder="Full Address" name="address" id="User_address" type="text" maxlength="255" />


                            </div>

                            <div class="col-md-6">
                                <label for="User_phone" class="required">Phone <span class="required">*</span></label>
                                <input class="form-control" placeholder="Phone" name="phone" id="User_phone"  type="text" maxlength="100" />
                                @if ($errors->has('phone'))
                                    <span class="error">
                                        <p>{{ $errors->first('phone') }}</p>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <label for="User_email" class="required">Email Address <span class="required">*</span></label>
                                <input class="form-control" placeholder="example@mail.com"  name="email" id="User_email" type="email" value="{{ old('email') }}" maxlength="255" />
                                @if ($errors->has('email'))
                                    <span class="error">
                                        <p>{{ $errors->first('email') }}</p>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label class="inline-label required" for="User_password" >Password <span class="required">*</span></label>
                                <input class="form-control" required="required"  type="password"  name="password" id="User_password"  maxlength="255" />
                                @if ($errors->has('password'))
                                    <span class="error">
                                        <p>{{ $errors->first('password') }}</p>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <label class="inline-label required" for="User_confirm_password">Confirm Password
                                    <span class="required">*</span></label>
                                <input class="form-control"  name="password_confirmation" id="password-confirm" type="password" />

                            </div>

                            <div class="col-md-12">
                                <label for="User_i_agree"><input id="ytUser_i_agree" type="hidden" value="0" name="User[i_agree]" />
                                    <input  name="User[i_agree]" id="User_i_agree" value="1" type="checkbox" required /> By clicking “Register” I agree to Terms and Conditions.</label>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block" value="submit-user">Register</button>
                            </div>
                        </form></div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{asset('offgrid-bazaar/js/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>


<script type="text/javascript">
    function toggleInvestmentType(){
        var val = $('input.user_role:checked').val();
        if(val == 1){
            $('div.investment-type').hide();
        }
        else{
            $('div.investment-type').show();
        }
    }

    $(document).ready(function(){
        //toggleInvestmentType();
        $('input.user_role').on('change',function(e){
            var val = $('input.user_role:checked').val();
            if(val == 1){
                $('div.investment-type').hide();
            }
            else{
                $('div.investment-type').show();
            }
        });

        $("#register-form").validate({
            rules: {
                "full_name": {
                    required: true,
                },
                "User[email]": {
                    required: true,
                    email: true,
                },
                "User[password]": {
                    required: true,
                },
                "User[confirm_password]": {
                    required: true,
                    equalTo : '#User_password'
                },
            },
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            submitHandler: function (form) {
                form.submit();
            },
            messages: {
                "User[confirm_password]": {
                    equalTo: "Passwords do not match."
                }
            },
        });

        // $.validator.addMethod("email", function (value, element) {
        //     return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.[a-zA-Z]{2,3})+$/.test(value);
        // }, "Please provide a valid email address.");


    });
</script>
</html>