@extends('layouts.new-default')
@section('content')
    <section class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2>Awaiting your Email verification</h2>
            </div>
            <div class="col-12">
                <div class="card text-center">
                    <div class="card-content p-5">
                        <i class="icon icon ion-md-paper-plane text-danger  display-1"></i>
                        <p class="h3">Just one more step...</p>
                        <p>
                            Your account has been created  but there is one more step before you're ready to go.<br>
                           We have sent you an email. Please check your inbox for email verification.</p>
                        <a href="{{route('index')}}"> <button class="btn btn-outline-secondary btn-lg">Okay</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection