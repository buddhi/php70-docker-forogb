@extends('layouts.new-default')
@section('content')
    <section class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2>Connected to {{ucFirst($provider)}} </h2>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-content p-5 row justify-content-center">
                        <div class="col-md-8 text-center">
                            <img src="{{$userSocial->avatar_original}}" class="rounded-circle mb-4" height="100"
                                 width="100">
                            <span class="display-4 ml-2"> + <i
                                        class="icon ion-logo-{{$provider}} text-{{$provider}}"></i></span>
                            <p class="h3">Welcome, {{isset($userSocial->name)? $userSocial->name:null}}!</p>
                            <p>Here's what we found about you on {{ucFirst($provider)}}. You can continue to sign up if
                                all the information you see below are correct. You can also enter additional details for
                                future uses.</p>

                        </div>
                        <form method="post" action="{{route('social.register')}}" class="col-md-6 mt-4">
                            {{csrf_field()}}
                            <input type="hidden" name="provider" value="{{$provider}}">
                            <input type="hidden" name="image_url" value="{{$userSocial->avatar_original}}">
                            <input type="hidden" name="provider_id" value="{{$userSocial->id}}">
                            <input type="hidden" name="full_name"
                                   value="{{isset($userSocial->name)? $userSocial->name:"unknown"}}">

                            <div class="row">
                                <div class="col-md-4 input-group" style="padding-right: 0px;">
                                    <label for="country_code">Country code:</label>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">+</span>
                                    </div>
                                    <input placeholder="" name="country_code" type="number" value="977" id="country_code" class="form-control">
                                </div>
                                <div class="col-md-8">
                                    <label for="phone" class="inline-label required">Phone number :</label>
                                    <input placeholder="Enter your phone number" name="phone" type="text" id="phone" class="form-control">
                                </div>
                            </div>

                            <div class="form-group mt-3">
                                <label>
                                    <span>Address</span>
                                    <input type="text" name="address" class="form-control"
                                           placeholder="Please, enter your address">
                                </label>
                            </div>
                            <button type="submit" class="btn btn-warning btn-block btn-lg">This looks good, lets
                                proceed
                            </button>
                            <div class="form-group mt-4">
                                <small class="text-muted">We will use your name and profile photo for marketing the
                                    projects you have invested in. You can change this anytime at Account Settings
                                </small>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
