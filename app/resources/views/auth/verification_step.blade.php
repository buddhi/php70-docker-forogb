@extends('layouts.new-default')
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style>
        .verification-card .card{
            min-height: 300px;
        }
    </style>
@endsection
@section('content')
    <section class="container mt-5 verification-card">
        <div class="row justify-content-center">
            <div class="col-12 mb-4">
                <h2 class="text-center">Awaiting account verification</h2>
                <p class="text-center">
                    Your account has been created but there are two more steps before you're ready to go.
                    Please complete the following.
                </p>
            </div>

            <div class="col-sm-6">
                <div class="card text-center mb-10">
                    <div class="card-content p-5">
                        <i class="icon ion-md-paper-plane text-{{$email_verification?'success':'danger'}} display-3"></i>
                        <p class="h4 mt-2">
                            @if($email_verification)
                            <strong><i class="icon ion-md-checkmark-circle text-success"></i>
                            </strong> E-mail verification completed</p>
                            @else
                            <strong><i class="icon ion-md-close-circle  text-danger"></i>
                            </strong> E-mail verification pending </p>
                            @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-center mb-10">
                    <div class="card-content p-5">
                        @if($admin_verification)
                            <i class="icon ion-md-person text-success display-3"></i>
                            <p class="h4 mt-2"><strong><i class="icon ion-md-checkmark-circle text-success"></i></strong> Admin Verification completed</p>
                        @else
                            <i class="icon ion-md-person text-danger display-3"></i>
                            <p class="h4 mt-2"><strong><i class="icon ion-md-close-circle text-danger"></i></strong> Admin Verification pending</p>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".switch-signup").on("click", function(e){
                e.preventDefault();
                var item = $(this).attr("rel");
                $(".signup-section").addClass("collapse");
                $("#"+item).removeClass("collapse");
            });
        });
    </script>
@endsection