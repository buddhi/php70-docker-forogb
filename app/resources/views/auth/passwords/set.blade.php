@extends('layouts.new-default')
@section('flash-message')
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {!! session()->get('success') !!}
        </div>
    @endif
@endsection
@section('content')
    @include('common.session_message')
    <div class="container">
        <div class="form-container">
            <div class="col-md-6 profile-form">
                <form class="form-horizontal" method="POST" action="{{route('password.set') }}">
                    {{ csrf_field() }}
                    <div class="col-md-12 text-center">
                        <h4>Set Password</h4>
                        <p style="text-align: left;font-size: 15px">Please, set a password so that you can login via email {{$user->email}} if you ever decide to deactivate the social media account</p>
                    </div>
                    <div class="col-md-12 mt-4 form-group">
                        <label for="current-password">Password:</label>
                        <input class="form-control" type="password" name="password" data-validetta="required">
                        @if ($errors->has('password'))
                            <span class="error">
                                <p>{{ $errors->first('password') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="new-password">Confirm Password:</label>
                        <input class="form-control" type="password" name="password_confirmation" data-validetta="required">
                        @if ($errors->has('password_confirmation'))
                            <span class="error">
                                <p>{{ $errors->first('password_confirmation') }}</p>
                            </span>
                        @endif
                    </div>
                    <div>
                        <div class="col-md-12 mt-4">
                            <button type="submit" class="btn btn-primary">
                                Set Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
