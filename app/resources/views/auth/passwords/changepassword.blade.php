@extends('layouts.new-default')
@section('content')
    @include('common.session_message')
    <div class="container">
        <div class="form-container">
            <div class="col-md-4 profile-form">
                <form class="form-horizontal" method="POST" action="{{route('changePassword') }}">
                    {{ csrf_field() }}
                    <div class="col-md-12 text-center">
                        <h4>Change Password</h4>
                    </div>
                    <div class="col-md-12 mt-4 form-group">
                        <label for="current-password"> Current Password</label>
                        <input class="form-control" type="password" name="current-password" data-validetta="required">
                        @if ($errors->has('current-password'))
                            <span class="error">
                                <p>{{ $errors->first('current-password') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="new-password"> New Password</label>
                        <input class="form-control" type="password" name="new-password" data-validetta="required">
                        @if ($errors->has('new-password'))
                            <span class="error">
                                <p>{{ $errors->first('new-password') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="new-password">Confirm new Password</label>
                        <input class="form-control" type="password" name="new-password_confirmation" data-validetta="required">
                        @if ($errors->has('new-password_confirmation'))
                            <span class="error">
                                <p>{{ $errors->first('new-password_confirmation') }}</p>
                            </span>
                        @endif
                    </div>
                    <div>
                        <div class="col-md-12 mt-4">
                            <button type="submit" class="btn btn-primary">
                                Change Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
