<!-- layout.nunjucks -->
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="ui/"/>
    <meta charset="UTF-8">
    <title>Log In</title>
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">

</head>
<body>
<div class="text-center">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert"></button>
            <strong>{!! session()->get('success_message') !!}</strong>
            <br></div>
    @endif
    @if(session()->has('error_message'))
        <div class="alert alert-danger">
            <strong>{!! session()->get('error_message') !!}</strong>
        </div>
    @endif
</div>

<div class="container">
    <div class="row">
        <div class="form-container form">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="form-logo">
                    </div>
                    <h1 class="page-title">Welcome</h1>
                </div>
                <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="col-md-12 form-group">
                        <label for="email">Email Address</label>
                        <input class="form-control" type="text" name="email" value="{{ old('email') }}" data-validetta="required,email">
                        @if ($errors->has('email'))
                            <span class="error">
                            <p>{{ $errors->first('email') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" data-validetta="required">
                        @if ($errors->has('password'))
                            <span class="error">
                                <p>{{ $errors->first('password') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="password-confirm">Password Confirmation</label>
                        <input class="form-control" type="password" name="password_confirmation" data-validetta="required">
                        @if ($errors->has('password_confirmation'))
                            <span class="error">
                                <p>{{ $errors->first('password_confirmation') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="col-md-12 form-group">
                        <button class="btn btn-info btn-block" type="submit">Reset Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{asset('offgrid-bazaar/js/bootstrap.min.js')}}"></script>
</body>
</html>