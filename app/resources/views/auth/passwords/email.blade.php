<!DOCTYPE html>
<html lang="en">
<head>
    <base href="ui/"/>
    <meta charset="UTF-8">
    <title>Log In</title>
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">

</head>
<body>
@if (session('status'))
    <div class="text-center">
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    </div>
@endif

<div class="container">
    <div class="row">
        <div class="form-container form">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <a href="{{route('index')}}"><img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="form-logo"></a>

                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="col-md-12 form-group">
                        <label for="email" class="text-center">Email Address</label>
                        <input class="form-control" type="text" name="email" value="{{ old('email') }}" data-validetta="required,email">
                        @if ($errors->has('email'))
                            <span class="error">
                            <p>{{ $errors->first('email') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <button class="btn btn-info btn-block" type="submit">Send Password Reset link</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{asset('offgrid-bazaar/js/bootstrap.min.js')}}"></script>
</body>
</html>
