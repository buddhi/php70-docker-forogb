@extends('layouts.new-default')
@section('metadata')
<meta property="og:title" content="Off Grid Bazar"/>
<meta property="og:url" content="offgridbazar.com" />
<meta property="og:description" content="This year, I am supporting awesome #impact projects by @GhamPowerproviding #solar to rural farmers in #Nepal! Join me to help more farmers. https://offgridbazaar.com #MakeaDifference #2019 #OffGridBaazar" />
@endsection
@section('title') Invest in Offgrid Solar Projects @endsection
@section('flash-message')
	@if (session()->has('success'))
		<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
			<strong>{!! session()->get('success') !!} !!</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	@if (session()->has('error'))
		<div class="alert alert-danger alert-dismissible text-center" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>{!! session()->get('error') !!} !!</strong>
		</div>
	@endif
@endsection
@section('content')
<!-- <div class="container">
  <div class="col-xs-5 col-sm-6 col-md-6 col-lg-3 hero-section nopadding">
      <img src="{{asset('/offgrid-bazaar/images/solar.jpg')}}" alt="offgridsolar" class="col-sm-12 nopadding">
      <div class="col-sm-12 hero-info">
          <div class="hero-title-text">Fund in Off-Grid Solar</div>
          <p class="hero-text">
              Register now to research, invest and bring power to the people.
          </p>
      </div>
  </div>
  <google-map></google-map>
  </div> -->
<!-- </div> -->
<section class="home-main">
	<div class="home-main-wrapper">
			<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-11 text-center">
					<h1>Fund solar irrigation projects in rural Nepal</h1>
					<p class="lead dim fund-subtitle">2 million farmers in Nepal suffer from poor yields as they solely rely on rain for irrigation. Solar-powered water pumps can help immensely, but are difficult to finance. By mixing your grant funds with debt financing, we enable farmers to easily access solar irrigation solutions and increase their income.</p>
					<p class="bold">Off Grid Bazaar lists pre-qualified solar irrigation projects that you can directly fund online and make a huge impact.</p>
					<button class="btn btn-warning btn-lg"  data-toggle="modal" data-target="#login">View the projects</button>
				</div>
      </div>
		</div>
	</div>
	<img src="img/top_panel_bg_alt.png" class="img-fluid" alt="backdrop" />
</section>

@include('common.hero-banner')
<div class="container hero-summary">
	<div class="row justify-content-center align-items-center">
		<div class="col-12 text-center mb-4">
			<h2 class="fund-title">Why is this grant needed?</h2>
			<p class="lead fund-subtitle">A solar-powered water pump typically costs $2,000, which is too expensive for a smallholder farmer to buy in one shot. This is much easier to afford if a farmer can pay for it over time, especially as irrigation also helps increase their income over time.</p>
		</div>

		<div class="col-md-6 col-lg-5 text-md-right">
			<p class="h1 text-muted">However, </p>
			<p class="h4 text-muted mb-3">local banks require significant collateral for lending, and they also can’t quantify risk very well, all of which makes financing very difficult, including for other commercial investors.</p>
			<p class="lead">With your grant funds, we can fund these projects and get them operational right away. We then use the analytics from the Off Grid Bazaar platform (based on data collected from the farmers) to help banks and other investors better understand and quantify the project risks and opportunities.That way, they have more confidence to fund future projects, thus enabling more farmers to access irrigation and help their communities thrive.</p>
		</div>	

		<div class="col-md-6 col-lg-5 mb-4 mb-md-0">
			<div class="card text-center h-100">
				<img src="img/install_firstbatch.jpg" alt="" class="img-fluid">
				<div class="card-body p-md-5">
					<p class="h1 text-primary"><i class="icon ion-md-rocket"></i></p>
					<h4>Install first batch of 100 projects</h4>
					<p>We aim to install the first 100 solar irrigation projects with your help, and generate success stories based on real operational metrics and repayment data. This will create confidence, reduce perceived risk, and catalyze further private investment for future projects.
				</div>
			</div>
		</div>
	</div>
</div>

<section class="bg-primary text-white">
  <div class="container-full">
    <div class="row no-gutters">
      <div class="col-lg-6">
        <div class="w-100 h-100 side-img" style="background-image: url('img/matters_family.jpg')"></div>
      </div>
      <div class="col-lg-6 py-md-5 justify-content-center d-flex">
        <div class="box p-5 col-md-10">
          <h2 class="fund-title">Why it matters?</h2>
          <p class="lead dim mb-4 fund-subtitle">Studies suggest that irrigation increases the net income of smallholder farmers by 30 to 100%. Yet, over 70 % of agricultural land in Nepal is unirrigated. Solar water pumps can help but have not scaled because:</p>
          <div class="media mb-4">
            <i class="icon ion-md-alert mr-4 h3 dim"></i>
            <p class="subtitle-text">majority of rural farmers can’t afford the upfront cost in cash, nor can provide collateral required by financiers</p>
          </div>
          <div class="media">
            <i class="icon ion-md-alert mr-4 h3 dim"></i>
            <p class="subtitle-text">cost of financing is prohibitively high which makes the investment unattractive to both farmers and equity investors</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="hero-segment train py-5">
<div class="container-fluid my-lg-5">
<div class="row justify-content-center">
	<div class="col-12 mb-5">
		<h3 class="h2">How funding works</h3>
		<p class="lead">Off Grid Bazaar helps you research and fund solar irrigation projects that make a huge impact for rural Nepali farmers.</p>
	</div>

	<div class="col-md-2 train-segment">
		<p class="h1 text-primary"><i class="icon ion-md-mail"></i></p>
		<span class="round">1</span>
		<h4 class="text-primary">Register</h4>
		<p>Create a login using your email address or a social media account.</p>
	</div>

	<div class="col-md-2 train-segment">
		<p class="h1 text-primary"><i class="icon ion-md-filing"></i></p>
		<span class="round">2</span>
		<h4 class="text-primary">Select</h4>
		<p>Browse our project listings, research project details and impact metrics, and select projects you’d like to fund.</p>
	</div>


	<div class="col-md-2 train-segment">
		<p class="h1 text-primary"><i class="icon ion-md-cash"></i></p>
		<span class="round">3</span>
		<h4 class="text-primary">Fund</h4>
		<p>Enter amount you want to contribute as grant funds. Pay with PayPal or with your Credit/Debit Cards.</p>
	</div>

	<div class="col-md-2 train-segment">
		<p class="h1 text-primary"><i class="icon ion-md-color-wand"></i></p>
		<span class="round">4</span>
		<h4 class="text-primary">Transform</h4>
		<p>100% of your contribution will go to the farmer. You can track the progress of the project in "My Investments" and we will also send you regular email updates.</p>
	</div>

	<div class="train-line d-none d-md-block"></div>

</div>
</div>
</section>
<section class="bg-warning">
  <div class="container-full">
    <div class="row">
      <div class="col-lg-6 order-md-2">
        <div class="w-100 h-100 side-img" style="background-image: url('img/matter_microgrid.jpg')"></div>
      </div>
      <div class="col-lg-6 py-md-5 justify-content-center d-flex order-md-1">
        <div class="box p-5 col-md-10">
          <h2 class="fund-title">Who are we? </h2>
          <p class="lead fund-subtitle">Off Grid Bazaar is developed by Gham Power Nepal Pvt. Ltd., a solar project developer based in Kathmandu, Nepal. </p>
          <p class="subtitle-text">Established in 2010, we have deployed over 2,000 solar projects with a cumulative installed capacity of over 2.5 MW. We are the first company in Nepal to introduce pay-as-you-go services for rural energy customers like smallholder farmers.</p>
          <a href="http://ghampower.com/about/" class="btn btn-primary" target="_blank">Learn more</a>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container py-5">
  <div class="row">
    <div class="col-12 text-center pb-0">
      <div class="text-uppercase text-muted bold">Some of the projects being funded right now</div>
    </div>
  </div>
</div>
<latest-invested-project> </latest-invested-project>
<share-news v-bind:paid="false" v-bind:showcontinue="false"></share-news>
@endsection


