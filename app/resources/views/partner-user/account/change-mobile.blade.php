<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Change Number</title>
</head>

<body>

<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>

<div class="container-fluid">

    <div class="row">
        <main class="col-12 col-md-12 col-xl-12 bd-content">

            <div class="box-wrap verify-mobile-number">
                <h3 class="text-center mb-4">Change Mobile Number</h3>
                <form action="{{route('partner-user.change-number.store',$user->user_id)}}" method = "post">
                    {{csrf_field()}}
                    <div class="col">
                        <label for="mobile"> Your new number</label>
                        <div class="form-group">
                            <input type="text" class="form-control text-left"  name="phone"   required>
                        </div>
                        @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        @endif
                        @if (session()->has('error'))
                            <div class="alert alert-danger" role="alert" class="flash-message">
                                <span class="text-danger"> {!! session()->get('error') !!}</span>
                            </div>
                        @endif

                    </div>
                    <div class="col">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Send OTP</button>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>

<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
</body>

</html>


