@extends('layouts.default')
@section('metadata') <META HTTP-EQUIV="refresh" CONTENT="240"> @endsection
@section('css')
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .card-container {
            padding: 2px 16px;
        }
    </style>
@endsection
@section('title')FTP DATA VISUALIZE @endsection
@section('content')
    <div class="container-fluid">
        <template id="totals">
            <div class="row">
                <div class="col-md-6">
                    <h1 style = "text-align: center">Total Daily Flow</h1>
                    <hr>
                    <h4 style = "text-align: center">@{{ flow }} Liters</h4>
                </div>
                <div class="col-md-6">
                    <h1 style = "text-align: center">Total Daily Runtime</h1>
                    <hr>
                    <h4 style = "text-align: center">@{{ runtime }} Minutes</h4>
                </div>
            </div>
        </template>
        <div id="app">
            <div v-if="a">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="card">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img style = "margin-top:35px;" src="{{asset('/offgrid-bazaar/images/cover_farmer_meter.jpg')}}" alt="Farmer Cover image" class = "img-responsive">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-container">
                                            <h4><b>Gham Power Office</b></h4>
                                            <p>Location (District, VDC, ward): Kathmandu, Chundevi</p>
                                            <p>GPS location: 27.736132 N,85.3351772 E</p>
                                            <p>Panel size: 1260 Watts</p>
                                            <p>Pump size: 1 hp</p>
                                            <p>Desired flow (m3): 70,000</p>
                                            <p>Water head (m): 10m</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-container">
                                <div class="row" style = "margin-top: 5px">
                                    <div class="col-md-2 pull-right"  style = " margin-left:18px">
                                        <form action="{{route('meter.change_status')}}" method = "post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="phone" value = "9803900166">
                                            <input type="hidden" name = "value" value = "4951111">
                                            <input type="hidden" name = "meter_id" value = "{{$meter_id}}">
                                            <input type="submit" class = "btn btn-primary" value = "ON" onclick="return confirm('Are you sure?');">
                                        </form>
                                    </div>
                                    <div class="col-md-1 pull-right">
                                        <form action="{{route('meter.change_status')}}" method = "post">
                                            {{csrf_field()}}
                                            <input type="hidden" name = "value" value = "4951000">
                                            <input type="hidden" name = "meter_id" value = "{{$meter_id}}">
                                            <input type="submit" class = "btn btn-warning" value = "OFF" onclick="return confirm('Are you sure?');">
                                        </form>
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <form action="generate" method = "POST" id = "export">
                                            {{csrf_field()}}
                                            <input type="hidden" name = "end" :value = "a[0].created_at">
                                            <input type="hidden" name = "start" :value = "a[a.length-1].created_at">
                                            <input type="hidden" name = "dailyflow" :value = "updateTotalFlow">
                                            <input type="hidden" name = "dailyruntime" :value = "updateTotalRunTime">
                                            <input type="submit" value = "Export" class="btn btn-primary">
                                        </form>
                                    </div>
                                </div>
                                <hr>
                                <totals :runtime = "updateTotalRunTime" :flow="updateTotalFlow"></totals>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <Chart  :a = "a" :title="titleValue" :age-pot-value="agePotValue" :voltage="voltage" :dcpower="dcpower" :acpower = "acpower" :discharge="discharge" :rpm="rpm" :frequency="frequency"></Chart>
                <div class="clearfix"></div>
                <hr>
                <div>
                    <h2 style = "text-align: center">DATA</h2>
                </div>
                <p style = "text-align: center">This data is fetched on {{\Carbon\Carbon::now()}}</p>
                <div class="clearfix"></div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Time</th>
                        <th>Current</th>
                        <th>Voltage</th>
                        <th>DC power</th>
                        <th>AC Power</th>
                        <th>Discharge(I/s)</th>
                        <th>Motor RPM</th>
                        <th>Frequency</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="meter_data in a">
                        <td>@{{ meter_data.timestamp }}</td>
                        <td>@{{ meter_data.current }}</td>
                        <td>@{{ meter_data.voltage }}</td>
                        <td>@{{ meter_data.dc_power }}</td>
                        <td>@{{ meter_data.ac_power }}</td>
                        <td>@{{ meter_data.discharge }}</td>
                        <td>@{{ meter_data.rpm }}</td>
                        <td>@{{ meter_data.frequency }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div v-else>
                <h1 style = "text-align:center">Sorry no data today</h1>
            </div>
        </div>

    </div>
    <script src="{{asset('/offgrid-bazaar/js/vue.min.js')}}"></script>
    <script src="{{asset('/offgrid-bazaar/js/highcharts.js')}}"></script>
    <!-- vue-highcharts should be load after Highcharts -->
    <script src="{{asset('/offgrid-bazaar/js/vue-highcharts.min.js')}}"></script>
    <script src = "{{asset('/offgrid-bazaar/js/axios.min.js')}}"></script>
    <script>
        Vue.component('totals',{
            template : '#totals',
            props : ['runtime','flow']
        });
        const Chart = Vue.component('Chart', {
            props: [ 'title', 'agePotValue','voltage','dcpower','acpower','discharge','rpm','frequency','a','id'],
            template: `
	<div>
		<div id="thechart"></div>
	</div>
	`,
            data(){
                return{
                    chart:'line'
                }
            },
            methods:{
                redraw(){
                    this.chart.series[0].setData(this.agePotValue,true);
                    this.chart.series[1].setData(this.voltage,true);
                    this.chart.series[2].setData(this.dcpower,true);
                    this.chart.series[3].setData(this.acpower,true);
                    this.chart.series[4].setData(this.discharge,true);
                    this.chart.series[5].setData(this.rpm,true);
                    this.chart.series[6].setData(this.frequency,true);
                },
            },
            watch:{
                title(){this.redraw()},
                agePotValue(){this.redraw()},
                voltage(){this.redraw()},
                dcpower(){this.redraw()},
                acpower(){this.redraw()},
                discharge(){this.redraw()},
                rpm(){this.redraw()},
                frequency(){this.redraw()},
            },
            mounted() {
                var date = new Date();
                var highchartsOptions = {
                    chart: {
                        type: 'line',
                        renderTo: 'thechart',
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: 'time'
                        },
                        type: 'datetime',
                        min: Date.UTC(2018,1,27,0,0),
                        max: Date.UTC(2018,1,27,23,59),

                        dateTimeLabelFormats : {
                            hour: '%I %p',
                            minute: '%I:%M %p'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'I(amp)'
                        },
                        labels: {
                            formatter: function () {
                                return this.value;
                            }
                        },
                        opposite: false,
                    },
                    tooltip: {
                        crosshairs: true,
                        shared: true
                    },
                    plotOptions: {
                        series: {
                            events: {
                                show: (function() {
                                    var chartData = {CURRENT:"I(amp)", VOLTAGE:"Volt(volt)", DCPOWER:'P(watt)',ACPOWER:'P(watt)',DISCHARGE:'Q(l/s)',RPM:'rpm',FREQUENCY:'F(hz)'};
                                    var chart = this.chart,
                                        series = chart.series,
                                        i = series.length,
                                        otherSeries;
                                        chart.yAxis[0].setTitle({text: chartData[this.name]});
                                    while(i--) {
                                        otherSeries = series[i];
                                        if (otherSeries != this && otherSeries.visible) {
                                            otherSeries.hide();
                                        }
                                    }
                                }),
                                click: (function() { this.chart.yAxis[0].setTitle({text: this.name}); })

                            }
                        }
                    },
                    series: [{
                        name: 'CURRENT',
                        data: this.agePotValue,
                        pointStart: Date.UTC(2018,1,27,0,1),
                        pointInterval: 1 * 60 * 1000 // one minute
                    },{
                        name: 'VOLTAGE',
                        data: this.voltage,
                        visible:false,
                    },{
                        name: 'DCPOWER',
                        data: this.dcpower,
                        visible:false,
                    },{
                        name: 'ACPOWER',
                        data: this.acpower,
                        visible:false,
                    },{
                        name: 'DISCHARGE',
                        data: this.discharge,
                        visible:false,
                    },{
                        name: 'RPM',
                        data: this.rpm,
                        visible:false,
                    },{
                        name: 'FREQUENCY',
                        data: this.frequency,
                        visible:false,
                    }],
                    credits: false
                }
                this.chart = new Highcharts.chart(highchartsOptions)
            }
        });

        new Vue({
            el: '#app',
            data: {
                a: null,
                id : null,
                totalflow :this.updateTotalFlow ,
                totalruntime : this.updateTotalRunTime,
                titleValue : 'Amp(I)',

            },
            mounted(){
                axios.get('/api/meterdata/{{$meter_id}}').then(response => (this.a = response.data));


            },
            computed: {
                updateTotalRunTime(){
                    return this.a.length;
                },
                updateTotalFlow(){
                  var count = 0;
                  for(i=0;i<this.a.length;i++){
                      count = count + parseFloat(this.a[i].discharge);
                  }
                  count = count * 60;
                  return (Math.round(count * 100)/100);
                },
                title() { return this.titleValue },
                agePotValue() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].current)]
                    }
                    // this.titleValue = 'I (amp)';
                    console.log(this.title);
                    return chartdata;
                },
                voltage() {
                    var vm = this;
                    var chartdata = [];
                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].voltage)]
                    }
                    return chartdata;
                },
                dcpower() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].dc_power)]
                    }
                    return chartdata;
                },
                acpower() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].ac_power)]
                    }
                    return chartdata;
                },
                discharge() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].discharge)]
                    }
                    return chartdata;
                },
                rpm() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].rpm)]
                    }
                    return chartdata;
                },
                frequency() {
                    var vm = this;
                    var chartdata = [];

                    for(i = 0;i<this.a.length;i++){
                        var res = this.a[i].filename.split("_");
                        var time = res[1];
                        var minute = time.toString().slice(-2);
                        var hour = time.toString().substr(4,2);
                        var parsed_date = new Date(this.a[i].created_at);
                        var year = parsed_date.getFullYear();
                        var month = parsed_date.getMonth();
                        var day = parsed_date.getDate();
                        date = Date.UTC(year,month,day,hour,minute);
                        chartdata[i] = [date,parseFloat(this.a[i].frequency)]
                    }
                    return chartdata;
                },

            },
            components: { Chart }
        });
    </script>
@endsection
