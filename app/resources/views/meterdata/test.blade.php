@extends('layouts.default')
@section('metadata') <META HTTP-EQUIV="refresh" CONTENT="240"> @endsection
@section('title')Meters @endsection
@section('content')
    <div class="container-fluid">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Meter #</th>
                <th>Number of Data Points</th>
                <th>Data last Updated on</th>
            </tr>
            </thead>
            <tbody>
            @foreach($project->meters as $meter)
                <tr>
                    <td>
                        <a href="{{route('meterdata_demo.show',['id'=>$meter->id])}}">{{$meter->phoneNo}}</a>
                    </td>
                    <td>{{count($meter->meterdata)}}</td>
                    <td>{{$meter->meterdata->last()->created_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
