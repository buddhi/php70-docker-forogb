@extends('layouts.new-default')

@php
    $investment_percent = isset($investmentTotalData['investment_percent'])? $investmentTotalData['investment_percent']:0;
    $investment_amount = isset($investmentTotalData['investment_amount'])? $investmentTotalData['investment_amount'] : 0;
    $project_cost = isset($project->cost)? $project->cost:0;
    $farmer = $project->farmers[0];
    $crop_data = [];
    foreach($crops as $cropInfo) $crop_data[$cropInfo->name] = \App\Services\CropArea::convertCropAreaToUnit($cropInfo,'acre');
    $cattle_data = [];
    foreach($cattles as $cattle) $cattle_data[$cattle->name] = $cattle->pivot->number;
    $land_data["owned"] = 0;
    $land_data["leased"] = 0;

    if(isset($land_array)){
        foreach($land_array as $land){
          if($land['ownership'] == "owned"){
            $land_data["owned"] =$land_data["owned"] + \App\Services\CropArea::convertLandAreaToUnit($land,'acre');
          }else{
            $land_data["leased"] =$land_data["leased"] + \App\Services\CropArea::convertLandAreaToUnit($land,'acre');
          }
        }
    }
    $farmer_finance = [
      'Annual Income' => $farmer->annual_household_income,
      'Annual Expense' => $farmer->annual_expenses
    ];
    $farm_irrigation = [
      'current_irrigation_system' => currentIrrigationSystem($farm->current_irrigation_system),
      'current_irrigation_source' => currentIrrigationsource($farm->current_irrigation_source)
    ];
    $data = [
      "crop_data" => $crop_data,
      "land_data" => $land_data,
      "cattle_data" => $cattle_data,
      "farmer_finance" => $farmer_finance
    ];
@endphp

@section('title')Invest in Offgrid Solar Projects @endsection

@section('content')
  @include('common.alert')
  @include('common.session_message')
  <project-detail></project-detail>
  <share-news v-bind:paid="false" v-bind:showcontinue="false"></share-news>
@endsection

@section('js')
  <script type="text/javascript">
  let projectDetails = {}
  projectDetails.project = {!! json_encode($project) !!}
  projectDetails.chart_data = {!! json_encode($data) !!}
  projectDetails.investor_list = {!! json_encode($investor_list) !!}
  projectDetails.farm_irrigation = {!! json_encode($farm_irrigation) !!}
  projectDetails.investmentStore = {
    state: {
      percent: 30,
      project_cost: {{$project_cost}},
      raised: {{$investment_amount}},
      project_id: {{$project->id}},
      developer_id: {{$project->created_by}},
      investor_id: {{ Auth::user()->user_id}}
    },
    updateInvestmentPercent(newPercent) {
      this.state.percent = newPercent
    }
  }
  </script>
@endsection
