@extends('layouts.new-default')

@section('content')
  @include('common.alert')
  @include('common.session_message')
  <checkout></checkout>
@endsection
