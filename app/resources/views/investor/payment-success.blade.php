@extends('layouts.new-default')
@section('content')
<section class="container mt-5">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <div class="card">
            <div class="card-body text-center">
               <i class="icon ion-md-checkmark-circle text-success display-1"></i>
               <h4>Thank you for investing. We applaude your generosity.</h4>
               <p class="lead mb-4">We have received your contribution and we will keep you posted on the
                  project with timely updates.
               </p>
               <p>In the mean time, help us serve more farmers by spreading the world and inviting your friends, families and colleagues to be a part of this cause.</p>
               <br>
               <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#share">Continue</a></li>
            </div>
         </div>
      </div>
      <share-news :paid="true" :showcontinue="true" :showskip="true"> </share-news>
      <payment-success> </payment-success>
   </div>
</section>
@endsection
