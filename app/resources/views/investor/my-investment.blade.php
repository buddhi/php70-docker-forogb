@extends('layouts.new-default')

@section('title') Invest in Offgrid Solar Projects @endsection

@section('content')


<investment-list></investment-list>
<share-news :paid="true" :showcontinue="true"></share-news>

@endsection
