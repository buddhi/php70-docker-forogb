@extends('layouts.new-default')
@section('content')
    @include('common.session_message')
    <div class="alert alert-danger text-center">
        <i class="icon ion-md-alert text-danger h2"></i>
        <p class="h3">There has been a problem</p>
        <p>Unfortunately, your payment did not run through and we could not secure your investment. You can either try
            again or check your Paypal account for any faults</p>

        @if(isset($paymentErrorMessage) && is_array($paymentErrorMessage))
            @foreach($paymentErrorMessage as $key => $message)
                Project Id :{{$key}} : Payment error:: {{$message['message']}}<br>
            @endforeach
        @endif
        <payment-failure></payment-failure>
    </div>
    <checkout></checkout>
@endsection