@php

    $month = App\components\MonthUtility::$_MONTHS;

    $investment_percent = isset($investmentTotalData['investment_percent'])? $investmentTotalData['investment_percent']:0;
    $investment_amount = isset($investmentTotalData['investment_amount'])? $investmentTotalData['investment_amount'] : 0;
    $project_cost= isset($project->cost)? $project->cost:0;
    $funding_status     = $project->status;
@endphp

        <!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/screen.css')}}"
          media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/print.css')}}" media="print">

    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/ie.css')}}" media="screen, projection">


    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/style.css')}}">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.1/css/simple-line-icons.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/offgrid-bazaar/assets/9bb1aea1/jui/css/base/jquery-ui.css"/>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/highcharts.src.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/9bb1aea1/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/modules/drilldown.src.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/modules/exporting.src.js')}}"></script>
    <title>offgrid - Detail Project</title>
</head>

<body>

<div class="container" id="page">
    <header>
        @include('common.alert')
        @if (session()->has('role_message'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
                <strong>{!! session()->get('role_message') !!} !!</strong>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
                <strong>{!! session()->get('error') !!} !!</strong>
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>

                <strong>{!! session()->get('success') !!} !!</strong>
            </div>
        @endif
        <div class="mainheader container">
            <div class="col-sm-3">
                @guest
                    <a href="{{url('/')}}">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                    </a>
                    @else
                        <a href="{{route('project.list')}}">
                            <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                        </a>
                        @endguest
            </div>
            <div class="col-sm-5">
                <div class="stat small">
                    <h4><i class="icon-project"></i> {{$project_count}}</h4>
                    <small class="light">Projects</small>
                </div>
                <div class="stat small">
                    <h4><i class="icon-investment"></i> $ {{round($received_investment/100000,0)}}K</h4>
                    <small class="light">Investment</small>
                </div>
            </div>
            <div class="col-sm-4 text-right">
                <nav class="topright">
                    @guest
                        <a href="{{url('/login')}}">Login</a>
                        <a href="{{url('/register')}}">|Signup</a>
                        @else
                            {{Auth::user()->username }}
                            |<strong>{{returnRole(session('role'))}}</strong>
                            | <a href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                       logout();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                            @endguest
                </nav>
            </div>
        </div>
        <div class="container">
            <nav class="nav topnav">
                <a class="nav-link active" href="javascript:void(0);">All</a>
                <a class="nav-link" href="javascript:void(0);">Water Pump</a>
                <a class="nav-link" href="javascript:void(0);">Grinding Mill</a>
                <a class="nav-link" href="javascript:void(0);">Dairy Chilling</a>
                <a class="nav-link" href="javascript:void(0);">Refrigeration</a>
                <a class="nav-link" href="javascript:void(0);">Microgrid</a>
            </nav>
        </div>
    </header>
    <section class="maincontent project-detail">
        <div class="container">
            <div class="col-sm-8">
                <!-- PROJECT BASIC INFORMATION :: START -->
                <h2>
                    <strong> {{$project->farmer_name}}</strong>
                    <span class="label label-info">{{$funding_status }}</span>
                    <span class="label label-success">{{$project->name}}</span>
                </h2>
                <p>
                    {{$project->municipality->name}} ,
                    {{$project->province->name}}
                    <span class="sep"></span>
                    <span class="light">
					Project Developed by
					<strong>{{$project->user->full_name}}</strong>
					(<a data-toggle="modal" data-target="#contactDeveloperModal">Contact</a>)
				</span>
                </p>
                <br>
                <!-- PROJECT BASIC INFORMATION :: END -->

                <!-- PROJECT INCOME/ROI :: START -->
                <h4 class="msg">Your investment will help fund a solar-powered water-pumping solution that discharges
                    {{isset($project->daily_discharge)? $project->daily_discharge/1000:180}} kL of water per day for irrigation. Here’s the farmer’s cash flow projection based on increased
                    crop yields:</h4>
                <div id = "app">
                    <div id="contactDeveloperModal" class= "modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Send Message to developer</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="#" id="contact" @submit="sendMessage">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label" id="message_text">Message:</label>
                                            <textarea name="message_text" rows="15" cols="50" class="form-control" v-model="message_text"></textarea>
                                        </div>

                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary pull-left" @click="sendMessage" data-dismiss="modal">submit</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


                <!-- CATTLE INFORMATION :: START -->
                <div class="well">
                    <h4 class="bold">Cattle Information</h4>
                    <table class="table table-responsive table-striped">
                        <thead>
                        <td>Name</td>
                        <td>Number</td>
                        </thead>
                        @foreach($cattles as $cattleInfo)
                            <tr>
                                <td>{{$cattleInfo->name}}</td>
                                <td>{{$cattleInfo->pivot->number}}</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
                <!-- CROP INFORMATION :: END -->
                <!-- INVESTMENT FORM :: START -->
                <hr>
                <br>
                <h2 id="investment">Express your Interest to Fund</h2>
                <p><strong>This project is looking for bank loans, grants/subsidies, and equity investment.</strong></p>
                <p>Select the amount or percentage you are interested to invest, and your desired investment terms. You
                    can also contact the project developer to get more information about the project.</p>
                <br>
                <form class="form-horizontal" action="{{route('investment.checkout.store')}}" method="post" id= "investment_form">
                    {{ csrf_field() }}
                    <div class="form form-group">
                        <div class="toggle-btn-group">
                            <div class="row">
                                <input class="form-control" name="payment_gateway" id="payment_gateway" type="hidden" value="noPaymentGateway">
                                <input class="form-control" name="project_id[]" id="ProjectInvestment_project_id" type="hidden" value="{{$project->id}}">
                                <input class="form-control" name="developer_id[]" id="ProjectInvestment_developer_id" type="hidden" value="{{$project->created_by}}">
                                <input class="form-control" name="investor_id[]" id="ProjectInvestment_investor_id" type="hidden" value="{{ Auth::user()->user_id}}">
                                <div class="col-md-7">
                                    <div class="btn-group" data-toggle="buttons" id="funding_type">
                                        <label class="btn btn-secondary active">
                                            <input type="radio" name="funding_type[]" id="ProjectInvestment_funding_type_percent" class="project_funding_type valid" value="0" autocomplete="off" checked="" data-toggle-fields="in-percent-fields" aria-required="true" aria-invalid="false"> In Percentage
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="funding_type[]" id="ProjectInvestment_funding_type_amt" class="project_funding_type valid" value="1" autocomplete="off" data-toggle-fields="in-amount-fields" aria-invalid="false"> In Amount
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div id="in-percent-fields" class="toggle-fields show">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Percentage" name="invest_percent[]" id="ProjectInvestment_investment_percent" type="number" max="100" aria-required="true">
                                                    <span class="input-group-addon" id="basic-addon1">%</span>
                                                </div>
                                                @if ($errors->has('invest_percent'))
                                                    <span class="error">
                                                    <strong>{{ $errors->first('invest_percent') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div id="in-amount-fields" class="toggle-fields">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1">Rs.</span>
                                                    <input class="form-control" placeholder="Amount in NPR" step="0.5" name="invest_amount[]" id="ProjectInvestment_invest_amount" type="number" maxlength="100" aria-required="true">
                                                </div>
                                                @if ($errors->has('invest_amount'))
                                                    <span class="error">
                                                    <strong>{{ $errors->first('invest_amount') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form form-group">
                        <h2 class="form-section-header">Kind of Investment</h2>
                        <label for="investment_tyep">How would you like to invest on this project as a investor?</label>
                        <div class="toggle-btn-group" id="investment_type" data-current-toggle="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-secondary active">
                                            <input type="radio" name="investment_type_id" id="ProjectInvestment_type_id_equity" class="project_investment_type valid" value="1" autocomplete="off" checked="" data-toggle-fields="equity-fields" aria-invalid="false"> Equity
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="investment_type_id" id="ProjectInvestment_type_id_debt" class="project_investment_type valid" value="2" autocomplete="off" data-toggle-fields="debt-fields" aria-invalid="false"> Debt
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="investment_type_id" id="ProjectInvestment_type_id_grant" class="project_investment_type valid" value="3" autocomplete="off" data-toggle-fields="null" aria-invalid="false"> Grant/Fund
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="debt-fields" class="toggle-fields">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Term" step="0.5" name="term[]" id="ProjectInvestment_term" type="number" aria-required="true">									<span class="input-group-addon" id="basic-addon1">Months</span>
                                                </div>
                                                @if ($errors->has('term'))
                                                    <span class="error">
                                                    <strong>{{ $errors->first('term') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Interest Rate" step="0.1" name="interest_rate[]" id="ProjectInvestment_interest_rate" type="number" aria-required="true">									<span class="input-group-addon" id="basic-addon1">%</span>
                                                </div>
                                                @if ($errors->has('interest_rate'))
                                                    <span class="error">
                                                    <strong>{{ $errors->first('interest_rate') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div id="equity-fields" class="toggle-fields show">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <input class="form-control" placeholder="Expected IRR" step="0.5" name="expected_irr[]" id="ProjectInvestment_expected_irr" type="number" aria-required="true">									<span class="input-group-addon" id="basic-addon1">%</span>
                                                </div>
                                                @if ($errors->has('expected_irr'))
                                                    <span class="error">
                                                    <strong>{{ $errors->first('expected_irr') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="comments">Comments</label>
                        <textarea class="form-control" name = "comments" rows="3" data-validetta="required"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-lg btn-success" type="submit" name="noPaymentGateway">Submit</button>
                        <a href="{{route('project.list')}}"><button class="btn btn-default "  type="button" name="Go_back">Go back</button></a>
                    </div>
                </form>

            </div>

            <div class="col-sm-4">
                <!-- PROJECT INVESTMENT DETAIL :: START -->

                <div class="well">
                    <p><strong>Total cost:  </strong> {{ displayUnitFormat("amount",$project_cost)}}<p>
                    <p><strong>Invested:   </strong> {{ displayUnitFormat("percent",$investment_percent)}}  of {{ displayUnitFormat("amount",$project_cost)}}<p>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{round($investment_percent,2)}}%;" data-progress-value={{$investment_percent}}>
                            {{displayUnitFormat("percent",$investment_percent)}}
                        </div>
                    </div>
                    <h1>{{displayUnitFormat("amount",$investment_amount)}}</h1>
                    <h2>{{displayUnitFormat("percent",$investment_percent)}}
                    </h2>
                    <p>Total investment received
                        <section class="row">
                            <div class="col-xs-4">
                                <h4>{{displayUnitFormat("percent",$projectInvestmentDetail['debt'])}}</h4>
                    <p class="light">Debt</p>
                </div>
                <div class="col-xs-4">
                    <h4>{{displayUnitFormat("percent",$projectInvestmentDetail['grant'])}}</h4>
                    <p class="light">Grant</p>
                </div>
                <div class="col-xs-4">
                    <h4>{{displayUnitFormat("percent",$projectInvestmentDetail['equity'])}}</h4>
                    <p class="light">Equity</p>
                </div>

                <div class="col-xs-6">
                    <a href="javascript:void(0)" class="btn btn-default btn-block favourite  "><i
                                class="icon-favourite-line"></i> Favorite</a>
                </div>
                <div class="col-xs-6">
                    <a href="{{@BASE}}/" class="btn btn-default btn-block"><i class="icon-share"></i> Share</a>
                </div>
            </div>
    </section>

</div>

</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>

<script>
    $(document).ready(function () {
        $("form").on("submit", function(event) {
            event.preventDefault();
            var project_cost = {!! json_encode($project_cost)!!}
            var funding_type = $("input[name='funding_type[]']:checked").val();
            if(funding_type == 0){
                var invest_percent = $("input[name='invest_percent[]']").val();
                var invest_amount = invest_percent/100*project_cost;
                $("input[name='invest_amount[]']").val(invest_amount);

            }else{
                var invest_amount = $("input[name='invest_amount[]']").val();
                var invest_percent = (invest_amount/ project_cost)*100;
                $("input[name='invest_percent[]']").val(invest_percent);
            }
             $("#investment_form")[0].submit()

        });
    })
</script>
<script type="text/javascript">
</script>
<script type="text/javascript">
    var investment_type = $('#investment_type');
    var funding_type = $('#funding_type');

    function toggleFields($type) {
        var $current_toggle = undefined;
        $.each($('[data-toggle-fields]', $type), function (index, value) {
            var $elem = $(value);
            var data = $elem.data('toggle-fields');
            var $toggle_elem = $('#' + data);
            $(this).on('change', function () {
                // console.log( 'before' , $current_toggle );
                if ($current_toggle !== undefined) {
                    $current_toggle.removeClass('show');
                }
                if ($toggle_elem.length) {
                    $toggle_elem.addClass('show');
                    $current_toggle = $toggle_elem;
                }
            });

            if ($(this).attr('checked') === 'checked') {
                if ($current_toggle !== undefined) {
                    $current_toggle.removeClass('show');
                }

                if ($toggle_elem.length) {
                    $toggle_elem.addClass('show');
                    $current_toggle = $toggle_elem;
                }
            }
        });
    }

    toggleFields(investment_type);

    toggleFields(funding_type);
</script>


<script src="{{asset('offgrid-bazaar/js/bootstrap.min.js')}}"></script>



</div><!-- page -->
@include('common.logout');

</body>
</html>
