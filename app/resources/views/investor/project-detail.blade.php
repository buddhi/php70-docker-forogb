<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/screen.css')}}"
          media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/print.css')}}" media="print">

    {{--<link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/ie.css')}}" media="screen, projection">--}}


    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.1/css/simple-line-icons.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/offgrid-bazaar/assets/9bb1aea1/jui/css/base/jquery-ui.css"/>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/highcharts.src.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/9bb1aea1/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/modules/drilldown.src.js')}}"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/fcedd565/modules/exporting.src.js')}}"></script>
    <title>offgrid - Detail Project</title>
</head>

<body>

<div class="container" id="page">
    <header>
        <div class="mainheader container">
            <div class="col-sm-3">
                @guest
                    <a href="{{url('/')}}">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                    </a>
                    @else
                        <a href="{{route('project.list')}}">
                            <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                        </a>
                        @endguest
            </div>
            <div class="col-sm-5">
                <div class="stat small">
                    <h4><i class="icon-project"></i> 0</h4>
                    <small class="light">Projects</small>
                </div>
                <div class="stat small">
                    <h4><i class="icon-investment"></i> Rs. 0.00K</h4>
                    <small class="light">Investment</small>
                </div>
            </div>
            <div class="col-sm-4 text-right">
                <nav class="topright">
                    @guest
                        <a href="{{url('/login')}}">Login</a>
                        <a href="{{url('/register')}}">|Signup</a>
                        @else
                            {{Auth::user()->username }}
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                            @endguest
                </nav>

            </div>
        </div>
        <div class="container">
            <nav class="nav topnav">
                <a class="nav-link active" href="javascript:void(0);">All</a>
                <a class="nav-link" href="javascript:void(0);">Water Pump</a>
                <a class="nav-link" href="javascript:void(0);">Grinding Mill</a>
                <a class="nav-link" href="javascript:void(0);">Dairy Chilling</a>
                <a class="nav-link" href="javascript:void(0);">Refrigeration</a>
                <a class="nav-link" href="javascript:void(0);">Microgrid</a>
            </nav>
        </div>
    </header>

    <section class="maincontent">
        <div class="container">
            <div class="col-sm-6">
                <h2>
                    <strong>{{$project->farmer_name}} </strong>
                    <span class="label label-success">Operational</span></h2>
                <p>
                    {{$project->municipality->name}},
                    {{$project->district->name}},
                    {{$project->province->name}}
                    <span class="sep"></span>
                    <span class="light">Project Developed by
        			<strong>{{$project->user->full_name}}</strong></span>
                </p>
            </div>
            <div class="col-sm-6 text-right">
                <div class="top-cta-buttons">
                    <a href="http://localhost/offgrid-bazaar/" class="btn btn-default"><i class="icon-contact"></i>
                        Contact</a>
                    <a href="javascript:void(0)" class="btn btn-default favourite "><i class="icon-favourite-line"></i>
                        Favorite</a>
                    <a href="http://localhost/offgrid-bazaar/" class="btn btn-default"><i class="icon-share"></i> Share</a>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="stat">
                    <h1><i class="icon-investment"></i> Rs. 45K</h1>
                    <p class="light">Repaid</p>
                </div>
                <div class="stat">
                    <h1><i class="icon-water"></i> 3.01 m<sup>3</sup></h1>
                    <p class="light">Water Discharged</p>
                </div>
                <div class="stat">
                    <h1><i class="icon-energy"></i> -0.06 KWh</h1>
                    <p class="light">Solar Energy Used</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="well">
                <div class="col-sm-6">
                    <h4>Crop yield prediction</h4>
                    <div id="container" style="min-width: 300px; height: 400px; margin: 0 auto">
                        <div id="crop-yield-chart-widget" data-highcharts-chart="0"><div id="highcharts-0" class="highcharts-container " style="position: relative; overflow: hidden; width: 508px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="508" viewBox="0 0 508 400" height="400"><desc>Created with Highcharts 5.0.2</desc><defs><clipPath id="highcharts-1"><rect x="0" y="0" width="422" height="289" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="508" height="400" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="76" y="10" width="422" height="289"></rect><g class="highcharts-grid highcharts-xaxis-grid "><path fill="none" class="highcharts-grid-line" d="M 110.5 10 L 110.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 145.5 10 L 145.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 181.5 10 L 181.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 216.5 10 L 216.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 251.5 10 L 251.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 286.5 10 L 286.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 321.5 10 L 321.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 356.5 10 L 356.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 391.5 10 L 391.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 427.5 10 L 427.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 462.5 10 L 462.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 497.5 10 L 497.5 299" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 75.5 10 L 75.5 299" opacity="1"></path></g><g class="highcharts-grid highcharts-yaxis-grid "><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 299.5 L 498 299.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 251.5 L 498 251.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 203.5 L 498 203.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 155.5 L 498 155.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 106.5 L 498 106.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 58.5 L 498 58.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 76 9.5 L 498 9.5" opacity="1"></path></g><rect fill="none" class="highcharts-plot-border" x="76" y="10" width="422" height="289"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 110.5 299 L 110.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 145.5 299 L 145.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 181.5 299 L 181.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 216.5 299 L 216.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 251.5 299 L 251.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 286.5 299 L 286.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 321.5 299 L 321.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 356.5 299 L 356.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 391.5 299 L 391.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 427.5 299 L 427.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 462.5 299 L 462.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 497.5 299 L 497.5 309" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 75.5 299 L 75.5 309" opacity="1"></path><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 76 299.5 L 498 299.5"></path></g><g class="highcharts-axis highcharts-yaxis "><text x="29.328125" text-anchor="middle" transform="translate(0,0) rotate(270 29.328125 154.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="154.5"><tspan>Yield (kg)</tspan></text><path fill="none" class="highcharts-axis-line" d="M 76 10 L 76 299"></path></g><path fill="none" class="highcharts-crosshair highcharts-crosshair-category undefined" stroke="rgba(204,214,235,0.25)" stroke-width="35.166666666666664" visibility="hidden" d="M 268.5 10 L 268.5 299"></path><g class="highcharts-series-group "><g class="highcharts-series highcharts-series-0 highcharts-column-series highcharts-color-0 highcharts-tracker" transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="6.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="42.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 " stroke="#ffffff" stroke-width="1"></rect><rect x="77.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 " stroke="#ffffff" stroke-width="1"></rect><rect x="112.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 " stroke="#ffffff" stroke-width="1"></rect><rect x="147.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 " stroke="#ffffff" stroke-width="1"></rect><rect x="182.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 " stroke="#ffffff" stroke-width="1"></rect><rect x="217.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="253.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="288.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="323.5" y="255.5" width="3" height="34" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="358.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect><rect x="393.5" y="289.5" width="3" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-0 highcharts-column-series highcharts-color-0 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-1 highcharts-column-series highcharts-color-1 highcharts-tracker " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="9.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="45.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1 " stroke="#ffffff" stroke-width="1"></rect><rect x="80.5" y="279.5" width="3" height="10" fill="#434348" class="highcharts-point highcharts-color-1 " stroke="#ffffff" stroke-width="1"></rect><rect x="115.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1 " stroke="#ffffff" stroke-width="1"></rect><rect x="150.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1 " stroke="#ffffff" stroke-width="1"></rect><rect x="185.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1 " stroke="#ffffff" stroke-width="1"></rect><rect x="220.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="256.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="291.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="326.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="361.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect><rect x="396.5" y="289.5" width="3" height="0" fill="#434348" class="highcharts-point highcharts-color-1" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-1 highcharts-column-series highcharts-color-1 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-2 highcharts-column-series highcharts-color-2 highcharts-tracker" transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="12.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="48.5" y="48.5" width="3" height="241" fill="#90ed7d" class="highcharts-point highcharts-color-2 " stroke="#ffffff" stroke-width="1"></rect><rect x="83.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2 " stroke="#ffffff" stroke-width="1"></rect><rect x="118.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2 " stroke="#ffffff" stroke-width="1"></rect><rect x="153.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2 " stroke="#ffffff" stroke-width="1"></rect><rect x="188.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2 " stroke="#ffffff" stroke-width="1"></rect><rect x="223.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="259.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="294.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="329.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="364.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect><rect x="399.5" y="289.5" width="3" height="0" fill="#90ed7d" class="highcharts-point highcharts-color-2" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-2 highcharts-column-series highcharts-color-2 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-3 highcharts-column-series highcharts-color-3 highcharts-tracker" transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="15.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="51.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3 " stroke="#ffffff" stroke-width="1"></rect><rect x="86.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3 " stroke="#ffffff" stroke-width="1"></rect><rect x="121.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3 " stroke="#ffffff" stroke-width="1"></rect><rect x="156.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3 " stroke="#ffffff" stroke-width="1"></rect><rect x="191.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3 " stroke="#ffffff" stroke-width="1"></rect><rect x="226.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="262.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="297.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="332.5" y="193.5" width="3" height="96" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="367.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect><rect x="402.5" y="289.5" width="3" height="0" fill="#f7a35c" class="highcharts-point highcharts-color-3" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-3 highcharts-column-series highcharts-color-3 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-4 highcharts-column-series highcharts-color-4 highcharts-tracker " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="18.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="54.5" y="202.5" width="3" height="87" fill="#8085e9" class="highcharts-point highcharts-color-4 " stroke="#ffffff" stroke-width="1"></rect><rect x="89.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4 " stroke="#ffffff" stroke-width="1"></rect><rect x="124.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4 " stroke="#ffffff" stroke-width="1"></rect><rect x="159.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4 " stroke="#ffffff" stroke-width="1"></rect><rect x="194.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4 " stroke="#ffffff" stroke-width="1"></rect><rect x="229.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="265.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="300.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="335.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="370.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect><rect x="405.5" y="289.5" width="3" height="0" fill="#8085e9" class="highcharts-point highcharts-color-4" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-4 highcharts-column-series highcharts-color-4 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-5 highcharts-column-series highcharts-color-5 highcharts-tracker" transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="21.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="57.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5 " stroke="#ffffff" stroke-width="1"></rect><rect x="92.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5 " stroke="#ffffff" stroke-width="1"></rect><rect x="127.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5 " stroke="#ffffff" stroke-width="1"></rect><rect x="162.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5 " stroke="#ffffff" stroke-width="1"></rect><rect x="197.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5 " stroke="#ffffff" stroke-width="1"></rect><rect x="232.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="268.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="303.5" y="202.5" width="3" height="87" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="338.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="373.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect><rect x="408.5" y="289.5" width="3" height="0" fill="#f15c80" class="highcharts-point highcharts-color-5" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-5 highcharts-column-series highcharts-color-5 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g><g class="highcharts-series highcharts-series-6 highcharts-column-series highcharts-color-6 highcharts-tracker " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="url(#highcharts-1)"><rect x="24.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="60.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6 " stroke="#ffffff" stroke-width="1"></rect><rect x="95.5" y="193.5" width="3" height="96" fill="#e4d354" class="highcharts-point highcharts-color-6 " stroke="#ffffff" stroke-width="1"></rect><rect x="130.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6 " stroke="#ffffff" stroke-width="1"></rect><rect x="165.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6 " stroke="#ffffff" stroke-width="1"></rect><rect x="200.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6 " stroke="#ffffff" stroke-width="1"></rect><rect x="235.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="271.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="306.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="341.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="376.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect><rect x="411.5" y="289.5" width="3" height="0" fill="#e4d354" class="highcharts-point highcharts-color-6" stroke="#ffffff" stroke-width="1"></rect></g><g class="highcharts-markers highcharts-series-6 highcharts-column-series highcharts-color-6 " transform="translate(76,10) scale(1 1)" width="289" height="422" clip-path="none"></g></g><g class="highcharts-button highcharts-contextbutton" style="cursor:pointer;" stroke-linecap="round" transform="translate(474,10)"><title>Chart context menu</title><rect fill="#ffffff" class=" highcharts-button-box" x="0.5" y="0.5" width="24" height="22" rx="2" ry="2" stroke="none" stroke-width="1"></rect><path fill="#666666" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" class="highcharts-button-symbol" stroke="#666666" stroke-width="3"></path><text x="0" style="font-weight:normal;color:#333333;fill:#333333;" y="12"></text></g><g class="highcharts-legend" transform="translate(51,336)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="405" height="49" visibility="visible"></rect><g><g><g class="highcharts-legend-item highcharts-column-series highcharts-color-0 highcharts-series-0" transform="translate(8,3)"><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" y="15">Paddy</text><rect x="2" y="4" width="12" height="12" fill="#7cb5ec" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-1 highcharts-series-1" transform="translate(87.1875,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Barley/Oats/Wheat</text><rect x="2" y="4" width="12" height="12" fill="#434348" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-2 highcharts-series-2" transform="translate(242.515625,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Tomato</text><rect x="2" y="4" width="12" height="12" fill="#90ed7d" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-3 highcharts-series-3" transform="translate(331.953125,3)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Squash</text><rect x="2" y="4" width="12" height="12" fill="#f7a35c" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-4 highcharts-series-4" transform="translate(8,22)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Cucumber</text><rect x="2" y="4" width="12" height="12" fill="#8085e9" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-5 highcharts-series-5" transform="translate(112.078125,22)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Cucumber</text><rect x="2" y="4" width="12" height="12" fill="#f15c80" rx="6" ry="6" class="highcharts-point"></rect></g><g class="highcharts-legend-item highcharts-column-series highcharts-color-6 highcharts-series-6" transform="translate(216.15625,22)"><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Squash</text><rect x="2" y="4" width="12" height="12" fill="#e4d354" rx="6" ry="6" class="highcharts-point"></rect></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels "><text x="93.58333333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Jan</text><text x="128.74999999999997" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Feb</text><text x="163.91666666666666" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Mar</text><text x="199.08333333333331" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Apr</text><text x="234.25" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">May</text><text x="269.4166666666667" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Jun</text><text x="304.5833333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Jul</text><text x="339.75" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Aug</text><text x="374.91666666666663" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Sep</text><text x="410.0833333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Oct</text><text x="445.25" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Nov</text><text x="480.41666666666663" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="318" opacity="1">Dec</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels "><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="302" opacity="1"><tspan>0</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="254" opacity="1"><tspan>10k</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="205" opacity="1"><tspan>20k</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="157" opacity="1"><tspan>30k</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="109" opacity="1"><tspan>40k</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="61" opacity="1"><tspan>50k</tspan></text><text x="61" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="13" opacity="1"><tspan>60k</tspan></text></g><g class="highcharts-label highcharts-tooltip highcharts-color-0" style="cursor:default;pointer-events:none;white-space:nowrap;" transform="translate(106,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 146.5 0.5 C 149.5 0.5 149.5 0.5 149.5 3.5 L 149.5 164.5 C 149.5 167.5 149.5 167.5 146.5 167.5 L 3.5 167.5 C 0.5 167.5 0.5 167.5 0.5 164.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 146.5 0.5 C 149.5 0.5 149.5 0.5 149.5 3.5 L 149.5 164.5 C 149.5 167.5 149.5 167.5 146.5 167.5 L 3.5 167.5 C 0.5 167.5 0.5 167.5 0.5 164.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 146.5 0.5 C 149.5 0.5 149.5 0.5 149.5 3.5 L 149.5 164.5 C 149.5 167.5 149.5 167.5 146.5 167.5 L 3.5 167.5 C 0.5 167.5 0.5 167.5 0.5 164.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 146.5 0.5 C 149.5 0.5 149.5 0.5 149.5 3.5 L 149.5 164.5 C 149.5 167.5 149.5 167.5 146.5 167.5 L 3.5 167.5 C 0.5 167.5 0.5 167.5 0.5 164.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path></g></svg><div class="highcharts-label highcharts-tooltip" style="position: absolute; left: 106px; top: -9999px; opacity: 0; pointer-events: none; visibility: visible;"><span style="font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif; font-size: 12px; position: absolute; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px">Jun</span><table><tbody><tr>
			   						<td style="color:#7cb5ec;padding:0">Paddy: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#434348;padding:0">Barley/Oats/Wheat: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#90ed7d;padding:0">Tomato: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#f7a35c;padding:0">Squash: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#8085e9;padding:0">Cucumber: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#f15c80;padding:0">Cucumber: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr><tr>
			   						<td style="color:#e4d354;padding:0">Squash: </td>
			   						<td style="padding:0"><b>0.0 </b></td>
			   					</tr></tbody></table></span></div></div></div>				</div>
                </div>
                <div class="col-sm-6">
                    <table class="table table-hover">
                        <thead>
                        <tr><td>yield(kg/ha)</td>
                            <td>Crop</td>
                            <td>yield month</td>
                        </tr></thead>
                        <tbody><tr>
                            <td>7,000</td>
                            <td>Paddy</td>
                            <td>Jun/Sep</td>
                        </tr>
                        <tr>
                            <td>2,087</td>
                            <td>Barley/Oats/Wheat</td>
                            <td>Nov/Feb</td>
                        </tr>
                        <tr>
                            <td>50,000</td>
                            <td>Tomato</td>
                            <td>Oct/Jan</td>
                        </tr>
                        <tr>
                            <td>20,000</td>
                            <td>Squash</td>
                            <td>May/Sep</td>
                        </tr>
                        <tr>
                            <td>18,000</td>
                            <td>Cucumber</td>
                            <td>Nov/Jan</td>
                        </tr>
                        <tr>
                            <td>18,000</td>
                            <td>Cucumber</td>
                            <td>May/Aug</td>
                        </tr>
                        <tr>
                            <td>20,000</td>
                            <td>Squash</td>
                            <td>Oct/Feb</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- <div class="col-sm-4">
                    <h4>Yearly yield prediction</h4>
                    <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div> -->
                <div class="clearfix"></div>
            </div>
            <div class="well">
                <div class="col-sm-6">
                    <h4>Yield Calculations Prediction</h4>
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">
                        <div id="yw0" data-highcharts-chart="1"><div id="highcharts-2" class="highcharts-container " style="position: relative; overflow: hidden; width: 508px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="508" viewBox="0 0 508 400" height="400"><desc>Created with Highcharts 5.0.2</desc><defs><clipPath id="highcharts-3"><rect x="0" y="0" width="416" height="265" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="508" height="400" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="82" y="53" width="416" height="265"></rect><g class="highcharts-grid highcharts-xaxis-grid "><path fill="none" class="highcharts-grid-line" d="M 116.5 53 L 116.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 150.5 53 L 150.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 185.5 53 L 185.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 220.5 53 L 220.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 254.5 53 L 254.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 289.5 53 L 289.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 324.5 53 L 324.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 358.5 53 L 358.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 393.5 53 L 393.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 428.5 53 L 428.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 462.5 53 L 462.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 497.5 53 L 497.5 318" opacity="1"></path><path fill="none" class="highcharts-grid-line" d="M 81.5 53 L 81.5 318" opacity="1"></path></g><g class="highcharts-grid highcharts-yaxis-grid "><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 318.5 L 498 318.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 265.5 L 498 265.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 212.5 L 498 212.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 159.5 L 498 159.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 106.5 L 498 106.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 82 52.5 L 498 52.5" opacity="1"></path></g><rect fill="none" class="highcharts-plot-border" x="82" y="53" width="416" height="265"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 116.5 318 L 116.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 150.5 318 L 150.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 185.5 318 L 185.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 220.5 318 L 220.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 254.5 318 L 254.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 289.5 318 L 289.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 324.5 318 L 324.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 358.5 318 L 358.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 393.5 318 L 393.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 428.5 318 L 428.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 462.5 318 L 462.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 497.5 318 L 497.5 328" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 81.5 318 L 81.5 328" opacity="1"></path><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 82 318.5 L 498 318.5"></path></g><g class="highcharts-axis highcharts-yaxis "><text x="28.390625" text-anchor="middle" transform="translate(0,0) rotate(270 28.390625 185.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="185.5"><tspan>Income (Rs.)</tspan></text><path fill="none" class="highcharts-axis-line" d="M 82 53 L 82 318"></path></g><path fill="none" class="highcharts-crosshair highcharts-crosshair-category undefined" stroke="rgba(204,214,235,0.25)" stroke-width="34.666666666666664" visibility="hidden" d="M 445.5 53 L 445.5 318"></path><g class="highcharts-series-group"><g class="highcharts-series highcharts-series-0 highcharts-column-series highcharts-color-0 highcharts-tracker " transform="translate(82,53) scale(1 1)" width="265" height="416" clip-path="url(#highcharts-3)"><rect x="11" y="227" width="13" height="39" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect><rect x="46" y="105" width="13" height="161" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect><rect x="80" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="115" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="150" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="184" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect><rect x="219" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect><rect x="254" y="257" width="13" height="9" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="288" y="36" width="13" height="230" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="323" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect><rect x="358" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0 "></rect><rect x="392" y="266" width="13" height="0" fill="#7cb5ec" class="highcharts-point highcharts-color-0"></rect></g><g class="highcharts-markers highcharts-series-0 highcharts-column-series highcharts-color-0 " transform="translate(82,53) scale(1 1)" width="265" height="416" clip-path="none"></g></g><g class="highcharts-button highcharts-contextbutton" style="cursor:pointer;" stroke-linecap="round" transform="translate(474,10)"><title>Chart context menu</title><rect fill="#ffffff" class=" highcharts-button-box" x="0.5" y="0.5" width="24" height="22" rx="2" ry="2" stroke="none" stroke-width="1"></rect><path fill="#666666" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" class="highcharts-button-symbol" stroke="#666666" stroke-width="3"></path><text x="0" style="font-weight:normal;color:#333333;fill:#333333;" y="12"></text></g><text x="254" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;width:444px;" y="24"><tspan>Monthly Net Income Projection</tspan></text><g class="highcharts-legend" transform="translate(201,355)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="106" height="30" visibility="visible"></rect><g><g><g class="highcharts-legend-item highcharts-column-series highcharts-color-0 highcharts-series-0" transform="translate(8,3)"><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" y="15"><tspan>Net Income</tspan></text><rect x="2" y="4" width="12" height="12" fill="#7cb5ec" rx="6" ry="6" class="highcharts-point"></rect></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels "><text x="99.33333333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Jan</text><text x="133.99999999999997" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Feb</text><text x="168.66666666666666" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Mar</text><text x="203.33333333333331" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Apr</text><text x="238" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">May</text><text x="272.6666666666667" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Jun</text><text x="307.3333333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Jul</text><text x="342" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Aug</text><text x="376.66666666666663" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Sep</text><text x="411.3333333333333" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Oct</text><text x="446" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Nov</text><text x="480.66666666666663" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="337" opacity="1">Dec</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels "><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="321" opacity="1"><tspan>0</tspan></text><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="268" opacity="1"><tspan>50k</tspan></text><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="215" opacity="1"><tspan>100k</tspan></text><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="162" opacity="1"><tspan>150k</tspan></text><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="109" opacity="1"><tspan>200k</tspan></text><text x="67" style="color:#666666;cursor:default;font-size:11px;fill:#666666;width:158px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="56" opacity="1"><tspan>250k</tspan></text></g><g class="highcharts-label highcharts-tooltip highcharts-color-0" style="cursor:default;pointer-events:none;white-space:nowrap;" transform="translate(378,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 125.5 0.5 C 128.5 0.5 128.5 0.5 128.5 3.5 L 128.5 51.5 C 128.5 54.5 128.5 54.5 125.5 54.5 L 73.5 54.5 67.5 60.5 61.5 54.5 3.5 54.5 C 0.5 54.5 0.5 54.5 0.5 51.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 125.5 0.5 C 128.5 0.5 128.5 0.5 128.5 3.5 L 128.5 51.5 C 128.5 54.5 128.5 54.5 125.5 54.5 L 73.5 54.5 67.5 60.5 61.5 54.5 3.5 54.5 C 0.5 54.5 0.5 54.5 0.5 51.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 125.5 0.5 C 128.5 0.5 128.5 0.5 128.5 3.5 L 128.5 51.5 C 128.5 54.5 128.5 54.5 125.5 54.5 L 73.5 54.5 67.5 60.5 61.5 54.5 3.5 54.5 C 0.5 54.5 0.5 54.5 0.5 51.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 125.5 0.5 C 128.5 0.5 128.5 0.5 128.5 3.5 L 128.5 51.5 C 128.5 54.5 128.5 54.5 125.5 54.5 L 73.5 54.5 67.5 60.5 61.5 54.5 3.5 54.5 C 0.5 54.5 0.5 54.5 0.5 51.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path></g></svg><div class="highcharts-label highcharts-tooltip" style="position: absolute; left: 378px; top: -9999px; opacity: 0; pointer-events: none; visibility: visible;"><span style="font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif; font-size: 12px; position: absolute; white-space: nowrap; color: rgb(51, 51, 51); margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px">Nov</span><table><tbody><tr>
			   						<td style="color:#7cb5ec;padding:0">Net Income: </td>
			   						<td style="padding:0"><b>Rs. 0.0 </b></td>
			   					</tr></tbody></table></span></div></div></div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr><td>Name</td>
                            <td>Start Month</td>
                            <td>End Month</td>
                            <td>Area (in Hectare)</td>
                        </tr></thead>
                        <tbody><tr>
                            <td>Paddy</td>
                            <td>Jun</td>
                            <td>Sep</td>
                            <td>1.3</td>
                        </tr>
                        <tr>
                            <td>Barley/Oats/Wheat</td>
                            <td>Nov</td>
                            <td>Feb</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>Tomato</td>
                            <td>Oct</td>
                            <td>Jan</td>
                            <td>0.03</td>
                        </tr>
                        <tr>
                            <td>Squash</td>
                            <td>May</td>
                            <td>Sep</td>
                            <td>0.03</td>
                        </tr>
                        <tr>
                            <td>Cucumber</td>
                            <td>Nov</td>
                            <td>Jan</td>
                            <td>0.03</td>
                        </tr>
                        <tr>
                            <td>Cucumber</td>
                            <td>May</td>
                            <td>Aug</td>
                            <td>0.03</td>
                        </tr>
                        <tr>
                            <td>Squash</td>
                            <td>Oct</td>
                            <td>Feb</td>
                            <td>0.03</td>
                        </tr>
                        </tbody></table>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- <div class="well">
                <div class="col-sm-6">
                    <h4>Farmer Cash Flow (Based on yield calculations)</h4>
                    <div id="container3" style="height: 400px; min-width: 310px"></div>
                </div>
                <div class="col-sm-6">
                    <table class="table table-hover">
                        <thead>
                            <td>Month</td>
                            <td>Net income</td>
                            <td>Net Profit</td>
                        </thead>
                        <tr>
                         <td>Jan</td>
                         <td>593</td>
                         <td>793</td>
                        </tr>
                        <tr>
                         <td>Feb</td>
                         <td>148.075</td>
                         <td>348.075</td>
                        </tr>
                        <tr>
                         <td>March</td>
                         <td>148.075</td>
                         <td>348.075</td>
                        </tr>
                        <tr>
                         <td>April</td>
                         <td>148.075</td>
                         <td>348.075</td>
                        </tr>
                        <tr>
                         <td>May</td>
                         <td>-161.39</td>
                         <td>38.61</td>
                        </tr>
                        <tr>
                         <td>Jun</td>
                         <td>-161.39</td>
                         <td>38.61</td>
                        </tr>
                        <tr>
                         <td>Jul</td>
                         <td>-161.39</td>
                         <td>38.61</td>
                        </tr>
                        <tr>
                         <td>Aug</td>
                         <td>372</td>
                         <td>572</td>
                        </tr>
                        <tr>
                         <td>Sep</td>
                         <td>372</td>
                         <td>572</td>
                        </tr>
                        <tr>
                         <td>Oct</td>
                         <td>372</td>
                         <td>572</td>
                        </tr>
                        <tr>
                         <td>Nov</td>
                         <td>593</td>
                         <td>793</td>
                        </tr>
                        <tr>
                         <td>Dec</td>
                         <td>593</td>
                         <td>793</td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div> -->
            <div class="well">
                <div class="col-sm-6">
                    <h4>Environmental Variables Trend</h4>
                    <div id="container4" class="climate-chart" style="min-width: 310px; height: 400px; margin: 0 auto">

                        <div id="climate-chart-widget" data-highcharts-chart="2"><div id="highcharts-4" class="highcharts-container " style="position: relative; overflow: hidden; width: 508px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="508" viewBox="0 0 508 400" height="400"><desc>Created with Highcharts 5.0.2</desc><defs><clipPath id="highcharts-5"><rect x="0" y="0" width="488" height="333" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="508" height="400" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="10" width="488" height="333"></rect><g class="highcharts-grid highcharts-xaxis-grid "></g><g class="highcharts-grid highcharts-yaxis-grid "></g><rect fill="none" class="highcharts-plot-border" x="10" y="10" width="488" height="333"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 10 343.5 L 498 343.5"></path></g><g class="highcharts-axis highcharts-yaxis "><path fill="none" class="highcharts-axis-line" d="M 10 10 L 10 343"></path></g><g class="highcharts-series-group"><g class="highcharts-series highcharts-series-0 highcharts-line-series highcharts-color-0 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-5)" width="333" height="488"></g><g class="highcharts-markers highcharts-series-0 highcharts-line-series highcharts-color-0 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-6)" width="333" height="488"></g><g class="highcharts-series highcharts-series-1 highcharts-line-series highcharts-color-1 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-5)" width="333" height="488"></g><g class="highcharts-markers highcharts-series-1 highcharts-line-series highcharts-color-1 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-6)" width="333" height="488"></g><g class="highcharts-series highcharts-series-2 highcharts-line-series highcharts-color-2 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-5)" width="333" height="488"></g><g class="highcharts-markers highcharts-series-2 highcharts-line-series highcharts-color-2 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-6)" width="333" height="488"></g><g class="highcharts-series highcharts-series-3 highcharts-line-series highcharts-color-3 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-5)" width="333" height="488"></g><g class="highcharts-markers highcharts-series-3 highcharts-line-series highcharts-color-3 " transform="translate(10,10) scale(1 1)" clip-path="url(#highcharts-6)" width="333" height="488"></g></g><g class="highcharts-button highcharts-contextbutton" style="cursor:pointer;" stroke-linecap="round" transform="translate(474,10)"><title>Chart context menu</title><rect fill="#ffffff" class=" highcharts-button-box" x="0.5" y="0.5" width="24" height="22" rx="2" ry="2" stroke="none" stroke-width="1"></rect><path fill="#666666" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" class="highcharts-button-symbol" stroke="#666666" stroke-width="3"></path><text x="0" style="font-weight:normal;color:#333333;fill:#333333;" y="12"></text></g><g class="highcharts-legend" transform="translate(48,355)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="412" height="30" visibility="visible"></rect><g><g><g class="highcharts-legend-item highcharts-line-series highcharts-color-0 highcharts-series-0" transform="translate(8,3)"><path fill="none" d="M 0 11 L 16 11" class="highcharts-graph" stroke="#7cb5ec" stroke-width="2"></path><path fill="#7cb5ec" d="M 8 7 C 13.328 7 13.328 15 8 15 C 2.6719999999999997 15 2.6719999999999997 7 8 7 Z" class="highcharts-point"></path><text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" y="15">Temperature</text></g><g class="highcharts-legend-item highcharts-line-series highcharts-color-1 highcharts-series-1" transform="translate(128.3125,3)"><path fill="none" d="M 0 11 L 16 11" class="highcharts-graph" stroke="#434348" stroke-width="2"></path><path fill="#434348" d="M 8 7 L 12 11 8 15 4 11 Z" class="highcharts-point"></path><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Humidity</text></g><g class="highcharts-legend-item highcharts-line-series highcharts-color-2 highcharts-series-2" transform="translate(226.28125,3)"><path fill="none" d="M 0 11 L 16 11" class="highcharts-graph" stroke="#90ed7d" stroke-width="2"></path><path fill="#90ed7d" d="M 4 7 L 12 7 12 15 4 15 Z" class="highcharts-point"></path><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start"><tspan>Soil Moisture</tspan></text></g><g class="highcharts-legend-item highcharts-line-series highcharts-color-3 highcharts-series-3" transform="translate(346.34375,3)"><path fill="none" d="M 0 11 L 16 11" class="highcharts-graph" stroke="#f7a35c" stroke-width="2"></path><path fill="#f7a35c" d="M 8 7 L 12 15 4 15 Z" class="highcharts-point"></path><text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">Power</text></g></g></g></g><g class="highcharts-axis-labels highcharts-xaxis-labels "></g><g class="highcharts-axis-labels highcharts-yaxis-labels "></g></svg></div></div>				</div>
                </div>
                <div class="col-sm-6">
                    <label>Date:</label>
                    <input id="publishDate" type="text" value="2018-01-25" name="publishDate" class="hasDatepicker"><img class="ui-datepicker-trigger" src="http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" alt="Calendar" title="Calendar">			</div>
                <div class="col-sm-6 climate-table-data">
                    <table class="table table-hover">
                        <thead>
                        <tr><td>Time</td>
                            <td>Temp <br><small>ºC</small></td>
                            <td>Humidity<br><small>%</small></td>
                            <td>Soil Moisture<br><small>%</small></td>
                            <td>Power<br><small>Watt</small></td>
                        </tr></thead>
                        <tbody><tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody></table>			</div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>



    <script type="text/javascript">
        $(document).ready(function () {
            $('#publishDate').on('change', function () {
                $.ajax({
                    url: 'http://localhost/offgrid-bazaar/project/getClimateData',
                    data: {project_id: '35', dt: $(this).val()},
                    method: 'POST',
                    dataType: 'json',
                    success: function (response) {
                        $('div.climate-table-data').html(response.climateTableData);
                        var chart = $('#climate-chart-widget').highcharts();
                        chart.series[0].setData(response.climateChartData.temperature);
                        chart.series[1].setData(response.climateChartData.humidity);
                        chart.series[2].setData(response.climateChartData.soil_moisture);
                    }
                });
            });
        });
    </script>


    <script src="http://localhost/offgrid-bazaar/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn.favourite').on('click', function () {
                var new_value = $(this).hasClass('is_favourite') ? '0' : '1',
                    id = '35';
                $.ajax({
                    url: 'http://localhost/offgrid-bazaar/project/toggle-favourite',
                    data: {id: id, new_value: new_value},
                    method: 'POST',
                    dataType: 'json',
                    success: function (response) {
                        if (response.success)
                            $('.btn.favourite').toggleClass('is_favourite');
                    }
                });
            });
        });
    </script>


</div><!-- page -->
<!-- include bootstrap theme css -->
<link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/theme/bootstrap.min.css')}}">
<!-- include alertify.css -->
<link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/alertify.min.css')}}">
<!-- include alertify script -->
<script src="{{asset('/offgrid-bazaar/js/alertify/alertify.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        alertify.defaults.theme.ok = "btn btn-primary";
        alertify.defaults.theme.cancel = "btn btn-danger";
        alertify.defaults.theme.input = "form-control";
        $('#logout').click(function (e) {
            alertify.confirm('Offgrid Bazar', 'Are you sure you want to logout?', function () {
                $.ajax({
                    url: '/offgrid-bazaar/logout',
                    method: 'POST',
                    dataType: 'json',
                    success: function (resp) {
                        location.reload();
                    }
                });
            }, function () {
            });
        });

        $('.nav-link').on('click', function () {
            $.ajax({
                url: '/offgrid-bazaar/project/getProjectList',
                method: 'POST',
                data: {project_type: $(this).text()},
                success: function (resp) {
                    $('div.projects').html(resp);
                }

            });
        });
    });
</script>

<script type="text/javascript" src="/offgrid-bazaar/assets/9bb1aea1/jui/js/jquery-ui.min.js"></script>
<script type="text/javascript">
    /*<![CDATA[*/
    jQuery(function ($) {
        jQuery('#publishDate').datepicker({
            'showAnim': 'fold',
            'showOn': 'both',
            'dateFormat': 'yy\x2Dmm\x2Ddd',
            'buttonImageOnly': true,
            'buttonImage': 'http\x3A\x2F\x2Fjqueryui.com\x2Fresources\x2Fdemos\x2Fdatepicker\x2Fimages\x2Fcalendar.gif',
            'buttonText': 'Calendar'
        });
    });
    jQuery(window).on('load', function () {
        Highcharts.setOptions([]);
        var chart = new Highcharts.Chart({
            'chart': {'renderTo': 'crop\x2Dyield\x2Dchart\x2Dwidget', 'type': 'column'},
            'credits': {'enabled': false},
            'title': {'text': ''},
            'subtitle': {'text': ''},
            'xAxis': {
                'crosshair': true,
                'categories': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            'yAxis': {'min': 0, 'title': {'text': 'Yield\x20\x28kg\x29'}},
            'tooltip': {
                'headerFormat': '\x3Cspan\x20style\x3D\x22font\x2Dsize\x3A10px\x22\x3E\x7Bpoint.key\x7D\x3C\x2Fspan\x3E\x3Ctable\x3E',
                'pointFormat': '\x3Ctr\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22color\x3A\x7Bseries.color\x7D\x3Bpadding\x3A0\x22\x3E\x7Bseries.name\x7D\x3A\x20\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22padding\x3A0\x22\x3E\x3Cb\x3E\x7Bpoint.y\x3A.1f\x7D\x20\x3C\x2Fb\x3E\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x3C\x2Ftr\x3E',
                'footerFormat': '\x3C\x2Ftable\x3E',
                'shared': true,
                'useHTML': true
            },
            'series': [{'name': 'Paddy', 'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7000, 0]}, {
                'name': 'Maize\x2Fgrain',
                'data': [0, 0, 2200, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }, {'name': 'Tomato', 'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000]}, {
                'name': 'Tomato',
                'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50000]
            }, {'name': 'Tomato', 'data': [0, 50000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}, {
                'name': 'Tomato',
                'data': [0, 0, 50000, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }, {'name': 'Potato', 'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 28000]}]
        });
        Highcharts.setOptions([]);
        var chart = new Highcharts.Chart({
            'chart': {'renderTo': 'yw0', 'type': 'column'},
            'credits': {'enabled': false},
            'title': {'text': 'Monthly\x20Net\x20Income\x20Projection'},
            'subtitle': {'text': ''},
            'xAxis': {
                'crosshair': true,
                'categories': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            'yAxis': {'min': 0, 'title': {'text': 'Income\x20\x28Rs.\x29'}},
            'tooltip': {
                'headerFormat': '\x3Cspan\x20style\x3D\x22font\x2Dsize\x3A10px\x22\x3E\x7Bpoint.key\x7D\x3C\x2Fspan\x3E\x3Ctable\x3E',
                'pointFormat': '\x3Ctr\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22color\x3A\x7Bseries.color\x7D\x3Bpadding\x3A0\x22\x3E\x7Bseries.name\x7D\x3A\x20\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22padding\x3A0\x22\x3E\x3Cb\x3ERs.\x20\x7Bpoint.y\x3A.1f\x7D\x20\x3C\x2Fb\x3E\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x3C\x2Ftr\x3E',
                'footerFormat': '\x3C\x2Ftable\x3E',
                'shared': true,
                'useHTML': true
            },
            'plotOptions': {'column': {'pointPadding': 0.2, 'borderWidth': 0}},
            'series': [{
                'name': 'Net\x20Income',
                'data': [95000, 114800, 0, 0, 0, 0, 0, 0, 0, 46830, 95000, 137000],
                'type': 'column'
            }]
        });
        Highcharts.setOptions([]);
        var chart = new Highcharts.Chart({
            'chart': {'renderTo': 'climate\x2Dchart\x2Dwidget'},
            'credits': {'enabled': false},
            'title': {'text': ''},
            'subtitle': {'text': ''},
            'xAxis': {
                'crosshair': true,
                'categories': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            'yAxis': {'min': 0, 'title': {'text': ''}},
            'tooltip': {
                'headerFormat': '\x3Cspan\x20style\x3D\x22font\x2Dsize\x3A10px\x22\x3E\x7Bpoint.key\x7D\x3C\x2Fspan\x3E\x3Ctable\x3E',
                'pointFormat': '\x3Ctr\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22color\x3A\x7Bseries.color\x7D\x3Bpadding\x3A0\x22\x3E\x7Bseries.name\x7D\x3A\x20\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x09\x3Ctd\x20style\x3D\x22padding\x3A0\x22\x3E\x3Cb\x3E\x7Bpoint.y\x3A.1f\x7D\x20\x3C\x2Fb\x3E\x3C\x2Ftd\x3E\x0D\x0A\x09\x09\x09\x20\x20\x20\x09\x09\x09\x09\x09\x3C\x2Ftr\x3E',
                'footerFormat': '\x3C\x2Ftable\x3E',
                'shared': true,
                'useHTML': true
            },
            'series': [{'name': 'Temperature', 'data': [], 'type': 'line'}, {
                'name': 'Humidity',
                'data': [],
                'type': 'line'
            }, {'name': 'Soil\x20Moisture', 'data': [], 'type': 'line'}, {'name': 'Power', 'data': [], 'type': 'line'}]
        });
    });
    /*]]>*/
</script>
</body>
</html>
