@extends('ogbadmin.layout')
@section('title')
    Investments
@endsection
@section('styles')
    <style>
    .enclosing-items,.enclosing-items-right{
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    }
    .enclosing-items span,.enclosing-items-right span{
    background: white;
    padding: 8px;
    border: 1px solid #ced4da;
    margin: 0!important;
    }
    .enclosing-items span,.enclosing-items-right input{
    border-right: none;
    border-radius: .25rem 0 0 .25rem;
    }
    .enclosing-items input, .enclosing-items-right span{
    border-left: none;
    border-radius: 0 .25rem .25rem 0;
    }
    </style>
    @endsection
@section('content')

    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
                <div class="container-fluid">
                    <div class="row flex-xl-nowrap align-items-center">
                        <div class="col-md-6">
                            <h3 class="mb-0">Investments</h3>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a href="#investment-modal" class="btn btn-primary" data-toggle="modal">Make an Investment</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-lg-4 col-sm-6 mb-sm-4">
                            <div class="info-card card-orange d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>Rs.938k</h4>
                                    <div class="text-muted">Asset Under Management</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mb-sm-4">
                            <div class="info-card card-blue d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>Rs.600k</h4>
                                    <div class="text-muted">My Investment</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mb-sm-4">
                            <div class="info-card card-green d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>Rs.1000k</h4>
                                    <div class="text-muted">My Returns</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-lg-4 mb-sm-4">
                            <div class="info-card card-grey d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>Rs.200k</h4>
                                    <div class="text-muted">Funding Gap</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-lg-4 mb-sm-4">
                            <div class="info-card card-grey d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>24%</h4>
                                    <div class="text-muted">Average Default Rate</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-lg-4 mb-sm-4">
                            <div class="info-card card-grey d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>16</h4>
                                    <div class="text-muted">Average Default Days</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-lg-6 col-sm-12">
                            <div class="text-muted mb-4">Investements</div>
                            <canvas id="project_investments" class="col_canvas"></canvas>
                        </div>
                        <div class="col-lg-6 col-sm-12 mt-sm-4">
                            <div class="text-muted mb-4">Returns</div>
                            <canvas id="project_returns" class="col_canvas"></canvas>
                        </div>
                    </div>

                    <div class="row flex-xl-nowrap">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="filter-bar">
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="row">
                                                <div class="col">
                                                    <button class="btn btn-filter d-flex justify-content-between align-items-center" type="button" data-toggle="collapse" data-target="#filterDropdown" role="button" aria-expanded="false" aria-controls="filterDropdown">
                                                        <span class="mr-5">Filter by:</span>
                                                        <i class="icon ion-md-funnel"></i>
                                                    </button>
                                                    <div class="filter-dropdown collapse" id="filterDropdown">
                                                        <form action="">
                                                            <div class="row">
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Location</strong>
                                                                    <select class="custom-select" id="inputGroupSelect01">
                                                                        <option selected>Provience</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                    </select>

                                                                    <select class="custom-select mt-3" id="inputGroupSelect01">
                                                                        <option selected>District</option>
                                                                        <option value="">Achham</option>
                                                                        <option value="">Arghakhanchi</option>
                                                                        <option value="">Baglung</option>
                                                                        <option value="">Baitadi</option>
                                                                        <option value="">Bajhang</option>
                                                                        <option value="">Bajura</option>
                                                                        <option value="">Banke</option>
                                                                        <option value="">Bara</option>
                                                                        <option value="">Bardiya</option>
                                                                        <option value="">Bhaktapur</option>
                                                                        <option value="">Bhojpur</option>
                                                                        <option value="">Chitwan</option>
                                                                        <option value="">Dadeldhura</option>
                                                                        <option value="">Dailekh</option>
                                                                        <option value="">Darchula</option>
                                                                        <option value="">Dhading</option>
                                                                        <option value="">Dhankutta</option>
                                                                        <option value="">Dhanusa</option>
                                                                        <option value="">Dolkha</option>
                                                                        <option value="">Dolpa</option>
                                                                        <option value="">Doti</option>
                                                                        <option value="">Ghorahi</option>
                                                                        <option value="">Gorkha</option>
                                                                        <option value="">Gulmi</option>
                                                                        <option value="">Humla</option>
                                                                        <option value="">Ilam</option>
                                                                        <option value="">Jajarkot</option>
                                                                        <option value="">Jhapa</option>
                                                                        <option value="">Jumla</option>
                                                                        <option value="">Kailali</option>
                                                                        <option value="">Kalikot</option>
                                                                        <option value="">Kanchanpur</option>
                                                                        <option value="">Kaski</option>
                                                                        <option value="">Kathmandu</option>
                                                                        <option value="">Kavreplanchauk</option>
                                                                        <option value="">Khotang</option>
                                                                        <option value="">Lalitpur</option>
                                                                        <option value="">Lamjung</option>
                                                                        <option value="">Mahottari</option>
                                                                        <option value="">Makwanpur</option>
                                                                        <option value="">Manag</option>
                                                                        <option value="">Morang</option>
                                                                        <option value="">Mugu</option>
                                                                        <option value="">Mustang</option>
                                                                        <option value="">Myagdi</option>
                                                                        <option value="">Nawalpur</option>
                                                                        <option value="">Nuwakot</option>
                                                                        <option value="">Okhaldunga</option>
                                                                        <option value="">Palpa</option>
                                                                        <option value="">Panchthar</option>
                                                                        <option value="">Parasi</option>
                                                                        <option value="">Parsa</option>
                                                                        <option value="">Parwat</option>
                                                                        <option value="">Pyuthan</option>
                                                                        <option value="">Ramechhap</option>
                                                                        <option value="">Rasuwa</option>
                                                                        <option value="">Rauthat</option>
                                                                        <option value="">Rolpa</option>
                                                                        <option value="">Rukum</option> Paschim
                                                                        <option value="">Rukum</option> Purba
                                                                        <option value="">Rupandehi</option>
                                                                        <option value="">Salyan</option>
                                                                        <option value="">Sankhuwasabha</option>
                                                                        <option value="">Saptari</option>
                                                                        <option value="">Sarlahi</option>
                                                                        <option value="">Sindhuli</option>
                                                                        <option value="">Sindhupalchauk</option>
                                                                        <option value="">Siraha</option>
                                                                        <option value="">Solukhumbu</option>
                                                                        <option value="">Sunsari</option>
                                                                        <option value="">Surkhet</option>
                                                                        <option value="">Syangja</option>
                                                                        <option value="">Tanahun</option>
                                                                        <option value="">Taplejung</option>
                                                                        <option value="">Taulihawa</option>
                                                                        <option value="">Terhathum</option>
                                                                        <option value="">Udaypur</option>
                                                                    </select>

                                                                </div>
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Size</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck1">
                                                                        <label class="form-check-label" for="sizeCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            1 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck2">
                                                                        <label class="form-check-label" for="sizeCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            1.5 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck3">
                                                                        <label class="form-check-label" for="sizeCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            2 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck4">
                                                                        <label class="form-check-label" for="sizeCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Small
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Category</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck1">
                                                                        <label class="form-check-label" for="categoryCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Fish
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck2">
                                                                        <label class="form-check-label" for="categoryCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Vegetables
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck3">
                                                                        <label class="form-check-label" for="categoryCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Crops
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck4">
                                                                        <label class="form-check-label" for="categoryCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Mix
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Project Status</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck1">
                                                                        <label class="form-check-label" for="projectStatusCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            New
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck2">
                                                                        <label class="form-check-label" for="projectStatusCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Approved
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck3">
                                                                        <label class="form-check-label" for="projectStatusCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Operational
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck4">
                                                                        <label class="form-check-label" for="projectStatusCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Error
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-between">
                                                                <div class="col-lg col-sm-4">
                                                                    <button type="reset" class="btn btn-outline-danger">Clear all Filters</button type="reset">                                                    </div>
                                                                <div class="col-lg col-sm-8 text-right">
                                                                    <button class="btn btn-outline-primary mr-3" data-toggle="collapse" data-target="#filterDropdown" role="button" aria-expanded="false" aria-controls="filterDropdown">Cancel</button>
                                                                    <button class="btn btn-primary">Apply Filter</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-sm-12 mt-lg-0 mt-sm-3">
                                            <div class="row d-flex justify-content-end">
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group filter-search">
                                                        <input type="text" class="form-control" placeholder="Search...">
                                                        <button class="btn btn-link"><i class="icon ion-md-search"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <select class="custom-select" id="inputGroupSelect01">
                                                        <option selected>Sort by</option>
                                                        <option value="1">Sort A - Z</option>
                                                        <option value="2">Sort Z - A</option>
                                                        <option value="3">Recent Projects</option>
                                                        <option value="4">Old Projects</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row flex-xl-nowrap">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 table-dashboard">
                                    <thead>
                                    <tr>
                                        <th scope="col">Payment ID</th>
                                        <th scope="col">System Size</th>
                                        <th scope="col">Farmer Name</th>
                                        <th scope="col">My Investments</th>
                                        <th scope="col">My Returns</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($projects as $project)
                                    <tr>
                                        <th scope="col">{{$project->payment_id}}</th>
                                        <td>2hp</td>
                                        <td class="text-capitalize">{{ $project->farmer_name }}
                                            <div class="additional-info">
                                                <div class="location"><i class="icon ion-md-pin"></i>{{ $project->district->name }}</div>
                                                <div class="contact-no"><i class="icon ion-md-call"></i>{{ $project->farmers()->first()->contact_no }}</div>
                                                <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">{{$project->farmers()->first()->credit_score? round($project->farmers()->first()->credit_score,2) : 0 }}</div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <p class="mb-2">Rs.2,000 of Rs.3,500</p>
                                                <div class="progress">
                                                    <span class="progress-blue" style="width:10%;"></span>
                                                    <span class="progress-purple" style="width:30%;"></span>
                                                    <span class="progress-yellow" style="width:15%;"></span>
                                                </div>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                Gham Power <span class="d-block text-muted">Rs.1,500 of <strong>Rs.5,000</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-blue" style="width:10%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                My Investment <span class="d-block text-muted">Rs.500 of <strong>Rs.5,000</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-purple" style="width:30%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                <div class="info">
                                                                    Other Investment <span class="d-block text-muted">Rs.750 of <strong>Rs.5,000</strong></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-yellow" style="width:15%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                <div class="info">
                                                                    Funding Gap <span class="d-block text-muted">Rs.1,600 of <strong>Rs.5,000</strong></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-grey" style="width:45%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <p class="mb-2">Rs.2,500</p>
                                                <div class="progress">
                                                    <span class="progress-green" style="width:34%;"></span>
                                                    <span class="progress-orange" style="width:33%;"></span>
                                                    <span class="progress-red" style="width:33%;"></span>
                                                </div>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                Total Recovered <span class="d-block text-muted">Rs.1,500 of <strong>Rs.5,000</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-green" style="width:34%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                Expected to Recover <span class="d-block text-muted">Rs.500 of <strong>Rs.5,000</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-orange" style="width:33%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                <div class="info">
                                                                    Risky to Recover <span class="d-block text-muted">Rs.750 of <strong>Rs.5,000</strong></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-red" style="width:33%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    {{--<tr>--}}
                                        {{--<th scope="col">190002</th>--}}
                                        {{--<td>2hp</td>--}}
                                        {{--<td>Lokman Parajuli--}}
                                            {{--<div class="additional-info">--}}
                                                {{--<div class="location"><i class="icon ion-md-pin"></i>Dhading</div>--}}
                                                {{--<div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>--}}
                                                {{--<div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,000 of $3,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-blue" style="width:10%;"></span>--}}
                                                    {{--<span class="progress-purple" style="width:30%;"></span>--}}
                                                    {{--<span class="progress-yellow" style="width:15%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-blue" style="width:10%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-purple" style="width:30%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-yellow" style="width:15%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Funding Gap <span class="d-block text-muted">$1,600 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-grey" style="width:45%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}

                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-green" style="width:34%;"></span>--}}
                                                    {{--<span class="progress-orange" style="width:33%;"></span>--}}
                                                    {{--<span class="progress-red" style="width:33%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Total Recovered <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-green" style="width:34%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Expected to Recover <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-orange" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Risky to Recover <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-red" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<th scope="col">190003</th>--}}
                                        {{--<td>2hp</td>--}}
                                        {{--<td>Lokman Parajuli--}}
                                            {{--<div class="additional-info">--}}
                                                {{--<div class="location"><i class="icon ion-md-pin"></i>Dhading</div>--}}
                                                {{--<div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>--}}
                                                {{--<div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,000 of $3,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-blue" style="width:10%;"></span>--}}
                                                    {{--<span class="progress-purple" style="width:30%;"></span>--}}
                                                    {{--<span class="progress-yellow" style="width:15%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-blue" style="width:10%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-purple" style="width:30%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-yellow" style="width:15%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Funding Gap <span class="d-block text-muted">$1,600 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-grey" style="width:45%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}

                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-green" style="width:34%;"></span>--}}
                                                    {{--<span class="progress-orange" style="width:33%;"></span>--}}
                                                    {{--<span class="progress-red" style="width:33%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Total Recovered <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-green" style="width:34%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Expected to Recover <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-orange" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Risky to Recover <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-red" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<th scope="col">190004</th>--}}
                                        {{--<td>2hp</td>--}}
                                        {{--<td>Lokman Parajuli--}}
                                            {{--<div class="additional-info">--}}
                                                {{--<div class="location"><i class="icon ion-md-pin"></i>Dhading</div>--}}
                                                {{--<div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>--}}
                                                {{--<div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,000 of $3,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-blue" style="width:10%;"></span>--}}
                                                    {{--<span class="progress-purple" style="width:30%;"></span>--}}
                                                    {{--<span class="progress-yellow" style="width:15%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-blue" style="width:10%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-purple" style="width:30%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-yellow" style="width:15%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Funding Gap <span class="d-block text-muted">$1,600 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-grey" style="width:45%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}

                                        {{--<td>--}}
                                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                                {{--<p class="mb-2">$2,500</p>--}}
                                                {{--<div class="progress">--}}
                                                    {{--<span class="progress-green" style="width:34%;"></span>--}}
                                                    {{--<span class="progress-orange" style="width:33%;"></span>--}}
                                                    {{--<span class="progress-red" style="width:33%;"></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Total Recovered <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-green" style="width:34%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--Expected to Recover <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-orange" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                        {{--<div class="row d-flex align-items-center">--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="info">--}}
                                                                    {{--Risky to Recover <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-6">--}}
                                                                {{--<div class="progress">--}}
                                                                    {{--<span class="progress-red" style="width:33%"></span>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
    @include('ogbadmin.investment.includes.create')


@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            var projectInvestments = document.getElementById('project_investments');
            var projectReturns = document.getElementById('project_returns');
            projectInvestments.height = 70;
            projectReturns.height = 70;

            var project_investments = new Chart(projectInvestments,{
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: ['29392.34','23214.29','8499.24','6439.24'],
                        backgroundColor: ["#03A9F4", "#AA95D0", "#FFB300", "#C0CCDA"]
                    }],
                    labels: ["Gham Power","My Investment","Other", "Gap"]
                },
                options: {
                    legend: {
                        position: "right",
                        fontSize: 14,
                        labels: {
                            fontSize: 14
                        },
                        onClick: null
                    }
                }
            });
            var project_returns = new Chart(projectReturns,{
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: ['29392.34','23214.29','8499.24'],
                        backgroundColor: ["#66BB6A", "#FF8C63", "#EF5350"]
                    }],
                    labels: ["Total Recovered","Expected to Recover","Risky to Recover"]
                },
                options: {
                    legend: {
                        position: "right",
                        fontSize: 14,
                        labels: {
                            fontSize: 14
                        },
                        onClick: null
                    }
                }
            });

        });
    </script>
    <script>
        var projects = {!! json_encode($projects) !!}

        $("#farmer").select2({
            templateResult: formatState,
            width: '100%'

        });
        function formatState (opt) {
            if (!opt.id) {
                return opt.text;
            }
            var optimage = $(opt.element).data('image');
            if(!optimage){
                return opt.text;
            } else {
                var $opt = $(
                    '<div class="dropdown-item d-flex"><div class="user-thumb">' +
                    '<img src="'+optimage+'" class="user-thumbnail" alt="">'+
                    '</div>' +
                    '<div class="d-flex flex-column">'+
                    '<div class="user-name mb-1"><strong>'+$(opt.element).text()+'</strong></div>'+
                    '<div class="d-flex">'+
                    '<div class="user-id mr-2"><strong class="mr-2">#</strong>'+$(opt.element).val()+'</div>'+
                    '<div class="user-location"><i class="icon ion-md-pin mr-2"></i>'+$(opt.element).data('farmer-address')+'</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
                );
                return $opt;
            }
        }

        $('#farmer').on("change", function(e) {
            //creating  the div with project details
            var payment_id = $("#farmer option:selected").val();
            var project = projects.find(function(project){
                return project.payment_id === payment_id
            });
            var html =
                '<div class="user-details" id = "details">' +
                '<div class="trash-item clear"><i class="icon ion-md-trash" id="trash-icon"></i></div>' +
                '<div class="user-info">' +
                '<span>Payment ID</span>' + payment_id+
                '</div>' +
                '<div class="user-info">' +
                '<span>Farmer Name</span>'+ project.farmer_name +
                '</div>' +
                '<div class="user-info">' +
                '<span>System Size</span>'+ project.solar_pv_size +'W' +
                '</div>' +
                '<div class="user-info">' +
                '<span>Location</span>'+project.municipality.name + ','+ project.district.name+
                '</div>' +
                '</div>'
            $('#project-detail').html(html).show();
        });
        $('#project-detail').on('click', '#trash-icon', function() {
            $('#investment-form').trigger('reset');
            $('#project-detail').hide();
            $('#farmer').val('').trigger('change');
        });
        </script>
        <script type="text/javascript">
        @if (count($errors) > 0)
            $('#investment-modal').modal('show');
        @endif
        </script>

        <script>
            $('#investment-submit').click(function () {
                $('#investment-form').submit();
            })
        </script>

    <script>
        $("#investment-modal form").validate({
            rules: {
                payment_id: {
                    required: true,

                },
                invest_amount: {
                    required: true,
                    minlength: 1,
                    maxlength: 10

                },
                term: {
                    required: true,
                    maxlength: 5
                },
                interest_rate: {
                    required: true,
                    maxlength: 5
                },

            },
        });
    </script>

@endsection