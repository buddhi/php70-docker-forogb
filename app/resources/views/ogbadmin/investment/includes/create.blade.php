<div class="modal fade" id="investment-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">
            <div class="row  justify-content-center">
                <div class="col-12 text-center mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3>Make an Investment</h3>
                </div>

                <div class="col-12">
                    <form action="{{route('ogbadmin.investment.store')}}" method="post" id="investment-form" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="hidden"  name="payment_gateway" class="form-control" id="payment_gateway" value="cash">
                        <input type="hidden"  name="investment_type_id" class="form-control" id="investment_type_id" value="{{\App\Enums\InvestmentType::Debt}}">
                        <div class="form-group auto-search-group">
                            <label><span>Payment ID or Name: <span class="required">*</span></span>
                                <select id="farmer" class="form-control" required="required" name="payment_id">
                                    <option ></option>
                                    @foreach( $projects as $project)
                                        <option value="{{isset($project->payment_id)?$project->payment_id:null}}"
                                                data-image="{{isset($project->image)? asset($project->image):asset('img/farmer_default.png')}}"
                                                data-farmer-address = "{{isset($project->farmers->first()->address)? $project->farmers->first()->address:null}}"
                                        >
                                            {!! $project->farmer_name!!}
                                        </option>
                                    @endforeach
                                </select>
                            </label>
                            @if ($errors->has('payment_id'))
                                <span class="error">
                                    <p class="text-danger">{{ $errors->first('payment_id') }}</p>
                                </span>
                            @endif
                        </div>
                        <div id="project-detail">
                        </div>

                        <div class="form-group">
                            <label for="amount"><span>Amount to Invest<span class="required">*</span></span>
                                <div class="enclosing-items"><span>Rs. </span><input type="number"  name="invest_amount" class="form-control" id="amount" required value="{{old('invest_amount')}}"></div>
                                @if ($errors->has('invest_amount'))
                                    <span class="error">
                                        <p class="text-danger">{{ $errors->first('invest_amount') }}</p>
                                    </span>
                                @endif
                            </label>

                        </div>

                        <div class="form-group">
                            <label for="interest_rate"><span>Interest Rate</span>
                                <div class="enclosing-items-right"><input type="number"  name="interest_rate" class="form-control" id="interest_date" value="{{old('interest_rate')}}"><span>(%)</span></div>
                                @if ($errors->has('interest_rate'))
                                    <span class="error">
                                        <p class="text-danger">{{ $errors->first('interest_rate') }}</p>
                                    </span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group">
                            {{--<label for="term"><span>Term(years)</span>--}}
                            <label for="term"><span>Term(month)</span>
                                <div class="enclosing-items-right"><input type="number"  name="term" class="form-control" id="term"  value="{{old('term')}}"><span>(month)</span></div>
                                @if ($errors->has('term'))
                                    <span class="error">
                                        <p class="text-danger">{{ $errors->first('term') }}</p>
                                    </span>
                                @endif
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="button"
                                    id="investment-submit">Make Investment</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
