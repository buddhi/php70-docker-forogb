@extends('ogbadmin.layout')
@section('title')
    Partner Listing
@endsection
@section('content')
<main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
    <div class="container-fluid">
        <div class="row flex-xl-nowrap align-items-center">
            <div class="col-md-6">
                <h3 class="mb-0">Partners</h3>
            </div>
            <div class="col-md-6 d-flex justify-content-end">
                <button type="button" class="btn btn-primary add-partner" data-toggle="modal"
                        data-target="#partner-verify">Add a
                    Partner</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-2">
                <hr>
            </div>
        </div>
        <div class="row flex-xl-nowrap">
            <hr class="mb-3">
            <div class="col-md-12">
                <table class="table table-wrapper">
                    <thead>
                    <tr>
                        <th scope="col">Name of Organization</th>
                        <th scope="col">Contact Person</th>
                        <th scope="col">Organization Email Address</th>
                        <th scope="col">Status</th>
                        <th scope="col" class="text-center">Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($partners->count() > 0)
                        @foreach($partners as $partner)
                            <tr>
                                {{--TODO::future implement image of farmer--}}
                                {{-- <th scope="col"><img src="../img/user-thumb.jpg" class="thumbnail" alt="">Example Project #1</th>--}}
                                <th scope="col" class="text-capitalize"><span class="thumbnail text-center text-white">E</span>{{$partner->name}}</th>
                                <td class="text-capitalize">{{$partner->contact_person}}</td>
                                <td>{{$partner->email}}</td>
                                <td>
                                    <div class="status {{$partner->status}}">{{ucfirst($partner->status)}}</div>
                                </td>
                                <td>
                                    <div class="action d-flex justify-content-end">
                                        <div class="d-flex align-items-center">
                                            @if(!$partner->verified)
                                                <div class="resend-email" data-toggle="tooltip" data-placement="top" title="Resend Verification Email">
                                                    <a href="#" data-target="#resend-verification"  data-toggle="modal" data-whatever="{{$partner->email}}" data-target="#resend-verification">
                                                        <i class="icon ion-md-mail"></i>
                                                    </a>
                                                </div>
                                            @endif
                                            <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Partner">
                                                <a href="{{route('ogbadmin.partner.edit', $partner->id)}}">
                                                    <i class="icon ion-md-create"></i>
                                                </a>
                                            </div>
                                            <div class="checkbox-container"  data-toggle="toggletip" data-placement="top" title="{{ $partner->disabled? "Enable": "Disable"}} Partner" id ="abc">
                                                <input type="checkbox" id="toggle-{{$partner->id}}" class="toggle-partner" attr = "{{$partner->id}}" {{ $partner->disabled? "": "checked"}} />
                                                <label for="toggle-{{$partner->id}}"></label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="5" class="pagination-td">{!! $partners->links('pagination::bootstrap-4') !!}</td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="5">No data found.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@include('ogbadmin.partner.verify')
@include('ogbadmin.partner.resend-verification')
@endsection
@section('js')
<script src="{{asset('js/ogbadmin/partner/partner-dashboard.js')}}"></script>
@endsection