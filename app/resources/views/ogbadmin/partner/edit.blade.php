@extends('ogbadmin.layout')
@section('title')
Edit  Partner
@endsection
@section('styles')
    <style>

        #pop-message {
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            bottom: 80vh;
            transform: translateY(50vh);
            left: 67vw;
            height: 55px;
            width: 30vw;
            border-radius: 5px;
            box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.2);
            padding: 8px 25px;
            /*border-radius: 25px;*/
            z-index: 9;
            opacity: 0;
            transition: .3s;
        }

        #pop-message.popup-appear {
            opacity: 1;
            transform: translateY(0);
        }

        .success-popup {
            color: #155724;
            background-color: #D4EDDA;
            border-color: #C3E6CB;
        }

        .error-popup {
            /*color: #856404;*/
            /*background-color: #FFF3CD;*/
            /*border-color: #FFEEBA;*/
            color: #721C24;
            background-color: #F8D7DA;
            border-color: #F5C6CB;
        }

        .title-popup {
            font-size: 16px;
        }

        @keyframes appearPopUp {
            from {
                opacity: 0;
                transform: translateX(300px);
            }
            to {
                transform: translateX(0);
                opacity: 1;
            }
        }
        </style>
    @endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
        <div id="pop-message">
            <span class="title-popup"></span>
        </div>
        <div class="container-fluid">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0">Edit Partner</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="card">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <span class="text-muted d-block mb-1">Partner Name</span>
                                <strong>{{$partner->name}}</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="text-muted d-block mb-1">Contact Person</span>
                                <strong>{{$partner->contact_person}}</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="text-muted d-block mb-1">Email Address</span>
                                <strong>{{$partner->email}}</strong>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-md-6">
                    <form action="{{route('ogbadmin.partner.update', $partner->id)}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="thumb-upload-wrapper">
                                <label class= "text-center">Partner Logo</label>
                                <div class="profile-upload rounded-circle browse">
                                    <img src="{{isset($partner->partner_logo)? '/storage/upload/'.$partner->partner_logo: '/img/farmer_default.png'}}"  accept="image/*" class="avatar image-change" alt="offgridbazaar user"  id="preview_image"/>
                                    <span class="text-center image-change">Upload</span>
                                    <input type="file" class="rounded-circle"  id="file" data-id ="{{$partner->id}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><span>Address <span class="required">*</span></span>
                                <textarea type="text" name="address" class="form-control" rows="5" >{!! old('address',isset($partner->address)? $partner->address:'') !!}</textarea>
                            </label>
                            @if ($errors->has('address'))
                                <p class="text-danger">{{ $errors->first('address') }}</p>
                            @endif
                        </div>

                        <div class="form-group">
                            <label><span>Mobile Number <span class="required">*</span></span>
                                <input type="text" name="phone" class="form-control" placeholder="Mobile Number" value="{{ old('phone',isset($partner->phone)? $partner->phone:'') }}">
                            </label>
                            @if ($errors->has('phone'))
                                <p class="text-danger">{{ $errors->first('phone') }}</p>
                            @endif
                        </div>

                        <div class="form-group">
                            <label><span>Landline Number</span>
                                <input type="text" name="landline" class="form-control" placeholder="Landline Number" value="{{ old('landline',isset($partner->landline)? $partner->landline:'') }}">
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Submit Changes</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="{{asset('js/ogbadmin/partner/partner-dashboard.js')}}"></script>
@endsection