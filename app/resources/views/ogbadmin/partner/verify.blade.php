<div class="modal fade" id="partner-verify" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">
            <section id="invite-partner" class="row  justify-content-center invite-partner-section">
                <div class="col-12 text-center mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3>Invite a Partner</h3>
                </div>
                <div class="alert alert-danger error-form" id="add-partner-error-bag">
                    <ul id="add-form-errors"></ul>
                </div>

                <div class="col-12">
                    <div class="col-12">
                        {!! Form::open(['class' => 'form-horizontal','id'=> 'add_partner', 'enctype' => "multipart/form-data"]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name of Partner:') !!}
                            {!! Form::text('name', null, ['id' =>'name','class' => 'form-control','placeholder' => 'Partner or Unit Name','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('contact_person', 'Contact Person:') !!}
                            {!! Form::text('contact_person', null, [ 'id' =>'contact_person','class' => 'form-control','placeholder' => 'Enter the contact person of organisation','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Organization Email Address:') !!}
                            {!! Form::text('email', null, ['id'=> 'email','class' => 'form-control','placeholder' => 'Enter email address']) !!}
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-block btn-primary verification-email" rel="verification-success">Send Verification Email</button>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@include('ogbadmin.email-verification-popup')