<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Login</title>
</head>

<body>

<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>

@if (session()->has('error'))
    <div class="text-center">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{!! session()->get('error') !!} !!</strong>
        </div>
    </div>

@endif
@if (session()->has('success'))
    <div class="text-center">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{!! session()->get('success') !!} !!</strong>
        </div>
    </div>
@endif
<div class="container-fluid">

    <div class="row">
        <main class="col-12 col-md-12 col-xl-12 bd-content">

            <div class="box-wrap verify-mobile-number">

                <form class="form-row" action="{{route('changePassword')}}" method = "post">
                    {{csrf_field()}}
                    <div class="col-md-12 text-center">
                        <h4>Change Password</h4>
                    </div>
                    <div class="col-md-12 mt-4 form-group">
                        <label for="current-password"> Current Password</label>
                        <input class="form-control text-left" type="password" name="current-password" data-validetta="required">
                        @if ($errors->has('current-password'))
                            <span class="error">
                                <p>{{ $errors->first('current-password') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="new-password"> New Password</label>
                        <input class="form-control text-left" type="password" name="new-password" data-validetta="required">
                        @if ($errors->has('new-password'))
                            <span class="error">
                                <p>{{ $errors->first('new-password') }}</p>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="new-password">Confirm new Password</label>
                        <input class="form-control text-left" type="password" name="new-password_confirmation" data-validetta="required">
                        @if ($errors->has('new-password_confirmation'))
                            <span class="error">
                                <p>{{ $errors->first('new-password_confirmation') }}</p>
                            </span>
                        @endif
                    </div>
                    <div>
                        <div class="col-md-12 mt-4">
                            <button type="submit" class="btn btn-primary">
                                Change Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
</body>

</html>



