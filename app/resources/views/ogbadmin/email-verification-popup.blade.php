<div class="modal fade" id="verification-email-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">
            <section id="verification-success">
                <div class="col-12 text-center mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <img class="mb-3" src="{{asset('img/sent-mail.svg')}}" alt="">
                        <p class="mb-0"> Verification email has been sent to <br><span id ="email-text"></span></p>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>