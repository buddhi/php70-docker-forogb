<table class="table table-wrapper">
    <thead>
    <tr>
        <th scope="col">Name </th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">Status</th>
        <th scope="col" class="text-center">Action
        </th>
    </tr>
    </thead>
    <tbody>
    @if ($agents->count() > 0)
        @foreach($agents as $agent)
            <tr>
                <th scope="col">
                    @if(isset($agent->profile_image))
                        <img src="{{'/storage/upload/'.$agent->profile_image}}" class="user-thumbnail" alt="">
                    @else
                        <span class="user-thumbnail text-center text-white text-capitalize">
                                            {!!  isset($agent->full_name[0])? $agent->full_name[0]:"A"!!}
                                        </span>
                    @endif
                    <span class="text-capitalize">{{$agent->full_name}}</span>
                </th>
                <td>{{$agent->email}}</td>
                <td>{{$agent->phone}}</td>
                <td>
                    <div class="status {{$agent->status}}">{{ucfirst($agent->status)}}</div>
                </td>
                <td>
                    <div class="action d-flex justify-content-end">
                        <div class="d-flex align-items-center">
                            {{--<div class="resend-email" data-toggle="tooltip" data-placement="top" title="Resend Verification Email">--}}
                                {{--<a href="#" data-target="#resend-verification"  data-toggle="modal" data-whatever="{{$agent->email}}" data-target="#resend-verification">--}}
                                    {{--<i class="icon ion-md-mail"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Agent">
                                <a href="{{route('ogbadmin.agent.edit',['date'=>$agent->user_id])}}">
                                    <i class="icon ion-md-create"></i>
                                </a>
                            </div>
                            <div class="checkbox-container"  data-toggle="toggletip" data-placement="top" title="{{ $agent->disabled? "Enable": "Disable"}} Partner" data-partner="{{$agent->partner_id}}">
                                <input type="checkbox"  class="toggle-agent" id="toggle-{{$agent->user_id}}" attr = "toggle-{{$agent->user_id}}"  data-id ="{{$agent->user_id}}" {{ $agent->disabled? "": "checked"}} data-partner="{{$agent->partner_id}}"/>
                                <label for="toggle-{{$agent->user_id}}"></label>
                            </div>
                        </div>
                    </div>

                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" class="pagination-td">{!!$agents->appends(request()->input())->links('pagination::bootstrap-4'); !!}</td>
        </tr>
    @else
        <tr>
            <td colspan="5">No data found.</td>
        </tr>
    @endif
    </tbody>
</table>