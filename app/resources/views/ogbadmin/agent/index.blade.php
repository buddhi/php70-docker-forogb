@extends('ogbadmin.layout')
@section('title')
    Agent Listing
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
        <div class="container-fluid">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    @if($filterStatus == 'valid')
                        <h3 class="mt-1">Registered Agents</h3>
                        <p style="font-size: 1rem;"><a href="{{route('ogbadmin.agent.index', ['filter' =>'pending'])}}">Show pending invites</a> </p>
                    @else
                        <h3 class="mt-1"> Pending Agent Invites</h3>
                        <p style="font-size: 1rem;"><a href="{{route('ogbadmin.agent.index', ['filter' =>'valid'])}}">Show registered Agents</a> </p>
                    @endif
                </div>
                <div class="col-md-6 d-flex justify-content-end">
                    <button type="button" class="btn btn-primary add-agent" data-toggle="modal"
                            data-target="#agent-verify">Add a
                        agent</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <hr class="mb-3">
                <div class="col-md-12">
                    @if($filterStatus == 'valid')
                        @include('ogbadmin.agent.agent-table')
                    @else
                        @include('ogbadmin.agent.pending-agent-table')
                    @endif
                </div>
            </div>
        </div>
    </main>
    @include('ogbadmin.agent.verify')
    @include('ogbadmin.agent.resend-verification')
@endsection
@section('js')
    <script>
        var disabled_partners = {{json_encode($disabled_partners)}}
    </script>
    <script src="{{asset('js/ogbadmin/agent/agent-dashboard.js')}}"></script>
@endsection