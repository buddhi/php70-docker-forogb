    <table class="table table-wrapper">
    <thead>
    <tr>
        <th scope="col">Email Address</th>
        <th scope="col">Status</th>
        <th scope="col" class="text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @if ($agents->count() > 0)
        @foreach($agents as $agent)
            <tr>
                <td><span class="user-thumbnail text-center text-white">{!! isset($agent->email[0])? $agent->email[0]:"A"!!}</span>
                    {{$agent->email}}</td>
                <td>
                    <div class="status verification-pending">Verification Pending</div>
                </td>
                <td>
                    <div class="action">
                        <div class="align-items-center">
                            <div class="resend-email text-center" data-toggle="tooltip" data-placement="top"
                                 title="Resend Verification Email">
                                <a href="#" data-target="#resend-verification" data-toggle="modal"
                                   data-whatever="{{$agent->email}}" data-id="{{$agent->id}}"
                                   >
                                    <i class="icon ion-md-mail"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3" class="pagination-td">{!!$agents->appends(request()->input())->links('pagination::bootstrap-4'); !!}</td>
        </tr>
    @else
        <tr>
            <td colspan="3">No data found.</td>
        </tr>
    @endif
    </tbody>
</table>