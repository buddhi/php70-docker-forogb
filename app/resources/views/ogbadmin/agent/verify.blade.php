<div class="modal fade" id="agent-verify" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">
            <section id="invite-agent" class="row  justify-content-center invite-partner-section">


                <div class="row  justify-content-center">
                    <div class="col-12 text-center mb-3">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h3>Invite an Agent</h3>
                    </div>
                    <div class="alert alert-danger error-form" id="add-agent-error-bag">
                        <ul id="add-agent-form-errors"></ul>
                    </div>
                    <div class="col-12">
                        <div class="col-12">
                            <form class="form-horizontal" id="add_agent" enctype="multipart/form-data">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <label for="partner">Name of Partner:</label>
                                    <select id="partner" class="form-control" required="required" name="partner">

                                        @if(session('role') == \App\Enums\Role::ADMIN))
                                        @foreach($partners as $partner)
                                            <option value="{{$partner->id}}" data-image="{{isset($partner->image)? asset($partner->image):asset('img/farmer_default.png')}}">{!! $partner->name!!}</option>
                                        @endforeach
                                        @else
                                            <option value="{{$selected_partner->id}}" data-image="{{isset($selected_partner->image)? asset($selected_partner->image):asset('img/farmer_default.png')}}">{!! $selected_partner->name!!}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email Address:</label>
                                    <input id="email" class="form-control" placeholder="Enter email address" name="email" type="text">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-primary verification-email-agent" rel="verification-success">Send Verification Email</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@include('ogbadmin.email-verification-popup')