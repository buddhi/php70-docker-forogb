<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
        @auth
        <ul class="navbar-nav ml-5">
            <li class="navbar-item header-search-bar">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Find projects or users . . .">
                    <button class="btn btn-link"><i class="icon ion-md-search"></i></button>
                </div>
            </li>
        </ul>

        <div class="navbar-nav flex-row ml-md-auto d-none d-md-flex" id="navbar">
            <ul class="navbar-nav ml-auto">
                <li class="navbar-item dropdown header-notification mr-4">
                    <a class="navbar-nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                        <div class="notification-bell">
                            <i class="icon ion-md-notifications"></i>
                        </div>
                        <div class="item-title d-md-none text-16 mg-l-10">Notification</div>
                        @if($unread_notification_count!=0)
                            <span id="unread-count">{{$unread_notification_count}}</span>
                        @endif
                    </a>
                    <div class="dropdown-menu pt-2 pb-2 mt-2">
                        <div class="notification-wrap">
                            @if(isset($notifications)  && count($notifications)> 0)
                                @foreach ($notifications as $notification)
                                    <div class="dropdown-item d-flex" id="notification-div">
                                        <div class="user-thumb mr-1"><img
                                                    src="{{$notification->data['message']['image']}}"
                                                    class="user-thumbnail" alt=""></div>
                                        <div class="d-flex flex-column">
                                            <div class="d-flex flex-column position-relative">
                                                <strong>Admin</strong>
                                                <a href="{{$notification->data['message']['url']}}"></a>
                                                <div class="d-flex mb-0">{{$notification->data['message']['notification_message']}}</div>
                                                <small class="text-muted">{{$notification->created_at->diffForHumans(\Carbon\Carbon::now())}}</small>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mb-2">
                                @endforeach
                            @endif
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown header-user">
                    <a class="d-flex align-items-center" href="#" data-toggle="dropdown" aria-expanded="false">
                        <img class="avatar rounded-circle mr-md-2" src="{{asset('img/farmer_default.png')}}"
                             alt="Image description">
                        @auth
                        @if(session('role') == \App\Enums\Role::PARTNER_USER)
                            <span class="d-none d-md-block">{{\Illuminate\Support\Facades\Auth::user()->full_name}}</span>
                        @elseif(session('role') == \App\Enums\Role::DEVELOPER)
                            <span class="d-none d-md-block">{{\Illuminate\Support\Facades\Auth::user()->full_name}}</span>
                        @else
                            <span class="d-none d-md-block">OGB ADMIN</span>
                        @endif
                        @endauth
                        <i class="icon ion-md-arrow-dropdown d-md-block ml-2"></i>
                    </a>
                    <div id="profileMenu" class="dropdown-menu pt-2 pb-2 mt-2">
                        <a class="dropdown-item" href="#">Profile</a>
                        @auth
                        <a class="dropdown-item"
                           href="{{route('account.edit' ,\Illuminate\Support\Facades\Auth::user()->user_id)}}">Account</a>
                        @endauth
                        <a class="dropdown-item" href="#">Settings</a>
                        <hr class="mt-2 mb-2">
                        <a class="dropdown-item" href="{{route('logout')}}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign
                            Out</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        @endauth
    </div>
</header>