<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Login</title>
</head>

<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>
<body>
<div class="container-fluid">
    <div class="row flex-xl-nowrap">
        <main class="col-12 col-md-12 col-xl-12 bd-content">
            <div class="container-fluid">
                <div class="box-wrap verify-mobile-number text-center mb-5">
                    <img src="{{asset('img/confirmed.png')}}" class="mb-4" alt="">
                    <h3 class="mb-3">Congratulations!</h3>
                    <p>Your organization has been registered as a partner in the <strong>Off - Grid Bazaar</strong>
                        platform with following details:</p>
                    <p class="text-muted mb-0">Partner</p>
                    <strong>{{$partner->name}}</strong>

                    <p class="text-muted mb-0 mt-2">Contact Person</p>
                    <strong>{{$partner->contact_person}}</strong>

                    <p class="text-muted mb-0 mt-2">organization Email</p>
                    <strong>{{$partner->email}}</strong>
                </div>
            </div>
        </main>
    </div>
</div>
</body>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>


</html>