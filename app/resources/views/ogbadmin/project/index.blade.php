@extends('ogbadmin.layout')
@section('title')
    Project | Listing
@endsection
@section('styles')
    <style>
        td a {
            width: 100%;
            display: block;
        }

        td a, a:hover, a:active {
            text-decoration: none;
            color: #223741;
        }

        .filter-bar {
            z-index: 4 !important;
        }

        .pageinations-wrapper {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .loading-content {
            width: 100%;
            text-align: center;
        }

        .custom-modal {
            position: fixed;
            height: 100vh;
            width: 100%;
            top: 0;
            left: 0;
            display: flex !important;
            justify-content: center;
            align-items: center;
            z-index: 5;
        }

        .backdrop {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            z-index: 8;
            background: rgba(0, 0, 0, 0.5);
        }

        .main-content {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 25%;
            height: 19%;
            z-index: 9;
            background-color: white;
            flex-direction: column;
            font-size: 26px;
            padding: 8px;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: .3rem;
            position: relative;
        }

        .main-content > .header {
            /*text-align: center;*/
            text-align: center;
            font-size: .88rem;
            color: #223741;
            padding: 8px 0;
        }

        .disp {
            display: table-row !important;
        }

        .disp-none {
            display: none;
        }

        .cross {
            position: absolute;
            top: 8px;
            right: 13px;
            /*font-size: 19px;*/
            cursor: pointer;
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
        }

        .progress-wrap .dropdown-menu {
            min-width: 330px;
            font-size: 0.88rem;
        }

        .edit-index-btn {
            border: none;
            background: none;
        }
        .checkbox-container label{
            background-color: #ff0000;
        }
        /* Agent Project Toggle */
        .toggle-disabled{
            background-color: lightgrey!important;
        }
        .toggle-disabled:hover{
            cursor: not-allowed;
        }
    </style>
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content" id="projectapp">
        <div class="container-fluid" id="project-edit-app">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0">Projects</h3>
                </div>
                @if(session('role') == 50 || session('role') == 1)
                    <div class="col-md-6 d-flex justify-content-end">
                        <a href="{{route('ogbadmin.project.create')}}" class="btn btn-primary">Create a Project</a>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-6 col-sm-12 mb-sm-4">
                    <div class="text-muted mb-4">Overview</div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="info-card card-green d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>{{$projects->count()}}</h4>
                                    <div class="text-muted">Total Projects</div>
                                </div>
                                <i class="icon ion-md-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-card card-grey d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>{{$projects->where('status','=','new')->count()}}</h4>
                                    <div class="text-muted">New Projects</div>
                                </div>
                                <i class="icon ion-md-star-outline"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="text-muted mb-4">Project Status</div>
                    <div class="row">
                        <div class="col-md-12">
                            <canvas id="project_status" class="col_canvas"></canvas>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="filter-bar">
                                <div class="col-lg-4 col-sm-12">
                                    <div class="row">
                                        <div class="col">
                                            <button class="btn btn-filter d-flex justify-content-between align-items-center"
                                                    type="button" data-toggle="collapse" data-target="#filterDropdown"
                                                    role="button" aria-expanded="false" aria-controls="filterDropdown">
                                                <span class="mr-5">Filter by:</span>
                                                <i class="icon ion-md-funnel"></i>
                                            </button>
                                            <div class="filter-dropdown collapse" id="filterDropdown">
                                                <form action="{{route('ogbadmin.project.filter')}}" method="post"
                                                      @submit.prevent="onFilterSubmit">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-lg col-sm-4">
                                                            <strong class="d-block mb-2">Location</strong>
                                                            <select class="custom-select" id="province_id"
                                                                    name="province_id" v-model="filters.province_id">
                                                                <option value="">Province</option>
                                                                @foreach($province as $data)
                                                                    <option value="{{$data->id}}" {!! old('province_id', isset($project)? $project->province_id : "")== $data->id? "selected":"" !!}>{{$data->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <div class="form-group">
                                                                <select name="district" id="district"
                                                                        class="form-control input-lg dynamic"
                                                                        v-model="filters.district">
                                                                    <option value="">Select District</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg col-sm-4">
                                                            <strong class="d-block mb-2">Size</strong>
                                                            @foreach($plans as $data)
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           value="{{$data->pump_size}}"
                                                                           id="sizeCheck{{$data->id}}"
                                                                           name=" pump_size[]"
                                                                           v-model="filters.pump_size">
                                                                    <label class="form-check-label"
                                                                           for="sizeCheck{{$data->id}}">
                                                                        <div><i class="icon ion-md-checkmark"></i></div>
                                                                        {{($data->pump_size!=''?$data->pump_size.'hp':0)}}
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                            {{--<div class="form-check">
                                                                <input class="form-check-input" type="checkbox" value=""
                                                                       id="sizeCheck1">
                                                                <label class="form-check-label" for="sizeCheck1">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    1 hp
                                                                </label>
                                                            </div>
                                                            <div class="form-check mt-2">
                                                                <input class="form-check-input" type="checkbox" value=""
                                                                       id="sizeCheck2">
                                                                <label class="form-check-label" for="sizeCheck2">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    1.5 hp
                                                                </label>
                                                            </div>
                                                            <div class="form-check mt-2">
                                                                <input class="form-check-input" type="checkbox" value=""
                                                                       id="sizeCheck3">
                                                                <label class="form-check-label" for="sizeCheck3">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    2 hp
                                                                </label>
                                                            </div>
                                                            <div class="form-check mt-2">
                                                                <input class="form-check-input" type="checkbox" value=""
                                                                       id="sizeCheck4">
                                                                <label class="form-check-label" for="sizeCheck4">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    Small
                                                                </label>
                                                            </div>--}}
                                                        </div>
                                                        <div class="col-lg col-sm-4">
                                                            <strong class="d-block mb-2">Category</strong>
                                                            @foreach($project_type as $value)
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           value="{{$value->id}}"
                                                                           id="categoryCheck{{$value->id}}"
                                                                           name="project_type_id[]"
                                                                           v-model="filters.project_type_id">
                                                                    <label class="form-check-label"
                                                                           for="categoryCheck{{$value->id}}">
                                                                        <div><i class="icon ion-md-checkmark"></i></div>
                                                                        {{$value->name}}
                                                                    </label>
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                        <div class="col-lg col-sm-4 mt-sm-3">
                                                            <strong class="d-block mb-2">Project Status</strong>
                                                            @foreach($project_status as $key=>$value)
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           value="{{$value}}"
                                                                           id="projectStatusCheck{{$key}}"
                                                                           name="status[]" v-model="filters.status">
                                                                    <label class="form-check-label"
                                                                           for="projectStatusCheck{{$key}}">
                                                                        <div><i class="icon ion-md-checkmark"></i></div>
                                                                        {{$value}}
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <div class="col-lg col-sm-4 mt-sm-3">
                                                            <strong class="d-block mb-2">Credit Score</strong>
                                                            <div class="form-check">
                                                                <input class="form-check-input" name="credit_score[]"
                                                                       type="checkbox" value="1"
                                                                       id="creditScore1" v-model="filters.credit_score">
                                                                <label class="form-check-label" for="creditScore1">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <span class="ml-2">5/5</span>
                                                                            </span>
                                                                </label>
                                                            </div>

                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox"
                                                                       value="0.8" name="credit_score[]"
                                                                       id="creditScore2" v-model="filters.credit_score">
                                                                <label class="form-check-label" for="creditScore2">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">4/5</span>
                                                                            </span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox"
                                                                       value="0.6" name="credit_score[]"
                                                                       id="creditScore3" v-model="filters.credit_score">
                                                                <label class="form-check-label" for="creditScore3">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">3/5</span>
                                                                            </span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox"
                                                                       value="0.4" name="credit_score[]"
                                                                       id="creditScore4" v-model="filters.credit_score">
                                                                <label class="form-check-label" for="creditScore4">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">2/5</span>
                                                                            </span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox"
                                                                       value="0.2" name="credit_score[]"
                                                                       id="creditScore5" v-model="filters.credit_score">
                                                                <label class="form-check-label" for="creditScore5">
                                                                    <div><i class="icon ion-md-checkmark"></i></div>
                                                                    <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">1/5</span>
                                                                            </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-between">
                                                        <div class="col-lg col-sm-4">
                                                            <button type="reset" @click="clearAllFilters"
                                                                    class="btn btn-outline-danger">Clear
                                                                all Filters
                                                            </button>
                                                        </div>
                                                        <div class="col-lg col-sm-8 text-right">
                                                            <button id="cancel-filter"
                                                                    class="btn btn-outline-secondary mr-3"
                                                                    data-toggle="collapse" data-target="#filterDropdown"
                                                                    role="button" aria-expanded="false"
                                                                    aria-controls="filterDropdown">Cancel
                                                            </button>
                                                            <button id="apply-filter" class="btn btn-primary">Apply
                                                                Filter
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-12 mt-lg-0 mt-sm-3">
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="form-group filter-search">
                                                <input type="text" class="form-control" placeholder="Search..."
                                                       v-model="filters.searchField" @keyup="onFilterSubmit">
                                                <button class="btn btn-link"><i class="icon ion-md-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <select class="custom-select" id="inputGroupSelect01"
                                                    v-model="filters.orderBy" @change="onFilterSubmit">
                                                <option selected>Sort by</option>
                                                <option value="1">Sort A - Z</option>
                                                <option value="2">Sort Z - A</option>
                                                <option value="3">Recent Projects</option>
                                                <option value="4">Old Projects</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <app-modal v-show="isActive" :is-active-prop="id" @close="isActive=false"
                           @changedata="onProjectUpdate"></app-modal>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-sm mb-0 table-dashboard">
                            <thead>
                            <tr>
                                <th scope="col">Payment ID</th>
                                <th scope="col">System Size</th>
                                <th scope="col">Farmer Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Project Status</th>
                                <th scope="col">Funding Mix</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="display: none;" :class="{'disp' : getProjects}"
                                v-for="(project,index) in getProjects">
                                <th scope="col" v-text="project.payment_id"></th>
                                <td>
                                    <a :href="'/ogbadmin/project/details/'+project.id"
                                       v-text="project.pump_size?project.pump_size + 'HP': '0'"></a>
                                </td>
                                <td>
                                    <a :href="'/ogbadmin/project/details/'+project.id"><p
                                                v-text="project.farmer_name" class="text-capitalize"></p>
                                        <div class="additional-info">
                                            <div class="location"><i
                                                        class="icon ion-md-pin"></i>@{{project.district}}
                                            </div>
                                            <div class="contact-no"><i
                                                        :class="'icon ion-md-call'"></i><span
                                                        v-text="project.contact_no"></span>
                                            </div>
                                            <div class="credit-score"><img :src="'../img/icon_meter_level3.png'" alt=""><span
                                                        v-text="project.credit_score?project.credit_score.toFixed(2):0"></span>
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td>
                                    <a :href="'/ogbadmin/project/details/'+project.id"
                                       v-text="project.name !== null ? project.name:'not available'"></a>
                                </td>
                                <td>
                                    <a :href="'/ogbadmin/project/details/'+project.id">
                                        <div :class="'status '+project.status" v-text="getStatusCapitalCased(project.status)"></div>
                                    </a>
                                </td>
                                <td>
                                    <a :href="'/ogbadmin/project/details/'+project.id">
                                        <div class="progress-wrap dropdown-toggle" data-toggle="dropdown"
                                             data-boundary="viewport" aria-haspopup="true" aria-expanded="false">
                                            <div class="progress">
                                                <span v-for="funding_item in project.funding_mix"
                                                      :style="{'width':getPercentage(funding_item.amount,project.cost)+'%'}"
                                                      :class="'progress-'+getColor(funding_item.investor)"></span>
                                                {{--                                                <span class="progress-green" style="width:10%;"></span>--}}
                                                {{--                                                <span class="progress-orange" style="width:15%;"></span>--}}
                                                {{--                                                <span class="progress-purple" style="width:13%;"></span>--}}
                                            </div>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                <div class="dropdown-item pl-3 pr-3 mb-2"
                                                     v-for="funding_item in project.funding_mix">
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-md-6">
                                                            @{{funding_item.investor}} <span
                                                                    class="d-block text-muted">Rs. @{{funding_item.amount ? Number.parseFloat(funding_item.amount).toFixed(0) : 0}} of <strong>Rs. @{{ project.cost }}</strong></span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="progress">
                                                                <span :style="{'width':getPercentage(funding_item.amount,project.cost)+'%'}"
                                                                      :class="'progress-'+getColor(funding_item.investor)"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--                                                <div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                {{--                                                    <div class="row d-flex align-items-center">--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            My Investment <span--}}
                                                {{--                                                                    class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            <div class="progress">--}}
                                                {{--                                                                <span class="progress-green" style="width:10%"></span>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                {{--                                                <div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                {{--                                                    <div class="row d-flex align-items-center">--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            Other Investment <span--}}
                                                {{--                                                                    class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            <div class="progress">--}}
                                                {{--                                                                <span class="progress-orange" style="width:15%"></span>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                {{--                                                <div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                {{--                                                    <div class="row d-flex align-items-center">--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            MFI Investment <span--}}
                                                {{--                                                                    class="d-block text-muted">$650 of <strong>$5,000</strong></span>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <div class="col-md-6">--}}
                                                {{--                                                            <div class="progress">--}}
                                                {{--                                                                <span class="progress-purple" style="width:13%"></span>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                                {{--<div class="row d-flex align-items-center">--}}
                                                {{--<div class="col-md-6">--}}
                                                {{--Funding Gap <span--}}
                                                {{--class="d-block text-muted">Rs. @{{ getFundingGap(project.cost) }} of <strong>Rs. @{{ project.cost }}</strong></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-6">--}}
                                                {{--<div class="progress">--}}
                                                {{--<span class="progress-grey" :style="{'width':getFundingGapPercentage(project.cost)+'%'}"></span>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </a>
                                </td>

                                <td>
                                    <div class="action d-flex justify-content-end">
                                        <div class="d-flex align-items-center">
                                            @if(session('role') != 1 && session('role') != 200)<div class="edit" data-toggle="tooltip" data-placement="top"
                                                 title="Edit Project">
                                                {{--<button @click="changeStatusAndInputId(project.id)">
                                                    <i class="icon ion-md-create"></i>
                                                </button>--}}
                                                <a :href="'/ogbadmin/project/edit/'+project.id">
                                                    <button class="edit-index-btn">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                            </div>@endif

                                                @if(session('role') != 1 && session('role') != 200)<div class="checkbox-container toggle" data-toggle="tooltip"
                                                     :id="'tooltip' + project.id" data-placement="top"
                                                     @click="showModal(project.id,index,project.delete_flg)"
                                                     :title="project.delete_flg? 'EnableProject': 'DisableProject'">
                                                    @else
                                                        <div class="checkbox-container toggle" data-toggle="tooltip"
                                                             :id="'tooltip' + project.id" data-placement="top"
                                                             :title="project.delete_flg? 'EnableProject': 'DisableProject'">
                                                    @endif
                                                    <input type="checkbox" :checked="!project.delete_flg"/>
                                            {{--<input type="checkbox" :checked="!project.delete_flg" disabled>--}}

                                                <label for="toggle-5" data-toggle="modal"
                                                       :data-target="'#disableProject'+project.id" 
                                                        class="@if(session('role') == 1 ) toggle-disabled @endif"></label>
                                            </div>
                                        </div>
                                    </div>
                                </td>


                                {{--<tr v-for="project in projects">--}}
                                {{--<th scope="col">190001</th>--}}
                                {{--<td>--}}
                                {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">@{{project.id}}</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">{{$project->farmer_name}}--}}
                                {{--<div class="additional-info">--}}
                                {{--<div class="location"><i--}}
                                {{--class="icon ion-md-pin"></i>{{$project->district->name}}</div>--}}
                                {{--<div class="contact-no"><i--}}
                                {{--class="icon ion-md-call"></i>{{$project->sim_number}}</div>--}}
                                {{--<div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">{{$project->getProjectType->name}}</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">--}}
                                {{--<div class="status {{$project->status}}">{{$project->status}}</div>--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">--}}
                                {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown"--}}
                                {{--data-boundary="viewport" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-blue" style="width:30%;"></span>--}}
                                {{--<span class="progress-green" style="width:10%;"></span>--}}
                                {{--<span class="progress-orange" style="width:15%;"></span>--}}
                                {{--<span class="progress-purple" style="width:13%;"></span>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                {{--<div class="row d-flex align-items-center">--}}
                                {{--<div class="col-md-6">--}}
                                {{--Gham Power <span--}}
                                {{--class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-blue" style="width:30%"></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                {{--<div class="row d-flex align-items-center">--}}
                                {{--<div class="col-md-6">--}}
                                {{--My Investment <span--}}
                                {{--class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-green" style="width:10%"></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                {{--<div class="row d-flex align-items-center">--}}
                                {{--<div class="col-md-6">--}}
                                {{--Other Investment <span--}}
                                {{--class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-orange" style="width:15%"></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                {{--<div class="row d-flex align-items-center">--}}
                                {{--<div class="col-md-6">--}}
                                {{--MFI Investment <span--}}
                                {{--class="d-block text-muted">$650 of <strong>$5,000</strong></span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-purple" style="width:13%"></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                                {{--<div class="row d-flex align-items-center">--}}
                                {{--<div class="col-md-6">--}}
                                {{--Funding Gap <span--}}
                                {{--class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<div class="progress">--}}
                                {{--<span class="progress-grey" style="width:32%"></span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                {{--<div class="action d-flex justify-content-end">--}}
                                {{--<div class="d-flex align-items-center">--}}
                                {{--<div class="edit" data-toggle="tooltip" data-placement="top"--}}
                                {{--title="Edit Project">--}}
                                {{--<button @click="changeStatusAndInputId({{ $project->id }})">--}}
                                {{--<i class="icon ion-md-create"></i>--}}
                                {{--</button>--}}
                                {{--</div>--}}

                                {{--<div class="checkbox-container" data-toggle="tooltip" data-placement="top"--}}
                                {{--title=" {{ $project->delete_flg? "Enable": "Disable"}}Project">--}}
                                {{--<input type="checkbox" {{ $project->delete_flg? "": "checked"}}/>--}}
                                {{--<label for="toggle-5" data-toggle="modal"--}}
                                {{--data-target="#disableProject{{$project->id}}"></label>--}}

                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--not here--}}

                                {{--<div class="modal fade" id="disableProject{{$project->id}}" role="dialog">--}}
                                {{--<div class="modal-dialog modal-dialog-centered">--}}
                                {{--<div class="modal-content p-4 p-md-5">--}}
                                {{--<div class="row  justify-content-center">--}}
                                {{--<div class="col-12 text-center">--}}
                                {{--<button type="button" class="close" data-dismiss="modal"--}}
                                {{--aria-label="Close">--}}
                                {{--<span aria-hidden="true">&times;</span></button>--}}
                                {{--<p>Are you sure you want--}}
                                {{--to {{ $project->delete_flg? "enable": "disable"}} Project?</p>--}}
                                {{--<a href="{{route('ogbadmin.project.change-status',['data'=>$project->id])}}"--}}
                                {{--class="btn btn-primary pl-4 pr-4 yes">Yes</a>--}}
                                {{--<a href="" data-id="{{$project->id}}"--}}
                                {{--class="btn btn-primary pl-4 pr-4 sure">Yes</a>--}}
                                {{--<a href="#" class="btn btn-outline-primary mr-3 pl-4 pr-4"--}}
                                {{--data-dismiss="modal">No</a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </tr>
                            {{--@if ($projects->count() > 0)--}}
                            {{--@foreach($projects as $project)--}}
                            {{--<tr>--}}
                            {{--<th scope="col">190001</th>--}}
                            {{--<td><a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">{{$project->pump_size}}</a></td>--}}
                            {{--<td><a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">{{$project->farmer_name}}--}}
                            {{--<div class="additional-info">--}}
                            {{--<div class="location"><i class="icon ion-md-pin"></i>{{$project->district->name}}</div>--}}
                            {{--<div class="contact-no"><i class="icon ion-md-call"></i>{{$project->sim_number}}</div>--}}
                            {{--<div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</td>--}}
                            {{--<td><a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">{{$project->getProjectType->name}}</a></td>--}}
                            {{--<td>--}}
                            {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}"> <div class="status {{$project->status}}">{{$project->status}}</div></a>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                            {{--<a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">--}}
                            {{--<div class="progress-wrap dropdown-toggle" data-toggle="dropdown" data-boundary="viewport" aria-haspopup="true" aria-expanded="false">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-blue" style="width:30%;"></span>--}}
                            {{--<span class="progress-green" style="width:10%;"></span>--}}
                            {{--<span class="progress-orange" style="width:15%;"></span>--}}
                            {{--<span class="progress-purple" style="width:13%;"></span>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-menu dropdown-menu-right" x-placement="top-end">--}}
                            {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                            {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-md-6">--}}
                            {{--Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-blue" style="width:30%"></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                            {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-md-6">--}}
                            {{--My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-green" style="width:10%"></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                            {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-md-6">--}}
                            {{--Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-orange" style="width:15%"></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                            {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-md-6">--}}
                            {{--MFI Investment <span class="d-block text-muted">$650 of <strong>$5,000</strong></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-purple" style="width:13%"></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-item pl-3 pr-3 mb-2">--}}
                            {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-md-6">--}}
                            {{--Funding Gap <span class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="progress">--}}
                            {{--<span class="progress-grey" style="width:32%"></span>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                            {{--<div class="action d-flex justify-content-end">--}}
                            {{--<div class="d-flex align-items-center">--}}
                            {{--<div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Project">--}}
                            {{--<button @click="changeStatusAndInputId({{ $project->id }})">--}}
                            {{--<i class="icon ion-md-create"></i>--}}
                            {{--</button>--}}
                            {{--</div>--}}

                            {{--<div class="checkbox-container"  data-toggle="tooltip" data-placement="top" title=" {{ $project->delete_flg? "Enable": "Disable"}}Project">--}}
                            {{--<input type="checkbox"  {{ $project->delete_flg? "": "checked"}}/>--}}
                            {{--<label for="toggle-5" data-toggle="modal" data-target="#disableProject{{$project->id}}"></label>--}}

                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</td>--}}
                            {{--<div class="modal fade" id="disableProject{{$project->id}}" role="dialog">--}}
                            {{--<div class="modal-dialog modal-dialog-centered">--}}
                            {{--<div class="modal-content p-4 p-md-5">--}}
                            {{--<div class="row  justify-content-center">--}}
                            {{--<div class="col-12 text-center">--}}
                            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                            {{--<span aria-hidden="true">&times;</span></button>--}}
                            {{--<p>Are you sure you want to {{ $project->delete_flg? "enable": "disable"}} Project?</p>--}}
                            {{--<a href="{{route('ogbadmin.project.change-status',['data'=>$project->id])}}"  class="btn btn-primary pl-4 pr-4 yes">Yes</a>--}}
                            {{--<a href="" data-id="{{$project->id}}" class="btn btn-primary pl-4 pr-4 sure">Yes</a>--}}
                            {{--<a href="#" class="btn btn-outline-primary mr-3 pl-4 pr-4" data-dismiss="modal" >No</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</tr>--}}

                            {{--finishes here--}}
                            <!-- <tr>
                                            <th scope="col">190002</th>
                                            <td>2hp</td>
                                            <td>Hari Bahadur
                                                <div class="additional-info">
                                                    <div class="location"><i class="icon ion-md-pin"></i>Dhading</div>
                                                    <div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>
                                                    <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>
                                                </div>
                                            </td>
                                            <td>Aggrotech</td>
                                            <td>
                                                <div class="status pending">Operational</div>
                                            </td>
                                            <td>
                                                <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <div class="progress">
                                                        <span class="progress-blue" style="width:30%;"></span>
                                                        <span class="progress-green" style="width:10%;"></span>
                                                        <span class="progress-orange" style="width:15%;"></span>
                                                        <span class="progress-purple" style="width:13%;"></span>
                                                    </div>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-blue" style="width:30%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-green" style="width:10%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-orange" style="width:15%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    MFI Investment <span class="d-block text-muted">$650 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-purple" style="width:13%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Funding Gap <span class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-grey" style="width:32%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="action d-flex justify-content-end">
                                                    <div class="d-flex align-items-center">
                                                        <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Project">
                                                            <a href="#">
                                                                <i class="icon ion-md-create"></i>
                                                            </a>
                                                        </div>
                                                        <div class="checkbox-container"  data-toggle="tooltip" data-placement="top" title="Disable Project">
                                                            <input type="checkbox" id="toggle-2" />
                                                            <label for="toggle-2" data-toggle="modal" data-target="#disableProject"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">190003</th>
                                            <td>2hp</td>
                                            <td>Shyam Bahadur
                                                <div class="additional-info">
                                                    <div class="location"><i class="icon ion-md-pin"></i>Dhading</div>
                                                    <div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>
                                                    <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>
                                                </div>
                                            </td>
                                            <td>Fish Farm</td>
                                            <td>
                                                <div class="status error">Error</div>
                                            </td>
                                            <td>
                                                <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <div class="progress">
                                                        <span class="progress-blue" style="width:30%;"></span>
                                                        <span class="progress-green" style="width:10%;"></span>
                                                        <span class="progress-orange" style="width:15%;"></span>
                                                        <span class="progress-purple" style="width:13%;"></span>
                                                    </div>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-blue" style="width:30%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-green" style="width:10%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-orange" style="width:15%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    MFI Investment <span class="d-block text-muted">$650 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-purple" style="width:13%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Funding Gap <span class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-grey" style="width:32%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="action d-flex justify-content-end">
                                                    <div class="d-flex align-items-center">
                                                        <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Project">
                                                            <a href="#">
                                                                <i class="icon ion-md-create"></i>
                                                            </a>
                                                        </div>
                                                        <div class="checkbox-container"  data-toggle="tooltip" data-placement="top" title="Disable Project">
                                                            <input type="checkbox" id="toggle-3" />
                                                            <label for="toggle-3" data-toggle="modal" data-target="#disableProject"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">190004</th>
                                            <td>2hp</td>
                                            <td>Ram Bahadur
                                                <div class="additional-info">
                                                    <div class="location"><i class="icon ion-md-pin"></i>Dhading</div>
                                                    <div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>
                                                    <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>
                                                </div>
                                            </td>
                                            <td>Aggrotech</td>
                                            <td>
                                                <div class="status new">New</div>
                                            </td>
                                            <td>
                                                <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <div class="progress">
                                                        <span class="progress-blue" style="width:30%;"></span>
                                                        <span class="progress-green" style="width:10%;"></span>
                                                        <span class="progress-orange" style="width:15%;"></span>
                                                        <span class="progress-purple" style="width:13%;"></span>
                                                    </div>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-blue" style="width:30%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    <div class="info">
                                                                        My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-green" style="width:10%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    <div class="info">
                                                                        Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-orange" style="width:15%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    MFI Investment <span class="d-block text-muted">$650 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-purple" style="width:13%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Funding Gap <span class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-grey" style="width:32%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="action d-flex justify-content-end">
                                                    <div class="d-flex align-items-center">
                                                        <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Project">
                                                            <a href="#">
                                                                <i class="icon ion-md-create"></i>
                                                            </a>
                                                        </div>
                                                        <div class="checkbox-container"  data-toggle="tooltip" data-placement="top" title="Disable Project">
                                                            <input type="checkbox" id="toggle-4" />
                                                            <label for="toggle-4" data-toggle="modal" data-target="#disableProject"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">190005</th>
                                            <td>2hp</td>
                                            <td>Gore Bahadur
                                                <div class="additional-info">
                                                    <div class="location"><i class="icon ion-md-pin"></i>Dhading</div>
                                                    <div class="contact-no"><i class="icon ion-md-call"></i>9841123456</div>
                                                    <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">3/5</div>
                                                </div>
                                            </td>
                                            <td>Fish Farm</td>
                                            <td>
                                                <div class="status active">Approved</div>
                                            </td>
                                            <td>
                                                <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <div class="progress">
                                                        <span class="progress-blue" style="width:30%;"></span>
                                                        <span class="progress-green" style="width:10%;"></span>
                                                        <span class="progress-orange" style="width:15%;"></span>
                                                        <span class="progress-purple" style="width:13%;"></span>
                                                    </div>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Gham Power <span class="d-block text-muted">$1,500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-blue" style="width:30%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    My Investment <span class="d-block text-muted">$500 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-green" style="width:10%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Other Investment <span class="d-block text-muted">$750 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-orange" style="width:15%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    MFI Investment <span class="d-block text-muted">$650 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-purple" style="width:13%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropdown-item pl-3 pr-3 mb-2">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-6">
                                                                    Funding Gap <span class="d-block text-muted">$1,60 of <strong>$5,000</strong></span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="progress">
                                                                        <span class="progress-grey" style="width:32%"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="action d-flex justify-content-end">
                                                    <div class="d-flex align-items-center">
                                                        <div class="edit" data-toggle="tooltip" data-placement="top" title="Edit Project">
                                                            <a href="#">
                                                                <i class="icon ion-md-create"></i>
                                                            </a>
                                                        </div>
                                                        <div class="checkbox-container"  data-toggle="tooltip" data-placement="top" title="Disable Project">
                                                            <input type="checkbox" checked id="toggle-5" />
                                                            <label for="toggle-5" data-toggle="modal" data-target="#disableProject"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> -->
                            {{--@endforeach--}}
                            {{--<tr>--}}
                            {{--<td colspan="5"--}}
                            {{--class="pagination-td">{!! $projects->links('pagination::bootstrap-4') !!}</td>--}}
                            {{--</tr>--}}
                            {{--@else--}}
                            {{--<tr>--}}
                            {{--<td colspan="5">No data found.</td>--}}
                            {{--</tr>--}}
                            {{--@endif--}}
                            <tr v-if="getProjects==null">
                                <td colspan="5">
                                    <div class="loading-content"><i class="fa fa-circle-notch fa-spin"
                                                                    style="margin-right:8px;"></i>Loading Data
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none;" :class="{'disp' : getProjects}">
                                <td colspan="8"
                                    class="pagination-td">
                                    <div class="pageinations-wrapper">
                                        <div class="paginations">
                                            <span class="first-page btn btn-primary" @click="loadFirstPageData"
                                                  v-if="getInFirst">|<</span>
                                            <span class="next-page btn btn-primary" @click="loadPreviousPageData"
                                                  v-if="getInFirst"><</span>
                                            <span class="next-page btn btn-primary" @click="loadNextPageData"
                                                  v-if="isInLast">></span>
                                            <span class="last-page btn btn-primary" @click="loadLastPageData"
                                                  v-if="isInLast">>|</span>
                                        </div>
                                        <div class="total"> @{{ to }} out of @{{ total }}</div>
                                    </div>
                                </td>
                            </tr>


                            </tbody>
                            {{--<div class="modal fade" role="dialog" v-if="showModalVariable">--}}
                            {{--<div class="modal-dialog modal-dialog-centered">--}}
                            {{--<div class="modal-content p-4 p-md-5">--}}
                            {{--<div class="row  justify-content-center">--}}
                            {{--<div class="col-12 text-center">--}}
                            {{--<button type="button" class="close" data-dismiss="modal"--}}
                            {{--aria-label="Close">--}}
                            {{--<span aria-hidden="true">&times;</span></button>--}}
                            {{--<p>Are you sure you want--}}
                            {{--to @{{ projects[indexOfProject].delete_flg? "enable": "disable"}} Project?</p>--}}
                            {{--<a :href="'/ogbadmin/project/change-status/'+project.id"--}}
                            {{--class="btn btn-primary pl-4 pr-4 yes">Yes</a>--}}
                            {{--<button @click="deactivateProject(projectToDeleteId,indexOfProject)"--}}
                            {{--class="btn btn-primary pl-4 pr-4 sure">Yes</button>--}}
                            {{--<a href="#" @click.prevent="showModal=false" class="btn btn-outline-primary mr-3 pl-4 pr-4"--}}
                            {{--data-dismiss="modal">No</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </table>
                        <div style="display: none" :class="{'custom-modal':showModalVariable}">
                            <div class="backdrop" @click="closeModal"></div>
                            <div class="main-content">
                                <span class="cross" @click="closeModal">x</span>
                                <span class="header">Are you sure you want to @{{ deleteFlg? 'enable' : 'disable' }} the project ?</span>
                                <span class="button-area">
                                    <button class="btn btn-outline-primary mr-2"
                                            @click="deactivateProject (projectToDeleteId, indexOfProject)">Yes</button>
                                    <button class="btn btn-outline-primary mr-2" @click="closeModal">No</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    {{--</div>--}}
    {{--</div>--}}


@endsection
@section('js')

    <script>
      $(document).ready(function () {

        $('.sure').click(function (evt) {
          evt.preventDefault()
          var id = $(this).data('id')
          $.ajax({
            type: 'GET',
            url: '/ogbadmin/project/change-status/' + id,
            data: {
              id: id
            },
            success: function (html) {
              if (html == 'Success') {
                var checkBoxState = $(`#checkedProject${id}`).prop('checked')
                /*  console.log(statuses);
                 console.log(checkBoxState);*/
                $(`#checkedProject${id}`).prop('checked', !checkBoxState)
                $(`#dialog${id}`).text(checkBoxState ? 'enable' : 'disable')
                $(`#tooltip${id}`).attr('data-original-title', checkBoxState ? 'Enable Project' : 'Disable Project')
                $('.disable').modal('hide')
              }
            }
          })

        })

        $('select[name=\'province_id\']').change(function () {
          var provinceID = $(this).val()
          if (provinceID) {
            $.ajax({
              url: '/ogbadmin/project/get_districts/' + provinceID,
              type: 'GET',
              data: {'id': provinceID},
              success: function (data) {
                $('select[name="district"]').empty()
                $.each(data, function (key, value) {
                  $('select[name="district"]').append('<option value="' + value.id + '">' + value.name + '</option>')
                })
              }
            })

          } else {
            $('select[name="district"]').empty()
          }
        })

        $('#apply-filter').click(function () { $('#cancel-filter').trigger('click') })

      })


    </script>
    <script>
      // $(document).ready(function () {
      //     $('#project-list').DataTable();
      // });
    </script>
    <script>
      var statuses =
              {!! json_encode($projectStatuses) !!}
      var dataSet = {!! json_encode($dataSets) !!}
        $(document).ready(function () {
          console.log(dataSet)

          var projectStatus = document.getElementById('project_status')
          projectStatus.height = 70

          var project_status = new Chart(projectStatus, {
            type: 'doughnut',
            data: {
              datasets: [{
                data: dataSet,
                backgroundColor: ['#03A9F4', '#FF8C63', '#66BB6A', '#EF5350']
              }],
              labels: statuses
            },
            options: {
              legend: {
                position: 'right',
                fontSize: 14,
                labels: {
                  fontSize: 14
                },
                onClick: null
              }
            }
          })

          $(function () {
            $('[data-toggle="tooltip"]').tooltip()
          })

        })
    </script>
    <script src="{{asset('js/ogbadmin/project/project-dashboard.js')}}"></script>
    <script src="{{asset('js/ogbadmin/project/project-index.js')}}"></script>
    <script src="{{asset('js/ogbadmin/project/project-modal.js')}}"></script>
@endsection
