<html>
<head>
    <title>Customize package</title>
    <style>

    </style>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <link href='{{asset('fontawesome/css/all.min.css')}}' rel="stylesheet">
    @yield('css')
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">

    <style>
        #flash-message {
            position: absolute;
            top: 100px;
            right: 20px;
            z-index: 10;
        }

        .error {
            color: red;
        }

        .heading {
            display: flex;
        }

        .back-button {
            margin-left: 10%;
        }

        .text {
            margin-left: 25%;
        }

        .form-part {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .input-group {
            width: 40%;
        }

        .save {
            margin-top: 2%;
        }

        header {
            padding: 0 5vw;
            box-sizing: border-box;
            min-height: unset;
        }

        header > .title {
            flex: 1;
            margin-right: 15%
        }

        hr {
            margin: 2rem 13vw;
        }

        .input-group {
            margin: 10px 0;
            margin-left: 2%;
        }
        #flash-message{
            margin-top: 5%;
        }
    </style>
</head>
<body>
@include('ogbadmin.header')
<div class="container-fluid">
    <header class="d-flex flex-xl-nowrap">
        <div class="back-button w-5">
            <a href="{{route('ogbadmin.project.details',['data'=>$project->id])}}">
                <button class="btn btn-outline-primary outlined-button">
                    <i class="icon ion-ios-arrow-back"></i> Go Back
                </button>
            </a>
        </div>
        <div class="title">
            <h4 class="text-center">Customize Package</h4>
        </div>
    </header>
    <hr>

    <div id="flash-message">
        @if (session()->has('success_message'))
            <div class="alert alert-success alert-dismissible" role="alert" class="flash-message">
                {{session()->get('success_message')}}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert" class="flash-message">
                {{session()->get('error')}}
            </div>
        @endif
    </div>
    <form action="{{route('ogbadmin.project.update-package',['data'=>$project->id])}}" method="post">

        <div class="form-part">
            {{csrf_field()}}
            <div class="input-group">
                <label class="inline-label">Package Name</label>
                <select class="form-control" name="name">
                    @foreach($plans as $plan)
                        @if($plan->id == 1)
                            <option value="{{$plan->name}}" disabled>{{$plan->name}}</option>
                        @else
                            <option value="{{$plan->name}}" {{($plan->name==ucwords($project->name))?'selected':''}}>{{$plan->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="input-group">
                <label class="inline-label">Pump Size</label>
                <input class="form-control" id ="pump_size" name="pump_size" type="text" value="{{$project->pump_size}}"/>

            </div>
            <div class="input-group">
                <label class="inline-label">Pump Brand </label>
                <input class="form-control"  id="pump_brand" name="pump_brand" type="text" value="{{$project->pump_brand}}"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Solar PV Size</label>
                <input class="form-control" id="solar_pv_size" name="solar_pv_size" type="text" value="{{$project->solar_pv_size}}"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Controller</label>
                <input class="form-control" id="controller" name="controller" type="text" value="{{$project->controller}}"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Maximum Daily Discharge</label>
                <input class="form-control" id="daily_discharge" name="daily_discharge" type="text" value="{{$project->daily_discharge}}"/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Price</label>
                <input class="form-control" id="cost" name="cost" type="text" value="{{$project->cost}}"/>
            </div>
            <input class="form-control" id="total_emi" name="total_emi" type="hidden" value="{{$project->cost}}"/>
            <input class="form-control" id="advance_amount" name="advance_amount" type="hidden" value="{{$project->cost}}"/>
            <input class="form-control" id="emi_amount" name="emi_amount" type="hidden" value="{{$project->cost}}"/>
            <input class="form-control" id="emi_count" name="emi_count" type="hidden" value="{{$project->cost}}"/>

            <div class="input-group">
                <button class="btn btn-primary save ">Save Changes</button>
            </div>
        </div>
    </form>


</div>


<script src="{{asset('js/ogbadmin/jquery.min.js')}}">
</script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var package =  $('select[name="name"]').val();
        console.log(package);
        if(package) {
            $.ajax({
                url: '/ogbadmin/project/getPackage/'+package,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    $('#pump_size').val(data.pump_size);
                    $('#pump_brand').val(data.pump_brand);
                    $('#solar_pv_size').val(data.solar_pv_size);
                    $('#controller').val(data.controller);
                    $('#daily_discharge').val(data.daily_discharge);
                    $('#cost').val(data.plan_cost);
                    $('#total_emi').val(data.total_emi);
                    $('#advance_amount').val(data.advance_amount);
                    $('#emi_amount').val(data.emi_amount);
                    $('#emi_count').val(data.emi_count);

                }
            });
        }
        $('select[name="name"]').on('change', function() {
            var package = $(this).val();
            if(package) {
                $.ajax({
                    url: '/ogbadmin/project/getPackage/'+package,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('#pump_size').val(data.pump_size);
                        $('#pump_brand').val(data.pump_brand);
                        $('#solar_pv_size').val(data.solar_pv_size);
                        $('#controller').val(data.controller);
                        $('#daily_discharge').val(data.daily_discharge);
                        $('#cost').val(data.plan_cost);
                        $('#total_emi').val(data.total_emi);
                        $('#advance_amount').val(data.advance_amount);
                        $('#emi_amount').val(data.emi_amount);
                        $('#emi_count').val(data.emi_count);

                    }
                });
            }else{
//                $('select[name="city"]').empty();
            }
        });
        
        
    });
</script>

<script>$('#flash-message').delay(2000).fadeOut(2000);</script>
@yield('js')
</body>
</html>

