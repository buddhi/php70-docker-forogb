@extends('ogbadmin.layout')
@section('styles')
    <style>
        div.col-md-12, div.col-md-6 {
            margin: 10px 0;
        }

        .button-area {
            margin-top: 20px;
        }
    </style>
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content" id="project-create-app">
        <form action="{{route('ogbadmin.project.store')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input class="form-control" name="is_draft" id="is_draft" value="1" type="hidden"/>
            <input class="form-control" name="latitude" id="latitude" value="27.5065119" type="hidden"/>
            <input class="form-control" name="longitude" id="longitude" value="83.4376749" type="hidden"/>
            <input class="form-control" name="created_by" id="created_by" value="{{Auth::id()}}" type="hidden"/>
            <div class="row">
                <div class="col-md-12">
                    <h4>Project Creation Details</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="project_name">Project Name<span class="required">*</span>
                        <span class="sl-text">({{__('app.project')}})</span>

                    </label>
                    <input class="form-control" placeholder="Project name" name="name" id="project_name"
                           required="required" type="text" maxlength="200"/>
                    @if ($errors->has('name'))
                        <span class="error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="project_type">Project_type <span class="required">*</span>
                        <span class="sl-text">({{__('app.ward_no')}})</span>

                    </label>
                    <select class="form-control" name="project_type_id" id="project_type" @change="getAllDistricts"
                            required="required">
                        <option value="">Please Select</option>
                        @foreach($projectTypes as $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('project_type'))
                        <span class="error">
                    <strong>{{ $errors->first('project_type') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="account_number">Account number<span
                                class="required">*</span> <span class="sl-text">({{__('app.account_number')}})</span>
                    </label>
                    <input class="form-control" placeholder="Account Number" name="account_name" id="account_name"
                           required="required" type="text" maxlength="200"/>
                    @if ($errors->has('name'))
                        <span class="error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="farmer_name">Name of the Farmer <span
                                class="required">*</span> <span class="sl-text">({{ __('app.farmer_name')}})</span>
                    </label>
                    <input class="form-control" placeholder="Farmer Name" name="farmer_name" type="text"
                          
                           value="{{ old('farmer_name', isset($farmer) ? $farmer[0]['farmer_name'] : "") }}"/>
                    @if ($errors->has('farmer_name'))
                        <span class="error">
                <strong>{{ $errors->first('farmer_name') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="contact_no">Sim_number Of creater<span
                                class="required">*</span> <span class="sl-text">({{__('app.contact_no')}})</span>

                    </label>
                    <input class="form-control" placeholder="Enter contact no" name="sim_number" id="contact_no"
                           type="text" value="{{ old('contact_no', isset($farmer) ? $farmer[0]['contact_no'] : "") }}"/>
                    @if ($errors->has('contact_no'))
                        <span class="error">
                <strong>{{ $errors->first('contact_no') }}</strong>
            </span>
                    @endif
                </div>
                <div class="col-md-12">
                    <label class="inline-label required" for="age">Age <span class="sl-text">({{__('app.age')}})</span></label>
                    <input class="form-control" placeholder="Enter age" name="age" id="age" type="number" min="0"
                           max="200" value="{{ old('age', isset($farmer) ? $farmer[0]['age'] : "") }}"/>
                    @if ($errors->has('age'))
                        <span class="error">
                <strong>{{ $errors->first('age') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="province">
                        <label class="inline-label" for="province_id">Province <span class="required">*</span><span
                                    class="sl-text">({{__('app.province')}})</span>

                        </label>
                        <select class="form-control" name="province_id" id="province_id" @change="getAllDistricts">
                            <option value="">Please Select</option>
                            @foreach($provinces as $province)
                                <option value="{{ $province->id }}"{!! old('province_id', isset($project)? $project->province_id : "")== $province->id? "selected":"" !!}>{{ $province->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('province_id'))
                            <span class="error">
                    <strong>{{ $errors->first('province_id') }}</strong>
                </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="district">
                        <label class="inline-label" for="district_id">District <span class="required">*</span><span
                                    class="sl-text">({{__('app.district')}})</span>

                        </label>
                        <select class="form-control" name="district_id" id="district_id" @change="getAllMunicipality">
                            <option value="" v-text="districtMessage"></option>
                            <option v-for="district in districts" v-bind:value="district.id">
                                @{{ district.name }}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label" for="municipality_id">Municipality/Village <span
                                class="required">*</span> <span
                                class="sl-text">({{__('app.municipality/gaunpalika')}})</span>

                    </label>
                    <select class="form-control" name="municipality_id" id="municipality_id">
                        <option value="" v-text="municipalityMessage"></option>
                        <option v-for="municipality in municipalities" v-bind:value="municipality.id">
                            @{{ municipality.name }}
                        </option>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="inline-label" for="ward_no">Ward No <span class="required">*</span> <span
                                class="sl-text">({{__('app.ward_no')}})</span>

                    </label>
                    <input class="form-control" placeholder="Ward Number" name="ward_no" id="ward_no" type="number"
                           value="{{ old('ward_no', isset($project) ? $project->ward_no : "") }}"/>
                    @if ($errors->has('ward_no'))
                        <span class="error">
                    <strong>{{ $errors->first('ward_no') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="address">Address<span class="required">*</span> <span
                                class="sl-text">({{__('app.address')}})</span>

                    </label>
                    <input class="form-control" placeholder="Enter address" name="address" id="Project_address"
                           required="required" type="text" maxlength="200"
                           value="{{ old('address', isset($farmer) ? $farmer[0]['address'] : "") }}"/>
                    @if ($errors->has('address'))
                        <span class="error">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
                    @endif
                </div>
                <div class="col-md-12">
                    <label class="inline-label required" for="contact_no">Contact no <span class="required">*</span>
                        <span class="sl-text">({{__('app.contact_no')}})</span>

                    </label>
                    <input class="form-control" placeholder="Enter contact no" name="contact_no" id="contact_no"
                           type="text" value="{{ old('contact_no', isset($farmer) ? $farmer[0]['contact_no'] : "") }}"/>
                    @if ($errors->has('contact_no'))
                        <span class="error">
                <strong>{{ $errors->first('contact_no') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="inline-label required" for="village_name">Village Name:
                        <br> <span class="sl-text">({{__('app.village_name')}})</span></label>

                    <input class="form-control" placeholder="Enter village name" name="village_name" id="village_name"
                           type="text"
                           value="{{ old('village_name', isset($farmer) ? $farmer[0]['village_name'] : "") }}"/>
                    @if ($errors->has('village_name'))
                        <span class="error">
                <strong>{{ $errors->first('village_name') }}</strong>
            </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label class="inline-label required" for="no_of_dependents">No of dependents <span class="required">*</span>:
                        <br><span class="sl-text">({{__('app.no_of_dependents')}})</span>
                    </label>
                    <input class="form-control" placeholder="Enter number of dependents" name="no_of_dependents"
                           id="no_of_dependents" type="number" min="0" max="1000" step="1"
                           value="{{ old('no_of_dependents', isset($farmer) ? $farmer[0]['no_of_dependents'] : "") }}"/>
                    @if ($errors->has('no_of_dependents'))
                        <span class="error">
                <strong>{{ $errors->first('no_of_dependents') }}</strong>
            </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="inline-label required" for="annual_household_income">Annual Household
                        Income({{displayUnitFormat('amount')}}) <span class="required">*</span>:
                        <br><span class="sl-text">({{__('app.annual_household_income')}})</span>
                    </label>
                    <input class="form-control" placeholder="Enter household income" name="annual_household_income"
                           id="annual_household_income" type="number"
                           value="{{ old('annual_household_income', isset($farmer) ? $farmer[0]['annual_household_income'] : "") }}"/>
                    @if ($errors->has('annual_household_income'))
                        <span class="error">
                <strong>{{ $errors->first('annual_household_income') }}</strong>
            </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label class="inline-label required" for="annual_expenses">Annual expenses
                        ({{displayUnitFormat('amount')}}) <span class="required">*</span>
                        <br><span class="sl-text">({{__('app.annual_expenses')}})</span>
                    </label>
                    <input class="form-control" placeholder="Enter annual name" name="annual_expenses"
                           id="annual_expenses" type="number"
                           value="{{ old('annual_expenses', isset($farmer) ? $farmer[0]['annual_expenses'] : "") }}"/>
                    @if ($errors->has('annual_expenses'))
                        <span class="error">
                <strong>{{ $errors->first('annual_expenses') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label class="inline-label required" for="education">Any person in the family has education greater
                        than 10 class? ({{displayUnitFormat('yes_no_question')}})
                        <br><span class="sl-text">({{__('app.education')}})</span>
                    </label>

                    <select class="form-control" name="education" id="education">
                        <option value="1" {!! old('education', isset($farmer[0]) ? $farmer[0]['education'] : "")== 1? "selected":"" !!}>
                            Yes
                        </option>
                        <option value="0" {!! old('education', isset($farmer[0]) ? $farmer[0]['education'] : "")== 0? "selected":"" !!}>
                            No
                        </option>
                    </select>
                    @if ($errors->has('education'))
                        <span class="error">
                <strong>{{ $errors->first('education') }}</strong>
            </span>
                    @endif
                </div>

                <div class="col-md-6">
                    <label class="inline-label required" for="certification">Have you had any formal education /
                        certification in farming ({{displayUnitFormat('yes_no_question')}})
                        <br><span class="sl-text">({{__('app.certification')}})</span>
                    </label>
                    <select class="form-control" name="certification" id="certification">
                        <option value="1" {!! old('certification', isset($farmer[0]) ?  $farmer[0]['certification'] : "")== 1 ? "selected":"" !!}>
                            Yes
                        </option>
                        <option value="0" {!! old('certification', isset( $farmer[0]) ? $farmer[0]['certification']:'') == 0? "selected":"" !!}>
                            No
                        </option>
                    </select>
                    @if ($errors->has('certification'))
                        <span class="error">
                <strong>{{ $errors->first('certification') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label class="inline-label required" for="gender">Gender <span class="required">*</span><span
                                class="sl-text">({{__('app.gender')}})</span>
                    </label>
                    <select class="form-control" name="gender" id="gender">
                        <option value="">Please Select</option>
                        <option value="male" {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "male"? "selected":"" !!}>
                            Male
                        </option>
                        <option value="female" {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "female"? "selected":"" !!}>
                            Female
                        </option>
                    </select>
                    @if ($errors->has('gender'))
                        <span class="error">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label class="inline-label required" for="no_of_people_in_house">No of People in house <span
                                class="required">*</span><span
                                class="sl-text">({{__('app.no_of_people_in_house')}})</span>
                    </label>
                    <input class="form-control" placeholder="Enter number of people in house"
                           name="no_of_people_in_house" id="no_of_people_in_house" type="number" min="0" max="150"
                           value="{{ old('no_of_people_in_house', isset($farmer) ? $farmer[0]['no_of_people_in_house'] : "") }}"/>
                    @if ($errors->has('no_of_people_in_house'))
                        <span class="error">
                <strong>{{ $errors->first('no_of_people_in_house') }}</strong>
            </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label class="inline-label required" for="has_bank">Bank Present
                        ({{displayUnitFormat('yes_no_question')}}) <span class="sl-text">({{__('app.has_bank')}})</span>
                    </label>
                    <select class="form-control" name="has_bank" id="has_bank">
                        <option value="">Please Select</option>
                        <option value="1" {!! old('has_bank', isset($farmer[0]) ? $farmer[0]['has_bank'] : "")== 1? "selected":"" !!}>
                            Yes
                        </option>
                        <option value="0" {!! old('has_bank', isset($farmer[0]) ? $farmer[0]['has_bank'] : "")== 0? "selected":"" !!}>
                            No
                        </option>
                    </select>
                    @if ($errors->has('has_bank'))
                        <span class="error">
                <strong>{{ $errors->first('has_bank') }}</strong>
            </span>
                    @endif
                </div>

                <div class="col-md-12" id="show_financial_institute">
                    <label class="inline-label required" for="financial_institute">Bank/Financial Institution
                    </label>
                    <input class="form-control" placeholder="Enter financial institution" name="financial_institute"
                           id="financial_institute" type="text"
                           value="{{ old('financial_institute', isset($farmer[0]['financial_institute']) ? $farmer[0]['financial_institute'] : "") }}"/>
                    @if ($errors->has('financial_institute'))
                        <span class="error">
                <strong>{{ $errors->first('financial_institute') }}</strong>
            </span>
                    @endif
                </div>
            </div>
          {{--  <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label for="farmer_photo">Select Farmers Image</label>
                        <input type="file" class="form-control-file" id="farmer_photo" name="farmer_photo" placeholder="select image" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="land_document">Select Farm Documentation</label>
                        <input type="file" class="form-control-file" id="land_document" name="land_document" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="citizenship">Select Citizenship</label>
                        <input type="file" class="form-control-file" id="citizenship" name="citizenship" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="bank_loan_application">Select bank loan application</label>
                        <input type="file" class="form-control-file" id="bank_loan_application" name="bank_loan_application" required>
                    </div>
                </div>
            </div>
--}}
            <div class="row button-area">
                <div class="col-sm-12">
                    <button class="btn btn-success" id="submit-analysis" name="submit-analysis" type="submit"
                            data-type="save" value="submit-analysis">Submit for Analysis
                    </button>
                    {{--@if(isset($project))
                        @if(strtolower($project->status) !='plan')
                            <button class="btn btn-default" type="submit" name="submit" value="submit">Save Draft
                            </button>
                        @endif
                    @else
                        <button class="btn btn-default" type="submit" name="submit" value="submit">Save Draft</button>
                    @endif--}}
                    <a class="btn btn-default pull-right" href="{{url('/ogbadmin/project')}}">Cancel</a>
                </div>
            </div>

        </form>
    </main>
@endsection
@section('js')
    <script src="/js/ogbadmin/project/project-create.js"></script>
@endsection