@extends('ogbadmin.layout')
{{--@extends('layouts.default')--}}

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/style.css')}}">

    <style>

        .checkbox-hide {
            display: none;
        }

        .pricingtable {
            transition: all 0.1s ease-in-out;
            position: relative;
        }

        .checked-icon-holder {
            position: absolute;
            color: #4CD964;
            width: 100%;
            left: 0;
            top: -32px;
            font-size: 48px;
        }

        .checkbox-hide:checked + label .pricingtable {
            border: 1px solid #4CD964;
            background-color: #F2FAF2;
            box-shadow: 0 18px 30px rgba(0, 0, 0, 0.1);
        }

        .checked-icon-holder {
            opacity: 0;
            transition: all 0.1s ease-in-out;
        }

        .checked-icon-holder .icon-done {
            position: relative;
            z-index: 1;
        }

        .checked-icon-holder .icon-done:after {
            content: '';
            display: block;
            width: 0.9em;
            height: 0.9em;
            background-color: #FFF;
            position: absolute;
            top: 0;
            left: 0.1em;
            z-index: -1;
            border-radius: 100%;
        }

        .checkbox-hide:checked + label .pricingtable .checked-icon-holder {
            opacity: 1;
        }

        /* body {
             margin: 0;
             font-size: 14.08px;
             font-family: "Fira Sans";
         }*/

        #myChart {
            height: 100% !important;
            width: 100% !important;
        }

        . /*title {
            font-size: 5vh;
            padding: 3vh 0vw;
            box-sizing: border-box;
            border-bottom: 1px solid gray;
        }*/
        .pumping-solutions {
            margin: 8vh 0;
            border: 1px solid gray;
            border-radius: 5px;
        }

        .pumping-solutions-title {
            display: flex;
            justify-content: space-between;
            padding: 2vh;
            font-weight: bold;
            font-size: 16px;
            border-bottom: 1px solid gray;
            box-sizing: border-box;
        }

        .pumping-indicator {
            display: flex;
            align-items: center

        }

        .pumping-icon {
            margin: 0 5px;
            height: 3px;
            width: 8px;
            background: blue;
        }

        .selection-area {
            text-align: center;
            margin: 20px 0;
        }

        .selection-area-title {
            font-size: 4vh;
        }

        .packages {
            margin: 5vh 0;
        }

        .package {
            width: 23%;
            position: relative;
        }

        .checkbox-area {
            height: 25px;
            width: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            position: absolute;
            right: 16px;
            top: 16px;
            border-radius: 50%;
            border: 1px solid gray;
        }

        .package-id {
            margin: 10px 0;
            text-transform: uppercase;
            font-size: .8rem;
        }

        .package-name {
            font-size: 4vh;
            font-weight: bolder;
        }

        .package-descriptions {
        }

        .package-description {
            display: flex;
            justify-content: space-between;
            padding: 15px 0;
            border-bottom: 1px solid rgba(0, 0, 0, 0.12);
        }

        .package-description-amount {
            font-weight: bolder;
        }

        .total-amount {
            font-size: 5vh;
            font-weight: bolder;
            margin: 10px 0;
        }

        .description {
            margin: 10px 0;
        }

        .custom-space {
            margin-top: 25px;
        }

        h5 {
            text-align: left;
        }

        .card-body > .container {
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .card-body > .container > .row {
            margin: 0 !important;
        }

        .row > .filezone {
            padding: 0;
            margin: 0;
            outline: none;
            border: 2px dashed rgba(0, 0, 0, .3);
            border-radius: 10px;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 160px;
        }

        .card-body {
            margin: 0;
            padding: 0;
        }

        .file-list {
            display: flex;
            flex-wrap: wrap;
            padding: 10px 0;
            box-sizing: border-box;
            border-bottom: 1px solid rgba(0, 0, 0, 0.3);
            /*justify-content: space-between;*/
            align-content: flex-start;
        }

        div.file-list > .file-listing:nth-child(3n) {
            border-right: none;
        }

        .file-list > .file-listing {
            width: 33%;
            border-right: 1px solid rgba(0, 0, 0, 0.3);
            margin-bottom: 10px;
            display: flex;
            align-items: center;
            margin: 10px 0;
            justify-content: space-between;
        }

        .image-holder > h5 {
            margin: 15px;
        }

        div.image-holder > img {
            height: 60px;
            width: 60px;
            border-radius: 8px;
            overflow: hidden;
            border: 1px solid rgba(0, 0, 0, .3);
        }

        .file-list > .file-listing .image-holder {
            display: flex;
        }

        @media screen and (min-width: 769px) {
            .select-plan-div > div:first-child {
                /*margin-left: 140px;*/
            }
            .pricingtable h1{
                font-size: 2.3VW;
            }
        }

        @media screen and (max-width: 768px) {
            .pricingtable {
                padding: 20px;
            }
        }

        .packages {
            display: flex;
            justify-content: space-between;
        }

        .package {
            width: 23%;
        }

        .checked-icon-holder {
            position: absolute;
            color: #4CD964;
            width: 100%;
            /* left: 0; */
            left: unset;
            right: 10px;
            text-align: right;
            top: 10px;
            font-size: 35px;
        }

        h2 {
            font-weight: 400;
        }

        .description {
            font: 18px;
        }

        input[type="file"] {
            opacity: 0;
            width: 100%;
            height: 200px;
            position: absolute;
            cursor: pointer;
        }

        .filezone {
            outline: 2px dashed grey;
            outline-offset: -10px;
            background: #fff;
            color: dimgray;
            padding: 10px 10px;
            min-height: 200px;
            position: relative;
            cursor: pointer;
            margin: 10px 0 0 0;
            width: 100%;
        }

        .filezone:hover {
            background: #c0c0c0;
        }

        .filezone p {
            font-size: 1.2em;
            text-align: center;
            padding: 50px 50px 50px 50px;
        }

        div.file-listing img {
            max-width: 90%;
        }

        div.file-listing {
            margin: auto;
            padding: 10px;
            /*border-bottom: 1px solid #ddd;*/
            width: 95%;
        }

        div.file-listing img {
            height: 60px;
        }

        div.success-container {
            text-align: center;
            color: green;
        }

        .remove {
            margin: 0 10px;
        }

        button.btn {
            font-weight: normal;
        }

        button.btn-outline-primary {
            border: 1px solid #4486F9;
        }

        div.remove-container {
            text-align: center;
            float: right;
        }

        div.remove-container a {
            color: red;
            cursor: pointer;
        }

        a.submit-button {
            display: block;
            margin: auto;
            text-align: center;
            width: 200px;
            padding: 10px;
            text-transform: uppercase;
            background-color: #CCC;
            color: white;
            font-weight: bold;
            margin-top: 20px;
        }

        .pricing li {
            display: flex;
            justify-content: space-between;
        }

        .backdrop, .confirm-box {
            position: fixed;
        }

        .backdrop {
            height: 100vh;
            width: 100%;
            background: rgba(0, 0, 0, .8);
            left: 0;
            top: 0;
            animation: appear .3s;
        }

        .confirm-box {
            background: white;
            border-radius: 5px;
            border: 1px solid rgba(0, 0, 0, .8);
            box-shadow: 2px 2px 8px rgba(0, 0, 0, .3);
            height: 30vh;
            width: 40vw;
            left: 30vw;
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            flex-direction: column;
            transition: .3s;
        }

        .appear-box {
            top: 20vh;
            opacity: 1;
        }

        .confirm-box > .confirm-button-area > button {
            margin: 0 10px;
            padding: 10px 15px;
        }

        .none {
            display: none
        }

        .block {
            display: block
        }

        .disappear-box {
            top: -50vh;
            opacity: 0;
        }

        @keyframes appear {
            from {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        .popup {
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            bottom: 15vh;
            transform: translateY(50vh);
            left: 35vw;
            height: 100px;
            width: 30vw;
            border-radius: 5px;
            box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.2);
            /*animation: appearPopUp .3s ease-in;*/
            padding: 8px 25px;
            /*border-radius: 25px;*/
            z-index: 9;
            opacity: 0;
            transition: .3s;
        }

        .popup.popup-appear {
            opacity: 1;
            transform: translateY(0);
        }

        .success-popup {
            color: #155724;
            background-color: #D4EDDA;
            border-color: #C3E6CB;
        }

        .error-popup {
            /*color: #856404;*/
            /*background-color: #FFF3CD;*/
            /*border-color: #FFEEBA;*/
            color: #721C24;
            background-color: #F8D7DA;
            border-color: #F5C6CB;
        }

        .disp-none {
            display: none !important;
        }

        .title-popup {
            font-size: 16px;
        }

        @keyframes appearPopUp {
            from {
                opacity: 0;
                transform: translateX(300px);
            }
            to {
                transform: translateX(0);
                opacity: 1;
            }
        }
    </style>
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
        <div class="popup error-popup" id="pop-message">
            <span class="title-popup"></span>
        </div>
        <div class="pumping-solutions">
            <div class="pumping-solutions-title">
                <span>{{__('app.monthly_water_requirement')}}</span>
                <div class="pumping-indicator">
                    <div class="pumping-icon"></div>
                    <div class="pumping-indicator-title">{{__('app.water_requirement')}}
                    </div>
                </div>
            </div>

            <div class="pumping-solutions-chart">
                <canvas id="myChart"></canvas>
            </div>
        </div>
        <div class="selection-area">
            <h1 class="selection-area-title">Select your Water Pumping Solution</h1>
            <div class="description">
                We analyzed water requirement for your crops throughout the year. Based on that, we suggest the Sahaj
                solar-powered water pumping solution with a daily water discharge of 180000 litres. You can also change
                the selection below to the solution that best suits you
            </div>
            <div class="description">
                {{__('app.select_plan_description1')}}
            </div>
            <form id="select-plan-form" method="post"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                <input class="form-control" name="project_id" id="Project_id" type="hidden" value="{{$project->id}}"/>
                <div class="packages select-plan-div">
                    @foreach($plans as $plan)
                        <div class="package form-group">
                            @if($plan->id == 1)
                                <input type="radio" name="Project[plan_id]" id="plan_{{$plan->id}}" value="{{$plan->id}}"
                                       class="checkbox-hide form-control"
                                       {{isset($selected_plan)? $plan->id == $selected_plan->id ? 'checked' : '':''}} data-cost="{{$plan->cost}}" disabled>
                            @else
                            <input type="radio" name="Project[plan_id]" id="plan_{{$plan->id}}" value="{{$plan->id}}"
                                   class="checkbox-hide form-control"
                                   {{isset($selected_plan)? $plan->id == $selected_plan->id ? 'checked' : '':''}} data-cost="{{$plan->cost}}">
                            @endif
                            <label for="plan_{{$plan->id}}" class="block">
                                <div class="well pricingtable">
                                    <div class="checked-icon-holder">
                                        <i class="icon-done"></i>
                                    </div>
                                    <p class="text-uppercase">{{$plan->code}}</p>
                                    <h1>{{$plan->name}}</h1>
                                    <ul class="pricing">
                                        <li><span>Daily Discharge</span><span class="text-right">{{$plan->daily_discharge}}
                                                L</span></li>
                                        <li><span>Solav PV Size</span><span class="text-right">{{$plan->solar_pv_size}}
                                                W</span></li>
                                        <li><span>Water Pump Size</span><span class="text-right">{{$plan->pump_size == ''? 0 : $plan->pump_size }}
                                                hp</span></li>
                                    </ul>
                                    <h2>Rs.{{$plan->plan_cost}}</h2>
                                </div>
                            </label>
                        </div>
                    @endforeach
                </div>
                <div class="row" id="select-plan">

                    @foreach($upload_attributes as $attribute_key =>$attribute)
                        <div class="col-md-12" style="margin-top: 25px;">
                            {{--<div class="card">--}}
                            {{--<div class="card-header" style="font-size: 14px;">Add {{$attribute}}</div>--}}
                            <h5>@if($attribute_key=='farmer_photo'){{__('app.enter_farmer_photo')}}
                                @elseif($attribute_key=='citizenship'){{__('app.citizenship')}}
                                @elseif($attribute_key=='land_document'){{__('app.land_documents')}}
                                @elseif($attribute_key=='bank_loan_application'){{__('app.bank_loan_documents')}}
                                @endif</h5>
                            <div class="card-body">
                                <upload-files :file-name="'{{$attribute_key}}'"
                                              :post_url="'ogbadmin/project/file/upload/'" id="file-upload" ref="refs"></upload-files>
                                {{--</div>--}}
                            </div>
                            @if(session()->has('file_upload_error'))
                                <span class="error"
                                      style="color:red;">{!! session()->get('file_upload_error') !!}</span>
                            @endif
                        </div>
                    @endforeach
                    <div class="col-sm-12 custom-space d-flex justify-content-between">
                        {{-- <button class="btn btn-outline-primary" formaction="{{route('ogbadmin.project.store-draft',['data'=>$project->id])}}">Save as draft</button></a> --}}
                        <div class="button-area">
                            <button class="btn btn-primary" @click="submitForm()"
                                    formaction="{{route('ogbadmin.project.selectedStore')}}">Submit for Financing
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="backdrop none" onclick="toggleConfirmBox()"></div>
        <div class="confirm-box disappear-box">
            <div class="message"><h3>Are you sure you want to remove?</h3></div>
            <div class="confirm-button-area">
                <button class="btn btn-success" onclick="removeContainerConfirm()">Confirm</button>
                <button class="btn btn-danger" onclick="toggleConfirmBox()">Cancel</button>
            </div>
        </div>
    </main>
@endsection
@section('js')
    <script src="{{asset('/offgrid-bazaar/js/vue.min.js')}}"></script>
    <script src="{{asset('/offgrid-bazaar/js/axios.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script type="text/javascript">
        var array =
                {!! json_encode($waterQuantity) !!}
        var ctx = document.getElementById('myChart').getContext('2d')
        var removeContainerId;
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: ['{{__('app.Baisakh')}}', '{{__('app.Jestha')}}', '{{__('app.Asar')}}', '{{__('app.Shrawan')}}', '{{__('app.Bhadra')}}', '{{__('app.Asoj')}}', '{{__('app.Kartik')}}', '{{__('app.Mangsir')}}', '{{__('app.Poush')}}', '{{__('app.Magh')}}', '{{__('app.Falgun')}}', '{{__('app.Chaitra')}}'],
                datasets: [{
                    label: '{{__('app.water_requirement')}}',
                    backgroundColor: 'rgb(0, 123, 255)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: {!! json_encode($waterQuantity) !!}
                }]
            },

            // Configuration options go here
            options: {}
        })
        const UploadFiles = Vue.component('upload-files', {
            template:`
            <div class="container">
                <div class="row">
                    <div class="large-12 medium-12 small-12 filezone">
                        <input type="file" :name="fileName" id="files" ref="files" multiple v-on:change="handleFiles(fileName)"/>
                        <p>Drag and drop your file here, or <button class="btn btn-outline-primary">Browse to Upload</button> </p>
                    </div>
                </div>
                <div class="file-list">
                <div v-for="(file, key) in files"   class="file-listing">
                     <div v-if ="file.id >0  && file.file_name!== undefined">
                        <div class="image-holder" v-if ="file.file_type != 'png' && file.file_type != 'jpg' && file.file_type != 'jpeg' && file.file_type != 'gif'">
                            <img class="preview" v-bind:src="'/offgrid-bazaar/images/generic.png'"/>
                           <h5> @{{file.actual_file_name}}</h5>
                        </div>
                        <div v-else class="image-holder">
                            <img class="preview" v-bind:src="relativePath(file.file_name,file.file_location)"/>
                           <h5> @{{file.actual_file_name}}</h5>
                        </div>
                     </div>
                     <div v-else class="image-holder">
                        <img class="preview" v-bind:ref="'preview'+parseInt(key)"/>
                           <h5> @{{file.name }}</h5>
                      </div>
                      <div class="remove-container">
                         <span :id="'span'+key" v-on:click="removeFile(key)" class="none"></span>
                            <a class="remove"  v-on:click="toggleConfirmBox('span'+key)"><i class='icon ion-md-trash' style="zoom:2.0;"></i></a>
                       </div>
                </div>
             </div>`,
            
            props: ['input_name', 'post_url', 'fileName'],
            data: function () {
                return {
                    confirm: true,
                    files: [],
                    filename: null,
                }
            },
            mounted(){
                vm = this;
               
                this.fetchFiles({{$project->id}}).then(data => {
                    Object.entries(data.uploads).map(files => {
                        if (this.fileName === files[1].upload_attribute_key) {
                            
                            this.files.push(files[1]);
                        }
                    })
                });

            },
            methods: {
                fetchFiles(projectId){
                    return axios.get(`/ogbadmin/project/file/get/${projectId}`).then(response => response.data);
                },
                removeFile(key){
                    this.deleteFile(key);
                    this.files.splice(key, 1);
                    this.getImagePreviews();
                },
                showPopUp(message){
                    document.querySelector(".popup").classList.add('popup-appear');
                    document.getElementById("pop-message").innerHTML = message;
                    setTimeout(() => {
                        document.querySelector(".popup").classList.remove('popup-appear');
                    }, 1500)
                },
                deleteFile(key){
                    var uploadId = this.files[key].id;
                    console.log(uploadId);
                    if (typeof uploadId == 'undefined') {
                        this.showPopUp("File couldnot be deleted due to unknown reasons");

                    }

                    axios.get('/ogbadmin/project/file/remove/' + uploadId)
                        .then(function (response) {
                            if (response.status != 200) {
                                this.showPopUp("File couldnot be deleted due to unknown reasons");
                            }
                            ;
                        });
                },
                handleFiles(fileName) {
                    let uploadedFiles = this.$refs.files.files;
                    for (var i = 0; i < uploadedFiles.length; i++) {
                        if (!(/\.(jpe?g|png|pdf|)$/i.test(uploadedFiles[i].name))) {
                            this.showPopUp("Please upload the files with extension jpg,jpeg,png,pdf");
                            return;
                        }
                        if ((uploadedFiles[i].size > 5242880)) {
                            this.showPopUp("Please upload files with size less than 5 MB");
                            return;
                        }
                        this.files.push(uploadedFiles[i]);
                    }
                    this.getImagePreviews();
                    this.submitFiles(fileName)
                },
                getImagePreviews(){
                    for (let i = 0; i < this.files.length; i++) {
                        if (/\.(jpe?g|png|gif)$/i.test(this.files[i].name)) {
                            let reader = new FileReader();
                            reader.addEventListener("load", function () {
                                this.$refs['preview' + parseInt(i)][0].src = reader.result;
                            }.bind(this), false);
                            reader.readAsDataURL(this.files[i]);
                        } else {
                            this.$nextTick(function () {
                                this.$refs['preview' + parseInt(i)][0].src = "{{asset('/offgrid-bazaar/images/generic.png')}}";
                            });
                        }
                    }
                },
                submitFiles(fileName) {
                    for (let i = 0; i < this.files.length; i++) {
                        if (this.files[i].id) {
                            continue;
                        }
                        let formData = new FormData();
                        formData.append(fileName, this.files[i]);
                        axios.post('/' + this.post_url + '{{$project->id}}',
                            formData,
                            {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }
                        ).then(function (data) {
                            this.files[i].id = data['data']['id'];
                            this.files.splice(i, 1, this.files[i]);
                        }.bind(this)).catch(function (data) {
                            console.log("error");
                        });
                    }
                },
                relativePath: function (name, location) {
                    let app_url = window.location.hostname;
                    let port = window.location.port;
                    let return_url = '';
                    let splitted_location = location.split("/")
                    let splitted_length = splitted_location.length;
                    port === '' ? return_url = `http://${app_url}/storage/${splitted_location[splitted_length - 2]}/${name}` : return_url = `http://${app_url}:${port}/storage/${splitted_location[splitted_length - 2]}/${name}`;
                    return return_url;
                }

            }
        });

        var app = new Vue({
            el: "#select-plan"
        });
        function toggleConfirmBox(id = '') {

            removeContainerId = '#' + id;
            document.querySelector('.confirm-box').classList.toggle('appear-box');
            document.querySelector('.confirm-box').classList.toggle('disappear-box');
            document.querySelector('.backdrop').classList.toggle('block');
            document.querySelector('.backdrop').classList.toggle('none');
        }
        function removeContainerConfirm() {
            if (removeContainerId) {
                document.querySelector(removeContainerId).click();
                toggleConfirmBox()
                //        console.log(document.querySelector(removeContainerId);
            }
        }

    </script>
    {{--<script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/71993dc7/modules/drilldown.src.js')}}/"></script>--}}
    {{--<script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/71993dc7/modules/exporting.src.js')}}"></script>--}}
@endsection