@extends('ogbadmin.layout')
@section('title')
    Project | Details
@endsection
@section('styles')
    <style>
        .text {
            font-weight: bold;
            font-size: 18px;
        }

        .card-header {
            display: flex;
        }

        .button_package {
            margin-left: 73%;
        }

        .button_packages {
            margin-left: 65%;
        }

        a, a:hover, a:active {
            text-decoration: none;
            color: #223740;
        }

        .disp {
            display: inline-block !important;
        }

        .for-edit-blade {
            width: 100%;
            position: fixed;
            right: 0;
            top: 0;
            background-color: rgba(0, 0, 0, 0.9);
            z-index: 1000;
            display: flex;
            justify-content: flex-end;
            color: #010538;
            height: 100vh;
        }

        .main-popup-form {
            overflow: auto;
            height: 100%;
            width: 30%;
            animation: appear .3s ease-in;
            /* color:white; */
            display: flex;
            flex-direction: column;
            background-color: white;
        }

        .form-header {
            /* padding: 10px 25px; */
            /*height: 80px;*/
            border-bottom: 2px solid rgba(0, 0, 0, 0.12);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .header-title {
            font-size: 20px;
            font-weight: bold;
            height: 58px;
            display: flex;
            align-items: center;
        }

        .form-header, .form-element > div, .form-footer {
            padding: 10px 25px;
        }

        .form-element {
            display: flex;
            background: white;
            flex-direction: column;
        }

        .id-project {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .project-farmer-image {
            height: 15vh;
            width: 15vh;
            border-radius: 50%;
            overflow: hidden;
            border: 1px dashed;
        }

        .project-farmer-image > input {
            display: none;
        }

        .project-farmer-image > label {
            height: 100%;
            width: 100%;
            object-fit: cover;
            object-position: center;
        }

        .project-farmer-image img {
            height: 100%;
            width: 100%;
            object-fit: cover;
            object-position: center;
        }

        .project-id, .payment-id {
            display: flex;
            flex-direction: column;
        }

        .id-header {
            font-size: 12px;
            font-weight: bolder;
            color: rgba(0, 0, 0, 0.5);
        }

        .id-body {
            font-weight: bold;
        }

        .form-main-body {
            width: 100%;
            display: flex;
            flex-direction: column;
            padding: 10px 0;
        }

        .form-main-body > * {
            margin: 10px 0;
        }

        .for-item label {
            font-weight: 400;
        }

        .form-item > input, .half-part > input, select {
            width: 100%;
            padding: 8px;
            border: 1px solid rgba(0, 0, 0, 0.3);
            border-radius: 5px;
        }

        .form-item-half {
            display: flex;
            justify-content: space-between;
        }

        .half-part {
            width: 46%;
        }

        input[type="date"]::-webkit-calendar-picker-indicator {
            color: rgba(0, 0, 0, 0);
            opacity: 1;
            display: block;
            background: url(https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png) no-repeat;
            width: 15px;
            height: 15px;
            border-width: thin;
        }

        .form-footer {
            display: flex;
            height: 80px;
            border-top: 2px solid rgba(0, 0, 0, 0.12);
            justify-content: flex-end;
            align-items: center;
        }

        .form-footer > .btn {
            margin: 0 10px;
        }

        .loader {
            position: fixed;
            height: 100vh;
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background: rgba(0, 0, 0, 0.1);
        }

        .message-box {
            height: 20%;
            width: 30%;
            color: black;
            background: white;
            font-size: 25px;
            font-weight: bold;
            border-radius: 10%;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .document-card:hover > .closex {
            display: block;
        }

        .closex {
            position: absolute;
            display: none;
            top: 5px;
            right: 5px;
            font-weight: bolder;
            font-size: 1.25rem;
            line-height: 1.25rem;
            animation: fadein .4s;
        }

        .document-content-box {
            height: 100%;
            overflow: auto;
        }

        .modal-content.document-modal-box {
            height: 88vh;
        }

        .document-modal-dialog {
            max-width: 80%
        }

        .closex > i {
            line-height: 24px;
        }

        #project-files .card-details {
            word-break: break-word;
        }

        #upload-document .upload-container {
            word-break: break-word;
        }

        .project-status {
            position: relative;
        }

        .user-thumbnail.profile-img-here {
            height: 275px;
            width: 275px;
            overflow: hidden;
            border-radius: 50%;
            position: relative;
            background: transparent;
        }

        .user-thumbnail.profile-img-here > label {
            width: 100%;
            background: rgba(0, 0, 0, 0.6);
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white;
            position: absolute;
            bottom: -50px;
            opacity: 0;
            transition: .3s;
            cursor: pointer;
        }

        .user-thumbnail.profile-img-here:hover > label {
            bottom: 0;
            opacity: 1;
        }

        .user-thumbnail.profile-img-here img {
            height: 100%;
            width: 100%;
            object-fit: cover;
            object-position: center;
        }

        .no-record-found {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .no-record-found svg {
            width: 70px;
            height: 70px;
        }

        @keyframes appear {
            from {
                opacity: 0;
                transform: translateX(300px);
            }
            to {
                transform: translateX(0);
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1
            }
        }

        .modal-body > img {
            width: 100%;
            height: 100%;
            object-fit: contain;
            object-position: center;
        }

        .hide {
            display: none;
        }

        .modal-close-btn {
            transform: scale(1.8);
            position: absolute;
            top: 3px;
            right: -7px;
            z-index: 99;
            background: #007bff;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 15px;
            width: 15px;
            border-radius: 50%;
            cursor: pointer;
            border: none;
        }

        .modal-close-btn > span {
            color: white;
            font-size: 1rem;
            line-height: 1rem;
        }

        .modal-body {
            padding: 0;
        }

        .change-picture {
            width: 100%;
            margin-top: -10%;
            margin-bottom: 5%;
        }

        #changePicture {
            display: none;
        }

        #pop-message {
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            bottom: 15vh;
            transform: translateY(50vh);
            left: 35vw;
            height: 50px;
            width: 30vw;
            border-radius: 5px;
            box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.2);
            padding: 8px 25px;
            /*border-radius: 25px;*/
            z-index: 9999;
            opacity: 0;
            transition: .3s;
        }

        #pop-message.popup-appear {
            opacity: 1;
            transform: translateY(0);
        }

        .success-popup {
            color: #155724;
            background-color: #D4EDDA;
            border-color: #C3E6CB;
        }

        .error-popup {
            /*color: #856404;*/
            /*background-color: #FFF3CD;*/
            /*border-color: #FFEEBA;*/
            color: #721C24;
            background-color: #F8D7DA;
            border-color: #F5C6CB;
        }

        .disp-none {
            display: none !important;
        }

        .title-popup {
            font-size: 16px;
        }
        .nav-link[disabled]:hover, .nav-link[disabled]:hover i, .nav-link.active[disabled], .nav-link.active[disabled] i{
            color:red!important;
        }
        .my-own-spacing-flex{
            justify-content: space-between;
            align-items: center;
        }

        @keyframes appearPopUp {
            from {
                opacity: 0;
                transform: translateX(300px);
            }
            to {
                transform: translateX(0);
                opacity: 1;
            }
        }
    </style>
@endsection
@section('content')
    <body class="position-relative" data-spy="scroll" data-offset="50">
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content" id="status-update">
        <div class="error-popup" id="pop-message">
            <span class="title-popup"></span>
        </div>
        <app-pop-up></app-pop-up>
        <div class="container-fluid">
            @if ($message = Session::get('info'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <meta name="project_id" content="{{ $project->id }}">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0">{{$project->getProjectType->name . ' for ' . $project->farmer_name}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr class="mb-0">
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <div class="col-md-12 position-relative">
                    <nav id="nav-sticky" class="navbar pr-0 pl-0">
                        <ul class="nav d-inline-flex">
                            <li class="nav-item mb-0">
                                <a href="#information" class="nav-link" {{!$project->delete_flg? ' ' :'disabled'}}><i class="icon ion-md-information-circle"></i>Information</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#financial-mix" class="nav-link" {{!$project->delete_flg? ' ' :'disabled'}}><i class="icon ion-md-cash"></i>Financial Mix</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#payments" class="nav-link"  {{!$project->delete_flg? ' ' :'disabled'}}><i class="icon ion-ios-cash"></i>Payments</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#equipment" class="nav-link"  {{!$project->delete_flg? ' ' :'disabled'}}><i class="icon ion-ios-flash"></i>Equipment</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#impact" class="nav-link" {{!$project->delete_flg? ' ' :'disabled'}}><i class="icon ion-md-stats"></i>Impact</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#project-files" class="nav-link"><i
                                            class="icon ion-md-document"></i>Document</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#comments" class="nav-link"><i class="icon ion-md-chatboxes"></i>Comments</a>
                            </li>
                            <li class="nav-item mb-0">
                                <a href="#logs" class="nav-link"><i class="icon ion-ios-paper"></i>Logs</a>
                            </li>
                        </ul>
                    </nav>

                    <hr class="mt-2 mb-5">

                    <section id="information" class="information">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <h5 class="mb-0">Information</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="file" name="file" id="file" style="display:none">
                                <input type="hidden" id="projectID" name="projectID" value="{{$project->id}}">

                                @if(!$image)
                                    <div class="user-thumbnail profile-img-here d-block mb-4">
                                        <img class="profile-img" src="{{asset('/img/avatar.png')}}">
                                        <label id="change-image-button" onclick="uploadFileDialog()">Select
                                            Image</label>
                                    </div>
                                @else
                                    <div class="user-thumbnail profile-img-here d-block mb-4">
                                        <img class="profile-img"
                                             src="{{ asset( 'storage/project_details/'.$image->file_name)}}">

                                        <label id="change-image-button" onclick="uploadFileDialog()">Select
                                            Image</label>
                                    </div>
                                    <br/>
                                @endif
                                <span id="uploaded_image"></span>
                                <app-funding-state id="{{$project->id}}" cost="{{$project->cost}}"></app-funding-state>
                                <h6 class="mb-3">Project Status</h6>
                                <div class="project-status dropdown-toggle d-flex align-items-center justify-content-between"
                                     href="#" data-toggle="dropdown" aria-expanded="false"
                                     @click="showPopup=!showPopup">

                                    <div class="status {{$project->status}}" id="projectStatus"
                                         v-text="getStatusCapitalCased">
                                        {{--{{$project->status}}--}}
                                    </div>
                                    <i class="icon ion-md-arrow-dropdown ml-4"></i>
                                    <app-available-status id="{{$project->id}}" role="{{session('role')}}"
                                                          v-show="showPopup"></app-available-status>
                                </div>


                            </div>
                            <div class="col-md-8 ml-3">
                                <p class="h3 font-weight-normal">{{$project->getProjectType->name . ' for ' . $project->farmer_name}}</p>
                                <div class="row">
                                    <div class="col-lg-8"><span class="d-flex align-items-center mr-3 mt-3 text-capitalize"><i
                                                    class="icon ion-md-pin text-primary mr-2"></i> {{$farmer->village_name}}
                                            Ward No. {{$project->ward_no}}, {{$project->district->name}}
                                            , Province No. {{$project->province_id}}, Nepal</span></div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-flash text-primary mr-2"></i> PEU 3.3 Kw</span>
                                    </div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-time text-primary mr-2"></i> Developed By {{ isset($project->getDevelopedBy->full_name) ? ($project->getDevelopedBy->full_name == 'root' ? 'Gham Power' : $project->getDevelopedBy->full_name) : 'Unknown' }}</span>
                                    </div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-phone-portrait text-primary mr-md-2"></i> {{$farmer->contact_no}}</span>
                                    </div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-people text-primary mr-2"></i> {{$farmer->non_earning_members}}
                                            Dependents</span></div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3 text-capitalize"><i
                                                    class="icon ion-md-male text-primary mr-2 "></i> {{$farmer->gender}}</span>
                                    </div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-people text-primary mr-2"></i> {{$farmer->no_of_people_in_house}}
                                            Family Members</span></div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-calendar text-primary mr-2"></i> {{$farmer->age}}
                                            Years Old</span></div>

                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-apps text-primary mr-2"></i> {{($farmer->educational_level==Null)?'No Education':'Passed '.$farmer->educational_level}}</span>
                                    </div>
                                    <div class="col-lg-4"><span class="d-flex align-items-center mr-3 mt-3"><i
                                                    class="icon ion-md-time text-primary mr-2"></i> Created on {{ \Carbon\Carbon::parse($project->created_on)->format('Y-m-d') }}</span>
                                    </div>
                                </div>

                                <hr class="mb-4">
                                <p id="project-edit-app">
                                    <app-modal v-show="isActive" :is-active-prop="id" @close="isActive=false"
                                    ></app-modal>
@if(!$project->delete_flg && session('role') != 1)
                                    <a href="#" class="btn btn-outline-primary mr-2"
                                       data-toggle="collapse" role="button"
                                       style="display:none"
                                       :class="{ 'disp' : getShowMakeInvestment}"
                                       @click="showInvestmentPopUp = !showInvestmentPopUp">Make
                                        Investment</a>
                                    <a href="#" class="btn btn-outline-primary mr-2"
                                       data-toggle="collapse" role="button"
                                       style="display:none"
                                       :class="{ 'disp' : getShowMakePayment}"
                                       @click="showPaymentPopUp = !showPaymentPopUp">Make Payment</a>
                                    @else
                                        <button href="#" class="btn btn-outline-primary mr-2"
                                           data-toggle="collapse" role="button"
                                           style="display:none"
                                           :class="{ 'disp' : getShowMakeInvestment}"
                                           disabled>Make
                                            Investment</button>
                                        <button href="#" class="btn btn-outline-primary mr-2"
                                           data-toggle="collapse" role="button"
                                           style="display:none"
                                           :class="{ 'disp' : getShowMakePayment}"
                                           disabled>Make Payment</button>
                                    @endif
                                    {{--<button class="btn btn-outline-primary mr-2"--}}
                                    {{--@click="changeStatusAndInputId({{ $project->id }})">Edit Information--}}
                                    {{--</button>--}}
                                    <a href="{{route('ogbadmin.project.edit',['data'=>$project->id])}}">
                                        <button class="btn btn-outline-primary mr-2"
                                        >Edit Information
                                        </button>
                                    </a>
                                    <a href="#" class="btn btn-outline-primary mr-2">Comment</a>

                                </p>

                                <app-project-investment
                                        payment_id="{{$project->payment_id}}" v-if="showInvestmentPopUp" @close="
                                showInvestmentPopUp = false"></app-project-investment>


                                <app-project-payment date="{{Carbon\Carbon::now()->toDateString()}}"
                                                     payment_id="{{$project->payment_id}}"
                                                     v-show="showPaymentPopUp" @close="showPaymentPopUp = false"
                                ></app-project-payment>

                                {{-- <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <form action="{{route('ogbadmin.project.investment.store')}}" method="post" id="cash-payment-form" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group form-row">
                                    <div class="col-md-6">
                                    <label>
                                    <span>Payment Name</span>
                                    <input type="text" name="payment_name" class="form-control" placeholder="emi or advance">
                                    </label>
                                    </div>
                                    <div class="col-md-6">
                                    <label>
                                    <span>Date of Payment</span>
                                    <input type="date" name="payment_date"
                                    class="form-control">
                                    </label>
                                    </div>
                                    <div class="col-md-6">
                                    <label><span>Date of Payment: <span
                                    class="required">*</span></span>
                                    <input type="date" class="form-control"
                                    placeholder="Date of Payment" name="payment_date"
                                    value="{{Carbon\Carbon::now()->toDateString()}}">
                                    @if ($errors->has('payment_date'))
                                    <span class="error">
                                    <p class="text-danger">{{ $errors->first('payment_date') }}</p>
                                    </span>
                                    @endif
                                    </label>
                                    </div>
                                    </div>
                                    <div class="form-group form-row">
                                    <div class="col-md-6">
                                    <label>
                                    <span>Payment Type</span>
                                    <input type="text" name="paymentGateway" class="form-control" placeholder="cash" id="payment_gateway" value="cash">
                                    </label>
                                    </div>
                                    <div class="col-md-6">
                                    <label>
                                    <span>Transaction Amount</span>
                                    <input type="text" name="amount" class="form-control" placeholder="amount">
                                    </label>
                                    </div>
                                    </div>
                                    <upload-files name="file[]"></upload-files>
                                    <div class="col-12">
                                    <div class="upload-wrapper mb-4">
                                    <div class="drag-n-drop text-center">
                                    <i class="icon ion-ios-cloud-upload d-block mb-2"></i>
                                    <span class="d-block mb-4">Drop your files here.</span>
                                    <label for="file-upload"
                                    class="btn btn-outline-primary browse">
                                    <span class="mb-0">Browse to Upload</span>
                                    <input id="file-upload" type="file"
                                    name="file_name[]" multiple>
                                    </label>


                                    </div>
                                    </div>
                                    <div class="document-wrapper">
                                    <ul id='document-list'>

                                    </ul>
                                    </div>
                                    </div>
                                    <input type="hidden" name="payment_id"
                                    value="{{$project->payment_id}}">
                                    <input type="hidden" name="payment_gateway" value="cash">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    </form>
                                </div> --}}


                            </div>
                            <div class="col-md-8">
                                <app-status-update id="{{$project->id}}" role="{{ session('role') }}" deleted="{{$project->delete_flg}}"></app-status-update>
                            </div>
                        </div>
                    </section>

                    <section id="financial-mix" class="financial-mix mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Financial Mix</h5>
                        <div class="row mb-5">
                            <div class="col-lg-3 col-sm-6">
                                <div class="info-card card-blue d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4>Rs. {{ $project->cost }}</h4>
                                        <div class="text-muted">Total Project Cost</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="info-card card-grey d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4 v-text="investmentPercentage+'%'"></h4>
                                        <div class="text-muted">Funding Status</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row">--}}
                        {{--<div class="col-lg-7 col-sm-12">--}}
                        {{--<div class="text-muted mb-4">Investment Breakdown</div>--}}
                        {{--<canvas id="project_status" class="col_canvas"></canvas>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-1"></div>--}}
                        {{--<div class="col-lg-4">--}}
                        {{--<div class="text-muted mb-4">EMI Breakdown</div>--}}
                        {{--<h3 class="section-title"></h3>--}}
                        {{--<div class="row mb-2">--}}
                        {{--<div class="col-md-6">EMI Gham Power</div>--}}
                        {{--<div class="col-md-6"><strong>Rs. 35,123.11</strong></div>--}}
                        {{--</div>--}}
                        {{--<div class="row mb-2">--}}
                        {{--<div class="col-md-6">EMI MFI</div>--}}
                        {{--<div class="col-md-6"><strong>Rs. 35,123.11</strong></div>--}}
                        {{--</div>--}}
                        {{--<div class="row mb-2">--}}
                        {{--<div class="col-md-6">EMI Other</div>--}}
                        {{--<div class="col-md-6"><strong>Rs. 35,123.11</strong></div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-6">EMI Grants</div>--}}
                        {{--<div class="col-md-6"><strong>Rs. 35,123.10</strong></div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                        {{--<hr>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row mb-2">--}}
                        {{--<div class="col-md-6">Total EMI for Farmer</div>--}}
                        {{--<div class="col-md-6"><strong>Rs. 1,40,492.43</strong></div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-6">Digital EMI</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<form action="" class="form-inline">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control mr-2">--}}
                        {{--<button type="submit" class="btn btn-primary">Save</button>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <app-payment-chart id="{{$project->id}}" role="{{session('role')}}"></app-payment-chart>
                    </section>

                    <section id="payments" class="section payments mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Payments</h5>
                        <div class="row mb-4">
                            <div class="col-md-4">
                                <div class="info-card card-green d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4>Rs. {{ number_format($project->cost) }}</h4>
                                        <div class="description">Total Project Cost</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-card card-blue d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4>Rs. {{number_format($project->total_emi)}}</h4>
                                        <div class="description">Total EMI</div>
                                    </div>
                                </div>
                            </div>
                            @if(session('role') != 1)
                            <div class="col-md-4">
                                <div class="info-card card-grey d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4>Rs. {{number_format($project->emi_amount)}}</h4>
                                        <div class="description">My EMI</div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-4 {{session('role') != 1 ? 'mt-4': ''}}">
                                <div class="info-card card-blue d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4 v-text="paymentDefaultedDetails.averageDefaultRate?paymentDefaultedDetails.averageDefaultRate+'%':0"></h4>
                                        <div class="description">Avg. Default Rate</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mt-4">
                                <div class="info-card card-orange d-flex justify-content-between">
                                    <div class="card-details">
                                        <h4 v-text="paymentDefaultedDetails.averageDefaultDays?paymentDefaultedDetails.averageDefaultDays:0"></h4>
                                        <div class="description">Avg. Default Days</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="payment-history" class="payment-history mt-5">
                        <hr class="mb-5">
                        <div class="d-flex justify-content-between">
                            <h5 class="mb-4">Payment History</h5>
                            <strong>Payment ID: {{ $project->payment_id }}</strong>
                        </div>
                        <div class="loading-content" v-if="!projectData.status"><i class="fa fa-circle-notch fa-spin"
                                                                                   style="margin-right:8px;"></i>Loading
                            Data
                        </div>
                        <app-payment-history id="{{$project->id}}"
                                             projectcost="{{ $project->advance_amount + $project->emi_amount*$project->emi_count }}"
                                             :status="projectData.status"></app-payment-history>
                        {{--<div class="row mb-5" v-if="projectData.status !== 'operational'">
                            <div class="col-lg-12">
                            <div class="no-record-found text-center mt-2">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0  60 60" xml:space="preserve">
                            <path d="M36.5,22h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S37,22,36.5,22z M11.5,16h10c0.6,0,1-0.4,1-1s-0.4-1-1-1h-10
                            c-0.6,0-1,0.4-1,1S10.9,16,11.5,16z M37.5,31c0-0.6-0.4-1-1-1h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25C37,32,37.5,31.6,37.5,31z M29.5,39
                            c0-0.6-0.4-1-1-1h-17c-0.6,0-1,0.4-1,1s0.4,1,1,1h17C29,40,29.5,39.6,29.5,39z M11.5,46c-0.6,0-1,0.4-1,1s0.4,1,1,1h14
                            c0.6,0,1-0.4,1-1s-0.4-1-1-1H11.5z M2.5,2h29v14h14v15h2V14.6L32.9,0H0.5v60h37v-2h-35C2.5,58,2.5,2,2.5,2z M33.5,3.4L44.1,14H33.5
                            V3.4z M59.2,58.3l-6-6.2c1.7-1.9,2.8-4.5,2.8-7.3c0-6-4.8-10.8-10.8-10.8s-10.8,4.8-10.8,10.8s4.8,10.8,10.8,10.8
                            c2.4,0,4.7-0.8,6.5-2.2l6,6.3c0.2,0.2,0.5,0.3,0.7,0.3c0.2,0,0.5-0.1,0.7-0.3C59.6,59.3,59.6,58.7,59.2,58.3z M36.5,44.8
                            c0-4.8,3.9-8.8,8.8-8.8s8.8,3.9,8.8,8.8s-3.9,8.8-8.8,8.8S36.5,49.6,36.5,44.8z"/>
                            </svg>
                            <p class="mt-3">No Record Found</p>
                            </div>
                            </div>
                        </div>--}}
                    </section>

                    <section id="equipment" class="equipments mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Equipment</h5>
                    </section>
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="text">Package Details</div>
                            <div class="button_package">
                                @if(session('role') == 50 && !$project->delete_flg) <a href="{{route('ogbadmin.project.customize-package',['data'=>$project->id])}}">
                                    <button id="customize-package" class="btn btn-outline-primary outlined-button">

                                        <i class="icon ion-ios-create"></i> Customize Package
                                    </button>
                                </a>
                                @else
                                    <a href="#">
                                        <button id="customize-package" class="btn btn-outline-primary outlined-button" disabled>

                                            <i class="icon ion-ios-create"></i> Customize Package
                                        </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th scope="col">Package Name</th>
                                    <th scope="col">Pump Size</th>
                                    <th scope="col">Pump Brand</th>
                                    <th scope="col">Solar PV Size</th>
                                    <th scope="col">Controller</th>
                                    <th scope="col">Maximum Daily Discharge</th>
                                    <th scope="col">System Price</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-capitalize">{{$project->name}}</td>
                                    <td>{{$project->pump_size}}{{($project->pump_size!='')?'hp':''}}</td>
                                    <td>{{$project->pump_brand}}</td>
                                    <td>{{$project->solar_pv_size}}{{($project->solar_pv_size!='')?'wP':''}}</td>
                                    <td>{{$project->controller}}</td>
                                    <td>{{$project->daily_discharge}}{{($project->daily_discharge!='')?'kL':''}}</td>
                                    <td>{{$project->cost}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card" style="width: 100%;margin-top: 5%">
                        <div class="card-header my-own-spacing-flex">
                            <div class="text">Meter Details</div>
                            <div class="button_packages" style="margin-left: 65%">
                                @if(session('role') == 50 && !$project->delete_flg)<a href="{{route('ogbadmin.project.create.meter',['data'=>$project->id])}}" v-if="!meterExist">
                                    <button class="btn btn-outline-primary outlined-button">
                                        {{--<i class="icon ion-ios-eye"></i>--}}
                                        Create Meter
                                    </button>
                                </a>
                                    @else
                                    <a href="#" v-if="!meterExist">
                                        <button class="btn btn-outline-primary outlined-button" disabled>
                                            {{--<i class="icon ion-ios-eye"></i>--}}
                                            Create Meter
                                        </button>
                                    </a>
                            @endif
                                    {{--{{route('ogbadmin.project.edit-meter',['data'=>$project->id])}}--}}
                                    <template v-if="meterExist">
                                        <a href="{{ route('ogbadmin.meterdata_demo.show',['project_id' => $project->id ]) }}">
                                            <button class="btn btn-outline-primary outlined-button">
                                                <i class="icon ion-ios-create"></i> View Meter
                                            </button>
                                        </a>

                                <a :href="'/ogbadmin/project/meter/edit/'+ projectData.meter_id">
                                    <button class="btn btn-outline-primary outlined-button">
                                        <i class="icon ion-ios-create"></i> Edit Meter
                                    </button>
                                </a>

                                    </template>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th scope="col">Meter Number</th>
                                    <th scope="col">Meter ID</th>
                                    <th scope="col">Meter Status</th>
                                    <th scope="col">Last Active</th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="meter in projectData.meters">
                                    <td v-text="meter.phoneNo"></td>
                                    <td v-text="meter.id"></td>
                                    <td v-text="meter.status ? 'Active' : 'Deactivated'"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    {{--<section id="impact" class="impact mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Impact</h5>
                        <div class="row mb-4">
                        <div class="col-lg-4 col-sm-6">
                        <div class="info-card card-light-blue d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>23</h4>
                        <div class="description">Projects Installed</div>
                        </div>
                        <img src="{{asset('/img/001-generator.png ')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                        <div class="info-card card-orange d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>10,304 kW</h4>
                        <div class="description">Solar Installed</div>
                        </div>
                        <img src="{{asset('/img/003-solar-panel.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                        <div class="info-card card-blue d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>10,304 Ha</h4>
                        <div class="description">Land Irrigated</div>
                        </div>
                        <img src="{{asset('/img/007-hydroponic.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>140</h4>
                        <div class="description">Women Farmers Served</div>
                        </div>
                        <img src="{{asset('/img/002-farmer.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>20,304 kWh</h4>
                        <div class="description">Clean Electricity Produced</div>
                        </div>
                        <img src="{{asset('/img/005-idea.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>$12,304,301</h4>
                        <div class="description">Private Captial Raised</div>
                        </div>
                        <img src="{{asset('/img/008-money.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>1,394</h4>
                        <div class="description">Lives Impacted</div>
                        </div>
                        <img src="{{asset('/img/004-family.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>1,304 tn/yr</h4>
                        <div class="description">Carbon-dioxide Curbed</div>
                        </div>
                        <img src="{{asset('/img/006-co2.png')}}" alt="">
                        </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 mt-4">
                        <div class="info-card card-grey d-flex justify-content-between align-items-center">
                        <div class="card-details">
                        <h4>$8,30,304.23</h4>
                        <div class="description">Amount Raised</div>
                        </div>
                        <img src="{{asset('/img/009-money-1.png')}}" alt="">
                        </div>
                        </div>
                        </div>
                    </section>--}}

                    <section id="project-files" class="project-files mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Project Document and Files</h5>
                        <div class="row mb-4">
                            @foreach($documents as $document)
                                <div class="col-md-3 mb-4" id="{{'document' . $document->id}}">
                                    <div class="document-card d-flex align-items-start position-relative">
                                        <i class="icon ion-md-folder-open mr-3"></i>
                                        <div class="card-details">
                                            <a href="#showDocument{{$document->id}}" class="openDocumentDialog"
                                               data-toggle="modal"
                                               data-id="{{asset('storage/project_details/'. $document->file_name)}}"
                                               data-target="#showDocument{{$document->id}}">
                                                <div class="document-title">
                                                    <strong>{{$document->actual_file_name}}</strong></div>
                                                <div class="description">{{$document->file_type}} - {{$document->size}}
                                                    MB
                                                </div>
                                            </a>
                                        </div>
                                        <span class="closex" data-toggle="modal"
                                              data-target="#deleteDocument{{$document->id}}"><i
                                                    class="icon ion-md-close-circle"></i></span>
                                    </div>
                                </div>

                                <div class="modal fade show" id="showDocument{{$document->id}}" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered document-modal-dialog">
                                        <button type="button" class="modal-close-btn" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <div class="modal-content document-modal-box p-4 p-md-4">
                                            <div class="row document-content-box justify-content-center">

                                                <div class="modal-body">
                                                    <img id="image" class="img-responsive" src="" alt="">
                                                    <embed src="" id="document" type="application/pdf" width="100%"
                                                           height="100%">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade delete" id="deleteDocument{{$document->id}}" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content p-4 p-md-5">
                                            <div class="row  justify-content-center">
                                                <div class="col-12 text-center">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <p>Are you sure you want to delete the document?</p>
                                                    <a href="" data-id="{{$document->id}}"
                                                       class="btn btn-primary pl-4 pr-4 delete-document">Yes</a>
                                                    <a href="#" class="btn btn-outline-primary mr-
                                                                3 pl-4 pr-4 cancel-document" data-dismiss="modal">No</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            <div class="col-md-3 mb-4">
                                <div class="document-card upload-document d-flex align-items-start" data-toggle="modal"
                                     data-target="#upload-document">
                                    <i class="icon ion-md-add-circle mr-3"></i>
                                    <div class="card-details">
                                        <div class="document-title">Upload Document</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>

                    {{--<section id="comments" class="comments mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Add Comment</h5>
                        <form method="POST" action="{{route('ogbadmin.comment.store')}}">
                            {{csrf_field()}}
                            <div class="d-flex mb-5">
                                <span class="user-thumbnail text-center text-white mr-3">H</span>
                                <div class="form-group w-100">
                                    <textarea type="text" name="comment_body" class="form-control d-block"
                                              rows="3"></textarea>
                                    <input type="hidden" name="project_id" value="{{$project->id}}">
                                    <button class="btn btn-primary mt-3">Post Comment</button>
                                </div>
                            </div>
                        </form>
                    </section>--}}

                    <section id="logs" class="logs mt-5">
                        <hr class="mb-5">
                        <h5 class="mb-4">Logs</h5>
                        <app-project-log id="{{$project->id}}"></app-project-log>
                    </section>
                    <div class="scroll-spy">
                    </div>
                </div>
            </div>

        </div>
    <!--  <div class="modal fade" id="upload-picture" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-4 p-md-5">
                    <form action="" method="post"
                          class="form-row mt-4" enctype="multipart/form-data">
                        {{ csrf_field() }}

            <div class="row  justify-content-center">
                <div class="col-12 text-center mb-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                                <h3>Upload Documents</h3>
                            </div>

                            <div class="col-12">
                                <div class="upload-wrapper mb-4">
                                    <div class="drag-n-drop text-center">
                                        <i class="icon ion-ios-cloud-upload d-block mb-2"></i>
                                        <span class="d-block mb-4">Drop your files here.</span>
                                        <label for="file-upload" class="btn btn-outline-primary browse">
                                            <span class="mb-0">Browse to Upload</span>
                                            <input id="file-upload" type="file" name="file_name[]" multiple>
                                        </label>


                                    </div>
                                </div>
                                <div class="document-wrapper">
                                    <ul id='document-list'>

                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div> -->

        <div class="modal fade" id="upload-document" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-4 p-md-5">
                    <form action="{{route('ogbadmin.project.upload.details',['data'=>$project->id])}}" method="post"
                          class="form-row mt-4" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row  justify-content-center">
                            <div class="col-12 text-center mb-3">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h3>Upload Documents</h3>
                            </div>

                            <div class="col-12">
                                <div class="upload-wrapper mb-4">
                                    <div class="drag-n-drop text-center">
                                        <i class="icon ion-ios-cloud-upload d-block mb-2"></i>
                                        <span class="d-block mb-4">Drop your files here.</span>
                                        <label for="file-upload" class="btn btn-outline-primary browse">
                                            <span class="mb-0">Browse to Upload</span>
                                            <input id="file-upload" type="file" name="file_name[]"  onchange="validate_doc_upload(this)" multiple>
                                        </label>


                                    </div>
                                </div>
                                <div class="document-wrapper">
                                    <ul id='document-list'>

                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-9">
                                        <select class="form-control" id="inputGroupSelect01" name="category_id"
                                                required>
                                            <option selected>Select Category</option>
                                            <option v-for="uploadType in uploadsTypes" :value="uploadType.id">
                                                @{{uploadType.name}}
                                            </option>
                                            {{--<option value="2">Contract with Farmer</option>--}}
                                            {{--<option value="3">Contract with Partner</option>--}}
                                            {{--<option value="4">Other Options</option>--}}
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary" onclick="validate_doc_upload()" >Upload</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </main>


    {{--</div>--}}
    {{--</div>--}}
    </body>

@endsection
@section('js')
    {{--<script src="{{@asset('js/ogbadmin/jquery.min.js')}}"></script>--}}
    {{--<script src="{{@asset('js/ogbadmin/popper.min.js')}}"></script>--}}
    {{--<script src="{{@asset('js/ogbadmin/bootstrap.min.js')}}"></script>--}}
    {{--<script src="{{@asset('js/ogbadmin/Chart.min.js')}}"></script>--}}
    {{--<script src="{{@asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>--}}
    <script type="text/javascript">
        function uploadFileDialog() {
            document.querySelector('#file').click();
        }
        function validate_doc_upload(obj){
            // var allowed_extensions = new Array("jpg","png","gif");

            var control = document.getElementById("file-upload");
            var filelength = control.files.length;
            var FileName = obj.value;
            var FileExt = FileName.substr(FileName.lastIndexOf('.') + 1);
            var error = ''
            // if ((FileExt.toUpperCase() != "PDF")){
            //     if ((FileExt !== "pdf") || (FileExt !== "PDF")){
            //         error = "File type : " + FileExt + "\n\n";
            //         error += "Please make sure your file is in pdf  format .\n\n";
            //         showPopUp(error);
            //         obj.value = '';
            //         return false;
            //     }
            // }
            if(control.files.item(this).size > 5242880 ){
                error += "Max file size is 5 MB\n\n";
                showPopUp(error);
                obj.value = '';
                return false;
            }
            return true;
        }

        function showPopUp(message) {
//            console.log("the message is "+message);
            if (message) {
                document.querySelector("#pop-message").classList.add('popup-appear');
                document.getElementById("pop-message").innerHTML = message;
                setTimeout(() => {
                    document.querySelector("#pop-message").classList.remove('popup-appear');
                }, 1500)
            }

        }
        $(document).ready(function () {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            $(document).on('change', '#file', function () {
                var f = document.getElementById("file").files[0];
                var name = f.name;
                var id = document.getElementById('projectID').value;
                var form_data = new FormData();
                var ext = name.split('.').pop().toLowerCase();
                if (jQuery.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                    showPopUp('Please upload files with extension jpg,jpeg,png');
                    return false;
                }
                if (f.size > 5242880) {
                    showPopUp("Please upload files with size less than 5 MB");
                    return false;
                }

                var oFReader = new FileReader();
                oFReader.readAsDataURL(f);
                form_data.append("file", f);
                console.log(form_data.entries);
                $.ajax({
                    url: "/ogbadmin/project/change-profile-picture/" + id,
                    method: "POST",
//                  data: JSON.stringify({id:id,form_data:form_data}),
                    data: form_data,

                    contentType: false,
                    /*cache: false,*/
                    processData: false,
                    beforeSend: function () {
                        $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
                    },
                    success: function (data) {
                        data = data.trim()
                        {{--console.log('{!! asset( 'storage/project_details/') !!}'+"/"+data);--}}
                        $('#uploaded_image').hide();
                        document.querySelector('.profile-img').setAttribute('src', "{!! asset( 'storage/project_details/') !!}" + "/" + data)

                    }
                });
            });
            if ($(window).width() >= 767) {
                $('#nav-sticky').stick_in_parent()
            }

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            // var projectStatus = document.getElementById('project_status')
            // projectStatus.height = 70
            //
            // var project_status = new Chart(projectStatus, {
            //   type: 'doughnut',
            //   data: {
            //     datasets: [{
            //       data: [25, 20, 30, 10, 15],
            //       backgroundColor: ['#03A9F4', '#FF8C63', '#66BB6A', '#EF5350', '#EF5350']
            //     }],
            //     labels: ['Gham Power', 'MFI Partner', 'Farmer Equity', 'Other (OGB / Crowd Funding)', 'Funding Gap']
            //   },
            //   options: {
            //     legend: {
            //       position: 'right',
            //       fontSize: 14,
            //       labels: {
            //         fontSize: 14
            //       },
            //       onClick: null
            //     }
            //   }
            // })

        })
    </script>
    <script>
        var status ={!! json_encode($project->status) !!}
    </script>
    {{--displaying filename--}}
    <script>
        var files = []
        var uploadInput = document.getElementById('file-upload')
        function removeBackdrop() {
            console.log(123);
            document.querySelectorAll('.modal-backdrop.fade').forEach(backdrop => backdrop.remove());
        }
        function FileListItem(a) {
            a = [].slice.call(Array.isArray(a) ? a : arguments)
            for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
            if (!d) throw new TypeError('expected argument to FileList is File or array of File objects')
            for (b = (new ClipboardEvent('')).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
            return b.files
        }

        function removeListItem(a) {
            let div = document.getElementById('document-list-item' + a)
            div.parentNode.removeChild(div)
            files.splice(a, 1)
            uploadInput.files = new FileListItem(files)
            console.log(files)
        }

        $(document).ready(function () {
            $('.document-wrapper').hide();
            $('#file-upload').change(function (e) {
                var fileName = e.target.files
                files = [...files, ...fileName]
                console.log(files)
                document.getElementById('document-list').innerHTML = ''
                for (var i = 0; i < files.length; i++) {
                    var f = files[i]
                    document.getElementById('document-list').innerHTML += '<li id=\'document-list-item' + i + '\'>' +
                        '<a href=\'#\' id=\'delete-list-item' + i + '\' onclick=\'removeListItem(' + i + ')\' class=\'delete delete-file\'><i class=\'icon ion-md-trash\'></i></a>' +
                        ' <div class=\'upload-container d-flex\'>' +
                        '<div class=\'success-icon mr-3\'><i class=\'icon ion-md-checkmark-circle\'></i></div>' +
                        ' <div class=\'file-description\'>' +
                        '<strong id=\'document-name\'>' + f.name + '</strong>' +
                        "<small class='text-uppercase text-muted bold d-block'>{{date('M d, Y')}}</small>" +
                        ' </div>' +
                        '</div>' +
                        '</li>'

                    $('.document-wrapper').show()
                }
                uploadInput.files = new FileListItem(files)
                console.log(uploadInput.files)
            })

            $('.delete-file').on('click', function (evt) {
                evt.preventDefault()
                $('.document-wrapper').hide()

            })
        })
    </script>
    {{--delete document--}}
    <script>
        $(document).ready(function () {

            $('.delete-document').click(function (evt) {
                evt.preventDefault()
                var id = $(this).data('id')
                $.ajax({
                    type: 'GET',
                    url: '/ogbadmin/project/delete-document/' + id,
                    data: {
                        id: id
                    },
                    success: function (result) {
                        var result = result.trim()
                        if (result == 'success') {
                            $(`#document${id}`).css('display', 'none')
                            $('.cancel-document').click();
//                  removeBackdrop();
                        }
                    }
                })
            })
        })

        //opening documents in dialog box
        $(document).on("click", ".openDocumentDialog", function () {
            var myDocumentPath = $(this).data('id');
            if (myDocumentPath.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                $(".modal-body #document").css('display', 'none');
                $(".modal-body #image").css('display', 'block');
                $(".modal-body #image").attr("src", myDocumentPath);
            }
            else {
                $(".modal-body #image").css('display', 'none');
                $(".modal-body #document").css('display', 'block');
                $(".modal-body #document").attr("src", myDocumentPath);
            }
        });

    </script>
    <script src="{{asset('js/ogbadmin/project/project-status.js')}}"></script>
    {{--<script src="{{asset('js/ogbadmin/project/project-detail.js')}}"></script>--}}
    {{--<script src="{{asset('js/ogbadmin/project/project-modal.js')}}"></script>--}}
@endsection
<script>
    import UploadFile from '../../../assets/js/ogbadmin/project/Components/UploadFile'

    export default {
        components: {UploadFile}
    }
</script>