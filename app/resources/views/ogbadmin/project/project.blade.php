@extends('ogbadmin.layout')
@section('title')
    Create | Project
@endsection
@section('styles')
    <style type="text/css">
        body {
            margin: 0;
        }

        .backgroundedCard {
            background: #F5F7FA;
            padding: 10px;
            border-radius: 5px;
            margin: 10px 0px;
        }

        table.gridView {
            min-width: 750px;
        }

        section {
            height: 83vh;
            width: 100%;
        }

        .project-create-app > form {
            margin: 0;
            margin-block-end: 0;
        }

        span.radio {
            min-width: 100px;
        }

        .gridView > tbody > tr > td:nth-last-child(1) {
            display: flex;
        }

        .gridView > tbody > tr > td:nth-last-child(1) > a {
            margin: 3px;
        }

        .multistep {
            height: 81vh;
            width: 100%;
            display: flex;
            4444 overflow-y: auto;
        }

        .multistep::-webkit-scrollbar {
            width: 4px;
        }

        .multistep::-webkit-scrollbar-track {
            width: 4px;
            background: rgba(0, 0, 0, 0.12);
            /*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);*/
        }

        .multistep::-webkit-scrollbar-thumb {
            background-color: #007bff;
            border-radius: 30px;
            width: 4px;
            outline: 1px solid slategrey;
        }

        .steps-section {
            width: 40%;
            height: 100%;
            position: sticky;
            top: 0;
            display: flex;
            justify-content: flex-end;
            padding: 0 4vw;
            box-sizing: border-box;
        }

        .step-background {
            left: 0;
            top: 0;
            background: #f2f4f7;
            opacity: 1;
            position: absolute;
            height: 100%;
            z-index: 0;
            width: 85%;
        }

        .steps {
            display: flex;
            flex-direction: column;
            z-index: 2;
            position: relative;
            align-items: flex-end;
        }

        .step {
            margin: 20px 0;
            display: flex;
            align-items: center;
        }

        .step-label {
            margin: 0 20px;
        }

        .step-number {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 2vw;
            width: 2vw;
            border-radius: 2.5vw;
            border: 2px solid gray;
        }

        .row > .form-group > .row > .form-group {
            display: flex;
        }

        .row > .form-group > .row > .form-group > .radio {
            margin: 0 30px;
        }

        .row > .form-group > .row > .form-group > label {
            width: unset !important;
        }

        .form-group > table.gridView {
            min-width: unset !important;
        }

        .form-group > .radio > label {
            display: flex;
        }

        .active-step > .step-label {
            color: #007bff;
        }

        .active-step > .step-number {
            background: #007bff;
            border-color: #007bff;
            color: white;
        }

        .progress-bar {
            width: 2px;
            background: gray;
            height: 40px;
            margin-right: 1vw;
            position: relative;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .active-progress-bar {
            background: #007bff;
        }

        .active-progress-bar::before, .active-progress-bar::after {
            background: #007bff !important;
        }

        .progress-bar::before, .progress-bar::after {
            content: '';
            height: 6px;
            width: 6px;
            border-radius: 3px;
            background: gray;
            position: absolute;
        }

        .progress-bar::before {
            bottom: 100%
        }

        .progress-bar::after {
            top: 100%;
        }

        .forms {
            min-height: 100%;
            width: 59%;
            padding: 2.5vh;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
        }

        .form {
            flex: 1;
            width: 100%;
            display: none;
        }

        .form > .row > div {
            margin-bottom: 15px;
        }

        .buttons-area {
            display: flex;
            align-items: center;
            justify-content: space-between;
            position: sticky;
        }

        .back-mode {
            justify-content: flex-end;
        }

        .back-mode > #back, .back-mode > #submitButtons {
            display: none;
        }

        .step-mode > #submitButtons {
            display: none;
        }

        .submit-mode > #next {
            display: none;
        }

        .form input + div, select + div {
            color: red;
        }

        .form input {
            border-radius: 3px;
        }

        .form-error {
            border-color: red;
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, .25);
        }

        #buttonsArea {
            display: flex;
        }

        #buttonsArea > button {
            margin: 0 3px;
            font-size: .8rem;
        }

        #saveChanges {
            font-size: .8rem;

        }

        #submit {
            width: 10px;
            right: 0;
            font-size: 0.8rem;
            display: none;
        }

        #submitButtons {
            margin: 0 3px;
            position: relative;
            min-width: 180
            font-size: 0.8rem;

        }

        #submitButtons > button {
            position: absolute;
            top: 0;
            left: 0;
            font-size: 0.8rem;

        }

        .outlined-button {
            color: #007BFF;
            background-color: transparent;
        }

        .add_income, .add_expense, .add_land, .add_crop {
            float: right;
            margin: 0 1.2rem;
            font-size: 0.8rem;
        }

        .remove_income, .remove_expense, .remove_land, .remove_crop {
            width: 30px;
            height: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 30px;
        }

        .rupee-input {
            position: relative;
        }

        .rupee-input::before {
            content: 'Rs.';
            position: absolute;
            left: 30px;
            top: 25px;
            min-width: 12px;
        }

        .rupee-input > input {
            padding-left: 40px;
        }

        .form-group.rupee-input::before {
            top: 38px;
        }
    </style>
@endsection
@section('content')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-success alert-dismissible fade show" id="formMessage" role="alert">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endif
    <section>
        <main id="project-create-app">
            <form id="msform" method="post" enctype="multipart/form-data" action="{{route('ogbadmin.project.store')}}">
                {{ csrf_field() }}
                <input class="form-control" name="is_draft" id="is_draft" value="1" type="hidden"/>
                <input class="form-control" name="latitude" id="latitude" value="27.5065119" type="hidden"/>
                <input class="form-control" name="longitude" id="longitude" value="83.4376749" type="hidden"/>
                <input class="form-control" name="created_by" id="created_by" value="{{Auth::id()}}" type="hidden"/>

                <div class="multistep">
                    <div class="steps-section">
                        <div class="step-background"></div>
                        <div class="steps">
                            <div class="step active-step">
                                <div class="step-label"><b>{{__('app.basic_information')}}</b></div>
                                <div class="step-number">१</div>
                            </div>
                            <div class="progress-bar"></div>
                            <div class="step">
                                <div class="step-label"><b>{{__('app.other_information')}}</b></div>
                                <div class="step-number">२</div>
                            </div>
                            <div class="progress-bar"></div>
                            <div class="step">
                                <div class="step-label"><b>{{__('app.current_condition')}}</b></div>
                                <div class="step-number">३</div>
                            </div>
                            <div class="progress-bar"></div>
                            <div class="step">
                                <div class="step-label"><b>{{__('app.project_type')}}</b></div>
                                <div class="step-number">४</div>
                            </div>
                            <div class="progress-bar"></div>
                            <div class="step">
                                <div class="step-label"><b>{{__('app.farm_related_information')}}</b></div>
                                <div class="step-number">५</div>
                            </div>
                        </div>
                    </div>
                    <div class="forms">
                        <div class="form" style="display:block">
                            <h3>{{__('app.basic_information')}}</h3>
                            <div class="row">
                                <div class=" form-group col-md-12">
                                    <label class="inline-label " for="farmer_name">{{ __('app.farmer_name')}} <span
                                                class="required">*</span>
                                    </label>
                                    <input class="form-control" placeholder="Please Input Farmer Name and Surname"
                                           id="farmer_name"
                                           name="farmer_name" type="text"
                                           {{--required="required" maxlength="200"--}}
                                           value="{{ old('farmer_name', isset($farmer) ? $farmer[0]['farmer_name'] : "") }}"/>
                                    <div></div>
                                    @if ($errors->has('farmer_name'))
                                        <span class="error">
                                        <strong>{{ $errors->first('farmer_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="gender">{{__('app.gender')}} <span
                                                class="required">*</span>
                                    </label>
                                    <div class="radio" style="display:block;width:10px">
                                        <label for="male"><input type="radio" name="gender" value="male" id="male"
                                                      {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "male"? "selected":"" !!} checked>{{__('app.male')}}
                                        </label>
                                    </div>
                                    <div class="radio" style="display:block;width:10px">
                                        <label for="female"><input type="radio" name="gender" id="female"
                                                      value="female" {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "female"? "selected":"" !!}>{{__('app.female')}}
                                        </label>
                                    </div>

                                    <div></div>

                                    @if ($errors->has('gender'))
                                        <span class="error">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
                                    @endif
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="age">{{__('app.age')}} <span
                                                class="required">*</span></label>
                                    <input class="form-control" placeholder="Please Input Age" name="age" id="age"
                                           type="number" min="0"
                                           max="200" value="{{ old('age', isset($farmer) ? $farmer[0]['age'] : "") }}"/>
                                    <div></div>
                                    @if ($errors->has('age'))
                                        <span class="error">
                <strong>{{ $errors->first('age') }}</strong>
            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="address">{{__('app.address')}} <span
                                                class="required">*</span>
                                    </label>
                                    <input class="form-control"
                                           placeholder="Tole Name,Municipal Level-(Ward No.),District" name="address"
                                           id="Project_address"
                                           type="text"
                                           {{--required="required" type="text" maxlength="200"--}}
                                           value="{{ old('address', isset($farmer) ? $farmer[0]['address'] : "") }}"/>
                                    <div></div>
                                    @if ($errors->has('address'))
                                        <span class="error">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
                                    @endif
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="contact_no">{{__('app.contact_no')}}<span
                                                class="required">*</span>

                                    </label>
                                    <input class="form-control" placeholder="Please Input Contact Number"
                                           name="contact_no"
                                           id="contact_no"
                                           type="text"
                                           value="{{ old('contact_no', isset($farmer) ? $farmer[0]['contact_no'] : "") }}"/>
                                    <div></div>
                                    @if ($errors->has('contact_no'))
                                        <span class="error">
                <strong>{{ $errors->first('contact_no') }}</strong>
            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="contact_no">{{__('app.email')}}
                                    </label>
                                    <input class="form-control" placeholder="Please Input Email Address" name="email"
                                           id="email" type="text"
                                    />
                                    <div></div>

                                </div>
                            </div>
                        </div>
                        <div class="form">
                            <h3>{{__('app.other_information')}}</h3>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="province">
                                        <label class="inline-label" for="province_id">{{__('app.province')}} <span
                                                    class="required">*</span>
                                        </label>
                                        <select class="form-control" name="province_id" id="province_id"
                                                @change="getAllDistricts">
                                            <option value="">Select Province</option>
                                            @foreach($provinces as $province)
                                                <option value="{{ $province->id }}"{!! old('province_id', isset($project)? $project->province_id : "")== $province->id? "selected":"" !!}>{{ $province->name }}</option>
                                            @endforeach
                                        </select>
                                        <div></div>
                                        @if ($errors->has('province_id'))
                                            <span class="error">
                                            <strong>{{ $errors->first('province_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="district">
                                        <label class="inline-label" for="district_id">{{__('app.district')}} <span
                                                    class="required">*</span>

                                        </label>
                                        <select class="form-control" name="district_id" id="district_id"
                                                @change="getAllMunicipality">
                                            <option value="" v-text="districtMessage"></option>
                                            <option v-for="district in districts" v-bind:value="district.id">
                                                @{{ district.name }}
                                            </option>
                                        </select>
                                        <div></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label"
                                           for="municipality_id">{{__('app.municipality/gaunpalika')}} <span
                                                class="required">*</span>
                                    </label>
                                    <select class="form-control" name="municipality_id" id="municipality_id">
                                        <option value="" v-text="municipalityMessage"></option>
                                        <option v-for="municipality in municipalities" v-bind:value="municipality.id">
                                            @{{ municipality.name }}
                                        </option>
                                    </select>
                                    <div></div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label" for="ward_no">{{__('app.ward_no')}} <span
                                                class="required">*</span>

                                    </label>
                                    <input class="form-control" placeholder="Please Input Ward Number" name="ward_no"
                                           id="ward_no"
                                           type="number"
                                           value="{{ old('ward_no', isset($project) ? $project->ward_no : "") }}"/>
                                    <div></div>

                                    @if ($errors->has('ward_no'))
                                        <span class="error">
                                    <strong>{{ $errors->first('ward_no') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="village_name">{{__('app.village_name')}} <span
                                                class="required">*</span>
                                    </label>

                                    <input class="form-control" placeholder="Please Input City or Village Name"
                                           name="village_name"
                                           id="village_name"
                                           type="text"
                                           value="{{ old('village_name', isset($farmer) ? $farmer[0]['village_name'] : "") }}"/>
                                    <div></div>

                                    @if ($errors->has('village_name'))
                                        <span class="error">
                <strong>{{ $errors->first('village_name') }}</strong>
            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row backgroundedCard">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="village_name">{{__('app.no_of_family_members')}}
                                        <span
                                                class="required">:-</span>
                                    </label>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="inline-label "
                                                   for="village_name">{{__('app.earning_members')}} <span
                                                        class="required">*</span>
                                            </label>

                                            <input class="form-control" name="earning_members"
                                                   id="earning_members"
                                                   type="text"
                                                   value="0"/>
                                            <div></div>

                                            @if ($errors->has('earning_members'))
                                                <span class="error">
                                                <strong>{{ $errors->first('earning_members') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <label class="inline-label "
                                                   for="village_name">{{__('app.non_earning_members')}} <span
                                                        class="required">*</span>
                                            </label>

                                            <input class="form-control" value="0" name="non_earning_members"
                                                   id="non_earning_members"
                                                   type="text"
                                            />
                                            <div></div>

                                            @if ($errors->has('non_earning_members'))
                                                <span class="error">
                <strong>{{ $errors->first('non_earning_members') }}</strong>
            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-12">
                                    <label class="inline-label "
                                           for="educational_level">{{__('app.educational_level')}} <span
                                                class="required">*</span>
                                    </label>
                                    <select class="form-control" name="educational_level"
                                            id="educational_level">
                                        <option value="">Please Select Educational Level</option>
                                        @foreach($education_level as $level)
                                            <option value="{{$level}}">
                                                {{$level}}
                                            </option>
                                        @endforeach

                                    </select>
                                    <div></div>
                                </div>

                            </div>

                        </div>
                        <div class="form">
                            <h3>{{__('app.current_condition')}}</h3>
                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label"
                                               for="annual_household_income">{{__('app.family_income')}}
                                            ({{displayUnitFormat('amount')}}) <span class="required">*</span>:
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <table class="table table-fixed gridView">
                                            <thead>
                                            <tr>
                                                <th>{{__('app.income_source')}}<span class="required">*</span></th>
                                                <th>{{__('app.monthly_income_amount')}}<span
                                                            class="required">(Rs.)*</span>
                                                </th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {{--                                                    <input type="text" onblur=" focusOutForms(this)"--}}
                                                    {{--                                                           name="income_source[]" placeholder="Please Input Income Source"--}}
                                                    {{--                                                           class="form-control quantity">--}}
                                                    <select name="income_source[]" class="form-control quantity"
                                                            onblur="focusOutForms(this)">
                                                        <option value="">Please Input Income Source</option>
                                                        @foreach($income_sources as $source)
                                                            <option value="{{$source}}">{{__('app.'.$source)}}</option>
                                                            @endforeach
                                                        {{--<option value="Business">Business/{{__('app.business')}}</option>--}}
                                                        {{--<option value="Job">Job/{{__('app.job')}}</option>--}}
                                                        {{--<option value="Remittance">Remittance/{{__('app.remittance')}}</option>--}}
                                                        {{--<option value="Other">Other/{{__('app.other')}}</option>--}}
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                </td>

                                                <td class="rupee-input"><input type="text" onblur=" focusOutForms(this)"
                                                                               name="income_amount[]"
                                                                               placeholder="Please Input Income Amount"
                                                                               class="form-control">
                                                    <div style="position:absolute"></div>
                                                </td>

                                                <td><a href='#' class='btn btn-small btn-danger remove_income'>-</a>
                                                    {{--<a--}}
                                                    {{--href='#'>--}}

                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-small btn-info add_income"
                                                onclick="addRowTotheTable(this)">
                                            + {{__('app.add_income')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label"
                                               for="annual_household_income">{{__('app.family_expenses')}}
                                            ({{displayUnitFormat('amount')}}) <span class="required">*</span>:
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <table class="table bill_product table-fixed gridView">
                                            <thead>
                                            <tr>
                                                <th>{{__('app.expense_source')}}<span class="required">*</span></th>
                                                <th>{{__('app.monthly_expense_amount')}}<span class="required">*</span>
                                                </th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {{--                                                    <input type="text" name="expense_source[]"--}}
                                                    {{--                                                           placeholder="Please Input Expense Source"--}}
                                                    {{--                                                           class="form-control quantity">--}}
                                                    <select name="expense_source[]" class="form-control quantity"
                                                            onblur="focusOutForms(this)">
                                                        <option value="">Please Input Expense Source</option>
                                                        @foreach($expense_sources as $source)
                                                            <option value="{{$source}}">{{__('app.'.$source)}}</option>
                                                        @endforeach
                                                        {{--<option value="HouseHold Expense">HouseHold Expense</option>--}}
                                                        {{--<option value="Education">Education</option>--}}
                                                        {{--<option value="Health">Health</option>--}}
                                                        {{--<option value="Installment">Installment</option>--}}
                                                        {{--<option value="Loan">Loan</option>--}}
                                                        {{--<option value="other">other</option>--}}
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                </td>

                                                <td class="rupee-input"><input type="text" name="expense_amount[]"
                                                                               placeholder="Please Input Expense Amount"
                                                                               class="form-control">
                                                    <div style="position:absolute"></div>
                                                </td>

                                                <td><a href='#' class='btn btn-small btn-danger remove_expense'>-</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-small btn-info add_expense"
                                                onclick="addRowTotheTable(this)">
                                            + {{__('app.add_expense')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form">
                            <h3>{{__('app.project_type')}}</h3>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="project_type">{{__('app.farmer_project')}}<span
                                                class="required">*</span>
                                    </label>
                                    <select class="form-control" name="project_type_id" id="project_type"
                                            @change="getAllDistricts"
                                    >
                                        {{--<option value="">Please Select Project Type</option>--}}
                                        @foreach($projectTypes as $type)
                                            <option value="{{ $type->id }}" {{($type->id==1)?'selected':'hidden'}}>{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                    <div></div>
                                    @if ($errors->has('project_type'))
                                        <span class="error">
                                    <strong>{{ $errors->first('project_type') }}</strong>
                                 </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="inline-label "
                                           for="current_irrigation_system">{{__('app.current_irrigation_system')}} <span
                                                class="required">*</span>

                                    </label>
                                    <select class="form-control" name="current_irrigation_system"
                                            id="current_irrigation_system">
                                        <option value="">Please Select</option>
                                        <option value="1"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 1 ? "selected":"" !!}>
                                            Diesel pump <span class="sl-text">({{__('app.diesel_pump')}})</span>
                                        </option>
                                        <option value="2"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 2 ? "selected":"" !!}>
                                            Electeric pump <span class="sl-text">({{__('app.electeric_pump')}})</span>
                                        </option>
                                        <option value="3"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 3 ? "selected":"" !!}>
                                            canal <span class="sl-text">({{__('app.canal')}})</span></option>
                                    </select>
                                    <div></div>
                                    @if ($errors->has('current_irrigation_system'))
                                        <span class="error">
                <strong>{{ $errors->first('current_irrigation_system') }}</strong>
            </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label class="inline-label"
                                           for="current_irrigation_source">{{__('app.current_irrigation_source')}} <span
                                                class="required">*</span>

                                    </label>
                                    <select class="form-control" name="current_irrigation_source"
                                            id="current_irrigation_source">
                                        <option value="">Please Select</option>
                                        <option value="1">
                                            Pond<span class="sl-text">({{__('app.pond')}})</span></option>
                                        <option value="2">
                                            Rain<span class="sl-text">({{__('app.rain')}})</span></option>
                                        <option value="3">
                                            River<span class="sl-text">({{__('app.river')}})</span></option>
                                        <option value="4">
                                            Boring<span class="sl-text">({{__('app.boring')}})</span></option>
                                        <option value="5">
                                            Well<span class="sl-text">({{__('app.well')}})</span></option>
                                    </select>
                                    <div></div>
                                    @if ($errors->has('current_irrigation_source'))
                                        <span class="error">
                <strong>{{ $errors->first('current_irrigation_source') }}</strong>
            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="project_type">{{__('app.monthly_expense')}}<span
                                                class="required">(Rs.)*</span>
                                    </label>
                                    <input class="form-control"
                                           name="grid_monthly_expense" id="grid_monthly_expense" type="number"
                                           value="{{ old('grid_monthly_expense', isset($farmer) ? $farmer[0]['grid_monthly_expense'] : "") }}"/>
                                    <div></div>
                                    @if ($errors->has('grid_monthly_expense'))
                                        <span class="error">
                                     <strong>{{ $errors->first('grid_monthly_expense') }}</strong>
                                     </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="inline-label " for="project_type">{{__('app.boring_width')}}<span
                                                class="required">*</span>
                                    </label>
                                    <input class="form-control" value="0" placeholder="Please Input Boring Size"
                                           name="boring_size" id="boring_size" type="number" step="0.01" min="0" max="10"
                                    />
                                    <div></div>
                                    @if ($errors->has('boring_size'))
                                        <span class="error">
                                     <strong>{{ $errors->first('boring_size') }}</strong>
                                     </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form">
                            <h3>{{__('app.farm_related_information')}}</h3>
                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label"
                                               for="annual_household_income">{{__('app.land_detail_information')}} <span
                                                    class="required">*</span>:
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <table class="table  table-fixed gridView">
                                            <thead>
                                            <tr>
                                                <th>{{__('app.land_type')}}<span class="required">*</span></th>
                                                <th>{{__('app.ownership')}}<span class="required">*</span></th>
                                                <th>{{__('app.area')}}<span class="required">*</span></th>
                                                <th>{{__('app.land_unit_type')}}<span class="required">*</span></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="crop-row">
                                                <td style="position:relative">
                                                    <select class="form-control resett" id="land-type"
                                                            name="land_type[]">
                                                        <option value="">Please Select</option>
                                                        <option value="dry">Dry Land</option>
                                                        <option value="wet">Wet Land</option>
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('land_type'))
                                                        <span class="error">
                                                            <strong>{{ $errors->first('land_type') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div></div>
                                                </td>
                                                <td style="position:relative">
                                                    <select class="form-control rephsett" id="ownership"
                                                            name="ownership[]">
                                                        <option value="">Please Select</option>
                                                        <option value="owned">Owned({{__('app.owned')}})</option>
                                                        <option value="leased">Leased({{__('app.leased')}})</option>
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('ownership'))
                                                        <span class="error">
                                                                <strong>{{ $errors->first('ownership') }}</strong>
                                                            </span>
                                                    @endif
                                                    <div></div>
                                                </td>
                                                <td style="position:relative">
                                                    <input type="number" placeholder="Area" class="form-control resett"
                                                           id="land-area" name="land_area[]" min="0.1"
                                                           step="0.1">
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('land_area'))
                                                        <span class="error">
                                                        <strong>{{ $errors->first('land_area') }}</strong>
                                                    </span>
                                                    @endif
                                                    <div></div>
                                                </td>
                                                <td>
                                                    <select class="form-control resett" id="unit-type"
                                                            name="unit_type[]">
                                                        <option value="">Please Select</option>
                                                        <option value="ropani">Ropani<span
                                                                    class="sl-text">({{__('app.ropani')}})</span>
                                                        </option>
                                                        <option value="katha">Katha<span
                                                                    class="sl-text">({{__('app.katha')}})</span>
                                                        </option>
                                                        <option value="bigaha">Bigaha<span
                                                                    class="sl-text">({{__('app.bigaha')}})</span>
                                                        </option>
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('unit_type'))
                                                        <span class="error">
                                                            <strong>{{ $errors->first('unit_type') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div></div>
                                                </td>
                                                <td>
                                                    <a href='#' class='btn btn-small btn-danger remove_land'>-</a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                        <button class="btn btn-small btn-info add_land" type="button"
                                                onclick="addRowTotheTable(this)">
                                            + {{__('app.add_crop')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label" for="crop_info">{{__('app.crop_info')}} <span
                                                    class="required">*</span>:
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <table class="table  table-fixed gridView">
                                            <thead>
                                            <tr>
                                                <th>{{__('app.crop_name')}}<span class="required">*</span></th>
                                                <th>{{__('app.crop_area')}}<span class="required">*</span></th>
                                                <th>{{__('app.land_unit_type')}}<span class="required">*</span></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="crop-row">
                                                <td style="position:relative">
                                                    <select class="form-control resett" id="crop-name"
                                                            name="crop_id[]">
                                                        <option value="">Please Select</option>
                                                        @foreach($crops as $crop)
                                                            <option value="{{ $crop->id }}">{{ $crop->name }}
                                                                @if(\Lang::has('app.'.$crop->name))
                                                                    ( {{__('app.'.$crop->name)}})
                                                                @endif
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('crop_id'))
                                                        <span class="error">
                                                <strong>{{ $errors->first('crop_id') }}</strong>
                                            </span>
                                                    @endif
                                                    <div></div>
                                                </td>
                                                <td style="position:relative">
                                                    <input type="number" placeholder="Area" class="form-control resett"
                                                           id="area" name="area[]" min="0.1"
                                                           step="0.1">
                                                    @if ($errors->has('area'))
                                                        <span class="error">
                            <strong>{{ $errors->first('area') }}</strong>
                        </span>
                                                    @endif
                                                    <div style="position:absolute"></div>
                                                </td>
                                                <td>
                                                    <select class="form-control resett" id="crop-area-unit-type"
                                                            name="crop_area_unit_type[]">
                                                        <option value="">Please Select</option>
                                                        <option value="ropani">Ropani<span
                                                                    class="sl-text">({{__('app.ropani')}})</span>
                                                        </option>
                                                        <option value="katha">Katha<span
                                                                    class="sl-text">({{__('app.katha')}})</span>
                                                        </option>
                                                        <option value="bigaha">Bigaha<span
                                                                    class="sl-text">({{__('app.bigaha')}})</span>
                                                        </option>
                                                    </select>
                                                    <div style="position:absolute"></div>
                                                    @if ($errors->has('crop_area_unit_type'))
                                                        <span class="error">
                        <strong>{{ $errors->first('crop_area_unit_type') }}</strong>
                    </span>
                                                    @endif
                                                    <div></div>
                                                </td>

                                                <td>
                                                    <a href='#' class='btn btn-small btn-danger remove_crop'>-</a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                        <button class="btn btn-small btn-info add_crop" type="button"
                                                onclick="addRowTotheTable(this)">
                                            + {{__('app.add_land')}}
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label" for="">{{__('app.animals_detail')}}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label" for="">{{__('app.animals')}} </label>
                                        <span class="radio">
                                            <label><input type="radio" name="has_cattle" value="1" id="yes"
                                                          checked>{{__('app.yes')}}</label>
                                        </span>
                                        <span class="radio">
                                            <label><input type="radio" name="has_cattle" id="no"
                                                          value="0">{{__('app.no')}}</label>
                                        </span>
                                    </div>
                                </div>
                                <div id="cattle_info">

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <table class="table table-fixed gridView">
                                                <thead>
                                                <tr>
                                                    <th>{{__('app.types_of_animals')}}
                                                    </th>
                                                    <th>{{__('app.number')}}
                                                    </th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><select class="form-control resett" id="cattle-name"
                                                                name="cattle_id[]">
                                                            <option value="">Please Select</option>
                                                            @foreach($cattles as $cattle)
                                                                <option value="{{ $cattle->id }}">{{ $cattle->name }}
                                                                    @if(\Lang::has('app.'.$cattle->name))
                                                                        ( {{__('app.'.$cattle->name)}})
                                                                    @endif
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <div></div>
                                                        @if ($errors->has('cattle_id'))
                                                            <span class="error">
                                                            <strong>{{ $errors->first('cattle_id') }}</strong>
                                                     </span>
                                                        @endif
                                                        <div></div>
                                                    </td>

                                                    <td><input type="number"
                                                               placeholder="Please Input Number of Cattles"
                                                               class="form-control "
                                                               id="number" name="number[]" min="0">
                                                        <div></div>
                                                        @if ($errors->has('number'))
                                                            <span class="error">
                                                            <strong>{{ $errors->first('number') }}</strong>
                                                        </span>
                                                        @endif
                                                        <div></div>
                                                    </td>

                                                    <td><a href='#'
                                                           class='btn btn-small btn-danger remove_cattles'>-</a>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <button type="button" class="btn btn-small btn-info add_income"
                                                    onclick="addRowTotheTable(this)">
                                                + {{__('app.add_cattles')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="backgroundedCard">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="inline-label" for="">{{__('app.crop_sell')}}
                                        </label>
                                        <div class="radio">
                                            <label><input type="radio" name="sell_crops" value="1" id="true"
                                                          checked>{{__('app.crop_sell_true')}}</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="sell_crops" id="false"
                                                          value="0">{{__('app.crop_sell_false')}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" id="market_distance">
                                        <label class="inline-label" for="">{{__('app.market_distance')}}
                                        </label>
                                        <input class="form-control"
                                               placeholder="Please Input Distance To Nearest Market"
                                               name="distance_to_nearest_market" id="distance_to_nearest_market"
                                               type="number"
                                               value="{{ old('distance_to_nearest_market', isset($farmer) ? $farmer[0]['distance_to_nearest_market'] : "") }}"/>
                                        <div></div>
                                        @if ($errors->has('distance_to_nearest_market'))
                                            <span class="error">
                                            <strong>{{ $errors->first('distance_to_nearest_market') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="buttons-area">
                            <button class="btn btn-outline-primary outlined-button" type="submit" name="submit-analysis"
                                    value="submit" data-type="save"
                                    id="saveChanges" disabled>
                                <i class="icon ion-ios-save"></i> Save as Draft
                            </button>
                            <div id="buttonsArea" class="back-mode">
                                <button class="btn btn-outline-primary outlined-button" id="back"><i
                                            class="icon ion-ios-arrow-back"></i> Back
                                </button>
                                <button class="btn btn-primary" id="next"> Next Step <i
                                            class="icon ion-ios-arrow-forward"></i></button>
                                <div class="submit-button" id="submitButtons">
                                    <button class="btn btn-primary" id="submit" type="submit" name="submit-analysis"
                                            data-type="save" value="submit-analysis">Submit for Analysis <i
                                                class="icon ion-ios-arrow-forward"></i>
                                    </button>
                                    <button class="btn btn-primary" id="faceButton">Submit for Analysis <i
                                                class="icon ion-ios-arrow-forward"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </main>
    </section>

@endsection


@section('js')
    {{--@include('includes.project_form_script')--}}


    {{--
        <script>
          $(function () {
            $('input[name=\'has_cattle\']').click(function () {
              if ($('#yes').is(':checked')) {
                $('#cattle_info').show()
              } else {
                $('#cattle_info').hide()
              }
            })
          })


          $(function () {
            $('input[name=\'sell_crops\']').click(function () {
              if ($('#true').is(':checked')) {
                $('#market_distance').show()
              } else {
                $('#market_distance').hide()
              }
            })
          })

          function addRowTotheTable(element){
            let btn = element;
            let tableRef=btn.parentElement.querySelector('table');
            let newRow =tableRef.insertRow(-1);
            newRow.innerHTML= tableRef.querySelector('tbody>tr:nth-last-child(2)').innerHTML;
            tableRef.querySelectorAll('tbody>tr:nth-last-child(2)>input,tbody>tr:nth-last-child(2)>select').forEach(i=>i.value='');
            newRow.querySelectorAll('input,select').forEach((input)=>input.addEventListener('focusout',function (e){
              focusOutForms(e.target);
            }));
            newRow.querySelectorAll('input').forEach(inp=>inp.addEventListener('keyup', function (e){
              console.log(e.key);
              if(e.key=='Enter'){
                console.log(btn);
                btn.click();
              }
            }))
            console.log(tableRef.querySelector('tbody>tr:nth-last-child(2)'));
            return false;
          }

          $(document).ready(function () {
            $('body').on('click', '.remove_income', function () {
    //          var lmnt = $(this)
              var parentTag = $(this).parent().get(0)
              var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
              console.log(trs.length)

              if (trs.length > 1) {
                $parentTR = $(this).closest('tr')
                $parentTR.remove()
              }
              return false
            })
          })

          $(document).ready(function () {

            $('body').on('click', '.remove_expense', function () {
              var parentTag = $(this).parent().get(0)
              var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
              console.log(trs.length)

              if (trs.length > 1) {
                $parentTR = $(this).closest('tr')
                $parentTR.remove()
              }
              return false

            })
          })

          $(document).ready(function () {
      /*      $('body').on('click', '.add_land', function () {
              var $this = $(this),

    //            $parentTR = $this.closest('tr')
              $parentTR.clone().insertAfter($parentTR).find('input').val('')

              return false
            })*/

            $('body').on('click', '.remove_land', function () {
              var parentTag = $(this).parent().get(0)
              var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
              console.log(trs.length)

              if (trs.length > 1) {
                $parentTR = $(this).closest('tr')
                $parentTR.remove()
              }
              return false
            })
          })

          $(document).ready(function () {
            /*$('body').on('click', '.add_crop', function () {
              var $this = $(this),
                $parentTR = $this.closest('tr')
              $parentTR.clone().insertAfter($parentTR).find('input').val('')

              return false
            })*/

            $('body').on('click', '.remove_crop', function () {
              var parentTag = $(this).parent().get(0)
              var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
              console.log(trs.length)

              if (trs.length > 1) {
                $parentTR = $(this).closest('tr')
                $parentTR.remove()
              }
              return false
            })
          })

        </script>
    --}}
    {{--<script src="/js/ogbadmin/project/project-create.js"></script>--}}
    {{--<script src="/js/ogbadmin/project/project-form.js"></script>--}}
    {{--<script src="/js/ogbadmin/project/project-element.js"></script>--}}
    <script src="{{asset('js/ogbadmin/project/project-create.js')}}"></script>
    <script src="{{asset('js/ogbadmin/project/project-form.js')}}"></script>
    <script src="{{asset('js/ogbadmin/project/project-element.js')}}"></script>

@endsection

