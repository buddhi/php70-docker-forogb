<html>
<head>
    <title>Create Meter</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <link href='{{asset('fontawesome/css/all.min.css')}}' rel="stylesheet">
    @yield('css')
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">

    <style>
        #flash-message {
            position: absolute;
            top: 100px;
            right: 20px;
            z-index: 10;
        }

        .error {
            color: red;
        }

        .heading {
            display: flex;
        }

        .back-button {
            margin-left: 10%;
        }

        .text {
            margin-left: 25%;
        }

        .form-part {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .input-group {
            width: 40%;
        }

        .save {
            margin-top: 2%;
        }

        header {
            padding: 0 5vw;
            box-sizing: border-box;
            min-height: unset;
        }

        header > .title {
            flex: 1;
            margin-right: 15%
        }

        hr {
            margin: 2rem 13vw;
        }

        .input-group {
            margin: 10px 0;
            margin-left: 2%;
        }

        #flash-message {
            margin-right: 60%;
            margin-top: 5%;
        }
    </style>
</head>
<body>
@include('ogbadmin.header')
<div class="container-fluid">
    <header class="d-flex flex-xl-nowrap">
        <div class="back-button w-5">
            <a href="{{route('ogbadmin.project.details',['id' => $project_id])}}">
                <button class="btn btn-outline-primary outlined-button">
                    <i class="icon ion-ios-arrow-back"></i> Go Back
                </button>
            </a>
        </div>
        <div class="title">
            <h4 class="text-center">Create Meter</h4>
        </div>
    </header>
    <hr>

    <div id="flash-message">
        @if (session()->has('success_message'))
            <div class="alert alert-success alert-dismissible" role="alert" class="flash-message">
                {{session()->get('success_message')}}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert" class="flash-message">
                {{session()->get('error')}}
            </div>
        @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
    </div>
    <form action="{{route('ogbadmin.project.store.meter')}}" method="post">
        <div class="form-part">
            {{csrf_field()}}
            <div class="input-group">
                <label class="inline-label">Phone Number</label>
                <input class="form-control" name="phoneNo" type="text" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">Hardware ID</label>
                <input class="form-control" name="hardwareId" type="text" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">Firmware Version </label>
                <input class="form-control" name="firmwareVersion" type="text" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">Type</label>
                <input class="form-control" name="type" type="text" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power <=250</label>
                <input class="form-control" name="sys_eff1" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>250<=350</label>
                <input class="form-control" name="sys_eff2" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>350<=450</label>
                <input class="form-control" name="sys_eff3" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>450<=550</label>
                <input class="form-control" name="sys_eff4" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>550<=650</label>
                <input class="form-control" name="sys_eff5" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>650<=750</label>
                <input class="form-control" name="sys_eff6" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>750<=850</label>
                <input class="form-control" name="sys_eff7" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">System Efficiency when DC power>850</label>
                <input class="form-control" name="sys_eff8" type="number" value=""/>
            </div>
            <div class="input-group">
                <label class="inline-label">Pump Efficiency</label>
                <input class="form-control" name="pump_efficiency" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">CD Efficiency</label>
                <input class="form-control" name="cd_efficiency" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Max Power</label>
                <input class="form-control" name="max_power" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Frequency Multiplier</label>
                <input class="form-control" name="frequency_multiplier" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">RPM Multiplier</label>
                <input class="form-control" name="rpm_multiplier" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Head</label>
                <input class="form-control" name="head" type="number" value="{{$head}}" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Current Threshold</label>
                <input class="form-control" name="current_threshold" type="number" value="" step="0.01" min="0" max="10"/>
            </div>
            <div class="input-group">
                <label class="inline-label">Status</label>
                <select class="form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
            <div class="input-group">
                <label class="inline-label">Meter Type</label>
                <select name="meter_type" class="form-control">
                    @foreach(\App\Enums\MeterType::ALL_METER_TYPES as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        @endforeach
                </select>
            </div>
            <input type="hidden" name="farmer_project_id" value="{{$farmer_project_id}}">
            <div class="input-group">
                <button class="btn btn-primary save ">Save Changes</button>
            </div>
        </div>
    </form>
</div>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}">
</script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script>$('#flash-message').delay(2000).fadeOut(2000);</script>
@yield('js')
</body>
</html>

