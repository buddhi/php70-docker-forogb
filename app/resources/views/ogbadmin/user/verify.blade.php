<div class="modal fade" id="user-verify" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">

            <section id="invite-user" class="invite-user-section">
                <div class="row  justify-content-center">
                    <div class="col-12 text-center mb-3">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h3>Invite a User</h3>
                    </div>
                    <div class="alert alert-danger error-form" id="add-user-error-bag">
                        <ul id="add-user-form-errors"></ul>
                    </div>
                    <div class="col-12">
                        <form method="POST" action="http://localhost:8000/ogbadmin/partner-user" accept-charset="UTF-8" class="form-horizontal" id="add_user" enctype="multipart/form-data">
                            <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label for="partner">Name of Partner:</label>
                                <select id="partner" class="form-control" required="required" name="partner" style="    height: auto !important;" {{session('role')== 50? '':'disabled'}}>
                                    @foreach($partners as $partner)
                                        <option value="{{$partner->id}}" data-image="{{isset($partner->image)? asset($partner->image):asset('img/farmer_default.png')}}" {{$partner->id == $partner_id ? 'selected': ''}}>{!! $partner->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">User Email address:</label>
                                <input id="email" class="form-control" placeholder="User Email address" name="email" type="text">
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-block btn-primary verification-email-user" rel="verification-success">Send Verification Email</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@include('ogbadmin.email-verification-popup')