@extends('ogbadmin.layout')
@section('title')
    Edit  Partner User
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
        <div class="container-fluid">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0">Edit User</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="card">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <span class="text-muted d-block mb-1">User Name</span>
                                <strong>{{$user->full_name}}</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="text-muted d-block mb-1">Email Address</span>
                                <strong>{{$user->email}}</strong>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div>
                                    <span class="text-muted d-block mb-1">Mobile Number</span>
                                    <strong>{{$user->phone}}</strong>
                                </div>
                                <a href="{{route('partner-user.change-number.form',['user_id' => $user->user_id] )}}" class="btn btn-link" >Change Number</a>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div>
                                    <span class="text-muted d-block mb-1">Password</span>
                                    <strong>********</strong>
                                </div>
                                <a href="{{route('changePassword')}}" class="btn btn-link">Change Password</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-md-6">
                    <form action="{{route('account.update', $user->user_id)}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="thumb-upload-wrapper">
                                <label class= "text-center">Profile Picture</label>
                                <div class="profile-upload rounded-circle browse">
                                    <img src="{{isset($user->profile_image)? '/storage/upload/'.$user->profile_image: '/img/farmer_default.png'}}"  accept="image/*" class="avatar image-change" alt="offgridbazaar user"  id="preview_image"/>
                                    <span class="text-center image-change">Upload</span>
                                    <input type="file" class="rounded-circle"  id="file" data-id ="{{$user->user_id}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><span>Address <span class="required">*</span></span>
                                <textarea type="text" name="address" class="form-control" rows="5" >{!! old('address',isset($user->address)? $user->address:'') !!}</textarea>
                            </label>
                            @if ($errors->has('address'))
                                <p class="text-danger">{{ $errors->first('address') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label><span>Landline Number</span>
                                <input type="text" name="landline" class="form-control" placeholder="Landline Number" value="{{ old('landline',isset($user->landline)? $user->landline:'') }}">
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Submit Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="{{asset('js/ogbadmin/user/user-dashboard.js')}}"></script>
@endsection