<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Login</title>
</head>

<body>
<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>
<div class="text-center">
    @include('common.session_message')
</div>
<div class="container-fluid">

    <div class="row">
        <main class="col-12 col-md-12 col-xl-12 bd-content">
            <form method="POST" action="{{route('login') }}">
                {{csrf_field()}}
                <input type="hidden" name="role" value="{{\App\Enums\Role::PARTNER_USER}}">
                <div class="box-wrap">
                    <h3 class="text-center">Partner User Login</h3>
                    <p class="text-center mb-4">Please login to your account.</p>
                    <div class="form-group mt-4">
                        <label>
                            <span>Enter Email Address</span>
                            <input type="text" class="form-control" name="email" value="{{ old('email')}}" placeholder="Enter Your Email Address">
                        </label>
                        @if ($errors->has('email'))
                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    <div class="form-group mt-4">
                        <label>
                            <span>Enter Your Password</span>
                            <input type="password" class="form-control" name="password"
                                   placeholder="Enter Your Password">
                        </label>
                        @if ($errors->has('password'))
                            <p class="text-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    <div class="form-group mt-4">
                        <button class="btn btn-primary btn-block">Login</button>
                    </div>
                    <p class="text-center"><a href="#">Forgot Your Password?</a></p>
                </div>
            </form>
        </main>
    </div>
</div>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
</body>
</html>