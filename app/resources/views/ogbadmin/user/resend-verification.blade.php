<div class="modal fade" id="resend-verification" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">

            <section id="resend-verification-partner" class="resend-verification-section">
                <div class="row  justify-content-center">
                    <div class="col-12 text-center mb-3">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h3>Resend Verification Email</h3>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label><span>Partner-User Email address</span>
                                <input type="email" class="form-control" name="verification-email" id ="verify_email">
                            </label>
                        </div>
                        <div class="form-group">
                                <input type="hidden" class="form-control" name="verification-id" id ="verify_id">
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-primary resend-verification-email"
                                    rel="resend-verification-success" id = "">Resend Verification Email</button>
                        </div>
                    </div>

                </div>
            </section>

            <section id="resend-verification-success" class="collapse resend-verification-section">
                <div class="row  justify-content-center">
                    <div class="col-12 text-center">
                        <img class="mb-3" src="../img/sent-mail.svg" alt="">
                        <p class="mb-0">Verification email has been sent to <br>
                            <strong>ram-bahadur@mail.com</strong></p>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>