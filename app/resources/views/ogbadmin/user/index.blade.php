@extends('ogbadmin.layout')
@section('title')
    User Listing
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
        <div class="container-fluid">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    @if($filterStatus == 'valid')
                        <h3 class="mb-1">Registered Users</h3>
                        <p style="font-size: 1rem;"><a href="{{route('ogbadmin.partner-user.index', ['filter' =>'pending'])}}">Show pending invites</a> </p>
                    @else
                        <h3 class="mb-1"> Pending User Invites</h3>
                        <p style="font-size: 1rem;"><a href="{{route('ogbadmin.partner-user.index', ['filter' =>'valid'])}}">Show registered User</a> </p>
                    @endif
                </div>
                <div class="col-md-6 d-flex justify-content-end align-self-start">
                    <button type="button" class="btn btn-primary add-partner" data-toggle="modal"
                            data-target="#user-verify">Add User
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                <hr class="mb-3">
                <div class="col-md-12">
                    @if($filterStatus == 'valid')
                        @include('ogbadmin.user.user-table')
                    @else
                        @include('ogbadmin.user.pending-user-table')
                    @endif
                </div>
            </div>
        </div>
    </main>
    @include('ogbadmin.user.verify',['partners' =>$partners])
    @include('ogbadmin.user.resend-verification')
@endsection
@section('js')
    <script>
        var disabled_partners = {{json_encode($disabled_partners)}}
    </script>
    <script src="{{asset('js/ogbadmin/user/user-dashboard.js')}}"></script>
    <script>
        function filterUser(elm) {
            window.location = '/ogbadmin/partner-user' + '?filter=' + elm.value;
        }
    </script>
@endsection
