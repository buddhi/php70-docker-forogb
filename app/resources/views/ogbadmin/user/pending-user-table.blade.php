<table class="table table-wrapper">
    <thead>
    <tr>
        <th scope="col">Email Address</th>
        <th scope="col">Status</th>
        <th scope="col" class="text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @if ($users->count() > 0)
        @foreach($users as $user)
            <tr>
                <td><span class="user-thumbnail text-center text-white">{!! isset($user->email[0])? $user->email[0]:"A"!!}</span>
                    {{$user->email}}</td>
                <td>
                    <div class="status verification-pending">Verification Pending</div>
                </td>
                <td>
                    <div class="action">
                        <div class="align-items-center">
                            <div class="resend-email text-center" data-toggle="tooltip" data-placement="top"
                                 title="Resend Verification Email">
                                {{--<meta name="id_temporary_user" content="{{$user->id}}">--}}
                                <a href="#" id="resend-email" data-target="#resend-verification" data-toggle="modal"
                                   data-id="{{$user->id}}" data-whatever="{{$user->email}}"
                                   >
                                    <i class="icon ion-md-mail"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" class="pagination-td">{!!$users->appends(request()->input())->links('pagination::bootstrap-4'); !!}</td>
        </tr>
    @else
        <tr>
            <td colspan="5">No data found.</td>
        </tr>
    @endif
    </tbody>
</table>