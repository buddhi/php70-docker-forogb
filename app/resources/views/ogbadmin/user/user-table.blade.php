<table class="table table-wrapper">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Email Address</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Status</th>
        <th scope="col" class="text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @if ($users->count() > 0)
        @foreach($users as $user)
            <tr>
                <th scope="col">
                    @if(isset($user->profile_image))
                        <img src="{{'/storage/upload/'.$user->profile_image}}" class="user-thumbnail" alt="">
                    @else
                        <span class="user-thumbnail text-center text-white text-capitalize">
                            {!!  isset($user->full_name[0])? $user->full_name[0]:"A"!!}
                        </span>
                    @endif
                    <span class="text-capitalize">{{$user->full_name}}</span>
                </th>
                <td>{{$user->email}}</td>
                <td>{{$user->phone}}</td>

                <td>
                    <div class="status {{$user->status}}">{{ucfirst($user->status)}}</div>
                </td>
                <td>
                    <div class="action d-flex justify-content-end">
                        <div class="d-flex align-items-center">
                            @if(!$user->verified)
                                <div class="resend-email" data-toggle="tooltip" data-placement="top"
                                     title="Resend Verification Email">
                                    <a href="#" data-target="#resend-verification" data-toggle="modal"
                                       data-whatever="haribahadur@mail.com"
                                       data-target="#resend-verification">
                                        <i class="icon ion-md-mail"></i>
                                    </a>
                                </div>
                            @endif
                            <div class="edit" data-toggle="tooltip" data-placement="top"
                                 title="Edit User">
                                <a href="{{route('ogbadmin.partner-user.edit',['data'=>$user->user_id])}}">
                                    <i class="icon ion-md-create"></i>
                                </a>
                            </div>
                            <div class="checkbox-container" data-toggle="toggletip"
                                 data-placement="top"
                                  data-partner="{{$user->partner_id}}" data-partner="{{$user->partner_id}}">
                                <input type="checkbox" id="toggle-{{$user->user_id}}" class="toggle-partner-user" attr = "{{$user->user_id}}" {{$user->disabled? "": "checked"}} data-partner="{{$user->partner_id}}"/>
                                <label for="toggle-{{$user->user_id}}"></label>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" class="pagination-td">{!!$users->appends(request()->input())->links('pagination::bootstrap-4'); !!}</td>
        </tr>
    @else
        <tr>
            <td colspan="5">No data found.</td>
        </tr>
    @endif
    </tbody>
</table>