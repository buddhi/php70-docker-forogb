<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Login</title>
</head>

<body>
<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>

<div class="container-fluid text-center">
    <h1>PARTNER USER DASHBOARD WILL BE BUILD SOON</h1>
    <h6>You are logged in partner user</h6>
    <h6>You can logout by</h6>

    <a href="{{ url('/logout') }}"
       onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
        Clicking here
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</div>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
</body>
</html>