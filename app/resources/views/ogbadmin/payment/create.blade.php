@extends('ogbadmin.layout')
@section('title')
    Payments
@endsection
@section('css')
    <style>
        .loader {
            border: 8px solid #f3f3f3;
            border-radius: 50%;
            border-top: 8px solid #777;
            width: 40px;
            height: 40px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection
@section('content')

    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">

        <div class="container-fluid">
            <div class="row flex-xl-nowrap align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0">Make a Payment</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <hr>
                </div>
            </div>
            <div class="row flex-xl-nowrap">
                    <div class="col-md-4">
                <form action="{{route('ogbadmin.payment.store')}}" method="post" id="cash-payment-form" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group auto-search-group">
                        <label><span>Payment ID or Name: <span class="required">*</span></span>
                            <select id="farmer" class="form-control" required="required" name="payment_id">
                                <option ></option>
                            @foreach( $projects as $project)
                                    <option value="{{isset($project->payment_id)?$project->payment_id:null}}"
                                            data-image="{{isset($project->image)? asset($project->image):asset('img/farmer_default.png')}}"
                                            data-farmer-address = "{{isset($project->farmers->first()->address)? $project->farmers->first()->address:null}}"
                                            >
                                        {!! $project->farmer_name!!}
                                    </option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                    <div id="project-detail">
                    </div>
                    <div class="loader text-center" id="loader" style="display: none;"></div>
                    <div id ="payment-form">
                        <div class="form-group">
                            <label><span>Payment Name:</span>
                                <input type="text" name="payment_name" class="form-control" placeholder="emi or advance" id="payment_type">
                            </label>
                            @if ($errors->has('payment_type'))
                                <span class="error">
                                    <p class="text-danger">{{$errors->first('payment_type') }}</p>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label><span>Transaction Amount(NRS): <span class="required">*</span></span>
                            <input type="number" name="amount" class="form-control" placeholder="amount" id="amount">
                        </label>
                        @if ($errors->has('amount'))
                            <span class="error">
                                    <p class="text-danger">{{$errors->first('amount') }}</p>
                                </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><span>Select Payment Type<span class="required">*</span></span>
                            <input type="text" name="paymentGateway" class="form-control" placeholder="cash" id="payment_gateway" value="cash">
                            @if ($errors->has('paymentGateway'))
                                <span class="error">
                                        <p class="text-danger">{{ $errors->first('paymentGateway') }}</p>
                                    </span>
                            @endif
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Upload receipt:</label>
                    <div class="custom-file">
                        <input name="file" type="file" class="custom-file-input" id="customFileLang" lang="es">
                        <label class="custom-file-label" for="customFileLang" placeholder="choose receipt"></label>
                    </div>
                    </div>
                    <div class="mb-2"></div>
                    <div class="form-group">
                        <label><span>Date of Payment: <span class="required">*</span></span>
                            <input type="date" class="form-control" placeholder="Date of Payment" name = "payment_date" value="{{Carbon\Carbon::now()->toDateString()}}">
                            @if ($errors->has('payment_date'))
                                <span class="error">
                                        <p class="text-danger">{{ $errors->first('payment_date') }}</p>
                                    </span>
                            @endif
                        </label>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary verification-email"
                                rel="verification-success">Make Payment</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('js')
    <script>

        var projects = {!! json_encode($projects) !!}
        $("#farmer").select2({
            templateResult: formatState,
            width: '100%'

        });
        function formatState (opt) {
            if (!opt.id) {
                return opt.text;
            }
            var optimage = $(opt.element).data('image');
            if(!optimage){
                return opt.text;
            } else {
                var $opt = $(
                    '<div class="dropdown-item d-flex"><div class="user-thumb">' +
                        '<img src="'+optimage+'" class="user-thumbnail" alt="">'+
                    '</div>' +
                    '<div class="d-flex flex-column">'+
                        '<div class="user-name mb-1"><strong>'+$(opt.element).text()+'</strong></div>'+
                            '<div class="d-flex">'+
                                '<div class="user-id mr-2"><strong class="mr-2">#</strong>'+$(opt.element).val()+'</div>'+
                                '<div class="user-location"><i class="icon ion-md-pin mr-2"></i>'+$(opt.element).data('farmer-address')+'</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                );
                return $opt;
            }
        }


        $('#farmer').on("change", function(e) {

            //creating  the div with project details
            var payment_id = $("#farmer option:selected").val();
            var project = projects.find(function(project){
                return project.payment_id === payment_id
            });
            var html =
                '<div class="user-details" id = "details">' +
                    '<div class="trash-item clear"><i class="icon ion-md-trash" id="trash-icon"></i></div>' +
                    '<div class="user-info">' +
                        '<span>Payment ID</span>' + payment_id+
                    '</div>' +
                    '<div class="user-info">' +
                        '<span>Farmer Name</span>'+ project.farmer_name +
                    '</div>' +
                    '<div class="user-info">' +
                        '<span>System Size</span>'+ project.solar_pv_size +'W' +
                    '</div>' +
                    '<div class="user-info">' +
                        '<span>Location</span>'+project.municipality.name + ','+ project.district.name+
                    '</div>' +
                '</div>'
            $('#project-detail').html(html)


            //ajax request to fill the payment details
            var url = '/data/'+ payment_id;
            $.ajax({
                url : url,
                method:'get',
                beforeSend: function(){
                    $('#loader').css('display', 'block');
                    $('#payment-form').css('display', 'none');
                },
                success:  function (data) {
                    setTimeout(function(){
                        $('#loader').css('display', 'none');
                        $('#payment-form').css('display', 'block');
                    }, 2000);
                    $('#amount').val(data.amount);
                    $('#amount').prop("readonly",true);
                    $("#payment_type").val(data.payment_type);
                    $("#payment_gateway").val('cash');
                    $('#payment_gateway').prop("readonly",true);
                    $('#project-detail').show();
                    $('#payment-form').find('input, textarea, button, select').attr('disabled','disabled');
                },
                error:function () {
                    $('#payment-form').css('display', 'block');
                }
             });

        });

        $(document).ready(function(){
            $('#project-detail').on('click', '#trash-icon', function() {
                $('#cash-payment-form').trigger('reset');
                $('#project-detail').hide();
                $('#farmer').val('').trigger('change');
            });
        });

        $('.custom-file input').change(function (e) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        });
    </script>
@endsection