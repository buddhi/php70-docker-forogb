@extends('ogbadmin.layout')
@section('title')
    Payments
@endsection
@section('content')
    <main class="col-12 col-md-9 col-xl-10 pb-md-3 bd-content">
                <div class="container-fluid">
                    <div class="row flex-xl-nowrap align-items-center">
                        <div class="col-md-6">
                            <h3 class="mb-0">Payments</h3>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a href="{{route('ogbadmin.payment.create')}}" class="btn btn-primary">Make a Payment</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-lg-4 col-sm-12 mb-sm-4">
                            <div class="info-card card-orange d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>{{displayUnitFormat('amount' ,$overdue_payments['total_overdue_amount'])}}</h4>
                                    <div class="text-muted">Total Payments for this Month</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 mb-sm-4">
                            <div class="info-card card-green d-flex justify-content-between">
                                <div class="card-details">
                                    <h4>{{displayUnitFormat('amount' ,$total_monthly_amount_raised['total'])}}</h4>
                                    <div class="text-muted">Total Payments Collected this Month</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-12 mb-sm-0">
                            <div class="info-card card-blue d-flex justify-content-between">
                                <div class="card-details">
                                    @if($overdue_payments['total_overdue_amount'] != 0)
                                        <h4>{{round($total_monthly_amount_raised['total']/$overdue_payments['total_overdue_amount'] ,3)}}%</h4>
                                    @else
                                        <h4>0%</h4>
                                    @endif
                                    <div class="text-muted">Recollection Progress for this Month</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-lg-6 col-sm-12">
                            <div class="text-muted mb-4">Payments Overview</div>
                            <canvas id="payments_overview" class="col_canvas"></canvas>
                        </div>


                        <div class="col-lg-6 col-sm-12">
                            <div class="text-muted mb-4">Recollection Overview</div>
                            <canvas id="recollection_overview" class="col_canvas"></canvas>
                        </div>
                    </div>

                    <div class="row flex-xl-nowrap">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="filter-bar">
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="row">
                                                <div class="col">
                                                    <button class="btn btn-filter d-flex justify-content-between align-items-center" type="button" data-toggle="collapse" data-target="#filterDropdown" role="button" aria-expanded="false" aria-controls="filterDropdown">
                                                        <span class="mr-5">Filter by:</span>
                                                        <i class="icon ion-md-funnel"></i>
                                                    </button>
                                                    <div class="filter-dropdown collapse" id="filterDropdown">
                                                        <form action="">
                                                            <div class="row">
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Location</strong>
                                                                    <select class="custom-select" id="inputGroupSelect01">
                                                                        <option selected>Provience</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                    </select>

                                                                    <select class="custom-select mt-3" id="inputGroupSelect01">
                                                                        <option selected>District</option>
                                                                        <option value="">Achham</option>
                                                                        <option value="">Arghakhanchi</option>
                                                                        <option value="">Baglung</option>
                                                                        <option value="">Baitadi</option>
                                                                        <option value="">Bajhang</option>
                                                                        <option value="">Bajura</option>
                                                                        <option value="">Banke</option>
                                                                        <option value="">Bara</option>
                                                                        <option value="">Bardiya</option>
                                                                        <option value="">Bhaktapur</option>
                                                                        <option value="">Bhojpur</option>
                                                                        <option value="">Chitwan</option>
                                                                        <option value="">Dadeldhura</option>
                                                                        <option value="">Dailekh</option>
                                                                        <option value="">Darchula</option>
                                                                        <option value="">Dhading</option>
                                                                        <option value="">Dhankutta</option>
                                                                        <option value="">Dhanusa</option>
                                                                        <option value="">Dolkha</option>
                                                                        <option value="">Dolpa</option>
                                                                        <option value="">Doti</option>
                                                                        <option value="">Ghorahi</option>
                                                                        <option value="">Gorkha</option>
                                                                        <option value="">Gulmi</option>
                                                                        <option value="">Humla</option>
                                                                        <option value="">Ilam</option>
                                                                        <option value="">Jajarkot</option>
                                                                        <option value="">Jhapa</option>
                                                                        <option value="">Jumla</option>
                                                                        <option value="">Kailali</option>
                                                                        <option value="">Kalikot</option>
                                                                        <option value="">Kanchanpur</option>
                                                                        <option value="">Kaski</option>
                                                                        <option value="">Kathmandu</option>
                                                                        <option value="">Kavreplanchauk</option>
                                                                        <option value="">Khotang</option>
                                                                        <option value="">Lalitpur</option>
                                                                        <option value="">Lamjung</option>
                                                                        <option value="">Mahottari</option>
                                                                        <option value="">Makwanpur</option>
                                                                        <option value="">Manag</option>
                                                                        <option value="">Morang</option>
                                                                        <option value="">Mugu</option>
                                                                        <option value="">Mustang</option>
                                                                        <option value="">Myagdi</option>
                                                                        <option value="">Nawalpur</option>
                                                                        <option value="">Nuwakot</option>
                                                                        <option value="">Okhaldunga</option>
                                                                        <option value="">Palpa</option>
                                                                        <option value="">Panchthar</option>
                                                                        <option value="">Parasi</option>
                                                                        <option value="">Parsa</option>
                                                                        <option value="">Parwat</option>
                                                                        <option value="">Pyuthan</option>
                                                                        <option value="">Ramechhap</option>
                                                                        <option value="">Rasuwa</option>
                                                                        <option value="">Rauthat</option>
                                                                        <option value="">Rolpa</option>
                                                                        <option value="">Rukum</option> Paschim
                                                                        <option value="">Rukum</option> Purba
                                                                        <option value="">Rupandehi</option>
                                                                        <option value="">Salyan</option>
                                                                        <option value="">Sankhuwasabha</option>
                                                                        <option value="">Saptari</option>
                                                                        <option value="">Sarlahi</option>
                                                                        <option value="">Sindhuli</option>
                                                                        <option value="">Sindhupalchauk</option>
                                                                        <option value="">Siraha</option>
                                                                        <option value="">Solukhumbu</option>
                                                                        <option value="">Sunsari</option>
                                                                        <option value="">Surkhet</option>
                                                                        <option value="">Syangja</option>
                                                                        <option value="">Tanahun</option>
                                                                        <option value="">Taplejung</option>
                                                                        <option value="">Taulihawa</option>
                                                                        <option value="">Terhathum</option>
                                                                        <option value="">Udaypur</option>
                                                                    </select>

                                                                </div>
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Size</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck1">
                                                                        <label class="form-check-label" for="sizeCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            1 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck2">
                                                                        <label class="form-check-label" for="sizeCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            1.5 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck3">
                                                                        <label class="form-check-label" for="sizeCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            2 hp
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="sizeCheck4">
                                                                        <label class="form-check-label" for="sizeCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Small
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg col-sm-4">
                                                                    <strong class="d-block mb-2">Category</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck1">
                                                                        <label class="form-check-label" for="categoryCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Fish
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck2">
                                                                        <label class="form-check-label" for="categoryCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Vegetables
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck3">
                                                                        <label class="form-check-label" for="categoryCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Crops
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="categoryCheck4">
                                                                        <label class="form-check-label" for="categoryCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Mix
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg col-sm-4 mt-sm-3">
                                                                    <strong class="d-block mb-2">Project Status</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck1">
                                                                        <label class="form-check-label" for="projectStatusCheck1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            New
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck2">
                                                                        <label class="form-check-label" for="projectStatusCheck2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Approved
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck3">
                                                                        <label class="form-check-label" for="projectStatusCheck3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Operational
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check mt-2">
                                                                        <input class="form-check-input" type="checkbox" value="" id="projectStatusCheck4">
                                                                        <label class="form-check-label" for="projectStatusCheck4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            Error
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg col-sm-4 mt-sm-3">
                                                                    <strong class="d-block mb-2">Credit Score</strong>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="creditScore1">
                                                                        <label class="form-check-label" for="creditScore1">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <span class="ml-2">5/5</span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="creditScore2">
                                                                        <label class="form-check-label" for="creditScore2">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">4/5</span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="creditScore3">
                                                                        <label class="form-check-label" for="creditScore3">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">3/5</span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="creditScore4">
                                                                        <label class="form-check-label" for="creditScore4">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">2/5</span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="checkbox" value="" id="creditScore5">
                                                                        <label class="form-check-label" for="creditScore5">
                                                                            <div><i class="icon ion-md-checkmark"></i></div>
                                                                            <span class="d-flex align-items-center credit-score mb-0">
                                                                                <i class="icon ion-md-star"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <i class="icon ion-md-star text-muted"></i>
                                                                                <span class="ml-2">1/5</span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-between">
                                                                <div class="col col-sm-4">
                                                                    <button type="reset" class="btn btn-outline-danger">Clear all Filters</button>                                                    </div>
                                                                <div class="col col-sm-8 text-right">
                                                                    <button class="btn btn-outline-secondary mr-3" data-toggle="collapse" data-target="#filterDropdown" role="button" aria-expanded="false" aria-controls="filterDropdown">Cancel</button>
                                                                    <button class="btn btn-primary">Apply Filter</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-sm-12 mt-lg-0 mt-sm-3">
                                            <div class="row d-flex justify-content-end">
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group filter-search">
                                                        <input type="text" class="form-control" placeholder="Search...">
                                                        <button class="btn btn-link"><i class="icon ion-md-search"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <select class="custom-select" id="inputGroupSelect01">
                                                        <option selected>Sort by</option>
                                                        <option value="1">Sort A - Z</option>
                                                        <option value="2">Sort Z - A</option>
                                                        <option value="3">Recent Projects</option>
                                                        <option value="4">Old Projects</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row flex-xl-nowrap">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 table-dashboard">
                                    <thead>
                                    <tr>
                                        <th scope="col">Payment ID</th>
                                        <th scope="col">Farmer Name</th>
                                        <th scope="col">Amount Paid</th>
                                        <th scope="col">EMI</th>
                                        <th scope="col">EMI Terms</th>
                                        <th scope="col">Next Due Date</th>
                                        <th scope="col">Last Repayment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($payments) && $payments->count()>0)
                                        @foreach($payments as $payment)
                                            @if(!$payment->project instanceof \App\Models\Project)
                                                @continue;
                                            @endif
                                            @php
                                             $total_payment_amount = $payment->project->emi_count * $payment->project->emi_amount;
                                             $paid_payment_amount = $payment->emiNumber() * $payment->project->emi_amount;
                                             $remaining_payment_amount = $payment->project->emi_count * $payment->project->emi_amount - $payment->emiNumber() * $payment->project->emi_amount;
                                            @endphp
                                            <tr>
                                        <th scope="col">{{$payment->project->payment_id}}</th>
                                        <td class="text-capitalize">{{$payment->project->farmer_name}}
                                            <div class="additional-info">
                                                <div class="location"><i class="icon ion-md-pin"></i>{{$payment->project->district->name}}</div>
                                                <div class="contact-no"><i class="icon ion-md-call"></i>{{$payment->project->farmers->first()->contact_no}}</div>
                                                <div class="credit-score"><img src="../img/icon_meter_level3.png" alt="">{{round(creditScoreToBankability($payment->project->farmers->first()->credit_score),2)}}/5</div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="progress-wrap dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <p class="mb-2">{{displayUnitFormat('amount', $paid_payment_amount) }} of {{displayUnitFormat('amount',$total_payment_amount )}}</p>
                                                <div class="progress">
                                                    <span class="progress-green" style="width:{{(( $paid_payment_amount / $total_payment_amount) * 100)}}%;"></span>
                                                </div>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                                    <div class="dropdown-item pl-3 pr-3 mb-2">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                Amount Paid <span class="d-block text-muted"> {{displayUnitFormat('amount',$paid_payment_amount) }} of <strong>{{ displayUnitFormat('amount',$total_payment_amount)}}</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-green" style="width:{{(($paid_payment_amount)/$total_payment_amount * 100)}}%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-item pl-3 pr-3">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col-md-6">
                                                                Amount Left <span class="d-block text-muted">{{displayUnitFormat('amount' ,$remaining_payment_amount)}} of <strong> {{displayUnitFormat('amount' ,$total_payment_amount)}}</strong></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="progress">
                                                                    <span class="progress-grey" style="width:{{(($remaining_payment_amount/$total_payment_amount) * 100)}}%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{displayUnitFormat('amount',$payment->amount)}}</td>
                                        <td>
                                            <div class="progress-wrap emi-terms">
                                                <p class="mb-2">{{$payment->emiNumber()}} / {{$payment->project->emi_count}}</p>
                                                <div class="progress">
                                                    <span class="progress-blue" style="width:{{(($payment->emiNumber() / $payment->project->emi_count)*100)}}%;"></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td> {{ \App\Facade\DateConverter::ad_to_bs_format(\Carbon\Carbon::parse($payment->nextPaymentDueDate()))}}</td>
                                        <td>{{\App\Facade\DateConverter::ad_to_bs_format(\Carbon\Carbon::parse($payment->paid_date))}}</td>
                                    </tr>
                                    @endforeach
                                        <tr>
                                            <td colspan="5" class="pagination-td">{!!$payments->appends(request()->input())->links('pagination::bootstrap-4'); !!}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="5"> No data</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
@endsection
@section('js')
    <script src="{{asset('js/ogbadmin/Chart.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var paymentsOverview = document.getElementById('payments_overview');
            var recollectionOverview = document.getElementById('recollection_overview');
            paymentsOverview.height = 70;
            recollectionOverview.height = 70;
            function formatMoney(value)
            {
                return "Rs "+ new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(Number(value ));
            }
            var payments_overview = new Chart(paymentsOverview,{
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [{{$overdue_payments['total_regular_emi_amount']}},{{$overdue_payments['total_overdue_emi_amount']}}],
                        backgroundColor: ["#66BB6A", "#FF8C63"]
                    }],
                    labels: [['Regular EMI'],"Overdue EMI"]
                },
                options: {
                    legend: {
                        position: "right",
                        fontSize: 14,
                        labels: {
                            fontSize: 14
                        },
                        onClick: null
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                let  value = data.datasets[0].data[tooltipItem.index];
                                return formatMoney(value)
                            }
                        } // end callbacks:
                    }, //end tooltips
                }

            });
            var recollection_overview = new Chart(recollectionOverview,{
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [{{$total_monthly_amount_raised['regular']}},{{$total_monthly_amount_raised['overdue']}},{{$total_monthly_amount_raised['down_payment']}}],
                        backgroundColor: ["#007BFF", "#FFB300", '#7D78A3']
                    }],
                    labels: ["Regular EMI Collected","Overdue of Past Month" ,"Advance Amount Collected"]
                },
                options: {
                    legend: {
                        position: "right",
                        fontSize: 14,
                        labels: {
                            fontSize: 14
                        },
                        onClick: null
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                let  value = data.datasets[0].data[tooltipItem.index];
                                return formatMoney(value)
                            }
                        } // end callbacks:
                    }, //end tooltips
                }
            });

        });
    </script>
@endsection