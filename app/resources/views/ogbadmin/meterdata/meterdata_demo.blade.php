@extends('ogbadmin.layout')
@section('metadata') <META HTTP-EQUIV="refresh" CONTENT="240"> @endsection
@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        #sel1{
            /* margin-top:10px; */
            font-size:13px;
        }
        .filter-button{
            margin-left: 10px;
        }
        .span{
            text-align:center;
            display: inline;
        }
        .active{
            padding-left : 10px;
            border-left: 1px solid #ccc;
            display: inline-block;
            font-size: 15px;
            color: green;
        }
        .active .inactive{
            color: red;
        }
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .loading {position: absolute;top:50%;left:50%}

        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            margin-top:35px;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .card-container {
            padding: 2px 16px;
        }
        .center{
            text-align:center;
        }
        .card-element{
            text-align: center;
        }
        .card-element .span{
            font-size: 18px;
        }
        .custom-button{
            padding : 10px;
            border: 1px solid lightgreen;
            color: lightgreen;
            font-size: 22px;
            border-radius: 20px;
        }
        .off{
            margin-left: 10px;
        }
        .mytable th{
            font-size:12px
        }

        .mytable td{
            font-size:12px
        }

        img.icon-image{
            max-width: 30%;
            height:auto;
        }
        .subtitleText{
            font-size:40px;
            display:inline;
            position: relative;
            top: 10px;
        }
        .subtitleText-sub{
            font-size:12px;
            position:absolute;
            bottom: -16px;
            right: 40%;
        }
        .next-subtitleSub-sub{
            font-size: 12px;
            position: absolute;
            top: 50px;
            right:30%;

        }
        .positioning{
            position: relative;
        }
        .overlay {
            position: absolute;
            top: 0;
            height: 100%;
            width: 350px;
            background: rgba(0, 0, 0, 0);
            transition: background 0.5s ease;
            border-radius: 10px;
        }

        .container-custom:hover .overlay {
            display: block;
            background: rgba(0, 0, 0, .3);
        }

        .labelclass {
            width: 200px;
            height: 40px;
            text-align: center;
            color: white;
            background-color: black;
            border: 0;
            font-weight: bolder;
            position: absolute;
            opacity: 0;
            transition: opacity .35s ease;
            left: 150px;
            top: 300px;
            line-height:40px;
            margin: -10px;
            cursor: pointer;
            border-radius: 20px;

        }
        #imageform{
            position:relative;
            top : -340px;
        }
        .container-custom {
            /*max-height : 300px;*/
            /*max-width:350px;*/
            /*position:relative*/
            margin: 0 15px;
        }
        .container-custom:hover .labelclass{
            opacity: 1;
        }
        .cover-image img{
            max-width: 350px;
            max-height:300px;
        }
        .for-align-custom{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .cover-image,.overlay{
            height:125px;
            width: 125px;
            border-radius: 10px;
            overflow: hidden;
        }
        .cover-image img{
            height:100%;
            width: 100%;
            object-position: center;
            object-fit: cover;
        }
    </style>
@endsection
@section('title')FTP DATA VISUALIZE @endsection
@section('content')
    <div class="container-fluid">
        @include('common.alert')
        <div id="app">
            <div v-if="loaded" v-cloak>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="container-custom">
                        <div class="cover-image">
                            @if($image)
                                <img src="{{asset('storage/project_details/'.$image->file_name)}}"  alt="farm-image">
                            @else
                                <img src="{{asset('storage/images/default.png')}}"  alt="farm-image">
                            @endif

                        </div>
                        {{--<div class="overlay"></div>--}}
                        {{--@if(session('role') == App\Enums\Role::ADMIN)--}}
                            {{--<form action="{{route('image.store',['id' =>$project_id])}}" id = "imageform" method="POST" enctype="multipart/form-data">                            {{ csrf_field() }}--}}
                                {{--<label class = "labelclass image-upload" for="farm_image">Upload Image</label>--}}
                                {{--<input id="farm_image" name="farm_image" type="file" style="display:none" value="Upload Image" onchange="submitForm()"/>--}}
                            {{--</form>--}}
                        {{--@endif--}}

                    </div>
                    <div class="col-md-6 pull-right">
                        <h2 class = "farmer-name"><b>@{{ project.farmer_name }}</b> <span class="custom-button" style = "margin-left:10px">Funded</span></h2>
                        <div class = "clearfix"></div>
                        <div class="address">
                            <h5 style = "font-size:15px"><b>@{{ project.municipality.name }}, @{{ project.district.name }}, @{{ project.province.name }}</b> <span style = "font-size:18px;color:#696868;margin-left:21px">Project Developed By @{{ project.user.full_name }}</span></h5>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix"></div>
                <!-- Modal -->
                <div id="exportModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Export Data</h4>
                            </div>
                            <div class="modal-body">
                                <form action="generate" method = "POST" id = "export">
                                    {{csrf_field()}}
                                    <input type="hidden" name = "meter_id" value = "{{$meter_id}}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="inline-label required" for="startdate">StartDate</label>
                                            @if ($errors->has('start'))
                                                <span class="error">
                                                <strong>{{ $errors->first('start') }}</strong>
                                            </span>
                                            @endif
                                            <input class="form-control" placeholder="Enter startdate"  name="start" id="startdate"  type="date" required/>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="inline-label required" for="enddate">Enddate</label>
                                            @if ($errors->has('end'))
                                                <span class="error">
                                                <strong>{{ $errors->first('end') }}</strong>
                                            </span>
                                            @endif
                                            <input class="form-control" placeholder="Enter enddate"  name="end" id="enddate" type="date" required/>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="submit" name = "export" value = "Export" class = "btn btn-primary" style = "margin-top:20px">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                    <totals :runtime = "updateTotalRunTime" :today-discharge = "checkTodayDischarge" :flow="updateTotalFlow" :first-flow="firstDischarge"  :last-flow="lastDischarge" :key="lastDischarge" :a = "a"></totals>
                    <div class="card">
                            <div class="card-element">
                                <div class="pull-left">
                                    <div class="buttons">
                                        @if(session('role') == App\Enums\Role::ADMIN)
                                        <div class="row">
                                            <div class="col-md-3">
                                                <form action="{{route('meter.change_status')}}" method = "post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name = "value" value = "4951111">
                                                    <input type="hidden" name = "meter_id" value = "{{$meter_id}}">
                                                    <input type="submit" class = "btn btn-primary" value = "ON" onclick="return confirm('Are you sure?');">
                                                </form>
                                            </div>
                                            <div class="col-md-3 off">
                                                <form action="{{route('meter.change_status')}}" method = "post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="phone" value = "9803900166">
                                                    <input type="hidden" name = "value" value = "4951000">
                                                    <input type="hidden" name = "meter_id" value = "{{$meter_id}}">
                                                    <input type="submit" class = "btn btn-warning" value = "OFF" onclick="return confirm('Are you sure?');">
                                                </form>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="span">
                                    <span>Showing Data of @{{selectedDate > 9 ? "" + selectedDate: "0" + selectedDate}}-@{{ (parseInt(selectedMonth) + 1) > 9 ? "" + parseInt(selectedMonth) : "0" + (parseInt(selectedMonth) + 1) }}-@{{ selectedYear }}</span>
                                </div>
                                <div class = "active">
                                    <div class = "inactive" v-if="difference_in_hours > 2">
                                        Inactive
                                    </div>
                                    <div v-else>
                                        Active
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exportModal">Export</button>
                                </div>
                            </div>
                        <div v-if="todayData === false">
                            <div class="row">
                                <h1 class="col-md-12" style="text-align:center">No Data Found</h1>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" v-else>
                            <div class="col-md-12">
                                <template v-if="indicator === 'daily'">
                                    <Chart style="margin-top:35px"  meter_type="{{$meter->meter_type}}" :a = "a" :title="titleValue" :current="current" :voltage="voltage" :dcpower="dcpower" :acpower = "acpower" :discharge="discharge" :rpm="rpm" :frequency="frequency" :temperature="temperature" :humidity="humidity" :key="a"></Chart>
                                </template>
                                <template v-else-if="indicator === 'monthly' || indicator === 'yearly'">
                                    <Charts  style = "margin-top:35px" :selected-month = "selectedMonth" :selected-year = "selectedYear" :indicator = "indicator"  :aggregate-discharge="aggregateDischarge" :aggregate-run-time="aggregateRunTime" :key="aggregateDischarge"></Charts>

                                </template>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="pull-left">
                                    <button @click.prevent.stop="updateindicator('daily')" :key = "indicator" class = "btn btn-primary">Daily</button>
                                    <button @click.prevent.stop="updateindicator('monthly')" :key = "indicator" class = "btn btn-primary">Monthly</button>
                                    <button @click.prevent.stop="updateindicator('yearly')" :key = "indicator" class = "btn btn-primary">Yearly</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div v-if="this.indicator === 'daily'" class="for-align-custom">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model="selectedDate">
                                                    <option v-for="day in getNumbers(1,this.daysBasedYearMonth)">@{{ day }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model="selectedMonth" @change = "changeDays">
                                                    <option v-for="(key,value) in months" :value = "value">
                                                        @{{key}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model = "selectedYear" @change = "changeDays">
                                                    <option v-for="year in getNumbers(2018,2030)"  :value = "year">
                                                        @{{year}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 filter-button">
                                            <div class="form-group">
                                                <input type="submit" value = "Filter" @click = "filterChart()" class = "btn btn-info">
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="indicator === 'monthly'" class="for-align-custom">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model="selectedMonth">
                                                    <option v-for="(key,value) in months" :value = "value">
                                                        @{{key}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model = "selectedYear">
                                                    <option v-for="year in getNumbers(2018,2030)"  :value = "year">
                                                        @{{year}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 filter-button">
                                            <div class="form-group">
                                                <input type="submit" value = "Filter" @click = "filterChart()" class = "btn btn-info">
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="indicator === 'yearly'" class="for-align-custom">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select  id="sel1" v-model = "selectedYear">
                                                    <option v-for="year in getNumbers(2018,2030)"  :value = "year">
                                                        @{{year}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 filter-button">
                                            <div class="form-group">
                                                <input type="submit" value = "Filter" @click = "filterChart()" class = "btn btn-info">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div style="display: flex;justify-content: flex-end;width:100%">
                                    <a  href="{{route('ogbadmin.project.details',['id' => $project_id])}}" class = "btn btn-primary">Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        <div v-else>
            <Loader :loaded = "loaded"></Loader>
        </div>
    </div>
    <script>
        function submitForm(){
            document.getElementById("imageform").submit();
        }
        window.meter_id = {{$meter_id}};
    </script>

    <script src="{{asset('/offgrid-bazaar/js/vue.min.js')}}"></script>
    <script src="{{asset('/offgrid-bazaar/js/highcharts.js')}}"></script>
    <!-- vue-highcharts should be load after Highcharts -->
    <script src="{{asset('/offgrid-bazaar/js/vue-highcharts.min.js')}}"></script>
    <script src = "{{asset('/offgrid-bazaar/js/axios.min.js')}}"></script>
    <script src = "{{asset('/offgrid-bazaar/js/main.js')}}"></script>
    </div>
@endsection
