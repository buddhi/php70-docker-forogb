<div class="modal fade" id="change-mobile" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content p-4 p-md-5">
            <section id="invite-agent" class="row  justify-content-center invite-partner-section">

                <div class="row  justify-content-center">
                    <div class="col-12 text-center mb-3">
                        <button type="button" class="close"  aria-label="Close" id="close-modal">
                            <span aria-hidden="true">&times;</span></button>
                        <h3>Change Mobile number</h3>
                    </div>
                    <div class="col-12">
                        <div class="col-12">
                            <form class="form-horizontal" id="change_mobile" enctype="multipart/form-data" method="post" action="{{route('change-mobile' ,$user->user_id)}}">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">

                                <div class="form-group">
                                    <label for="email">Mobile:</label>
                                    <input id="phone" class="form-control" placeholder="Enter new phone number" name="phone" type="text">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary verification-email-agent" rel="verification-success">Change</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>