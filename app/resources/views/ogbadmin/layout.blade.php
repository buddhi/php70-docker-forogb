<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <link href = '{{asset('fontawesome/css/all.min.css')}}' rel="stylesheet">
    @yield('css')
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">
    <style>
        #flash-message{
            position: absolute;
            top: 100px;
            right: 20px;
            z-index: 10;
        }
        .error{
            color: red;
        }
    </style>
    <title>@yield('title')</title>
    @yield('styles')
</head>

<body>
@include('ogbadmin.header')

<div class="container-fluid">
    <div class="row flex-xl-nowrap">
        @if(Illuminate\Support\Facades\Route::currentRouteName() != 'ogbadmin.meterdata_demo.show')
            @include('ogbadmin.sidebar')
        @endif
        @yield('content')
    </div>
</div>
<div id="flash-message">
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissible" role="alert" class="flash-message">
            {{session()->get('success_message')}}
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" class="flash-message">
            {{session()->get('error')}}
        </div>
    @endif
</div>


<script src="{{asset('js/ogbadmin/jquery.min.js')}}">
</script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/Chart.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script>$('#flash-message').delay(2000).fadeOut(2000);</script>
@yield('js')
</body>
</html>