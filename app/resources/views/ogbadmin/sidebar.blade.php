<div class="col-12 col-md-3 col-xl-2 bd-sidebar">
    <nav class="">
        <ul class="nav flex-column">
            {{-- <li class="nav-item">
                 <a class="nav-link" href="#"><i class="icon h5 ion-md-speedometer"></i>
                     Dashboard</a>
             </li>--}}
            <li class="nav-item">
                {{--<a class="nav-link {{request()->is('ogbadmin/project') || request()->is('partner-user/project*')? 'active' : '' }}"--}}
                   {{--href="{{route('ogbadmin.project.index')}}"><i class="icon ion-md-folder-open"></i>Projects</a>--}}
                <a class="nav-link {{ request()->is('ogbadmin/agent*')||request()->is('partner-user/agent*') ? 'active' : '' }}"
                   href="{{ session('role') == \App\Enums\Role::PARTNER_USER? route('partner-user.project.index'):route('ogbadmin.project.index')}}"><i
                            class="icon ion-md-folder-open"></i>Projects</a>
            </li>
            @can('view',\App\Models\ProjectInvestment::class)
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('ogbadmin/investment*')? 'active':''}}"
                       href="{{route('ogbadmin.investment.index')}}"><i class="icon ion-md-stats"></i>Investments</a>
                </li>
            @endcan
            @can('view',\App\Models\Payment::class)
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('ogbadmin/payment*') || request()->is('partner-user/payment*') ? 'active' : '' }}"
                       href="{{ route('ogbadmin.payment.index')}}"><i class="icon ion-md-cash"></i>Payments</a>
                </li>
            @endcan
            {{-- <li class="nav-item">
                 <a class="nav-link" href="#"><i class="icon ion-md-trending-up"></i>Impact</a>
             </li>--}}
            @can('view',\App\Models\Partner::class)

                <li class="nav-item">
                    <a class="nav-link {{request()->is('ogbadmin/partner')? 'active' : '' }}"
                       href="{{route('ogbadmin.partner.index')}}"><i class="icon ion-md-globe"></i>Partners</a>
                </li>
            @endcan
            @can('view',\App\User::class)
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('ogbadmin/partner-user*') ? 'active' : '' }}"
                       href="{{route('ogbadmin.partner-user.index')}}"><i class="icon ion-md-person"></i>Users</a>
                </li>
            @endcan
            @can('view',\App\User::class)
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('ogbadmin/agent*')||request()->is('partner-user/agent*') ? 'active' : '' }}"
                       href="{{ session('role') == \App\Enums\Role::PARTNER_USER? route('partner-user.agent', Illuminate\Support\Facades\Auth::user()->partner_id):route('ogbadmin.agent.index')}}"><i
                                class="icon ion-md-person-add"></i>Agents</a>
                </li>
            @endcan
        </ul>
    </nav>

</div>