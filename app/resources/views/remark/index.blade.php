@extends($default_page)
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
@section('content')
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success_message') }}
        </div>
    @endif
    @include('common.alert')
    <div class="container">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #f5f5f5">
                        <strong>Project Discussion page</strong>
                        <ul>
                            <li>This page is associated with discussion of project  Id {{$project->id}}.</li>
                            <li>Project is created by:{{$project->user->full_name}}</li>
                            <li>Farmer name: {{$project->farmer_name}} </li>
                        </ul>
                        Please enter the remarks below to discuss about the events......
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach ($remarks as $remark)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#">
                                {{ isset($remark->commenter_name)? $remark->commenter_name:"somebody"}}
                            </a>
                            said {{$remark->created_at->diffForHumans()}}..

                        </div>

                        <div class="panel-body">
                            {{ $remark->comment }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        @if (auth()->check())
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form method="POST" action="{{route('remark.create',$project->id)}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <textarea name="remark" id="remark" class="form-control" placeholder="Have something to say?" rows="5"></textarea>
                        </div>
                        @if ($errors->has('remark'))
                            <span class="help-block">
                                <strong>{{ $errors->first('remark') }}</strong>
                            </span>
                        @endif

                        <button type="submit" class="btn btn-default">Post</button>
                    </form>
                </div>
            </div>
        @else
            <p class="text-center">Please <a href="{{ route('login') }}">sign in</a> to participate in this discussion.</p>
        @endif
    </div>
@endsection