@extends('layouts.default')
@section('metadata') <META HTTP-EQUIV="refresh" CONTENT="240"> @endsection
@section('title')Water Requirement @endsection
@section('content')
    <div class="container-fluid">
        <div id="app">
            <div class="row">
                <div id="app">
                    <div v-if="loaded" v-cloak>

                        <Charts style = "margin-top:100px" :net-income="netIncome" :net-expenditure="netExpenditure" :key="netIncome"></Charts>

                    </div>
                    <div v-else>
                        Loading
                    </div>


                </div>
            </div>
        </div>
    </div>



@endsection
