<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="language" content="en">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('metadata')
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/png"/>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,600" rel="stylesheet">
    @yield('css')
    <title>Offgrid Bazaar | @yield('title')</title>
  </head>
  <body>
    <div id="app">
      @include('common.alert')
      @yield('flash-message')
      @include('common.login-logout-modal')
      <header class="navbar navbar-expand-sm navbar-light bg-light {{ Request::path() ==  '/' ? ' fixed' : ''  }}">
        <div class="container">
          <a class="navbar-brand" href="{{url('/')}}">
          <img src="{{asset('/offgrid-bazaar/images/ogb-logo.png')}}" class="toplogo" alt="Logo" height="40">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto">
              @guest
              <li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#login">Log in</a></li>
              <li class="nav-item ml-md-3"><a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#register">Sign up for free</a></li>
              @else
              <li class="nav-item dropdown mr-3">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownmenu" data-toggle="dropdown"> Hello <strong>{{strtok(Auth::user()->full_name,' ')}}</strong> </a>
                <div class="dropdown-menu" aria-labelledby="dropdownmenu">
                  <a class="dropdown-item" href="/investment/my-investment">My Investments</a>
                  <a class="dropdown-item" href="{{route('profile.edit')}}">My Account</a>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                  Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
              <my-list/>
              @endguest
            </ul>
          </div>
        </div>
      </header>
      @yield('content')
    </div>
    <footer>
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-12 my-4">
            <hr>
          </div>
          <div class="col-md-5">
            <p class="text-uppercase bold text-muted">About us</p>
            <h5 class="fund-title">Off Grid Bazaar</h5>
            <small class="footer-text">Off Grid Bazaar (OGB) is an online project development platform to scale up the implementation of off-grid solar-based projects to serve smallholding farmers and rural communities in Nepal.</small>
            <p><a href="http://ghampower.com/investors" class="small" target="_blank">Learn More</a></p>
            <small>Copyright 2019, Off Grid Bazaar, All Rights Reserved.</small>
          </div>
          <div class="col-md-4">
            <a href="http://ghampower.com" target="_blank"><img src="/img/ghampower-logo.png" alt="GhamPower" height="60" class="mb-2"></a>
            <address>
              <strong class="footer-text">Gham Power Nepal Private Limited</strong><br>
              <small class="footer-text">Shah Villa - 292, Chundevi Marga, Maharajgunj<br>
              Kathmandu 44600, Nepal
              <abbr title="Phone"></abbr> 977-1-4721486, 4721450<br>
              <a href="http://ghampower.com" target="_blank" class="footer-text">http://ghampower.com</a>
              </small>
            </address>
            <ul class="list-inline lead">
              <li class="list-inline-item"><a href="https://www.facebook.com/ghampower/" target="_blank" title=""><i class="icon ion-logo-facebook"></i> </a></li>
              <li class="list-inline-item"><a href="https://twitter.com/ghampower" target="_blank" title=""><i class="icon ion-logo-twitter"></i></a></li>
              <li class="list-inline-item"><a href="https://www.linkedin.com/company/gham-power" target="_blank" title=""><i class="icon ion-logo-linkedin"></i></a></li>
              <li class="list-inline-item"><a href="https://www.youtube.com/user/ghampower" target="_blank" title=""><i class="icon ion-logo-youtube"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    {{--START JS--}}
    @yield('js')
    {{--END JS--}}
    <!-- Scripts -->
    <script>
      var usd_exchange_rate = {!! currencyConverterFactor("USD") !!}
    </script>    
    <script src="{{ asset('js/app.js') }}"></script>
    @include ('common.analytics')
  </body>
</html>