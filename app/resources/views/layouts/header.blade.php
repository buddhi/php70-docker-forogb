<header class="navbar navbar-expand-sm navbar-light bg-light">
    <div class="container">
        @guest
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{asset('/offgrid-bazaar/images/ogb-logo.png')}}" class="toplogo" alt="Logo" height="40">
            </a>
        @else
            <a class="navbar-brand" href="{{route('project.list')}}">
                <img src="{{asset('/offgrid-bazaar/images/ogb-logo.png')}}" class="toplogo" alt="Logo" height="40">
            </a>
        @endguest
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#login">Log in</a>
                </li>
                <li class="nav-item ml-3"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#signup">Sign
                        up</a></li>
            </ul>
        </div>
    </div>
</header>

         