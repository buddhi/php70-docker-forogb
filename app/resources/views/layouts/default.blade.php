<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="8Q4cLZ61CThcZIFajfch6XeT5c628AKFBR7eit2PucI"/>
    <meta name="language" content="en">
    @yield('metadata')
    {{-- START CSS--}}
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/screen.css')}}" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/print.css')}}" media="print">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/ie.css')}}" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/form.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.1/css/simple-line-icons.css">
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/theme/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/offgrid-bazaar/css/alertify/alertify.min.css')}}">
    @yield('css')
    {{--END CSS --}}
    {{--Necessary for create project so js is kept above--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('/offgrid-bazaar/assets/9bb1aea1/jquery.js')}}"></script>
    <title>Offgrid | @yield('title')</title>
</head>

<body>
    <div class="container" id="page">
        @include('layouts.headsection')
        <section class="maincontent">
          @yield('content')
        </section>
    </div>
    {{--START JS--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
    <script src="{{asset('/offgrid-bazaar/js/alertify/alertify.min.js')}}"></script>
    <script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    @yield('js')
    @include('common.logout');
    {{--END JS--}}
</body>

</html>