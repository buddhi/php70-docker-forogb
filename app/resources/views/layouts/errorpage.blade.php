<!DOCTYPE html>
<html lang="en">
<head>
    <base href="ui/"/>
    <meta charset="UTF-8">
    <title>ERROR</title>
    <link rel="stylesheet" href="http://offgrid-bazaar1.loc/offgrid-bazaar/css/main.css">
    <link rel="stylesheet" type="text/css" href="http://offgrid-bazaar1.loc/offgrid-bazaar/css/form.css">
</head>
<body>
<div class="text-center">
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="form-logo">
            </div>

        </div>
        <h1>Forbidden</h1>
        <p>You don't have permission to access the page or link may be broken on this server.<br/>
        </p>
        <p>Additionally, a 403 Forbidden error was encountered while trying to use an ErrorDocument to handle the request.</p>

        @guest
            <a href="{{route('index')}}"><strong>Please click here to go back home </strong></a>
        @else
            <a href="{{route('project.list')}}"><strong>Please click here to go back home</strong></a>
        @endguest
    </div>

</div>

</body>

</html>