<header>
    @if (session()->has('role_message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">X</span>
            </button>
            <strong>{!! session()->get('role_message') !!} !!</strong>
        </div>
    @endif
    <div class="mainheader container">
        <div class="col-xs-4 col-md-3">
            @guest
                <a href="{{url('/')}}">
                    <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                </a>
            @else
                    <a href="{{route('project.list')}}">
                        <img src="{{asset('/offgrid-bazaar/images/logo.png')}}" class="toplogo" alt="Logo">
                    </a>
            @endguest
        </div>
        <div class="col-xs-4 col-md-5">
            <div class="stat small">
                <h4><i class="icon-project"></i> {{$project_count}}</h4>
                <small class="light">Projects</small>
            </div>
            <div class="stat small">
                <h4><i class="icon-investment"></i> ${{round($received_investment/100000,0)}}K</h4>
                <small class="light">Investment</small>
            </div>
        </div>
        <div class="col-xs-4 col-md-4 text-right">
            <nav class="topright">
                @guest
                    <a href="{{url('/login')}}">Login</a> |
                    <a href="{{url('/register')}}">Signup</a>
                @else
                    {{Auth::user()->username }}
                        |<strong>{{returnRole(session('role'))}}</strong>
                    |<a href="{{ route('logout') }}"
                       onclick="event.preventDefault();logout();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                @endguest
            </nav>
        </div>
    </div>
    <div class="container">
        <nav class="nav topnav">
            <a class="nav-link active" href="javascript:void(0);">All</a>
            <a class="nav-link" href="javascript:void(0);">Water Pump</a>
            <a class="nav-link" href="javascript:void(0);">Grinding Mill</a>
            <a class="nav-link" href="javascript:void(0);">Dairy Chilling</a>
            <a class="nav-link" href="javascript:void(0);">Refrigeration</a>
            <a class="nav-link" href="javascript:void(0);">Microgrid</a>
        </nav>
    </div>
</header>
