<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">
    <style>
        #flash-message{
            position: absolute;
            top: 100px;
            right: 20px;
            z-index: 10;
        }
    </style>
    <title>Contact Page</title>
</head>

<body>

<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>
<div id="flash-message">
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissible" role="alert" class="flash-message">
            {{session()->get('success_message')}}
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" class="flash-message">
            {{session()->get('error')}}
        </div>
    @endif
</div>

<div class="container-fluid">
    <div class="row">
        <main class="col-12 col-md-12 col-xl-12 bd-content">
            <div class="box-wrap verify-mobile-number">
                <div class="text-center mb-4">
                    <img src="{{asset('img/hand.png')}}" alt="Contact Ghampower" height="80" width="80">
                </div>

                <ul class="mt-2 list-unstyled">
                    <p>
                        Dear User,<br>
                        Your account has been disabled by  the Off Grid Bazaar admin. Please contact Gham Power to re-activate your account.
                    </p>
                    <li>Gham Power Nepal Private Limited</li>
                    <li>House No. 292, Chundevi Marga</li>
                    <li>Maharajgunj-3,Kathmandu, Nepal</li>
                    <br>

                    <li> <img src="{{asset('img/call-icon.png')}}" alt="" style="height: 16px"> +977-1-4721486</li>
                    <li class="my-2"> <i style="color:#999; font-size: 14px;" class="icon ion-md-mail"></i><a href="mailto:contact@ghampower.com" style="vertical-align: top;"> contact@ghampower.com</a></li>
                    <li class="my-2"> <i style="color:#999; font-size: 14px;" class="icon ion-md-mail"></i><a href="mailto:ogbadmin@ghampower.com" style="vertical-align: top;"> ogbadmin@ghampower.com</a></li>
                </ul>
            </div>
        </main>
    </div>
</div>

<script src="{{asset('js/ogbadmin/jquery.min.js')}}">
</script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>$('#flash-message').delay(2000).fadeOut(2000);</script>

</body>
</html>