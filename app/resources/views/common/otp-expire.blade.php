<script>
    var countdown = {{$expire_time}}  * 1000;
    var timerId = setInterval(function(){
        countdown -= 1000;
        var min = Math.floor(countdown / (60 * 1000));
        var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);
        if (countdown <= 0) {
            $("#countTime").html("00:00");
            alert("otp has expired!");
            clearInterval(timerId);
            $('#otp-time').html('OTP has expired.');
            $('#otp-expire').css({'display':'block'})
        } else {
            $("#countTime").html(min + " : " + sec);
        }
    }, 1000);

    $(function() {
        $("#change_mobile").validate({
            rules: {
                phone: {
                    required: true,
                    maxlength: 10,
                    minlength: 10,
                    number: true
                },
                action: "required",
                error: function() {
                    $(this).addClass("error");
                }
            },
            messages: {
                phone: {
                    required: "Phone number is required",
                    minlength: "Phone number should be atleast 10 digits",
                    maxlength: "Phone number should not be more 10 digits"
                },
                action: "Please provide some data"
            }
        });
        $('#close-modal').click(function () {
            $('#change-mobile').modal('hide');
        });
    });
</script>