@if ($errors->has('digit*'))
    <div class="alert alert-danger text-center" role="alert">
        <img src="{{asset('img/cross.png')}}" height="22" alt="" class="mr-1">
        <span class="text-danger" style="font-size:13px ;color:#f16c6a;">{!! $errors->first('digit1')  !!}</span>
    </div>
@endif

@if (session()->has('error'))
    <div class="alert alert-danger text-center" role="alert" class="flash-message">
        <img src="{{asset('img/cross.png')}}" height="20" alt="" class="mr-1">
        <span class="text-danger" style="font-size:13px;color:#f16c6a;"> {!! session()->get('error') !!}</span>
    </div>
@endif

@if (session()->has('success'))
    <div class="alert alert-success text-center" role="alert" class="flash-message">
        <img src="{{asset('img/check.png')}}" height="22" alt="" class="mr-1">
        <span style="font-size:12px;color:#45d887;" > {!! session()->get('success') !!}</span>
    </div>
@endif