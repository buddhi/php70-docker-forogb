<!-- <div class="modal fade" id="login" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content p-4 p-md-5">
      <div class="row  justify-content-center">
        <div class="col-12 text-center mb-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h3>Login As:</h3>
          <hr>
        </div>
        <div class="form-group">
          <a class="btn btn-lg btn-primary text-white mr-2" data-dismiss="modal" data-toggle="modal" data-target="#login-investor" role="button" id="investor">Investor</a>
          <a class="btn btn-lg btn-success text-white" href="{{route('login')}}" role="button">Developer</a>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="modal fade" id="register" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content p-4 p-md-5">
      <div class="row  justify-content-center">
        <div class="col-12 text-center mb-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h3>Sign Up As:</h3>
          <hr>
        </div>
        <div class="form-group">
          <a class="btn btn-lg btn-primary text-white mr-2" data-dismiss="modal" data-toggle="modal" data-target="#signup" role="button" id="investor">Investor</a>
          <a class="btn btn-lg btn-success text-white" href="{{route('register')}}" role="button">Developer</a>
        </div>
      </div>
    </div>
  </div>
</div> -->
<div class="modal fade" id="login" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content p-4 p-md-5">
      <div class="row  justify-content-center">
        <div class="col-12 text-center mb-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h3>Login</h3>
          <hr>
        </div>
        <div class="col-12">
          <div class="btn-group d-flex justify-content-center" role="group">
            <button type="button" class="btn btn-secondary">Investor</button>
            <a href="{{route('login')}}"  class="btn btn-outline-secondary">Developer <i class="icon ion-md-log-out"></i></a>
          </div>
          <hr>
        </div>
        <div class="col-12">
          <section class="row">
            <div class="col-3">
              <a class="btn btn-block btn-lg btn-facebook" href="{{route('social.login',['provider'=>'facebook'])}}">
                <i class="icon ion-logo-facebook"></i>
              </a>
            </div>
            <div class="col-3">
              <a class="btn btn-block btn-lg btn-twitter" href="{{route('social.login',['provider' => 'twitter'])}}">
                <i class="icon ion-logo-twitter"></i>
              </a>
            </div>
            <div class="col-3">
              <a class="btn btn-block btn-lg btn-google" href="{{route('social.login',['provider'=>'google'])}}">
                <i class="icon ion-logo-google"></i>
              </a>
            </div>
            <div class="col-3">
              <a class="btn btn-block btn-lg btn-linkedin" href={{route('social.login',['provider'=>'linkedin'])}}>
                <i class="icon ion-logo-linkedin"></i>
              </a>
            </div>
            <div class="col-12">
              <hr class="my-4">
            </div>
          </section>
        </div>
        <form class="col-12" action="{{route('login')}}" method="post">
          {{csrf_field()}}
          <input type="hidden" name="role" value="10" />
          <div class="form-group">
            <label><strong>Email address</strong>
            <input type="email"  name="email" class="form-control" placeholder="Email address">
            </label>
          </div>
          <div class="form-group">
            <label><strong>Password <a href="{{url('/password/reset')}}" class="float-right">Forgot?</a></strong>
            <input type="password" name="password" class="form-control" placeholder="Password">
            </label>
          </div>
          <div class="form-group">
            <button class="btn btn-lg btn-block btn-success" type="submit">Log in</button>
            <p class="mt-3 text-center">Don't have an account? <strong><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#register">Sign up</a></strong></p>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="register" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content p-4 p-md-5">
      <div class="row justify-content-center">
        <div class="col-12 text-center mb-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4>Sign up to start Investing</h4>
        </div>
        <div class="col-12">
          <div class="btn-group d-flex justify-content-center" role="group">
            <button type="button" class="btn btn-secondary">Investor</button>
            <a href="{{route('register')}}"   class="btn btn-outline-secondary">Developer <i class="icon ion-md-log-out"></i></a>
          </div>
          <hr>
        </div>
        <section id="email-login" class="collapse signup-section">
          <form action="{{route('register')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="role[]" value="10">
            <div class="row">
              <div class="form-group col-12">
                <label><span>Your full name</span>
                <input type="text" name="full_name" class="form-control" placeholder="eg. John Smith">
                @if ($errors->has('full_name'))
                <span class="error">{{ $errors->first('full_name') }}</span>
                @endif
                </label>
              </div>

              <div class="col-md-4 input-group" style="padding-right: 0px;">
                <label for="country_code">Country code:</label>
                <div class="input-group-prepend">
                  <span class="input-group-text">+</span>
                </div>
                <input placeholder="" name="country_code" type="number" value="977" id="country_code" class="form-control">
              </div>
              <div class="col-md-8">
                <label for="phone" class="inline-label">Phone number:</label>
                <input placeholder="Enter your phone number" name="phone" type="text" id="phone" class="form-control">
                @if ($errors->has('phone'))
                  <span class="error">{{ $errors->first('phone') }}</span>
                @endif
              </div>
              <div class="form-group col-md-12 mt-4">
                <label><span>Address</span>
                  <input type="text" name="address" class="form-control" placeholder="Enter address">
                  @if ($errors->has('address'))
                    <span class="error">{{ $errors->first('address') }}</span>
                  @endif
                </label>
              </div>
              <div class="form-group col-12">
                <label><span>Email address</span>
                  <input type="email" name="email" class="form-control" placeholder="Email address">
                  @if ($errors->has('email'))
                    <span class="error">{{ $errors->first('email') }}</span>
                  @endif
                </label>
              </div>


              <div class="form-group col-md-6">
                <label><span>Password</span>
                <input type="password" name ="password" class="form-control" placeholder="Password">
                @if ($errors->has('password'))
                <span class="error">{{ $errors->first('password') }}</span>
                @endif
                </label>
              </div>
              <div class="form-group col-md-6">
                <label><span>Repeat Password</span>
                <input  type="password" name="password_confirmation" class="form-control" placeholder="Repeat Password">
                </label>
              </div>
              <div class="form-group col-12 text-center">
                <button class="btn btn-lg btn-block btn-success">Sign up</button>
                <p class="mt-3 mb-0 text-center">Already have an account? Then you should <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#login">log in</a></p>
                <hr class="my-4">
                or, <strong><a href="#" class="switch-signup" rel="social">sign up using social accounts</a></strong>
              </div>
            </div>
          </form>
        </section>
        <section id="social" class="row text-center signup-section">
          <div class="col-12">
            <hr class="mb-4">
            <p>Use any of your social profiles to create an account</p>
          </div>
          <div class="col-6">
            <a href="{{route('social.login',['provider'=>'facebook'])}}" class="btn btn-block btn-lg btn-facebook mb-3 "><i class="icon ion-logo-facebook"></i> Facebook</a>
          </div>
          <div class="col-6">
            <a href="{{route('social.login',['provider' => 'twitter'])}}" class="btn btn-block btn-lg btn-twitter mb-3"><i class="icon ion-logo-twitter"></i> Twitter</a>
          </div>
          <div class="col-6">
            <a href="{{route('social.login',['provider'=>'google'])}}" class="btn btn-block btn-lg btn-google mb-3"><i class="icon ion-logo-google"></i> Google</a>
          </div>
          <div class="col-6">
            <a href="{{route('social.login',['provider' => 'linkedin'])}}" class="btn btn-block btn-lg btn-linkedin mb-3"><i class="icon ion-logo-linkedin"></i> LinkedIn</a>
          </div>
          <div class="col-12 text-center">
            <hr class="my-4">
            otherwise, <strong><a href="#" class="switch-signup" rel="email-login">sign up using email address</a></strong>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>