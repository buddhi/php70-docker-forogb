<section class="hero-banner py-5">
  <div class="container">
    <div class="row text-center justify-content-center">
      <div class="col-12 mb-4">
        <p class="lead text-muted">OUR IMPACT SO FAR</p>
      </div>
      <div class="col-lg-3 col-4">
        <p class="h2 m-0"><i class="icon ion-md-cube d-block text-primary"></i> {{$project_count}}</p>
        <span>Active Projects</span>
      </div>
      <div class="col-lg-3 col-4">
        <p class="h2 m-0"><i class="icon ion-md-podium d-block text-primary"></i>${{round($received_investment/100000,0)}}K</p>
        <span>Funds raised</span>
      </div>
      <div class="col-lg-3 col-4">
        <p class="h2 m-0"><i class="icon ion-md-cash d-block text-primary"></i>${{round($expected_investment/100000,0)}}K</p>
        <span>Funds Needed</span>
      </div>
      <div class="w-100 d-block mb-3 mb-sm-5"></div>
      <div class="col-lg-2 col-sm-4 col-6">
        <p class="h4 text-muted m-0"><img src="/img/icon_lives.png" alt="Co2"> {{$lives_impacted}}</p>
        <span>Lives impacted</span>
      </div>
      <div class="col-lg-2 col-sm-4 col-6">
        <p class="h4 text-muted m-0"><img src="/img/icon_solar.png" alt="Co2"> {{$solar_capacity/1000}} kW</p>
        <span>Solar Capacity</span>
      </div>
      <div class="col-lg-2 col-sm-4 col-6">
        <p class="h4 text-muted m-0"><img src="/img/icon_co2.png" alt="Co2"> {{round($total_co2_curbed)}} Tonnes</p>
        <span>CO<sub>2</sub> Curbed</span>
      </div>
    </div>
  </div>
</section>