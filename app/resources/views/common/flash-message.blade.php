<div id="flash-message" style="position: absolute;top: 100px;right: 20px;z-index: 10;">
    @if (session()->has('success_message'))
        <div class="alert alert-success alert-dismissible" role="alert" class="flash-message">
            {{session()->get('success_message')}}
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" class="flash-message">
            {{session()->get('error')}}
        </div>
    @endif
</div>