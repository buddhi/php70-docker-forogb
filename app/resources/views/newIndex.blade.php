<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Checks</title>
</head>
<body>

@if (App::environment('staging','development','local'))

{{--User verification  Email Template start--}}
<div  align="center">
  <h5>1.User verification  Email Template : Investor<br>
      Subject:Email Verification for your Off Grid Bazaar Account
  </h5>

</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
                <td><a href="{{route('my.investment')}}" style="color:#959292;font-family:Trebuchet MS;font-size:14px;text-decoration: none"
                       target="_blank">My Investments</a> | <a href="{{route('project.list')}}"
                                                               style="color:#959292;font-family:Trebuchet MS;font-size:14px;text-decoration: none"
                                                               target="_blank">Project Catalog</a></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Welcome to Off Grid bazaar!
        </h2>
        <p>Dear {user-name},</p>
        <p>
            Thank you for registering with Off Grid Bazaar platform. We are excited to have your support in funding high
            impact projects in Nepal.
        </p>
        <p>
            Thank you for registering with Off Grid Bazaar platform. We are excited to have your support in funding high
            impact projects in Nepal.
        </p>
        <p>
            <a href="#" style="color:#4472c4;" target="_blank">Please click here to verify your email address</a>
        </p>
        <p>
            If the link above does not work, please copy and paste this URL into your browserís address bar:
        </p>
        <a href="#" target="_blank" style="color:#4472c4;">https://offgridbazaar.com/project-fund/somerandomemaillink58?newcomeshereanjkbkjbf/ncbkbck/kbckbejck/jkbcdjkkjbc/kjbcjkbc</a>
        <p>
            Your Impact Investor Account will be up and running once you verify your email. You will then be able to
            review all investment-ready projects on Off Grid Bazaar website, and select the ones that best meet your
            criteria.
        </p>
        <p>
            If you have any questions, please contact me directly.
        </p>
        <p>
            Bishwaraj Bhattarai <br/>
            Impact Investment Manager <br/>
            Email: <a href="mailto:bishwaraj@ghampower.com" style="color:#4472c4;">bishwaraj@ghampower.com</a>
        </p>
        <p>
            Your partner in building a better planet, <br/>
            The Off Grid Bazaar Team
        </p>
    </div>
    @include('emails.includes.footer')
</div>

{{--User Verification  Email Template End --}}

{{--User verification  Email Template start--}}
<div  align="center">
    <h5>1.1 User verification  Email Template : Developer<br>
        Subject:Email Verification for your Off Grid Bazaar Account
    </h5>

</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Welcome to Off Grid bazaar!
        </h2>
        <p>Dear {user-name},</p>
        <p>
            Thank you for registering with Off Grid Bazaar platform. We are excited to have your support in funding high
            impact projects in Nepal.
        </p>
        <p>
            Thank you for registering with Off Grid Bazaar platform. We are excited to have your support in funding high
            impact projects in Nepal.
        </p>
        <p>
            <a href="#" style="color:#4472c4;" target="_blank">Please click here to verify your email address</a>
        </p>
        <p>
            If the link above does not work, please copy and paste this URL into your browserís address bar:
        </p>
        <a href="#" target="_blank" style="color:#4472c4;">https://offgridbazaar.com/project-fund/somerandomemaillink58?newcomeshereanjkbkjbf/ncbkbck/kbckbejck/jkbcdjkkjbc/kjbcjkbc</a>
        <p>
            Your Impact Investor Account will be up and running once you verify your email. You will then be able to
            review all investment-ready projects on Off Grid Bazaar website, and select the ones that best meet your
            criteria.
        </p>
        <p class="email-text">
            If you have any questions, please contact:</br>
            Gham Power Nepal</br>
            Phone:  01-4721486 / 4721123 / 4721486</br>
            Website: GhamPower.com/Agents</br>
            Email: OgbAdmin@ghampower.com</br>

        </p>
        <p>
            Your partner in building a better planet, <br/>
            The Off Grid Bazaar Team
        </p>
    </div>
    @include('emails.includes.footer')
</div>

{{--User Verification  Email Template End --}}

{{--User Activation  Email Template Developer Start --}}
<div  align="center">

    <h5>2. User Activation email Template:developer <br>
        Subject:User Activation for your Off Grid Bazaar Account
    </h5>
</div>
<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">

    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>

            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Welcome to Off Grid bazaar !
        </h2>
        <div>
            <p class="mail-title">Dear {full_name},</p>
            <p>Welcome to Off Grid Bazaar Developer team where you can create additional source of income by identifying local farmers requiring irrigation and helping us connect to them.</p>
            <p>Please follow the following steps to create projects</p>
            <ol>
                <li>Visit <a href="http://www.offgridbazaar.com" target="_blank">offgridbazaar.com </a>and select <a href="{{route('index')}}" target="_blank">Login</a></li>
                <li>Select <a href="#">&lsquo;Developer&rsquo;</a></li>
                <li>Login using your account details. (Email address, Password)</li>
                <li>Start creating project by clicking <span class="green"> &lsquo; List your project &rsquo; </span></li>
            </ol>
            <p>Learn more about Gham Power at <a href="http://www.ghampower.com">www.ghampower.com</a>. If you have further queries, email us at <a href="mailto:ogbadmin@ghampower.com">ogbadmin@ghampower.com</a> or call us at 01-4721486, 01-4721450</p>
            <p>&copy; Gham Power Nepal Pvt. Ltd</p>
        </div>

        <div style="font-family: 'Mukta', sans-serif;">
            <p>नमस्कार!</p>
            <p>तपाइलाई <span class="eng">Off Grid Bazaar </span> मा स्वागत छ| यस परियोजना मार्फत अतिरिक्त आम्दानी गर्न को लागि आफ्नो गाउ ठाउँ मा सिचाईआवश्यक परेकाकिसानहरुको जानकारीलिई यस <span class="eng">platform </span> मा प्रोजेक्ट दर्ता गर्नु होस्| </p>
            <p>प्रोजेक्ट बनाउन तल दिएको निर्देशन पालना गर्नु होला </p>
            <ol>
                <li><a href="http://www.offgridbazaar.com">offgridbazaar.com</a>मा जानुहोस्</li>
                <li><span>&lsquo;Login&rsquo; </span >थिच्नुहोस्</li>
                <li><span>&lsquo;Developer&rsquo; </span>मा थिच्नुहोस्</li>
                <li>आफ्नो<span> &lsquo;Email address&rsquo; </span> र <span> &lsquo;Password&rsquo; </span>हाल्नुहोस्</li>
                <li><span> &lsquo;List your project&rsquo; </span> मा गएरप्रोजेक्टहरु बनाउन सुरु थाल्नुहोस्</li>
            </ol>
            <p>थप जानकारीको लागितलको फोननम्बरमा सम्पर्क गर्न सक्नु हुनेछ |</p>
            <p><strong>फोन</strong>:०१-४७२१४८६, ०१-४७२१४५० </p>
            <p><strong>इमेल:</strong><a href="mailto:ogbadmin@ghampower.com">ogbadmin@ghampower.com</a></p>
            <p>&copy; Gham Power Pvt. Ltd</p>
        </div>
    </div>
    @include('emails.includes.footer')
</div>
{{--User Activation  Email Template Developer End --}}

{{--User Activation  Email Template Investor start --}}
<div  align="center">
    <h5>3 .User Activation email Template:investor <br>
        Subject:Email Verification for your Off Grid Bazaar Account
    </h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
                <td><a href="{{route('my.investment')}}" style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                       target="_blank">My investments</a> | <a href="{{route('project.list')}}"
                                                               style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                                                               target="_blank">Project catalog</a></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Welcome to Off Grid bazaar !
        </h2>
        <div>
            <p class="email-title">Congratulations, you are ready to make an impact</p>
            <p class="email-text">
                Dear {full_name}
            </p>
            <p class="email-text">
                Welcome to Off Grid Bazaar. We are pleased to inform you that your investor account has been activated and you can now browse and find the projects to fund on the platform.
            </p>
            <p class="email-text">
                You can now start making an impact in four simple steps:
            </p>
            <ol>
                <li class="email-text">
                    Browse the projects available for funding on the Project Catalog.
                </li>
                <br/>
                <li class="email-text">
                    Review the Project Details and impact metrics of each of the project that you are interested in, and select the project(s) of your interest to fund.
                </li>
                <br/>
                <li class="email-text">
                    Confirm Payment and check-out through PayPal. You can also change your percentage of funding before confirming the payment.
                </li>
                <br/>
                <li class="email-text">
                    Track the implementation progress and operational data of the projects you have funded by going to My Investment page.
                </li>
            </ol>
            <a href="{{route('project.list')}}"> <button type="button" style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold"> View project catalog</button></a>
            <p class="email-text">
                You can <a href="http://ghampower.com/blog/">also read our Blogs</a> or <a href="https://www.youtube.com/watch?v=NsYGhDBDY2w&list=PLzPsJH7qwZYvOz2Pn1IwL2f5GxzIh2-Zr">watch our impact stories</a> to learn more about us and how we are creating opportunities to empower smallholding farmers and rural communities.
            </p>

            <p class="email-text">
                If you have any questions, please contact me directly.
            </p>
            <p class="email-text">
                Bishwaraj Bhattarai <br/>
                Impact Investment Manager <br/>
                Email: <a href="mailto:bishwaraj@ghampower.com">bishwaraj@ghampower.com</a>
            </p>
            <p class="email-text">
                Your partner in building a better planet, <br/>
                The Off Grid Bazaar Team
            </p>
        </div>
    </div>
    @include('emails.includes.footer')
</div>
{{--User Activation  Email Template Investor end --}}


{{--User Change Password Email--}}

<div  align="center">
    <h5> 4.Change Password Emails:all users
        <br>
        Subject: Password Changed!!
    </h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Password changed!
        </h2>
        <div>
            <p class="email-text">
                Dear {full_name}
            <p class="email-text">
                This email is sent to notify that your password is successfully changed.
            </p>

            <div style="text-align: center;margin: 30px">
                <a href="#"><button type="button" style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold"> Change Password</button></a>
            </div>

            <p class="email-text">
                If you have any questions, please contact me directly.
            </p>
            <p class="email-text">
                Your partner in building a better planet, <br/>
                The Off Grid Bazaar Team
            </p>
        </div>
    </div>

@include('emails.includes.footer')
</div>


<div  align="center">
    <h5>5 .Password reset:all user
        <br>
        Subject: Password Reset
    </h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Password reset
        </h2>
        <div>
            <p class="email-text">
                Dear {full_name}
            <p class="email-text">
                You are receiving this email because we received a password reset request for your account.
            </p>
            <div style="text-align: center;margin: 30px">
                <a href="#"><button type="button" style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold"> Reset Password</button></a>
            </div>

            <p class="email-text">
                Your partner in building a better planet, <br/>
                The Off Grid Bazaar Team
            </p>
        </div>
    </div>

    @include('emails.includes.footer')
</div>



<div  align="center">
    <h5>6.  Email For investing in  project:investor
        <br>
        Subject: Thank you, you are our hero!
    </h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
                <td><a href="{{route('my.investment')}}" style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                       target="_blank">My investments</a> | <a href="{{route('project.list')}}"
                                                               style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                                                               target="_blank">Project catalog</a></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Thank you for your generosity!
        </h2>
        <div>
            <p class="email-text">
                Dear {full_name}

            <p class="email-text">
                Thank you for funding {n} project{s} {$farmer_name}
                {{--@if($funded_project_count>1)--}}
                    {{--{{$funded_project_count}} {{str_plural('project', $funded_project_count)}}--}}
                {{--@elseif($funded_project_count == 1)--}}
                    {{--{{$farmer_name}}--}}
                {{--@endif--}}
                on Off Grid Bazaar.
                We have received the confirmation of your payment.
            </p>
            <p class="email-text">
                Heres what happens next:
            </p>
            <ol>
                <li class="email-text">
                    You can track the progress of the projects you funded in My Investment page.
                </li>
                <br/>
                <li class="email-text">
                    If a project is fully funded, we shall commence material dispatch and installation. Typically, it takes 4 to 6 weeks for us to install and commission a system after funding is complete.
                </li>
                <br/>
                <li class="email-text">
                    If the project is not yet fully funded, we are still looking for other generous supporters like yourself. You can help us raise funds by spreading the word. Please go to My Investmentand click Share.
                </li>
                <br/>
                <li class="email-text">
                    We shall send you regular updates of the progress through emails. Once the system is commissioned, you can view the operational data and impact metrics of each of the project in an interactive dashboard.
                </li>
            </ol>

            <a href="{{route('my.investment')}}"><button type="button" style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold"> View My Investments</button></a>

            <p class="email-text">
                Please remember to share the news of Off Grid Bazaar. Sharing is caring :)
            </p>

            <p class="email-text">
                Bishwaraj Bhattarai <br/>
                Impact Investment Manager <br/>
                Email: <a href="mailto:bishwaraj@ghampower.com">bishwaraj@ghampower.com</a>
            </p>
            <p class="email-text">
                Your partner in building a better planet, <br/>
                The Off Grid Bazaar Team
            </p>
        </div>
    </div>

    @include('emails.includes.footer')
</div>

<div  align="center">
<h5> 7. Contact developer email template:admins
    <br>
    Subject: About farmer {farmer-name}
</h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            Contact developer email
        </h2>
    </div>
    <div>

            <p class="email-text">
                {text-message from investor to admin like.....}
            </p>

    </div>

    @include('emails.includes.footer')
</div>

<div  align="center">
    <h5> 8. Email For approved Project :agent
        <br>
        Subject: {farmer-name} को प्रोजेक्ट बारे | About {farmer-name} Project | Gham Power
    </h5>
</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
                <td><a href="{{route('my.investment')}}" style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                       target="_blank">My investments</a> | <a href="{{route('project.list')}}"
                                                               style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                                                               target="_blank">Project catalog</a></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            {Needed title text for approve project}
        </h2>
        <div class="center-box">
            <p class="email-text">
                प्रिय प्रतिनिधि,<br>
                धेरै बधाई !!<br>
                तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनुभएको निम्न प्रोजेक्ट घाम पावरले अनुमोदन गरेको कुरा सहर्ष जानकारी गराउन चाहान्छौँ । घाम पावर को प्रतिनिधि भई सौर्य सिचाई प्रणाली प्रबर्धन तथा स्थापना गरि माशीक अतिरिक्त आम्दानी गर्ने यो प्रक्रिया को पहिलो खुड्किलो पार गर्नु भएकोमा बधाई । साथै अब अगाडी गर्नु पर्ने कार्यहरु पनि समयमै सम्पन्न  गर्नुहुन हार्दिक शुभकामना ।
                कृपया यो इमेल ध्यान दिएर पढ्नु होला । अब अगाडीका क्रियाकलाप, तपाईको कमिशन शुल्क ,लगायात जानकारी तल दियिएको छ, थप जानकारीका लागि यहाँ जानुहोस (तपाईले अफ-ग्रीड बजार प्लेटफर्ममा दर्ता गरेको इमेल लग-ईन गरेको हुन पर्नेछ )
                कृपया कुनै समस्या वा  प्रश्न भएमा तल दिईए अनुसार घाम पावरलाई सम्पर्क गर्नुहोस् । घाम  पावरसंग सहकार्य गर्नुभएकोमा तपाईलाई धन्यबाद । आगामी दिनमा अझै धेरै प्रोजेक्टमा सहर्कार्य गरिने अपेक्षा राख्दछौं ।
            </p>


                <p>Dear {$agent_full_name}<br>
                    Congratulations!!<br>
                    We are pleased to inform you that a project you created has been approved for next stage by Gham Power. Please read this email carefully and complete next steps to install the solar water pump.

                <ul>
                    <li> Name of Farmer: {farmer_name}}</li>
                    <li>System Name: {project_plan_name}</li>
                    <li>System Size:{project_solar_pv_size}}</li>
                    <li>Payment ID:{project_payment_id}</li>
                    <li>Monthly EMI: <Rs></Rs>.</li>
                    <li>Finance Term: 3 years / 36 months</li>
                </ul>

                Complete Your Next Steps to schedule Installation
                <ol>
                    <li>Notify the farmer that his/her application has been accepted and inform about the next steps of the installation process.</li>
                    <li>Collect the Advance/Booking Fee for the project and sign the Agreement Document with the farmer.
                        <a href="https://drive.google.com/drive/folders/1cKFgJQMe0lxOZVRYshea0UpwjWt0d0mh?usp=sharing">Find the Agreement document here</a>(you need to sign in using your registered email address).</li>
                    <li>Deposit the Advance Booking Fee via eSewa website/app. Go to eSewa/GhamPower and use the payment ID of the project to transfer the amount.</li>
                    <li>Scan and upload a copy of the Agreement Document.</li>
                </ol>
            <div>

                <p>
                    You’ll receive your commission, upto 4 %  of EMI-1 after completing these steps.
                    <a href="https://docs.google.com/document/d/1UyVrlp2xImKu20AWFpHbdr3TLxzq_-3WFOKsCd-Zvmc/edit?usp=sharing">Read more about Your Commission and the Project Installation Process</a>
                </p>
            </div>
    </div>
        <p class="email-text">
            If you have any questions, please contact:</br>
            Gham Power Nepal</br>
            Phone:  01-4721486 / 4721123 / 4721486</br>
            Website: GhamPower.com/Agents</br>
            Email: OgbAdmin@ghampower.com</br>

        </p>
        <p class="email-text">
            Your partner in building a better planet, <br/>
            The Off Grid Bazaar Team
        </p>
    </div>
    @include('emails.includes.footer')
</div>



{{--User verification  Email Template start--}}
<div  align="center">
    <h5>9 .Project Funded Email Template:investor <br>
        Subject:Your project is ready to take off at Off Grid Bazaar | Gham Power
    </h5>

</div>

<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        <table>
            <tr>
                <td style="width:66%;"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                            alt="OffgridBazaar" class="project-logo" style="width: 180px"></td>
                <td><a href="{{route('my.investment')}}" style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                       target="_blank">My investments</a> | <a href="{{route('project.list')}}"
                                                               style="color:#4472c4;font-family:Trebuchet MS;font-size:14px;"
                                                               target="_blank">Project catalog</a></td>
            </tr>
        </table>
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            A project you helped is taking off!!
        </h2>
        <p class="email-text">
            Dear {full_name}<br>
            We are pleased to inform you that one of the project you contributed in has now secured 100% funding and we
            are already starting preparations to schedule the installation.
            Here’s the sneak-peek of the project:
        </p>
        <div style="width: 245px;
                        font-family: arial;
                        border: 1px solid #ddd;">
            <label for="check__80" class="addcheck">
                <div class="farmer" style="text-align:center;">
                    <img src="http://demo.offgridbazaar.com/storage/upload/516011545735281_33566043_1802964523094211_324410871415570432_n.jpg" alt="" height="200" width="245">
                </div>
                <ul style="margin: -30px 5px -15px;
                            background-color: #fff;
                            padding: 10px 20px;
                            border-radius: 50px;
                            position: relative;
                            -webkit-box-sizing: border-box;
                            box-sizing: border-box;
                            display: -webkit-box;
                            display: -ms-flexbox;
                            display: flex;
                            z-index: 100;
                            -ms-flex-pack: distribute;
                            justify-content: space-around;
                            -webkit-box-shadow: 0 -5px 5px rgba(0,0,0,.2);
                            box-shadow: 0 -5px 5px rgba(0,0,0,.2);
                            list-style:none;">
                    <li style="margin:0;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_oil.png" alt="" title="" height="20" width="20"><br/><small>0.6 Kl</small></li>
                    <li style="margin:0;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_co2.png" alt="" title="" height="20" width="20"><br/><small>1.7 tons</small></li>
                    <li style="margin:0;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_meter_level4.png" alt="" title="" height="20" width="20"><br/><small>3/5</small></li>
                </ul>
                <div style="min-height: 240px;padding: 0 10px 10px;">
                    <div>
                        <p style="font-weight: bold;
                                color: #000;">Hari tamang</p>
                        <p style="color: #767676;"><small><i class="icon ion-md-pin text-primary"></i> Chitwan <i class="icon ion-md-flash ml-2 text-primary"></i> 1 Kw</small></p>
                        <p style="color: #9b9b9b;
                                font-size: 13px;">Help Hari tamang install solar water pump in Chitwan so that he  can grow crops to support his  family.</p>
                    </div>
                    <div>
                        <div>

                            <div style="height: 3px;
                                        background-color: grey;
                                        margin: 6px 0;">
                                <div style="height: 3px;
                                            background-color: #ffc107;
                                            width: 100%;"></div>
                            </div>

                            <div style="margin:20px 0;">
                                <div style="width: 32%;
                                        display: inline-block;">
                                    <small>Raised</small>
                                    <p style="font-weight: bold;
                                        color: #000; margin:4px 0;">$23232</p>
                                </div>
                                <p style=" color: #ffc107 !important;">Funding completed</p>
                            </div>

                        </div>
                        <a href="{{route('project.detail',['id' => 1,'new'])}}" data-target="#login" style="color: #fff;
                                        background-color: #17a2b8;
                                        border-color: #17a2b8;display: inline-block;
                                        padding: .25rem .5rem;
                                        font-size: .875rem;
                                        line-height: 1.5;
                                        text-decoration: none;
                                        font-weight: bold;
                                        border-radius: 5px;">View Details</a>
                        <a href="#" style="background-color: #fff;
                                border: 1px solid #ccc;
                                color: #888;display: inline-block;
                                padding: .25rem .5rem;
                                font-size: .875rem;
                                line-height: 1.5;
                                text-decoration: none;
                                font-weight: bold;
                                border-radius: 5px;"><i class="icon ion-md-share"></i> Share</a>
                    </div>
                </div>
            </label>
        </div>

        <p class="email-text">
            We wanted to share this great news with you as soon as we found out ourselves. We shall keep in touch with
            updates and more, as we install and commission the solar irrigation system for the farmer.
            Further, we have recently listed some new projects that you may find interesting.
        </p>
        <a href="{{route('project.list')}}">
            <button type="button" style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold"> View Project Catalog</button>
        </a>

        <p>
            Bishwaraj Bhattarai <br/>
            Impact Investment Manager <br/>
            Email: <a href="#" style="color:#4472c4;">bishwaraj@ghampower.com</a>
        </p>
        <p>
            Your partner in building a better planet, <br/>
            The Off Grid Bazaar Team
        </p>
    </div>
    @include('emails.includes.footer')
</div>

@endif

</body>

</html>
