@extends('layouts.default')
@php
    $month = App\components\MonthUtility::$_MONTHS;
@endphp
@section('title')
    Create Project
@endsection
@section('css')
    <style>
        #show_financial_institute{
            display: none;
        }
    </style>
@endsection
@section('content')

    <div class=" form-container-project form" >
        <div class="col-sm-12 form-heading">
            <h1>Create new project</h1>
            <h4>Fill in the information about the project below.</h4><br>
        </div>
        <form action="{{route('project.store')}}" method="post" class="form-horizontal" id="project-form">
            <input type="hidden" name="id" id = "project_id" value="{{$draftProject->id}}">
            <input type="hidden" name="_method" value="PUT">
            {{csrf_field()}}
            @include('includes.form')
            <div class="col-sm-12">
                <button class="btn btn-success" id="submit-analysis" name="submit-analysis" value="submit-analysis" type="submit" data-type="save">Submit for Analysis</button>
                @if(isset($project))
                    @if(strtolower($project->status) !='plan')
                        <button class="btn btn-default"  type="submit" name="submit" value="submit">Save Draft</button>
                    @endif
                @else
                    <button class="btn btn-default"  type="submit" name="submit" value="submit">Save Draft</button>
                @endif
                <a class="btn btn-default pull-right" href="{{url('/project/list')}}">Cancel</a>
            </div>
        </form>
    </div>
@endsection
@section('js')
    @include('includes.project_form_script')
    @include('includes.project_form_validation')
@endsection

