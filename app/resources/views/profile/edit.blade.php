@extends('layouts.new-default')

@section('title') Invest in Offgrid Solar Projects @endsection
@section('css')

@endsection
@section('content')
    @include('common.session_message')
    <div class="form-container">
        <div class="col-md-4 profile-form">
            {!! Form::model($user, ['route' => ['profile.update'], 'class' => 'form-horizontal', 'enctype' => "multipart/form-data"]) !!}
            <image-component></image-component>

            </hr><br>
            <div class="col-md-12">
                Email : {{$user->email}}
            </div>
            <div class="col-md-12 mt-4 form-group">
                {!! Form::label('full_name', 'Your Full name:', ['class' => 'inline-label required' ]) !!}
                {!! Form::text('full_name', null, ['class' => 'form-control','placeholder' => 'Enter your full_name ','required' =>'required']) !!}
                @if ($errors->has('full_name'))
                    <span class="error">{{ $errors->first('full_name') }}</span>
                @endif
            </div>
            <div class="row col-md-12">
                <div class="col-md-4 input-group" style="padding-right:0px;">
                    {!! Form::label('country_code', 'Country code:', []) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">+</span>
                    </div>
                    {!! Form::number('country_code', isset($user->country_code)? $user->country_code:null, ['class' => 'form-control','placeholder' => '']) !!}
                </div>
                <div class="col-md-8">
                    {!! Form::label('phone', 'Phone number:', ['class' => 'inline-label required']) !!}
                    {!! Form::text('phone', null, ['class' => 'form-control','placeholder' => 'Enter your phone number']) !!}
                </div>

                @if ($errors->has('phone'))
                    <span class="error">
                        {{ $errors->first('phone') }}</span>
                @endif
            </div>
            <br>
            <div class="col-md-12 form-group">
                {!! Form::label('address', 'Address:', ['class' => 'inline-label required' ]) !!}
                {!! Form::text('address', null, ['class' => 'form-control','placeholder' => 'Enter your address','required' =>'required']) !!}
                @if ($errors->has('address'))
                    <span class="error">{{ $errors->first('address') }}</span>
                @endif
            </div>
            {{--Checkbox status input is in the reverse case condition. So, not anonymous is considered checked--}}
            <div class="col-md-12 mt-4">
                {{ Form::checkbox('anonymous',null,$user->anonymous == \App\OGB\Constants::NOT_ANONYMOUS)}}
                <small class="text-muted">Use my name and profile photo for marketing the projects I have invested in.</small>
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-info" id="submit" name="submit" type="submit" data-type="save">Update Info
                </button>
                <a href="{{route('changePassword.form')}}" class="btn btn-secondary" name="change-password">Change Password</a>
            </div>
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div>
@endsection
@section('js')
    <script>
        var user = {!! json_encode($user) !!}
    </script>
@endsection

