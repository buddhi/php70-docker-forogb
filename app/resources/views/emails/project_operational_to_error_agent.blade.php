@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        प्रिय प्रतिनिधि,<br>
        तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्टमा केही प्राविधिक समस्या आएको हुन सक्ने खबर हामीलाई प्राप्त भयो । कृपया यो ईमेल ध्यान दिएर पढी निम्न प्रकृया पुरा गर्नु होला ।
        कृषक को सिचाई प्रणाली मा समस्या आउनुले मासिक किस्ता उठाउन जोखिम  पैदा गर्ने छ , र यसले तपाई तथा घाम - पावर, दुबैलाई नोक्सान गर्न सक्ने छ ।
        समझदारी का लागी धन्यबा

    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$agent->full_name}} <br><br>

        One of the project you created is suffering technical problem. Please review the details of the system and take necessary actions mentioned below.
    </p>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name: {{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Next Deadline: <b>{{$next_due_date}}</b></li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Finance term : 3 years/36 months</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Farmer Phone Number: {{$farmer->contact_no}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Farmer Address: {{$farmer->address}}</li>
    </ul>
    <b  style="font-family:Trebuchet MS;font-size: 18px; color: #959292">Next Steps:</b><br>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Contact the farmer URGENTLY, collect information on issues and the problem.</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Contact Gham Power and notify of the situation</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Visit the site, take photo/videos of the system and the problem</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Report the photo/video to Gham Power and wait for further instructions</li>
    </ul>
<p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
    Thank you for being part of this project. We look forward to doing more projects with you.
</p>
@endsection


