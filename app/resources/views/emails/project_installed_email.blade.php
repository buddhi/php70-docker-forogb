@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        प्रिय प्रतिनिधि,<br>
        धेरै बधाई !!<br>
        तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्ट जडान भई संचालन सुरु भएको कुरा सहर्ष जानकारी गराउन चाहान्छौँ । अब प्रत्येक महिनाको किस्ता रकम सम्बन्धित किसानबाट समयमैऊठाई आफ्नो कमिसन समयमै  पाउनुहोला  ।
    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$agent->full_name}} <br>
        Congratulations!! <br>

        We are pleased to inform you that a project you created has been installed and is ready for operation. Please collect the monthly payment EMI on time to receive your monthly comission.
    </p>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of
            Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name: {{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Next Deadline: {{$next_due_date}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Finance Term: 3 years / 36 months</li>
    </ul>
@endsection


