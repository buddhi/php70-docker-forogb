@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            प्रिय प्रतिनिधि,<br>
            धेरै बधाई !!<br>
            तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनुभएको निम्न प्रोजेक्ट घाम पावरले अनुमोदन गरेको कुरा सहर्ष जानकारी
            गराउन
            चाहान्छौँ । घाम पावर को प्रतिनिधि भई सौर्य सिचाई प्रणाली प्रबर्धन तथा स्थापना गरि माशीक अतिरिक्त आम्दानी
            गर्ने
            यो प्रक्रिया को पहिलो खुड्किलो पार गर्नु भएकोमा बधाई । साथै अब अगाडी गर्नु पर्ने कार्यहरु पनि समयमै सम्पन्न
            गर्नुहुन हार्दिक शुभकामना ।
            कृपया यो इमेल ध्यान दिएर पढ्नु होला । अब अगाडीका क्रियाकलाप, तपाईको कमिशन शुल्क ,लगायात जानकारी तल दियिएको
            छ, थप
            जानकारीका लागि यहाँ जानुहोस (तपाईले अफ-ग्रीड बजार प्लेटफर्ममा दर्ता गरेको इमेल लग-ईन गरेको हुन पर्नेछ )
            कृपया कुनै समस्या वा प्रश्न भएमा तल दिईए अनुसार घाम पावरलाई सम्पर्क गर्नुहोस् । घाम पावरसंग सहकार्य
            गर्नुभएकोमा
            तपाईलाई धन्यबाद । आगामी दिनमा अझै धेरै प्रोजेक्टमा सहर्कार्य गरिने अपेक्षा राख्दछौं ।
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Dear {{$agent->full_name}}<br>
            Congratulations!!<br>
            We are pleased to inform you that a project you created has been approved for next stage by Gham Power.
            Please
            read this email carefully and complete next steps to install the solar water pump.</p>
        <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <li> Name of Farmer: {{$project->farmer_name}}</li>
            <li>System Name: {{$project->name}}</li>
            <li>System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
            <li>Payment ID:{{$project->payment_id}}</li>
            <li>Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
            <li>Finance Term: 3 years / 36 months</li>
        </ul>

        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Complete Your Next Steps to schedule
            Installation:</p>
        <ol style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <li>Notify the farmer that his/her application has been accepted and inform about the next steps of the
                installation process.
            </li>
            <li>Collect the Advance/Booking Fee for the project and sign the Agreement Document with the farmer.
                <a href="https://drive.google.com/drive/folders/1cKFgJQMe0lxOZVRYshea0UpwjWt0d0mh?usp=sharing" target="_blank">Find the
                    Agreement document here</a>(you need to sign in using your registered email address).
            </li>
            <li>Deposit the Advance Booking Fee via eSewa website/app. Go to eSewa/GhamPower and use the payment ID of
                the
                project to transfer the amount.
            </li>
            <li>Scan and upload a copy of the Agreement Document.</li>
        </ol>

        <p>
            You’ll receive your commission, upto 4 % of EMI-1 after completing these steps.
            <a href="https://docs.google.com/document/d/1UyVrlp2xImKu20AWFpHbdr3TLxzq_-3WFOKsCd-Zvmc/edit?usp=sharing">Read
                more about Your Commission and the Project Installation Process</a>
        </p>
    </div>
@endsection

