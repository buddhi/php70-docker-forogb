@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$investor->full_name}}<br>
        We are pleased to inform you that one of the project you contributed in has now secured 100% funding and we
        are already starting preparations to schedule the installation.
        Here’s the sneak-peek of the project:
    </p>
    <div style="width: 245px;
                        font-family: arial;
                        border: 1px solid #ddd;position:relative;">
        <label for="check__80" class="addcheck">
            <div class="farmer" style="text-align:center;">
                <img src="http://demo.offgridbazaar.com/storage/upload/516011545735281_33566043_1802964523094211_324410871415570432_n.jpg"
                     alt="" height="200" width="245">
            </div>
            <ul style="margin: -30px 5px -15px;
                            background-color: #fff;
                            padding: 10px 20px;
                            border-radius: 50px;
                            position: absolute;
                            -webkit-box-sizing: border-box;
                            box-sizing: border-box;
                            display: flex;
                            z-index: 100;
                            -ms-flex-pack: distribute;
                            justify-content: space-around;
                            -webkit-box-shadow: 0 -5px 5px rgba(0,0,0,.2);
                            box-shadow: 0 -5px 5px rgba(0,0,0,.2);
                            list-style:none;">
                <li style="margin:0 0 0 10px;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_oil.png" alt=""
                                                     title="" height="20" width="20"><br/>
                    <small>{{round(calculateEnvParams($project->solar_pv_size)['diesel_displaced'],1)}} Kl</small>
                </li>
                <li style="margin:0 50px;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_co2.png" alt=""
                                                     title="" height="20" width="20"><br/>
                    <small>{{ round(calculateEnvParams($project->solar_pv_size)['co2_curbed'],1) }} tons</small>
                </li>
                <li style="margin:0 0 0 7px;padding:0;"><img src="http://demo.offgridbazaar.com/img/icon_meter_level4.png"
                                                     alt="" title="" height="20" width="20"><br/>
                    <small>{{round(creditScoreToBankability($farmer->credit_score),1)}}/5</small>
                </li>
            </ul>
            <div style="min-height: 240px;padding: 0 10px 10px;">
                <div>
                    <p style="font-weight: bold;
                                color: #000;">{{$project->farmer_name}}</p>
                    <p style="color: #767676;">
                        <small><i class="icon ion-md-pin text-primary"></i> {{$district}} <i
                                    class="icon ion-md-flash ml-2 text-primary"></i> 1 Kw
                        </small>
                    </p>
                </div>
                <div>
                    <div>
                        <div style="height: 3px;
                                        background-color: grey;
                                        margin: 6px 0;">
                            <div style="height: 3px;
                                            background-color: #ffc107;
                                            width: 100%;"></div>
                        </div>

                        <div style="margin:20px 0;">
                            <div style="width: 32%;
                                        display: inline-block;">
                                <small>Raised</small>
                                <p style="font-weight: bold;
                                        color: #000; margin:4px 0;">${{number_format($project_cost)}}</p>
                            </div>
                            <p style=" color: #ffc107 !important;">Funding completed</p>
                        </div>

                    </div>
                    <a href="{{route('project.detail',['id' => $project->id,'new'])}}" target="_blank" data-target="#login" style="color: #fff;
                                        background-color: #17a2b8;
                                        border-color: #17a2b8;display: inline-block;
                                        padding: .25rem .5rem;
                                        font-size: .875rem;
                                        line-height: 1.5;
                                        text-decoration: none;
                                        font-weight: bold;
                                        border-radius: 5px;">View Details</a>
                </div>
            </div>
        </label>
    </div>
@endSection



