@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Dear user,
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            You are receiving this email because we received a password reset request for your account.
        </p>
        <div style="text-align: center;margin: 30px">
            <a href="{{$reset_password_url}}">
                <button type="button"
                        style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold">
                    Reset Password
                </button>
            </a>
        </div>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292;text-decoration: none!important;">
            If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below
            into your web browser: {{$reset_password_url}}
        </p>
    </div>
@endsection