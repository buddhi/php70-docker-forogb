@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        प्रिय प्रतिनिधि,<br>
        धेरै बधाई !!<br>
        तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्टको ३ - वर्षे किस्ता/लगानी गर्न आवश्यक सम्पूर्ण
        प्रकृया पुरा भएको कुरा सहर्ष जानकारी गराउन चाहान्छौँ । अब पहिलो महिना वापतको किस्ता रकम सम्बन्धित किसानबाट
        ऊठाई सौर्य सिचाई प्रणाली जडान गर्ने मिति टुंगो लगाउन घाम पावरमा सम्पर्क गर्नु होला ।
        कृपया यो इमेल ध्यान दिएर पढ्नु होला । अब अगाडीका क्रियाकलाप लगायात जानकारी तल दिईएको छ, थप जानकारीका लागि
        यहाँ जानुहोस (तपाईले अफ-ग्रीड बजार प्लेटफर्ममा दर्ता गरेको इमेल लग-ईन गरेको हुन पर्नेछ )
        कृपया कुनै समस्या वा प्रश्न भएमा तल दिईए अनुसार घाम पावरलाई सम्पर्क गर्नुहोस् । घाम पावरसंग सहकार्य
        गर्नुभएकोमा तपाईलाई धन्यबाद । आगामी दिनमा अझै धेरै प्रोजेक्टमा सहर्कार्य गरिने अपेक्षा राख्दछौं ।
    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$agent->full_name}} <br>
        Congratulations!! <br>

        We are pleased to inform you that a project you created has been fully funded and is ready for
        installation. Please read this email carefully and complete next steps to install the solar water pump.
        Project Details
    </p>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of
            Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name: {{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Finance Term: 3 years / 36 months</li>
    </ul>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
    Complete Your Next Steps to schedule Installation
    </p>
    <ol style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Notify the farmer that his/her
            project has been fully funded / ready for financing and inform about the next steps of the
            installation process.
        </li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Collect the First month EMI for the
            project. EMI collection is necessary before scheduling installation. Pay EMI through eSewa
            app/website.
        </li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Communicate with Gham Power as well
            as the farmer to schedule installation.
        </li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Be present at the installation
            site, sign the Installation / Commissioning Report and upload photos/videos from the site.
        </li>
        You’ll receive your commission, upto 4 % of EMI-1 after completing these steps.
    </ol>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        You’ll receive your commission, upto 4 % of EMI-1 after completing these steps.
        <a href="https://docs.google.com/document/d/1UyVrlp2xImKu20AWFpHbdr3TLxzq_-3WFOKsCd-Zvmc/edit">Read more
            about Your Commission and the Project Installation Process</a>
    </p>

@endsection


