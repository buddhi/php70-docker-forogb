@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        प्रिय प्रतिनिधि,<br>
        तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्ट जडान भई संचालन सुरु भएकोमा सम्बन्धित कृषकले मासिक किस्ता समयमा नतिरेकोले ध्यानाकर्षण गराउन चाहान्छौँ । सम्बन्धित कृषकले आफ्नो किस्ता समयमा नतिरे तपाईले पाउने मासिक कमिसन त रोकिन्छ नै , साथै तपाईले ल्याउनु भएको धेरै कृषकले यसै गरि बक्यौता बाकी राखे भविस्य मा तपाईले पाउने कमिसन समेत घट्न सक्ने अथवा तपाई को प्रोजेक्टहरु स्वीकृत नहुन सक्छ । तसर्थ सम्बन्धित किसानलाई सम्पर्क गरि किस्ता ऊठाउने प्रयत्न गर्नु होला । धन्यबाद ।
    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$agent->full_name}} <br><br>

        Please note that following farmer is not paying his monthly EMI on time and thus is at risk of being cut-off from the system. Please contact the farmer to recover the EMI on time to get your commission on time.
    </p>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name: {{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Next Deadline: {{$next_due_date}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Days since overdue : {{$no_of_days_since_overdue}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">No of EMI overdue  : {{$no_of_emi_overdue}}</li>

    </ul>
@endsection


