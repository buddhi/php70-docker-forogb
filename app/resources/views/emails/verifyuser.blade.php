@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p>
            Dear {{$user->full_name}}
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Thank you for registering with Off Grid Bazaar platform. We are excited to have your support in funding high
            impact projects in Nepal.
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <a href="{{route('user.verify',$user->verifyusers->token)}}" class="verify-link">Please click here to verify
                your email address</a>
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            If the link above does not work, please copy and paste this URL into your browser address bar:
        </p>
        <a href="{{route('user.verify',$user->verifyusers->token)}}">{{route('user.verify',$user->verifyusers->token)}}</a>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Your Impact Investor Account will be up and running once you verify your email. You will then be able to
            review all investment-ready projects on Off Grid Bazaar website, and select the ones that best meet your
            criteria.
        </p>
    </div>
@endsection