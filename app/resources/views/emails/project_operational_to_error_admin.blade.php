@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear Admin
    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        One of the project at Off Grid Bazaar is suffering technical problem. Please review the details of the system and take necessary actions.
    </p>
    <b>Project Details</b>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name:{{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Next Deadline: <b>{{$next_due_date}}</b></li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Finance term: 3 years/36 months</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Farmer Phone Number: {{$farmer->contact_no}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Meter Phone Number: {{$meter_phone_number}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Farmer address: {{$farmer->address}}</li>
    </ul>
@endsection


