@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            प्रिय प्रतिनिधि,<br>
            तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्टको अग्रिम पेश्की / बैना रकम हामीलाई प्राप्त
            भएकोमा धन्यबाद ।<br>
            अब यस प्रोजेक्ट को कर्जा सम्बन्धि प्रक्रिया अघि बढ्ने छ । यस प्रोजेक्टका किसानको कर्जा निवेदन घाम पावर तथा
            घाम पावरका साझेदार संस्थाले स्वीकृत गर्न साथ हामी सो को जानकारी गराउने छौ । यसका लागी औसत ३ देखि ५ हप्ता
            लाग्ने छ ।<br>
            धन्यवाद | घाम पावर<br>
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
            =====================================================================<br>
            Dear {{$agent->full_name}} <br>
            We are pleased to inform you that we have received the payment for Booking Fee/Advance for the following
            project you created in the Off Grid Bazaar platform.<br>
            Please note that we shall now process the loan application for the concerned farmer and you will be notified
            if/when the loan application is approved by Gham Power and Partners. This will generally take 3 - 5 weeks.
            Thank you.<br>
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
            Payment Details:<br>
            Name of Farmer: {{$farmer->farmer_name}}<br>
            System Name: {{$project->name}}<br>
            System Size: {{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}<br>
            Payment ID:{{$project->payment_id}} <br>
            Booking Fee Paid:{{displayUnitFormat('amount',$payment_detail->amount) }} <br>
            Thank you for being part of this project. We look forward to doing more projects with you<br>
        </p>
    </div>
@endsection