@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div class="center-box">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Dear {{$recipient->full_name}}
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Welcome to Off Grid Bazaar. We are pleased to inform you that your investor account has been activated and
            you can now browse and find the projects to fund on the platform.
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            You can now start making an impact in four simple steps:
        </p>
        <ol style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                Browse the projects available for funding on the Project Catalog.
            </li>
            <br/>
            <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                Review the Project Details and impact metrics of each of the project that you are interested in, and
                select the project(s) of your interest to fund.
            </li>
            <br/>
            <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                Confirm Payment and check-out through PayPal. You can also change your percentage of funding before
                confirming the payment.
            </li>
            <br/>
            <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                Track the implementation progress and operational data of the projects you have funded by going to My
                Investment page.
            </li>
        </ol>
        <a href="{{route('project.list')}}">
            <button type="button"
                    style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold">
                View project catalog
            </button>
        </a>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            You can <a href="http://ghampower.com/blog/">also read our Blogs</a> or <a
                    href="https://www.youtube.com/watch?v=NsYGhDBDY2w&list=PLzPsJH7qwZYvOz2Pn1IwL2f5GxzIh2-Zr">watch our
                impact stories</a> to learn more about us and how we are creating opportunities to empower smallholding
            farmers and rural communities.
        </p>
    </div>
@endsection