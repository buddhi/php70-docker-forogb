<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
{{--@include('emails.includes.template-body')--}}
<div class="template-design" style="width:685px;margin:auto;border: 1px solid #7e7e7e;font-family:Trebuchet MS;">
    <div class="template-body" style="padding:20px;color: #959292;line-height: 24px;">
        @include('emails.includes.header')
        <h2 class="template-title" style="color: #b18767;font-size: 28px;font-weight:normal;">
            @if(isset($title))
                {{$title}}
            @else
                Welcome to Off Grid bazaar!
            @endif
        </h2>
        @yield('main-content')
        @if($for_investor)
            <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                If you have any questions, please contact me directly.<br>
                Bishwaraj Bhattarai <br>
                Impact Investment Manager <br>
                Email: <a href="#" style="color:#4472c4;">bishwaraj@ghampower.com</a>
            </p>
        @else
            @if(!isset($for_admin))
                <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
                    If you have any questions, please contact:<br>
                    Gham Power Nepal<br>
                    Phone:  01-4721486 / 4721123 / 4721486<br>
                    Website: GhamPower.com/Agents<br>
                    Email: OgbAdmin@ghampower.com<br>
                </p>
            @endif
        @endif
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Your partner in building a better planet, <br/>
            The Off Grid Bazaar Team
        </p>
    </div>
    @include('emails.includes.footer')
</div>

</body>
</html>