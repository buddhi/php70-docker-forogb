@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        प्रिय प्रतिनिधि,<br>
        तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्टमा मासिक किस्ता भुक्तानी को समस्या आएकोमा उक्त समस्या हल भैसकेको सहर्ष जानकारी गराउन चाहन्छौं । फेरी केही समस्या आए सम्पर्क गर्नु होला ।
        यो समस्या हल गर्न तपाईले देखाउनु भएको तदारुकता का लागी धन्यबाद ।
    </p>
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$agent->full_name}} <br><br>
        We are glad to inform you that one the project you created that was overdue with delayed payments, which is now paid and operational. Please contact us if there is any other issue.
        Thank you for your support in solving the problem.

    </p>
    <ul style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292"> Name of Farmer: {{$project->farmer_name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Name: {{$project->name}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">System Size:{{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Payment ID:{{$project->payment_id}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Monthly EMI:{{displayUnitFormat('amount',$project->emi_amount)}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Next Deadline: <b>{{$next_due_date}}</b></li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Finance term : 3 years/36 months</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Farmer Phone Number: {{$farmer->contact_no}}</li>
        <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292">FarmerAddress: {{$farmer->address}}</li>
    </ul>
<p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
    Thank you for being part of this project. We look forward to doing more projects with you.
</p>
@endsection


