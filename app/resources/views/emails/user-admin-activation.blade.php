@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Dear {{$recipient->full_name}},</p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Welcome to Off Grid Bazaar Developer team
            where you can create additional source of income by identifying local farmers requiring irrigation and
            helping us connect to them.</p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Please follow the following steps to create
            projects</p>
        <ol style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <li>Visit <a href="http://www.offgridbazaar.com">offgridbazaar.com </a>and select <a href="#">Login</a></li>
            <li>Select <a href="#">&lsquo;Developer&rsquo;</a></li>
            <li>Login using your account details. (Email address, Password)</li>
            <li>Start creating project by clicking <span class="green"> &lsquo; List your project &rsquo; </span></li>
        </ol>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">Learn more about Gham Power at <a
                    href="http://www.ghampower.com">www.ghampower.com</a>. If you have further queries, email us at <a
                    href="mailto:ogbadmin@ghampower.com">ogbadmin@ghampower.com</a> or call us at 01-4721486, 01-4721450
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">&copy; Gham Power Nepal Pvt. Ltd</p>
    </div>

    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">नमस्कार!</p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">तपाइलाई <span
                    class="eng">Off Grid Bazaar </span> मा स्वागत छ| यस परियोजना मार्फत अतिरिक्त आम्दानी गर्न को लागि
            आफ्नो गाउ ठाउँ मा सिचाईआवश्यक परेकाकिसानहरुको जानकारीलिई यस <span class="eng">platform </span> मा प्रोजेक्ट
            दर्ता गर्नु होस्| </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">प्रोजेक्ट बनाउन तल दिएको निर्देशन पालना
            गर्नु होला </p>
        <ol style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <li><a href="http://www.offgridbazaar.com">offgridbazaar.com</a>मा जानुहोस्</li>
            <li><span>&lsquo;Login&rsquo; </span>थिच्नुहोस्</li>
            <li><span>&lsquo;Developer&rsquo; </span>मा थिच्नुहोस्</li>
            <li>आफ्नो<span> &lsquo;Email address&rsquo; </span> र <span> &lsquo;Password&rsquo; </span>हाल्नुहोस्</li>
            <li><span> &lsquo;List your project&rsquo; </span> मा गएरप्रोजेक्टहरु बनाउन सुरु थाल्नुहोस्</li>
        </ol>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">थप जानकारीको लागितलको फोननम्बरमा सम्पर्क
            गर्न सक्नु हुनेछ |</p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292"><strong>फोन</strong>:०१-४७२१४८६, ०१-४७२१४५०
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292"><strong>इमेल:</strong><a
                    href="mailto:ogbadmin@ghampower.com">ogbadmin@ghampower.com</a></p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">&copy; Gham Power Pvt. Ltd</p>
    </div>
@endsection





