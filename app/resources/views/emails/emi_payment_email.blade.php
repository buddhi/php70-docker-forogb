@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            प्रिय प्रतिनिधि, <br>


            तपाईले अफ-ग्रीड बजार प्लेटफर्ममा बनाउनु भएको निम्न प्रोजेक्टको मासिक किस्ता रकम हामीलाई प्राप्त भएको छ ।
            आगामी महिना हरु मा पनि आफ्नो किसानलाई समयमै मासिक किस्ता बुझाउन प्रेरित गरि किसान को पम्प बन्द हुने तथा
            तपाईको कमिसन रोकिने सम्भावनाबाट मुक्त हुनु होला।

            यस प्रोजेक्टको आगामी किस्ता {{$payment_detail->nextDueDate()}} भित्र तिरी सक्न पर्नेछ |<br>

            धन्यवाद | घाम पावर <br>

            =====================================================================
            <br>

        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">

            Dear {{$agent->full_name}},<br>

            We are pleased to inform you that we have received the monthly EMI payment for following project you created
            in the Off Grid Bazaar platform.
            Please encourage your farmer for regular and timely payments in the future to avoid interruptions in pump
            operation and to get your commission on time. Thank you.
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292 !important;">
            Payment Details:<br>
            Name of Farmer: {{$farmer->farmer_name}}<br>
            System Name: {{$project->name}} <br>
            System Size: {{$project->solar_pv_size}} {{str_plural('Watt', $project->solar_pv_size)}}<br>
            Payment ID: {{$project->payment_id}}<br>
            Monthly EMI Paid:{{displayUnitFormat('amount',$payment_detail->amount)}}<br>
            Month Paid / EMI number:{{$payment_detail->paidMonthInBsFormat()}} / {{$payment_detail->emiNo()}} of 36<br>
            Next Due Date: {{$next_due_date}}<br>
            Thank you for being part of this project. We look forward to doing more projects with you<br>
        </p>

    </div>
@endsection