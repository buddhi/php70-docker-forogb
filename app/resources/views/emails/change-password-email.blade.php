@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        Dear {{$user->full_name}}
    <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        {{$confirmation_message}}
    </p>
@endsection
