@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            महोदय,<br>

            तपाईलाई घाम पावर को अफ् ग्रिड बजार प्लेटफर्ममा {{$partner->name}} को एक प्रयोगकर्ता  बन्न आमन्त्रण गरिएकोले तल दियिएको लिंक मा गई आफ्नो इमेल रुजु गर्नुहुन अनुरोध छ ।
                <br>धन्यवाद | घाम पावर

        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            Dear user,<br>
            You have been invited to the <b>Off Grid Bazaar</b> platform to register as a User of <b>{{$partner->name}}.</b> Please
            Click on the following link to verify your email address and sign up to the platform.
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            <a href="{{route('partner-user.verify',$token)}}" class="verify-link">Please
                click here to verify
                your email address</a>
        </p>
        <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292">
            If the link above does not work, please copy and paste this URL into your browser address bar:
        </p>
        <a href="{{route('partner-user.verify',$token)}}">{{route('partner-user.verify',$token)}}</a>
    </div>
@endsection