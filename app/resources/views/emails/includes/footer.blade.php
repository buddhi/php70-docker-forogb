<div class="template-footer">
    <table style="background-color:#b2b2b2;color:#fff; padding:20px;font-size:14px;display:block;">
        <tfoot>
        </tfoot>
        <tbody>
        <tr>
            <td colspan="1" style="font-size: 14px;margin: 16px 0;font-weight:bold;line-height:18px;">About Off Grid
                Bazaar
            </td>
            <td colspan="5" style="font-size: 13px;
			               "><i>Off Grid Bazaar is an initiative of</i></td>
        </tr>
        <tr>
            <td rowspan="7" style="width: 266px;
			               text-align: justify;
			               line-height: 20px;
			               padding: 15px 50px 0 0;">Off Grid Bazaar (OGB) is an online project development platform
                developed by Gham Power Nepal to scale
                up the implementation of off-grid solar based projects to serve smallholding farmers and rural
                communities in Nepal. The platform provides an eco-system for all the stakeholders to interact: from
                villagers and farmers to the EPC companies, banks and investors to the policy makers. OGB is the
                first
                step towards designing holistic solution for the complex “Energy Access” problem.
            </td>
            <td colspan="5" style="padding-top:9px;">
                <a href="http://ghampower.com/">
                <img src="https://offgridbazaar.com/img/ghampower-logo.png"
                                                           height="60px" style="display:block;" alt="off grid Bazaar"/>
                </a>
            </td>
        </tr>
        <tr>

        </tr>
        <tr>
            <td colspan="5" style="font-size:14px;">Gham Power Nepal Private Limited</td>
        </tr>

        <tr>
            <td style="width:24px;">
                <img src="https://offgridbazaar.com/img/location-icon.png" style="width:18px;height:18px;text-align: center;
			                  display: inline-block;">
            </td>
            <td colspan="4" style="font-size:14px;padding-top: 6px;"><span>Shah Villa – 292, Chundevi Marga, Maharajgunj<br/>Kathmandu 44600, Bagmati, Nepal</span>
            </td>
        </tr>
        <tr>
            <td style="width:24px;">
                <img src="https://offgridbazaar.com/img/call-icon.png" style="width:14px;height:18px;text-align: center;
			                  display: inline-block;">
            </td>
            <td colspan="4" style="font-size:14px;"><span>00977-1-4721486, 4721450</span>
            </td>
        </tr>
        <tr>
            <td style="width:24px;">
                <img src="https://offgridbazaar.com/img/web-icon.png" style="width:18px;height:18px;text-align: center;
			                  display: inline-block;">
            </td>
            <td colspan="4" style="font-size:14px;"><span><a href="http://ghampower.com/" style="color:#fff;"
                                                             target="_blank">http://ghampower.com/</a></span></td>
        </tr>
        <tr style="padding-top:15px;">
            <td></td>
            <td>
                <a href="https://www.facebook.com/ghampower/" target="_blank">
                    <img src="http://demo.offgridbazaar.com/img/facebook_grey_footer.png" style="width:24px;height:24px;text-align: center;display: inline-block;"></a>
                &nbsp;&nbsp;

                <a href=" https://twitter.com/ghampower" target="_blank">
                    <img src="http://demo.offgridbazaar.com/img/twitter_grey_footer.png" style="width:24px;height:24px;text-align: center;display: inline-block;"></a>
                &nbsp;&nbsp;
                <a href="https://www.linkedin.com/company/gham-power" target="_blank">
                    <img src="http://demo.offgridbazaar.com/img/linkedin_grey_footer.png" style="width:24px;height:24px;text-align: center;display: inline-block;"></a>
                &nbsp;&nbsp;
                <a href="https://www.youtube.com/user/ghampower" target="_blank"><img src="http://demo.offgridbazaar.com/img/youtube_grey_footer.png" style="width:26px;height:24px;text-align: center;display: inline-block;"></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>