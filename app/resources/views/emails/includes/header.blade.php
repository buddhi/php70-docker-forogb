<table>
    <tr>
        <td style="width:66%;">
            <a href="{{route('index')}}"><img src="https://www.offgridbazaar.com/offgrid-bazaar/images/logo.png"
                                              alt="OffgridBazaar" class="project-logo" style="width: 200px"></a>
        </td>
        @if($for_investor)
            <td><a href="{{route('my.investment')}}"
                   style="color:#959292;font-family:Trebuchet MS;font-size:14px;text-decoration: none"
                   target="_blank">My Investments</a> | <a href="{{route('project.list')}}"
                                                           style="color:#959292;font-family:Trebuchet MS;font-size:14px;text-decoration: none"
                                                           target="_blank">Project Catalog</a></td>
        @endif
    </tr>
</table>