@extends('emails.default',['title' => $title, 'for_investor' => $for_investor])
@section('main-content')
    <div class="mail-template">
        <div class="center-box">
            <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                Dear {{$user->full_name}}
            </p>
            <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                Thank you for funding
                @if($funded_project_count>1)
                    {{$funded_project_count}} {{str_plural('project', $funded_project_count)}}
                @elseif($funded_project_count == 1)
                    {{$farmer_name}}
                @endif
                on Off Grid Bazaar.
                We have received the confirmation of your payment.
            </p>
            <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                Heres what happens next:
            </p>
            <ol>
                <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                    You can track the progress of the projects you funded in My Investment page.
                </li>
                <br/>
                <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                    If a project is fully funded, we shall commence material dispatch and installation. Typically, it
                    takes 4 to 6 weeks for us to install and commission a system after funding is complete.
                </li>
                <br/>
                <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                    If the project is not yet fully funded, we are still looking for other generous supporters like
                    yourself. You can help us raise funds by spreading the word. Please go to My Investmentand click
                    Share.
                </li>
                <br/>
                <li style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                    We shall send you regular updates of the progress through emails. Once the system is commissioned,
                    you can view the operational data and impact metrics of each of the project in an interactive
                    dashboard.
                </li>
            </ol>

            <a href="{{route('my.investment')}}" target="_blank">
                <button type="button"
                        style="color: #747474;background-color: #f8d260; height: 40px;font-size: 16px;font-weight: bold">
                    View My Investments
                </button>
            </a>
            <p style="font-family:Trebuchet MS;font-size: 16px; color: #959292!important;">
                Please remember to share the news of Off Grid Bazaar. Sharing is caring :)
            </p>
        </div>
    </div>
@endsection



