<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">
    <title>@yield('title')</title>
    {{--TODO::generate separate css file using node modules--}}
    <style>
        .pagination-td ul{
            flex: 1;
            justify-content: center;
        }
    </style>
</head>
<body>
@include('ogbadmin.header')
@yield('content')

<script src="{{asset('js/manifest.js')}}"></script>
<script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
@yield('js')
</body>
</html>

