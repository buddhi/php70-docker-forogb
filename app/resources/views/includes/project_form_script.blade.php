
<script>
    if($('#cattlelist').children().length > 0)
        $('#nocattle').addClass('hide')

    $("#addcattle").on("click", function (e) {
        e.preventDefault();
        var cropName = $('#cattle-name').val()
        var number = $('#number').val()
        if (cropName === "" || number === "0"|| number === "") {
            alertify.alert('Off Grid Bazaar','Please specify all fields to add a cattle');
            return;
        }
        $("#nocattle").addClass("hide");
        $("#cattlelist").removeClass();
        $('#cattlelist').append(
            '<tr class="crop-row">' +
            '<td><input type="hidden" class="form-control" name="cattle_id[]" value="' + $('#cattle-name').val() + '"/>' + $('#cattle-name').find(":selected").text() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="number[]" value="' + $('#number').val() + '"/>' + $('#number').val() + '</td>' +
            '<td><a href="#" class="removecattle" id="">×</a></td>' +
            '</tr>'
        );
        $('.resett').val('');
    });

    $("body").on("click", ".removecattle", function (e) {
        e.preventDefault();
        var me = $(this),
            idAttr = me.attr('id');
        idAttr = idAttr.split('_');
        var id = idAttr.length == 3 ? idAttr[2] : 0;
        alertify.confirm('Off Grid Bazaar', 'Are you sure you want to delete this cattle information?', function () {
            if (id === 0) {
                me.blur();
                me.parents('tr').remove();
            }
            else {
                me.blur();
                me.parents('tr').remove();
                if (!$('#cattlelist').children('tr').length) {
                    $("#nocattle").removeClass("hide");
                    $("#cattlelist").addClass("hide");
                    e.preventDefault();
                    return false;
                }
            }
            if($('#cattlelist').children().length === 0)
                $('#nocattle').removeClass('hide')
        }, function () {
        });
    });
</script>

<script>
    //console.log($('#cattlelist'));
    if($('#incomesourcelist').children().length > 0)
        $('#noincomesource').addClass('hide');
    $("#add_income_source").on("click", function (e) {
        e.preventDefault();
        var  income_source_value = $('#income_source').val();
        var  income_amount_value= $('#income_amount').val();
        if (income_source_value === "" || income_amount_value === "0" || income_amount_value === "") {
            alertify.alert('Off Grid Bazaar','Please specify all fields to add a income source');
            return;
        }
        $("#noincomesource").addClass("hide");
        $("#incomesourcelist").removeClass();
        $('#incomesourcelist').append(
            '<tr class="income-row">' +
            '<td><input type="hidden" class="form-control" name="income_source[]" value="' + $('#income_source').val() + '"/>' + $('#income_source').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="income_amount[]" value="' + $('#income_amount').val() + '"/>' + $('#income_amount').val() + '</td>' +
            '<td><a href="#" class="removesource" id="">×</a></td>' + '</tr>'
        );
        $('.resett').val('');
    });

    $("body").on("click", ".removesource", function (e) {
        e.preventDefault();
        var me = $(this);
        idAttr = me.attr('id');
        idAttr = idAttr.split('_');
        var id = idAttr.length == 3 ? idAttr[2] : 0;
        alertify.confirm('Off Grid Bazaar', 'Are you sure you want to delete this income source?', function () {
            if (id === 0) {
                me.blur();
                me.parents('tr').remove();
            }
            else {
                me.blur();
                me.parents('tr').remove();
                if (!$('#incomesourcelist').children('tr').length) {
                    $("#noincomesource").removeClass("hide");
                    $("#incomesourcelist").addClass("hide");
                    e.preventDefault();
                    return false;
                }
            }
            if($('#incomesourcelist').children().length === 0)
                $('#noincomesource').removeClass('hide')
        }, function () {
        });
    });
</script>


<script>
    if($('#expensesourcelist').children().length > 0)
        $('#noexpensesource').addClass('hide');

    $("#add_expense_source").on("click", function (e) {
        e.preventDefault();
        var  expense_source_value = $('#expense_source').val()
        var  expense_amount_value= $('#expense_amount').val()
        if (expense_source_value === "" || expense_amount_value === "0"|| expense_amount_value === "") {
            alertify.alert('Off Grid Bazaar','Please specify all fields to add a expense source');
            return;
        }
        $("#noexpensesource").addClass("hide");
        $("#expensesourcelist").removeClass();
        $('#expensesourcelist').append(
            '<tr class="expense-row">' +
            '<td><input type="hidden" class="form-control" name="expense_source[]" value="' + $('#expense_source').val() + '"/>' + $('#expense_source').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="expense_amount[]" value="' + $('#expense_amount').val() + '"/>' + $('#expense_amount').val() + '</td>' +
            '<td><a href="#" class="removesourceexpense" id="">×</a></td>' + '</tr>'
        );
        $('.resett').val('');
    });

    $("body").on("click", ".removesourceexpense", function (e) {
        e.preventDefault();
        var me = $(this);
        console.log(me);
        idAttr = me.attr('id');
        idAttr = idAttr.split('_');
        var id = idAttr.length == 3 ? idAttr[2] : 0;
        alertify.confirm('Off Grid Bazaar', 'Are you sure you want to delete this expense source?', function () {
            if (id === 0) {
                me.blur();
                me.parents('tr').remove();
            }
            else {
                me.blur();
                me.parents('tr').remove();

                if (!$('#expensesourcelist').children('tr').length) {
                    $("#noexpensesource").removeClass("hide");
                    $("#expensesourcelist").addClass("hide");
                    e.preventDefault();
                    return false;
                }
            }
            if($('#expensesourcelist').children().length === 0)
                $('#noexpensesource').removeClass('hide')
        }, function () {
        });
    });
</script>


<script>
    if($('#landlist').children().length > 0)
        $('#nolands').addClass('hide');

    $("#addlands").on("click", function (e) {
        e.preventDefault();
        var  land_type = $('#land-type').val()
        var  unit_type= $('#unit-type').val()
        var  ownership= $('#ownership').val()
        var  owner= $('#owner').val()
        var  land_area= $('#land-area').val()
        if (land_type === "" || unit_type === "0"|| ownership === "" || owner===""|| land_area===""||unit_type ==="") {
            alertify.alert('Off Grid Bazaar','Please specify all fields to add a land');
            return;
        }
        $("#nolands").addClass("hide");
        $("#landlist").removeClass();
        $('#landlist').append(
            '<tr class="land-row">' +
            '<td><input type="hidden" class="form-control" name="land_type[]" value="' + $('#land-type').val() + '"/>' + $('#land-type').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="ownership[]" value="' + $('#ownership').val() + '"/>' + $('#ownership').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="owner[]" value="' + $('#owner').val() + '"/>' + $('#owner').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="land_area[]" value="' + $('#land-area').val() + '"/>' + $('#land-area').val() + '</td>' +
            '<td><input type="hidden" class="form-control"  name="unit_type[]" value="' + $('#unit-type').val() + '"/>' + $('#unit-type').val() + '</td>' +
            '<td><a href="#" class="removeland" id="">×</a></td>' + '</tr>'
        );
        $('.resett').val('');
    });

    $("body").on("click", ".removeland", function (e) {
        console.log("hhere");
        e.preventDefault();
        var me = $(this);
        console.log(me);
        idAttr = me.attr('id');
        idAttr = idAttr.split('_');
        var id = idAttr.length == 3 ? idAttr[2] : 0;
        alertify.confirm('Off Grid Bazaar', 'Are you sure you want to delete this land information?', function () {
            if (id === 0) {
                me.blur();
                me.parents('tr').remove();
            }
            else {
                me.blur();
                me.parents('tr').remove();

                if (!$('#landlist').children('tr').length) {
                    $("#nolands").removeClass("hide");
                    $("#landlist").addClass("hide");
                    e.preventDefault();
                    return false;
                }
            }
            if($('#landlist').children().length === 0)
                $('#nolands').removeClass('hide')
        }, function () {
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var value =$('#has_bank').val();
        if(value==='1'){
            $("#show_financial_institute").css('display','block')
        }
        $('#has_bank').on('change',function(){
            if( $(this).val()==='1'){
                $("#show_financial_institute").css('display','block')
            }
            else{
                $("#show_financial_institute").css('display','none')
            }
        });


        var water_distribution =$('#water_distribution_to_farmers').val();
        if(water_distribution==='1'){
            $("#revenues_from_water_distribution").css('display','block')
        }else{
            $("#revenues_from_water_distribution").css('display','none')
        }

        $('#water_distribution_to_farmers').on('change',function(){
            if( $(this).val()==='1'){
                $("#revenues_from_water_distribution").css('display','block')
            }
            else{
                $("#revenues_from_water_distribution").css('display','none')
            }
        });


        var debt_service =$('#debt_service_obligation').val();
        if(debt_service==='1'){
            $("#debt_monthly_payment").css('display','block')
        }else{
            $("#debt_monthly_payment").css('display','none')
        }

        $('#debt_service_obligation').on('change',function(){
            if( $(this).val()==='1'){
                $("#debt_monthly_payment").css('display','block')
            }
            else{
                $("#debt_monthly_payment").css('display','none')
            }
        });

        var monthly_saving_state =$('#monthly_saving').val();
        if(monthly_saving_state ==='1'){
            $("#monthly_saving_amount").css('display','block')
        }else{
            $("#monthly_saving_amount").css('display','none')
        }

        $('#monthly_saving').on('change',function(){
            if( $(this).val()==='1'){
                $("#monthly_saving_amount").css('display','block')
            }
            else{
                $("#monthly_saving_amount").css('display','none')
            }
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        if($('#croplist').children().length > 0)
            $('#nocrops').addClass('hide');

        $("#addcrop").on("click", function (e) {
            console.log("da");
            e.preventDefault();

            var cropName = $('#crop-name').val();
            var startMonth = $('#start-month').val();
            var endMonth = $('#end-month').val();
            var unit_type_crop_area = $('#crop-area-unit-type').val();
            var area = $('#area').val();
            if (cropName === "" || startMonth === "" || endMonth === "" || area === "0"|| area ===""||unit_type_crop_area ==="") {
                alertify.alert('Off Grid Bazaar','Please specify all fields to add a crop');
                return;
            }
            $("#nocrops").addClass("hide");
            $("#croplist").removeClass();
            $('#croplist').append(
                '<tr class="crop-row">' +
                '<td><input type="hidden" class="form-control" name="crop_id[]" value="' + $('#crop-name').val() + '"/>' + $('#crop-name').find(":selected").text() + '</td>' +
                '<td><input type="hidden" class="form-control" name="start_month[]" value="' + $('#start-month').val() + '"/>' + $('#start-month').find(":selected").text() + '</td>' +
                '<td><input type="hidden" class="form-control"  name="area[]" value="' + $('#area').val() + '"/>' + $('#area').val() + '</td>' +
                '<td><input type="hidden" class="form-control"  name="crop_area_unit_type[]" value="' + $('#crop-area-unit-type').val() + '"/>' + $('#crop-area-unit-type').val() + '</td>' +
                '<td><a href="#" class="removecrop" id="">×</a></td>' +
                '</tr>'
            );
            $('.resett').val('');
        });

        $("body").on("click", ".removecrop", function (e) {
            e.preventDefault();
            var me = $(this),
                idAttr = me.attr('id');
            idAttr = idAttr.split('_');
            var id = idAttr.length == 3 ? idAttr[2] : 0;
            alertify.confirm('Off Grid Bazaar', 'Are you sure you want to delete this crop information?', function () {
                if (id === 0) {
                    me.blur();
                    me.parents('tr').remove();
                }
                else {
                    $.ajax({
                        url: '#',
                        method: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            cropinfo_id: $this.attr('cropinfo-id'),
                        },
                        success: function (resp) {
                            me.blur();
                            me.parents('tr').remove();
                            if (!$('#croplist').children('tr').length) {
                                $("#nocrops").removeClass("hide");
                                $("#croplist").addClass("hide");
                                e.preventDefault();
                                return false;
                            }
                        }
                    });
                }
                if($('#croplist').children().length === 0)
                    $('#nocrops').removeClass('hide')
            }, function () {
            });
        });
        $('#province_id').on('change', function(e){
            var route = "{{url('/data/districts')}}/" + e.target.value;
            var route_municipality = "{{url('/data/municipalities')}}/" + e.target.value;
            updateSelectWithDataFrom($('#municipality_id'), route_municipality);
            updateSelectWithDataFrom($('#district_id'), route);
        })

        $('#district_id').on('change', function(e){
            var route = "{{url('/data/municipalities')}}/" + e.target.value;
            updateSelectWithDataFrom($('#municipality_id'), route);
        })

        function updateSelectWithDataFrom(selectElement, dataUrl){
            $.get(dataUrl, function(data) {
                selectElement.empty();
                selectElement.append('<option value="">Please Select</option>')
                data.map(el => selectElement.append('<option value=' + el.id + '>' + el.name + '</option>'));
            })
        }
    });
</script>