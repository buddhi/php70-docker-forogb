@php
    if(isset($selected_districts) && isset($selected_municipalities)){
      $districts =$selected_districts;
      $municipalities =$selected_municipalities;
    }
@endphp
<link rel="stylesheet" type="text/css" href="{{asset('/offgrid-bazaar/css/project-form.css')}}">

<input class="form-control" name="is_draft" id="is_draft" value="1" type="hidden" />
<input class="form-control" name="latitude" id="latitude" value="27.5065119" type="hidden"/>
<input class="form-control" name="longitude" id="longitude" value="83.4376749" type="hidden"/>
<input class="form-control" name="created_by" id="created_by" value="{{Auth::id()}}" type="hidden" />

<div class="row">
    <div class="col-md-12">
        <h4>Farmer's Details</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="farmer_name">Name of the Farmer <span class="required">*</span> <span class="sl-text">({{ __('app.farmer_name')}})</span>
            </label>
        <input class="form-control"  placeholder="Farmer Name" name="farmer_name"   type="text" required="required" maxlength="200" value="{{ old('farmer_name', isset($farmer) ? $farmer[0]['farmer_name'] : "") }}"/>
        @if ($errors->has('farmer_name'))
            <span class="error">
                <strong>{{ $errors->first('farmer_name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="address">Address<span class="required">*</span> <span class ="sl-text">({{__('app.address')}})</span>
            
        </label>
        <input class="form-control"  placeholder="Enter address" name="address" id="Project_address" required="required" type="text" maxlength="200" value="{{ old('address', isset($farmer) ? $farmer[0]['address'] : "") }}" />
        @if ($errors->has('address'))
            <span class="error">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="contact_no">Contact no <span class="required">*</span> <span class ="sl-text">({{__('app.contact_no')}})</span>
            
        </label>
        <input class="form-control" placeholder="Enter contact no"  name="contact_no" id="contact_no" type="text" value="{{ old('contact_no', isset($farmer) ? $farmer[0]['contact_no'] : "") }}"/>
        @if ($errors->has('contact_no'))
            <span class="error">
                <strong>{{ $errors->first('contact_no') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-12">
        <label class="inline-label required" for="age">Age <span class ="sl-text">({{__('app.age')}})</span></label>
        <input class="form-control" placeholder="Enter age"  name="age" id="age" type="number" min = "0" max = "200" value="{{ old('age', isset($farmer) ? $farmer[0]['age'] : "") }}"/>
        @if ($errors->has('age'))
            <span class="error">
                <strong>{{ $errors->first('age') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class ="province">
            <label class="inline-label" for="province_id">Province <span class="required">*</span><span class ="sl-text">({{__('app.province')}})</span>
                
            </label>
            <select class="form-control" name="province_id" id="province_id"  >
                <option value="">Please Select</option>
                @foreach($provinces as $province)
                    <option value="{{ $province->id }}"{!! old('province_id', isset($project)? $project->province_id : "")== $province->id? "selected":"" !!}>{{ $province->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('province_id'))
                <span class="error">
                    <strong>{{ $errors->first('province_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="district">
            <label class="inline-label" for="district_id">District <span class="required">*</span><span class ="sl-text">({{__('app.district')}})</span>
                
            </label>
            <select class="form-control" name="district_id" id="district_id" >
                <option value="">Please Select</option>
                @foreach($districts as $district)
                    <option value="{{ $district->id }}"{!! old('district_id', isset($project) ? $project->district_id : "")== $district->id? "selected":"" !!}>{{ $district->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('district_id'))
                <span class="error">
                <strong>{{ $errors->first('district_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="inline-label" for="municipality_id">Municipality/Village <span class="required">*</span> <span class ="sl-text">({{__('app.municipality/gaunpalika')}})</span>
            
        </label>
        <select class="form-control" name="municipality_id" id="municipality_id"  >
            <option value="">Please Select</option>
            @foreach($municipalities as $municipality)
                <option value="{{ $municipality->id }}"{!! old('municipality_id', isset($project) ? $project->municipality_id : "")== $municipality->id? "selected":"" !!}>{{ $municipality->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('municipality_id'))
            <span class="error">
                        <strong>{{ $errors->first('municipality_id') }}</strong>
                     </span>
        @endif
    </div>
    <div class="col-md-12">
        <label class="inline-label" for="ward_no">Ward No <span class="required">*</span> <span class ="sl-text">({{__('app.ward_no')}})</span>
            
        </label>
        <input class="form-control" placeholder="Ward Number"  name="ward_no" id="ward_no" type="number" value="{{ old('ward_no', isset($project) ? $project->ward_no : "") }}"/>
        @if ($errors->has('ward_no'))
            <span class="error">
                    <strong>{{ $errors->first('ward_no') }}</strong>
                </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="village_name">Village Name:
        <br> <span class ="sl-text">({{__('app.village_name')}})</span></label>

        <input class="form-control" placeholder="Enter village name"  name="village_name" id="village_name" type="text" value="{{ old('village_name', isset($farmer) ? $farmer[0]['village_name'] : "") }}"/>
        @if ($errors->has('village_name'))
            <span class="error">
                <strong>{{ $errors->first('village_name') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="no_of_dependents">No of dependents <span class="required">*</span>:
            <br><span class ="sl-text">({{__('app.no_of_dependents')}})</span>
        </label>
        <input class="form-control" placeholder="Enter number of dependents"  name="no_of_dependents" id="no_of_dependents" type="number" min = "0" max = "1000" step ="1" value="{{ old('no_of_dependents', isset($farmer) ? $farmer[0]['no_of_dependents'] : "") }}"/>
        @if ($errors->has('no_of_dependents'))
            <span class="error">
                <strong>{{ $errors->first('no_of_dependents') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="annual_household_income">Annual Household Income({{displayUnitFormat('amount')}}) <span class="required">*</span>:
            <br><span class ="sl-text">({{__('app.annual_household_income')}})</span>
        </label>
        <input class="form-control" placeholder="Enter household income"  name="annual_household_income" id="annual_household_income" type="number" value="{{ old('annual_household_income', isset($farmer) ? $farmer[0]['annual_household_income'] : "") }}"/>
        @if ($errors->has('annual_household_income'))
            <span class="error">
                <strong>{{ $errors->first('annual_household_income') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="annual_expenses">Annual expenses ({{displayUnitFormat('amount')}})  <span class="required">*</span>
            <br><span class ="sl-text">({{__('app.annual_expenses')}})</span>
        </label>
        <input class="form-control" placeholder="Enter annual name"  name="annual_expenses" id="annual_expenses" type="number" value="{{ old('annual_expenses', isset($farmer) ? $farmer[0]['annual_expenses'] : "") }}"/>
        @if ($errors->has('annual_expenses'))
            <span class="error">
                <strong>{{ $errors->first('annual_expenses') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="education">Any person in the family has education greater than 10 class? ({{displayUnitFormat('yes_no_question')}})
            <br><span class ="sl-text">({{__('app.education')}})</span>
        </label>
         
        <select class="form-control" name="education" id = "education">
            <option  value="1" {!! old('education', isset($farmer[0]) ? $farmer[0]['education'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0"  {!! old('education', isset($farmer[0]) ? $farmer[0]['education'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('education'))
            <span class="error">
                <strong>{{ $errors->first('education') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="certification">Have you had any formal education / certification in farming ({{displayUnitFormat('yes_no_question')}})
            <br><span class ="sl-text">({{__('app.certification')}})</span>
        </label>
        <select class="form-control" name="certification" id = "certification">
            <option  value="1"  {!! old('certification', isset($farmer[0]) ?  $farmer[0]['certification'] : "")== 1 ? "selected":"" !!}>Yes</option  >
            <option value= "0"  {!! old('certification', isset( $farmer[0]) ? $farmer[0]['certification']:'') == 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('certification'))
            <span class="error">
                <strong>{{ $errors->first('certification') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="gender">Gender <span class="required">*</span><span class ="sl-text">({{__('app.gender')}})</span>
        </label>
        <select class="form-control" name="gender" id = "gender">
            <option value ="">Please Select</option>
            <option  value="male" {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "male"? "selected":"" !!}>Male</option>
            <option value="female" {!! old('gender', isset($farmer[0]) ? $farmer[0]['gender'] : "")== "female"? "selected":"" !!}>Female</option>
        </select>
        @if ($errors->has('gender'))
            <span class="error">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="no_of_people_in_house">No of People in house <span class="required">*</span><span class ="sl-text">({{__('app.no_of_people_in_house')}})</span>
        </label>
        <input class="form-control" placeholder="Enter number of people in house"  name="no_of_people_in_house" id="no_of_people_in_house" type="number" min = "0" max = "150" value="{{ old('no_of_people_in_house', isset($farmer) ? $farmer[0]['no_of_people_in_house'] : "") }}"/>
        @if ($errors->has('no_of_people_in_house'))
            <span class="error">
                <strong>{{ $errors->first('no_of_people_in_house') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="has_bank">Bank Present ({{displayUnitFormat('yes_no_question')}}) <span class ="sl-text">({{__('app.has_bank')}})</span> </label>
        <select class="form-control" name="has_bank" id = "has_bank">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('has_bank', isset($farmer[0]) ? $farmer[0]['has_bank'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('has_bank', isset($farmer[0]) ? $farmer[0]['has_bank'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('has_bank'))
            <span class="error">
                <strong>{{ $errors->first('has_bank') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-12" id = "show_financial_institute">
        <label class="inline-label required" for="financial_institute">Bank/Financial Institution
        </label>
        <input class="form-control" placeholder="Enter financial institution"  name="financial_institute" id="financial_institute" type="text" value="{{ old('financial_institute', isset($farmer[0]['financial_institute']) ? $farmer[0]['financial_institute'] : "") }}"/>
        @if ($errors->has('financial_institute'))
            <span class="error">
                <strong>{{ $errors->first('financial_institute') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="water_distribution_to_farmers">Water distribution to farmers ({{displayUnitFormat('yes_no_question')}})<span class ="sl-text">({{__('app.water_distribution_to_farmers')}})</span></label>
        <select class="form-control" name="water_distribution_to_farmers" id = "water_distribution_to_farmers">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('water_distribution_to_farmers', isset($farmer[0]) ? $farmer[0]['water_distribution_to_farmers'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('water_distribution_to_farmers', isset($farmer[0]) ? $farmer[0]['water_distribution_to_farmers'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('water_distribution_to_farmers'))
            <span class="error">
                <strong>{{ $errors->first('water_distribution_to_farmers') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-12" id = "revenues_from_water_distribution">
        <label class="inline-label required" for="revenues_from_water_distribution">Revenues from water distribution ({{displayUnitFormat('amount')}})
        </label>
        <input class="form-control" placeholder="Enter revenues from water distribution"  name="revenues_from_water_distribution" id="revenues_from_water_distribution" type="number" value="{{ old('revenues_from_water_distribution', isset($farmer[0]['revenues_from_water_distribution']) ? $farmer[0]['revenues_from_water_distribution'] : "") }}"/>
        @if ($errors->has('revenues_from_water_distribution'))
            <span class="error">
                <strong>{{ $errors->first('revenues_from_water_distribution') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="debt_service_obligation">Any debts to pay ({{displayUnitFormat('yes_no_question')}}) <span class ="sl-text">({{__('app.debt_service_obligation')}}) </span></label>
        <select class="form-control" name="debt_service_obligation" id = "debt_service_obligation">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('debt_service_obligation', isset($farmer[0]) ? $farmer[0]['debt_service_obligation'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('debt_service_obligation', isset($farmer[0]) ? $farmer[0]['debt_service_obligation'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('debt_service_obligation'))
            <span class="error">
                <strong>{{ $errors->first('debt_service_obligation') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-12" id = "debt_monthly_payment">
        <label class="inline-label required" for="debt_monthly_payment">Debt payment to institutions ({{displayUnitFormat('amount')}})
        </label>
        <input class="form-control" placeholder="Enter debt monthly payment"  name="debt_monthly_payment" id="debt_monthly_payment" type="text" value="{{ old('debt_monthly_payment', isset($farmer[0]['debt_monthly_payment']) ? $farmer[0]['debt_monthly_payment'] : "") }}"/>
        @if ($errors->has('debt_monthly_payment'))
            <span class="error">
                <strong>{{ $errors->first('debt_monthly_payment') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="monthly_saving">Monthly saving Present ({{displayUnitFormat('yes_no_question')}})? <span class = "sl-text">({{__('app.monthly_saving')}}) </span></label>
        <select class="form-control" name="monthly_saving" id = "monthly_saving">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('monthly_saving', isset($farmer[0]) ? $farmer[0]['monthly_saving'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('monthly_saving', isset($farmer[0]) ? $farmer[0]['monthly_saving'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('monthly_saving'))
            <span class="error">
                <strong>{{ $errors->first('monthly_saving') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-12" id = "monthly_saving_amount">
        <label class="inline-label required" for="monthly_saving_amount">Monthly saving amount ({{displayUnitFormat('amount')}})
        </label>
        <input class="form-control" placeholder="Enter monthly saving amount"  name="monthly_saving_amount" id="monthly_saving_amount" type="number" value="{{ old('monthly_saving_amount', isset($farmer[0]['monthly_saving_amount']) ? $farmer[0]['monthly_saving_amount'] : "") }}"/>
        @if ($errors->has('monthly_saving_amount'))
            <span class="error">
                <strong>{{ $errors->first('monthly_saving_amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="additional_income_source">Additional income source ({{displayUnitFormat('yes_no_question')}})<span class = "sl-text">({{__('app.additional_income_source')}})</span>
        </label>
        <select class="form-control" name="additional_income_source" id = "additional_income_source">
            <option  value="1" {!! old('additional_income_source', isset($farmer[0]) ? $farmer[0]['additional_income_source'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0"  {!! old('additional_income_source', isset($farmer[0]) ? $farmer[0]['additional_income_source'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('additional_income_source'))
            <span class="error">
                <strong>{{ $errors->first('additional_income_source') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="additional_salaried_occupation">Member with salaried occupation ({{displayUnitFormat('yes_no_question')}}) <span class ="sl-text">({{__('app.additional_salaried_occupation')}})</span> 
        </label>
        <select class="form-control" name="additional_salaried_occupation" id = "additional_salaried_occupation">
            <option  value="1"  {!! old('additional_salaried_occupation', isset($farmer[0]) ?  $farmer[0]['additional_salaried_occupation'] : "")== 1 ? "selected":"" !!}>Yes</option  >
            <option value= "0"  {!! old('additional_salaried_occupation', isset( $farmer[0]) ? $farmer[0]['additional_salaried_occupation']:'') == 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('additional_salaried_occupation'))
            <span class="error">
                <strong>{{ $errors->first('additional_salaried_occupation') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="recent_large_purchase_through_payment_plan">Have you completed any recent large purchases(such as house,land,vehicles) through a payment plans ({{displayUnitFormat('yes_no_question')}})? <span class = "sl-text">({{__('app.recent_large_purchase_through_payment_plan')}})</span>

        </label>
        <select class="form-control" name="recent_large_purchase_through_payment_plan" id = "recent_large_purchase_through_payment_plan">
            <option  value="1" {!! old('recent_large_purchase_through_payment_plan', isset($farmer[0]) ? $farmer[0]['recent_large_purchase_through_payment_plan'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0"  {!! old('recent_large_purchase_through_payment_plan', isset($farmer[0]) ? $farmer[0]['recent_large_purchase_through_payment_plan'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('recent_large_purchase_through_payment_plan'))
            <span class="error">
                <strong>{{ $errors->first('recent_large_purchase_through_payment_plan') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="future_capital_expenditure_causing_cash_flow_interruptions">Do you expect any interruptions to your cash flows due to planned capital expenditures (e.g. more land, equipment, buildings)? ({{displayUnitFormat('yes_no_question')}})
        <span class ="sl-text">({{__('app.future_capital_expenditure_causing_cash_flow_interruptions')}})</span>
        </label>
        <select class="form-control" name="future_capital_expenditure_causing_cash_flow_interruptions" id = "future_capital_expenditure_causing_cash_flow_interruptions">
            <option  value="1"  {!! old('future_capital_expenditure_causing_cash_flow_interruptions', isset($farmer[0]) ?  $farmer[0]['future_capital_expenditure_causing_cash_flow_interruptions'] : "")== 1 ? "selected":"" !!}>Yes</option  >
            <option value= "0"  {!! old('future_capital_expenditure_causing_cash_flow_interruptions', isset( $farmer[0]) ? $farmer[0]['future_capital_expenditure_causing_cash_flow_interruptions']:'') == 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('future_capital_expenditure_causing_cash_flow_interruptions'))
            <span class="error">
                <strong>{{ $errors->first('future_capital_expenditure_causing_cash_flow_interruptions') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="nearest_market"> Enter name of nearest marketplace<span class="required">*</span><span class ="sl-text">({{__('app.nearest_market')}})</span>
        </label>
        <input class="form-control" placeholder="Enter the nearest market"  name="nearest_market" id="nearest_market" type="text" value="{{ old('nearest_market', isset($farmer) ? $farmer[0]['nearest_market'] : "") }}"/>
        @if ($errors->has('nearest_market'))
            <span class="error">
                <strong>{{ $errors->first('nearest_market') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="distance_to_nearest_market"> Distance to nearest market({{displayUnitFormat('distance_to_nearest_market')}}) <span class ="sl-text">({{__('app.distance_to_nearest_market')}})</span>
        </label>
        <input class="form-control" placeholder="Enter the distance to nearest market"  name="distance_to_nearest_market" id="distance_to_nearest_market" type="number" value="{{ old('distance_to_nearest_market', isset($farmer) ? $farmer[0]['distance_to_nearest_market'] : "") }}"/>
        @if ($errors->has('distance_to_nearest_market'))
            <span class="error">
                <strong>{{ $errors->first('distance_to_nearest_market') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="land_owned">Do you/ any members in your family own land ? ({{displayUnitFormat('yes_no_question')}}) <span class ="sl-text">({{__('app.land_owned')}})</span> </label>
        <select class="form-control" name="land_owned" id = "land_owned">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('land_owned', isset($farmer[0]) ? $farmer[0]['land_owned'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('land_owned', isset($farmer[0]) ? $farmer[0]['land_owned'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('land_owned'))
            <span class="error">
                    <strong>{{ $errors->first('land_owned') }}</strong>
                </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="crop_inventory">Crop inventory ({{displayUnitFormat('yes_no_question')}}) <span class = "sl-text">({{__('app.crop_inventory')}}) </span></label>
        <select class="form-control" name="crop_inventory" id = "crop_inventory">
            <option value = "">Please Select</option>
            <option  value="1" {!! old('crop_inventory', isset($farmer[0]) ? $farmer[0]['crop_inventory'] : "")== 1? "selected":"" !!}>Yes</option>
            <option value="0" {!! old('crop_inventory', isset($farmer[0]) ? $farmer[0]['crop_inventory'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('crop_inventory'))
            <span class="error">
                    <strong>{{ $errors->first('crop_inventory') }}</strong>
                </span>
        @endif
    </div>
</div>





<div class="row">

    <div class="col-md-6">
        <label class="inline-label required" for="expected_solar_loan_monthly_payment">Expected solar loan monthly payment ({{displayUnitFormat('amount')}}) <span class="required">*</span><span class ="sl-text">({{__('app.expected_solar_loan_monthly_payment')}})</span>
        </label><input class="form-control"  placeholder="Enter expected solar payment" name="expected_solar_loan_monthly_payment" id="Project_expected_solar_loan_monthly_payment"  type="number" maxlength="200" value="{{ old('expected_solar_loan_monthly_payment', isset($farmer) ? $farmer[0]['expected_solar_loan_monthly_payment'] : "") }}"/>
        @if ($errors->has('expected_solar_loan_monthly_payment'))
            <span class="error">
                <strong>{{ $errors->first('expected_solar_loan_monthly_payment') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="outstanding_debt">Outstanding debt ({{displayUnitFormat('amount')}})<span class ="sl-text">({{__('app.outstanding_debt')}})</span>
        </label><input class="form-control"  placeholder="Outstanding debt" name="outstanding_debt" id="Project_outstanding_debt"  type="number" maxlength="200" value="{{ old('outstanding_debt', isset($farmer) ? $farmer[0]['outstanding_debt'] : "") }}"/>
        @if ($errors->has('outstanding_debt'))
            <span class="error">
                <strong>{{ $errors->first('outstanding_debt') }}</strong>
            </span>
        @endif
    </div>


</div>
<div class ="row">
    <div class="col-md-12">
        <label class="inline-label required" for="project_type_id">Project Type
            <span class="required">*</span>
        </label>
        <select class="form-control" name="project_type_id" id="project_type_id" >
            <option value="">Please Select</option>
            @foreach($project_types as $project_type)
                <option value="{{ $project_type->id }}" {!! old('project_type_id', isset($project)? $project->project_type_id : "")== $project_type->id? "selected":"" !!}>{{ $project_type->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('project_type_id'))
            <span class="error">
                <strong>{{ $errors->first('project_type_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4>Farm Details</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="distance">Distance ({{displayUnitFormat('distance')}})<span class="required">*</span> <span class ="sl-text">({{__('app.distance')}})</span>
           
        </label>
        <input class="form-control" placeholder="Enter distance from source to farm in meters"  name="distance" id="distance" type="number" value="{{ old('distance', isset($farm) ? $farm->distance : "") }}"/>
        @if ($errors->has('distance'))
            <span class="error">
                <strong>{{ $errors->first('distance') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="current_irrigation_source">Current Irrigation Source <span class="required">*</span><span class ="sl-text">({{__('app.current_irrigation_source')}})</span>
            
        </label>
        <select class="form-control"  name="current_irrigation_source" id="current_irrigation_source" >
            <option value="">Please Select</option>
            <option value="1"{!! old('current_irrigation_source', isset($farm) ? $farm->current_irrigation_source:'')== 1 ? "selected":"" !!}>Pond<span class ="sl-text">({{__('app.pond')}})</span></option>
            <option value="2"{!! old('current_irrigation_source', isset($farm) ? $farm->current_irrigation_source:'')== 2 ? "selected":"" !!}>Rain<span class ="sl-text">({{__('app.rain')}})</span></option>
            <option value="3"{!! old('current_irrigation_source', isset($farm) ? $farm->current_irrigation_source:'')== 3 ? "selected":"" !!}>River<span class ="sl-text">({{__('app.river')}})</span></option>
            <option value="4"{!! old('current_irrigation_source', isset($farm) ? $farm->current_irrigation_source:'')== 4 ? "selected":"" !!}>Boring<span class ="sl-text">({{__('app.boring')}})</span></option>
            <option value="5"{!! old('current_irrigation_source', isset($farm) ? $farm->current_irrigation_source:'')== 5 ? "selected":"" !!}>Well<span class ="sl-text">({{__('app.well')}})</span></option>
        </select>
        @if ($errors->has('current_irrigation_source'))
            <span class="error">
                <strong>{{ $errors->first('current_irrigation_source') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="current_irrigation_system">Current Irrigation System <span class="required">*</span><span class ="sl-text">({{__('app.current_irrigation_system')}})</span>
            
        </label>
        <select class="form-control"  name="current_irrigation_system" id="current_irrigation_system" >
            <option value="">Please Select</option>
            <option value="1"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 1 ? "selected":"" !!}>Diesel pump <span class ="sl-text">({{__('app.diesel_pump')}})</span></option>
            <option value="2"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 2 ? "selected":"" !!}>Electeric pump <span class ="sl-text">({{__('app.electeric_pump')}})</span></option>
            <option value="3"{!! old('current_irrigation_system', isset($farm) ? $farm->current_irrigation_system:'')== 3 ? "selected":"" !!}>canal <span class ="sl-text">({{__('app.canal')}})</span></option>
        </select>
        @if ($errors->has('current_irrigation_system'))
            <span class="error">
                <strong>{{ $errors->first('current_irrigation_system') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="boring_size">Boring Size ({{displayUnitFormat('boring_size')}}) <span class="required">*</span> <span class ="sl-text">({{__('app.boring_size')}})</span>
            
        </label>
        <input class="form-control" placeholder="Enter boring size in inches"  name="boring_size" id="boring_size" type="number" value="{{ old('boring_size', isset($farm) ? $farm->boring_size: "") }}"/>
        @if ($errors->has('boring_size'))
            <span class="error">
                <strong>{{ $errors->first('boring_size') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="vertical_head">Vertical head ({{displayUnitFormat('vertical_head')}})<span class="required">*</span> <span class ="sl-text">({{__('app.vertical_head')}})</span>
            
        </label>
        <input class="form-control" placeholder="Enter vertical head in meter"  name="vertical_head" id="vertical_head" type="number" value="{{ old('vertical_head', isset($farm) ? $farm->vertical_head : "") }}"/>
        @if ($errors->has('vertical_head'))
            <span class="error">
                <strong>{{ $errors->first('vertical_head') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="water_requirement">Water requirement ({{displayUnitFormat('water_requirement')}})
            <span class="required">*</span>
        </label>
        <input class="form-control" placeholder="Enter water requirement in L/day"  name="water_requirement" id="water_requirement" type="text" value="{{ old('water_requirement', isset($farm) ? $farm->water_requirement : "") }}"/>
        @if ($errors->has('water_requirement'))
            <span class="error">
                <strong>{{ $errors->first('water_requirement') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="boring_depth">Boring depth ({{displayUnitFormat('boring_depth')}})<span class="required">*</span><span class ="sl-text">({{__('app.boring_depth')}})</span>
            
        </label>
        <input class="form-control" placeholder="Enter boring depth in feet"  name="boring_depth" id="boring_depth" type="number" value="{{ old('boring_depth', isset($farm) ? $farm->boring_depth : "") }}"/>
        @if ($errors->has('boring_depth'))
            <span class="error">
                <strong>{{ $errors->first('boring_depth') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="type_of_soil">Type of soil
            <span class="required">*</span>
        </label>
        <input class="form-control" placeholder="Enter type of soil"  name="type_of_soil" id="type_of_soil" type="text" value="{{ old('type_of_soil', isset($farm) ? $farm->type_of_soil : "") }}"/>
        @if ($errors->has('type_of_soil'))
            <span class="error">
                <strong>{{ $errors->first('type_of_soil') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="ground_water_level">Ground water level({{displayUnitFormat ('ground_water_level')}})
            <span class="required">*</span>
            <span class ="sl-text">({{__('app.ground_water_level')}})</span>
        </label>
        <input class="form-control" placeholder="Enter ground water level in feet"  name="ground_water_level" id="ground_water_level" type="number" value="{{ old('ground_water_level', isset($farm) ? $farm->ground_water_level : "") }}"/>
        @if ($errors->has('ground_water_level'))
            <span class="error">
                <strong>{{ $errors->first('ground_water_level') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="fuel_monthly_expense">Fuel monthly Expense ({{displayUnitFormat('amount')}})<span class="required">*</span><span class ="sl-text">({{__('app.fuel_monthly_expense')}})</span>
        </label>
        <input class="form-control" placeholder="Enter monthly expense"  name="fuel_monthly_expense" id="fuel_monthly_expense" type="number" value="{{ old('fuel_monthly_expense', isset($farmer) ? $farmer[0]['fuel_monthly_expense'] : "") }}"/>
        @if ($errors->has('fuel_monthly_expense'))
            <span class="error">
                <strong>{{ $errors->first('fuel_monthly_expense') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="grid_monthly_expense">Grid monthly expense ({{displayUnitFormat('amount')}})<span class="required">*</span><span class ="sl-text">({{__('app.grid_monthly_expense')}})</span>
        </label>
        <input class="form-control" placeholder="Enter grid monthly expense"  name="grid_monthly_expense" id="grid_monthly_expense" type="number" value="{{ old('grid_monthly_expense', isset($farmer) ? $farmer[0]['grid_monthly_expense'] : "") }}"/>
        @if ($errors->has('grid_monthly_expense'))
            <span class="error">
                <strong>{{ $errors->first('grid_monthly_expense') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Psychomatric risk commented out for now--}}
{{--@include('includes.pyschomatric_form')--}}

{{--start of income source form--}}
<div class="col-sm-12">
    <p class="form-section-header">Income Source Information<span class ="sl-text">({{__('app.income_source_information')}})</span></p>
</div>
<div class="col-sm-12">
    <table class="table table-responsive table-striped">
        <thead>
        <td>Income source<span class ="sl-text">({{__('app.income_source')}})</span></td>
        <td>Monthly Income amount<span class ="sl-text">({{__('app.monthly_income_amount')}})</span> </td>
        <td></td>
        </thead>
        <tr id="noincomesource">
            <td colspan="5" class="text-center no-incomesource">
                <h4>There are no income source amount right now.<span class ="sl-text">({{__('app.no_income_source')}})</span></h4>
                <small>Add a income source in form below.<span class ="sl-text">({{__('app.add_income_source')}})</span></small>
            </td>
        </tr>
        <tbody id="incomesourcelist">

        @if(is_array(old('income_source')) && is_array(old('income_amount')))
            <?php $income_source = old('income_source'); $income_amount = old('income_amount');?>
            @for($i = 0; $i < sizeof($income_source); $i++)
                <tr class="crop-row">
                    <td>
                        <input type="hidden" class="form-control" name="income_source[]" value="{{ $income_source[$i]}}">{{ $income_source[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="income_amount[]" value="{{$income_amount[$i]}}">{{$income_amount[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="" value=""><a href="#" class="removesource" id="income_id">×</a>
                    </td>
                </tr>
            @endfor
        @else
            @if(isset($income_array) && count($income_array) > 0)
                @foreach ($income_array as $key => $income)
                    <tr class="crop-row">
                        <td>
                            <input type="hidden" class="form-control" name="income_source[]" value="{{ $income['income_source']}}">{{$income['income_source']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="income_amount[]" value="{{ $income['income_amount']}}">{{$income['income_amount']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="" value=""><a href="#" class="removesource" id="income_id">×</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
        <tr class="create_income option_value">
            <td width="45%">
                <label>Income source <span class="required" aria-required="true">*</span><span class ="sl-text">({{__('app.income_source')}})</span></label>
                <input type="text" placeholder="source of income" class="form-control resett" id="income_source" name="">
                @if ($errors->has('income_source'))
                    <span class="error">
                            <strong>{{ $errors->first('income_source') }}</strong>
                    </span>
                @endif
            <td width="45%">
                <label>Monthly Income amount ({{displayUnitFormat('amount')}}) <span class="required" aria-required="true">*</span><span class ="sl-text">({{__('app.monthly_income_amount')}}) </span></label>
                <input type="number" placeholder="Amount" class="form-control resett" id="income_amount" name="" min = "0">
                @if ($errors->has('income_amount'))
                    <span class="error">
                        <strong>{{ $errors->first('income_amount') }}</strong>
                    </span>
                @endif
            </td>
            <td style = "padding-top:40px">
                <button id="add_income_source" class="btn btn-info btn-block" type = "button">Add</button>
            </td>
        </tr>
    </table>
    <div>
        <label class="cattle-error error hide">Cattle Information missing.</label>
    </div>
</div>
{{--End of income source form--}}


{{--start of expense source form--}}
<div class="col-sm-12">
    <p class="form-section-header">Expense Source Information<span class ="sl-text">({{__('app.expense_source_information')}})</span></p>
</div>
<div class="col-sm-12">
    <table class="table table-responsive table-striped">
        <thead>
        <td>Expense source<span class ="sl-text">({{__('app.expense_source')}})</span></td>
        <td>Monthly expense amount<span class ="sl-text">({{__('app.monthly_expense_amount')}})</span></td>
        <td></td>
        </thead>
        <tr id="noexpensesource">
            <td colspan="5" class="text-center no-expense">
                <h4>There are no expense source amount right now.<span class ="sl-text">({{__('app.no_expense_source')}})</span></h4>
                <small>Add a expense source in form below.<span class ="sl-text">({{__('app.add_expense_source')}})</span> </small>
            </td>
        </tr>
        <tbody id="expensesourcelist">
        @if(is_array(old('expense_source')) && is_array(old('expense_amount')))
            <?php $expense_source = old('expense_source'); $expense_amount = old('expense_amount');?>
            @for($i = 0; $i < sizeof($expense_source); $i++)
                <tr class="crop-row">
                    <td>
                        <input type="hidden" class="form-control" name="expense_source[]" value="{{ $expense_source[$i]}}">{{ $expense_source[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="expense_amount[]" value="{{$expense_amount[$i]}}">{{$expense_amount[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="" value=""><a href="#" class="removesource" id="income_id">×</a>
                    </td>
                </tr>
            @endfor
        @else
            @if(isset($expense_array) && count($expense_array) > 0)
                @foreach ($expense_array as $key => $expense)
                    <tr class="crop-row">
                        <td>
                            <input type="hidden" class="form-control" name="expense_source[]" value="{{$expense['expense_source']}}">{{$expense['expense_source']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="expense_amount[]" value="{{ $expense['expense_amount']}}">{{$expense['expense_amount']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="" value=""><a href="#" class="removesourceexpense" id="expense_id">×</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
        <tr class="create_expense option_value">
            <td width="45%">
                <label>Expense source <span class="required" aria-required="true">*</span><span class ="sl-text">({{__('app.expense_source')}})</span></label>
                <input type="text" placeholder="source of expense" class="form-control resett" id="expense_source" name="">
                @if ($errors->has('expense_source'))
                    <span class="error">
                            <strong>{{ $errors->first('expense_source') }}</strong>
                    </span>
            @endif

            <td width="45%">
                <label>Monthly expense amount ({{displayUnitFormat('amount')}}) <span class="required" aria-required="true">*</span><span class ="sl-text">({{__('app.monthly_expense_amount')}}) </span></label>
                <input type="number" placeholder="Amount" class="form-control resett" id="expense_amount" name="" min = "0">
                @if ($errors->has('expense_amount'))
                    <span class="error">
                        <strong>{{ $errors->first('expense_amount') }}</strong>
                    </span>
                @endif
            </td>
            <td  style = "padding-top:40px">
                <button id="add_expense_source" class="btn btn-info btn-block" type = "button">Add</button>
            </td>
        </tr>
    </table>
    <div>
        <label class="cattle-error error hide">Cattle Information missing.</label>
    </div>
</div>
{{--End of expense source form--}}



{{--start of cattle form--}}
<div class="col-sm-12">
    <p class="form-section-header">Cattle Information <span class ="sl-text">({{__('app.animals_detail')}})</span></p>
</div>
<div class="col-sm-12">
    <table class="table table-responsive table-striped">
        <thead>
        <td>Cattle Name<span class ="sl-text">({{__('app.cattle_name')}})</span></td>
        <td>Number<span class ="sl-text">({{__('app.number')}})</span></td>
        <td></td>
        </thead>
        <tr id="nocattle">
            <td colspan="5" class="text-center no-cattle">
                <h4>There are no cattle right now.
                
                <span class ="sl-text">({{__('app.no_animal_details_available')}})</span></h4>
                <small>Add a cattle from the form below.<span class ="sl-text">({{__('app.select_animals')}})</span></small>

            </td>
        </tr>
        <tbody id="cattlelist">
        @if(is_array(old('cattle_id')) && is_array(old('number')))
            <?php $cattle_id = old('cattle_id'); $number = old('number');?>
            @for($i = 0; $i < sizeof(old('cattle_id')); $i++)
                <tr class="crop-row">
                    <input type="hidden" class="form-control" name="cattle_info_id[]" value="null">
                    <td>
                        <input type="hidden" class="form-control" name="cattle_id[]" value="{{$cattle_id[$i]}}">{{getAttributeForModel(\App\Models\Cattle::class,$cattle_id[$i],'name')}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="number[]" value="{{ $number[$i]}}">{{ isset($number[$i])?$number[$i]: 0}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="" value=""><a href="#" class="removecattle" id="cattle_info">×</a>
                    </td>
                </tr>
            @endfor
        @else
            @if(isset($cattleInfoArray) && count($cattleInfoArray) > 0)
                @foreach ($cattleInfoArray as $key => $cattleInfo)
                    <tr class="crop-row">
                        <input type="hidden" class="form-control" name="cattle_info_id[]"
                               value="{{isset($draftProject)? null:$cattleInfo->id}}">
                        <td>
                            <input type="hidden" class="form-control" name="cattle_id[]"
                                   value="{{$cattleInfo['cattle_id']}}">{{isset($draftProject)? $cattleInfo['cattle_name']:$cattleInfo->cattle->name}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="number[]"
                                   value="{{ $cattleInfo['number']}}">{{ $cattleInfo['number']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="" value=""><a href="#" class="removecattle" id="cattle_info">×</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
        <tr class="create_cattle option_value">
            <td width="45%">
                <label>Name of the cattle<span class="required">*</span><span class ="sl-text">({{__('app.cattle_name')}})</span></label>
                <select class="form-control resett" id="cattle-name" name="" >
                    <option value="">Choose..</option>
                    @foreach($cattles as $cattle)
                        <option value="{{ $cattle->id }}">{{ $cattle->name }}
                            @if(\Lang::has('app.'.$cattle->name))
                                ( {{__('app.'.$cattle->name)}})
                            @endif
                        </option>
                    @endforeach
                </select>
                @if ($errors->has('cattle_id'))
                    <span class="error">
                        <strong>{{ $errors->first('cattle_id') }}</strong>
                </span>
            @endif
            <td width="45%">
                <label>Number of cattles<span class="required">*</span> <span class ="sl-text">({{__('app.number')}})</span> </label>
                <input type="number" placeholder="Number" class="form-control " id="number" name="" min = "0">
                @if ($errors->has('number'))
                    <span class="error">
                            <strong>{{ $errors->first('number') }}</strong>
                        </span>
                @endif
            </td>
            <td  style = "padding-top:40px">
                <button id="addcattle" class="btn btn-info btn-block" type = "button">Add</button>
            </td>
        </tr>
    </table>
    <div>
        <label class="cattle-error error hide">Cattle Information missing.</label>
    </div>
</div>
{{--End of Cattle--}}

{{--Start of Crop detail form--}}

<div class="col-sm-12">
    <p class="form-section-header">Crop Information <span class ="sl-text">({{__('app.crop_info')}})</span></p>
</div>
<div class="col-sm-12">
    <table class="table table-responsive table-striped">
        <thead>
        <td width="25%">Crop Name<span class ="sl-text">({{__('app.crop_name')}})</span></td>
        <td>Start Month<span class ="sl-text">({{__('app.start_month')}})</span></td>
        <td>Area<span class ="sl-text">({{__('app.area')}})</span></td>
        <td>Type of unit<span class ="sl-text">({{__('app.unit_type')}})</span></td>
        </thead>
        <tr id="nocrops">
            <td colspan="5" class="text-center no-crops">
                <h4>There are no crops right now.
                <span class ="sl-text">({{__('app.no_crop_details_available')}})</span></h4>
                <small>Add a crop from the form below.<span class ="sl-text">({{__('app.select_crops')}})</span></small>
            </td>
        </tr>
        <tbody id="croplist">
        @if(is_array(old('crop_id')) && is_array(old('start_month')) && is_array(old('area')) && is_array(old('crop_area_unit_type')))
            <?php $crop_id_array = old('crop_id'); $area = old('area'); $start_month= old('start_month'); $crop_area_unit_type = old('crop_area_unit_type')?>
            @for ($i = 0; $i < sizeof($crop_id_array);$i++)
                <tr class="crop-row">
                    <td>
                        <input type="hidden" class="form-control" name="crop_id[]" value="{{$crop_id_array[$i]}}">{{ getAttributeForModel(\App\Models\Crop::class,$crop_id_array[$i],'name')}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="start_month[]" value="{{$start_month[$i]}}">{{ $month[$start_month[$i]]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="area[]" value="{{ $area[$i]}}">{{ $area[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="crop_area_unit_type[]" value="{{ $crop_area_unit_type[$i]}}">{{ $crop_area_unit_type[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="" value=""><a href="#" class="removecrop" id="crop_info">×</a>
                    </td>
                </tr>
            @endfor
        @else
            @if(isset($cropInfoArr) && count($cropInfoArr) > 0)
                @foreach ($cropInfoArr as $key => $cropInfo)
                    <tr class="crop-row">

                        <input type="hidden" class="form-control" name="crop_info_id[]"
                               value="{{isset($cropInfo['id'])?$cropInfo['id']:null}}">
                        <td>
                            <input type="hidden" class="form-control" name="crop_id[]"
                                   value="{{$cropInfo['crop_id']}}">
                            {{isset($draftProject)?$cropInfo['crop_name']:$cropInfo->crops->name}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="start_month[]" value="{{ $cropInfo['start_month']}}">{{ $month[$cropInfo['start_month']]}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="area[]" value="{{ $cropInfo['area']}}">{{ $cropInfo['area']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="crop_area_unit_type[]" value="{{ isset($cropInfo['crop_area_unit_type'])? $cropInfo['crop_area_unit_type']:'hectare'}}">{{isset($cropInfo['crop_area_unit_type'])?$cropInfo['crop_area_unit_type']:'hectare'}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="" value=""><a href="#" class="removecrop" id="crop_info">×</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
        <tr class="create_crop option_value">
            <td style="width:20%">
                <label>Name of the Crop <span class="required">*</span><br><span class ="sl-text">({{__('app.crop_name')}})</span></label>
                <select class="form-control resett" id="crop-name" name="" >
                    <option value="">Choose..</option>
                    @foreach($crops as $crop)
                        <option value="{{ $crop->id }}">{{ $crop->name }}
                            @if(\Lang::has('app.'.$crop->name))
                               ( {{__('app.'.$crop->name)}})
                            @endif
                        </option>
                    @endforeach
                </select>
                @if ($errors->has('crop_id'))
                    <span class="error">
                        <strong>{{ $errors->first('crop_id') }}</strong>
                    </span>
            @endif
            <td style="width:20%">
                <label>Start Month<span class="required">*</span><br><span class ="sl-text">({{__('app.start_month')}})</span></label>
                <select class="form-control resett" id="start-month" name="">
                    <option value ="">Choose..</option>
                    <option value="1">Jan</option>
                    <option value="2">Feb</option>
                    <option value="3">Mar</option>
                    <option value="4">Apr</option>
                    <option value="5">May</option>
                    <option value="6">Jun</option>
                    <option value="7">Jul</option>
                    <option value="8">Aug</option>
                    <option value="9">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
                </select>
                @if ($errors->has('start_month'))
                    <span class="error">
                        <strong>{{ $errors->first('start_month') }}</strong>
                    </span>
                @endif

            </td>
            <td style="width:20%">
                <label>Area<span class="required">*</span><br><span class ="sl-text">({{__('app.crop_area')}})</span></label>
                <input type="number" placeholder="Area" class="form-control resett" id="area" name="" value="1" min="0.1"
                       step="0.1">
                @if ($errors->has('area'))
                    <span class="error">
                            <strong>{{ $errors->first('area') }}</strong>
                        </span>
                @endif
            </td>
            <td style="width:20%">
                <label>Type of unit <span class="required">*</span><br><span class ="sl-text">({{__('app.unit_type')}})</span> </label>
                <select class="form-control resett" id="crop-area-unit-type" name="">
                    <option value ="">Choose..</option>
                    <option value="ropani">Ropani<span class ="sl-text">({{__('app.ropani')}})</span></option>
                    <option value="katha">Katha<span class ="sl-text">({{__('app.katha')}})</span></option>
                    <option value="bigaha">Bigaha<span class ="sl-text">({{__('app.bigaha')}})</span></option>
                </select>
                @if ($errors->has('crop_area_unit_type'))
                    <span class="error">
                        <strong>{{ $errors->first('crop_area_unit_type') }}</strong>
                    </span>
                @endif
            </td>

            <td>
                <button id="addcrop" class="btn btn-info btn-block" style="margin-top: 40px">Add</button>
            </td>
        </tr>
    </table>
    <div>
        <label class="crop-error error hide">Crop Information missing.</label>
    </div>
</div>
{{--End of Crop detail form--}}
{{--Start of farms--}}
<div class="col-sm-12">
    <p class="form-section-header">Land Detail Information<span class ="sl-text">({{__('app.land_detail_information')}})</span></p>
</div>
<div class="col-sm-12">
    <table class="table table-responsive table-striped">
        <thead>
        <td width="15%">Type of land<span class ="sl-text">({{__('app.land_type')}})</span></td>
        <td>Ownership<span class ="sl-text">({{__('app.ownership')}})</span></td>
        <td>Owner<span class ="sl-text">({{__('app.owner')}})</span></td>
        <td>Area<span class ="sl-text">({{__('app.area')}})</span></td>
        <td>Type of Unit<span class ="sl-text">({{__('app.unit_type')}})</span></td>
        </thead>
        <tr id="nolands">
            <td colspan="9" class="text-center no-lands">
                <h4>There are no land information right now.<span class ="sl-text">({{__('app.no_land_details_available')}})</span></h4>
                <small>Add a land area from the form below.<span class ="sl-text">({{__('app.select_lands')}})</span></small>
            </td>
        </tr>
        <tbody id="landlist">
        @if(is_array(old('land_type')) && is_array(old('ownership')) && is_array(old('owner')) && is_array(old('land_area')))
            <?php $land_type = old('land_type'); $ownership = old('ownership'); $owner= old('owner'); $land_area = old('land_area'); $unit_type = old('unit_type') ?>
            @for ($i = 0; $i < sizeof($land_type);$i++)
                <tr class="crop-row">
                    <td>
                        <input type="hidden" class="form-control" name="land_type[]" value="{{$land_type[$i]}}">{{$land_type[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="ownership[]" value="{{$ownership[$i]}}">{{ $ownership[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="owner[]" value="{{ $owner[$i]}}">{{ $owner[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="land_area[]" value="{{ $land_area[$i]}}">{{ $land_area[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="unit_type[]" value="{{ $unit_type[$i]}}">{{$unit_type[$i]}}
                    </td>
                    <td>
                        <input type="hidden" class="form-control" name="land" value=""><a href="#" class="removeland" id="land">×</a>
                    </td>
                </tr>
            @endfor
        @else
            @if(isset($land_array) && count($land_array) > 0)
                @foreach ($land_array as $key => $land)
                    <tr class="crop-row">
                        <td>
                            <input type="hidden" class="form-control" name="land_type[]" value="{{$land['land_type']}}">{{$land['land_type']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="ownership[]" value="{{ $land['ownership']}}">{{$land['ownership']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="owner[]" value="{{ $land['owner']}}">{{$land['owner']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="land_area[]" value="{{ $land['area']}}">{{$land['area']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="unit_type[]" value="{{ $land['unit_type']}}">{{$land['unit_type']}}
                        </td>
                        <td>
                            <input type="hidden" class="form-control" name="" value=""><a href="#" class="removeland" id="land">×</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
        <tr class="create_land option_value create_crop">
            <td width="15%">
                <label>Type of land<span class="required">*</span> <span class ="sl-text">({{__('app.land_type')}})</span></label>
                <select class="form-control resett" id="land-type" name="" >
                    <option value="">Choose..</option>
                    <option value="dry">Dry Land</option>
                    <option value="wet">Wet Land</option>
                </select>
                @if ($errors->has('land_type'))
                    <span class="error">
                        <strong>{{ $errors->first('land_type') }}</strong>
                    </span>
                @endif
            </td>

            <td width="15%">
                <label>Ownership <span class="required">*</span><span class ="sl-text">({{__('app.ownership')}})</span></label>
                <select class="form-control resett" id="ownership" name="">
                    <option value ="">Choose..</option>
                    <option value="owned">Owned({{__('app.owned')}})</option>
                    <option value="leased">Leased({{__('app.leased')}})</option>
                </select>
                @if ($errors->has('ownership'))
                    <span class="error">
                        <strong>{{ $errors->first('ownership') }}</strong>
                    </span>
                @endif
            </td>

            <td width="18%">
                <label>owner<span class="required">*</span><span class ="sl-text">({{__('app.owner')}})</span></label>
                <input type="text" placeholder="name" class="form-control resett" id="owner" name="">
                @if ($errors->has('owner'))
                    <span class="error">
                        <strong>{{ $errors->first('owner') }}</strong>
                    </span>
                @endif
            </td>

            <td width="18%">
                <label>Area <span class="required">*</span><span class ="sl-text">({{__('app.area')}})</span></label>
                <input type="number" placeholder="Area" class="form-control resett" id="land-area" name="" value="1" min="0.1"
                       step="0.1">
                @if ($errors->has('land_area'))
                    <span class="error">
                            <strong>{{ $errors->first('land_area') }}</strong>
                        </span>
                @endif
            </td>
            <td width="15%">
                <label>Type of unit <span class="required">*</span><span class ="sl-text">({{__('app.unit_type')}})</span> </label>
                <select class="form-control resett" id="unit-type" name="">
                    <option value ="">Choose..</option>
                    <option value="ropani">Ropani<span class ="sl-text">({{__('app.ropani')}})</span></option>
                    <option value="katha">Katha<span class ="sl-text">({{__('app.katha')}})</span></option>
                    <option value="bigaha">Bigaha<span class ="sl-text">({{__('app.bigaha')}})</span></option>
                </select>
                @if ($errors->has('unit_type'))
                    <span class="error">
                        <strong>{{ $errors->first('unit_type') }}</strong>
                    </span>
                @endif
            </td>
            <td>
                <button id="addlands" class="btn btn-info btn-block">Add</button>
            </td>
        </tr>
    </table>
    <div>
        <label class="land-error error hide">Crop Information missing.</label>
    </div>
</div>
{{--End of farms--}}