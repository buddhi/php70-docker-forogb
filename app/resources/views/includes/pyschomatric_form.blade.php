
{{--Psycometric Question Section--}}
<div class="row">
    <div class="col-md-12">
        <h4>Psychometric Questions</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="size_fish">Would you sell small fish in half year or large fish in one year ?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="size_fish" id = "size_fish">

            <option  value="1" {!! old('size_fish', isset($farmer[0]) ? $farmer[0]['size_fish'] : "")== 1? "selected":"" !!} >Large</option>
            <option value="0" {!! old('size_fish', isset($farmer[0]) ? $farmer[0]['size_fish'] : "")== 0? "selected":"" !!}>Small</option>
        </select>
        @if ($errors->has('size_fish'))
            <span class="error">
                <strong>{{ $errors->first('size_fish') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="neighbour_trust">What percent of neighbour will trust doing business with you ?
            <span class="required">*</span>
        </label>
        <input type="number" placeholder="What percent of neighbour will trust doing business with you ?" class="form-control" id="neighbour_trust" name="neighbour_trust" min = "0" max = "100" value ="{{ old('neighbour_trust', isset($farmer) ? $farmer[0]['neighbour_trust'] : "") }}" >
        @if ($errors->has('neighbour_trust'))
            <span class="error">
                <strong>{{ $errors->first('neighbour_trust') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="harvest_lost">If you lost a harvest for how long would you be able to feed your family ?
            <span class="required">*</span>
        </label>
        <input type="number" placeholder="If you lost a harvest for how long would you be able to feed your family?" class="form-control" id="harvest_lost" name="harvest_lost" min = "0" max = "10"  value ="{{ old('harvest_lost', isset($farmer) ? $farmer[0]['harvest_lost'] : "") }}">
        @if ($errors->has('harvest_lost'))
            <span class="error">
                <strong>{{ $errors->first('harvest_lost') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="village_meeting">If you have to meet someone in the village, are you usually early or late ?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="village_meeting" id = "village_meeting">
            <option  value="1" {!! old('village_meeting', isset($farmer[0]) ? $farmer[0]['village_meeting'] : "")== 1? "selected":"" !!} >Early</option>
            <option value="0" {!! old('village_meeting', isset($farmer[0]) ? $farmer[0]['village_meeting'] : "")== 0? "selected":"" !!}>Late</option>
        </select>
        @if ($errors->has('village_meeting'))
            <span class="error">
                <strong>{{ $errors->first('village_meeting') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="inline-label required" for="image_recall_exercise">Did farmer cheat on image recall exercise?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="image_recall_exercise" id = "image_recall_exercise">
            <option  value="1" {!! old('image_recall_exercise', isset($farmer[0]) ? $farmer[0]['image_recall_exercise'] : "")== 1? "selected":"" !!} >Didnot cheat</option>
            <option value="0" {!! old('image_recall_exercise', isset($farmer[0]) ? $farmer[0]['image_recall_exercise'] : "")== 0? "selected":"" !!}>cheated</option>
        </select>
        @if ($errors->has('image_recall_exercise'))
            <span class="error">
                <strong>{{ $errors->first('image_recall_exercise') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="question_timing">Question timing (Long/short)?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="question_timing" id = "question_timing">
            <option  value="1" {!! old('question_timing', isset($farmer[0]) ? $farmer[0]['question_timing'] : "")== 1? "selected":"" !!} >long</option>
            <option value="0" {!! old('question_timing', isset($farmer[0]) ? $farmer[0]['question_timing'] : "")== 0? "selected":"" !!}>short</option>
        </select>

        @if ($errors->has('question_timing'))
            <span class="error">
                <strong>{{ $errors->first('question_timing') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-md-6">
        <label class="inline-label required" for="answered_all_questions">Did farmer answered all questions?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="answered_all_questions" id = "answered_all_questions">
            <option  value="1" {!! old('answered_all_questions', isset($farmer[0]) ? $farmer[0]['answered_all_questions'] : "")== 1? "selected":"" !!} >Yes</option>
            <option value="0" {!! old('answered_all_questions', isset($farmer[0]) ? $farmer[0]['answered_all_questions'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('answered_all_questions'))
            <span class="error">
                <strong>{{ $errors->first('answered_all_questions') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6">
        <label class="inline-label required" for="equivocating_question">Equivocating question?
            <span class="required">*</span>
        </label>
        <select class="form-control" name="equivocating_question" id = "equivocating_question">
            <option  value="1" {!! old('equivocating_question', isset($farmer[0]) ? $farmer[0]['equivocating_question'] : "")== 1? "selected":"" !!} >Yes</option>
            <option value="0" {!! old('equivocating_question', isset($farmer[0]) ? $farmer[0]['equivocating_question'] : "")== 0? "selected":"" !!}>No</option>
        </select>
        @if ($errors->has('equivocating_question'))
            <span class="error">
                <strong>{{ $errors->first('equivocating_question') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <label class="inline-label required" for="farming_skill">Rate your farming skill from 0 - 100 percent ?
            <span class="required">*</span>
        </label>
        <input type="number" placeholder="Rate your farming skill from 0 - 100 percent ?" class="form-control" id="farming_skill" name="farming_skill" min = "0" max = "100" value ="{{ old('farming_skill', isset($farmer) ? $farmer[0]['farming_skill'] : "") }}" >
        @if ($errors->has('farming_skill'))
            <span class="error">
                <strong>{{ $errors->first('farming_skill') }}</strong>
            </span>
        @endif
    </div>
</div>