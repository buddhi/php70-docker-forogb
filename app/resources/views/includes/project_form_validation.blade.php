<script>
    $("#project-form").validate({
        rules: {
            "farmer_name": {
                required: true,
            },
            "address": {
                required: true,
            },
            "project_type_id": {
                required: true,
            },
            "province_id": {
                required: true,
            },
            "contact_no": {
                required: true,
            },
            "district_id": {
                required: true,
            },
            "municipality_id":{
                required: true,
            },
            "ward_no":{
                required: true,
            },
            "current_irrigation_source":{
                required: true,
            },
            "current_irrigation_system":{
                required: true,
            },
            "boring_size":{
                required: true,
            },
            "distance":{
                required: true,
            },
            "gender":{
                required: true,
            },
            "water_distribution_to_farmers":{
                required: true,
            },
            "boring_depth":{
                required: true,
            },
            "ground_water_level":{
                required: true,
            },
            "crop_inventory":{
                required: true,
            },
            "education":{
                required: true,
            },
            "certification":{
                required: true,
            },
            "no_of_people_in_house":{
                required: true,
            },

            "fuel_monthly_expense":{
                required: true,
            },
            "grid_monthly_expense":{
                required: true,
            },
            "expected_solar_loan_monthly_payment":{
                required: true,
            },
            "debt_service_obligation":{
                required: true,
            },
            "annual_household_income":{
                required: true,
            },
            "annual_expenses":{
                required: true,
            },
            "no_of_dependents":{
                required: true,
            },
            "nearest_market":{
                required: true,
            },
            "distance_to_nearest_market":{
                required: true,
            },

        },
        onkeyup: function (element) {
            $(element).valid();
        },
        onfocusout: function (element) {
            $(element).valid();
        }
    });
</script>