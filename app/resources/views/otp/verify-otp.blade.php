<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/ogbadmin/style.css')}}"/>
    <link href="https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('js/ogbadmin/bootstrap.min.js')}}/"></script>
    <title>Verify Otp</title>
</head>
<style>
    .error{
        color: red;
    }
</style>
<body>


<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar mb-4 px-4 border-bottom">
    <div class="container">
        <a class="navbar-brand mr-5" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" height="40"></a>
    </div>
</header>

<div class="container-fluid">
    <div class="row">
        <main class="col-12 col-md-12 col-xl-12 bd-content">

            <div class="box-wrap verify-mobile-number">
                <h3 class="text-center mb-4">Verify Mobile Numbers</h3>
                @include('common.otp-error-message')
                <div class="alert alert-danger text-center" role="alert" class="flash-message" style="display:none" id="otp-expire">
                    <span class="text-danger"> Otp has expired</span>
                </div>
                <p class="text-center">An 6-digit OTP code has been send to <strong>{{$user->phone}}</strong> <a
                            href="#" data-toggle="modal"
                            data-target="#change-mobile" >change</a>

                </p>
                <form class="form-row" action="{{route('verify-otp',$user->user_id)}}" method = "post">
                    {{csrf_field()}}
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit1" id = "digit1" data-id="1">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit2" id ="digit2" data-id="2">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit3" id ="digit3" data-id="3">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit4" id ="digit4" data-id="4">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit5" id ="digit5" data-id="5">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control inputs-otp" placeholder="-" maxlength="1" name ="digit6"  id ="digit6" data-id="6">
                        </div>
                    </div>

                    <div class="col ">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Verify</button>
                        </div>
                    </div>

                </form>
                <ul class="mb-0">
                    <li id="otp-time">OTP will expire in  <span id="countTime"></span></li>
                    <li>Didn't receive the code?
                        <a href="{{route('resend-otp' ,$user->user_id)}}">Resend</a>
                </ul>
            </div>
        </main>
    </div>
</div>
@include('ogbadmin.change-mobile')
<script src="{{asset('js/ogbadmin/jquery.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/popper.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/jquery.sticky-kit.min.js')}}"></script>
<script src="{{asset('js/ogbadmin/ogbadmin.js')}}"></script>
@include('common.otp-expire')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>

</body>
</html>


