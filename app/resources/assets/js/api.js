export default {
  getProjects() {
    return axios.get('/all').then(res => res.data)
  },
  
  getCashflowForProject(projectId) {
    return axios.get('/cashflow/${projectId}').then(res => res.data)
  },

  getMyProjects(){
    return axios.get('/my-investment').then(res => res.data)
  },

  getLatestProjects(){
    return axios.get('/latest').then(res => res.data)

  },
}
