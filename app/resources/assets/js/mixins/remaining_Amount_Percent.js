export default {
  methods: {
    percent(project) {
      if (
        project.investment.investment_percent !== null ||
        project.investment.investment_percent < 100
      ) {
        return 100 - project.investment.investment_percent;
      }
      return 100;
    }
  },
  computed: {
    getRequiredAmount() {
      if (this.project.investment.investment_amount != null) {
        return this.project.cost - this.project.investment.investment_amount;
      }
      return this.project.cost;
    }
  }
};
