import projectHelper from "../helpers/project";
export default {
    computed: {
        bankabilityScore:function() {
            return projectHelper.creditScoreToBankability(
              parseFloat(this.project.farmers[0].credit_score)
            );
          },
          bankabilityImage:function() {
            const num = Math.round(this.bankabilityScore);
            if (num == 0) return 1;
            return num;
          },
    }
}