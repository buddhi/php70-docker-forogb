export default {
    computed: {
        title:function() {
            return `Help ${this.project.farmer_name} install solar water pump in ${
              this.project.district.name
            } so that ${
              this.gender == "Female" ? "she" : "he"
            }  can grow crops to support ${
              this.gender == "Female" ? "her" : "his"
            }  family.`;
          },
          gender:function() {
            if (this.project.farmers[0].gender === null) {
              return null;
            } else {
              return (
                this.project.farmers[0].gender.charAt(0).toUpperCase() +
                this.project.farmers[0].gender.slice(1)
              );
            }
          }        
    }
}