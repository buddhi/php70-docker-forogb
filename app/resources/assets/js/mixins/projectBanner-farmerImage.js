export default {
    computed: {
        getBannerImageForProject: function () {
            let max = 14; //max: Maximum number of stock image available
            return `/img/solar_farm_${this.project.id % max}.jpg`
        },
        getImg:function() {
            let farmerImage = this.project.farmers[0].farmer_image;
            if (farmerImage === null || this.notLoggedIn) {
                return "/img/farmer_default.png";
            }
            return farmerImage;
        }
    }
}