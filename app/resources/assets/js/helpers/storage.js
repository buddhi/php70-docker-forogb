/* global sessionStorage */
const initialState = {
  selectedProjects: [],
  timestamp: null
};

const storage = {
  state: Object.assign({}, initialState),

  addProject({
    projectId,
    farmerName,
    amount,
    municipality,
    province,
    bankabilityScore,
    cost,
    investment,
    districtName,
    gender
  }) {
    const prevCommitment = this.state.selectedProjects.findIndex(
      el => el.id === projectId
    );
    if (prevCommitment !== -1) {
      this.state.selectedProjects.splice(prevCommitment, 1);
    }
    this.state.selectedProjects.push({
      id: projectId,
      amount,
      farmerName,
      municipality,
      province,
      bankabilityScore,
      cost,
      investment,
      districtName,
      gender
    });
    this.save();
  },

  init() {
    try {
      const oldState = JSON.parse(sessionStorage.getItem("ogb_state"));
      if (!oldState) return;

      const diff = new Date().getTime() - oldState.timestamp;
      const min20 = 20 * 60 * 60 * 100;
      if (diff < min20) {
        // 20 minutes
        this.state = oldState;
      }
    } catch (e) {
      // ignore error
    }
  },

  removeProject(projectId) {
    const project = this.findProjectForId(projectId);
    if (project === -1) return;

    this.state.selectedProjects.splice(project, 1);
    this.save();
  },

  removeAllProjects() {
    sessionStorage.setItem("ogb_state", []);
    Object.assign(this.state, initialState);
  },

  updateTimestamp() {
    this.state.timestamp = new Date().getTime();
  },

  save() {
    this.updateTimestamp();
    sessionStorage.setItem("ogb_state", JSON.stringify(this.state));
  },

  updateCommitment(projectId, percentage) {
    const prevCommitment = this.findProjectForId(projectId);
    if (prevCommitment === -1) return;
    this.state.selectedProjects[prevCommitment].percentage = percentage;
    this.save();
  },
  updateAmount(projectId, amount) {
    const prevCommitment = this.findProjectForId(projectId);
    if (prevCommitment === -1) return;
    this.state.selectedProjects[prevCommitment].amount = amount;
    this.save();
  },

  findProjectForId(projectId) {
    return this.state.selectedProjects.findIndex(el => el.id === projectId);
  },

  get numberOfSelectedProjects() {
    return this.state.selectedProjects.length;
  },

  get projects() {
    return this.state.selectedProjects;
  }
};

storage.init();

export default storage;
