
/*
KW or kw mean KiloWatt
Note::All constants are given on basis of KW unit. so system_size_in_KW variable must be in KW.
TODO FUTURE :: Define units of attributes in config/Unit.php
*/

const AVERAGE_DAILY_SUN_HOURS = 5 // Hours
const NO_OF_DAYS_IN_YEAR = 365
const SYSTEM_UTILIZATION_RATE = 70 // percent
const DIESEL_REQUIRED_PER_KWH = 0.51 // L/KWh
const CO2_EMISSION_PER_LITER_DIESEL_CONSUMPTION = 2.68 // KG/L

export default {
  calculateEnvParams (systemSize) {
    const systemSizeInKw =  systemSize/1000; //unit:Kw
    const DailyEnergyProduction = systemSizeInKw * AVERAGE_DAILY_SUN_HOURS // unit:kWh
    const annualEnergyProduction = DailyEnergyProduction * NO_OF_DAYS_IN_YEAR // kWh
    const annualEnergyUsage = annualEnergyProduction * SYSTEM_UTILIZATION_RATE / 100 // kWh
    const dieselDisplacedinL = annualEnergyUsage * DIESEL_REQUIRED_PER_KWH // Liters / kWh
    const dieselDisplaced = dieselDisplacedinL / 1000

    const co2Curbedinkg = dieselDisplacedinL * CO2_EMISSION_PER_LITER_DIESEL_CONSUMPTION
    const co2Curbed = co2Curbedinkg / 1000 // unit :ton , 1KG =1/1000 ton

    return { dieselDisplaced, co2Curbed }
  },

  // credit score - between 0 and 1
  // 1 is risky and 0 is not risky
  // bankability score - 1 -> 5
  creditScoreToBankability (creditScore) {
    return 6.25 - (creditScore * 5)
  }
}
