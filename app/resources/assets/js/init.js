$(document).ready(function () {
  $('[data-toggle="popover"]').popover()

  // social signup
  if ($('.switch-signup')) {
    $('.switch-signup').on('click', function (e) {
      e.preventDefault()
      var item = $(this).attr('rel')
      $('.signup-section').addClass('collapse')
      $('#' + item).removeClass('collapse')
    })
  }

  // activete this is we are in the investmentDetailPage
  // dirty way of making sure we are on the right page
  if ($('#investmentDetailPage')) {
    if ($(window).width() >= 768) {
      $('#investmentbox').stick_in_parent()
    }

    if ($(window).width() < 768) {
      $('.mobile-trigger button').on('click', function () {
        $('.mobile-trigger').slideUp()
        $('.mobile-body, .mobile-cancel').slideDown()
      })

      $('.mobile-close-trigger').on('click', function () {
        $('.mobile-trigger').slideDown()
        $('.mobile-body, .mobile-cancel').slideUp()
      })
    }

    $('.toggle_supporters').on('click', function (e) {
      e.preventDefault()
      $('.hidden_until_clicked').fadeIn()
      $(this)
        .parent()
        .hide()
    })
  }
})
