$(document).ready(function () {
    //popup for email input for resend verification
    $('#resend-verification').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.form-group input').val(recipient);
    });

    //resend verification email ajax request
    $(".resend-verification-email").on("click", function (e) {
        e.preventDefault();
        var formData = {
            email: $('#verify_email').val(),
        };
        $.ajax({
            type: "POST",
            url: 'partner/send-verification',
            data: formData,
            dataType: 'json',
            success: function (response) {
                if (!response.error) {
                    $("#resend-verification").modal("hide");
                    $("#verification-email-modal").modal("show");
                  $("#email-text").css({ 'font-weight': 'bold' }).append(formData.email);
                }
            },
        });

    });

    //adding partner via ajax request
    $("#add-partner-error-bag").hide();
    $(".verification-email").on("click", function (e) {
        e.preventDefault();
        var formData = {
            name: $('#name').val(),
            email: $('#email').val(),
            contact_person: $('#contact_person').val(),
        }
        $.ajax({
            type: "POST",
            url: '/ogbadmin/partner/store',
            data: formData,
            dataType: 'json',
            success: function (response) {
                if (!response.error) {
                    $("#partner-verify").modal("hide");
                    $("#add_partner").trigger("reset");
                    $("#verification-email-modal").modal("show");
                    $("#email-text").css({ 'font-weight': 'bold' }).append(formData.email);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-form-errors').html('');
                $.each(errors.messages, function (key, value) {
                    $('#add-form-errors').append('<li>' + value + '</li>');
                });
                $("#add-partner-error-bag").show();
            }
        });

    });

    //Closing the verification email modal to reload the page to fetch the latest records
    $('#verification-email-modal').on('hide.bs.modal', function (e) {
        location.reload()
        $("#add-partner-error-bag").hide();
    });

    $('body').on('click', '.toggle-partner', function (e) {
        var result = confirm("Do you want to change the partner status?");
        if(!result){
            e.preventDefault();
            return;
        }
        else{
            var $this = $(this);
            $.ajax({
                method: 'POST',
                url: '/ogbadmin/partner/change-status',
                data: {
                    partner_id: $this.attr('attr'),
                },
                success: function (response) {
                    if(!response.error){
                        alert(response.messages);
                        $($this).closest('td').prev('td').html('<div class="status '+ response.partner_status.toLowerCase() +'">'+ response.partner_status+'</div>');
                    }
                }
            });
        }
    });

    $('.image-change').click(function(){
        $('#file').click();
    });
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload($(this));
        }
    });

    function showPopUp(message,success=true) {
        //            console.log("the message is "+message);

        if (message) {
            if(success){
                document.querySelector("#pop-message").classList.remove('error-popup');
                document.querySelector("#pop-message").classList.add('success-popup')
            }else{
                document.querySelector("#pop-message").classList.remove('success-popup');
                document.querySelector("#pop-message").classList.add('error-popup')
            }
            document.querySelector("#pop-message").classList.add('popup-appear');
            document.getElementById("pop-message").innerHTML = message;
            setTimeout(function () {
                document.querySelector("#pop-message").classList.remove('popup-appear');
            }, 1500);
        }
    }

    function upload(img) {
        var form_data = new FormData();
        form_data.append('partner_id', img.data('id'));
        form_data.append('file', img[0].files[0]);
        if (!(/\.(jpe?g|png|pdf|)$/i.test(img[0].files[0].name))) {
            showPopUp('Please upload files with extension jpg,jpeg,png',false);
            return false;
        }
        if(img[0].files[0].size>5242880){
            showPopUp("Please upload files with size less than 5 MB",false);
            return false;
        }
        $.ajax({
            url: "/ogbadmin/partner/change-logo",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (response) {
                console.log(response);
                // alert(response.messages)
                showPopUp(response.messages);
                if (!response.error) {
                    $('#preview_image').attr('src', '/storage/upload/'+response.image);
                }
            }
        });
    }

    $(function () {
        $('[data-toggle="toggletip"]').tooltip({
            trigger : 'hover',
        }).on('show.bs.tooltip', function(e) {
            var isChecked = $(this).find('input').prop('checked');
            $(this).attr('data-original-title', isChecked? "Disable Partner": "Enable Partner");
        });
    });
});