showhideCattleBox();
showhideSellCrops();

$(function () {
    $('input[name=\'has_cattle\']').click(function () {
        showhideCattleBox();
    })
})
function showhideCattleBox(){
    if ($('#yes').is(':checked')) {
        $('#cattle_info').show()
    } else
    {
        // document.querySelectorAll('.add-cattle').forEach((inp)=>console.log(inp));
        $('#cattle_info').hide()
    }
}
function showhideSellCrops(){
    if ($('#true').is(':checked')) {
        $('#market_distance').show()
    } else {
        $('#market_distance').hide()
    }
}
$(function () {
    $('input[name=\'sell_crops\']').click(function () {
        showhideSellCrops();
    })
})

window.addRowTotheTable=function(element){
    let btn = element;
    let tableRef=btn.parentElement.querySelector('table');
    let newRow =tableRef.insertRow(-1);
    newRow.innerHTML= tableRef.querySelector('tbody>tr:nth-last-child(2)').innerHTML;
    tableRef.querySelectorAll('tbody>tr:nth-last-child(2)>input,tbody>tr:nth-last-child(2)>select').forEach(i=>i.value='');
    newRow.querySelectorAll('input,select').forEach(inp=>inp.value='');
    newRow.querySelectorAll('input,select').forEach((input)=>input.addEventListener('focusout',function (e){
        focusOutForms(e.target);
    }));
    newRow.querySelectorAll('input').forEach(inp=>inp.addEventListener('keyup', function (e){
        console.log(e.key);
        if(e.key=='Enter'){
            console.log(btn);
            btn.click();
        }
    }))
    console.log(tableRef.querySelector('tbody>tr:nth-last-child(2)'));
    return false;
}

$(document).ready(function () {
    $('body').on('click', '.remove_income', function () {
//          var lmnt = $(this)
        var parentTag = $(this).parent().get(0)
        var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
        console.log(trs.length)

        if (trs.length > 1) {
            $parentTR = $(this).closest('tr')
            $parentTR.remove()
        }
        return false
    })
})

$(document).ready(function () {

    $('body').on('click', '.remove_expense', function () {
        var parentTag = $(this).parent().get(0)
        var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
        console.log(trs.length)

        if (trs.length > 1) {
            $parentTR = $(this).closest('tr')
            $parentTR.remove()
        }
        return false

    })
})

$(document).ready(function () {
  /*      $('body').on('click', '.add_land', function () {
   var $this = $(this),

   //            $parentTR = $this.closest('tr')
   $parentTR.clone().insertAfter($parentTR).find('input').val('')

   return false
   })*/

    $('body').on('click', '.remove_land', function () {
        var parentTag = $(this).parent().get(0)
        var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
        console.log(trs.length)

        if (trs.length > 1) {
            $parentTR = $(this).closest('tr')
            $parentTR.remove()
        }
        return false
    })
})

$(document).ready(function () {
  /*$('body').on('click', '.add_crop', function () {
   var $this = $(this),
   $parentTR = $this.closest('tr')
   $parentTR.clone().insertAfter($parentTR).find('input').val('')

   return false
   })*/

    $('body').on('click', '.remove_crop', function () {
        var parentTag = $(this).parent().get(0)
        var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
        console.log(trs.length)

        if (trs.length > 1) {
            $parentTR = $(this).closest('tr')
            $parentTR.remove()
        }
        return false
    })
})


$(document).ready(function () {

    $('body').on('click', '.remove_cattles', function () {
        var parentTag = $(this).parent().get(0)
        var trs = parentTag.parentNode.parentNode.getElementsByTagName('tr')
        console.log(trs.length)

        if (trs.length > 1) {
            $parentTR = $(this).closest('tr')
            $parentTR.remove()
        }
        return false
    })
})
