import Vue from 'vue'
import axios from 'axios'

var app = new Vue({
    el: '#project-create-app',
    data: {
        districts: [],
        municipalities: [],
        selectedDistrict: {},
        selectedMunicipality: {},
    },
    methods: {
        getDistrictsAndMunicipalitiesForEditPage(){
            // console.log({p_iddisc_id,munc_id})
            let p_id=document.querySelector('#project-create-app').getAttribute('proid');
            let disc_id=document.querySelector('#project-create-app').getAttribute('distid');
            let munc_id=document.querySelector('#project-create-app').getAttribute('muncid');
            if(p_id&&disc_id&&munc_id){
            axios.get('/ogbadmin/project/get_districts/' + p_id)
                .then(response => {
                this.districts = response.data;
                }).catch(error => {
                    this.errored = true
                });
            axios.get('/ogbadmin/project/get_municipalities/' + disc_id)
                        .then(response => {
                        this.municipalities = response.data;
            }).catch(error => {
                        console.log(error)
                    this.errored = true
                });
            }
        },
        getAllDistricts (event) {
            this.districts = [];
            this.municipalities=[];
            const province_id = event.target.value
            axios.get('/ogbadmin/project/get_districts/' + province_id)
                .then(response => {
                this.districts = response.data
        }).catch(error => {
                console.log(error)
            this.errored = true
        });
        },
        getAllMunicipality (event) {
            this.municipalities = {};
            const municipality_id = event.target.value
            axios.get('/ogbadmin/project/get_municipalities/' + municipality_id)
                .then(response => {
                this.municipalities = response.data
            console.log('here is the municipality', this.municipalities)
        })
        .
            catch(error => {
                console.log(error)
            this.errored = true
        });
        }
    },
    computed: {
        districtMessage () {
            return this.districts.length > 0 ? 'Select District' : 'Please Select Province First'
        },

        municipalityMessage () {
            return this.municipalities.length > 0 ? 'Select Local Body' : 'Please Select District First'
        }
    },
    created:function (){
        // console.log('asdasdasd')

        this.getDistrictsAndMunicipalitiesForEditPage();
    },
})
