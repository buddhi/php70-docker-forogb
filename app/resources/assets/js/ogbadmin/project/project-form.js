let forms = document.querySelectorAll('.form')
let steps = document.querySelectorAll('.step')
let progressBar = document.querySelectorAll('.progress-bar')
var index, bars = index = 0
displayForm()
$('#draft').click(function (e) {
  e.preventDefault()
})

let rules = {
  farmer_name: {
    value: '',
    valid: false
  },
  address: {
    value: '',
    valid: false
  },
  age: {
    value: '',
    isNumber: true,
    minLength: 12,
    maxLength: 100,
    valid: false
  },
  gender: {
    value: '',
    valid: false
  },
  contact_no: {
    value: '',
    isNumber: true,
    valid: false
  },
  email: {
    value: 'Not Empty',
    valid: false
  },
  province_id: {
    value: '',
    valid: false
  },
  district_id: {
    value: '',
    valid: false
  },
  municipality_id: {
    value: '',
    valid: false
  },
  ward_no: {
    value: '',
    valid: false,
    minLength:1//changes
  },
  village_name: {
    value: '',
    valid: false
  },
  no_of_people_in_house: {
    value: '',
    valid: false,
    minLength:0//changes
  },
  no_of_dependents: {
    value: '',
    valid: false,
    minLength:0//changes
  },
  annual_household_income: {
    value: '',
    valid: false,
    minLength:0//changes
  },
  education: {
    value: '',
    valid: false
  },
  certification: {
    value: '',
    valid: false
  },
  name: {
    value: '',
    valid: false
  },
  earning_members: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0,//changes
  },
  non_earning_members: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0,//changes
  },
  educational_level: {
    value: '',
    valid: false
  },
  expense_source: {
    value: '',
    valid: false
  },
  expense_amount: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0//changes
  },
  income_source: {
    value: '',
    valid: false
  },
  income_amount: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0//changes
  },
  project_type_id: {
    value: '',
    valid: false
  },
  current_irrigation_system: {
    value: '',
    valid: false
  },
  current_irrigation_source: {
    value: '',
    valid: false
  },
  grid_monthly_expense: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0 //changes
  },
  boring_size: {
    value: '',
    isNumber: true,
    valid: false,
    minValue:0 //changes
  },
  land_type: {
    value: '',
    valid: false
  },
  ownership: {
    value: '',
    valid: false
  },
  land_area: {
    value: '',
    valid: false,
    minValue:0 //changes
  },
  unit_type: {
    value: '',
    valid: false
  },
  crop_id: {
    value: '',
    valid: false
  },
  area: {
    value: '',
    valid: false,
    minValue:0 //changes
  },
  crop_area_unit_type: {
    value: '',
    valid: false
  },
  cattle_id: {
    value: 'Not empty',
    valid: true
  },
  number: {
    value: 'Not empty',
    valid: true,
    minValue:0 //changes
  },
  distance_to_nearest_market: {
    value: 'Not empty',
    isNumber: true,
    valid: false,
    minValue:0 //changes
  }
}
let message = {
  farmer_name: {
    value: 'Please Input Name and Surname '
  },
  address: {
    value: 'Please Input Address '
  },
  age: {
    value: 'Please Input Age ',
    minLength: 'Please input age more than ' + rules.age.minLength,
    maxLength: 'Please input age less than ' + rules.age.maxLength,
    isNumber: 'The field should only contain numbers '
  },
  contact_no: {
    value: 'Please Input Contact Number',
    isNumber: 'The field should only contain numbers '
  },
  email: {
    value: 'Please Input Email. '
  },
  gender: {
    value: 'Please Select Any One '
  },
  province_id: {
    value: 'Please Select Province '
  },
  district_id: {
    value: 'Please Select District '
  },
  municipality_id: {
    value: 'Please Select Local Body '
  },
  ward_no: {
    value: 'Please Input Ward Number ',
    minLength:'Ward No. cannot be in negative value' //changes
  },
  village_name: {
    value: 'Please Input Village Name '
  },
  no_of_people_in_house: {
    value: 'Please Enter Number Of Family Members'
  },
  no_of_dependents: {
    value: 'Please Enter Number Of Dependents'
  },
  annual_household_income: {
    value: 'Please Input Household Income'
  },
  education: {
    value: 'Please Select Education Level. '
  },
  certification: {
    value: 'Please select certification. '
  },
  name: {
    value: 'Please enter project name. '
  },
  earning_members: {
    value: 'Please Input Number Of Family Members ',
    isNumber: 'The field should only contain numbersssssssss ',
    minValue: 'People cannot be in negative value'//changes

  },
  non_earning_members: {
    value: 'Please Input Number Of Family Members ',
    isNumber: 'The field should only contain numbers ',
    minValue: 'People cannot be in negative value'//changes

  },
  educational_level: {
    value: 'Please Select Educational Level '
  },
  expense_source: {
    value: 'Please Input Expense Source '
  },
  expense_amount: {
    value: 'Please Input Expense Amount ',
    isNumber: 'The field should only contain numbers ',
    minValue: 'Expenses amount should be greater than 0' //changes

  },
  income_source: {
    value: 'Please Input Income Source '
  },
  income_amount: {
    value: 'Please Input Income Amount ',
    isNumber: 'The field should only contain numbers ',
    minValue: 'Income amount should be greater than 0' //changes
  },
  project_type_id: {
    value: 'Please Select Project Type '
  },
  current_irrigation_system: {
    value: 'Please Select Current Irrigation System '
  },
  current_irrigation_source: {
    value: 'Please Select Current Irrigation Source '
  },
  grid_monthly_expense: {
    value: 'Please Input Monthly Expenses Amount ',
    isNumber: 'The field should only contain numbers ',
    minValue: 'Expenses amount should be greater than 0' //changes

  },
  boring_size: {
    value: 'Please Input Boring Size',
    minValue: 'Boring size should be greater than 0' //changes
  },
  land_type: {
    value: 'Please Select Land Type '
  },
  ownership: {
    value: 'Please Select Ownership Type '
  },
  land_area: {
    value: 'Please Input Land Area ',
    minValue: 'Land area should be greater than 0' //changes
  },
  unit_type: {
    value: 'Please Select Unit Type '
  },
  crop_id: {
    value: 'Please Select Crop '
  },
  area: {
    value: 'Please Input Area ',
    minValue: 'Area should be greater than 0' //changes
  },
  crop_area_unit_type: {
    value: 'Please Select Crop Unit Type '
  },
  cattle_id: {
    value: 'Please Select Cattle '
  },
  number: {
    value: 'Please Input Cattle number ',    
    minValue: 'Cattle number should be greater than 0' //changes
  },
  distance_to_nearest_market: {
    value: 'Please Enter Distance ',
    isNumber: 'The field should only contain numbers ',    
    minValue: 'Distance should be greater than 0' //changes

  }

}
document.querySelector('#back').addEventListener('click', function (e) {
  e.preventDefault()
  prevStep()
})

document.querySelector('#next').addEventListener('click', function (e) {
  e.preventDefault()
  nextStep()
})

$('#faceButton').click(function (e) {
  // let valid = ;

  e.preventDefault()
  if (submitForm()) {
    document.querySelector('#submit').click()
  }
})
$('#saveChanges').click(function (e) {
  // let valid = submitForm()
  // e.preventDefault()

  if (submitForm()) {
    return true
  } else {
    return false
  }
})

document.querySelectorAll('input[type=text],input[type=number],select').forEach(function (input) {
  input.addEventListener('focusout', function (e) {
    focusOutForms(e.target)
  })
})
document.querySelectorAll('td>input').forEach(function (inp) {
  inp.addEventListener('keyup', function (e) {
    if (e.key == 'Enter') {
      inp.closest('table').nextElementSibling.click()
    }
  })
})
function nextStep () {
  console.log(forms[index])
  if (validateForm(forms[index])) {
    if (index >= 0 && index < forms.length - 1) {
      progressBar[index].classList.add('active-progress-bar')
    }
    index++
    index = checkIndex(index)
    displayForm()
    steps[index].classList.add('active-step')
  }
}

function prevStep () {
  if (index > 0 && index <= forms.length - 1) {
    progressBar[index - 1].classList.remove('active-progress-bar')
  }
  if (index != 0) {
    steps[index].classList.remove('active-step')
  }
  index--
  index = checkIndex(index)
  displayForm()
  console.log(index)
}

function displayForm () {
  if (index >= 0 && index < forms.length) {
    forms.forEach(function (form) {
      form.style.display = 'none'
    })
    forms[index].style.display = 'block'
    formMode()
  }
}
function checkIndex (a) {
  if (a < 0) {
    return 0
  } else if (a >= forms.length) {
    return forms.length - 1
  } else {
    return a
  }
}
function formMode () {
  let btArea = document.getElementById('buttonsArea')
  if (index == 0) {
    btArea.classList.remove('step-mode', 'submit-mode')
    btArea.classList.add('back-mode')
    btArea.parentElement.style.width = '100%'

  } else if (index == forms.length - 1) {
    btArea.classList.remove('step-mode', 'back-mode')
    btArea.classList.add('submit-mode')
    document.getElementById('saveChanges').disabled = false
    let editFormMode=document.querySelector('.edit-project-form')
    btArea.parentElement.style.width = editFormMode?'100%':'78%';
  } else {
    btArea.classList.remove('back-mode', 'submit-mode')
    btArea.classList.add('step-mode')
    btArea.parentElement.style.width = '100%'

  }
}
function submitForm () {
  let valid = validateForm(forms[forms.length - 1])
  return valid
}
function validateForm (form) {
  let valid = true
  let inputs = form.querySelectorAll('input[type=text],input[type=number],select')
  console.log(inputs)
  let errors = 0
  inputs.forEach(function (input) {
    valid = focusOutForms(input)
    if (!valid) {
      errors++
    }
  })
  //        let validity= (Object.values(rules)).filter(function (rule){ return !rule.valid});
  return errors == 0
}
window.focusOutForms =function(input) {
  let rule = input.getAttribute('name')
  rule = rule.includes('[]') ? rule.slice(0, rule.length - 2) : rule
  let error = validateInput(rule, input.value ? input.value : '')
  if (error !== '') {
    input.classList.add('form-error')
    if (input.nextElementSibling) {
      input.nextElementSibling.innerText = error
    }
    return false
  } else {
    input.classList.remove('form-error')
    if (input.nextElementSibling) {
      input.nextElementSibling.innerText = ''
    }
    rules[rule].valid = true
    return true
  }
}
function validateInput (rule, value) {
  let error = ''
  if (typeof(rules[rule].value) !== undefined && rules[rule].value === value) {
    error = message[rule].value
  }

  if (rules[rule].minLength && typeof(rules[rule].minLength) !== undefined && rules[rule].minLength >= parseInt(value)) {
    error += message[rule].minLength
  }
  // changes minLength into maxLength
  if (rules[rule].maxLength && typeof(rules[rule].maxLength) !== undefined && rules[rule].maxLength <= parseInt(value)) {
    error += message[rule].maxLength
  }

  if (rules[rule].minValue > parseInt(value)) {
    error += message[rule].minValue
  }

  if (rules[rule].isNumber && typeof(rules[rule].isNumber) !== undefined && isNaN(value)) {
    error += message[rule].isNumber
  }
  if (rule === 'email' && value.length !== 0 && !validateEmail(value)) {
    error += 'You have entered an invalid email address!'
  }
  return error
}

function validateEmail (mail) {
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  console.log(mailformat.test(mail))
  if (mailformat.test(mail)) {
    return (true)
  } else {
    return (false)
  }
}