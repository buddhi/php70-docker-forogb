import Vue from 'vue'
import axios from 'axios'
import Modal from './Components/Modal'

window.axios = axios
var app2 = new Vue({
  el: '#project-edit-app',
  components: {
    'app-modal': Modal
  },
  data: {
    showModalVariable: false,
    projectToDeleteId: 0,
    indexOfProject: 0,
    isActive: false,
    id: 0,
    requestMethod: 'get',
    current_page: null,
    from: null,
    last_page: null,
    pageIndex: 1,
    requestUrl: '/ogbadmin/get-project-data',
    projects: null,
    to: null,
    total: null,
    deleteFlg: 0,
    filters: {
      province_id: '',
      district: '',
      pump_size: [],
      project_type_id: [],
      status: [],
      credit_score: [],
      searchField: null,
      orderBy: null,
      totalInvestment: 0
    }
  },
  // components:{
  //     'app-edit-modal' : ProjectEditModal
  // },
  mounted () {
    // console.log('hello')
    this.getData(this.requestUrl + '?page=' + this.pageIndex)
    this.totalInvestment = 0
  },
  methods: {
    clearAllFilters () {
      this.projects = null
      this.filters = {
        province_id: '',
        district: '',
        pump_size: [],
        project_type_id: [],
        credit_score: [],
        status: [],
        searchField: null,
        orderBy: null
      }

      this.getData(this.requestUrl + '?page=' + this.pageIndex)
      $('#cancel-filter').trigger('click')
    },
    closeBtn (e) {
      this.isActive = false
    },
    changeStatusAndInputId (projectid) {
      this.id = projectid
      this.isActive = true
    },
    getData (url) {
      axios[this.requestMethod](url, this.filters).then(res => {
        // console.log(res)
        this.projects = res.data.data
        // console.log(res)
        this.last_page = res.data.meta.last_page
        this.from = res.data.meta.from
        this.current_page = res.data.meta.current_page
        this.last_page_url = res.data.last_page_url
        this.total = res.data.meta.total
        this.to = res.data.meta.to
      })
    },
    loadFirstPageData () {
      this.projects = null
      this.getData(this.requestUrl + '?page=' + 1)
    },
    loadLastPageData () {
      // let previousPage = this.current_page - 1
      this.projects = null
      this.getData(this.requestUrl + '?page=' + this.last_page)
    },
    loadPreviousPageData () {
      this.projects = null
      let previousPage = this.current_page - 1
      this.getData(this.requestUrl + '?page=' + previousPage)
    },
    loadNextPageData () {
      this.projects = null
      let nextPage = this.current_page + 1
      this.getData(this.requestUrl + '?page=' + nextPage)
    },
    onFilterSubmit () {
      this.projects = null
      this.requestMethod = 'post'
      this.requestUrl = 'project/filter'
      this.getData(this.requestUrl)
    },
    onProjectUpdate () {
      this.getData(this.requestUrl + '?page=' + this.current_page)
    },
    deactivateProject (id, index) {
      axios.get('/ogbadmin/project/change-status/' + id, {
        id: id
      }).then(res => {
        this.projects[index].delete_flg = !this.projects[index].delete_flg
        this.projectToDeleteId = 0
        this.indexOfProject = 0
        this.showModalVariable = false
      }).catch(err => {
        alert(err)
      })
    },
    showModal (id, index, deleteFlg) {
      console.log('here')
      this.projectToDeleteId = id
      this.indexOfProject = index
      this.showModalVariable = true
      this.deleteFlg = deleteFlg
    },
    closeModal () {
      this.showModalVariable = false
    },
    getPercentage (investment, projectCost) {
      this.totalInvestment = this.totalInvestment + investment
      // console.log((investment*100)/projectCost)
      console.log('in the front first', investment, this.totalInvestment)
      return (investment * 100) / projectCost
    },
    getFundingGap (projectCost) {
      let fundingGap = projectCost - this.totalInvestment
      console.log('gap in second', projectCost, this.totalInvestment)
      // this.totalInvestment = 0

      return fundingGap
    },
    getFundingGapPercentage (projectCost) {
      console.log('gap', projectCost, this.totalInvestment)
      let fundingGap = projectCost - this.totalInvestment
      this.totalInvestment = 0
      console.log('funding Gap', fundingGap)
      return (fundingGap * 100) / projectCost
    },
    getColor (name) {
      switch (name) {
        case 'Gham Power':
          return 'blue'
        case 'My Investment':
          return 'green'
        case 'MFI Partner':
          return 'purple'
        case 'Other (OGB / Crowd Funding)':
          return 'orange'
        case 'Farmer Equity':
          return 'green'
        case 'Funding Gap':
          return 'grey'
        default:
          return 'purple'
      }
    },
    getStatusCapitalCased (projectToCapitalCase) {
      return projectToCapitalCase ? (projectToCapitalCase.charAt(0).toUpperCase() + projectToCapitalCase.slice(1)) : ''
    }
  },
  computed: {
    isActiveComputed () {
      return this.isActive
    },
    getInFirst () {
      if (this.current_page === 1) {
        return false
      }
      return true
    },
    isInLast () {
      if (this.current_page === this.last_page) {
        return false
      }
      return true
    },
    getProjects () {
      return this.projects
    }
  }
})
