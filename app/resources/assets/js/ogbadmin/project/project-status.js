import Vue from 'vue'
import axios from 'axios'
import StatusUpdate from './Components/StatusUpdate'
import StatusDropDown from './Components/StatusDropDown'
import FundingState from './Components/FundingSate'
import ProjectLog from './Components/ProjectLog'
import Modal from './Components/Modal'
import UploadFiles from './Components/UploadFile'
import ProjectInvestment from './Components/ProjectInvestment'
import ProjectPayment from './Components/ProjectPayment'
import VueNoty from 'vuejs-noty'
import PopUp from './Components/PopUp'
import PaymentHistory from './Components/PaymentHistory'
import InvestmentChart from './Components/InvestmentChart'

window.axios = axios
// axios.defaults.baseURL = `/ogbadmin`
window.eventBus = new Vue()
Vue.use(VueNoty, { timeout: 3000, progressBar: true })

var app = new Vue({
  el: '#status-update',
  data: {
    guideline: null,
    isActive: false,
    id: document.querySelector('meta[name="project_id"]').getAttribute('content'),
    projectData: {},
    uploadsTypes: {},
    showPopup: 0,
    showInvestmentPopUp: false,
    showPaymentPopUp: false,
    investmentPercentage: 0,
    paymentData: {},
    payments: {},
    paymentDefaultedDetails: {}
  },
  components: {
    'app-status-update': StatusUpdate,
    'app-available-status': StatusDropDown,
    'app-funding-state': FundingState,
    'app-project-log': ProjectLog,
    'app-modal': Modal,
    'upload-files': UploadFiles,
    'app-project-investment': ProjectInvestment,
    'app-project-payment': ProjectPayment,
    'app-pop-up': PopUp,
    'app-payment-history': PaymentHistory,
    'app-payment-chart': InvestmentChart
  },
  created () {
    console.log('here I am')
    eventBus.$on('refresh', this.getProjectData)
    eventBus.$on('investment-percentage-change', this.investmentPercentageChange)
    // eventBus.$on('payment-refresh', this.getPaymentHistory)
    this.getProjectData()
    // this.getPaymentHistory()
  },
  mounted () {
    // axios.get('/project/')

  },
  methods: {
    changeStatusAndInputId (projectid) {
      this.isActive = true
      this.id = projectid
    },
    getProjectData () {
      axios.get('/ogbadmin/project-information/' + this.id).then(data => {
        this.projectData = data.data.data
        this.getUploadTypes()
        this.getPaymentAverageDefaultInfos();
        // this.isLoading = false
      })
    },
    investmentPercentageChange (data) {
      this.investmentPercentage = data.investmentPercentage

    },
    getUploadTypes () {
      axios.get('/ogbadmin/project/' + this.id + '/document-upload-list').then(res => {
        this.uploadsTypes = res.data
      })
    },
    getPaymentAverageDefaultInfos(){
      axios.get('/ogbadmin/project/' + this.id + '/average-default-data').then(res => {
        this.paymentDefaultedDetails = res.data
      })
    },
    // getPaymentHistory () {
    //   axios.get('/ogbadmin/project/' + this.id + '/payment_history').then(res => {
    //     this.paymentData = res.data.paymentData
    //     this.payments = res.data.payments
    //     console.log('payment',this.payments)
    //   })
    // }
  },
  computed: {
    isActiveComputed () {
      return this.isActive
    },
    getShowMakeInvestment () {
      return this.projectData.status === 'funding'
    },
    getShowMakePayment () {
      return this.projectData.status === 'operational'
    },
    getStatusCapitalCased () {
      return this.projectData.status ? (this.projectData.status.charAt(0).toUpperCase() + this.projectData.status.slice(1)) : ''
    },
    meterExist () {
      return this.projectData.meter_id > 0
    }
  }
})
