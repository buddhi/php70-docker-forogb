$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  $('#add-user-error-bag').hide()
  $('.verification-email-user').on('click', function (e) {
    e.preventDefault()

    var formData = {
      partner_id: $('#partner').val(),
      email: $('#email').val()
    }
    $.ajax({
      type: 'POST',
      url: '/ogbadmin/partner-user/store-temp-user',
      data: formData,
      dataType: 'json',
      success: function (response) {
        if (!response.error) {
          $('#user-verify').modal('hide')
          $('#add_user').trigger('reset')
          $('#verification-email-modal').modal('show')
          $('#email-text').css({ 'font-weight': 'bold' }).append(formData.email)
        }
      },
      error: function (data) {
        var errors = $.parseJSON(data.responseText)
        $('#add-user-form-errors').html('')
        $.each(errors.messages, function (key, value) {
          $('#add-user-form-errors').append('<li>' + value + '</li>')
        })
        $('#add-user-error-bag').show()
      }
    })
  })

  // Closing the verification email modal to reload the page to fetch the latest records
  $('#verification-email-modal').on('hide.bs.modal', function (e) {
    location.reload()
  })

  $('.toggle-partner-user').on('click', function (e) {
    let partner = $(this).data('partner')
    let isChecked = $(this).prop('checked')
    if (isChecked) {
      let isPartnerDisabled = disabled_partners.includes(partner)
      if (isPartnerDisabled) {
        e.preventDefault()
        alert('Partner is disabled. Please enable the partner first')
        return
      }
    }
    var result = confirm('Do you want to change the user status?')
    var $this = $(this)
    if (!result) {
      e.preventDefault()
      return
    }
    if (result) {
      $.ajax({
        method: 'POST',
        url: '/ogbadmin/partner-user/' + $this.attr('attr') + '/change-status',
        success: function (response) {
          if (!response.error) {
            alert(response.messages)
            $($this).closest('td').prev('td').html('<div class="status ' + response.user_status.toLowerCase() + '">' + response.user_status + '</div>')
          }
        }
      })
    }
  })

  $('.image-change').click(function () {
    $('#file').click()
  })
  $('#file').change(function () {
    if ($(this).val() != '') {
      upload($(this))
    }
  })

  function upload (img) {
    var form_data = new FormData()
    form_data.append('user_id', img.data('id'))
    form_data.append('file', img[0].files[0])
    $.ajax({
      url: '/account/user/change-image',
      data: form_data,
      type: 'POST',
      contentType: false,
      processData: false,
      success: function (response) {
        alert(response.messages)
        if (!response.error) {
          $('#preview_image').attr('src', '/storage/upload/' + response.image)
        }
      },
      error: function (response) {
        alert(response.responseJSON.messages)
      }
    })
  }

  $(function () {
    $('[data-toggle="toggletip"]').tooltip({
      trigger: 'hover'
    }).on('show.bs.tooltip', function (e) {
      let hovered_partner = $(this).data('partner')
      let isPartnerDisabled = disabled_partners.includes(hovered_partner)
      let isChecked = $(this).find('input').prop('checked')
      $(this).attr('data-original-title', isChecked ? 'Disable User' : isPartnerDisabled ? ' Partner Disabled, Enable Partner First' : 'Enable User')
    })
  })
  // $('#resend-email').on('click', function (event) {
  //   event.preventDefault()
  //   var id = $('meta[name="id_temporary_user"]').attr('content')
  //   // var button = $(event.relatedTarget)
  //   $.ajax({
  //     type: 'GET',
  //     url: 'partner-user/resend-verification-custom/'+ id,
  //     dataType: 'json',
  //     success: function (response) {
  //       if (!response.error) {
  //         // $('#resend-verification').modal('hide')
  //         $('#verification-email-modal').modal('show')
  //         $('#email-text').css({ 'font-weight': 'bold' }).append('Some')
  //       }
  //     }
  //   })
  // })
  $('#resend-verification').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var recipient = button.data('whatever')
    var recipientId = button.data('id')
    // console.log(recipientId)
    console.log(button.data('id'))
    var modal = $(this)
    modal.find('.form-group input').val(recipient);
    modal.find('.form-group #verify_id').val(recipientId);
  });

  //resend verification email ajax request
  $(".resend-verification-email").on("click", function (e) {
    e.preventDefault();
    console.log($('#verify_id').val());
    var formData = {
      email: $('#verify_email').val(),
      id: $('#verify_id').val()
    };
    $.ajax({
      type: 'GET',
      url: 'partner-user/resend-verification-custom/'+ formData.id,
      dataType: 'json',
      success: function (response) {
        if (!response.error) {
          $('#resend-verification').modal('hide')
          $('#verification-email-modal').modal('show')
          $('#email-text').css({ 'font-weight': 'bold' }).append(formData.email)
        }
      }
    })

  });
})


