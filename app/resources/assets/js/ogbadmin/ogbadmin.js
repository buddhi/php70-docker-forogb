$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".inputs-otp").keyup("input", function() {
        var $this = $(this);
        var id = $this.data('id');
        var nextId = parseInt(id) + 1;
        if ( $this.val().length >= $this.attr("maxlength")){
            $('#digit' + nextId).focus();
        }
    });

    //partner drop down select2
    $("#partner").select2({
        templateResult: formatState,
        width: '100%'

    });
    function formatState (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span><img src="' + optimage + '" class="user-thumbnail" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    }

    $('.notification-bell').click(function () {
        var formData ={};
        $('#unread-count').css('display' ,'none');
        $.ajax({
            type: "POST",
            url: '/ogbadmin/notification/mark-as-read',
            data: formData,
            dataType: 'json',
            success: function (response) {
            }
        });
    });

    $("#notification-div").click(function(event) {
        event.preventDefault();
        window.location.href = $(this).find("a").attr("href");
    });


});

