$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
    $("#add-agent-error-bag").hide();
    $(".verification-email-agent").on("click", function (e) {
        e.preventDefault();

        var formData = {
            email: $('#email').val(),
            partner_id: $('#partner').val(),
        }
        $.ajax({
            type: "POST",
            url: '/agent/store-temp-user',
            data: formData,
            dataType: 'json',
            success: function (response) {
                if (!response.error) {
                    $("#agent-verify").modal("hide");
                    $("#add_agent").trigger("reset");
                    $("#verification-email-modal").modal("show");
                    $("#email-text").css({'font-weight': 'bold'}).append(formData.email);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-agent-form-errors').html('');
                $.each(errors.messages, function (key, value) {
                    $('#add-agent-form-errors').append('<li>' + value + '</li>');
                });
                $("#add-agent-error-bag").show();
            }
        });
    });

    $('.toggle-agent').on('click', function (e) {
        let partner = $(this).data('partner');
        let isChecked = $(this).prop('checked');
        if(isChecked){
            let isPartnerDisabled = disabled_partners.includes(partner);
            if(isPartnerDisabled){
                e.preventDefault()
                alert('Partner is disabled. Please enable the partner first')
                return;
            }
        }
        let result = confirm("Do you want to change the agent status?");
        var $this = $(this);
        if(!result){
            e.preventDefault();
            return;
        }
        if(result){
            var $this = $(this);
            $.ajax({
                method: 'POST',
                url: '/agent/' + $this.data('id') + '/change-status',
                success: function (response) {
                    if (!response.error) {
                        alert(response.messages);
                        $($this).closest('td').prev('td').html('<div class="status ' + response.user_status.toLowerCase() + '">' + response.user_status + '</div>');
                    }
                }
            });

        }
    });

    //Closing the verification email modal to reload the page to fetch the latest records
    $('#verification-email-modal').on('hide.bs.modal', function (e) {
        location.reload()
    });

    $(function () {
        $('[data-toggle="toggletip"]').tooltip({
            trigger : 'hover',
        }).on('show.bs.tooltip', function(e) {
            var hovered_partner = $(this).data('partner');
            var isPartnerDisabled = disabled_partners.includes(hovered_partner);
            var isChecked = $(this).find('input').prop('checked');
            $(this).attr('data-original-title', isChecked? "Disable Agent": isPartnerDisabled?  " Partner Disabled, Enable Partner First":"Enable Agent");
        });
    });

  $('#resend-verification').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var recipient = button.data('whatever')
    var recipientId = button.data('id')
    // console.log(recipientId)
    console.log(button.data('id'))
    var modal = $(this)
    modal.find('.form-group input').val(recipient);
    modal.find('.form-group #verify_id').val(recipientId);
  });

  //resend verification email ajax request
  $(".resend-verification-email").on("click", function (e) {
    e.preventDefault();
    console.log($('#verify_id').val());
    var formData = {
      email: $('#verify_email').val(),
      id: $('#verify_id').val()
    };
    $.ajax({
      type: 'GET',
      url: '/agent/resend-email-verification/'+ formData.id,
      dataType: 'json',
      success: function (response) {
        if (!response.error) {
          $('#resend-verification').modal('hide')
          $('#verification-email-modal').modal('show')
          $('#email-text').css({ 'font-weight': 'bold' }).append(formData.email)
        }
      }
    })

  });
});
