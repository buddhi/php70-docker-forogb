/* global Vue */
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import numFormat from 'vue-filter-number-format'
import store from './helpers/storage'
import './helpers/locale'
import * as VueGoogleMaps from 'vue2-google-maps'
import GmapCluster from 'vue2-google-maps/dist/components/cluster'
require('./bootstrap')
require('./init')

window.Vue = require('vue')
window._token = document.head.querySelector('meta[name="csrf-token"]').content
window.store = store

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDa0mYwWMgHdH-ClgyhUWe8P7qvP4ATkEM',
    libraries: 'places' // necessary for places input
  }
})

/* vue filters */
Vue.filter('numFormat', numFormat)
Vue.filter('currencyConversion', function (value) {
  return value / 100
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('doughnut-chart', require('./components/DoughnutChart.vue').default)
Vue.component('loading', require('./components/Loading.vue').default)

// header
Vue.component('my-list', require('./components/header/MyList.vue').default)

// checkout page
Vue.component('checkout', require('./components/project/Checkout.vue').default)

// Investment list page
Vue.component('project-list', require('./components/project/List.vue').default)
Vue.component('project-list-single', require('./components/project/ListSingle.vue').default)
Vue.component('latest-invested-project', require('./components/project/LatestInvestedProjects.vue').default)

// Investment details page
Vue.component('project-detail', require('./components/project/Detail.vue').default)
Vue.component('cashflow', require('./components/project/CashFlow.vue').default)
Vue.component('investment-confirmation-modal', require('./components/project/InvestmentConfirmationModal.vue').default)

// My investment list page
Vue.component('investment-list', require('./components/project/Investments.vue').default)

//Google Map
Vue.component('gmap-cluster', GmapCluster)
Vue.component('google-map', require('./components/GoogleMap.vue').default)

// Payment
Vue.component('share-news', require('./components/project/ShareNews.vue').default)
Vue.component('payment-success', require('./components/project/PaymentSuccess.vue').default)
Vue.component('payment-failure', require('./components/project/PaymentFailure.vue').default)
Vue.component('image-component', require('./components/ImageuploadComponent.vue').default);



new Vue({
  el: '#app'
})
