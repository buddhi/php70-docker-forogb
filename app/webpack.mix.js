let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
// mix.sass('resources/assets/sass/app.scss', 'public/css')
// mix.js('resources/js/app.js', 'public/js').extract(['vue'])
mix.options({ imgLoaderOptions: { enabled: false } })
    .js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
mix.js('resources/assets/js/ogbadmin/ogbadmin.js', 'public/js/ogbadmin')
mix.js('resources/assets/js/ogbadmin/partner/partner-dashboard.js', 'public/js/ogbadmin/partner')
mix.js('resources/assets/js/ogbadmin/user/user-dashboard.js', 'public/js/ogbadmin/user')
mix.js('resources/assets/js/ogbadmin/agent/agent-dashboard.js', 'public/js/ogbadmin/agent')
mix.js('resources/assets/js/ogbadmin/project/project-create.js', 'public/js/ogbadmin/project')
mix.js('resources/assets/js/ogbadmin/project/project-modal.js', 'public/js/ogbadmin/project')
mix.js('resources/assets/js/ogbadmin/project/project-status.js', 'public/js/ogbadmin/project')
mix.js('resources/assets/js/ogbadmin/project/project-element.js', 'public/js/ogbadmin/project')
mix.js('resources/assets/js/ogbadmin/project/project-form.js', 'public/js/ogbadmin/project')
