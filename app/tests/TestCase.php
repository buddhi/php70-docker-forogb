<?php

namespace Tests;

use App\Enums\Verification;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\User;
use App\Enums\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function authenticateAsUser()
    {
        $user = factory(User::class)->make();
        $user->verified = Verification::EMAIL_VERIFIED;
        $this->actingAs($user)->withSession(['role' => Role::INVESTOR]);
    }

    protected function signInUserWithRole($role,$user = null)
    {
        if(!isset($user)){
            $user = factory(User::class)->make();
        }
        $user->verified = Verification::EMAIL_VERIFIED;
        $user->admin_verification = Verification::ADMIN_VERIFIED;
        $this->actingAs($user)->withSession(['role' => $role]);

    }

    public function disableCookiesEncryption($name)
    {
        $this->app->resolving(EncryptCookies::class,
            function ($object) use ($name)
            {
                $object->disableFor($name);
            });

        return $this;
    }
}
