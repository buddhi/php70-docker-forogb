<?php

namespace Tests\Unit;
use App\Models\TransitionState;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TransitionStateFilterTest extends TestCase
{
    public $actual_transition_states;
    use  RefreshDatabase;
    public $projectStateUpdate,$project,$smsRepo;
    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed', ['--class' => 'TransitionStateTableSeeder']);
        $this->project = factory('App\Models\Project')->create();
    }

    public function testGetTransitionStatesReturnsStatesWithOnlyCertainStatus()
    {
        $states = array_keys(config('transition.states'));
        foreach ($states as $state) {
            $transition_states = getTransitionStates($state, null);
            foreach ($transition_states as $transition_state) {
                $this->assertEquals($state, $transition_state['status']);
            }

        }
    }

    public function testGetTransitionStatesReturnsStateWithCertainColumnOnly()
    {

        $transition_states_ids = getTransitionStates(null, 'id');
        foreach ($transition_states_ids as $transition_states_id) {
            $this->assertInternalType("int", $transition_states_id);
        }

    }

    public function testGetTransitionStatesReturnsStateMatchingCertainStatusAndColumn()
    {
        $transition_state_ids_with_approved_status = [3, 4, 5, 6];
        $transition_states_ids = getTransitionStates('approved', 'id');
        foreach ($transition_states_ids as $transition_states_id) {
            $this->assertContains($transition_states_id, $transition_state_ids_with_approved_status);
        }
    }

    public function testGetTransitionStatesReturnsTransitionStateArrayOnly()
    {
        $transition_states = getTransitionStates(null, null);
        $expected_transition_states = TransitionState::all()->toArray();
        $this->assertEquals($transition_states, $expected_transition_states);
    }
}
