<?php

namespace Tests\Unit;

use App\Enums\UserStatus;
use App\OGB\Constants;
use App\User;
use Tests\TestCase;

class UserStatusTest extends TestCase
{
    private $user;
    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }

    /** @test */
    //soft deleted user status is deactivated
    public function disabled_user_has_deactivated_status()
    {
        $this->user->disabled = Constants::ACTIVE;
        $this->assertEquals($this->user->status, UserStatus::DEACTIVATED);
    }

    /** @test */
    // Email Not Verified status is pending
    public function email_unverified_user_has_pending_status()
    {
        $this->user->verified = Constants::OFF;
        $this->assertEquals($this->user->status, UserStatus::VERIFICATION_PENDING);
    }

    /** @test */
    // Email Verified  But Otp Not Verified status is verified
    public function email_verified_but_otp_not_verifed_user_has_verified_status()
    {
        $this->user->verified = Constants::ACTIVE;
        $this->user->otp_verified = Constants::OFF;
        $this->assertEquals($this->user->status, UserStatus::VERIFIED);
    }

    /** @test */
    // Email and Otp  Verified status is activated
    public function test_email_and_otp_verified_user_has_activated_status()
    {
        $this->user->otp_verified = Constants::ACTIVE;
        $this->user->verified = Constants::ACTIVE;
        $this->assertEquals($this->user->status, UserStatus::ACTIVATED);
    }

    /** @test */
    public function disabled_status_has_high_priority()
    {
        $this->user->disabled = Constants::ACTIVE;
        $this->user->verified = Constants::ACTIVE;
        $this->user->otp_verified = Constants::ACTIVE;
        $this->assertEquals($this->user->status, UserStatus::DEACTIVATED);
    }
}
