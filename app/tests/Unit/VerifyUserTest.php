<?php

namespace Tests\Unit;

use App\Enums\Role;
use App\Enums\Verification;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VerifyUserTest extends TestCase
{
    public $user, $middleware;
    use RefreshDatabase;
    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }
    public function test_developer_cannot_login_with_email_not_verified()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::DEVELOPER);
        $registration_form = array_merge(
          $registration_form,[
              'verified'=>Verification::EMAIL_NOT_VERIFIED,
              'admin_verification'=>Verification::ADMIN_VERIFIED
            ]
        );
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role'=>Role::DEVELOPER,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLogoutRoute(Role::DEVELOPER,Verification::EMAIL_NOT_VERIFIED,Verification::ADMIN_VERIFIED));
    }
    public function test_investor_cannot_login_with_email_not_verified()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::INVESTOR);
        $registration_form = array_merge(
            $registration_form,[
                'verified'=>Verification::EMAIL_NOT_VERIFIED,
                'admin_verification'=>Verification::ADMIN_VERIFIED
            ]
        );
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role'=>Role::INVESTOR,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLogoutRoute(Role::INVESTOR));
    }
    public function createRegistrationFormWithCertainRole($role)
    {
        $registration_form = array_merge(
            $this->user->toArray(),
            [   'role' => [$role],
                'password' => 'secret',
                'email'=>$this->user->email,
                'password_confirmation' =>'secret',
                'user-form'=>'user-form'
            ]);
        return $registration_form;
    }

    protected function loginPostRoute($role =null)
    {
        return route('login');
    }
    private function submitRegistrationForm($registration_form)
    {
        $response =$this->post(route('register'),
            $registration_form
        );
        return $response;
    }
    private function successfulLoginRoute($role)
    {
        if($role == Role::ADMIN) {
            return route('admin.project.list');
        }
        return route('ogbadmin.project.index');
    }

    protected function successfulLogoutRoute($role,$email_verification=null,$admin_verification =null)
    {
        if($role == Role::ADMIN) {
            return route('admin.login');
        }
        if($role == Role::DEVELOPER ) {
            return route('login');
        }

        return route('index');
    }

    public function test_developer_can_login_with_admin_not_verified()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole(Role::DEVELOPER);
        $registration_form = array_merge(
            $registration_form, [
                'verified' => Verification::EMAIL_VERIFIED,
                'admin_verification' => Verification::ADMIN_NOT_VERIFIED
            ]
        );
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role' => Role::DEVELOPER,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLoginRoute(Role::DEVELOPER));

    }

    public function test_investor_can_login_with_admin_not_verified()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole(Role::INVESTOR);
        $registration_form = array_merge(
            $registration_form, [
                'verified' => Verification::EMAIL_VERIFIED,
                'admin_verification' => Verification::ADMIN_NOT_VERIFIED,
            ]
        );
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role' => Role::INVESTOR,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLoginRoute(Role::INVESTOR));
    }
}
