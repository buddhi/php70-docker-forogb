<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\MeterData;
use App\Models\Meter;

class MeterDataTest extends TestCase
{
    private $meterData, $meter;
    private $current = 0.65;
    private $voltage= 165.32;
    private $temperature= 17.28;
    private $humidity = 1036.78;
    private $cd_efficiency = 0.85;
    protected function setUp()
    {
        parent::setUp();

        $this->meter = new Meter();
        $this->meter->id = 1;
        $this->meter->cd_efficiency = $this->cd_efficiency;

        $this->meterData = new MeterData();
        $this->meterData->current = $this->current;
        $this->meterData->voltage = $this->voltage;
        $this->meterData->temperature = $this->temperature;
        $this->meterData->humidity = $this->humidity;

        $this->meterData->meter()->associate($this->meter);
    }
    public function testMeterIdExists()
    {
        $current = $this->meterData->meter->id;
        $expected = 1;
        $this->assertEquals($expected, $current);
    }
    public function testDcDischargeIsExpected()
    {
        $current = $this->meterData->getDcPowerAttribute();
        $expected = $this->current * $this->voltage;
        $this->assertEquals($expected, $current);
    }
    public function testAcDischargeIsExpected()
    {
        $current = $this->meterData->getAcPowerAttribute();
        $expected = $this->current * $this->voltage * $this->cd_efficiency;
        $this->assertEquals($expected, $current);
    }
}