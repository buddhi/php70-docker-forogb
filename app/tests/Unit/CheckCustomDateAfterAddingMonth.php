<?php

namespace Tests\Unit;

use App\Facade\DateConverter;
use Carbon\Carbon;
use Tests\TestCase;

class CheckCustomDateAfterAddingMonth extends TestCase
{
    /**
     * Testing date addition in accordance to bs format*
     * 2075-12-11 BS must be equal to 2076-1-11 BS after month addition
     * @return void
     */
    public function testCustomDateAddition()
    {
        $expected_bs_date_after_adding_month_array = [
            "2076-1-31 BS",
            "2076-2-31 BS",
            "2076-3-31 BS",
            "2076-4-31 BS",
            "2076-5-31 BS",
            "2076-6-30 BS",
            "2076-7-30 BS",
            "2076-8-30 BS",
            "2076-9-29 BS",
            "2076-10-29 BS",
            "2076-11-30 BS",
            "2076-12-30 BS",
            "2077-1-31 BS",
            ];


        $ad_date = DateConverter::bs_to_ad(2076,1,31);
        $number_of_checks = 13;
        
        //adding {i} month through the loop
        for($i = 1 ; $i< $number_of_checks; $i++){
            $ad_date_string = $ad_date['year']."-".$ad_date['month']."-".$ad_date['date'];
            $date_after_adding_month = BS_addMonth(Carbon::parse($ad_date_string),$i);
            $bs_date_after_adding_month = DateConverter::ad_to_bs_date($date_after_adding_month);
            $this->assertEquals($expected_bs_date_after_adding_month_array[$i],$bs_date_after_adding_month);

        }
    }
}