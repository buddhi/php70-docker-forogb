<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;

class PaymentIdTest extends TestCase
{
    private $project;
    protected function setUp()
    {
        parent::setUp();
        $this->project = factory('App\Models\Project')->make();
    }
    public function testPaymentIdReturnsNullWhenProjectIdIsAbsent()
    {
        $this->project->id = null;
        $this->assertNull($this->project->generatePaymentId());
    }

    public function testPaymentIdReturnsNullCreatedDateIsAbsent()
    {
        $this->project->id = 1;
        $this->project->created_on = null;
        $this->assertNull($this->project->generatePaymentId());
    }

    public function testPaymentIdReturnsNullWhenCreatedDateAndProjectIdIsAbsent()
    {
        $this->project->id = null;
        $this->project->creeated_on = null;
        $this->assertNull($this->project->generatePaymentId());
    }
    public function testPaymentIdTakesYearMonthAndProjectId()
    {
        $this->project->id = 1;
        $this->project->created_on = Carbon::now()->toDateString();
        $payment_id = Carbon::parse($this->project->created_on)->format('ym').$this->project->id;
        $this->assertEquals($this->project->generatePaymentId(), $payment_id);
    }

    public function testPaymentIdTakesYearMonthAndAnyProjectId()
    {
        $this->project->id = rand(1,2000000);
        $this->project->created_on = Carbon::now()->addMonth(1)->toDateString();
        $payment_id = Carbon::parse($this->project->created_on)->format('ym').$this->project->id;
        $this->assertEquals($this->project->generatePaymentId(), $payment_id);
    }
}
