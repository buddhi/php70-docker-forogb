<?php

namespace Tests\Unit;

use App\Models\Risk;
use App\Models\RiskAttribute;
use App\Models\RiskAttributeCategory;
use App\Services\CreditScore;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class CreditScoreTest extends TestCase
{
//    private $risk_input_array = array(
//        array('age' => 12, 'gender' => 'male'),
//        array('age' => 12, 'gender' => 'female'),
//        array('age' => 62, 'gender' => 'male'),
//        array('age' => 62, 'gender' => 'female')
//    );
//    public $risk_collection, $risk_model;
//    private $expected_risk_output = array(0.032, 0.056, 0.016, 0.04);
//    public $risk, $risks, $risk_attribute, $risk_attributes, $risk_attribute_category, $risk_attribute_categories;
//    public $risk_array = [
//        [1, 'demographic', 0.2, null, null],
//        [2, 'financial', 0.4, null, null],
//    ];
//    public $risk_attribute_array = [
//        [1, 'age', 0.2, 'range', 1, null, null],
//        [2, 'gender', 0.2, 'fixed', 1, null, null],
//    ];
//
//    public $risk_attribute_categories_array = [
//        [1, 'below60', 0.6, 1, 0, 60, null, null],
//        [2, 'above60', 0.2, 1, 61, 200, null, null],
//        [3, 'male', 0.2, 1, null, null, null, null],
//        [4, 'female', 0.8, 2, null, null, null, null]
//    ];
//
//    protected function setUp()
//    {
//        parent::setUp();
//        $this->risks = new Collection();
//        $this->risk_attributes = new Collection();
//        $this->risk_attribute_categories = new Collection();
//        foreach ($this->risk_array as $key => $item) {
//            $this->risk = new Risk();
//            $this->risk->id = $item[0];
//            $this->risk->name = $item[1];
//            $this->risk->risk_weight_factor = $item[2];
//            $this->risk->setRelation('attributes', null);
//            $this->risks->push($this->risk);
//        }
//
//        foreach ($this->risk_attribute_array as $key => $item) {
//            $this->risk_attribute = new RiskAttribute();
//            $this->risk_attribute->id = $item[0];
//            $this->risk_attribute->name = $item[1];
//            $this->risk_attribute->attribute_weight_factor = $item[2];
//            $this->risk_attribute->type = $item[3];
//            $this->risk_attribute->risks()->associate($this->risks->first());
//            $this->risk_attribute->setRelation('categories', null);
//            $this->risk_attributes->push($this->risk_attribute);
//        }
//        $this->risks[0]->setRelation('attributes', new Collection([$this->risk_attributes[0], $this->risk_attributes[1]]));
//
//        foreach ($this->risk_attribute_categories_array as $key => $item) {
//            $this->risk_attribute_category = new RiskAttributeCategory();
//            $this->risk_attribute_category->id = $item[0];
//            $this->risk_attribute_category->name = $item[1];
//            $this->risk_attribute_category->category_weight_factor = $item[2];
//            $this->risk_attribute_category->min_value = $item[4];
//            $this->risk_attribute_category->max_value = $item[5];
//            $this->risk_attribute_category->riskAttributes()->associate($this->risk_attributes->first());
//            $this->risk_attribute_categories->push($this->risk_attribute_category);
//        }
//        $this->risk_attributes[0]->setRelation('categories', new Collection([$this->risk_attribute_categories[0], $this->risk_attribute_categories[1]]));
//        $this->risk_attributes[1]->setRelation('categories', new Collection([$this->risk_attribute_categories[2], $this->risk_attribute_categories[3]]));
//    }

//    public function testCreditScoreOutputs()
//    {
//        $credit_score_output = [];
//        foreach ($this->risk_input_array as $key => $item) {
//            CreditScore::$risk_input = $item;
//            $credit_score_output[$key] = CreditScore::totalRisk($this->risks);
//        }
//        $this->assertEmpty(array_merge(array_diff($credit_score_output, $this->expected_risk_output), array_diff($this->expected_risk_output, $credit_score_output)));
//    }
//
//    public function testEmptyInputGivesOutput0()
//    {
//        $risk_input = [];
//        $credit_score_output = CreditScore::totalRisk($this->risk_collection,$risk_input);
//        $this->assertEquals($credit_score_output, 0);
//    }
//
//    public function testWrongInputGivesOutput0()
//    {
//        $risk_input = ['wrong_array_key1' => 1, 'wrong_array_key2' => 2];
//        $credit_score_output = CreditScore::totalRisk($this->risk_collection,$risk_input);
//        $this->assertEquals($credit_score_output, 0);
//    }
//
//    public function testEmptyCollectionWeightFactorGivesOutput0()
//    {
//        $risk_input = ['age' => 12, 'gender' => 'male'];
//        $this->risk_collection = new Collection();
//        $credit_score_output = CreditScore::totalRisk($this->risk_collection,$risk_input);
//        $this->assertEquals($credit_score_output, 0);
//    }

    public function testEmptyCollectionWeightFactorGivesOutput0()
    {
       $this->assertTrue(true);
    }
}
