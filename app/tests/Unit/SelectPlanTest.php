<?php

namespace Tests\Unit;

use App\Models\Plan;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class SelectPlanTest extends TestCase
{
    public $plans_array = [
        [1, 'Plan 1', 'Sahaj', 210000, '1', 180000, 945],
        [2, 'Plan 2', 'Sulav', 240000, '1', 250000, 1575],
        [3, 'Plan 3', 'Sambridhi', 376000, '2', 350000, 2205]
    ];


    public $low_water_requirement_monthly = [
        "jan" => 0,
        "feb" => 31403.076923077,
        "mar" => 55395.027692308,
        "apr" => 80140.652307692,
        "may" => 33098.843076923,
        "jun" => 0,
        "jul" => 0,
        "aug" => 0,
        "sep" => 0,
        "oct" => 0,
        "nov" => 0,
        "dec" => 0,

    ];
    public $more_water_requirement_monthly = [
        "jan" => 0,
        "feb" => 0,
        "mar" => 0,
        "apr" => 0,
        "may" => 0,
        "jun" => 350000,
        "jul" => 360000,
        "aug" => 370000,
        "sep" => 0,
        "oct" => 0,
        "nov" => 0,
        "dec" => 0,
    ];

    private $plan, $plans, $projectService;

    protected function setUp()
    {
        parent::setUp();
        $this->projectService = $this->app->make('App\Repositories\Project\ProjectService');
        $this->plans = new Collection();
        foreach ($this->plans_array as $key => $item) {
            $this->plan = new Plan();
            $this->plan->id = $item[0];
            $this->plan->code = $item[1];
            $this->plan->name = $item[2];
            $this->plan->plan_cost = $item[3];
            $this->plan->pump_size = $item[4];
            $this->plan->daily_discharge = $item[5];
            $this->plan->solar_pv_size = $item[6];
            $this->plans->push($this->plan);
        }
    }

    public function test_low_requirement_chooses_pump_with_less_discharge()
    {
        $selected_plan = $this->projectService->recommendPlanBasedOnWaterRequirement($this->low_water_requirement_monthly, $this->plans);
        $expected_plan = $this->plans->first();
        $this->assertEquals($expected_plan->daily_discharge, $selected_plan->daily_discharge);
    }

    public function test_large_requirement_chooses_pump_with_more_discharge()
    {
        $selected_plan = $this->projectService->recommendPlanBasedOnWaterRequirement($this->more_water_requirement_monthly, $this->plans);
        $expected_plan = $this->plans->last();
        $this->assertEquals($expected_plan->daily_discharge, $selected_plan->daily_discharge);
    }
}
