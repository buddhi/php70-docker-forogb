<?php

namespace Tests\Unit;

use App\Http\Requests\Investment\CheckoutFormValidation;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckoutFormValidationTest extends TestCase
{
    public $rules,$request;
    public function setUp()
    {
        parent::setUp();
        $this->request = new CheckoutFormValidation();
        $this->rules = $this->request->rules();
    }
    public function testCheckoutFormValidationRejectsRandomData()
    {

        $random_data = [
            ['aa' => 'asd'],
            ['bb' => 'asd'],
            ['ccassd' => 'asd'],
            ['aaasdad' => 'asd'],

        ];
        foreach ($random_data as $data){
            $validator = Validator::make($data ,$this->rules);
            $fails = $validator->fails();
            $this->assertEquals(true, $fails);
        }
    }

    public function testCheckoutFormValidationAcceptsIntegerData()
    {
        $this->assertTrue(true);
        $valid_integer_data = [
            'project_id' => [1],
            'invest_amount' => [1],
            'invest_percent' => [1]
        ];
        $validator = Validator::make($valid_integer_data ,$this->rules);
        $fails = $validator->fails();
        $this->assertEquals(false, $fails);
    }

    public function testCheckoutFormValidationRejectsFloatProjectId()
    {
        $this->assertTrue(true);
        $valid_integer_data = [
            'project_id' => [1.3],
            'invest_amount' => [1],
            'invest_percent' => [1]
        ];
        $validator = Validator::make($valid_integer_data ,$this->rules);
        $fails = $validator->fails();
        $this->assertEquals(true, $fails);
    }
    public function testCheckoutFormValidationRejectsFloatAmont()
    {
        $this->assertTrue(true);
        $valid_integer_data = [
            'project_id' => [1],
            'invest_amount' => [1.3],
            'invest_percent' => [1]
        ];
        $validator = Validator::make($valid_integer_data ,$this->rules);
        $fails = $validator->fails();
        $this->assertEquals(true, $fails);
    }
}
