<?php

namespace Tests\Unit;

use App\Models\Crop;
use App\Models\CropInfo;
use App\Models\Project;
use App\Services\CropArea;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CropAreaConversionTest extends TestCase
{
    //This traits refreshes the database
    use RefreshDatabase;

    public $crop, $project, $crop_info;
    public $id = 1;
    public $one_katha = 0.033863;     //(1 katha = 0.033863 hectares)
    public $one_bigaha = 0.67726;     //(1 bigaha = 0.67726 hectares)
    public $one_ropani = 0.050872;    //(1 ropani = 0.050872 hectares)
    public $tolerance = 0.01;       // Conversion tolerance difference
    public $message = "System can tolerate the certain difference in float values";

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed');
        $this->crop = factory(Crop::class)->make();
        $this->crop->id = $this->id;
        $this->project = new Project();
        $this->crop = factory(Project::class)->make();
        $this->project->id = $this->id;
        $this->crop_info = new CropInfo();
        $this->crop_info->id = $this->id;
    }

    public function test_one_katha_conversion_into_hectare()
    {
        $this->project->crops()->attach($this->project, ['area' => 1, 'start_month' => 1, 'crop_area_unit_type' => 'katha']);
        $crop = $this->project->crops->first();
        $hectare_area = CropArea::convertCropAreaToUnit($crop,'hectare');
        $this->assertEquals($this->one_katha, $hectare_area, $this->message, $this->tolerance);
    }

    public function test_one_bigha_conversion_into_hectare()
    {
        $this->project->crops()->attach($this->project, ['area' => 1, 'start_month' => 1, 'crop_area_unit_type' => 'bigaha']);
        $crop = $this->project->crops->first();
        $hectare_area = CropArea::convertCropAreaToUnit($crop,'hectare');
        $this->assertEquals($this->one_bigaha, $hectare_area, $this->message, $this->tolerance);
    }

    public function test_one_ropani_conversion_into_hectare()
    {
        $this->project->crops()->attach($this->project, ['area' => 1, 'start_month' => 1, 'crop_area_unit_type' => 'ropani']);
        $crop = $this->project->crops->first();
        $hectare_area = CropArea::convertCropAreaToUnit($crop,'hectare');
        $this->assertEquals($this->one_ropani, $hectare_area, $this->message, $this->tolerance);
    }
}

