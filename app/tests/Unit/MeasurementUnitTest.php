<?php

namespace Tests\Unit;

use Tests\TestCase;

class MeasurementUnitTest extends TestCase
{
    
    public function testFieldReturn()
    {
        $value = displayUnitFormat("amount");
        $this->assertTrue($value == "Rs");
    }
    
    public function testValueAndFieldReturn()
    {
        $value = displayUnitFormat("amount", 20);
        $this->assertTrue($value == "Rs 20");
    }
    
    public function testValueReturn()
    {
        $value = displayUnitFormat("wrong_field", 20);
        $this->assertTrue($value == 20);
    }
    
    public function testValueDecimalReturn()
    {
        $value = displayUnitFormat("boring_size", 20.56);
        $this->assertTrue($value == "21 inches");
    }
    
    public function testValueAndPluralFieldReturn()
    {
        $value = displayUnitFormat("boring_size", 20);
        $this->assertTrue($value == "20 inches");
    }
    
    public function testValueAndSingularFieldReturn()
    {
        $value = displayUnitFormat("boring_size", 1);
        $this->assertTrue($value == "1 inch");
    }
    
    public function testCommaOnLargeValueReturn()
    {
        $value = displayUnitFormat("boring_size", 10020);
        $this->assertTrue($value == "10,020 inches");
    }
}