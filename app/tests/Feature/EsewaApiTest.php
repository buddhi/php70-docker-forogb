<?php
//
//namespace Tests\Feature;
//
//use Illuminate\Http\Request;
//use Tests\TestCase;
//use Illuminate\Foundation\Testing\WithFaker;
//use Illuminate\Foundation\Testing\RefreshDatabase;
//
//class EsewaApiTest extends TestCase
//{
//
//    private static $successPostParams = [
//        "requestId"       =>"m9843487012",
//        "amount"           => 100,
//        "transactionCode" => "abc",
//        "transactionDate" => "2018/01/05 04:11:00"
//    ];
//
//    // Inquiry Endpoint
//    public function testSuccessInquiryHasStatus200()
//    {
//        $response = $this->get('/api/esewa/m9843487012');
//        $response->assertStatus(200);
//    }
//    public function testSuccessInquiryReturnsRequestId()
//    {
//        $response = $this->get('/api/esewa/3');
//        $response->assertJsonFragment([ 'requestId' => "3" ]);
//    }
//    public function testSuccessInquiryReturnsMessage()
//    {
//        $response = $this->get('/api/esewa/m9843487012');
//        $response->assertJson([ 'message' => "SUCCESS" ]);
//    }
//    public function testFailedInquiryReturnsCodeAs1()
//    {
//        $response = $this->get('/api/esewa/d5');
//        $response->assertJson([ 'code' => 1]);
//    }
//    public function testSuccessInquiryReturnsAmount()
//    {
//        $response = $this->get('/api/esewa/m9843487012');
//        $response->assertJsonFragment([ 'amount' ]);
//    }
//
////    // Payment endpoint
//    public function testSuccessPaymentHasStatus200()
//    {
//        $response = $this->post('/api/esewa/payment', ['requestId' =>"m9843487012"]);
//        $response->assertStatus(200);
//    }
//    public function testSuccessPaymentReturnsExpected()
//    {
//        $response = $this->post('/api/esewa/payment', self::$successPostParams);
//        $response->assertJson([ "requestId" => "m9843487012" ]);
//
//    }
//    public function testFailedPaymentReturnsCode()
//    {
//        $response = $this->post('/api/esewa/payment',[]);
//        $response->assertJson([ 'code' => 1 ]);
//    }
//    public function testSuccessPaymentReturnsAmount()
//    {
//        $response = $this->post('/api/esewa/payment', self::$successPostParams);
//        $response->assertJson([ "amount" => (string)self::$successPostParams["amount"] ]);
//    }
//}