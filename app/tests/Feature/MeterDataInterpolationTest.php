<?php

namespace Tests\Feature;

use App\Models\EnrichMeterData;
use App\Models\Meter;
use App\Models\MeterData;
use App\Services\MeterDataRefreshService;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeterDataInterpolationTest extends TestCase
{
    use  RefreshDatabase;
    public $project, $projectStateUpdate;
    private $meter,$meter_data_refresh;
    private $current = 0.65;
    private $voltage = 165.32;
    private $temperature = 17.28;
    private $humidity = 1036.78;
    private $cd_efficiency = 0.85;
    private $phone_no = 9843487012;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->meter = factory(Meter::class)->create();
        $this->meter_data_refresh = new MeterDataRefreshService();
    }

   // Test interpolation works for increasing order timestamps
    public function testMeterDataInterpolationInIncreasingOrder()
    {
        //Generating the meter data for testing interpolation
        $meter_data_before_interpolation = $this->generateRandomMeterDataForDay();
        $this->meter_data_refresh->interpolateMeterData();
        $meter_data_after_interpolation = EnrichMeterData::count();

        //Checking that meter data count is always greater after interpolation
        $this->assertGreaterThanOrEqual($meter_data_before_interpolation, $meter_data_after_interpolation);

        //Checking that interpolated data has greater time stamp
        $enrich_meter_data = EnrichMeterData::all();
        $is_time_reverse_exist = $this->checkTimeReverse($enrich_meter_data);
        $this->assertFalse($is_time_reverse_exist);

        //Checking that interpolated data has greater time stamp filename
        $is_file_name_always_increase = $this->checkFileNameAlwaysLarge($enrich_meter_data);
        $this->assertTrue($is_file_name_always_increase);

    }

    //Testing interpolation works for random previous dates as well
    public function testMeterDataInterpolationInRandomOrder()
    {
        $meter_data_before_interpolation = $this->generateRandomMeterDataForDay();
        $this->meter_data_refresh->interpolateMeterData();
        $meter_data_after_first_interpolation = EnrichMeterData::count();

        //Testing that interpolated meter data is always greater
        $this->assertGreaterThanOrEqual($meter_data_before_interpolation, $meter_data_after_first_interpolation);

        //Generating meter data for previous date(eg from time duration files)
        $meter_data_previous_date = $this->generateRandomMeterDataForDay($previous_date = true);
        $this->meter_data_refresh->interpolateMeterData();
        $meter_data_after_interpolation = EnrichMeterData::count();

        //Testing that interpolated meter data also works for previous dates
        $this->assertGreaterThanOrEqual($meter_data_after_first_interpolation+$meter_data_previous_date, $meter_data_after_interpolation);

        //Checking that interpolated now has no reverse time stamp
        $this->assertFalse($this->checkTimeReverse(EnrichMeterData::all()));
    }

    public function generateRandomMeterDataForDay($previous_day = false)
    {

        $start_date = Carbon::now()->startOfDay();
        $end_date = Carbon::now()->endOfDay();
        if($previous_day){
            $start_date = Carbon::yesterday()->startOfDay();
            $end_date = Carbon::yesterday()->endOfDay();
        }
        $meter_data_count = 0;
        for ($date = $start_date; $date->lte($end_date); $date->addMinutes(rand(1, 9))) {
            $date_value [] = $date->format('ymdHi');
            $meterData = new MeterData();
            $meterData->filename = $this->phone_no . '_' . $date->format('ymdHi');;
            $meterData->current = $this->current;
            $meterData->voltage = $this->voltage;
            $meterData->temperature = $this->temperature;
            $meterData->humidity = $this->humidity;
            $meterData->meter_id = $this->meter->id;
            $meterData->created_at = $date;
            $meterData->save();
            $meter_data_count++;
        }
        return $meter_data_count;
    }


    private function checkTimeReverse($enrich_meter_datas)
    {
        for ($i = 1; $i < ($enrich_meter_datas->count() - 1); $i++) {
            if (Carbon::parse($enrich_meter_datas[$i]->created_at)->lt(Carbon::parse($enrich_meter_datas[$i - 1]->created_at))) {
                return true;
                break;
            };
        }
        return false;
    }

    private function checkFileNameAlwaysLarge($enrich_meter_datas)
    {
        for ($i = 1; $i < ($enrich_meter_datas->count() - 1); $i++) {
            if ((float)explode('_', $enrich_meter_datas[$i]->filename)[1] < (float)explode('_', $enrich_meter_datas[$i - 1]->filename)[1]) {
                return false;
                break;
            };
        }
        return true;
    }

}