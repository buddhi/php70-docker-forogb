<?php

namespace Tests\Feature;

use App\Enums\PaymentType;
use App\Models\FarmerProject;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\TransitionState;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EsewaPaymentApiTest extends TestCase
{
    use  RefreshDatabase;
    protected $project, $farmer, $farm, $user,$headers;
    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed');
        $this->user = factory('App\User')->create();
        $this->project = factory('App\Models\Project')->create();
        $this->project->created_by = $this->user->user_id;
        $this->project->cost = 376000;
        $this->project->payment_id =(string)Carbon::parse($this->project->created_on)->format('ym') . $this->project->id;
        $this->project->save();
        $this->farm = factory('App\Models\Farm')->create();
        $this->farmer = $this->farm->farmer;
        FarmerProject::create([
            'farmer_id' => $this->farmer->id,
            'project_id' => $this->project->id,
        ]);
        $this->headers = [
            'Accept'        => 'application/json',
            'AUTHORIZATION' => env('API_PAYMENT_TOKEN')
        ];

    }
    //INQUIRY ENDPOINT TEST
    public function testSuccessInquiryHasStatus200()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertStatus(200);
    }

    //INQUIRY ENDPOINT TEST
    public function testSuccessInquiryHasStatus401()
    {
        $response = $this->get('/api/esewa/' . $this->project->payment_id);
        $response->assertStatus(401);
    }

    public function testSuccessInquiryReturnsRequestId()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertJsonFragment(['request_id' => $this->project->payment_id]);
    }

    public function testSuccessInquiryReturnsSuccessMessageAndCode0()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertJsonFragment(['response_message' => "SUCCESS"]);
        $response->assertJsonFragment(['response_code' => 0]);
        $response->assertJsonFragment(['amount']);

    }

    public function testSuccessInquiryReturnsMessageWithFarmerNameAndAddress()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertJsonFragment(['farmer_name' => $this->farmer->farmer_name]);
        $response->assertJsonFragment(['address' => $this->farmer->address]);
    }

    public function  getResponseFromInquiryApiEndpoint()
    {
        return $this->get('/api/esewa/' . $this->project->payment_id,$this->headers);
    }

    public function testFailedInquiryReturnsCodeAs1()
    {
        $response = $this->get('/api/esewa/d5');
        $response->assertJson(['response_code' => 1]);
        $response->assertJson(['response_message' => 'Error due to Invalid token.']);
    }

    public function testSuccessInquiryReturnsAdvanceAmountFirst()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertJsonFragment(['amount' => $this->project->advance_amount]);
    }

    public function testSuccessInquiryRejectEmiAmountWithoutAdvanceAmount()
    {
        $response = $this->getResponseFromInquiryApiEndpoint();
        $response->assertJsonFragment(['response_code' => 0]);
    }

    //PAYMENT ENDPOINT TEST
    public function testSuccessPaymentHasStatus200()
    {
        Mail::fake();
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $response->assertStatus(200);
    }

    public function testPaymentReturns401ForAbsenceOfToken()
    {
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount));
        $response->assertStatus(401);
    }

    // @var $esewaPayment1 = Advance Payment: First transaction log
    // @var $esewaPayment2 = Emi First Payment:Second transaction log
    // @var $esewaPayment2 = Emi Second Payment:Third transaction log
    //This test checks the relative of second and third transaction log
    //This test also checks automatic transition-state update
    public function testRelativePaymentLogsInEsewaPaymentLog()
    {
        Mail::fake();
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $this->assertCount(1, Payment::all());
        $esewaPayment1 = Payment::first();
        $this->assertEquals(PaymentType::ADVANCE, $esewaPayment1->payment_type);
        $this->assertEquals($this->project->advance_amount, $esewaPayment1->amount);
        $this->assertEquals($this->project->id, $esewaPayment1->project_id);
        $this->assertNull ($esewaPayment1->emi_start_month);
        $this->assertNull($esewaPayment1->emi_start_month);


        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(),$this->headers);
        $this->assertCount(2, Payment::all());
        $esewaPayment2 = Payment::all()->last();
        $this->assertEquals(PaymentType::EMI, $esewaPayment2->payment_type);
        $this->assertEquals($this->project->emi_amount, $esewaPayment2->amount);
        $this->assertEquals($this->project->id, $esewaPayment2->project_id);

        //Checking the update of transition states after the emi and advance payment
        $this->checkEmiAdvancePaidTransitionStateUpdate();

        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(),$this->headers);
        $this->assertCount(3, Payment::all());
        $esewaPayment3 = Payment::all()->last();
        $this->assertEquals(PaymentType::EMI, $esewaPayment3->payment_type);
        $this->assertEquals($this->project->emi_amount, $esewaPayment3->amount);
        $this->assertEquals($this->project->id, $esewaPayment3->project_id);
    }

    public function getPaymentParamsAPi($amount  = null)
    {
        if(!isset($amount)){
            $amount = $this->project->emi_amount;
        }
        $successPostParams = [
            "request_id" => $this->project->payment_id,
            "amount" => $amount,
            "transaction_code" => "abc",
        ];
        return $successPostParams;

    }

    public function testFailedPaymentReturnsCode()
    {
        Mail::fake();
        $response = $this->post('/api/esewa/payment', [],$this->headers);
        $response->assertJson(['response_code' => 1]);
    }

    //Test the payment are not allowed to happen after the emi amount has been paid
    public function testEmiCanBePaidOnlyFixedNumberOfTime()
    {
        Mail::fake();
        $total_count = $this->project->emi_count;
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $response->assertJson(["request_id" => $this->project->payment_id]);
        $this->assertCount(1, Payment::all());

        for ($i = 1; $i <= $total_count + 1; $i++) {
            $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(),$this->headers);
            if ($i == $total_count + 1) {
                $value = $response->getContent();
                $result = json_decode($value, true);
                $this->assertEquals($result['response_code'], 1);
            }
        }
        $total_payment_count = (int)$total_count+ 1;
        $this->assertCount($total_payment_count, Payment::all());
    }

    public function testAdvanceCanBePaidOnlyOnce()
    {
        Mail::fake();
        $total_count = $this->project->emi_count;
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $response->assertJson(["request_id" => $this->project->payment_id]);
        $this->assertCount(1, Payment::all());

        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $response->assertJson(['response_code' => 1]);
    }

    public function testSuccessPaymentHasStoredTransaction()
    {
        Mail::fake();
        $response = $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount),$this->headers);
        $transaction = Transaction::first();
        $this->assertCount(1,Transaction::all());
        $this->assertEquals("abc",$transaction->transaction_id);
        $response->assertJson(['response_code' => 0]);
    }

    public function testFailedPaymentStoresTransaction()
    {
        Mail::fake();
        $response = $this->post('/api/esewa/payment', ["transaction_code" => "random"],$this->headers);
        $this->assertCount(1,Transaction::all());
        $this->assertEquals("random",Transaction::first()->transaction_id);
        $response->assertJson(['response_code' => 1]);
    }

    public function checkEmiAdvancePaidTransitionStateUpdate()
    {
        //fetching all the transition states
        $updated_transition_states = $this->project->transitionstate;
        //Advance payment + Emi payment
        $this->assertCount(2, $updated_transition_states);
        $advance_paid_transition_state = TransitionState::where('title', 'collected_advance_and_emi_for_first_month')->first();
        $emi_paid_transition_state = TransitionState::where('title', 'first_emi_collected')->first();

        //checking that updated transition state has advance paid transition state updated
        $this->assertTrue($updated_transition_states->contains($advance_paid_transition_state));

        //checking that updated transition state has emi paid transition state updated
        $this->assertTrue($updated_transition_states->contains($emi_paid_transition_state));
    }
}

