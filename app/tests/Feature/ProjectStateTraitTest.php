<?php

namespace Tests\Feature;

use App\Models\TransitionState;
use App\Repositories\Project\ProjectStateUpdate;
use App\Repositories\Sms\SmsRepository;
use App\Services\ProjectStateUpdateTrigger;
use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectStateTraitTest extends TestCase
{
    use  RefreshDatabase;
    public $project, $projectStateUpdate;
    protected function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'TransitionStateTableSeeder']);
        $this->project = factory('App\Models\Project')->create();
        $projectStateUpdateTriggerMock = $this->createMock(ProjectStateUpdateTrigger::class);;
        $this->projectStateUpdate = new ProjectStateUpdate($projectStateUpdateTriggerMock);

    }

    public function testNextStatus()
    {
        $states = array_keys(config('transition.states'));
        $count = count($states);
        foreach ($states as $key => $state) {
            if ($key + 1 == $count) {
                continue;
            }
            $this->assertEquals($states[$key + 1], $this->getNextStatus($state));
        }
    }

    public function testTotalTransitionStateCountForStatus()
    {
        $states = array_keys(config('transition.states'));
        foreach ($states as $key => $state) {
            $transitional_state_count = TransitionState::where('status', $this->getNextStatus($state))->count();
            $actual_count = $this->project->totalTransitionStateCount($this->project->status);
            $this->assertEquals($transitional_state_count, $actual_count);
        }
    }

    public function testCurrentTransitionStateCountForPlanState()
    {
        $states = array_keys(config('transition.states'));
        $count = count($states);
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id, 'new');
        $actual_count = $this->project->currentTransitionStateCount('plan');
        $next_state_count = count(getTransitionStates('new'));
        $this->assertEquals($actual_count, $next_state_count);
    }

    public function testCurrentTransitionStateCountForNewState()
    {
        $states = array_keys(config('transition.states'));
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id, 'approved');
        $actual_count = $this->project->currentTransitionStateCount('new');
        $next_state_count = count(getTransitionStates('approved'));
        $this->assertEquals($actual_count, $next_state_count);
    }

    public function testTotalTransitionStateCount()
    {
        $states = array_keys(config('transition.states'));
        $count = count($states);
        foreach ($states as $key => $state) {
            if ($key + 1 == $count) {
                continue;

            }
            $actual_count = $this->project->totalTransitionStateCount($state);
            $expected_count = TransitionState::where('status', $states[$key + 1])->count();
            $this->assertEquals($expected_count, $actual_count);
        }
    }

    public function getNextStatus($status)
    {
        $this->project->status = $status;
        $this->project->save();
        return $this->project->getNextStatus($this->project->status);
    }
}
