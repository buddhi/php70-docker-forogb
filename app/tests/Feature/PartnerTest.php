<?php
//
//namespace Tests\Feature;
//
//use App\Enums\Role;
//use App\Models\Partner;
//use App\Models\Token;
//use Illuminate\Foundation\Testing\RefreshDatabase;
//use Illuminate\Foundation\Testing\WithFaker;
//use Illuminate\Support\Facades\Bus;
//use Tests\TestCase;
//
//class PartnerTest extends TestCase
//{
//    private $partner, $token, $partner_form;
//    use WithFaker, RefreshDatabase;
//    public function setUp()
//    {
//        parent::setUp();
//        $this->artisan('migrate');
//        Bus::fake();    //prevents job queue of email from being dispatched
//        $this->partner = factory(Partner::class)->create();
//        $this->token = factory(Token::class)->create();
//        $this->partner_form = factory(Partner::class)->make()->toArray();
//    }
//
//    //test: admin can create partner
//    public function testAdminCanCreatePartner()
//    {
//        $this->signInUserWithRole(Role::ADMIN);
//        $response = $this->submitPartnerCreationForm();
//        $response
//            ->assertStatus(200)
//            ->assertJson(['messages' => 'Partner is successfully created.']);
//    }
//
//    //test: other role cannot create partner
//    public function testDeveloperCannotCreatePartner()
//    {
//        $this->signInUserWithRole(Role::DEVELOPER);
//        $response = $this->submitPartnerCreationForm();
//        $response
//            ->assertStatus(302)
//            ->assertRedirect(route('admin.login'));
//    }
//
//    //test: when partner is created ,token is generated
//    public function testCreatingPartnerGeneratesNewToken()
//    {
//        $this->assertCount(1, Token::all());
//        $this->signInUserWithRole(Role::ADMIN);
//        $this->submitPartnerCreationForm();
//        $this->assertCount(2, Token::all());
//    }
//
//    //test: email verification
//    public function testEmailVerificationForPartner()
//    {
//        //bind partner with token
//        $this->partner->tokens()->save($this->token);
//        $response = $this->get(route('ogbadmin.partner.verify', $this->token->token));
//        $response
//            ->assertSessionHas('success', 'Your email is verified successfully')
//            ->assertSee($this->partner->name)
//            ->assertSee($this->partner->email)
//            ->assertSee($this->partner->contact_person);
//    }
//
//    //Testing of polymorphic relation of partner with token
//    public function testGeneratedTokenBelongsToPartner()
//    {
//        $this->partner->tokens()->save($this->token);
//        $associated_partner = $this->token->tokenable;
//        $this->assertSame($this->partner->name, $associated_partner->name);
//    }
//
//    public function testAdminCanViewEditPartnerPage()
//    {
//        $this->signInUserWithRole(Role::ADMIN);
//        $response =$this->get(route('ogbadmin.partner.edit', $this->partner->id));
//        $response
//            ->assertStatus(200)
//            ->assertSee($this->partner->name)
//            ->assertSee($this->partner->contact_person)
//            ->assertSee($this->partner->email);
//    }
//
//    public function testAdminCanUpdatePartnerDetails()
//    {
//        $this->signInUserWithRole(Role::ADMIN);
//        $update_details = [
//            'address' => $this->faker->address,
//            'landline' => $this->faker->phoneNumber,
//            'phone' => $this->faker->phoneNumber,
//        ];
//        $this->post(route('ogbadmin.partner.update', $this->partner->id),$update_details);
//        $this->assertDatabaseHas('partners', $update_details);
//    }
//
//    public function submitPartnerCreationForm()
//    {
//        return $this->post(route('ogbadmin.partner.store'), $this->partner_form);
//    }
//
//    public function testAdminCanChangePartnerStatus()
//    {
//        $this->signInUserWithRole(Role::ADMIN);
//        //asserting that disabled state is 0
//        $this->assertEquals($disabled = 0,$this->partner->disabled);
//
//        //disabling partner
//        $response = $this->post(route('ogbadmin.partner.change-status', ['partner_id' => $this->partner->id]));
//        $response->assertJson(
//            [
//                "partner_status" => "Deactivated"]
//        );
//
//        //enabling partner
//        $response = $this->post(route('ogbadmin.partner.change-status', ['partner_id' => $this->partner->id]));
//        $response->assertJson(
//            [
//                "partner_status" => "Activated"
//            ]
//        );
//
//    }
//}
