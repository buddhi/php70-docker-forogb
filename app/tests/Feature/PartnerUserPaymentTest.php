<?php

namespace Tests\Feature;

use App\Enums\PaymentType;
use App\Enums\Role;
use App\Models\Payment;
use App\Models\Project;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PartnerUserPaymentTest extends TestCase
{
    use WithFaker,RefreshDatabase;
    private $partner, $token, $payment_form,$project;
    public function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        Bus::fake();    //prevents job for email from being dispatched"
        $this->project = factory(Project::class)->create();
        $this->payment_form = [
            'payment_id' => $this->project->payment_id,
            'paymentGateway' =>  'cash',
            'payment_date' => Carbon::now(),
            'amount' => $this->project->emi_amount
        ];
    }

//    public function testPaymentPage()
//    {
//
//        $this->signInUserWithRole(Role::PARTNER_USER);
//        $response = $this->call('GET',route('ogbadmin.payment.index'));
//        $response
//            ->assertStatus(200)
//            ->assertSee('Payment');
//
//    }

    public function testPaymentCreatePage()
    {
        $this->signInUserWithRole(Role::PARTNER_USER);
        $response = $this->call('GET',route('ogbadmin.payment.create'));
        $response
            ->assertStatus(200)
            ->assertSee('Payment');

    }

    public function testPaymentStore()
    {
        $this->assertCount(0,Payment::all());
        $this->assertCount(0,Transaction::all());
        $this->signInUserWithRole(Role::PARTNER_USER);
        $response = $this->post(route('ogbadmin.payment.store'),$this->payment_form);
        $this->assertCount(1,Transaction::all());
        $this->assertCount(1,Payment::all());
        $response->assertStatus(302)
            ->assertRedirect(route('ogbadmin.payment.index'));

        //after first payment
        $first_payment = Payment::first();
        $this->assertEquals(PaymentType::ADVANCE,$first_payment->payment_type);
        $this->assertEquals('cash', $first_payment->payment_gateway);


        //after second payment
        $response = $this->post(route('ogbadmin.payment.store'),$this->payment_form);
        $this->assertCount(2,Payment::all());
        $this->assertCount(2,Transaction::all());
        $second_payment = Payment::all()->last();
        $this->assertEquals(PaymentType::EMI,$second_payment->payment_type);
        $this->assertEquals('cash', $second_payment->payment_gateway);
    }


}
