<?php

namespace Tests\Feature;

use GuzzleHttp\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HttpEndPointTest extends TestCase
{

    public function test(){
        $response = $this->json('POST', '/api/ingest/meter_data', ['name'=>'Hemant','age'=>4,'salary'=>'$400']);
        $response->assertStatus(200);
    }

}
