<?php

namespace Tests\Feature;

use App\Enums\Role;

use App\Models\Farm;
use App\Models\Farmer;
use App\Models\FarmerProject;
use App\Models\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvestmentTest extends TestCase
{
    use  RefreshDatabase;
    protected $project, $farmer, $farm, $user;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed');
        $this->user = factory('App\User')->create();
        $this->project = factory('App\Models\Project')->create();
        $this->project->created_by = $this->user->user_id;
        $this->project->cost = 376000;
        $this->project->save();
        $this->farm = factory('App\Models\Farm')->create();
        $this->farmer = $this->farm->farmer;
        FarmerProject::create([
            'farmer_id' => $this->farmer->id,
            'project_id' => $this->project->id,
        ]);

    }

    public function test_investor_can_enter_detail_page()
    {

        $this->assertCount(1, Farm::all());
        $this->assertCount(1, Farmer::all());
        $this->assertCount(1, Project::all());
        $this->signInUserWithRole(Role::INVESTOR, $this->user);
        $response = $this->get(route('project.detail', [$this->project->id, 'new']));
        $response->assertStatus(200);

    }

    public function test_investor_can_pay_via_paypal()
    {
        $this->signInUserWithRole(Role::INVESTOR, $this->user);
        $valid_payment_data = [
            'project_id' => [1],
            'invest_amount' => [1],
            'invest_percent' => [1]
        ];
        $response = $this->post(route('investment.checkout.store'), $valid_payment_data);
        $paypal_url = $response->getTargetUrl();
        $this->assertTrue($this->url_exists($paypal_url));
        $this->assertTrue($this->checkThatUrlHasPaypalWord($paypal_url));
        $response->assertSessionHas('data');
        $response->assertSessionHas('payment');

    }

    public function test_investor_can_enter_checkout_page()
    {
        $this->signInUserWithRole(Role::INVESTOR, $this->user);
        $response = $this->get(route('investment.checkout'));
        $response->assertStatus(200);
    }

    function url_exists($url)
    {
        if (!$fp = curl_init($url)) return false;
        return true;
    }

    function checkThatUrlHasPaypalWord($paypal_url)
    {
        if (strpos($paypal_url, 'paypal') !== false) {
            return true;
        }
        return false;
    }


}
