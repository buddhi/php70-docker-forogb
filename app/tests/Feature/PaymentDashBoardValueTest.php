<?php

namespace Tests\Feature;

use App\Enums\PaymentType;
use App\Models\Payment;
use App\Models\Project;
use App\Repositories\ApiPayment\ApiPayment;
use App\Repositories\ApiPayment\Esewa\EsewaPaymentService;
use App\Repositories\CashPayment\CashPaymentService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentDashBoardValueTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    private $partner, $token, $payment_form, $project, $apiPaymentRepo, $headers;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        Bus::fake();
        $this->apiPaymentRepo = new ApiPayment($this->createMock(EsewaPaymentService::class), $this->createMock(CashPaymentService::class));
    }

    public function testFetchPaymentsFunction()
    {
        $project = factory(Project::class)->create();

        //First emi Payment
        factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);

        //after the first emi payment , number of payment is 1
        $payments = $this->apiPaymentRepo->fetchPayments(Project::all());
        $this->assertEquals($payments->count(), 1);

        //after second emi payment
        factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);

        //after the second payment , number of payment is 2
        $payments = $this->apiPaymentRepo->fetchPayments(Project::all());
        $this->assertEquals($payments->count(), 2);

    }

//    public function testMonthlyAmountRaisedFunction()
//    {
//
//        $project = factory(Project::class)->create();
//
//        //First emi Payment
//        factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);
//
//        //after the first payment ,amount raised == emi amount
//        $amount_raised = $this->apiPaymentRepo->amountRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
//
//        $expected_amount_raised = [
//                "total" => $project->emi_amount,
//                "regular" => $project->emi_amount,
//                "overdue" => 0,
//                "down_payment" => 0,
//
//        ];
//        $this->assertSame(array_diff($expected_amount_raised, $amount_raised), array_diff($amount_raised, $expected_amount_raised));
//
//
//        //after second emi payment
//        factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);
//
//        //after the second payment ,amount raised == 2* emi amount
//        $amount_raised = $this->apiPaymentRepo->amountRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
//
//        $expected_amount_raised = [
//            "total" => 2 *$project->emi_amount,
//            "regular" => 2* $project->emi_amount,
//            "overdue" => 0,
//            "down_payment" => 0,
//
//        ];
//        $this->assertSame(array_diff($expected_amount_raised, $amount_raised), array_diff($amount_raised, $expected_amount_raised));
//
//    }


    // public function testAmountToBeRaisedInBsMonthFunction()
    // {
    //     $project = factory(Project::class)->create();
    //     factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);
    //     $payment_array = $this->apiPaymentRepo->amountToBeRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
    //     $expected = [
    //         "total_regular_emi_count" => 1, //1 emi payment
    //         "total_overdue_emi_count" => 0, // since paid today no overdue
    //         "total_regular_emi_amount" => $project->emi_amount,
    //         "total_overdue_emi_amount" => 0.0,//// since paid today no overdue
    //         "total_overdue_amount" => $project->emi_amount //regular(first emi) + overdue amount
    //     ];
    //     $this->assertSame(array_diff($expected, $payment_array), array_diff($payment_array, $expected));

    //     //Prepayment results  in same array as there is no increase in regular or overdue emi
    //     factory(Payment::class)->create(['project_id' => $project->id, 'payment_type' => PaymentType::EMI, 'amount' => $project->emi_amount]);
    //     $payment_array = $this->apiPaymentRepo->amountToBeRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
    //     $this->assertSame(array_diff($expected, $payment_array), array_diff($payment_array, $expected));
    // }

//    public function testAmountToBeRaisedInBsMonthForPreviousMonths()
//     {

//        //start date is before 2 months ie 3 months payment needs to be done
//        $emi_start_date = Carbon::now()->startOfMonth()->subMonth(2)->subDays(10);
//        $project = factory(Project::class)->create(['emi_start_date' => $emi_start_date]);
//        $payment_array = $this->apiPaymentRepo->amountToBeRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
//        $expected = [
//            "total_regular_emi_count" => 1, // 1 emi payment this month
//            "total_overdue_emi_count" => 2, // 2 emi month of before
//            "total_regular_emi_amount" => 1 * $project->emi_amount,
//            "total_overdue_emi_amount" => 2 * $project->emi_amount,//// since paid today no overdue
//            "total_overdue_amount" => 3 * $project->emi_amount //regular(first emi) + overdue amount
//        ];
//        $this->assertSame(array_diff($expected, $payment_array), array_diff($payment_array, $expected));

//        //Making payment doesnt affect on monthly payment
//        factory(Payment::class)->create(
//            ['project_id' => $project->id,
//                'payment_type' => PaymentType::EMI,
//                'amount' => $project->emi_amount,
//                'emi_start_month' => $emi_start_date,
//                'emi_end_month' => $emi_start_date->addMonth(1)
//            ]);
//        $payment_array = $this->apiPaymentRepo->amountToBeRaisedInBsMonth(Project::all(), Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
//        $this->assertSame(array_diff($expected, $payment_array), array_diff($payment_array, $expected));
//     }

}

