<?php

namespace Tests\Feature;

use App\Enums\Role;
use App\Models\Otp;
use App\Models\Partner;
use App\Models\Token;
use App\Models\UserRegister;
use App\User;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Agent extends TestCase
{
    use WithFaker,RefreshDatabase;
    private $partner, $token, $temp_partner_user_form,$agent_form;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        Bus::fake();    //prevents job for email from being dispatched"
        $this->partner = factory(Partner::class)->create();
        $this->temp_partner_user_form = ['email'=> $this->faker->email,'partner_id' => $this->partner->id];
        $this->agent_form = [
          'password' => 'secret',
          'password_confirmation' => 'secret',
          'phone' => $this->faker->phoneNumber,
          'full_name' => $this->faker->name
        ];
    }

    public function testAdminCanAccessAgentsTabOnDashboard()
    {
        $this->signInUserWithRole(Role::ADMIN);
        $response = $this->get(route('ogbadmin.agent.index'));
        $response
            ->assertStatus(200)
            ->assertSee('Agents');
    }

    public function testAdminCanCreateTemporaryAgent()
    {
        $this->assertCount(0,UserRegister::all());
        $this->signInUserWithRole(Role::ADMIN);
        $response = $this->post(route('agent.store.temp-user'),$this->temp_partner_user_form);
        $response
            ->assertStatus(200)
            ->assertJson([
                'error' => false,
                'messages' => 'Email is sent successfully'
            ]);
        $this->assertCount(1,UserRegister::all());
        $this->assertDatabaseHas('user_registers' ,$this->temp_partner_user_form);
    }

    public function testPartnerUserCanCreateTemporaryAgent()
    {
        $this->assertCount(0,UserRegister::all());
        $this->signInUserWithRole(Role::PARTNER_USER);
        $response = $this->post(route('agent.store.temp-user'),$this->temp_partner_user_form);
        $response
            ->assertStatus(200)
            ->assertJson([
                'error' => false,
                'messages' => 'Email is sent successfully'
            ]);
        $this->assertCount(1,UserRegister::all());
        $this->assertDatabaseHas('user_registers' ,$this->temp_partner_user_form);
    }

//    public function testAdminCannotCreateTemporaryAgent()
//    {
//        $this->signInUserWithRole(Role::DEVELOPER);
//        $response = $this->post(route('ogbadmin.partner-user.store-temp-user'),$this->temp_partner_user_form);
//        $response
//            ->assertStatus(302)
//            ->assertRedirect(route('admin.login'));
//    }



    public function testEmailVerificationRedirectsAgentCreationPage()
    {
        $userRegister = factory(UserRegister::class)->create();
        $token = factory(Token::class)->create();
        $userRegister->tokens()->save($token);
        $this->assertGuest();
        $response = $this->get(route('partner-user.verify',$token->token));
        $response
            ->assertSee('Enter full name:')
            ->assertSessionHas('success', 'Email verified successfully.');
    }
//
    /* @test */
    //Testing of storing of new partner-user
    //Testing of deleting of old temp user when new partner user is added
    public function testStoringOfPartnerAgentAndDeletingOfTempUser()
    {
        $userRegister = factory(UserRegister::class)->create(['role'=> Role::DEVELOPER]);
        $response = $this->post(route('agent.store'),array_merge($this->agent_form,['user_register_id' => $userRegister->id]));
        $response->assertRedirect(route('verify-otp.form',$userRegister->id));
        $this->assertDatabaseHas('app_user', ['email' => $userRegister->email, 'partner_id' => $userRegister->partner_id]);
        $this->assertDatabaseMissing('user_registers', $userRegister->toArray());
    }

    //test verify otp page
    public function testOtpVerificationPage()
    {
        $otp = factory(Otp::class)->create();
        $user = $otp->user;
        $this->disableCookiesEncryption('user_details');
        $cookies = [
            'user_details' => json_encode(['user_id' => $user->user_id]),
        ];
        $response = $this->call('get', route('verify-otp.form',$user->user_id), [], $cookies);
        $response
            ->assertSee($user->phone)
            ->assertSee('Verify Mobile');

    }

    //test otp verification
    public function testOtpVerification()
    {
        $otp = factory(Otp::class)->create();
        $user = $otp->user;
        $input_digit = str_split($otp->otp);
        $this->disableCookiesEncryption('user_details');
        $cookies = [
            'user_details' => json_encode(['user_id' => $user->user_id]),
        ];
        $response = $this->call('POST',route('verify-otp',$user->user_id),
            [
                'user_id'=>$user->user_id ,
                'digit1' => (int)$input_digit[0],
                'digit2' => (int)$input_digit[1],
                'digit3' => (int)$input_digit[2],
                'digit4' => (int)$input_digit[3],
                'digit5' => (int)$input_digit[4],
                'digit6' => (int)$input_digit[5],
            ],$cookies);
       $this->assertEquals(1,User::first()->otp_verified);
    }

    public function testAdminCanChangeAgentStatus()
    {
        $this->signInUserWithRole(Role::ADMIN);

        //asserting that disabled state is 0
        $user = factory(User::class)->create();
        $this->assertEquals($disabled = 0,$user->disabled);

        //disabling user
        $response = $this->post(route('agent.change-status', ['partner_user_id' => $user->user_id]));
        $response->assertJson(
            [
                "user_status" => "Deactivated"
            ]
        );

        //enabling user
        $response = $this->post(route('agent.change-status', ['agent' => $user->user_id]));
        //asserting that user status is updated and changed to sthing
        $response->assertJsonMissing(
            [
                "user_status" => "Deactivated"
            ]
        );

    }
}
