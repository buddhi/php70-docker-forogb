<?php

namespace Tests\Feature;

use App\Enums\Role;
use App\Enums\Verification;
use App\Models\Partner;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;
    public $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }

    public function test_user_can_view_a_loginForm()
    {
        $response = $this->get($this->loginGetRoute());
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function test_user_cannot_view_login_form_when_authenticated()
    {
        $this->user = factory(User::class)->make();
        $this->user->verified = Verification::EMAIL_VERIFIED;
        $this->user->admin_verification = Verification::ADMIN_VERIFIED;
        $response = $this->actingAs($this->user)->withSession(['role' => Role::INVESTOR])->get($this->loginGetRoute());
        $response->assertRedirect($this->guestMiddlewareRoute());
    }

    public function testDeveloperCanLoginWithCorrectCredentials()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::DEVELOPER);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role'=>Role::DEVELOPER,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLoginRoute(Role::DEVELOPER));
    }

    public function testInvestorCanLoginWithCorrectCredentials()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::INVESTOR);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role'=>Role::INVESTOR,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertCount(1, $users = User::all());
        $response->assertRedirect($this->successfulLoginRoute(Role::INVESTOR));
    }

    public function testAdminCanLoginWithCorrectCredentials()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::ADMIN);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $response->assertRedirect($this->successfulLoginRoute(Role::ADMIN));

    }

    public function testUserCannotLoginWithEmailThatDoesNotExist()
    {
        $response = $this->from($this->loginGetRoute())->post($this->loginPostRoute(), [
            'email' => 'nobody@example.com',
            'password' => 'invalid-password',
        ]);
        $response->assertRedirect($this->loginGetRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }
    public function testUserCannotLoginWithIncorrectPassword()
    {
        $registration_form =$this->createRegistrationFormWithCertainRole(Role::ADMIN);
        $this->submitRegistrationForm($registration_form);
        $response = $this->from($this->loginGetRoute())->post($this->loginPostRoute(), [
            'email' => $this->user->email,
            'password' => 'invalid-password',
        ]);
        $response->assertRedirect($this->loginGetRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function testDeveloperCanLogout()
    {
        $this->signInUserWithRole(Role::DEVELOPER);
        $response = $this->post($this->logoutRoute());
        $response->assertRedirect($this->successfulLogoutRoute(Role::DEVELOPER));
        $this->assertGuest();
    }

    public function testInvestorCanLogout()
    {
        $this->signInUserWithRole(Role::INVESTOR);
        $response = $this->post($this->logoutRoute());
        $response->assertRedirect($this->successfulLogoutRoute(Role::INVESTOR));
        $this->assertGuest();
    }

    public function testAdminCanLogout()
    {
        $this->signInUserWithRole(Role::ADMIN);
        $response = $this->post($this->logoutRoute());
        $response->assertRedirect($this->successfulLogoutRoute(Role::ADMIN));
        $this->assertGuest();
    }

    public function testUserCannotLogoutWhenNotAuthenticated()
    {
        $response = $this->post($this->logoutRoute());
        $response->assertRedirect($this->successfulLogoutRoute(Role::DEVELOPER));
        $this->assertGuest();
    }

    private function submitRegistrationForm($registration_form)
    {
        $response =$this->post(route('register'),
            $registration_form
        );
        return $response;
    }


    protected function loginGetRoute()
    {
        return route('login');
    }

    private function guestMiddlewareRoute()
    {
        return route('project.list');
    }

    protected function loginPostRoute()
    {
        return route('login');
    }

    private function successfulLoginRoute($role,$partner_id= null)
    {
        if($role == Role::ADMIN) {
            return route('admin.project.list');
        }

        if($role == Role::PARTNER_USER) {
            return route('partner-user.project.index');
        }
        return route('ogbadmin.project.index');
    }
    public function createRegistrationFormWithCertainRole($role)
    {
        $this->user->verified = Verification::EMAIL_VERIFIED;
        $this->user->admin_verification = Verification::ADMIN_VERIFIED;
        $registration_form = array_merge(
            $this->user->toArray(),
            [   'role' => [$role],
                'password' => 'secret',
                'email'=>$this->user->email,
                'password_confirmation' =>'secret',
            ]);
        return $registration_form;
    }
    protected function logoutRoute()
    {
        return route('logout');
    }
    
    protected function successfulLogoutRoute($role)
    {
        if($role == Role::ADMIN) {
            return route('admin.login');
        }
        if($role == Role::PARTNER_USER) {
            return route('partner-user.login');
        }
        if($role == Role::INVESTOR) {
            return route('index');
        }
        return route('login');
    }

    public function testOGBAdminCanLoginWithCorrectCredentials()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole(Role::ADMIN);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'email' => $this->user->email,
            'password' => 'secret',
            'admin_type' => 'ogbadmin'
        ]);
        $response->assertRedirect(route('ogbadmin.project.index'));
    }

    public function testPartnerUserCanLoginWithCorrectCredentials()
    {
        $partner = factory(Partner::class)->create();
        $this->user->partner_id = $partner->id;
        $registration_form = $this->createRegistrationFormWithCertainRole(Role::PARTNER_USER);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role' => Role::PARTNER_USER,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $response->assertRedirect($this->successfulLoginRoute(Role::PARTNER_USER, $partner->id));
    }

    public function testPartnerUserCanLogout()
    {
        $this->signInUserWithRole(Role::PARTNER_USER);
        $response = $this->post($this->logoutRoute());
        $response->assertRedirect($this->successfulLogoutRoute(Role::PARTNER_USER));
        $this->assertGuest();
    }

    public function testSoftDeletedDeveloperCannotLogin()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole(Role::DEVELOPER);
        $registration_form = array_merge($registration_form, ['disabled' => 1]);
        $this->submitRegistrationForm($registration_form);
        $response = $this->post($this->loginPostRoute(), [
            'role' => Role::DEVELOPER,
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $this->assertEquals(1, \DB::table('app_user')->count());
        $this->assertGuest();
        $response->assertRedirect(route('contact'));
    }
}
