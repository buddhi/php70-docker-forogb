<?php

namespace Tests\Feature;

use App\Enums\ProjectStatus;
use App\Models\FarmerProject;
use App\Models\Meter;
use App\Models\MeterData;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectOperationCycleTest extends TestCase
{
    use  RefreshDatabase;
    protected $project, $farmer, $farm, $user, $meter;
    private $current = 0.65;
    private $voltage = 165.32;
    private $temperature = 17.28;
    private $humidity = 1036.78;
    private $phone_no = 9843487012;

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed');
        $this->user = factory('App\User')->create();
        $this->project = factory('App\Models\Project')->create();
        $this->project->created_by = $this->user->user_id;
        $this->project->cost = 376000;
        $this->project->status = ProjectStatus::OPERATIONAL;
        $this->project->payment_id = (string)Carbon::parse($this->project->created_on)->format('ym') . $this->project->id;
        $this->project->emi_start_date = Carbon::now()->subMonth(1)->subDay(5);
        $this->project->save();

        $this->farm = factory('App\Models\Farm')->create();
        $this->farmer = $this->farm->farmer;
        $farmer_project = FarmerProject::create([
            'farmer_id' => $this->farmer->id,
            'project_id' => $this->project->id,
        ]);
        $this->meter = factory(Meter::class)->create();
        $this->meter->farmer_project_id = $farmer_project->id;
        $this->meter->save();
        $this->headers = [
            'Accept' => 'application/json',
            'AUTHORIZATION' => env('API_PAYMENT_TOKEN')
        ];

    }

    //testing 'operational' <==> 'error' cycle
    public function testOperationalErrorTransition()
    {
        //Emi start date set early to avoid overdue state since we are checking  'operational' <==> 'error' cycle.
        //emi_start_date = Carbon::now()->subMonth(1)->subDay(5) in constructor to handle overdue state;
        $this->project->emi_start_date = Carbon::tomorrow();
        $this->project->save();

        Bus::fake();//Prevents sending emails while testing
        $this->assertEquals(ProjectStatus::OPERATIONAL, Project::first()->status);


        //Testing 'operational' ==> 'error' transition
        $this->artisan('check:operational-cycle');
        $this->assertEquals(ProjectStatus::ERROR, Project::first()->status, "Operational ==> error transition");

        //Testing 'error' ==> 'operational' transition
        //func:generateRandomMeterDataForDay to make sure that project isnot in error state
        $this->generateRandomMeterDataForDay();

        $this->artisan('check:operational-cycle');
        $this->assertEquals(ProjectStatus::OPERATIONAL, Project::first()->status, "error ==> operational transition");

    }

    //testing 'operational' <==> 'Overdue' cycle
    public function testingOperationalOverdueTransition()
    {
        Bus::fake();//Prevents sending emails while testing
        $this->assertEquals(Project::first()->status, ProjectStatus::OPERATIONAL);

        //Advance payment
        $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount), $this->headers);
        //Emi payment
        $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(), $this->headers);

        //Testing 'operational' ==> 'overdue' transition
        $this->generateRandomMeterDataForDay();
        $this->artisan('check:operational-cycle');
        $this->assertEquals(ProjectStatus::OVERDUE, Project::first()->status, "Operational ==> overdue transition");

        //Testing 'overdue' ==> 'operational' transition
        $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(), $this->headers);
        $this->assertEquals(ProjectStatus::OPERATIONAL, Project::first()->status, "Overdue ==> Operational transition");
    }

    //testing 'Error' <==> 'Overdue' cycle
    public function testingErrorOverdueTransition()
    {
        Bus::fake();//Prevents sending emails while testing
        $this->assertEquals(Project::first()->status, ProjectStatus::OPERATIONAL);

        //Advance payment
        $this->post('/api/esewa/payment', $this->getPaymentParamsAPi($this->project->advance_amount), $this->headers);
        //Emi payment
        $this->post('/api/esewa/payment', $this->getPaymentParamsAPi(), $this->headers);

        //Testing 'error' ==> 'overdue' transition
        $this->artisan('check:operational-cycle');
        $this->assertEquals(ProjectStatus::ERROR, Project::first()->status, "Error ==> Overdue transition");

        //Testing 'overdue' ==> 'error' transition
        $this->generateRandomMeterDataForDay();
        $this->artisan('check:operational-cycle');
        $this->assertEquals(ProjectStatus::OVERDUE, Project::first()->status, "Overdue ==> Error transition");


    }

    //testing that emi start date if not defined project willnot be in overdue.
    public function testAbsenceOfEmiStartDateWillNotOverDueProject()
    {

        //Project wiil be overdue if we set the startdate of yesterday as first emi isnot paid
        $this->project->emi_start_date = Carbon::yesterday();
        $this->project->save();
        $isOverdue =$this->project->isEmiPending();
        $this->assertTrue($isOverdue);

        //Project wont be overdue if we set the emi start date isnot defined
        $this->project->emi_start_date = null;
        $this->project->save();
        $isOverdue =$this->project->isEmiPending();
        $this->assertFalse($isOverdue);
    }

    //Generates meter data
    public function generateRandomMeterDataForDay()
    {

        $start_date = Carbon::now()->subMinutes(5);
        $end_date = Carbon::now()->addMinutes(5);
        for ($date = $start_date; $date->lte($end_date); $date->addMinutes(rand(1, 9))) {
            $date_value [] = $date->format('ymdHi');
            $meterData = new MeterData();
            $meterData->filename = $this->phone_no . '_' . $date->format('ymdHi');;
            $meterData->current = $this->current;
            $meterData->voltage = $this->voltage;
            $meterData->temperature = $this->temperature;
            $meterData->humidity = $this->humidity;
            $meterData->meter_id = $this->meter->id;
            $meterData->created_at = $date;
            $meterData->save();
        }
    }

    public function getPaymentParamsAPi($amount = null)
    {
        if (!isset($amount)) {
            $amount = $this->project->emi_amount;
        }
        $successPostParams = [
            "request_id" => $this->project->payment_id,
            "amount" => $amount,
            "transaction_code" => "abc",
        ];
        return $successPostParams;

    }
}
