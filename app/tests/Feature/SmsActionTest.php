<?php
//
//namespace Tests\Feature;
//
//use Tests\TestCase;
//use GuzzleHttp\Client;
//use GuzzleHttp\Handler\MockHandler;
//use GuzzleHttp\HandlerStack;
//use GuzzleHttp\Psr7\Response;
//
//class SmsActionTest extends TestCase
//{
//    private $mock, $handler, $client;
//    protected function setUp()
//    {
//        parent::setUp();
//
//        // Create a mock and queue two responses.
//        $this->mock = new MockHandler([
//           new Response(200, ['X-Foo' => 'Bar']),
//           new Response(500, ['Content-Length' => 0])
//        ]);
//        $this->handler = HandlerStack::create($this->mock);
//        $this->client = new Client(['handler' => $this->handler]);
//        $this->app->instance(\GuzzleHttp\Client::Class, $this->client);
//        $this->authenticateAsUser();
//    }
//
//    public function testDemoEndpointWithSuccessAndFailure(){
//        $response = $this->post('/demo/smsAction', ['phone' => 12345, 'value' => 10]);
//        $response->assertSee("User notified successfully");
//
//        $response = $this->post('/demo/smsAction', ['phone' => 12345, 'value' => 10]);
//        $response->assertSee("SMS Gateway unresponsive");
//    }
//}
