<?php

namespace Tests\Feature;

use App\Enums\Role;
use App\Models\Project;
use App\Models\TransitionState;
use App\Repositories\Project\ProjectStateUpdate;
use App\Repositories\Sms\SmsRepository;
use App\Services\ProjectStateUpdateTrigger;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectStateTest extends TestCase{

    use  RefreshDatabase;
    public $projectStateUpdate,$project,$smsRepo;
    protected function setUp()
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed', ['--class' => 'TransitionStateTableSeeder']);
        $this->project = factory('App\Models\Project')->create();
        $this->signInUserWithRole(Role::DEVELOPER);
        $projectStateUpdateTriggerMock = $this->createMock(ProjectStateUpdateTrigger::class);;
        $this->projectStateUpdate = new ProjectStateUpdate($projectStateUpdateTriggerMock);
    }

    /**
     *
     */
    public function testProjectUpdateNewStatusHasOneTransitionState()
    {
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id ,'new');
        $transitional_states_count = count($this->project->transitionstate);
        $this->assertEquals(1,$transitional_states_count);
    }

    public function testProjectUpdateApprovedStatusHasFourTransitionState()
    {

        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id ,'approved');
        $transitional_states_count = count($this->project->transitionstate);
        $this->assertEquals(4,$transitional_states_count);
    }

    public function testProjectUpdateFundingStatusHasThreeTransitionState()
    {
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id ,'funding');
        $transitional_states_count = count($this->project->transitionstate);
        $this->assertEquals(3,$transitional_states_count);
    }

    public function testProjectUpdateFundedStatusHasThreeTransitionState()
    {
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id ,'funded');
        $transitional_states_count = count($this->project->transitionstate);
        $this->assertEquals(1,$transitional_states_count);
    }

    public function testProjectStateUpdate()
    {
        $this->assertDatabaseHas('tbl_project', [
            'farmer_name' => $this->project->farmer_name
        ]);
        $this->projectStateUpdate->updateTransitionStateOfProject($this->project->id ,'new');
        $transitional_states = $this->project->transitionstate;
        foreach ($transitional_states as $transitional_state){
            $this->assertDatabaseHas('project_transition_states', [
                'project_id' => $this->project->id,
                'transition_state_id' => $transitional_state->id
            ]);
        }

    }

}