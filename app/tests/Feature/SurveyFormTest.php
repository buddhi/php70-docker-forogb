<?php

namespace Tests\Feature;

use App\Enums\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SurveyFormTest extends TestCase
{
    use  RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();
    }

    public function test_guest_cannot_enter_survey_form_create_page()
    {
        $this->get(route('project.create'))->assertRedirect(route('index'));
    }

    public function test_investors_cannot_enter_survey_form_create_page()
    {
        $this->signInUserWithRole(Role::INVESTOR);
        $this->get(route('project.create'))->assertRedirect(route('error'));
    }

    public function test_admins_cannot_enter_survey_form_create_page()
    {
        $this->signInUserWithRole(Role::ADMIN);
        $this->get(route('project.create'))->assertRedirect(route('error'));
    }

    public function test_developer_can_store_Survey_Form()
    {
        $this->signInUserWithRole(Role::DEVELOPER);
        $project = factory('App\Models\Project')->make();
        $farmer = factory('App\Models\Farmer')->make();
        $farm = factory('App\Models\Farm')->make();
        $farm->farmer_id = null;
        $survey_form = array_merge($project->toArray(), $farmer->toArray(), $farm->toArray(), ['submit' => 'submit-analysis']);
        $response = $this->submitSurveyForm($survey_form);

        $this->assertDatabaseHas('tbl_project', [
            'farmer_name' => $farmer->farmer_name
        ]);
        $this->assertDatabaseHas('tbl_project', [
            'farmer_name' => $farmer->farmer_name
        ]);
    }

    public function test_investor_cannot_submit_Survey_Form()
    {
        $this->signInUserWithRole(Role::INVESTOR);
        $project = factory('App\Models\Project')->make();
        $farmer = factory('App\Models\Farmer')->make();
        $farm = factory('App\Models\Farm')->make();
        $farm->farmer_id = null;
        $survey_form = array_merge($project->toArray(), $farmer->toArray(), $farm->toArray(), ['submit' => 'submit-analysis']);
        $response = $this->submitSurveyForm($survey_form);
        $response->assertRedirect(route('error'));
    }

    public function test_admin_cannot_submit_Survey_Form()
    {
        $this->signInUserWithRole(Role::ADMIN);
        $project = factory('App\Models\Project')->make();
        $farmer = factory('App\Models\Farmer')->make();
        $farm = factory('App\Models\Farm')->make();
        $farm->farmer_id = null;
        $survey_form = array_merge($project->toArray(), $farmer->toArray(), $farm->toArray(), ['submit' => 'submit-analysis']);
        $response = $this->submitSurveyForm($survey_form);
        $response->assertRedirect(route('error'));
    }

    public function submitSurveyForm($survey_form)
    {
        return $this->post(route('project.store'), $survey_form);
    }
}
