<?php

namespace Tests\Feature;

use App\Enums\Role;
use App\Enums\Verification;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;
    public $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }

    public function test_user_can_view_registration_form()
    {
        $response = $this->get($this->getUserRegistrationRoute());
        $response->assertSuccessful();
        $response->assertViewIs('auth.register');
    }

    public function test_user_can_register_with_test_data()
    {
        $registration_form_with_developer_role = [
                'username' => 'ogb-test',
                'full_name' =>'ogb-test',
                'phone' => '9843487012',
                'email' => 'ogb_test@gmail.com',
                'remember_token' => str_random(10),
                'address' => 'address',
                'role' => [Role::DEVELOPER],
                'password' =>  'test-password',
                'password_confirmation' => 'test-password',
            ];
        $response = $this->post(route('register'),
            $registration_form_with_developer_role
        );

        $this->assertDatabaseHas('app_user', [
            'email' => 'ogb_test@gmail.com'
        ]);

        $this->assertCount(1, $users = User::all());
    }

    public function test_user_cannot_view_a_registration_form_when_authenticated()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $this->submitForm($registration_form);
        $this->assertDatabaseHas('app_user', [
            'email' => $this->user->email
        ]);
        $response = $this->actingAs($this->user)->get($this->getUserRegistrationRoute());
        $response->assertRedirect($this->guestMiddlewareRoute());
    }

    public function test_user_can_register()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $this->submitForm($registration_form);
        $this->assertDatabaseHas('app_user', [
            'email' => $this->user->email
        ]);
        $users =User::all();
        $this->assertCount(1, $users);
        $this->assertGuest();
    }

    public function test_user_cannot_register_without_input_email()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $registration_form_missing_email =array_merge($registration_form,['email'=>'']);
        $response =$this->post(route('register'),$registration_form_missing_email);
        $this->assertDatabaseMissing('app_user', [
            'email' => $this->user->email
        ]);
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('full_name'));
        $this->assertFalse(session()->hasOldInput('password'));
        $users =User::all();
        $this->assertCount(0, $users);
        $this->assertGuest();
    }

    public function test_user_cannot_register_without_input_Password()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $registration_form_missing_password =array_merge($registration_form,['password'=>'']);
        $response =$this->post(route('register'),$registration_form_missing_password);
        $this->assertDatabaseMissing('app_user', [
            'email' => $this->user->email
        ]);
        $response->assertSessionHasErrors('password');

        $users =User::all();
        $this->assertCount(0, $users);
        $this->assertGuest();
    }

    public function test_user_cannot_register_without_Password_confirmation()
    {

        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $registration_form_without_Password_confirmation =array_merge($registration_form,['password_confirmation'=>'']);
        $response =$this->post(route('register'),$registration_form_without_Password_confirmation);
        $this->assertDatabaseMissing('app_user', [
            'email' => $this->user->email
        ]);
        $response->assertSessionHasErrors('password');
        $users =User::all();
        $this->assertCount(0, $users);
        $this->assertGuest();
    }

    public function test_user_cannot_register_without_Password_Matching()
    {
        $registration_form = $this->createRegistrationFormWithCertainRole($this->user,Role::DEVELOPER);
        $registration_form_with_wrong_password =array_merge($registration_form,['password_confirmation'=>'wrong-secret']);
        $response =$this->post(route('register'),$registration_form_with_wrong_password);
        $this->assertDatabaseMissing('app_user', [
            'email' => $this->user->email
        ]);
        $response->assertSessionHasErrors('password');
        $users =User::all();
        $this->assertCount(0, $users);
        $this->assertGuest();
    }

    public function test_user_can_register_with_test_data_with_both_roles()
    {
        $registration_form_with_developer_role = [
            'username' => 'ogb-test',
            'full_name' =>'ogb-test',
            'phone' => '9843487012',
            'email' => 'ogb_test@gmail.com',
            'remember_token' => str_random(10),
            'address' => 'address',
            'role' => [Role::DEVELOPER,Role::INVESTOR],
            'password' =>  'test-password',
            'password_confirmation' => 'test-password',
        ];
        $response = $this->post(route('register'),
            $registration_form_with_developer_role
        );

        $this->assertDatabaseHas('app_user', [
            'email' => 'ogb_test@gmail.com'
        ]);

        $this->assertCount(1, $users = User::all());
    }

    private function getUserRegistrationRoute()
    {
        return route('register');
    }

    protected function registerPostRoute()
    {
        return route('register');

    }

    private function guestMiddlewareRoute()
    {
        return route('project.list');
    }

    public function createRegistrationFormWithCertainRole($user,$role)
    {
        $this->user->verified = Verification::EMAIL_VERIFIED;
        $this->user->admin_verification = Verification::ADMIN_VERIFIED;
        $registration_form = array_merge($this->user->toArray(),
            ['role' => [$role],
                'password' => 'secret',
                'email'=>$this->user->email,
                'password_confirmation' =>'secret',
            ]);
        return $registration_form;
    }

    private function submitForm($registration_form)
    {
        $response =$this->post(route('register'),
            $registration_form
        );
        return $response;
    }


}