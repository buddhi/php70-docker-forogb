Vue.filter('round',function(value,place){
    if(value === 0) return 0;
    if (!value) return '';
    if(place > 0){
        return value.toFixed(place);
    }else{
        return Math.round(value,place)
    }
});
Vue.filter('convert_flow',function(value){
    if(value/1 <= 1){
        return ((value/1).toFixed(1) + ' L');
    }else if(value/1000 <= 1000){
        return ((value/1000).toFixed(1) + ' KL');
    }else{
        return ((value/1000000).toFixed(2) + ' ML');
    }
});

Vue.filter('format_date',function(value){
    var today = new Date();
    var d = new Date(value),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    minute = d.getMinutes();
    hour = d.getHours();
    formatted_minute = (minute<10?'0':'') + minute;
    formatted_hour = (hour<10?'0':'') + hour;
    return [formatted_hour, formatted_minute].join(':');
});
Vue.filter('get_day',function(value){
    var today = new Date();
    var d = new Date(value);
    var timeDiff = Math.abs(today.getTime() - d.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    month = '' + (d.getMonth() + 1);
    day = '' + d.getDate();
    year = d.getFullYear();
    month = month > 9 ? "" + month: "0" + month;
    day = day > 9 ? "" + day: "0" + day;
    year = year > 9 ? "" + year: "0" + year;
    minute = d.getMinutes();
    hour = d.getHours();
    return year + ":" + month + ":" + day ;
});
const Totals = Vue.component('totals',{
    template : `
                <div class="row">
                    <div class="col-md-3 col-sm-3 center positioning">
                     <span class="fa fa-tint" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                     <span style = "font-size:40px;display:inline-block">{{ flow | convert_flow }}</span>
                     <span class = "next-subtitleSub-sub" ><em>Total Discharge</em></span>
                    </div>
                    <div class="col-md-3 col-sm-3 center positioning">
                        <span class="fa fa-bolt" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                        <span style = "font-size:40px;display:inline-block">{{ runtime | round(1)}} hr</span>
                        <span class = "next-subtitleSub-sub" ><em>Total runtime</em></span>
                    </div>
                    <div class="col-md-3 col-sm-3 center positioning">
                        <div v-if="a">
                            <div v-if="a === null || a.length <= 0">
                                 <span class="fa fa-clock-o" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                                <span style = "font-size:40px;display:inline-block">No data</span>
                            </div>
                            <div v-else>
                                <div v-if="firstFlow === null">
                                    <span class="fa fa-clock-o" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                                    <span style = "font-size:40px;display:inline-block">No data</span>
                                </div>
                                <div v-else>
                                    <span class="fa fa-clock-o" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                                    <span style = "font-size:40px;display:inline-block">{{ firstFlow | format_date }}</span>
                                    <span class = "next-subtitleSub-sub" ><em>First Discharge</em></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 center positioning">
                        <div v-if="a" >
                            <span class="fa fa-clock-o" aria-hidden="true" style = "font-size:40px;color:#4687ee"></span>
                            <span style = "font-size:40px;display:inline-block">{{ lastFlow | format_date }}</span>
                            <div v-if ="todayDischarge">
                               <span class = "next-subtitleSub-sub" ><em>{{ lastFlow | get_day }}</em></span>
                            </div>
                        </div>
                    </div>
                </div>
           `,
    props : ['runtime','flow','first-flow','last-flow','a','today-discharge'],


});
const Loader = Vue.component('Loader',{
    props: ['loaded'],
    template:  `
            <div class = "loading" v-show="!loaded">
                <div class="loader"></div>
            </div>
           `

});
const Chart = Vue.component('Chart', {
    props: [ 'title', 'current','voltage','dcpower','acpower','discharge','rpm','frequency','temperature','humidity','a','id','meter_type'],
    template: `
            <div>
                <div id="thechart"></div>
            </div>
            `,
    data(){
        return{
            chart:'line'
        }
    },
    methods:{
        redraw(){
            if(this.indicator === 'daily'){
                this.chart.series[0].setData(this.current,true);
                this.chart.series[1].setData(this.voltage,true);
                this.chart.series[2].setData(this.dcpower,true);
                this.chart.series[3].setData(this.acpower,true);
                this.chart.series[4].setData(this.discharge,true);
                this.chart.series[5].setData(this.rpm,true);
                this.chart.series[6].setData(this.frequency,true);
                this.chart.series[7].setData(this.temperature,true);
                this.chart.series[8].setData(this.humidity,true);
            }
        },
    },
    watch:{
        title(){this.redraw()},
        current(){this.redraw()},
        voltage(){this.redraw()},
        dcpower(){this.redraw()},
        acpower(){this.redraw()},
        discharge(){this.redraw()},
        rpm(){this.redraw()},
        frequency(){this.redraw()},
        temperature(){this.redraw()},
        humidity(){this.redraw()},
    },
    mounted() {
        var date = new Date(this.a[0].created_at);
        var meter_type = this.meter_type
        var highchartsOptions = {
            chart: {
                type: 'line',
                renderTo: 'thechart',
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'time'
                },
                type: 'datetime',
                min: Date.UTC(date.getFullYear(),date.getMonth(),date.getDate(),0,0),
                max: Date.UTC(date.getFullYear(),date.getMonth(),date.getDate(),23,59),

                dateTimeLabelFormats : {
                    hour: '%I %p',
                    minute: '%I:%M %p'
                }
            },
            yAxis: [{
                title: { text: 'I(amp)'},
                max: 10,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 400,
                tickInterval: 100,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 5000,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 5000,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 22,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 3000,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 50,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 100,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            },{
                title: { text: ''},
                max: 2000,
                labels: { formatter: function() { return this.value }},
                opposite: false,
            }
            ],
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                series: {
                    events: {
                        show: (function() {
                            var chartData = {CURRENT:"I(amp)", VOLTAGE:"Volt(volt)", DCPOWER:'P(watt)',ACPOWER:'P(watt)',DISCHARGE:'Q(l/s)',RPM:'rpm',FREQUENCY:'F(hz)',TEMPERATURE:'Temp(celsius)', HUMIDITY:'Humidity'};
                            var chart = this.chart,
                                series = chart.series,
                                i = series.length,
                                otherSeries;
                            chart.yAxis[0].setTitle({text: chartData[this.name]});
                            while(i--) {
                                otherSeries = series[i];
                                if (otherSeries != this && otherSeries.visible) {
                                    otherSeries.hide();
                                }
                            }
                        }),

                    }
                }
            },
            series: [{
                name: 'CURRENT',
                data: this.current,
            },{
                name: 'VOLTAGE',
                data: this.voltage,
                visible:false,
                yAxis: 1,
            },{
                name: 'DCPOWER',
                data: this.dcpower,
                visible:false,
                yAxis: 2,
            },{
                name: 'ACPOWER',
                data: this.acpower,
                visible:false,
                yAxis: 3,
            },{
                name: 'DISCHARGE',
                data: this.discharge,
                visible:false,
                yAxis: 4,
            },{
                name: 'RPM',
                data: this.rpm,
                visible:false,
                yAxis: 5,
            },{
                name: 'FREQUENCY',
                data: this.frequency,
                visible:false,
                yAxis: 6,
             }
              ,{
                name: 'TEMPERATURE',
                data: this.temperature,
                visible:false,
                yAxis: 7,
            },{
                name: 'HUMIDITY',
                data: this.humidity,
                visible:false,
                yAxis: 8,
            }
            ],
            credits: false
        }
        if( meter_type !== 'agriculture_meter'){
          highchartsOptions.series.splice(7,2);
        }
        this.chart = new Highcharts.chart(highchartsOptions)
    }
});
const Charts = Vue.component('Charts', {
    props: [ 'aggregateDischarge','aggregateRunTime','indicator','selectedYear','selectedMonth'],
    template: `
            <div>
                <div id="thechart"></div>
            </div>
            `,
    data(){
        return{
            chart:'line'
        }
    },
    methods:{
        redraw(){
            this.chart.series[0].setData(this.aggregateDischarge,true);
            this.chart.series[1].setData(this.aggregateRunTime,true);
            if(this.indicator === "monthly"){
                highchartsOptions = {
                    chart: {
                        type: 'column',
                        renderTo: 'thechart'
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: true,
                        pointFormat: "Value: {point.y:.1f}"
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            // month: '%e. %b',
                            year: '%b',
                        },
                        min: Date.UTC(this.selectedYear,this.selectedMonth,0),
                        max: Date.UTC(this.selectedYear,this.selectedMonth,30)
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Kilo Liters'
                        }

                    },
                    plotOptions: {
                        series: {
                            events: {
                                show: (function() {
                                    var chartData = {Discharge:"Liters", Runtime:"Minutes"};
                                    var chart = this.chart,
                                        series = chart.series,
                                        i = series.length,
                                        otherSeries;
                                    chart.yAxis[0].setTitle({text: chartData[this.name]});
                                    while(i--) {
                                        otherSeries = series[i];
                                        if (otherSeries != this && otherSeries.visible) {
                                            otherSeries.hide();
                                        }
                                    }
                                }),
                                click: (function() { this.chart.yAxis[0].setTitle({text: this.name}); })

                            }
                        },
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Discharge',
                        data: this.aggregateDischarge

                    },{
                        name: 'Runtime',
                        data: this.aggregateRunTime,
                        visible : false

                    }]
                }
            }
            else{
                highchartsOptions = {
                    chart: {
                        type: 'column',
                        renderTo: 'thechart'
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: true,
                        pointFormat: "Value: {point.y:.1f}"
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            // month: '%e. %b',
                            year: '%b',
                        },
                        min: Date.UTC(this.selectedYear,0,0),
                        max: Date.UTC(this.selectedYear,11,30)
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Kilo Liters'
                        }

                    },
                    plotOptions: {
                        series: {
                            events: {
                                show: (function() {
                                    var chartData = {Discharge:"Liters", Runtime:"Minutes"};
                                    var chart = this.chart,
                                        series = chart.series,
                                        i = series.length,
                                        otherSeries;
                                    chart.yAxis[0].setTitle({text: chartData[this.name]});
                                    while(i--) {
                                        otherSeries = series[i];
                                        if (otherSeries != this && otherSeries.visible) {
                                            otherSeries.hide();
                                        }
                                    }
                                }),
                                click: (function() { this.chart.yAxis[0].setTitle({text: this.name}); })

                            }
                        },
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Discharge',
                        data: this.aggregateDischarge

                    },{
                        name: 'Runtime',
                        data: this.aggregateRunTime,
                        visible : false

                    }]
                }
            }
            this.chart = new Highcharts.chart(highchartsOptions)
        },
    },
    watch:{
        aggregateRunTime(){this.redraw()},
        indicator(){this.redraw()},
    },
    mounted() {
        if(this.indicator === "monthly"){
             highchartsOptions = {
                chart: {
                    type: 'column',
                    renderTo: 'thechart'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: "Value: {point.y:.1f}"
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        // month: '%e. %b',
                        year: '%b',
                    },
                    min: Date.UTC(this.selectedYear,this.selectedMonth,0),
                    max: Date.UTC(this.selectedYear,this.selectedMonth,30)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Kilo Liters'
                    }

                },
                plotOptions: {
                    series: {
                        events: {
                            show: (function() {
                                var chartData = {Discharge:"Liters", Runtime:"Minutes"};
                                var chart = this.chart,
                                    series = chart.series,
                                    i = series.length,
                                    otherSeries;
                                chart.yAxis[0].setTitle({text: chartData[this.name]});
                                while(i--) {
                                    otherSeries = series[i];
                                    if (otherSeries != this && otherSeries.visible) {
                                        otherSeries.hide();
                                    }
                                }
                            }),
                            click: (function() { this.chart.yAxis[0].setTitle({text: this.name}); })

                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Discharge',
                    data: this.aggregateDischarge

                },{
                    name: 'Runtime',
                    data: this.aggregateRunTime,
                    visible : false

                }]
            }
        }
        else{
             highchartsOptions = {
                chart: {
                    type: 'column',
                    renderTo: 'thechart'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: "Value: {point.y:.1f}"
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        // month: '%e. %b',
                        year: '%b',
                    },
                    min: Date.UTC(this.selectedYear,0,0),
                    max: Date.UTC(this.selectedYear,11,30)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Kilo Liters'
                    }

                },
                plotOptions: {
                    series: {
                        events: {
                            show: (function() {
                                var chartData = {Discharge:"Liters", Runtime:"Minutes"};
                                var chart = this.chart,
                                    series = chart.series,
                                    i = series.length,
                                    otherSeries;
                                chart.yAxis[0].setTitle({text: chartData[this.name]});
                                while(i--) {
                                    otherSeries = series[i];
                                    if (otherSeries != this && otherSeries.visible) {
                                        otherSeries.hide();
                                    }
                                }
                            }),
                            click: (function() { this.chart.yAxis[0].setTitle({text: this.name}); })

                        }
                    },
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Discharge',
                    data: this.aggregateDischarge

                },{
                    name: 'Runtime',
                    data: this.aggregateRunTime,
                    visible : false

                }]
            }
        }
        this.chart = new Highcharts.chart(highchartsOptions)
    },

});

new Vue({
    el: '#app',
    data: {
        a: null,
        id : null,
        totalflow :this.updateTotalFlow ,
        totalruntime : this.updateTotalRunTime,
        aggregate_flow_runtime: null,
        runtime: null,
        titleValue : 'Amp(I)',
        date : null,
        firstDischarge : 'a',
        lastDischarge : null,
        lastDataDate : null,
        loaded : false,
        todayData: false,
        project : null,
        indicator : 'daily',
        chartdata : null,
        dailyData: null,
        months : {0:'Jan',1:'Feb',2:'Mar',3:'Apr',4:'May',5:'Jun',6:'Jul',7:'Aug',8:'Sep',9:'Oct',10:'Nov',11:'Dec'},
        year : null,
        selectedDate : null,
        selectedYear : null,
        selectedMonth : null,
        difference_in_hours: null,
        daysBasedYearMonth:null,
        dischargedToday: true,
    },
    methods: {
        getNumbers:function(start,stop){
            return new Array(stop-start).fill(start).map((n,i)=>n+i);
        },
        updateindicator(value){
            this.loaded = false;
            this.extractData(value).then(() => {
                this.indicator = value;
                this.loaded = true;
                if(this.a.length <= 0 && this.indicator !== 'daily'){
                    this.todayData = true;
                }else if(this.a.length > 0){
                    this.todayData = true;
                }else{
                    this.todayData = false;
                }
            });
        },
        loadTodayData() {
            var vm = this;
            return axios.get('/api/meterdata/'+meter_id).then(function(response){
                vm.a = response.data[0];
                vm.lastDischarge = response.data[1];
                vm.lastDataDate = response.data[4];
                vm.project = response.data[2];
                vm.aggregate_flow_runtime = response.data[3];
            });
        },
        loadData(ind,date){
            date = date || this.a[0].created_at;
            vm = this;
            return axios({
                method: 'post',
                url: '/api/meterdata/filter/'+meter_id,
                data: {
                    indicator: ind,
                    date: date,
                }
            }).then(response => {
                vm.a = response.data[0];
                vm.lastDischarge = response.data[1];
            });
        },
        extractData(ind, date){
            var selectedDate = new Date(this.selectedYear,this.selectedMonth,this.selectedDate);
            date = date || selectedDate;
            vm = this;
            return axios({
                method: 'post',
                url: '/api/meterdata/filter/'+meter_id,
                data: {
                    indicator: ind,
                    date: date,
                }
            }).then(response => {
                vm.dailyData = response.data[0];
                vm.lastDischarge = response.data[1];
            });
        },
        changeDays(){
            this.daysBasedYearMonth = this.getNumberOfDays(this.selectedYear,this.selectedMonth)+1;
            this.selectedDate = this.selectedDate < this.daysBasedYearMonth? this.selectedDate:1;
        },
        getNumberOfDays(year, month) {
            var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
            return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        },
        filterChart() {
            this.loaded = false;
            if (this.indicator === 'daily') {
                var selectedDate = new Date(this.selectedYear, this.selectedMonth, this.selectedDate);
                this.loadData(this.indicator, selectedDate).then(() => {
                    this.loaded = true;
                    if (this.a.length > 0) {
                        this.todayData = true;
                    } else {
                        this.todayData = false;
                    }
                });
            } else {
                var selectedDate = new Date(this.selectedYear, this.selectedMonth, this.selectedDate);
                this.extractData(this.indicator, selectedDate).then(() => {
                    this.loaded = true;
                    this.todayData = false;
                    if (this.dailyData.length >= 1) {
                        this.todayData = true;
                    } else {
                        this.todayData = false;
                    }
                });
            }
        },

        extractDate(data){
            var res = data.filename.split("_");
            var time = res[1];
            var parsed_date = new Date(data.created_at);
            if(time.length < 10){
                var minute = time.toString().slice(-2);
                var hour = time.toString().substr(4,2);
            }
            else{
                var minute = parsed_date.getMinutes();
                var hour = parsed_date.getHours();
            }
            var year = parsed_date.getFullYear();
            var month = parsed_date.getMonth();
            var day = parsed_date.getDate();
            date = Date.UTC(year,month,day,hour,minute);
            return date;
        },
        extractDataValue(data,dataType){
            switch(dataType){
                case 'current':
                    return parseFloat(data.current);
                    break;
                case 'voltage':
                    return parseFloat(data.voltage);
                    break;
                case 'dcpower':
                    var dc_power = parseFloat(data.dc_power);
                    return Math.round(dc_power * 100)/100;
                    break;
                case 'acpower':
                    var ac_power = parseFloat(data.ac_power);
                    return Math.round(ac_power * 100)/100;
                    return parseFloat(data.ac_power);
                    break;
                case 'discharge':
                    var discharge = parseFloat(data.discharge);
                    return Math.round(discharge*100)/100;
                    break;
                case 'rpm':
                    return parseFloat(Math.round(data.rpm));
                    break;
                case 'frequency':
                    var frequency = parseFloat(data.frequency);
                    return Math.round(frequency * 100)/100;
                    break;
                case 'temperature':
                    var temperature = parseFloat(data.temperature);
                    return Math.round(temperature * 100)/100;
                    break;
                case 'humidity':
                    var humidity = parseFloat(data.humidity);
                    return Math.round(humidity * 100)/100;
                    break;
                default:
                    break;
            }
        },

        chartData(data,dataType){
            var chartdata = [];
            if(dataType !== 'aggregateRunTime' || dataType !== 'aggregateDischarge'){
                for(i = 0;i<data.length;i++){
                    var date = this.extractDate(data[i]);
                    var dataValue = this.extractDataValue(data[i],dataType);
                    chartdata[i] = [date,dataValue]
                }
            }
            return chartdata;
        }


    },
    mounted(){
        this.loadTodayData().then(() => {
            this.loaded = true;
            let date_now =  new Date();
            let lastDischarge = new Date(this.lastDischarge);
            let lastDataDate = new Date(this.lastDataDate);
            this.difference_in_hours = Math.abs(date_now - lastDataDate)/36e5;
            if(this.a.length > 0){
                this.todayData = true;
                date = new Date(this.a[this.a.length-1].created_at);
                this.selectedDate = date.getDate();
                this.selectedMonth = date.getMonth();
                this.selectedYear = date.getFullYear();
                this.changeDays();
            }else{
                date = new Date();
                this.selectedDate = date.getDate();
                this.selectedMonth = date.getMonth();
                this.selectedYear = date.getFullYear();
                this.changeDays();
            }
        });
    },
    computed: {
        checkTodayDischarge(){
            var d1 = new Date();
            dischargeDate = new Date(this.lastDischarge);
            if(Math.abs(dischargeDate-d1)/(1000) >= 86400){
                return true;
            }
            return false;
        },
        monthArray(){
            var month = [];
            var count = 0;
            for( i =0; i < 12; i++){
                month[count++] = i;
            }
            return month;
        },
        lastFlow(){
            return this.lastDischarge;
        },
        updateTotalRunTime(){
            var count = parseFloat(this.aggregate_flow_runtime.runtime/60);
            var index = Array.from(this.a).filter(a => a.discharge > 0);
            if(index.length > 0){
                this.firstDischarge = index[0].created_at;
            }else{
                this.firstDischarge = null;
            }
            return count;
        },
        updateTotalFlow(){
            var count = parseFloat((this.aggregate_flow_runtime.discharge)*60);
            var index = Array.from(this.a).filter(a => a.discharge > 0);

            if(index.length > 0){
                this.lastDischarge = index[index.length - 1].created_at;
            }
            return count;
        },
        title() { return this.titleValue },
        current() {
            return this.chartData(this.a,'current');
        },
        voltage() {
            return this.chartData(this.a,'voltage');
        },
        dcpower() {
            return this.chartData(this.a,'dcpower');
        },
        acpower() {
            return this.chartData(this.a,'acpower');
        },
        discharge() {
            return this.chartData(this.a,'discharge');
        },
        rpm() {
            return this.chartData(this.a,'rpm');
        },
        frequency() {
            return this.chartData(this.a,'frequency');
        },
        temperature() {
            return this.chartData(this.a,'temperature');
        },
        humidity() {
            return this.chartData(this.a,'humidity');
        },
        aggregateDischarge(){
            var dataset = [];
            for(i = 0;i<this.dailyData.length;i++){
                for(j = 0;j<this.dailyData[i].length;j++){
                    var parsed_date = new Date(this.dailyData[i][j].date);
                    var year = parsed_date.getFullYear();
                    var month = parsed_date.getMonth();
                    var day = parsed_date.getDate();
                    date = Date.UTC(year,month,day);
                    dataset[j] = [date,((parseFloat(this.dailyData[i][j].daily_discharge)*60)/1000)]
                }
            }

            return dataset;
        },
        aggregateRunTime(){
            var dataset = [];
            for(i = 0;i<this.dailyData.length;i++){
                for(j = 0;j<this.dailyData[i].length;j++){
                    var parsed_date = new Date(this.dailyData[i][j].date);
                    var year = parsed_date.getFullYear();
                    var month = parsed_date.getMonth();
                    var day = parsed_date.getDate();
                    date = Date.UTC(year,month,day);
                    dataset[j] = [date,parseFloat(this.dailyData[i][j].daily_runtime)]
                }
            }

            return dataset;
        }

    },
    components: { Chart, Charts, Loader }
});