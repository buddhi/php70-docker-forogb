<?php

return [
    'graphA' => [
        // class of your domain object
        'class' => App\User::class,

        // name of the graph (default is "default")
        'graph' => 'graphA',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'state',

        // list of all possible states
        'states' => [
            'new',
            'pending_review',
            'awaiting_changes',
            'accepted',
            'published',
            'rejected',
        ],

        // list of all possible transitions
        'transitions' => [
            'create' => [
                'from' => ['new'],
                'to' => 'pending_review',
            ],
            'ask_for_changes' => [
                'from' =>  ['pending_review', 'accepted'],
                'to' => 'awaiting_changes',
            ],
            'cancel_changes' => [
                'from' => ['awaiting_changes'],
                'to' => 'pending_review',
            ],
            'submit_changes' => [
                'from' => ['awaiting_changes'],
                'to' =>  'pending_review',
            ],
            'approve' => [
                'from' => ['pending_review', 'rejected'],
                'to' =>  'accepted',
            ],
            'publish' => [
                'from' => ['accepted'],
                'to' =>  'published',
            ],
        ],

        // list of all callbacks
        'callbacks' => [
            // will be called when testing a transition
            'guard' => [
                'guard_on_submitting' => [
                    // call the callback on a specific transition
                    'on' => 'submit_changes',
                    // will call the method of this class
                    'do' => ['MyClass', 'handle'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
            ],

            // will be called before applying a transition
            'before' => [],

            // will be called after applying a transition
            'after' => [],
        ],
    ],
    //State Machine Configuration for loan
    'loan' => [
        // class of your domain object
        'class' => App\Models\Loan::class,

        // name of the graph (default is "default")
        'graph' => 'loan',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'last_state',

        // list of all possible states
        'states' => [
            'ok',
            'processing',
            'notOk',
            'notReachable'
        ],

        // list of all possible transitions
        'transitions' => [
            'paid' => [
                'from' => ['ok','processing','notOk','notReachable'],
                'to' => 'ok',
            ],
            'isDue' => [
                'from' =>  ['ok', 'processing','notOk','notReachable'],
                'to' => 'notOk',
            ],
            'pendingPay' => [
                'from' => ['ok','processing','notOk','notReachable'],
                'to' => 'processing',
            ],

        ],
    ],

    //State Machine Configuration for loan
    'project' => [
        // class of your domain object
        'class' => App\Models\Project::class,

        // name of the graph (default is "default")
        'graph' => 'project',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            'plan',
            'new',
            'approved',
            'funding',
            'funded',
            'operational'
        ],

        // list of all possible transitions
        'transitions' => [
            'plan_new' => [
                'from' => ['plan'],
                'to' => 'new',
            ],
            'new_approved' => [
                'from' =>  ['new'],
                'to' => 'approved',
            ],
            'approved_funding' => [
                'from' => ['approved'],
                'to' => 'funding',
            ],
            'funding_funded' => [
                'from' => ['funding'],
                'to' => 'funded',
            ],
            'funded_operational' => [
                'from' => ['funded'],
                'to' => 'operational',
            ],

        ],
    ],
];
