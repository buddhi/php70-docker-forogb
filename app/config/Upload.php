<?php
return [
    'project' => [
        'farmer_photo' => 'Farmers photo',
        'citizenship' => 'citizenship',
        'land_document' => 'Land related documents (Land ownership certificate and Land lease documents if present)',
        'bank_loan_application' => 'Bank/Loan Application Form Copy',
    ],
];