<?php
return [
    'units' => [
        'ground_water_level' => 'feet',
        'boring_size' => 'inch',
        'boring_depth' => 'feet',
        'vertical_head' => 'meter',
        'distance' => 'meter',
        'distance_to_nearest_market' => 'km',
        'land_size' => 'hectare',
        'amount' => 'Rs',
        'percent' => '%',
        'water_requirement' => 'L/day',
        'yes_no_question'=>'Y/N'
    ]
];
