<?php
return [
    'risk' => [
        'demographic' =>[
            'age','gender','education','certification','no_of_people_in_house','no_of_dependents'
        ],
        'financial'=>[
            'revenues_from_water_distribution',
            'additional_income_source',
            'additional_salaried_occupation',
            'monthly_saving',
            'has_bank',
            'percentage_of_debt_on_ebitda',
            'recent_large_purchase_through_payment_plan',
            'future_capital_expenditure_causing_cash_flow_interruptions',
            'distance_to_nearest_market',
            'land_owned',
            'percentage_of_solar_loan_on_expense',
            'percentage_of_expenses_on_income'
        ],

        // TODO:: Add agricultural risk later
        'agricultural'=>[
            'crop_risk',
            'livestock_risk',
            'crop_diversity',
            'has_cereal_grain',
            'crop_inventory',
            'livestock_diversity'
        ],

        'psychometric'=>[
            'size_fish',
            'neighbour_trust',
            'harvest_lost',
            'village_meeting',
            'farming_skill',
            'image_recall_exercise',
            'question_timing',
            'answered_all_questions',
            'equivocating_question',
        ]
    ],
];
