<?php
return [
    'states' =>
        [
            'plan' =>
                [
                    'form_submission'
                ],
            'new' =>
                [
                    'plan_chosen'
                ],
            'approved' =>
                [
                    'contact_farmer',
                    'review_survey_form',
                    'check_documents',
                    'has_acceptable_risk'
                ],
            'funding' =>
                [
                    'commission_paid_to_agent',
                    'preliminary_contract_between_agent_and_farmer',
                    'collected_advance_and_emi_for_first_month',
                ],
            'funded' =>
                [
                    'percent_check'
                ],
            'installed' =>[
                    'first_emi_collected',
                    'installation_report_collected',
                    'contract_signed_and_received',
                    'multimedia_information for project collected',
                ],
            'operational' =>
                [
                    'insurance_process_completed',
                    'meter_is_live',
                    'no_pending_emi',
                ],
        ],

];