<?php
return [
    'CurrentIrrigationSystem' => [
        'Diesel'=> 1,
         'Electeric' => 2,
         'Canal' => 3,
    ],
    'CurrentIrrigationSource' => [
        'Pond' => 1,
         'Rain'=> 2,
         'River' => 3,
         'Boring'=> 4,
         'Well' => 5,
    ]
];