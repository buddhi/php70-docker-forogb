<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

Route::group(['namespace'=>'Project'],function(){
    Route::get('/',             [ 'as'=>'index','uses' => 'SiteController@index']);
    Route::get('/index',        [ 'as'=>'new.index','uses' => 'SiteController@newIndex'])->middleware(['guest']);;
    Route::get('/error',        [ 'as'=>'error',      'uses' => 'SiteController@error']);
    Route::get('/verification-steps', [ 'as'=>'verification.step', 'uses' => 'SiteController@verificationStep']);
    /*
     * API Route for loading and filtering data to the project-list page. although needs to be pulled in a controller
     */
    Route::get('/all','SiteController@loadData');
    Route::get('/fetch-location','SiteController@loadMapData');
    Route::get('/my-investment','SiteController@loadMyInvestmentProjects');
    Route::get('/filter','SiteController@filterProjects');
    Route::get('/latest','SiteController@latestFundedProjects');

});
Route::group([ 'prefix'=>'admin','namespace'=>'Admin', 'as'=>'admin.'],function(){
    Route::get('login',[ 'as'=>'login',      'uses' => 'SiteController@admin']);
});

//Admin Routes
Route::group(['prefix' => 'admin', 'namespace'=>'Admin','middleware' => ['AdminPanelAuth','DisablePreventBack']], function () {
    include_route_files(__DIR__.'/admin/');
});
// Routes accessible after login
Route::group(['middleware' => ['auth','DisablePreventBack']], function (){
    /*
     * Project Routes'.
     */
    Route::group(['prefix' => 'project','namespace' => 'Project'],function(){
        /* DEVELOPER ROUTES*/
        //Developer Routes to create,read,edit and update project and choose plan
        include_route_files(__DIR__.'/project/');
    });

    //Static Data routes
    Route::get('/data/districts/{province_id}',      [ 'as'=>'data.districts', 'uses' => 'DataController@districtsForProvince']);
    Route::get('/data/municipalities/{district_id}', [ 'as'=>'data.municipalities', 'uses' => 'DataController@municipalityForDistrict']);
    Route::get('/data/{payment_id}',                 [ 'as'=>'data.payment', 'uses' => 'DataController@projectDetailsForPayment']);

    //draft routes
    Route::group(['prefix'=>'draft','as'=>'draft.','namespace'=>'Project'],function(){
        Route::get('create',                   [ 'as'=>'create',  'uses' => 'ProjectController@draftCreate']);
        Route::get('{id}',                     [ 'as'=>'edit',    'uses' => 'ProjectController@draftEdit']);
    });

    //SMS Sms Routes
    Route::group(['namespace' => 'Sms'], function(){
        Route::post('/meter/changestatus',['as'=>'meter.change_status','uses'=>'SmsController@changeMeterStatus'])->middleware('AdminPanelAuth');
    });

});

//Routes that help in authentication
Auth::routes();

Route::get('/user/verify/{token}', ['as'=>'user.verify','uses'=>'Auth\RegisterController@verifyUser']);
Route::post('/profile/image', ['as'=>'profile.image','uses'=>'Profile\ProfileController@uploadProfileImage']);
Route::post('/profile/image/delete', ['as'=>'profile.image.delete','uses'=>'Profile\ProfileController@deleteProfileImage']);

Route::group(['middleware' => ['auth','DisablePreventBack'],'namespace'=>'MeterData'], function() {
    include_route_files(__DIR__.'/meterdata/');
});

Route::get('cashflow/{project_id}',[ 'uses' => 'Project\CashFlowController@calculateCashFlow']);

Route::group(['middleware' => ['auth','DisablePreventBack']], function() {
    Route::get('remark/{project_id}',            [ 'as'=>'remark.index',   'uses' => 'RemarksController@index']);
    Route::post('remark/{project_id}/create',    [ 'as'=>'remark.create',  'uses' => 'RemarksController@create']);
});

//Social login routes
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login');
Route::post('register/social', 'Auth\RegisterController@registerSocialUser')->name('social.register');
Route::get('login/{provider}/callback','Auth\LoginController@Callback');
Route::get('register/social/waiting','Project\SiteController@socialRegisterWaiting')->name('social.register.waiting');
Route::get('verify/email','Project\SiteController@verifyEmail')->name('verify.email');

Route::group(['middleware' => [ 'auth','InvestorPanelAuth','DisablePreventBack']], function (){
    //New Investment routes
    Route::get('/project-fund/{id}',                    [ 'as'=>'project.detail',               'uses' => 'Investment\InvestmentController@investmentDetail']);
    Route::post('investment/invest',                    [ 'as'=>'investment.checkout.store',    'uses' => 'Investment\InvestmentController@submitCheckout']);
    Route::get('investment/status/{paymentGateway}',    ['as'=>'investment.status',             'uses' =>'Investment\InvestmentController@getPaymentStatus']);
    Route::get('investment/payment-success',            [ 'as'=>'payment.success',              'uses' => 'Investment\InvestmentController@paymentSuccess']);
    Route::get('investment/payment-failure',            [ 'as'=>'payment.failure',              'uses' => 'Investment\InvestmentController@paymentFailure']);
    Route::get('investment/my-investment',              [ 'as'=>'my.investment',                'uses' => 'Investment\InvestmentController@myInvestment']);
    Route::get('/investment/checkout',                        [ 'as'=>'investment.checkout', 'uses' => 'Investment\InvestmentController@checkout']);
    Route::post('project_fund/{project_id}/contact/developer', ['uses'=>'Investment\InvestmentController@contactDeveloper']);
});

Route::group(['middleware' => ['auth']], function (){
    Route::get('/setPassword','Auth\PasswordController@setPasswordForm')->name('password.set.form');
    Route::post('/setPassword','Auth\PasswordController@setPassword')->name('password.set');
    Route::get('/changePassword','Auth\ChangePasswordController@showChangePasswordForm')->name('changePassword.form');
    Route::post('/changePassword','Auth\ChangePasswordController@changePassword')->name('changePassword');
    Route::get('/profile', 'Profile\ProfileController@index')->name('profile.edit');
    Route::post('/profile/update', 'Profile\ProfileController@update')->name('profile.update');
});


/* NEW OGB ADMIN DASHBOARD ROUTES**/
//Email verification routes for partner
Route::get('partner/verify/{token}',                     ['as' => 'ogbadmin.partner.verify',       'uses' => 'OgbAdmin\PartnerController@verifyEmail']);

//routes for partner-user
Route::group(['prefix' => 'partner-user', 'as'=> 'partner-user.','namespace'=>'OgbAdmin'], function () {
    include_route_files(__DIR__.'/partner-user/');
});
Route::get('partner-user/login',[ 'as'=>'partner-user.login',      'uses' => 'Admin\SiteController@partnerUserLogin']);

//new admin routes
include(__DIR__.'/ogbadmin/ogbadmin.php');

Route::get('agent/verify/{token}',                     ['as' => 'ogbadmin.agent.verify',       'uses' => 'OgbAdmin\AgentController@verifyEmail']);
//routes for partner-user
Route::group(['prefix' => 'agent', 'as'=> 'agent.','namespace'=>'OgbAdmin'], function () {
    include_route_files(__DIR__.'/agent/');
});

Route::post('agent/store-temp-user',              ['as' => 'agent.store.temp-user',         'uses' => 'OgbAdmin\AgentController@store'])->middleware('AdminAndPartnerUserCheck');
Route::get('agent/resend-email-verification/{userRegister}',              ['as' => 'agent.resend.email',         'uses' => 'OgbAdmin\AgentController@resendEmailVerification'])->middleware('AdminAndPartnerUserCheck');

//Routes related with account edit
Route::group(['middleware' => ['auth','AdminAndPartnerUserCheck']], function (){
Route::post('account/user/change-image',                 ['as' => 'account.change-image',        'uses' => 'OgbAdmin\AccountController@uploadImage']);
Route::get('account/{partner_user_id}/edit',             ['as' => 'account.edit',                'uses' => 'OgbAdmin\AccountController@edit']);
Route::post('account{partner_user_id}/update',           ['as' => 'account.update',              'uses' => 'OgbAdmin\AccountController@update']);
});

include(__DIR__.'/otp/otp.php');