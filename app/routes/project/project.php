<?php
/*
 * All the Project Route with namespace of Project and prefix project is listed here
 */
Route::get('/list', [ 'as'=>'project.list',     'uses' => 'SiteController@projectList']);
Route::get('create',[ 'as'=>'project.create',  'uses' => 'ProjectController@create']);
Route::post('store',[ 'as'=>'project.store',   'uses' => 'ProjectController@projectStore']);
Route::put('store', [ 'as'=>'project.store',   'uses' => 'ProjectController@projectStore']);
Route::get('{id}',[ 'as'=>'project.edit',    'uses' => 'ProjectController@edit']);
Route::put('{id}',[ 'as'=>'project.update',  'uses' => 'ProjectController@update']);
Route::get('{id}/select-plan',[ 'as'=>'project.selectPlan',  'uses' => 'ProjectController@selectPlan']);
Route::post('file/upload/{projectId}',[ 'as'=>'file.upload',  'uses' => 'ProjectController@uploadFile']);
Route::post('select-plan',[ 'as'=>'project.selectPlanStore',  'uses' => 'ProjectController@selectPlanStore']);
Route::get('api/fetch/{id}','ProjectController@fetchWaterRequirement');
Route::get('file/delete/{uploadId}',[ 'as'=>'file.delete',  'uses' => 'ProjectController@deleteFile']);
Route::get('file/get/{project_id}',[ 'as'=>'file.get',  'uses' => 'ProjectController@getFiles']);
Route::get('/project-list-developer',['as'=>'project.project-list-developer',  'uses' => 'ProjectController@getAgentProject']);
