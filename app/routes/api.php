<?php


use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function(){

// Esewa API can be added :
    Route::group(['middleware' => 'ApiPaymentToken', 'prefix' => 'esewa'], function() {
        Route::get('/{requestId}', 'ApiPaymentController@inquiry');
        Route::post('/payment', 'ApiPaymentController@payment');
    });
    
// meter ingest API
Route::post('/ingest/meter','IngestController@meterLog');

Route::post('/ingest/meter_data','IngestController@ingestHttpMeterData');
Route::get('/show/meter_data','IngestController@showMeterData');
Route::get('/download/meter_data','IngestController@downloadMeterData');

});
/*
 * This is API route for meterdata
 */
Route::group(['namespace'=>'MeterData'],function(){
    Route::get('meterdata/{meter_id}','MeterDataController@sendData');
    Route::post('meterdata/filter/{meter_id}','MeterDataController@filterData');
});

Route::post('amazon-sns/notifications', 'EmailController@handleBounceOrComplaint');

