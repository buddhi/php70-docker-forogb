<?php
/**
 * Admin Controllers
 * All route names are prefixed with 'admin.'.
 */

Route::get('meter',         [ 'as' => 'admin.meter.list',      'uses' => 'MeterController@meterList']);
Route::get('meter/create/{id}', ['as' => 'admin.meter.create',    'uses' => 'MeterController@create']);
Route::post('meter/store',      ['as' => 'admin.meter.store',    'uses' => 'MeterController@store']);
Route::get('meter/edit/{id}',   ['as' => 'admin.meter.edit',      'uses' => 'MeterController@edit']);
Route::post('meter/update/{id}',['as' => 'admin.meter.update',    'uses' => 'MeterController@update']);

Route::get('user',               [ 'as' => 'admin.user.list',      'uses' => 'UserController@userList']);
Route::post('user/activate',[ 'as' => 'admin.user.activate',      'uses' => 'UserController@activateUser']);
Route::get('user/{user_id}',[ 'as' => 'admin.user.edit',      'uses' => 'UserController@editUser']);
Route::post('user/{user_id}',[ 'as' => 'admin.user.update',      'uses' => 'UserController@updateUser']);
Route::get('/user/{user_id}/disable',['as' => 'admin.user.disable',     'uses' => 'UserController@changeStatus']);

Route::get('/project',           ['as' => 'admin.project.list',      'uses' => 'ProjectController@index']);
Route::get('/project/{project_id}',         ['as' => 'admin.project.edit',      'uses' => 'ProjectController@edit']);
Route::put('/project/{project_id}',         ['as' => 'admin.project.update',    'uses' => 'ProjectController@update']);
Route::get('/project/{project_id}/investor',['as' => 'admin.project.investor',  'uses' => 'ProjectController@investorDetail']);
Route::get('/project/{project_id}/disable',['as' => 'admin.project.disable',     'uses' => 'ProjectController@disable']);

Route::get('/project-plan',                       ['as' => 'admin.project-plan.list',  'uses' => 'ProjectPlanController@listProjectsWithPlans']);
Route::get('/project-plan/edit/{id}', ['as' => 'admin.project-plan.edit',  'uses' => 'ProjectPlanController@editProjectPlan']);
Route::post('/project-plan/update/{id}',['as' => 'admin.project-plan.update',  'uses' => 'ProjectPlanController@updateProjectPlan']);

Route::get('/project/download/{upload_id}', ['as' => 'admin.project-files.download',  'uses' => 'ProjectController@downloadFile']);
Route::get('/project/download/all/{project_id}', ['as' => 'admin.project-files.all.download',  'uses' => 'ProjectController@downloadAllFiles']);


Route::post('/image-store/{id}', ['as'=>'image.store',   'uses'=>'ImageController@store']);
Route::get('/project-makeOperational/{id}',    [ 'as' => 'project.makeOperational','uses' => 'ProjectController@projectMakeOperational']);

Route::get('credit/{id}',               [ 'as' => 'admin.credit',           'uses' => 'CreditScoreController@creditScore']);
Route::get('credit-weight',             [ 'as' => 'admin.credit.weight',    'uses' => 'CreditScoreController@creditScoreWeight']);
Route::get('credit-weight/attribute/{attribute_id}',             [ 'as' => 'admin.credit.weight.attribute',    'uses' => 'CreditScoreController@creditScoreAttributeWeight']);
Route::get('project/csv/download',      [ 'as' => 'admin.project.csv',      'uses' => 'ExportController@projectCsvExport']);

Route::get('user/change-status/{user_id}',[ 'as' => 'admin.user.change-status',      'uses' => 'UserController@changeStatus']);
Route::get('user/resend-verification/{user_id}',[ 'as' => 'admin.user.resend-verification',      'uses' => 'UserController@resendVerificationEmail']);

Route::post('state/update/{project_id}',              [ 'as'=>'admin.state.update',  'uses' => 'ProjectController@stateUpdate']);
Route::post('transition-state/update',              [ 'as'=>'admin.transition.state.update',  'uses' => 'ProjectController@transitionStateUpdate']);
