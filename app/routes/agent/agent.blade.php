            <?php

Route::get('verify/{token}',                ['as' => 'verify',                  'uses' => 'AgentController@verifyEmail']);
Route::get('create/{user_register_id}',     ['as' => 'create',                  'uses' => 'AgentController@create']);
Route::post('store',                        ['as' => 'store',                   'uses' => 'AgentController@storeAgent']);
Route::get('/',                             ['as' => 'dashboard',               'uses' => 'AgentController@dashboard']);
Route::post('{agent_id}/change-status',     ['as' => 'change-status',           'uses' => 'AgentController@changeStatus']);



