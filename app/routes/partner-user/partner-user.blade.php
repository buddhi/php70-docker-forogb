<?php

Route::get('verify/{token}',                ['as' => 'verify',                  'uses' => 'PartnerUserController@verifyEmail']);
Route::get('create',                        ['as' => 'create',                  'uses' => 'PartnerUserController@create']);
Route::post('store',                        ['as' => 'store',                   'uses' => 'PartnerUserController@storeUser']);
Route::get('/',                             ['as' => 'dashboard',               'uses' => 'PartnerUserController@dashboard']);
Route::get('agent/{partner_user_id}',       [ 'as'=>'agent',   'uses' => 'PartnerUserDashBoardController@agentList']);
Route::get('project', ['as' => 'project.index', 'uses' => 'ProjectController@index']);

Route::get('change-number/{user_id}',              ['as' => 'change-number.form',             'uses' => 'AccountController@changeMobile']);
Route::post('change-number/{user_id}/store',       ['as' => 'change-number.store',            'uses' => 'AccountController@storeNumber']);
