<?php
Route::get('/meterdata/{project_id}',['as'=>'meterdata_demo.show','uses'=>'MeterDataController@index']);
Route::post('/meterdata/generate', 'MeterDataController@generateCsv');
Route::post('/meterdata_demo/generate', 'MeterDataController@generateCsv');
