<?php
/*********************************
 * ROUTES FOR ADMIN ONLY         *
*********************************/

Route::group(['prefix' => 'ogbadmin', 'as'=> 'ogbadmin.','namespace'=>'OgbAdmin','middleware' => ['auth','AdminPanelAuth']], function () {

    //partner routes
    Route::post('partner/send-verification',            ['as' => 'partner.send-verification',   'uses' => 'PartnerController@resendVerificationEmail']);
    Route::post('partner/change-logo',                  ['as' => 'partner.change-logo',          'uses' => 'PartnerController@uploadPartnerLogo']);
    Route::post('partner/change-status',                ['as' => 'partner.change-status',       'uses' => 'PartnerController@changeStatus']);
    Route::get('partner',                               ['as' => 'partner.index',               'uses' => 'PartnerController@index']);
    Route::get('partner/create',                        ['as' => 'partner.create',              'uses' => 'PartnerController@create']);
    Route::post('partner/store',                        ['as' => 'partner.store',               'uses' => 'PartnerController@store']);
    Route::get('partner/{partner_id}',                  ['as' => 'partner.edit',                'uses' => 'PartnerController@edit']);
    Route::post('partner/{partner_id}',                 ['as' => 'partner.update',              'uses' => 'PartnerController@update']);

    //partner-user routes
    Route::get('partner-user',                                    ['as' => 'partner-user.index',                 'uses' => 'PartnerUserController@index']);
    Route::get('partner-user/resend-verification-custom/{userRegister}',         ['as' => 'partner-user.resend.email',                 'uses' => 'PartnerUserController@resendVerificationEmail']);
    Route::post('partner-user/store-temp-user',                   ['as' => 'partner-user.store-temp-user',       'uses' => 'PartnerUserController@store'])->middleware('developerRestriction');
    Route::post('partner-user/change-image',   ['as' => 'partner-user.change-image',         'uses' => 'PartnerUserController@changeImage']);
    Route::post('partner-user/{partner_user_id}/change-status',   ['as' => 'partner-user.change-status',         'uses' => 'PartnerUserController@changeStatus']);
    Route::get('partner-user/{partner_user_id}',   ['as' => 'partner-user.edit',         'uses' => 'PartnerUserController@editUser']);
    Route::post('partner-user/{partner_user_id}',   ['as' => 'partner-user.update',         'uses' => 'PartnerUserController@updateUser']);



    //agent routes
    Route::get('agent',                             ['as' => 'agent.index',                 'uses' => 'AgentController@index']);
    Route::post('agent/change-image',['as' => 'agent.change-image',                 'uses' => 'AgentController@changeImage']);
    Route::get('agent/{agent_id}',['as' => 'agent.edit',                 'uses' => 'AgentController@edit']);
    Route::post('agent/{agent_id}',['as' => 'agent.update',                 'uses' => 'AgentController@update']);

    //common routes via composer
    Route::post('notification/mark-as-read', [ 'as'=>'notification.mark-as-read',  'uses' => 'SiteController@markReadForNotification']);
});

/*********************************
 * ROUTES FOR ADMIN + PARTNER USER
 *********************************/

Route::group(['prefix' => 'ogbadmin', 'as'=> 'ogbadmin.','namespace'=>'OgbAdmin','middleware' => ['auth','AdminAndPartnerUserCheck']], function () {
    //Todo::future create a separate routes(make available for admin as well)
    Route::get('payment',                       [ 'as'=>'payment.index',    'uses' => 'PaymentController@payment']);
    Route::get('payment/create',                [ 'as'=>'payment.create',   'uses' => 'PaymentController@create']);
    Route::post('payment/store',                [ 'as'=>'payment.store',    'uses' => 'PaymentController@store']);

    Route::get('investment',                    [ 'as'=>'investment.index', 'uses' => 'InvestmentController@index']);
    Route::post('investment/store',             [ 'as'=>'investment.store','uses' => 'InvestmentController@store']);
});


//unauthenticated routes
Route::get('ogbadmin/login',[ 'as'=>'ogbadmin.login',      'uses' => 'Admin\SiteController@ogbAdminLogin']);
Route::get('contact',function (){
    return view('contact');
})->name('contact');


/*********************************
 * ROUTES FOR Project
 *********************************/
Route::group(['prefix' => 'ogbadmin', 'as'=> 'ogbadmin.','namespace'=>'OgbAdmin','middleware' => ['auth','AdminPanelAuth']], function () {
    Route::get('project', ['as' => 'project.index', 'uses' => 'ProjectController@index']);
    Route::post('project/filter', ['as' => 'project.filter', 'uses' => 'ProjectController@filter']);
    Route::get('/project/create', ['as' => 'project.create', 'uses' => 'ProjectController@create']);
    Route::post('/project/store', ['as' => 'project.store', 'uses' => 'ProjectController@store']);

    Route::get('/project/get_districts/{province_id}', ['as' => 'project.getalldistricts', 'uses' => 'ProjectController@getAllDistricts']);

    Route::get('/project/get_municipalities/{municipality_id}', ['as' => 'project.getmunicipalties', 'uses' => 'ProjectController@getAllMunicipalities']);

    Route::get('/project/select-plan/{id}', ['as' => 'project.selectplan', 'uses' => 'ProjectController@selectPlan']);
    Route::post('/project/selectedStore', ['as' => 'project.selectedStore', 'uses' => 'ProjectController@selectPlanStore']);

   /* Route::get('/project-information/{id}', ['as' => 'project.information', 'uses' => 'ProjectController@getProjectInformation']);

    Route::post('/project/edit/{id}', ['as' => 'project.edit-custom', 'uses' => 'ProjectController@update']);*/
    Route::get('/project-information/{id}',['as' => 'project.information', 'uses' => 'ProjectController@getProjectInformation']);

    Route::get('/project/edit/{id}',['as'=>'project.edit','uses'=>'ProjectController@editProjectData']);

    Route::post('/project/update/{id}',['as'=>'project.update','uses'=>'ProjectController@updateProjectData']);


    Route::get('/project/change-status/{id}', ['as' => 'project.change-status', 'uses' => 'ProjectController@changeStatus']);

    Route::get('/project/details/{id}', ['as' => 'project.details', 'uses' => 'ProjectController@projectDetails']);

    Route::post('/project/details/change_status', ['as' => 'project.details.change_status', 'uses' => 'ProjectController@changeProjectStatus']);

    Route::post('/project/upload/details/{id}', ['as' => 'project.upload.details', 'uses' => 'ProjectController@uploadFile']);

    Route::get('/project/delete-document/{id}', ['as' => 'project.delete-document', 'uses' => 'ProjectController@deleteDocument']);

    Route::post('/project/change-profile-picture/{id}',['as' => 'project.change-profile-picture', 'uses' => 'ProjectController@changeProfilePicture']);

    Route::post('/comment/store', ['as' => 'comment.store', 'uses' => 'ProjectController@storeComment']);

    Route::get('/get-project-data', ['as' => 'project.get.data', 'uses' => 'ProjectController@getProjectData']);

    Route::post('/project/file/upload/{projectId}',[ 'as'=>'file.upload',  'uses' => 'ProjectController@uploadProjectDocument']);
    Route::get('/project/file/remove/{documentId}',[ 'as'=>'file.delete',  'uses' => 'ProjectController@deleteProjectDocument']);
    Route::get('/project/file/get/{project_id}',[ 'as'=>'file.get',  'uses' => 'ProjectController@getProjectDocument']);

//Investment
    Route::get('investment', ['as' => 'investment.index', 'uses' => 'InvestmentController@index']);

    Route::post('/investment/store', ['as' => 'investment.store', 'uses' => 'InvestmentController@store']);

    Route::get('/project/guidelines/{project}', ['as' => 'project.get.guideline', 'uses' => 'ProjectController@getGuidelines']);

    Route::post('/project/{project}/timeline/{guideline}', ['as' => 'project.get.guideline', 'uses' => 'ProjectController@storeTimeline']);

    Route::get('/project/getAvailableStatus/{project}', ['as' => 'project.get.available', 'uses' => 'ProjectController@getStatusAvailable']);

    Route::put('/project/changeFundingState/{project}', ['as' => 'project.get.funding', 'uses' => 'ProjectController@updateTimelineForFundingComplete']);

    Route::post('/project/{project}/new-status', ['as' => 'project.get.new', 'uses' => 'ProjectController@updateTimelineForNewStatusComplete']);

    Route::get('/project/{project}/logs', ['as' => 'project.get.logs', 'uses' => 'ProjectController@projectLogs']);

    Route::get('/project/{project}/document-upload-list', ['as' => 'project.get.uploaddoc', 'uses' => 'ProjectController@getAvailableDocumentCategoryForProject']);

    Route::post('/project/store-investment',['as' => 'project.investment.store', 'uses' => 'ProjectController@storeProjectInvestment']);

    Route::post('/project/store-payment',['as' => 'project.payment.store', 'uses' => 'ProjectController@storeProjectPayment']);

    Route::get('/project/{project}/investment-percentage',['as' => 'project.investment.percentage', 'uses' => 'ProjectController@getInvestmentPercentage']);

    Route::get('/project/{project}/payment_history',['as' => 'project.payment.history', 'uses' => 'ProjectController@getPaymentHistory']);

    Route::get('/project/customize-package/{id}','ProjectController@editPackage')->name('project.customize-package');
    Route::post('/project/update-package/{id}','ProjectController@updatePackage')->name('project.update-package');
    Route::get('/project/getPackage/{package}','ProjectController@getPackage')->name('project.getPackage');
//Route::post('/project/{project}/logs'',function(Request $request,App\Models\Project $project){
//    echo 'hello';
//});

    //Meter
    Route::get('/project/create/meter/{id}',['as' => 'project.create.meter', 'uses' => 'MeterController@create']);
    Route::post('/project/store/meter',['as'=>'project.store.meter','uses'=>'MeterController@store']);
    Route::get('/project/meter/edit/{id}',   ['as' => 'project.edit-meter',      'uses' => 'MeterController@edit']);
    Route::post('/project/meter/update/{id}',['as' => 'meter.update',    'uses' => 'MeterController@update']);

    Route::get('/project/{project}/investment-chart',['as' => 'project.investment.chart', 'uses' => 'ProjectController@getInvestmentChartData']);

    Route::get('/project/{project}/average-default-data', ['as' => 'project.payment.defaults', 'uses' => 'ProjectController@getProjectPaymentDefaultedDetails']);

    Route::get('/meterdata/{project_id}',['as'=>'meterdata_demo.show','uses'=>'MeterDataController@index']);
});