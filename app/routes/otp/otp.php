<?php

Route::get('verify-otp/{user_id}',                      ['as' => 'verify-otp.form',         'uses' => 'Otp\OtpController@showOtpForm']);
Route::post('verify-otp/{user_id}',                     ['as' => 'verify-otp',              'uses' => 'Otp\OtpController@verifyOtp']);
Route::post('verify-otp/{user_id}/change-mobile',       ['as' => 'change-mobile','uses' => 'Otp\OtpController@changeMobileNumber']);
Route::get('verify-otp/{user_id}/resend-otp',          ['as' => 'resend-otp',   'uses' => 'Otp\OtpController@resendOtp']);

