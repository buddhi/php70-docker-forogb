<?php

use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictCoordinateSeeder extends Seeder
{
    public function run()
    {
        $district_coordinates = [
            //Province 1
            [28 , 26.527014 ,  88.069550 ] ,
            [43 , 26.470454 ,  87.303886 ] ,
            [71 , 26.577212 ,  87.165940 ] ,

            //Province 2
            [65 , 26.530190 ,  86.734886 ] ,
            [69 , 26.733838 ,  86.246422 ] ,
            [20 , 26.667979 ,  85.887709 ] ,
            [40 , 26.675183 ,  85.774595 ] ,
            [66 , 26.873829 ,  85.548730 ] ,
            [58 , 26.801112 ,  85.283622 ] ,
            [9  , 27.003296 ,  85.007188 ] ,
            [54 , 27.017481 ,  84.85539  ] ,

            //Province 3
            [13 , 27.597201 ,  84.321621 ] ,

            //Province 4
            [47 , 27.628559 ,  84.129226 ] ,

            //Province 5
            [48 , 27.530466 ,  83.685029 ] ,
            [62 , 27.491668 ,  83.440357 ] ,
            [33 , 27.522554 ,  83.059178 ] ,
            [16 , 28.023581 ,  82.469301 ] ,
            [8  , 28.068745 ,  81.614506 ] ,
            [10 , 28.207398 ,  81.334480 ] ,

            //Province 7
            [30 , 28.746047 ,  80.591143 ] ,
            [32 , 28.981760 ,  80.185039 ] ,
        ];
        $this->command->info("Adding District co-ordinates");
        foreach($district_coordinates as $district_coordinate) {
            District::updateOrCreate(['id'=> $district_coordinate[0]],[
                'id'              => $district_coordinate[0],
                'latitude'        => $district_coordinate[1],
                'longitude'       => $district_coordinate[2],
            ]);
        }
    }
}
