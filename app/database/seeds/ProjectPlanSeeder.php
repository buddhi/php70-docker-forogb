<?php

use Illuminate\Database\Seeder;

use App\Models\Plan;


class ProjectPlanSeeder extends Seeder
{
    public function run()
    {
        $plans = [
            [1, 'Package 1', 'Mini Pump',       50000, '',    8000, 150,22500,5400,36,3,'Pedrollo','Invt xxx' ],
            [2, 'Package 2', 'Sahaj',       225000, '1',    180000, 960,22500,5400,36,3,'Pedrollo','Invt xxx' ],
            [3, 'Package 3', 'Sampurna',    324000, '1.5',  250000, 1600,32400,7700,36,3,'Pedrollo','Invt xxx' ],
            [4, 'Package 4', 'Sambridhi',   376000, '2',    350000, 2240,37600,8900,36,3,'Pedrollo','Invt xxx' ]
        ];
        $this->command->info("Adding Plans");
        foreach ($plans as $plan) {
            Plan::updateOrCreate(['id' => $plan[0]], [
                'id' => $plan[0],
                'code' => $plan[1],
                'name' => $plan[2],
                'plan_cost' => $plan[3],
                'pump_size' => $plan[4],
                'daily_discharge' => $plan[5],
                'solar_pv_size' => $plan[6],

                'advance_amount' => $plan[7],
                'emi_amount' => $plan[8],
                'emi_count' => $plan[9],
                'no_of_year_for_payment' => $plan[10],
                'total_emi' => $plan[8] * $plan[9],
                'pump_brand'=>$plan[11],
                'controller'=>$plan[12]
            ]);
        }
    }
}
