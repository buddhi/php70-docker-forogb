<?php

use Illuminate\Database\Seeder;

class CroppingFactorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 1;
        if (($handle = fopen('database/SeedData/cropping_factor_stage.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $cropping_factor = [
                    'id' => $count++,
                    'crop_id' => $data[0],
                    'stage1' => $data[1],
                    'stage2' => $data[2],
                    'stage3' => $data[3],
                    'stage4' => $data[4],
                ];
                \App\Models\CroppingFactor::firstOrCreate($cropping_factor);
            }
            fclose($handle);
        }
    }
}
