<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $categories = [
            ['id' => 1, 'attribute_key' => 'farmer_photo', 'name' => 'Farmer photo', 'status_id' => 3,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 2, 'attribute_key' => 'citizenship', 'name' => 'Citizenship', 'status_id' => 3,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 3, 'attribute_key' => 'land_document', 'name' => 'Land related documents', 'status_id' => 3,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 4, 'attribute_key' => 'bank_loan_application', 'name' => 'Bank/Loan Application Form Copy', 'status_id' => 3,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 5, 'attribute_key' => 'perliminary_contract_farmer', 'name' => 'Perliminary contract with farmer', 'status_id' => 5,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 6, 'attribute_key' => 'perliminary_contract_agent', 'name' => 'Perliminary contract with agent', 'status_id' => 5,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 7, 'attribute_key' => 'installation_report', 'name' => 'Installation report', 'status_id' => 7,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 8, 'attribute_key' => 'signed_contract_farmer', 'name' => 'Signed contract with farmer', 'status_id' => 7,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 9, 'attribute_key' => 'insurance', 'name' => 'Insurance document', 'status_id' => 8,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 10, 'attribute_key' => 'invoice_project', 'name' => 'Invoice for project', 'status_id' => 8,'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
            ['id' => 11, 'attribute_key' => 'miscallenous', 'name' => 'Miscallenous','status_id'=>null, 'created_at'=>Carbon::create('2019', '01', '01') , 'updated_at'=>Carbon::create('2019', '01', '01')],
        ];

        DB::table('categories')->insert($categories);
    }
}
