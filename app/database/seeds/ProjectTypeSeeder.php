<?php

use Illuminate\Database\Seeder;

use App\Models\ProjectType;


class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $projectTypes = [
        [1  , 'Water Pump'] ,
        [2  , 'Grinding Mill'] ,
        [3  , 'Dairy Chilling'] ,
        [4  , 'Refrigeration'],
        [5  , 'Microgrid'] ,
      ];

      $this->command->info("Adding Crops");
      foreach($projectTypes as $projectType) {
        ProjectType::firstOrCreate([
          'id'           => $projectType[0],
          'name'         => $projectType[1],
          'delete_flg'   => '0'
        ]);
      }
    }
}
