<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusGuidelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_guidelines')->delete();
        $status_guidelines = [
            ['id' => 1, 'guideline' => 'Saved project while filling content of form', 'status_id' => 1, 'type' => 'automatic', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 2, 'guideline' => 'Necessary information for a project has been submitted by an agent', 'status_id' => 2, 'type' => 'automatic', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 3, 'guideline' => 'Contact farmer to verify their interest and requirements', 'status_id' => 3, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 4, 'guideline' => 'Review survey form\'s data for accuracy and consistency', 'status_id' => 3, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 5, 'guideline' => 'Check upload documents and photos for legitimacy', 'status_id' => 3, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 6, 'guideline' => 'Check if projects have an acceptable credit score', 'status_id' => 3, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 7, 'guideline' => 'Collect advance payment', 'status_id' => 4, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 8, 'guideline' => 'Pay commission to agent', 'status_id' => 4, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 9, 'guideline' => 'Preliminary Contract with Farmer', 'status_id' => 4, 'type' => 'upload', 'attribute_key' => 'perliminary_contract_farmer', 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 10, 'guideline' => 'Minimum certain percent Fund raised', 'status_id' => 5, 'type' => 'fillable', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 11, 'guideline' => 'Collect first emi\'s for the project', 'status_id' => 6, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 12, 'guideline' => 'Installation report', 'status_id' => 6, 'type' => 'upload', 'attribute_key' => 'installation_report', 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 13, 'guideline' => 'Signed contract with farmer', 'status_id' => 6, 'type' => 'upload', 'attribute_key' => 'signed_contract_farmer', 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 14, 'guideline' => 'Recieve multimedia information', 'status_id' => 6, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 15, 'guideline' => 'Insurance documents', 'status_id' => 7, 'type' => 'upload', 'attribute_key' => 'insurance', 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 16, 'guideline' => 'Invoice for the project', 'status_id' => 7, 'type' => 'upload', 'attribute_key' => 'invoice_project', 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
            ['id' => 17, 'guideline' => 'Add meter', 'status_id' => 8, 'type' => 'check', 'attribute_key' => NULL, 'created_at' => Carbon::create('2019', '01', '01'), 'updated_at' => Carbon::create('2019', '01', '01')],
        ];

        DB::table('status_guidelines')->insert($status_guidelines);
    }
}
