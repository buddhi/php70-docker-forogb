<?php

use Illuminate\Database\Seeder;

class DistrictCattleInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cattles = [
            [1  , 1, 10 , 7000,1,12,"5", "['apr']" , 35000,2] ,
            [2  , 1, 16 , 7000,1,12,"5", "['apr']" , 35000,2] ,
            [3  , 1, 30 , 7000,1,12,"5", "['apr']" , 35000,2] ,
            [4  , 2, 10 , 2800,1,12,"10","['sep']" , 14000,2],
            [5  , 2, 16 , 2800,1,12,"10","['sep']" , 14000,2],
            [6  , 2, 30 , 2800,1,12,"10","['sep']" , 14000,2],
        ];

        $this->command->info("Adding Cattle");
        foreach($cattles as $cattle) {
            \App\Models\DistrictCattleInfo::updateOrCreate(['id' => $cattle[0]],[
                'id'           => $cattle[0],
                'cattle_id'         => $cattle[1],
                'district_id' => $cattle[2],
                'cost_of_production'  => $cattle[3],
                'number_of_cycle'  => $cattle[4],
                'total_duration'  => $cattle[5],
                'start_month' => $cattle[6],
                'harvesting_months' => $cattle[7],
                'price_unit'  => $cattle[8],
                'unit'  => $cattle[9],
            ]);
        }
    }
}
