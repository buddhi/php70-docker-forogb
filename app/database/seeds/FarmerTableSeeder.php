<?php

use App\Models\Farmer;
use Illuminate\Database\Seeder;

class FarmerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Farmer::create([
            'firstName' => 'kalu',
            'lastName' => 'Pandey',
            'Address' => 'Kirtipur',
            'latitude' => 27.63,
            'longitude' => 85.6,
        ]);
        Farmer::create([
            'firstName' => 'Ram',
            'lastName' => 'Ale',
            'Address' => 'Kalanki',
            'latitude' => 27.63,
            'longitude' => 85.6,
        ]);


    }
}
