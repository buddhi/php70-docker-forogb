<?php

use Illuminate\Database\Seeder;

class RainFallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = \App\Models\District::all();
        $count = 1;
        $rainfall = [];
        $months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        $district_rainfall = [
            10 => ['jan'=>20.3,'feb'=>0.4,'mar'=>17.3,'apr'=>44.9,'may'=>97.9,'jun'=>197.5,'jul'=>601,'aug'=>618.4,'sep'=>278.5,'oct'=>9.3,'nov'=>1.3,'dec'=>4.5],
            30 => ['jan'=>7.3,'feb'=>7.2,'mar'=>8.2,'apr'=>15.9,'may'=>17,'jun'=>57.2,'jul'=>416.1,'aug'=>490.9,'sep'=>208.6,'oct'=>0,'nov'=>0,'dec'=>2.8],
            32 => ['jan'=>0,'feb'=>1.6,'mar'=>20.7,'apr'=>61.1,'may'=>112,'jun'=>172.2,'jul'=>433.3,'aug'=>441.3,'sep'=>156,'oct'=>107,'nov'=>0,'dec'=>0],
        ];
        foreach($districts as $district){
            foreach($months as $m){
                if($district->id == 30 || $district->id == 10 || $district->id == 32) {
                    $rainfall = [
                        'id' => $count,
                        'district_id' => $district->id,
                        'month' => $m,
                        'rainfall' => $district_rainfall[$district->id][$m]
                    ];
                }else{
                    $rainfall = [
                        'id' => $count,
                        'district_id' => $district->id,
                        'month' => $m,
                        'rainfall' => 10
                    ];
                }
                \App\Models\AverageMonthlyRainfall::updateOrCreate(['id' => $rainfall['id']],$rainfall);
                $count++;
            }

        }
    }
}
