<?php

use Illuminate\Database\Seeder;

class CashFlowTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = \App\Models\District::all();
        $crops = \App\Models\Crop::all();
        foreach($districts as $district){
            foreach($crops as $crop){
                $data = [];
                $count = 0;
                if($district->id == 10){
                    if($crop->id == 30){

                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>67384,'amount_produced_per_cycle'=>3726,'cultivating_months'=>'["jun","jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 14){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>51871,'amount_produced_per_cycle'=>3691,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>25,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 13){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>232168,'amount_produced_per_cycle'=>24915,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 31){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>697620,'amount_produced_per_cycle'=>6000,'cultivating_months'=>'["apr", "may", "jun","jul","aug","sept"]','harvesting_months'=>'["oct", "nov"]','number_of_cycle_yearly'=>1,'average_market_price'=>200,'district_id'=>$district->id
                        ];
                    }else{
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>100,'amount_produced_per_cycle'=>100,'cultivating_months'=>'["jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }
                }elseif($district->id == 16){
                    if($crop->id == 30){

                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>67804,'amount_produced_per_cycle'=>4022,'cultivating_months'=>'["jun","jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 14){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>54368,'amount_produced_per_cycle'=>4022,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>25,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 13){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>212303,'amount_produced_per_cycle'=>17850,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>21,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 31){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>697620,'amount_produced_per_cycle'=>6000,'cultivating_months'=>'["apr", "may", "jun","jul","aug","sept"]','harvesting_months'=>'["oct","nov"]','number_of_cycle_yearly'=>1,'average_market_price'=>200,'district_id'=>$district->id
                        ];
                    }else{
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>100,'amount_produced_per_cycle'=>100,'cultivating_months'=>'["jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }
                }elseif($district->id == 30){
                    if($crop->id == 30){

                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>67804,'amount_produced_per_cycle'=>4022,'cultivating_months'=>'["jun","jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 14){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>51871,'amount_produced_per_cycle'=>3691,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>25,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 13){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>232168,'amount_produced_per_cycle'=>24915,'cultivating_months'=>'["dec", "jan", "feb"]','harvesting_months'=>'["mar"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }elseif($crop->id == 31){
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>697620,'amount_produced_per_cycle'=>6000,'cultivating_months'=>'["apr", "may", "jun","jul","aug","sept"]','harvesting_months'=>'["oct", "nov"]','number_of_cycle_yearly'=>1,'average_market_price'=>200,'district_id'=>$district->id
                        ];
                    }else{
                        $data = [
                            'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>100,'amount_produced_per_cycle'=>100,'cultivating_months'=>'["jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                        ];
                    }
                }
                else{
                    $data = [
                        'id'=>$count,'crop_id'=>$crop->id,'area'=>1,'cost_production'=>100,'amount_produced_per_cycle'=>100,'cultivating_months'=>'["jul", "aug", "sep"]','harvesting_months'=>'["oct"]','number_of_cycle_yearly'=>1,'average_market_price'=>24,'district_id'=>$district->id
                    ];
                }
                \App\Models\CashFlow::updateOrCreate(['id' => $data['id']],$data);
                $count++;
            }
        }


    }
}
