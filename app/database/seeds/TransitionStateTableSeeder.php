<?php

use Illuminate\Database\Seeder;

class TransitionStateTableSeeder extends Seeder
{
    public function run()
    {

        $transition_states = [
            [
                'id' => 1,
                'title' => 'form_submission',
                'description' => 'Is survey form submitted?',
                'status' => 'plan'
            ],

            [
                'id' => 2,
                'title' => 'plan_chosen',
                'description' => 'Have agent chosen certain plan?',
                'status' => 'new'
            ],

            [
                'id' => 3,
                'title' => 'contact_farmer',
                'description' => 'Contact farmer to verify their interest and requirements',
                'status' => 'approved'
            ],

            [
                'id' => 4,
                'title' => 'review_survey_form',
                'description' => 'Review survey form’s data for accuracy and consistency',
                'status' => 'approved'
            ],

            [
                'id' => 5,
                'title' => 'check_documents',
                'description' => 'Check uploaded documents and photos for legitimacy, orientation, and accurate field/label',
                'status' => 'approved'
            ],

            [
                'id' => 6,
                'title' => 'has_acceptable_risk',
                'description' => 'Acceptable Risk Profile.',
                'status' => 'approved'
            ],
            [
                'id' => 7,
                'title' => 'commission_paid_to_agent',
                'description' => 'Commission paid to the agent',
                'status' => 'funding'
            ],
            [
                'id' => 8,
                'title' => 'preliminary_contract_between_agent_and_farmer',
                'description' => 'Preliminary contract with agent and farmer',
                'status' => 'funding'
            ],

            [
                'id' => 9,
                'title' => 'collected_advance_and_emi_for_first_month',
                'description' => 'Collected Advance',
                'status' => 'funding'
            ],

            [
                'id' => 10,
                'title' => 'percent_check',
                'description' => 'Minimum certain percent Fund raised',
                'status' => 'funded'
            ],

            [
                'id' => 11,
                'title' => 'admin_final_verification',
                'description' => 'Final check for project is verified and running',
                'status' => 'void'
            ],

            [
                'id' => 12,
                'title' => 'first_emi_collected',
                'description' => 'Emi 1 collected',
                'status' => 'installed'
            ],

            [
                'id' => 13,
                'title' => 'installation_report_collected',
                'description' => 'Installation report collected',
                'status' => 'installed'
            ],
            [
                'id' => 14,
                'title' => 'contract_signed_and_received',
                'description' => 'Contract signed and received',
                'status' => 'installed'
            ],
            [
                'id' => 15,
                'title' => 'multimedia_information for project collected',
                'description' => 'Multimedia information for project collected',
                'status' => 'installed'
            ],

            [
                'id' => 17,
                'title' => 'insurance_process_completed',
                'description' => 'Insurance Process Completed',
                'status' => 'operational'
            ],
            [
                'id' => 18,
                'title' => 'invoice_generated',
                'description' => 'Invoice generated',
                'status' => 'operational'
            ],

            [
                'id' => 19,
                'title' => 'meter_is_live',
                'description' => 'Meter Is live',
                'status' => 'void'
            ],
            [
                'id' => 20,
                'title' => 'no_pending_emi',
                'description' => 'No Pending Emi',
                'status' => 'void'
            ],

        ];
        $this->command->info("Add intermediate transition state");
        foreach ($transition_states as $key => $transition_state) {
            \App\Models\TransitionState::updateOrCreate(['id' => $transition_state['id']], [
                'id' => $transition_state['id'],
                'title' => $transition_state['title'],
                'description' => $transition_state['description'],
                'status' => $transition_state['status'],
            ]);
        }
    }
}
