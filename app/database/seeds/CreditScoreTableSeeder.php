<?php

use Illuminate\Database\Seeder;

class CreditScoreTableSeeder extends Seeder
{
    public $risk, $risks, $risk_attribute, $risk_attributes, $risk_attribute_category, $risk_attribute_categories;
    public $risk_array = [
        [1, 'demographic',1, 3],
        [2, 'financial',  1, 5],
        [3, 'agricultural', 1,2],
        [4, 'psychometric', 1 , 4],
    ];
    
    public $risk_attribute_array = [
        //agricultural
        [1, 'age', 1, 'range', 1],
        [2, 'gender', 1, 'fixed', 1],
        [3, 'education', 1, 'boolean', 1],
        [4, 'certification', 1, 'boolean', 1],
        [5, 'no_of_people_in_house', 1, 'range', 1],
        [6, 'no_of_dependents', 1, 'range', 1],

        //financial
        [7,  'revenues_from_water_distribution', 1, 'range',2],
        [8,  'additional_income_source', 1, 'boolean', 2],
        [9 , 'additional_salaried_occupation', 1, 'boolean', 2],
        [10, 'monthly_saving', 1, 'boolean', 2],
        [11, 'has_bank', 1, 'boolean', 2],
        [12, 'percentage_of_debt_on_ebitda', 1, 'range', 2],
        [13, 'recent_large_purchase_through_payment_plan', 1, 'boolean', 2],
        [14, 'future_capital_expenditure_causing_cash_flow_interruptions', 1, 'boolean', 2],
        [15, 'distance_to_nearest_market', 1, 'range', 2],
        [16, 'land_owned', 1, 'boolean', 2],
        [17, 'percentage_of_solar_loan_on_expense', 1, 'range', 2],
        [18, 'percentage_of_expenses_on_income', 1, 'range', 2],

        [19, 'size_fish', 1,'boolean',4],
        [20, 'neighbour_trust', 1,'range',4],
        [21, 'harvest_lost', 1,'range',4],
        [22, 'village_meeting', 1,'boolean', 4],
        [23, 'farming_skill', 1,'range', 4],
        [24, 'image_recall_exercise', 1,'boolean', 4],
        [25, 'question_timing', 1, 'boolean',4],
        [26, 'answered_all_questions', 1,'boolean', 4],
        [27, 'equivocating_question', 1,'boolen' ,4],

        [28, 'crop_risk', 1,'boolean',3],
        [29, 'livestock_risk', 1,'boolean',3],
        [30, 'crop_diversity', 1,'boolean',3],
        [31, 'has_cereal_grain', 1,'boolean', 3],
        [32, 'crop_inventory', 1, 'boolean',3],
        [33, 'livestock_diversity', 1,'boolean', 3],

    ];

    public $risk_attribute_categories_array = [
        [1, 'below60', 0, 1, 0, 59.99 ,null],
        [2, 'above60', 1, 1, 60, 200 ,null],

        [3, 'male', 1, 2,   null, null  ,null],
        [4, 'female', 0, 2, null, null,null],

        [5, 'educated', 0, 3, null, null,1],
        [6, 'not_educated', 1, 3, null, null,0],

        [7, 'certified', 0, 4, null, null,1],
        [8, 'not_certified', 1, 4, null, null,0],

        [9, 'below8',0 , 5, 0, 6.99,null],
        [10, 'above8', 1, 5, 7, 200,null],

        [11, 'dependent_absent', 0 , 6, 0,0,null],
        [12, 'dependent_present', 1, 6, 1, 1000,null],

        [13, 'revenues_from_water_distribution_absent', 1, 7,0,0,1],
        [14, 'revenues_from_water_distribution_present', 0, 7,1, 10000000000,0],

        [15, 'additional_income_source_present', 0, 8, null,null,1],
        [16, 'additional_income_source_absent', 1, 8, null,null,0],

        [17, 'additional_salaried_occupation_present', 0, 9, null,null,1],
        [18, 'additional_salaried_occupation_absent', 1, 9, null,null,0],

        [19, 'monthly_saving_present', 0, 10, null,null,1],
        [20, 'monthly_saving_absent', 1, 10, null,null,0],

        [21, 'bank_present', 0, 11, null,null,1],
        [22, 'bank_absent', 1, 11, null,null,0],

        [23, 'percentage_of_debt_on_ebitda_less than 50', 0, 12, 0, 49.99,null],
        [24, 'percentage_of_debt_on_ebidta more than 50', 1,12, 50, 100,null,null],

        [25, 'recent_large_purchase_through_payment_plan', 0, 13, null,null,1],
        [26, 'recent_large_purchase_not_done', 1, 13, null,null,0],

        [27, 'future_capital_expenditure_causing_cash_flow_interruptions', 1, 14, null,null,1],
        [28, 'future_capital_expenditure_causing_cash_flow_interruptions_absent', 0, 14,null,null,0],

        [29, 'distance_to_nearest_market_small', 0, 15, 0, 0.99,null],
        [30, 'distance_to_nearest_market_large', 1, 15, 1, 10000,null],

        [31, 'land_owned', 0, 16, null, null,1],
        [32, 'land_not_owned', 1, 16, null,null,0],

        [33, 'percentage_of_solar_loan_on_expense_less_than_50', 0, 17, 0, 49.99,null],
        [34, 'percentage_of_solar_loan_on_expense_more_than_50', 1, 17, 50, 100,null],

        [35, 'percentage_of_expenses_on_income_less_than_45', 0, 18, 0, 44.99,null],
        [36, 'percentage_of_expenses_on_income_more_than_45', 1, 18, 45, 100,null],


        [37, 'size_fish_small', 1, 19, null, null,0],
        [38, 'size_fish_large', 0, 19, null, null,1],

        [39, 'Doesnt has_neighbour_trust', 1, 20, 0, 50,null],
        [40, 'have neighbour_trust', 0, 20, 50, 100,null],

        [41, 'harvest_lost_less_than_6_months', 1, 21, 0, 0.5, null,null,1],
        [42, 'harvest_lost_more_than_6_months', 0, 21, 0.51, 200, null,null,0],

        [43, 'village_meeting_early', 0, 22, null,null,1],
        [44, 'village_meeting_late', 1, 22, null,null,0],

        [45, 'farming_skill_absent', 1, 23, 0, 49.99, null],
        [46, 'farming_skill_present', 0, 23, 50, 100, null],

        [47, 'image_recall_exercise_didnot_cheat', 0, 24, null,null,1],
        [48, 'image_recall_exercise cheated', 1, 24, null,null,0],

        [49, 'question_timing_long', 0, 25, null,null,1],
        [50, 'question_timing_short', 1, 25, null,null,0],

        [51, 'answered_all_questions', 0, 26,  null,null,1],
        [52, 'didnot_answered_all_questions', 1, 26 , null,null,0],

        [53, 'equivocating_question', 1, 27, null,null,1],
        [54, 'not_equivocating_question', 0, 27, null,null,0],

        [55, 'crop_risky', 1, 28, null,null,1],
        [56, 'crop not risky', 0, 28, null,null,0],

        [57, 'livestock_risk', 1, 29, null,null,1],
        [58, 'livestock not risky', 0, 29, null,null,0],

        [59, 'crop_diversity', 1, 30, null,null,0],
        [60, 'crop not diverse', 0, 30, null,null,1],

        [61, 'has_cereal_grain_only', 1, 31, null,null,1],
        [62, 'doesnot have cereal grain', 0, 31, null,null,0],

        [63, 'has_crop_inventory', 1, 32, null,null,0],
        [64, 'doesnot have crop inventory', 0, 32, null,null,1],

        [65, 'livestock_diversity', 1, 33, null,null,0],
        [66, 'doesnot have livestock inventory', 0, 33, null,null,1]
    ];
    public function run()
    {
        $this->command->info("Adding Creditscore weights");

        foreach($this->risk_array as $item) {
            \App\Models\Risk::updateOrCreate(['id'=> $item[0]],[
                'id'              => $item[0],
                'name'            => $item[1],
                'risk_weight_factor'=> $item[2],
                'risk_threshold'=> $item[3],
            ]);
        }

        foreach($this->risk_attribute_array as $item) {
            \App\Models\RiskAttribute::updateOrCreate(['id'=> $item[0]],[
                'id'              => $item[0],
                'name'            => $item[1],
                'attribute_weight_factor'=> $item[2],
                'type'=> $item[3],
                'risk_id'=> $item[4],
            ]);
        }

        foreach($this->risk_attribute_categories_array as $item) {
            \App\Models\RiskAttributeCategory::updateOrCreate(['id'=> $item[0]],[
                'id'              => $item[0],
                'name'            => $item[1],
                'category_weight_factor'=> $item[2],
                'risk_attribute_id'=> $item[3],
                'min_value'=> $item[4],
                'max_value'=> $item[5],
                'boolean_value'=> $item[6],
            ]);
        }

    }

}

