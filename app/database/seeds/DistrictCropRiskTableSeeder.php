<?php

use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictCropRiskTableSeeder extends Seeder
{

    public $crop_risk_according_district= [];
    public function run()
    {   $districts = District::all();
        $crop_order_array = range(1,36);
        if (($handle = fopen('database/SeedData/crop_district.csv', "r")) !== FALSE) {
            $count1 = 0;
            while (($data = fgetcsv($handle, 10000, ","))!== FALSE) {
                $count1++;
                foreach ($crop_order_array as $key =>$crop_order){
                    $crop_district = [
                        'crop_id' => $crop_order,
                        'district_id' => $count1,
                        'risk' => $data[$key]=='NR'? 0:1
                    ];

                    \App\Models\DistrictCropRisk::firstOrCreate($crop_district);
                }

            }
            fclose($handle);
        }

    }
}
