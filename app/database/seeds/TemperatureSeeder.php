<?php

use Illuminate\Database\Seeder;

class TemperatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = \App\Models\District::all();
        $count = 1;
        $temperature = [];
        $months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        $district_temperature = [
            10 => ['jan'=>16,'feb'=>20,'mar'=>24,'apr'=>31,'may'=>33,'jun'=>33,'jul'=>29,'aug'=>29,'sep'=>27,'oct'=>25,'nov'=>21,'dec'=>19],
            30 => ['jan'=>17,'feb'=>21,'mar'=>28,'apr'=>36,'may'=>34,'jun'=>33,'jul'=>33,'aug'=>32,'sep'=>31,'oct'=>29,'nov'=>25,'dec'=>21],
            32 => ['jan'=>19,'feb'=>24,'mar'=>28,'apr'=>33,'may'=>33,'jun'=>34,'jul'=>32,'aug'=>33,'sep'=>31,'oct'=>28,'nov'=>26,'dec'=>23],
        ];
        foreach($districts as $district){
            foreach($months as $m){
                if($district->id == 30 || $district->id == 10 || $district->id == 32){
                    $temperature = [
                        'id' => $count,
                        'district_id' => $district->id,
                        'month' => $m,
                        'temperature' => $district_temperature[$district->id][$m]
                    ];
                }else{
                    $temperature = [
                        'id' => $count,
                        'district_id' => $district->id,
                        'month' => $m,
                        'temperature' => 10
                    ];
                }

                \App\Models\AverageMonthlyTemperature::updateOrCreate(['id' => $temperature['id']],$temperature);
                $count++;
            }

        }
    }
}
