<?php

use App\Models\Cattle;
use Illuminate\Database\Seeder;

class CattleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cattles = [
            [1  , 'Cow/Buffallo'] ,
            [2  , 'Goat']
        ];

        $this->command->info("Adding Cattle");
        foreach($cattles as $cattle) {
            Cattle::firstOrCreate([
                'id'           => $cattle[0],
                'name'         => $cattle[1],
            ]);
        }
    }
}
