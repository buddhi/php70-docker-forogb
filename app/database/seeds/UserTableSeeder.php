<?php

use App\Enums\Verification;
use App\Models\RoleUser;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate(['email'=>'root@admin.com'],[
            'username' => 'root@admin.com',
            'email' => 'root@admin.com',
            'password' => bcrypt('root'),
            'full_name' => 'root',
            'phone' => '9843487012',
            'address' => 'kathmandu',
            'investment_type' => null,
            'verified' => Verification::EMAIL_VERIFIED,
            'admin_verification' => Verification::ADMIN_VERIFIED,
        ]);
        RoleUser::updateOrCreate(['user_id'=>$user->user_id], [
            'id' => $user->user_id,
            'role'=>50
        ]);
    }
}

