<?php

use App\Models\Meter;
use Illuminate\Database\Seeder;

class MeterTableSeeder extends Seeder
{
    public function run()
    {

        Meter::updateOrCreate(['id'=> 1],[
            'id' => 1,
            'farmer_project_id' => null,
            'phoneNo' => '983573409',
            'hardwareId' => '983573409',
            'firmwareVersion' => '983573409',
            'type' => '983573409',
            'pump_efficiency' => 0.52,
            'cd_efficiency' => 0.7,
            'max_power' => 833.333,
            'head' => 3,
            'frequency_multiplier' => 5.8,
            'rpm_multiplier' => 60
        ]);
    }
}
