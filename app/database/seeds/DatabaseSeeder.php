<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  $this->call(FarmerTableSeeder::class);
        $this->call(MeterTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ProvinceDistrictMunicipalitySeeder::class);
        $this->call(CropSeeder::class);
        $this->call(ProjectPlanSeeder::class);
        $this->call(ProjectTypeSeeder::class);
        $this->call(TemperatureSeeder::class);
        $this->call(RainFallSeeder::class);
        $this->call(CroopingFactorDayTableSeeder::class);
        $this->call(CroppingFactorTableSeeder::class);
        $this->call(CashFlowTableSeeder::class);
        $this->call(CattleTableSeeder::class);
        $this->call(DistrictCattleInfoTableSeeder::class);
        $this->call(CreditScoreTableSeeder::class);
        $this->call(DistrictCropRiskTableSeeder::class);
        $this->call(DistrictCoordinateSeeder::class);
        $this->call(TransitionStateTableSeeder::class);
        $this->call(StatusesSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(StatusGuidelineSeeder::class);
    }
}
