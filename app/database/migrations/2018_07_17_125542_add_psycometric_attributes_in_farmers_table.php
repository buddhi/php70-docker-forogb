<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPsycometricAttributesInFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->integer('size_fish')->nullable()->comment('Size of fish is 1 for large and 0 for small');
            $table->integer('neighbour_trust')->nullable()->comment('This is a psycometric question on how much does a neighbour trust the farmer');
            $table->integer('harvest_lost')->nullable()->comment('Psycometric question on how long can a farmer sustain without harvest in years and 0.5 is 6 months');
            $table->integer('village_meeting')->nullable()->comment('Psycometric question on how farmer respects other people ');
            $table->integer('farming_skill')->nullable()->comment('Psycometric question on how farmer rates his farming skill');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('size_fish');
            $table->dropColumn('neighbour_trust');
            $table->dropColumn('harvest_lost');
            $table->dropColumn('village_meeting');
            $table->dropColumn('farming_skill');
        });
    }
}
