<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('farmers', function (Blueprint $table){
            $table->string('email')->nullable();
            $table->integer('earning_members')->nullable();
            $table->integer('non_earning_members')->nullable();
            $table->string('educational_level')->nullable();
            $table->string('has_cattle')->nullable();
            $table->string('sell_crops')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
