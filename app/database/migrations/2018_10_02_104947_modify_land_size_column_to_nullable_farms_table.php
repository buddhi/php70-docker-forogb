<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyLandSizeColumnToNullableFarmsTable extends Migration
{
    public function up()
    {
        Schema::table('farms', function (Blueprint $table) {
            $table->decimal('land_size',20 ,4)->nullable()->change();
        });
    }
}
