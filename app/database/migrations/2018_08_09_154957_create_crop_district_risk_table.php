<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropDistrictRiskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crop_district_risk', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('crop_id');
            $table->unsignedInteger('district_id');
            $table->boolean('risk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_district_risk');
    }
}
