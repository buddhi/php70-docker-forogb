<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToDistrictsTable extends Migration
{
    public function up()
    {
        Schema::table('tbl_district', function (Blueprint $table) {
            $table->decimal('latitude',11,8)->nullable();
            $table->decimal('longitude',11,8)->nullable();
        });
    }

    public function down()
    {
        Schema::table('tbl_district', function (Blueprint $table) {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
