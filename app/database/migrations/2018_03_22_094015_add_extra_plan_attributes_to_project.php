<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraPlanAttributesToProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_project',function (Blueprint $table){
            $table->string('pump_size',100)->default(0);
            $table->integer('daily_discharge')->default(0);
            $table->integer('solar_pv_size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_project',function (Blueprint $table){
            $table->dropColumn('pump_size');
            $table->dropColumn('daily_discharge');
            $table->dropColumn('solar_pv_size');
        });
    }
}
