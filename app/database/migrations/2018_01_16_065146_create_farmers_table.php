<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->float('latitude',10,6);
            $table->float('longitude',10,6);
            $table->string('farmer_name');
            $table->integer('age')->nullable();
            $table->string('contact_no');
            $table->string('village_name')->nullable();
            $table->integer('no_of_dependents')->nullable();
            $table->decimal('annual_household_income',20,4)->nullable();
            $table->decimal('annual_expenses',20,4)->nullable();
            $table->decimal('outstanding_debt',20,4)->nullable();
            $table->boolean('has_bank')->nullable();
            $table->string('financial_institute')->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmers');
    }
}
