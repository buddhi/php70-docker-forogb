<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTimelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_timelines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status_id');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id')->references('id')->on('tbl_project');
            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')->references('user_id')->on('app_user');
            $table->unsignedInteger('status_guidelines_id');
            $table->foreign('status_guidelines_id')->references('id')->on('status_guidelines');
            $table->tinyInteger('delete_flg')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_timelines');
    }
}
