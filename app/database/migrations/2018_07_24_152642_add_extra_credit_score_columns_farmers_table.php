<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraCreditScoreColumnsFarmersTable extends Migration
{
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->double('percentage_of_solar_loan_on_expense')->nullable();
            $table->double ('percentage_of_expenses_on_income')->nullable();
            $table->double ('percentage_of_debt_on_ebitda')->nullable();
        });
    }

    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('percentage_of_solar_loan_on_expense');
            $table->dropColumn('percentage_of_expenses_on_income');
            $table->dropColumn('percentage_of_debt_on_ebitda');
        });
    }
}
