<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_crop', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('delete_flg');
            $table->decimal('yield_ton_ha',19,5);
            $table->decimal('yield_kg_ha',19,5);
            $table->decimal('price_rs_kg',19,5);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_crop');
    }
}
