<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('transaction_id');
            $table->text('payer_email')->nullable();
            $table->text('payer_name')->nullable();
            $table->text('payee_email')->nullable();
            $table->text('payee_name')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('project_id')->nullable();
            $table->decimal('amount',19,4);
            $table->string('currency');
            $table->text('payment_gateway');
            $table->text('transaction_type');
            $table->text('status')->nullable();
            $table->text('response_data')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
