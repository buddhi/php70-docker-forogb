<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCropFactorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_crop_factor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->decimal('crop_factor',19,6)->nullable();
            $table->integer('rank')->nullable();

        });
    }

//CREATE TABLE `tbl_crop_factor` (
//`id` int(11) DEFAULT NULL,
//`name` varchar(255) DEFAULT NULL,
//`crop_factor` decimal(19,6) DEFAULT NULL,
//`rank` int(11) DEFAULT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_crop_factor');
    }
}
