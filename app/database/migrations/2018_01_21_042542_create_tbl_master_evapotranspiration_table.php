<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMasterEvapotranspirationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_master_evapotranspiration', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district',255);
            $table->decimal('evapotrans',19,5);
            $table->integer('month_id');

        });
    }


//CREATE TABLE `tbl_master_evapotranspiration` (
//`id` int(11) NOT NULL,
//`district` varchar(255) DEFAULT NULL,
//`evapotrans` decimal(19,6) DEFAULT NULL,
//`month_id` int(11) DEFAULT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_master_evapotranspiration');
    }
}
