<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCroppingFactorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cropping_factor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crop_id')->unsigned();
            $table->decimal('stage1',20,4);
            $table->decimal('stage2',20,4);
            $table->decimal('stage3',20,4);
            $table->decimal('stage4',20,4);
            $table->foreign('crop_id')
                ->references('id')
                ->on('tbl_crop')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cropping_factor');
    }
}
