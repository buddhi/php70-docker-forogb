<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToMeterDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meter_datas', function (Blueprint $table){
            $table->decimal('dc_power')->nullable();
            $table->decimal('power_percent')->nullable();
            $table->decimal('discharge')->nullable();
            $table->decimal('ac_power')->nullable();
            $table->decimal('frequency')->nullable();
            $table->bigInteger('rpm')->nullable();
            $table->bigInteger('runtime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
