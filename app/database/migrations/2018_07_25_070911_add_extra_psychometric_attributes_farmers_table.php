<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraPsychometricAttributesFarmersTable extends Migration
{
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->double('image_recall_exercise')->nullable();
            $table->double ('question_timing')->nullable();
            $table->double ('answered_all_questions')->nullable();
            $table->double ('equivocating_question')->nullable();
        });
    }

    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('image_recall_exercise');
            $table->dropColumn('question_timing');
            $table->dropColumn('answered_all_questions');
            $table->dropColumn('equivocating_question');
        });
    }
}
