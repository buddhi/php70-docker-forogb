<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstantattributesToMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meters', function (Blueprint $table){
            $table->decimal('system_efficiency',20,4)->nullable();
            $table->decimal('pump_efficiency',20,4)->nullable();
            $table->decimal('cd_efficiency',20,4)->nullable();
            $table->decimal('max_power',20,4)->nullable();
            $table->decimal('frequency_multiplier',20,4)->nullable();
            $table->decimal('rpm_multiplier',20,4)->nullable();
            $table->decimal('head',20,4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meters',function(Blueprint $table){
            $table->dropColumn('system_efficiency');
            $table->dropColumn('pump_efficiency');
            $table->dropColumn('cd_efficiency');
            $table->dropColumn('max_power');
            $table->dropColumn('frequency_multiplier');
            $table->dropColumn('rpm_multiplier');
            $table->dropColumn('head');
        });
    }
}
