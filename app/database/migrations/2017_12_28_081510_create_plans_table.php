<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',100)->nullable();
            $table->string('name',100)->nullable();
            $table->integer('plan_cost')->default(0);
            $table->string('pump_size',100)->default(0);
            $table->integer('daily_discharge')->default(0);
            $table->integer('solar_pv_size')->nullable();
            $table->tinyInteger('delete_flg')->default('0');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tbl_plan');
    }
}