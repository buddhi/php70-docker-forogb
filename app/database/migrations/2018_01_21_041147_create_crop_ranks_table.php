<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void

     */
    protected $timestamps = false;
    public function up()
    {
        Schema::create('crop_ranks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('crop_id')->unsigned()->nullable();
            $table->integer('month_id');
            $table->integer('rank');
            $table->timestamp('created_on')->default(DB::raw('CURRENT_TIMESTAMP'));;
            $table->integer('created_by')->nullable();
            $table->tinyInteger('delete_flg');
        });
    }


//CREATE TABLE `tbl_crop_rank` (
//`id` int(11) NOT NULL,
//`project_id` int(11) NOT NULL,
//`crop_id` int(11) NOT NULL,
//`month_id` int(11) NOT NULL,
//`rank` int(11) NOT NULL DEFAULT '1',
//`created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
//`created_by` int(11) DEFAULT NULL,
//`delete_flg` int(11) DEFAULT '0'
//) ENGINE=MyISAM DEFAULT CHARSET=utf8;


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crop_ranks');
    }
}
