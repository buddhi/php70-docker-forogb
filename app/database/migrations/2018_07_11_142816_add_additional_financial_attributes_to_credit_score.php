<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFinancialAttributesToCreditScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            //financial
            $table->boolean('water_distribution_to_farmers')->nullable();
            $table->boolean('monthly_saving')->nullable();
            $table->boolean('debt_service_obligation')->nullable();
            $table->boolean('recent_large_purchase_through_payment_plan')->nullable();
            $table->boolean('future_capital_expenditure_causing_cash_flow_interruptions')->nullable();
            $table->boolean('additional_income_source')->nullable();
            $table->boolean('additional_salaried_occupation')->nullable();
            $table->double('revenues_from_water_distribution')->nullable();
            $table->double('monthly_saving_amount')->nullable();
            $table->double('debt_monthly_payment')->nullable();
            $table->double('expected_solar_loan_monthly_payment')->nullable();
            $table->double('distance_to_nearest_market')->nullable();
            $table->string('nearest_market')->nullable();
            $table->string('income')->nullable();
            $table->string('expense')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //financial
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('water_distribution_to_farmers');
            $table->dropColumn('monthly_saving');
            $table->dropColumn('debt_service_obligation');
            $table->dropColumn('recent_large_purchase_through_payment_plan');
            $table->dropColumn('future_capital_expenditure_causing_cash_flow_interruptions');
            $table->dropColumn('additional_income_source');
            $table->dropColumn('additional_salaried_occupation');
            $table->dropColumn('revenues_from_water_distribution');
            $table->dropColumn('monthly_saving_amount');
            $table->dropColumn('debt_monthly_payment');
            $table->dropColumn('expected_solar_loan_monthly_payment');
            $table->dropColumn('distance_to_nearest_market');
            $table->dropColumn('nearest_market');
            $table->dropColumn('income');
            $table->dropColumn('expense');
        });

    }
}
