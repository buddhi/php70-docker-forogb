<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialAttributesForSocialLoginInUsersTable extends Migration
{
    public function up()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->string('password')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->dropColumn('provider')->nullable();
            $table->dropColumn('provider_id')->nullable();
        });
    }
}
