<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInvestmentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_investment_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->tinyInteger('delete_flg')->default(0);
        });
    }

//CREATE TABLE `tbl_investment_type` (
//`id` int(11) NOT NULL,
//`name` varchar(100) NOT NULL,
//`delete_flg` tinyint(4) DEFAULT '0'
//) ENGINE=MyISAM DEFAULT CHARSET=utf8;

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_investment_type');
    }
}
