<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistictCattleInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_district_cattle_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cattle_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->decimal('cost_of_production',19,5);
            $table->integer('number_of_cycle');
            $table->integer('total_duration');
            $table->longText('harvesting_months')->nullable();
            $table->longText('start_month')->nullable();
            $table->decimal('price_unit',19,5);
            $table->integer('unit');
            $table->foreign('cattle_id')
                ->references('id')
                ->on('cattles')
                ->onDelete('cascade');
            $table->foreign('district_id')
                ->references('id')
                ->on('tbl_district')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_district_cattle_info');
    }
}
