<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminVerifyColumnToUsersTable extends Migration
{
    public function up()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->boolean('admin_verification')->nullable();

        });
    }

    public function down()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->dropColumn('admin_verification');
        });
    }
}
