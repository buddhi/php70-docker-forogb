<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreAgriculturalAttributesForCreditScore extends Migration
{
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->boolean('crop_risk')->nullable();
            $table->boolean('livestock_risk')->nullable();
            $table->boolean('crop_diversity')->nullable();
            $table->boolean('has_cereal_grain')->nullable();
            $table->boolean('livestock_diversity')->nullable();
        });
    }

    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('crop_risk');
            $table->dropColumn('livestock_risk');
            $table->dropColumn('crop_diversity');
            $table->dropColumn('has_cereal_grain');
            $table->dropColumn('livestock_diversity');
        });
    }
}
