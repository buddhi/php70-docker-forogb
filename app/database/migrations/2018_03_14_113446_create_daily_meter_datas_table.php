<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyMeterDatasTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('daily_meter_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meter_id')->nullable();
            $table->decimal('daily_discharge')->nullable();
            $table->integer('daily_runtime')->nullable();
            $table->integer('daily_count')->nullable();
            $table->date('date')->nullable();
            $table->foreign('meter_id')
                    ->references('id')
                    ->on('meters')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_meter_data');
    }
}
