<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('otp');
            $table->boolean('status');
            $table->timestamp('expires_on');
            $table->timestamp('validated_at')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });

        Schema::table('otps', function($table) {
            $table->foreign('user_id')->references('user_id')->on('app_user')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otps');
    }
}
