<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToMeterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meters', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('farmer_project_id')->unsigned()->nullable();
            $table->string('phoneNo',50)->unique();
            $table->string('hardwareId',200);
            $table->string('firmwareVersion',200);
            $table->string('type');
            $table->timestamp('grace_ends_at')->nullable();
            $table->integer('status')->default(0);
            $table->foreign('farmer_project_id')
                ->references('id')
                ->on('farmer_project')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meters');
    }
}
