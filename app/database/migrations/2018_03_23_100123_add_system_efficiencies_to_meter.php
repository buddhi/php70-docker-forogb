<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemEfficienciesToMeter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meters', function (Blueprint $table) {
            $table->dropColumn('system_efficiency');
        });

        Schema::table('meters', function (Blueprint $table) {
            $table->decimal('sys_eff1', 20, 4)->default(1.05)->comment = 'System efficiency when dc_power is <= 250';
            $table->decimal('sys_eff2', 20, 4)->default(0.85)->comment = 'System efficiency when dc_power is > 250 and <= 350';
            $table->decimal('sys_eff3', 20, 4)->default(0.78)->comment = 'System efficiency when dc_power is > 350 and <= 450';
            $table->decimal('sys_eff4', 20, 4)->default(0.68)->comment = 'System efficiency when dc_power is > 450 and <= 550';
            $table->decimal('sys_eff5', 20, 4)->default(0.6)->comment = 'System efficiency when dc_power is > 500 and <= 650';
            $table->decimal('sys_eff6', 20, 4)->default(0.55)->comment = 'System efficiency when dc_power is > 650 and <= 750';
            $table->decimal('sys_eff7', 20, 4)->default(0.5)->comment = 'System efficiency when dc_power is > 750 and <= 850';
            $table->decimal('sys_eff8', 20, 4)->default(0.5)->comment = 'System efficiency when dc_power is > 850';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meters', function (Blueprint $table) {
            $table->decimal('system_efficiency', 20, 4)->default(0.5);
        });
        Schema::table('meters', function (Blueprint $table) {
            $table->decimal('system_efficiency', 20, 4)->default(0.5);
            $table->dropColumn('sys_eff1');
            $table->dropColumn('sys_eff2');
            $table->dropColumn('sys_eff3');
            $table->dropColumn('sys_eff4');
            $table->dropColumn('sys_eff5');
            $table->dropColumn('sys_eff6');
            $table->dropColumn('sys_eff7');
        });
    }
}