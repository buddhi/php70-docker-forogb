<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadAttributeKeyColumnToUploadTable extends Migration
{
    public function up()
    {
        Schema::table('uploads', function (Blueprint $table) {
            $table->string('upload_attribute_key')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('uploads', function (Blueprint $table) {
            $table->dropColumn('upload_attribute_key');
        });
    }
}
