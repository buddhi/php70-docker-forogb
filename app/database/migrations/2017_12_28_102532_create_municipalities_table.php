<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_municipality', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->integer('district_id')->unsigned()->nullable();
            $table->tinyInteger('delete_flg')->default(0);
            $table->foreign('district_id')->references('id')->on('tbl_district')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_municipality');
    }
}