<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskAttributeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_attribute_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('category_weight_factor');
            $table->integer('risk_attribute_id');
            $table->double('min_value')->nullable();
            $table->double('max_value')->nullable();
            $table->double('boolean_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_attribute_categories');
    }
}
