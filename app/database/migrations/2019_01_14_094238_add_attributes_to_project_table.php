<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class AddAttributesToProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_project', function (Blueprint $table) {
            $table->double('total_emi',19,4)->nullable();
            $table->double('advance_amount',19,4)->nullable();
            $table->double('emi_amount',19,4)->nullable();
            $table->integer('emi_count')->nullable();
            $table->timestamp('emi_start_date')->nullable();
            $table->timestamp('emi_end_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_project', function (Blueprint $table) {
            $table->dropColumn('total_emi');
            $table->dropColumn('advance_amount');
            $table->dropColumn('emi_amount');
            $table->dropColumn('emi_count');
            $table->dropColumn('emi_start_date');
            $table->dropColumn('emi_end_date');
        });
    }
}
