<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashFlowMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashflow_measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crop_id')->unsigned()->nullable();
            $table->integer('area')->nullable();
            $table->decimal('cost_production',20,6)->nullable();
            $table->integer('amount_produced_per_cycle')->nullable();
            $table->longText('cultivating_months')->nullable();
            $table->longText('harvesting_months')->nullable();
            $table->integer('number_of_cycle_yearly')->nullable();
            $table->decimal('average_market_price',20,6)->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->foreign('crop_id')
                ->references('id')
                ->on('tbl_crop')
                ->onDelete('cascade');
            $table->foreign('district_id')
                ->references('id')
                ->on('tbl_district')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashflow_measurements');
    }
}
