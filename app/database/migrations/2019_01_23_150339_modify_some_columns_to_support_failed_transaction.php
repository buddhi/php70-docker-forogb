<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySomeColumnsToSupportFailedTransaction extends Migration
{
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('amount',19,4)->nullable()->change();
            $table->text('transaction_type')->nullable()->change();
        });
    }
}
