<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeToTableTblCropInfo extends Migration
{
    public function up()
    {
        Schema::table('tbl_crop_info', function (Blueprint $table) {
            $table->string('crop_area_unit_type')->nullable();

        });
    }

    public function down()
    {
        Schema::table('tbl_crop_info', function (Blueprint $table) {
            $table->dropColumn('crop_area_unit_type');
        });
    }
}
