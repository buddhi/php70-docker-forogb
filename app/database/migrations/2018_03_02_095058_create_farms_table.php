<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('farmer_id')->unsigned()->nullable();
            $table->decimal('land_size',20 ,4);
            $table->integer('current_irrigation_source');
            $table->integer('current_irrigation_system');
            $table->float('boring_size')->comment = "This is ground water level in inches";
            $table->float('vertical_head')->comment = "This field should be in Feet";
            $table->float('distance')->comment = "This is distance from source to farm in meters";
            $table->foreign('farmer_id')
                ->references('id')
                ->on('farmers')
                ->onDelete('cascade');
        });
        
    }
    
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farms');
    }
}
