<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPartnerIdInUserRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_registers', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->integer('partner_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_registers', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table
            $table->integer('partner_id')->unsigned()->nullable(false)->change();
        });
    }
}
