<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProjectInvestmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_project_investment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('developer_id')->unsigned()->nullable();
            $table->integer('investor_id')->unsigned()->nullable();
            $table->integer('investment_type_id')->unsigned()->nullable();
            $table->string('funding_type')->default(0);
            $table->string('investment_percent',100)->nullable();
            $table->double('invest_amount',100)->default(0);
            $table->integer('expected_irr')->nullable();
            $table->integer('term')->nullable();
            $table->double('interest_rate')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('created_on')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->tinyInteger('delete_flg')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_project_investment');
    }
}


//CREATE TABLE `tbl_project_investment` (
//`id` int(11) NOT NULL,
//  `project_id` int(11) NOT NULL,
//  `developer_id` int(11) NOT NULL,
//  `investor_id` int(11) NOT NULL,
//  `investment_type_id` int(11) NOT NULL,
//  `funding_type` int(11) DEFAULT '0',
//  `investment_percent` varchar(100) DEFAULT NULL,
//  `invest_amount` varchar(100) NOT NULL,
//  `expected_irr` int(11) DEFAULT NULL,
//  `term` int(11) DEFAULT NULL,
//  `interest_rate` int(11) DEFAULT NULL,
//  `created_by` int(11) DEFAULT NULL,
//  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
//  `delete_flg` tinyint(4) DEFAULT '0'
//) ENGINE=MyISAM DEFAULT CHARSET=utf8;
