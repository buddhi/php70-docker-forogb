<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMasterRainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_master_rain', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district',255)->nullable();
            $table->decimal('rainfall',19,6)->nullable();
            $table->integer('month_id')->nullable();


        });
    }

//CREATE TABLE `tbl_master_rain` (
//`id` int(11) NOT NULL,
//`district` varchar(255) DEFAULT NULL,
//`rainfall` decimal(19,6) DEFAULT NULL,
//`month_id` int(11) DEFAULT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_master_rain');
    }
}
