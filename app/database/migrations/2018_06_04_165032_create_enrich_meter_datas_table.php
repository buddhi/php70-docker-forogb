<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrichMeterDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrich_meter_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename')->unique()->index();
            $table->string('current')->nullable();
            $table->string('temperature')->nullable();
            $table->string('voltage')->nullable();
            $table->string('humidity')->nullable();
            $table->string('soil_moisture')->nullable();
            $table->timestamps();
            $table->bigInteger('meter_id')->nullable();
            $table->decimal('dc_power')->nullable();
            $table->decimal('power_percent')->nullable();
            $table->decimal('discharge')->nullable();
            $table->decimal('ac_power')->nullable();
            $table->decimal('frequency')->nullable();
            $table->bigInteger('rpm')->nullable();
            $table->bigInteger('runtime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrich_meter_datas');
    }
}
