<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtpVerifiedColumnToUsersTable extends Migration
{
    public function up()
    {
        Schema::table('app_user', function (Blueprint $table){
            $table->integer('partner_id')->nullable();
            $table->boolean('otp_verified')->default(0);
        });
    }

    public function down()
    {
        Schema::table('app_user', function (Blueprint $table){
            $table->dropColumn('partner_id');
            $table->dropColumn('otp_verified');
        });
    }
}
