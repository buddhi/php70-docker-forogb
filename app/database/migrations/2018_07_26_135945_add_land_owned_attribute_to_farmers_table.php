<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLandOwnedAttributeToFarmersTable extends Migration
{
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->boolean('land_owned')->nullable();

        });
    }

    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('land_owned');
        });
    }
}
