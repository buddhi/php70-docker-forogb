<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->longText('code')->nullable();
            $table->timestamp('emi_start_month')->nullable();
            $table->timestamp('emi_end_month')->nullable();
            $table->double('amount',19,4);
            $table->boolean('paid_status');
            $table->string('payment_type');
            $table->string('payment_gateway')->nullable();
            $table->timestamp('paid_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esewa_payments');
    }
}
