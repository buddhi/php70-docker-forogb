<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps = false;
    public function up()
    {
        Schema::create('tbl_crop_info', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('crop_id');
            $table->unsignedInteger('project_id');
            $table->integer('start_month');
            $table->integer('end_month');
            $table->string('area',100);
            $table->integer('created_by')->nullable();
            $table->timestamp('created_on')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('updated_by')->nullable();
            $table->dateTime('updated_on')->nullable();
            $table->tinyInteger('delete_flg')->default(0);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_crop_info');
    }
}
