<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganisationIdInUsersTable extends Migration
{
    public function up()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->boolean('organisation_id')->nullable();

        });
    }

    public function down()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->dropColumn('organisation_id');
        });
    }
}
