<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraThresholdRiskToRisksTable extends Migration
{
    public function up()
    {
        Schema::table('risks', function (Blueprint $table) {
            $table->double('risk_threshold')->nullable();

        });
    }

    public function down()
    {
        Schema::table('risks', function (Blueprint $table) {
            $table->dropColumn('risk_threshold');
        });
    }
}
