<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_plan', function (Blueprint $table) {
            $table->double('total_emi', 19, 4)->nullable();
            $table->double('emi_amount', 19, 4)->nullable();
            $table->integer('emi_count')->nullable();
            $table->double('advance_amount', 19, 4)->nullable();
            $table->double('no_of_year_for_payment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_plan', function (Blueprint $table) {
            $table->dropColumn('total_emi');
            $table->dropColumn('emi_amount');
            $table->dropColumn('emi_count');
            $table->dropColumn('advance_amount');
            $table->dropColumn('no_of_year_for_payment');
        });
    }
}
