<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('next_due',20,4);
            $table->decimal('over_due',20,4);
            $table->integer('status');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('bank_name');
            $table->string('loan_account_number');
            $table->integer('meter_id')->unsigned()->nullable();
            $table->string('last_state')->default('ok');
            $table->foreign('meter_id')
                  ->references('id')
                  ->on('meters')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
