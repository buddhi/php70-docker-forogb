<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesForAgriculturalScoreInFarmersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('farms', function (Blueprint $table) {
            $table->double('boring_depth')->nullable()->comment = "in feet";
            $table->double('ground_water_level')->nullable()->comment = "in feet";
            $table->string('type_of_soil')->nullable();
            $table->text('land')->nullable();
            $table->double('water_requirement')->nullable()->comment = "in liter/day";
        });

        Schema::table('farmers', function (Blueprint $table) {
            $table->boolean('crop_inventory')->nullable();
            $table->double('fuel_monthly_expense')->nullable();
            $table->double('grid_monthly_expense')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('farms', function (Blueprint $table) {
            $table->dropColumn('boring_depth');
            $table->dropColumn('ground_water_level');
            $table->dropColumn('type_of_soil');
            $table->dropColumn('land');
            $table->dropColumn('water_requirement');
        });

        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('crop_inventory');
            $table->dropColumn('fuel_monthly_expense');
            $table->dropColumn('grid_monthly_expense');
        });
    }
}
