<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToUsersAndPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->boolean('disabled')->default(0);
        });

        Schema::table('partners', function (Blueprint $table) {
            $table->boolean('disabled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_user', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('disabled');
        });
    }
}
