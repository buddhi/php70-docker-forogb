<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',100)->nullable();
            $table->string('name',200)->nullable();
            $table->integer('project_type_id')->nullable();
            $table->string('farmer_name',200);
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('municipality_id')->unsigned()->nullable();
            $table->integer('ward_no');
            $table->integer('plan_id')->unsigned()->nullable();
            $table->string('cost')->default(0);
            $table->string('status');
            $table->string('description')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('account_number')->nullable();
            $table->string('sim_number')->nullable();
            $table->integer('head')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('created_on')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('updated_by')->nullable();
            $table->dateTime('updated_on')->nullable();
            $table->integer('is_draft')->default(0);
            $table->integer('delete_flg')->default(0)->nullable();


        });

        Schema::table('tbl_project', function($table) {
            $table->foreign('plan_id')->references('id')->on('tbl_plan')->onDelete('cascade');
        });

        Schema::table('tbl_project', function($table) {
            $table->foreign('district_id')->references('id')->on('tbl_district')->onDelete('cascade');
        });

        Schema::table('tbl_project', function($table) {
            $table->foreign('municipality_id')->references('id')->on('tbl_municipality')->onDelete('cascade');
        });

        Schema::table('tbl_project', function($table) {
            $table->foreign('province_id')->references('id')->on('tbl_province')->onDelete('cascade');
        });

        Schema::table('tbl_project', function($table) {
            $table->foreign('created_by')->references('user_id')->on('app_user')->onDelete('cascade');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_project');
    }
}