<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeterDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meter_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename')->unique()->index();
            $table->string('current')->nullable();
            $table->string('temperature')->nullable();
            $table->string('voltage')->nullable();
            $table->string('humidity')->nullable();
            $table->string('soil_moisture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meter_datas');
    }
}
