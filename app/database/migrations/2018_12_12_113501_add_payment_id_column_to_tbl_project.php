<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentIdColumnToTblProject extends Migration
{
    public function up()
    {
        Schema::table('tbl_project', function (Blueprint $table){
            $table->string('payment_id',10)->nullable()->unique();
        });
    }

    public function down()
    {
        Schema::table('tbl_project', function (Blueprint $table){
            $table->dropColumn('payment_id');
        });
    }
}
