<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneNumberToMeterDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meter_datas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('meter_id')->unsigned()->nullable();
            $table->foreign('meter_id')
                ->references('id')
                ->on('meters')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meter_datas', function (Blueprint $table) {
            $table->dropForeign(['meter_id']);
        });
    }
}
