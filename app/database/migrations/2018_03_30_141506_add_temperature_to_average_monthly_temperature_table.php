<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemperatureToAverageMonthlyTemperatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('average_monthly_temperature', function (Blueprint $table) {
            $table->decimal('temperature',20,4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('average_monthly_temperature', function (Blueprint $table) {
            $table->dropColumn('temperature');
        });
    }
}
