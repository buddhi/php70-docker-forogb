<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ModifyIncomeColumnToNullableFarmersTable extends Migration
{
    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->longText('income')->change();
            $table->longText('expense')->change();
        });
    }
}
