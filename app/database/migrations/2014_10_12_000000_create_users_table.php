<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_user', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('username',255)->nullable();
            $table->string('password',255);
            $table->string('full_name',255);
            $table->string('phone');
            $table->string('email')->unique();
            $table->integer('role');
            $table->string('language',10)->default('en');
            $table->integer('company_id')->default(0);
            $table->string('user_agent')->nullable();
            $table->string('ip',100)->nullable();
            $table->dateTime('last_login_time')->nullable();
            $table->integer('login_access_count')->nullable();
            $table->tinyInteger('delete_flg')->default(0);
            $table->integer('created_by')->default(1);
            $table->timestamp('created_on')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('updated_by')->nullable();
            $table->dateTime('updated_on')->nullable();
            $table->string('investment_type',255)->nullable();
            $table->string('address')->nullable();
            $table->tinyInteger('is_locked')->default(1);
            $table->string('text_password')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_user');
    }
}