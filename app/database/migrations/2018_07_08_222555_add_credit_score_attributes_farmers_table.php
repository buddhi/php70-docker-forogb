<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditScoreAttributesFarmersTable extends Migration
{
    // demographic : education,certification,gender,no_of_people_in_house
    /** financial:stated_monthly_expenses,revenues_from_water_distribution,additional_income_source,
    other_current_assets,debt_Service,land_ownership,additional_salaried_occupation,access_to_financial_institution
    solar_loan_expenses,recent_large_purchase_through_PMT_plan,future_capex_causing_cash_flow_interruptions
    distance_to_market **/

    //agricultural:crop_risks,livestock_risk,crop_diversity,cereal_grain,livestock_diversity,storage
    //Psychometric Scoring:q1,q2,q3,q4,q5,q6,q7,q8,q9

    public function up()
    {
        Schema::table('farmers', function (Blueprint $table) {
            //demographic
            $table->string('gender')->nullable();
            $table->boolean('education')->nullable();
            $table->boolean('certification')->nullable();
            $table->integer('no_of_people_in_house')->nullable();
            $table->double('credit_score')->nullable();
        });
    }

    public function down()
    {
        Schema::table('farmers', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('education');
            $table->dropColumn('certification');
            $table->dropColumn('no_of_people_in_house');
            $table->dropColumn('credit_score');
        });
    }
}
