<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FarmerProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmer_project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('farmer_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
        });
        Schema::table('farmer_project',function(Blueprint $table){
            $table->foreign('farmer_id')
                ->references('id')
                ->on('farmers')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('tbl_project')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmer_project');
    }
}
