<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\MeterData::class, function (Faker $faker) {
    return [
        'filename' => '9843487012_01030201',
        'current' => 2,
        'voltage' => 170, 'temperature' => 24,
        'humidity' => 1111,
        'soil_moisture' => 1,
        'dc_power' => 665.00,
        'power_percent' => 1,
        'discharge' => 44, 'ac_power' => 438,
        'frequency' => 44,
        'rpm' => 44,
        'runtime' => 51,
        'created_at' => \Carbon\Carbon::now()
    ];
});