<?php

use App\OGB\Constants;
use Carbon\Carbon;
use Faker\Generator as Faker;
$factory->define(\App\Models\Otp::class, function (Faker $faker) {
    return [
        'otp' => rand(pow(10,5), pow(10,6) - 1),
        'status' => Constants::ACTIVE,
        'expires_on' => Carbon::now()->addMinutes(15),
        'user_id' => function () {
            return factory(App\User::class)->create()->user_id;
        }
    ];
});
