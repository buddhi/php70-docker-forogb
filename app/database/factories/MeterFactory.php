<?php

use Faker\Generator as Faker;
$factory->define(App\Models\Meter::class, function (Faker $faker) {
    return
        ['farmer_project_id' => null,
            'phoneNo' => (string)"9843487012",
            'hardwareId' => "1",
            'pump_efficiency' => 0.99,
            'cd_efficiency' => 0.99,
            'sys_eff1' => 0.99,
            'sys_eff2' => 0.99,
            'sys_eff3' => 0.99,
            'sys_eff4' => 0.99,
            'sys_eff5' => 0.99,
            'sys_eff6' => 0.99,
            'sys_eff7' => 0.99,
            'sys_eff8' => 0.99,
            'max_power' => 0.99,
            'frequency_multiplier' => 0.99,
            'rpm_multiplier' => 0.99,
            'current_threshold' => 0.99,
            'head' => 0.99,
            'firmwareVersion' => 0.99,
            'type' => 0.99,
            'status' => 1
        ];

});