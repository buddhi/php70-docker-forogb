<?php

use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->define(\App\Models\Token::class, function (Faker $faker) {
    return [
        'token' => Uuid::generate()->string,
    ];
});
