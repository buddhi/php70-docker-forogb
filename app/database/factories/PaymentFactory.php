<?php

use App\Models\Project;
use Faker\Generator as Faker;

$factory->define(\App\Models\Payment::class, function (Faker $faker) {
    $project = Project::first()? Project::first():factory(Project::class)->create();
    return [
        'project_id' => $project->id,
        'code' => str_random(16),
        'emi_start_month' => \Carbon\Carbon::now(),
        'emi_end_month' => \Carbon\Carbon::now()->addMonth(1),
        'amount' => $project->isAdvanceAmountPaid()? $project->emi_amount: $project->advance_amount,
        'paid_status'  => 1,
        'payment_type' => $project->isAdvanceAmountPaid()? \App\Enums\PaymentType::EMI: \App\Enums\PaymentType::ADVANCE,
        'payment_gateway'  => 'cash',
        'paid_date' => \Carbon\Carbon::now(),
    ];
});
