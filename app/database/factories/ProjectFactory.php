<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Models\Project::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'project_type_id'=>rand(1,5),
        'farmer_name'=>$faker->name,
        'province_id'=>rand(1,7),
        'district_id'=>rand(1,77),
        'municipality_id'=>rand(1,100),
        'ward_no'=> rand(1,10),
        'plan_id'=>rand(1,3),
        'status'=>'new',
        'latitude'=>rand(1,100),
        'longitude'=>rand(1,100),
        'head'=>rand(1,10),
        'created_by'=>1,
        'created_on'=>Carbon::now(),
        'updated_by'=>1,
        'updated_on'=>Carbon::now(),
        'total_emi'=>(float)80,
        'emi_amount'=>(float)10,
        'emi_count'=>(float)8,
        'advance_amount'=>(float)20,
        'emi_start_date'=>Carbon::now()->copy(),
        'emi_end_date'=>Carbon::now()->copy()->addMonths(8),
        'payment_id' =>Carbon::parse(Carbon::now())->format('ym').rand(1,100)
    ];
});
