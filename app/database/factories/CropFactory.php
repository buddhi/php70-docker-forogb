<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Crop::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'delete_flg'=>0,
        'yield_ton_ha'=>rand(1,10),
        'yield_kg_ha'=>rand(1,10),
        'price_rs_kg'=>rand(1,10),
    ];
});
