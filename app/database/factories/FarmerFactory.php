<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Farmer::class, function (Faker $faker) {
    return [
                'address'=>'1',
                'latitude'=>'27',
                'longitude'=>'85',
                'farmer_name'=>$faker->name,
                'age'=>rand(18,70),
                'contact_no'=>$faker->phoneNumber,
                'village_name'=>$faker->address,
                'no_of_dependents'=>rand(0,15),
                'annual_household_income'=>'1',
                'annual_expenses'=>'1',
                'outstanding_debt'=>'1',
                'has_bank'=>$faker->boolean,
                'financial_institute'=>$faker->word,
                'gender'=>$faker->randomElement(['male','female']),
                'education'=>$faker->boolean,
                'certification'=>$faker->boolean,
                'no_of_people_in_house'=>rand(1,20),
                'water_distribution_to_farmers'=>$faker->boolean,
                'monthly_saving'=>$faker->boolean,
                'debt_service_obligation'=>$faker->boolean,
                'recent_large_purchase_through_payment_plan'=>$faker->boolean,
                'future_capital_expenditure_causing_cash_flow_interruptions'=>$faker->boolean,
                'additional_income_source'=>$faker->boolean,
                'additional_salaried_occupation'=>$faker->boolean,
                'revenues_from_water_distribution'=>rand(200,100000),
                'monthly_saving_amount'=>rand(200,100000),
                'debt_monthly_payment'=>rand(200,100000),
                'expected_solar_loan_monthly_payment'=>rand(200,100000),
                'distance_to_nearest_market'=>rand(1,20),
                'nearest_market'=>$faker->streetName,
                'income'=>rand(1000000,999999999),
                'expense'=>rand(1000000,999999999),
                'crop_inventory'=>$faker->boolean,
                'fuel_monthly_expense'=>rand(100000,9999999),
                'grid_monthly_expense'=>rand(100000,9999999),
                'land_owned'=>$faker->boolean,
    ];
});
