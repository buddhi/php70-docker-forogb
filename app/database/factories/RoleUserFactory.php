<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\RoleUser::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'role_id' => [1,10,50,100][array_rand([1,10,50,100])]
    ];
});
