<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\UserRegister::class, function (Faker $faker) {
    return [
        'email' => $faker->safeEmail,
        'verified' => true,
        'partner_id' => factory(\App\Models\Partner::class)->create()->id
    ];
});
