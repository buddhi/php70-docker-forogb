<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Farm::class, function (Faker $faker) {
    return [
        'farmer_id' => function () {
            return factory('App\Models\Farmer')->create()->id;
        },
        'land_size'=>$faker->numberBetween(1,300),
        'current_irrigation_source'=>$faker->numberBetween(1,3),
        'current_irrigation_system'=>$faker->numberBetween(1,5),
        'boring_size'=>$faker->numberBetween(1,30),
        'vertical_head'=>$faker->numberBetween(1,30),
        'distance'=>$faker->numberBetween(1,30),
        'farm_image'=>null,
        'boring_depth'=>$faker->numberBetween(1,30),
        'ground_water_level'=>$faker->numberBetween(1,30),
        'type_of_soil'=>null,
        'land'=>null,
        'water_requirement'=>null,
    ];
});
