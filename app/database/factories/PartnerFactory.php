<?php

use Faker\Generator as Faker;
$factory->define(App\Models\Partner::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'contact_person' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'verified' => true,
        'address' => $this->faker->address,
        'landline' => $this->faker->phoneNumber,
        'phone' => $this->faker->phoneNumber,
        'disabled' => 0
    ];
});
