<?php

use App\Models\TransitionState;
use Faker\Generator as Faker;

$factory->define(TransitionState::class, function (Faker $faker) {
    return [
        'title'=> getTransitionStates(null,'title')[rand(1,11)],
        'description'=>getTransitionStates(null,'description')[rand(1,11)],
        'status'=>getTransitionStates(null,'status')[rand(1,11)],
        'delete_flg'=>0,
    ];
});
