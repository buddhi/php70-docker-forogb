#/bin/bash
LTS="build-`date +\"%Y%m%d%H%M\"`"
BUILD_FILE="$1"
RELEASES="/home/tdeployer/ogb/releases"
OGB="/home/tdeployer/ogb"
tar xvf $RELEASES/$1  -C $RELEASES/
mv $RELEASES/builds/ghampower/offgridbazaar-laravel $RELEASES/$LTS
rm -f $RELEASES/build*.tar.bz2 
rm -rf $RELEASES/builds
rm -f $OGB/lts
ln -s $RELEASES/$LTS $OGB/lts
ln -nfs  $OGB/.env $RELEASES/$LTS/.env 
rm -rf $RELEASES/$LTS/storage
ln -nfs $OGB/storage $RELEASES/$LTS/storage
php7.0 $OGB/lts/artisan storage:link
#chmod 777 /home/tdeployer/ogb/releases/$LTS/storage
chmod 777 $RELEASES/$LTS/bootstrap/cache
cd $RELEASES && rm -rf `ls -t | tail -n +4`