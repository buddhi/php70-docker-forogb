<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class FireEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $class_name,$email_attribute,$params;
    public function __construct($class_name,$email_attribute,$params)
    {
        $this->class_name = $class_name;
        $this->email_attribute = $email_attribute;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new $this->class_name($this->email_attribute,$this->params);
        Mail::to($this->email_attribute->to)->send($email);
    }

}
