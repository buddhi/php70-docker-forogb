<?php

namespace App\Jobs;

use App\Mail\ContactDeveloper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $sender;
    private $admin;
    private $ccUsers;
    private $text_message;
    private $farmer;
    private $subject,$agent;

    public function __construct($sender,$admin,$ccUsers,$text_message,$farmer,$subject,$agent)
    {
        $this->sender = $sender;
        $this->admin = $admin;
        $this->ccUsers = $ccUsers;
        $this->text_message = $text_message;
        $this->farmer = $farmer;
        $this->subject = $subject;
        $this->agent = $agent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $contact_developer = new ContactDeveloper($this->farmer->farmer_name,$this->text_message,$this->subject,$this->sender->full_name,$this->agent->full_name);
        Mail::to($this->admin)->cc($this->ccUsers)->send($contact_developer);
    }
}
