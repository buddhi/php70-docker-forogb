<?php

namespace App\Jobs;

use App\Mail\SendEmailForApprovedProject;
use App\Mail\SendEmailForFundedProject;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJobForApprovedProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected  $agent,$project,$farmer;
    public function __construct($agent,$project,$farmer)
    {
        $this->agent  = $agent;
        $this->project = $project;
        $this->farmer = $farmer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->agent->email)->send(new SendEmailForApprovedProject($this->agent,$this->project,$this->farmer));
    }
}
