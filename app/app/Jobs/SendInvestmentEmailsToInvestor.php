<?php

namespace App\Jobs;

use App\Mail\SendInvestmentDetailEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class SendInvestmentEmailsToInvestor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $projectInvestments;
    public function __construct($user,$projectInvestments)
    {
        $this->user = $user;
        $this->projectInvestments = $projectInvestments;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)->send(new SendInvestmentDetailEmail($this->user,$this->projectInvestments));
    }
}
