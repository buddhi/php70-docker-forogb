<?php

namespace App\Jobs;

use App\Mail\SendEmailForFundedProject;
use App\Mail\SendEmailToInvestorForFundedProject;
use App\Models\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJobForFundedProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $investors, $project_id;
    public function __construct($investors, $project_id)
    {
        $this->investors = $investors;
        $this->project_id = $project_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $project = Project::with('farmers', 'plan')->find($this->project_id);
        $agent = User::find($project->created_by);
        $farmer = $project->farmers->first();
        $plan = $project->plan()->first();
        $investment = $project->investment;
        $amount_raised = $investment['investment_amount'];
        $project_cost = $project->cost;
        $amount_required = $project_cost - $amount_raised;
        Mail::to($agent->email)->send(new SendEmailForFundedProject($project, $agent, $farmer, $plan));
        $investors = $this->investors;
        foreach ($investors as $investor) {
            Mail::to($investor)->send(new SendEmailToInvestorForFundedProject($investor, $project, $farmer, $amount_raised, $amount_required));
        }

    }
}
