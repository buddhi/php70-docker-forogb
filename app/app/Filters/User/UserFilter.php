<?php


namespace App\Filters\User;


use App\User;
use Illuminate\Http\Request;

class UserFilter
{

    protected $request;

    protected $related_models = ['roles'];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function filter()
    {
        $filterStatus = $this->request->get('filter') ? $this->request->get('filter') : 'active';
        $users = $this->getUsersQueryBuilderDependingUponStatus($filterStatus)->get();
        return $users;
    }

    private function getUsersQueryBuilderDependingUponStatus($filterStatus) {
        switch ($filterStatus){
            case 'inactive': return User::InActive();
            case 'all': return User::getAllUsers();
            default: return User::Active();
        }
    }
}
