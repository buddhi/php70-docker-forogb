<?php

namespace App\Filters\Project;

use App\Enums\ProjectStatus;
use App\Models\Meter;
use App\Models\Project;
use App\OGB\Constants;
use Illuminate\Http\Request;

class ProjectFilter
{

    protected $request;

    protected $related_models = ['uploadable', 'comments', 'user'];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function filter()
    {
        $filterStatus = $this->request->get('filter') ? $this->request->get('filter') : 'active';
        $projects = $this->getProjectsQueryBuilderDependingUponStatus($filterStatus);
        $projects = $this->getProjectListByStatus($filterStatus, $projects);
        return $projects;
    }

    private function getProjectsQueryBuilderDependingUponStatus($filterStatus)
    {
        switch ($filterStatus) {
            case 'inactive':
                return Project::InActive();
            case 'all':
                return Project::getAllProjects();
            case 'plan':
                return Project::getAllProjects()->where('delete_flg', 0);
            default:
                return Project::Active();
        }
    }

    private function getProjectListByStatus($filterStatus, $projects)
    {

        if ($filterStatus == 'notoperational') {
            return $projects->with($this->related_models)->where('status', '!=', ProjectStatus::OPERATIONAL)->get();
        }

        if ($this->checkProjectStatus($filterStatus)) {
            return $projects->where('status', '=', $filterStatus)->get();
        }

        return $projects->get();
    }

    protected function checkProjectStatus($status)
    {
        if (in_array($status, ProjectStatus::ALL_PROJECT_STATUS, true)) {
            return true;
        }
        return false;
    }
}