<?php

namespace App\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class OgbAdminViewComposer
{
    public function compose(View $view)
    {
        $notifications = Auth::check() ? auth()->user()->notifications : [];
        $unread_notification_count = Auth::check() ? auth()->user()->unreadNotifications->count() : 0;
        $view->with('notifications', $notifications);
        $view->with('unread_notification_count', $unread_notification_count);
    }
}