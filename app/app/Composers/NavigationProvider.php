<?php

namespace App\Composers;

use App\Models\Project;
use App\Models\Farmer;
use App\Models\ProjectInvestment;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;


class NavigationProvider
{
    public function compose(View $view)
    {
        $project_count = 0;
        $received_investment = 0;
        $expected_investment = 0;
        $project_count = Project::where('status', '!=', 'plan')->count();
        $received_investment = ProjectInvestment::sum('invest_amount');
        $total_cost = Project::where('status', '!=', 'plan')->sum('cost');
        $expected_investment = $total_cost - $received_investment;
        $solar_capacity = Project::where('status', '!=', 'plan')->sum('solar_pv_size');
        $lives_impacted = Farmer::join('farmer_project', 'farmer_project.farmer_id', '=', 'farmers.id')
            ->join('tbl_project', 'farmer_project.project_id', '=', 'tbl_project.id')
            ->where('tbl_project.status', '!=', 'plan')
            ->sum('no_of_people_in_house');
        $total_co2_curbed = Project::where('status','!=','plan')->get(['solar_pv_size'])->reduce(function ($total, $item) {
            return $total + calculateEnvParams($item->solar_pv_size)['co2_curbed'];
        }, 0);
        $view->with('total_co2_curbed', $total_co2_curbed);
        $view->with('project_count', $project_count);
        $view->with('received_investment', $received_investment);
        $view->with('expected_investment', $expected_investment);
        $view->with('solar_capacity', $solar_capacity);
        $view->with('lives_impacted', $lives_impacted);
    }
}