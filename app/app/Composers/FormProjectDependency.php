<?php

namespace App\Composers;

use App\Models\Cattle;
use App\Models\Crop;
use App\Models\District;
use App\Models\Municipality;
use App\Models\ProjectType;
use App\Models\Province;
use Illuminate\Contracts\View\View;

class FormProjectDependency
{
    public function compose(View $view){
        $districts = District::all();
        $municipalities = Municipality::all();
        $provinces = Province::all();
        $project_types = ProjectType::all();
        $crops = Crop::all();
        $cattles = Cattle::all();
        $view->with('districts',$districts);
        $view->with('municipalities',$municipalities);
        $view->with('provinces',$provinces);
        $view->with('project_types',$project_types);
        $view->with('crops',$crops);
        $view->with('cattles',$cattles);
    }
}