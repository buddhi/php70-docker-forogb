<?php


namespace App\Repositories\CashPayment;
use App\Enums\PaymentType;
use App\Models\Payment;
use App\Models\Project;
use App\Repositories\TransactionModule\TransactionModuleRepository;

use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;

class CashPaymentService implements CashPaymentRepository
{
    private $transactionModuleRepository;
    public function __construct(TransactionModuleRepository $transactionModuleRepository)
    {
        $this->transactionModuleRepository = $transactionModuleRepository;
    }

    private static function getReferenceCode()
    {
        return[
            'referenceCode' => Uuid::generate()->string
        ];
    }

    public function payment(array $data)
    {
        $paymentGateway = $data['paymentGateway'];
        $payment_id = $data['payment_id'];
        $code = self::getReferenceCode();
        $payment_param = $this->resolvePaymentParameters($data,$code);
        if(!$payment_param){
            return false;
        }
        $payment = Payment::create($payment_param);
        $transaction_param = $this->resolveTransactionParameters($payment,$payment_id,$code);
        if(!$transaction_param){
            return false;
        }
        $this->transactionModuleRepository->storeTransaction($transaction_param,$payment,$paymentGateway);
        return $payment;
    }

    private function resolvePaymentParameters(array $data ,$code)
    {
        $paymentId = $data['payment_id'];
        $project = self::getRelatedEntity($paymentId);
        $paymentParameter = [
            'code' => json_encode($code),
            'paid_status' => 1,
            'paid_date' => $data['payment_date'],
            'project_id' => $project->id,
            'payment_gateway' => 'cash', //
        ];
        if (!$project->isAdvanceAmountPaid()) {
            return array_merge($paymentParameter, [
                'amount' => $project->advance_amount,
                'payment_type' => PaymentType::ADVANCE
            ]);
        }

        $paymentParameter = array_merge($paymentParameter, [
            'emi_start_month' => $project->nextEmiStartDate(),
            'emi_end_month' => $project->nextEmiEndDate(),
            'amount' => $project->emi_amount,
            'payment_type' => PaymentType::EMI
        ]);
        return $paymentParameter;
    }

    private function resolveTransactionParameters($payment, $payment_id , $transaction_code)
    {
        $project = self::getRelatedEntity($payment_id);
        return [
            'payer_email' => Auth::user()? Auth::user()->email:null,
            'payer_name' => Auth::user()? Auth::user()->full_name:null,
            'transaction_id' => 'unknown', //magic string
            'amount' => isset($payment->amount) ? $payment->amount : null,
            'currency' => "NRS",
            'transaction_type' => isset($payment->payment_type) ? $payment->payment_type : PaymentType::INVALID,
            'project_id' => isset($payment->project_id) ? $payment->project_id : ($project != false) ? $project->id : null,
            'payment_gateway' => $payment->payment_gateway,
            'status' => isset($payment) ? 'paid' : 'unpaid',
            'response_data' => isset($payment) ? json_encode($payment->toArray()) : null,

        ];
    }

    //Gets the related Project(Entity) for particular payment id
    private static function getRelatedEntity($paymentId)
    {
        $project = Project::where('payment_id', $paymentId)->first();
        if (isset($project)) {
            return $project;
        }
        return false;
    }
}