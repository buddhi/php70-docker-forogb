<?php

namespace App\Repositories\CashPayment;

interface CashPaymentRepository
{
    public function payment(array $input_param);
}