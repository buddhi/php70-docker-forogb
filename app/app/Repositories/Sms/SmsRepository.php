<?php

namespace App\Repositories\Sms;

interface SmsRepository
{
    public function sendSmsNotification($to, $type, $substitution = [], $message = null);
}