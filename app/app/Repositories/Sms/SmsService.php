<?php

namespace App\Repositories\Sms;

use App\Utilities\StringManipulator;

abstract class SmsMessage
{
    public static $MESSAGES = [

        'PROJECT_APPROVED_MESSAGE_FARMER' => "Dear :farmer_name, your project at OffGrid Bazaar has been approved by Gham Power. Please deposit Booking Fee with eSewa to start loan/financing process.",
        'PROJECT_APPROVED_MESSAGE_AGENT' => 'Dear :agent_name, your project at OffGrid Bazaar for :farmer_name has been approved by Gham Power. Please collect Booking Fee to get your commission.',

        'PROJECT_FUNDED_MESSAGE_FARMER' => 'Dear :farmer_name, your Gham Power solar pump project is ready to install. Please contact our eSewa agent and deposit first month EMI to install.',
        'PROJECT_FUNDED_MESSAGE_AGENT' => 'Dear :agent_name, your OffGridBazaar project for :farmer_name has been fully funded. Please collect first month EMI to schedule the installation date.',

        'PROJECT_INSTALLED_MESSAGE_FARMER' => 'Dear :farmer_name, your Gham Power solar pump project is installed. Please pay next EMI by :next_due_date.',
        'PROJECT_INSTALLED_MESSAGE_AGENT' => 'Dear :agent_name, your Gham Power project for :farmer_name has been installed. Please collect next EMI by :next_due_date.',

        'PROJECT_OPERATIONAL_TO_OVERDUE_MESSAGE_FARMER' => 'Dear :farmer_name , your Gham Power solar pump EMI is overdue. Please pay EMI urgently to avoid switching off of the system.',
        'PROJECT_OPERATIONAL_TO_OVERDUE_MESSAGE_AGENT' => 'Dear :agent_name, your Gham Power project for :farmer_name is overdue. Please collect EMI urgently to get commission and prevent switch off. ',

        'PROJECT_OVERDUE_TO_OPERATIONAL_MESSAGE_FARMER' => 'Dear :farmer_name, thank you for paying your overdue amount for Gham Power project. Your project is operational. Please pay future EMI on time.',
        'PROJECT_OVERDUE_TO_OPERATIONAL_MESSAGE_AGENT' => 'Dear :agent_name, your overdue project for :farmer_name is now paid and operational. Thank you for collecting EMI - GhamPower.',

        'EMI_PAID_MESSAGE_FARMER' => 'Dear :farmer_name, thank you for payment Rs :amount for EMI number :emi_no/36, :emi_month for your Gham Power project. Next due date-:next_due_date.',
        'EMI_PAID_MESSAGE_AGENT' => 'Dear :agent_name, thank you for payment Rs :amount for EMI number :emi_no/36,:emi_month for :farmer_name Gham Power project.',

        'ADVANCE_PAID_MESSAGE_FARMER' => 'Dear :farmer_name, thank you for payment of Rs. :amount as Booking fee/Advance for your Gham Power project.Loan Processing will now start now.',
        'ADVANCE_PAID_MESSAGE_AGENT' => 'Dear :agent_name,thank you for payment Rs :amount as Booking fee/Advance for :farmer_name s Gham Power project.Loan process will start now.',

        'SMS_ACTION' => 'Value Exceeded Threshold',
        'SMS_CHANGE_STATUS' => '',

        'OTP_MESSAGE' => 'Dear user,your otp code is :otp_code',
        'PROJECT_IN_GRACE_DAYS_MESSAGE_FARMER' => 'Dear :farmer_name , EMI payment for Gham Power solar pump is due in :number_of_days days. Please pay EMI urgently to avoid switching off of the system.',

    ];
}


class SmsService implements SmsRepository
{

    private $sms_notify;
    public function __construct(SendSms $sms_notify)
    {
        $this->sms_notify = $sms_notify;
    }

    public function sendSmsNotification($to, $type, $substitution = [], $sms_value = null)
    {
        $message = $this->getMessageText($type, $substitution, $sms_value);
        if (!$message) {
            return "Invalid SMS type";
        }
        return $this->sms_notify->send_sms_message($to, $message);
    }

    public function getMessageText($type, $substitution, $sms_value)
    {
        //Check the sms message type
        if (!isset(SmsMessage::$MESSAGES[strtoupper($type)])) {
            return false;
        }
        //Getting the message from the request
        if (!is_null($sms_value))
            return $sms_value;

        //Getting the message according to the sms type
        $message = SmsMessage::$MESSAGES[strtoupper($type)];
        if (empty($substitution) || is_null($substitution)) {
            return $message;
        }

        //Replacing the variable(denoted by : ie :farmer_name) and getting message
        $replaced_message = StringManipulator::makeReplacements($message, $substitution);
        return $replaced_message;
    }


}