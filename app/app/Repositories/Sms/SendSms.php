<?php

namespace App\Repositories\Sms;

use App\Repositories\Logger\ApplicationLogger;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Support\Facades\App;

class SendSms
{
    private $guzzle, $applicationLogger;

    public function __construct(\GuzzleHttp\Client $guzzle, ApplicationLogger $applicationLogger)
    {
        $this->guzzle = $guzzle;
        $this->applicationLogger = $applicationLogger;
    }

    public function getSMSAPIEndpoint($phone, $value)
    {
        if (App::environment('production', 'staging')) {
            return 'http://mobile.sms123go.com/http.aspx?guid='
                . env('SMS123_GUID')
                . '&username='
                . env('SMS123_USERNAME')
                . '&password='
                . env('SMS123_PASSWORD')
                . '&countryCode=np&mobileNumber=%2b977'
                . $phone
                . '&message=' . $value;
        }

        // TODO: replace with local developement API
        return 'https://localhost:56666/';
    }

    public function send_sms_message($to, $message)
    {
        try {
            $this->guzzle->get($this->getSMSAPIEndpoint($to,$message));
            $this->logMessage($to, true);
            return "User notified successfully";
        } catch (TransferException $e) {
            $this->logMessage($to, false, $e);
            return "SMS Gateway unresponsive";
        }
    }

    public function logMessage($phone_number, $message_sent = true, $e = null)
    {
        $log_message = "Message sent to phone number {$phone_number}";
        if (!$message_sent) {
            $log_message = "Failed to send SMS to {$phone_number}";
        }
        $this->applicationLogger->logMessage('SMS', 'logs/sms/sms.log', $log_message);
        if (isset($e)) {
            report($e);
        }
    }
}