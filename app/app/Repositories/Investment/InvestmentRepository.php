<?php

namespace App\Repositories\Investment;

interface InvestmentRepository
{
    public function submitCheckout($projectPayment, $paymentGateway);

    public function getPaymentStatus($data, $paymentGateway);

    public function checkProjectPaymentCheckout($data ,$paymentGateway);

    public function investmentDataStore($data, $projectPayments);

    public function investmentTransactionStore($data, $projectInvestments, $paymentGateway);

    public function getByIdWithRelations($id, $relations);

}