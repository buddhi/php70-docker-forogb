<?php

namespace App\Repositories\Investment;

use App\components\ProjectUtil;
use App\Enums\FundingType;
use App\Enums\InvestmentType;
use App\Enums\ProjectStatus;
use App\Jobs\SendEmail;
use App\Models\Project;
use App\Models\ProjectInvestment;
use App\Repositories\PaymentModule\PaymentModuleRepository;
use App\Repositories\Project\ProjectStateUpdateRepository;
use App\Repositories\TransactionModule\TransactionModuleRepository;
use App\Services\ProjectStateUpdateTrigger;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvestmentService implements InvestmentRepository
{
    protected $paymentModuleRepository, $transactionModuleRepository,$projectStateUpdateRepository,$projectStateUpdateTrigger;

    public function __construct(
        PaymentModuleRepository $paymentModuleRepository,
        TransactionModuleRepository $transactionModuleRepository,
        ProjectStateUpdateRepository $projectStateUpdateRepository,
        ProjectStateUpdateTrigger $projectStateUpdateTrigger
    )
    {
        $this->paymentModuleRepository = $paymentModuleRepository;
        $this->transactionModuleRepository = $transactionModuleRepository;
        $this->projectStateUpdateRepository = $projectStateUpdateRepository;
        $this->projectStateUpdateTrigger = $projectStateUpdateTrigger;
    }


    /**
     * @param $projectPayments:contains details of project for payment
     * @param $paymentGateway:default paypal
     * @return mixed:details obtained from the processing payments
     * calls the paymentHandler function to process the payments according to payment gateways
     */
    public function submitCheckout($projectPayments, $paymentGateway)
    {
        return $this->paymentModuleRepository->paymentHandler($projectPayments, $paymentGateway);

    }

    public function getPaymentStatus($data, $paymentGateway)
    {
        return $this->paymentModuleRepository->paymentStatus($paymentGateway);
    }

    //Checking every project doesnt exceeds the payment
    public function checkProjectPaymentCheckout($data ,$paymentGateway)
    {
        $project_ids = $data['request']->get('project_id');
        $projects = Project::findMany($project_ids);
        $project_investments = $this->getPreviousProjectInvestments($projects);
        $investment_details = $this->getCurrentInvestmentDetails($projects, $data['request']);
        $projectPaymentStatus = [];
        foreach ($projects as $project) {
            $projectPaymentStatus[$project->id] = $this->checkInvestmentForProject($investment_details[$project->id], $project_investments[$project->id], $project->id,$paymentGateway);
        }

        return $projectPaymentStatus;
    }

    private function getPreviousProjectInvestments($projects)
    {
        return $projects->mapWithKeys(function ($project) {
            return [$project['id'] => ProjectUtil::getProjectInvestmentTotal($project->id)];
        });
    }

    //Get current investments
    private function getCurrentInvestmentDetails($projects, $request)
    {
        $investment_details = [];

        foreach ($projects as $key => $project) {
            $investment_details[$project->id]['developer_id'] = $project->created_by;
            $investment_details[$project->id]['investor_id'] = Auth::user()->id;
            $investment_details[$project->id]['funding_type'] = $request->get('funding_type')[$key];
            $investment_details[$project->id]['invest_percent'] = $request->get('invest_percent')[$key];
            $investment_details[$project->id]['invest_amount'] = $request->get('invest_amount')[$key];
            $investment_details[$project->id]['interest_rate'] = $request->get('interest_rate')[$key];
            $investment_details[$project->id]['term'] = $request->get('term')[$key];
        }
        return $investment_details;
    }

    //Checking investment if valid for project. Investment cannot exceed the project cost
    public function checkInvestmentForProject($data, $investmentTotalData, $project_id,$paymentGateway)
    {
        if (!(isset($data) && isset($investmentTotalData))) {
            return false;
        }
        $project = Project::findOrFail($project_id);
        $projectCost = (float)$project->cost;
        $totalPercent = 0;
        $totalAmount = 0;
        $expectedIrr = 0;
        $enteredAmount = 0;
        $term = isset($data['term'])?$data['term']:0;
        $interest_rate = isset($data['interest_rate'])?$data['interest_rate']:0;
        $enteredAmount = $data['invest_amount'];
        $enteredPercent = $data['invest_percent'];
        $currentInvestedAmount = $investmentTotalData['investment_amount'];
        $currentInvestedPercent = $investmentTotalData['investment_percent'];
        $totalPercent = $enteredPercent + $currentInvestedPercent;
        $totalAmount = $enteredAmount + $currentInvestedAmount;
        $this->paymentModuleRepository->logPaymentAttempt($enteredAmount,$project->id,$paymentGateway);
        if ($totalAmount > $projectCost) {
            return $this->getMessage("error", 'Entered amount or percentage exceeds project amount', "payment.failure");
        }
        $payment['project_id'] = $project_id;
        $payment['developer_id'] = $project->created_by;
        $payment['investor_id'] =  Auth::user()->user_id;
        $payment['funding_type'] = array_key_exists('funding_type', $data) ? $data['funding_type'] : FundingType::Percent;
        $payment['percent'] = $enteredPercent;
        $payment['amount'] = $enteredAmount;
        $payment['investment_type_id'] = InvestmentType::Grant;
        $payment['expected_irr'] = $expectedIrr ? $expectedIrr : null;
        $payment['term'] = $term ? $term : null;
        $payment['interest_rate'] = $interest_rate ? $interest_rate : null;
        return $payment;
    }

    public function getMessage($message_type, $message, $route = null)
    {
        $returnValue['message_type'] = $message_type;
        $returnValue['message'] = $message;
        $returnValue['route'] = $message;
        return $returnValue;
    }

    //Storing the investment details of every project.
    public function investmentDataStore($data, $projectPayments)
    {
        $projectInvestments = [];
        foreach ($projectPayments as $projectPayment) {
            $project_id = $projectPayment['project_id'];
            $project = Project::findOrFail($project_id);
            $totalProjectCost = $project->cost;try {
                $projectInvestment = ProjectInvestment::create([
                    'project_id' => $projectPayment['project_id'],
                    'developer_id' => $projectPayment['developer_id'],
                    'investor_id' => $projectPayment['investor_id'],
                    'investment_type_id' => $projectPayment['investment_type_id'],
                    'invest_amount' => $projectPayment['amount'],
                    'investment_percent' => $projectPayment['percent'],
                    'funding_type' => (int)$projectPayment['funding_type'],
                    'expected_irr' => $projectPayment['expected_irr'],
                    'term' => $projectPayment['term'],
                    'interest_rate' => $projectPayment['interest_rate'],
                    'created_by' => $projectPayment['developer_id'],
                    'created_on' => Carbon::now()
                ]);
                $projectInvestments[] = $projectInvestment;

            } catch (\Exception $exception) {
                DB::rollback();
            }
            DB::commit();

            //Checking the total amount of the project after funding to check status
            $totalInvestmentForProject = ProjectUtil::getProjectInvestmentTotal($project->id);
            if (isset($totalInvestmentForProject) && isset($totalInvestmentForProject['investment_amount'])) {
                if ((float)$totalInvestmentForProject['investment_amount'] == (float)$totalProjectCost) {
                    $project->status = ProjectStatus::FUNDED;
                    $project->save();
                    $this->projectStateUpdateRepository->updateTransitionStateOfProject($project->id,$project->status);
                    $this->projectStateUpdateTrigger->sendSmsEmailOnStateChange($project,ProjectStatus::FUNDED);
                }
            }
        }
        return $projectInvestments;
    }

    //storing transaction of every investment
    public function investmentTransactionStore($data, $projectInvestments, $paymentGateway)
    {
        $transaction = $this->transactionModuleRepository->storeTransaction($data, $projectInvestments, $paymentGateway);
        return $transaction;
    }

    //Getting projectId with relations
    public function getByIdWithRelations($id, $relations)
    {
        if(!is_array($relations)){
            return null;
        }
        return Project::with($relations)->findOrFail($id);
    }

}