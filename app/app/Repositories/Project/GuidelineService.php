<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 20/07/2019
 * Time: 15:59
 */

namespace App\Repositories\Project;

use App\Models\Project;
use App\Models\ProjectTimeline;
use App\Models\Status;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class GuidelineService implements GuideLineRepository
{
    public function getGuidelines(Project $project)
    {
        // TODO: Implement getGuidelines() method.
        $data = new \stdClass();
        $nextStatus = $project->getNextStatus($project->status);
        $status = Status::where('name', $project->status)->first();

//        dd($nextStatus);
        $hasUploadable = ($status->guidelines()->where('type','upload')->count())>0?true:false;
        $guideline = $status->guidelines()->withCount(['timeline' => function(Builder $q) use($project){
            $q->where('project_id',$project->id)->where('delete_flg',0);
            return $q;
        }])->get();
        $data->hasUploadable = $hasUploadable;
        $data->guideline = $guideline->toArray();
        $data->currentStatus = $project->status;
        $data->nextStatus = $nextStatus;

        $nextStatus = $project->getNextStatus($project->status);
//            dd('hello',$projectStatus);
        $status = Status::where('name', $project->status)->first();

//            dd($currentProjectStatus);
        $nextProject = Status::where('name', $nextStatus)->first();
//            dd($nextProject);
        $nextStatusGuidelines = $status->guidelines()->count();
//            dd($nextProject->id,$project->id);
        $currentFulfilledGuidelines = ProjectTimeline::where('status_id', $status->id)->where('project_id', $project->id)->where('delete_flg', 0)->count();
//            dd($nextStatusGuidelines,$currentFulfilledGuidelines);

        if ($nextStatusGuidelines == $currentFulfilledGuidelines && $status) {
            $data->fullfilledGuideline = true;
        }
        else{
            $data->fullfilledGuideline = false;
        }
//        dd($data);
        return $data;

//        foreach ($guideline as $line) {
//            $selectedGuideline = ProjectTimeline::where('project_id',$project->id)->where('status_id',$status->id+1)->first()!=null?true:false;
//            dd($selectedGuideline);
//        }
    }
}