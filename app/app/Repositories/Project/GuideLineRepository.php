<?php

namespace App\Repositories\Project;

use App\Models\Project;

interface GuideLineRepository
{
    public function getGuidelines(Project $project);
}
