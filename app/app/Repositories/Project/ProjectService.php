<?php


namespace App\Repositories\Project;

use App\Enums\ProjectStatus;
use App\Enums\Role;
use App\Models\CattleInfo;
use App\Models\Farmer;
use App\Http\Resources\ProjectIndexResource;
use App\Models\Category;
use App\Models\Document;
use App\Http\Resources\Project as ResourceForProject;

use App\Models\Guideline;
use App\Models\Project;
use App\Models\ProjectLog;
use App\Models\ProjectTimeline;
use App\Models\Province;
use App\Models\District;
use App\Models\ProjectType;
use App\Models\Status;
use App\Models\Upload;
use App\OGB\Constants;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\Draft\DraftRepository;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farmer\FarmerRepository;
use App\Repositories\ProjectPlan\ProjectPlanRepository;
use App\Repositories\Upload\UploadRepository;
use App\Scope\ProjectScope;
use App\Services\CreditScore;
use App\Services\WaterFlowCalulation;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class ProjectService implements ProjectRepository
{
    protected $project_model, $farmer_model, $farm_model, $cropInfo_model, $projectStateUpdateRepository, $farmerRepository, $farmRepository, $cropInfoRepository, $cattleInfoRepository, $draftRepository, $projectPlanRepository, $uploadRepository, $folder_path;

    public function __construct(
        Project $project_model,
        ProjectStateUpdateRepository $projectStateUpdateRepository,
        FarmerRepository $farmerRepository,
        FarmRepository $farmRepository,
        CropInfoRepository $cropInfoRepository,
        CattleInfoRepository $cattleInfoRepository,
        DraftRepository $draftRepository,
        ProjectPlanRepository $projectPlanRepository,
        UploadRepository $uploadRepository
    )
    {
        $this->project_model = $project_model;
        $this->projectStateUpdateRepository = $projectStateUpdateRepository;
        $this->farmerRepository = $farmerRepository;
        $this->farmRepository = $farmRepository;
        $this->cropInfoRepository = $cropInfoRepository;
        $this->cattleInfoRepository = $cattleInfoRepository;
        $this->draftRepository = $draftRepository;
        $this->projectPlanRepository = $projectPlanRepository;
        $this->uploadRepository = $uploadRepository;
        $this->folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'project_details' . DIRECTORY_SEPARATOR;

    }

    //Gets the Project
    public function getById($id)
    {
//        $project = DB::table('tbl_project')->where('id',$id)->first();
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($id);
//        $this->changeStatus($project);
        return $project;
    }

    //Gets all the project
    public function getAll()
    {
        return $this->project_model->all();
    }


    /**********************
     * CREATING(STORING) PROJECT
     **************************/
    //If submit-analysis in form(ie button) then store(create) project with associated details
    //  store(S) performs following tasks
    //  S1 stores basic project details
    //  S2 Update the status of project to plan stage
    //  S3 Stores Crop details related with Project(CropInfo model ie Crop-Project pivot table)
    //  S4 Stores Cattle details related with Project(CattleInfo model ie Cattle-Project pivot table)
    //  S5 Stores farmer details and bind farmer with project
    //  S6 Stores farm details and bind farm with farmer
    //  S7 Calculate the risk analyis(credit score) of farmer
    //  S8 Generate paymentId related with project

    //If submit in form(ie button) then store/update draft
    public function store(array $data)
    {

        $return_value = null;
        $form = $data['request']->all();
// dd($data);

        if (is_array($form) && in_array('submit-analysis', $form)) {

//Checking the draft related with project and deleting it while creating Project
            $draft_id = $data['request']->id ? $data['request']->id : null;
            if (isset($draft_id)) {
                $draftProject = $this->draftRepository->getById($draft_id);
                $this->draftRepository->delete($draftProject);
            }

//To avoid the conflict between draft id and project we dont take id from form
//S1.stores basic project details
            $this->project_model->fill($data['request']->except('id'));
            $this->project_model->status = ProjectStatus::PLAN;
            $this->project_model->created_on = date('Y-m-d H:i:s');
            $this->project_model->save();

            //S2.store/update the transition state of project
            $this->projectStateUpdateRepository->updateTransitionStateOfProject($this->project_model->id, $this->project_model->status);

//S3. Store the crop-project(ie model CropInfo) details
            $this->cropInfoRepository->setCropInfo($data, $this->project_model);

//S4. Store the cattle-project(ie model CattleInfo) details
            if (!empty($data['request']->cattle_id[0])) {
                $this->cattleInfoRepository->setCattleInfo($data, $this->project_model);
            }
//S5. Store farmer details && bind Project-Farmer in pivot table
            $this->farmer_model = $this->farmerRepository->setFarmerAttributes($data, $this->project_model);
            $this->project_model->farmers()->sync($this->farmer_model->id);

//S6. Store farm details
            $this->farm_model = $this->farmRepository->setFarmAttributes($data, $this->farmer_model);

//S7. Calculate the risk-analysis of farmer and store it
            $this->farmer_model->credit_score = $this->calculateCreditScore($this->farmer_model->id);
            $this->farmer_model->save();

//S8. Generate PaymentId for creating new project
            $this->setPaymentId();

//set the redirect routes
            $redirect_route = 'ogbadmin.project.selectplan';
            $project_id = $this->project_model->id;
            $return_value['redirect'] = $redirect_route;
            $return_value['id'] = $project_id;

            $this->changeProjectStatus('plan',$this->project_model->id);

        } elseif (in_array('submit', $form)) {
//If id present then it is draft which we update.

            if (!$data['request']->id) {
                $return_value = $this->draftRepository->draftStore($data);
            } else {
                $return_value = $this->draftRepository->draftUpdate($data, $data['request']->id);
            }


            //set the redirect routes
//        $redirect_route = 'project.selectPlan';
//        $project_id = $this->project_model->id;
//        $return_value['redirect'] = $redirect_route;
//        $return_value['id'] = $project_id;
//        return $return_value;
        }
        return $return_value;
    }


    /*************************
     * UPDATE PROJECT
     *************************
     * @param array $data
     * @param $project
     * @return array|string
     */

    //  This function updates the project and associated details
    //  U1 Updating basic project details
    //  U2 Update Crop details related with Project(CropInfo model ie Crop-Project pivot table)
    //  U3 Update Cattle details related with Project(CattleInfo model ie Cattle-Project pivot table)
    //  U4 Update farmer details and bind farmer with project
    //  U5 Update farm details and bind farm with farmer
    //  U6 Update the credit score related with project

    public function updateProjectData(array $data, $project)
    {
        $crop_project = [];
        $this->project_model = $project;
        //U1 Updates the projects(even admin can update the project so we avoid the project created_by update from admin)
        $this->project_model->update($data['request']->except('created_by'));

        //U2 Update Crop details related with Project(CropInfo model ie Crop-Project pivot table)
        //Update ==> deleting the previous data that user has deleted + storing the new one
        //Deleting crop-project data if the user has deleted the crop project data
        //Storing the crop-project(ie cattle info) data
        $this->cropInfoRepository->deleteCropInfo($data, $this->project_model);

        $this->cropInfoRepository->setCropInfo($data, $this->project_model);
        // U3 Update Cattle details related with Project(CattleInfo model ie Cattle-Project pivot table)
        //Check and deletes the cattle-project data if the user has deleted the cattle project data
        //Update the cattle-project data
        /*if($data['request']['has_cattle']==0){
            $cattle=CattleInfo::where('project_id',$project->id)->destroy();
        }*/


        if ($data['request']['has_cattle'] == 0) {
            $cattles = CattleInfo::where('project_id', $project->id)->get();
            foreach ($cattles as $cattle) {
                $cc = DB::table('tbl_cattle_info')->where('id', $cattle->id);
                $cc->delete();
            }
        } else {
            $this->cattleInfoRepository->deleteCattleInfo($data, $this->project_model);
            $this->cattleInfoRepository->setCattleInfo($data, $this->project_model);
        }
        //U4 Updating the farmer details in project
        $this->farmer_model = $this->project_model->farmers->first();

//        $this->farmer_model = $this->farmerRepository->setFarmerAttributes($data, $this->farmer_model, $this->project_model);
        $this->farmer_model = $this->farmerRepository->updateFarmerAttributes($data, $this->farmer_model, $this->project_model);

        //U5 Updating the farm details in project
        $this->farm_model = $this->farmer_model->farm;

//        $this->farm_model = $this->farmRepository->setFarmAttributes($data, $this->farm_model, $this->farmer_model);
        $this->farm_model = $this->farmRepository->updateFarmAttributes($data, $this->farm_model, $this->farmer_model);

        //U6 Calculate the credit score related with project
        //Some of the credit score analysis of farmer also depends upon the related farm of farmer
        //Credit score is always calculated after calculating farmer and farm details
        $this->farmer_model->credit_score = self::calculateCreditScore($this->farmer_model->id);
        $this->farmer_model->save();
        $this->project_model->farmers()->sync($this->farmer_model->id);
        //Redirect routes based on admin and developer(agent)
        $return_value = [];
        /*
        if (session('role') == Role::DEVELOPER) {
            $this->project_model->status = ProjectStatus::PLAN;
            $this->project_model->save();
            $this->projectStateUpdateRepository->updateTransitionStateOfProject($this->project_model->id, $this->project_model->status);
            $return_value['route'] = "project.selectPlan";
        } else {
            if (session('role') == Role::ADMIN) {
                $return_value['route'] = 'admin.project.list';
                return $return_value['route'];
            }
        }*/
        $return_value['route'] = 'ogbadmin.project.index';
        return $return_value;
    }

    public function update(array $data, $project)
    {
        $crop_project = [];
        $this->project_model = $project;

        //U1 Updates the projects(even admin can update the project so we avoid the project created_by update from admin)
        $this->project_model->update($data['request']->except('created_by'));

        //U2 Update Crop details related with Project(CropInfo model ie Crop-Project pivot table)
        //Update ==> deleting the previous data that user has deleted + storing the new one
        //Deleting crop-project data if the user has deleted the crop project data
        //Storing the crop-project(ie cattle info) data
        $this->cropInfoRepository->deleteCropInfo($data, $this->project_model);
        $this->cropInfoRepository->setCropInfo($data, $this->project_model);

        // U3 Update Cattle details related with Project(CattleInfo model ie Cattle-Project pivot table)
        //Check and deletes the cattle-project data if the user has deleted the cattle project data
        //Update the cattle-project data
        $this->cattleInfoRepository->deleteCattleInfo($data, $this->project_model);
        $this->cattleInfoRepository->setCattleInfo($data, $this->project_model);

        //U4 Updating the farmer details in project
        $this->farmer_model = $this->project_model->farmers->first();
        $this->farmer_model = $this->farmerRepository->setFarmerAttributes($data, $this->farmer_model, $this->project_model);

        //U5 Updating the farm details in project
        $this->farm_model = $this->farmer_model->farm;
        $this->farm_model = $this->farmRepository->setFarmAttributes($data, $this->farm_model, $this->farmer_model);

        //U6 Calculate the credit score related with project
        //Some of the credit score analysis of farmer also depends upon the related farm of farmer
        //Credit score is always calculated after calculating farmer and farm details
        $this->farmer_model->credit_score = self::calculateCreditScore($this->farmer_model->id);
        $this->farmer_model->save();
        $this->project_model->farmers()->sync($this->farmer_model->id);

        //Redirect routes based on admin and developer(agent)
        $return_value = [];
        if (session('role') == Role::DEVELOPER) {
            $this->project_model->status = ProjectStatus::PLAN;
            $this->project_model->save();
            $this->projectStateUpdateRepository->updateTransitionStateOfProject($this->project_model->id, $this->project_model->status);
            $return_value['route'] = "project.selectPlan";
        } else {
            if (session('role') == Role::ADMIN) {
                $return_value['route'] = 'admin.project.list';
                return $return_value['route'];
            }
        }
        return $return_value;
    }

    //Selecting plan for project
    //Plans can be custom according to requirement
    public function selectPlanStore(array $data)
    {

        $comment = "";
        $this->project_model = $data['project'];
        $plan = $this->projectPlanRepository->getProjectPlanData($data['request']->Project['plan_id']);
        if (isset($plan)) {
            $this->project_model->plan_id = $data['request']->Project['plan_id'];
            $this->project_model->code = $plan->code;
            $this->project_model->name = $plan->name;
            $this->project_model->cost = $plan->plan_cost;
            $this->project_model->head = $plan->head;
            $this->project_model->pump_size = $plan->pump_size;
            $this->project_model->daily_discharge = $plan->daily_discharge;
            $this->project_model->solar_pv_size = $plan->solar_pv_size;
            $this->project_model->total_emi = $plan->total_emi;
            $this->project_model->advance_amount = $plan->advance_amount;
            $this->project_model->emi_amount = $plan->emi_amount;
            $this->project_model->emi_count = $plan->emi_count;
            $this->project_model->status = ProjectStatus::NEW;
            $this->project_model->emi_start_date = Carbon::now();
            $this->project_model->emi_end_date = Carbon::now()->addMonths(2);
            $this->project_model->save();

            $this->projectStateUpdateRepository->updateTransitionStateOfProject($this->project_model->id, $this->project_model->status);
            $this->changeProjectStatus('new',$this->project_model->id);
        }
        /*if ($data['request']->comment) {
        $comment = $data['request']->comment;
        }
        $this->project_model->comments()->create(['comment' => $comment]);*/
    }

    private function setPaymentId()
    {
        $this->project_model->payment_id = $this->project_model->generatePaymentId();
        $this->project_model->save();
    }


    public function fetchWaterRequirement($id)
    {
        $data = WaterFlowCalulation::calculateFlow($id);
        return $data;
    }

    //Choosing the plan whose daily discharge is closest to median of monthly water requirement.
    public function recommendPlanBasedOnWaterRequirement($daily_requirement_according_to_month, $plans)
    {
        $store_difference = null;
        $selected_plan = $plans->first();
        $daily_requirement_according_to_month = array_filter($daily_requirement_according_to_month, function ($a) {
            return ($a !== 0);
        });
        $median = self::calculate_median($daily_requirement_according_to_month);
        if (!isset($median)) {
            return null;
        }
        foreach ($plans as $key => $plan) {
            $difference = abs($median - $plan->daily_discharge);
            if (!isset($store_difference)) {
                $store_difference = $difference;
            }
            if ($difference < $store_difference) {
                $selected_plan = $plan;
            }
        }

        return $selected_plan;
    }


    //calculating median
    private static function calculate_median($array)
    {


        // All non numeric values should filtered out of $array here
        $iCount = count($array);
        if ($iCount == 0) {
            return null;
        }
        $middle_index = floor($iCount / 2);
        sort($array, SORT_NUMERIC);
        $median = $array[$middle_index];
        // assume an odd # of items
        // Handle the even case by averaging the middle 2 items
        if ($iCount % 2 == 0) {
            $median = ($median + $array[$middle_index - 1]) / 2;
        }
        return $median;
    }

    private static function calculateCreditScore($farmer_id)
    {
        $creditScoreObj = new CreditScore();
        $credit_score_array = $creditScoreObj->creditScore($farmer_id);
        return isset($credit_score_array['credit_score']) ? $credit_score_array['credit_score'] : null;
    }

    public function fetchProjectsForPayment($for_investment = false)
    {
        //project ready for payments
//        dd('hello');
        if ($for_investment) {
            $projects = Project::with('district', 'municipality', 'farmers')->where('status', '=', 'funding')->whereNotNull('payment_id');
        } else {
            $projects = Project::with('farmers', 'district', 'municipality')->whereNotNull('emi_start_date')->whereNotNull('emi_amount')->whereNotNull('payment_id');
        }
        //filtering payments logs for project belonging to users under certain partner
        if (session('role') == Role::PARTNER_USER) {
            $projects = $projects->with('user')->whereHas('user', function ($q) {
                $q->where('partner_id', '=', Auth::user()->partner_id);
            });
        }
//        dd($projects->get());
        return $projects->get();
    }

    public function getByPaymentId($payment_id, $related_models = null)
    {
        if (!is_null($related_models)) {
            return Project::with($related_models)->where('payment_id', $payment_id)->first();
        }
//        dd($payment_id);
        return Project::where('payment_id', $payment_id)->first();
    }

    public function getPaginatedProjects($pagination_limit = null)
    {
        $projects = Project::select('*')->orderBy('created_on', 'DESC');
        $role = session('role');
        if ($role != Role::ADMIN) {
            $projects = $this->getQueryForPartnerUser($projects);
        }
        return $projects->get();
    }

    public function getDataSetForProjectStatus($projectStatuses)
    {
        $dataSetArray = [];
        foreach ($projectStatuses as $status) {
            $getCount = Project::where('status', '=', $status);
            $role = session('role');
            if ($role != Role::ADMIN) {
//            return redirect(route('admin.login'));
//            dd('hello');
                $partner_id = Auth::user()->partner_id;
//            dd();
                $getCount = $getCount->whereHas('user', function ($q) use ($partner_id) {
                    $q->where('partner_id', $partner_id);
                });
            }
            /* if($role==Role::DEVELOPER){
                $getCount=$getCount->where('tbl_projects.created_by',Auth::user()->user_id);
            }*/
            $getCount = $getCount->count();
            array_push($dataSetArray, $getCount);
        }
        return $dataSetArray;
    }

    public function getDistricts($province_id)
    {
        $districts = Province::find($province_id)->districts;
        return $districts;
    }

    public function getAllProvinces()
    {
        $allProvinces = Province::all();
        return $allProvinces;
    }

    public function getMunicipalities($district_id)
    {
        $municipalities = District::find($district_id)->municipalities;
        return $municipalities;
    }

    public function getAllProjectTypes()
    {
        $projectType = ProjectType::all();
        return $projectType;
    }

    public function storeCustom(array $data)
    {
        DB::beginTransaction();
        $return_value = null;
        $form = $data['request']->all();
        if (is_array($form) && $form['submit-analysis'] == 'submit-analysis') {
            //Checking the draft related with project and deleting it while creating Project
            $draft_id = $data['request']->id ? $data['request']->id : null;
            if (isset($draft_id)) {
                $draftProject = $this->draftRepository->getById($draft_id);
                $this->draftRepository->delete($draftProject);
            }
            $projectToStore = new Project();
            $form['status'] = ProjectStatus::PLAN;
            $form['created_by'] = Auth::user()->user_id;
            $form['updated_by'] = Auth::user()->user_id;
            $projectToStore = Project::create($form);
            $redirect_route = 'project.selectPlan';
            $project_id = $this->project_model->id;
            $return_value['redirect'] = $redirect_route;
            $return_value['id'] = $project_id;


            if (!$projectToStore) {
                DB::rollback();
                return false;
            }

            $this->farmer_model = $this->farmerRepository->setFarmerAttributes($data, $projectToStore);
            $projectToStore->farmers()->sync($this->farmer_model->id);
            $this->cropInfoRepository->setCropInfo($data, $projectToStore);
            $this->cattleInfoRepository->setCattleInfo($data, $projectToStore);
            $this->farm_model = $this->farmRepository->setFarmAttributes($data, $this->farmer_model);


            $request = $data['request'];
            DB::commit();
            return $projectToStore;
        } elseif ($form['submit-analysis'] == 'submit') {
            //If id present then it is draft which we update.

            if (!$data['request']->id) {
                $return_value = $this->draftRepository->draftStore($data);
            } else {
                $return_value = $this->draftRepository->draftUpdate($data, $data['request']->id);
            }

        }
        DB::commit();
        return $return_value;

    }

    public function filterProject(array $data)
    {
        $searchParam = $data['searchField'];
        $orderBy = $data['orderBy'];

//        $value = Project::withoutGlobalScope(ProjectScope::class)->select('tbl_project.id', 'tbl_project.solar_pv_size', 'tbl_project.payment_id', 'tbl_project.pump_size', 'farmers.contact_no', 'tbl_project_type.name', 'farmers.credit_score', 'tbl_project.status', 'tbl_project.farmer_name', 'tbl_project.delete_flg','tbl_district.name as district')->join('farmer_project', 'tbl_project.id', '=', 'farmer_project.project_id')->
//        join('farmers', 'farmer_project.farmer_id', '=', 'farmers.id')->
//        join('tbl_project_type', 'tbl_project.project_type_id', '=', 'tbl_project_type.id')->
//        join('tbl_district','tbl_project.district_id','=','tbl_district.id');

//        $value = Project::withoutGlobalScope(ProjectScope::class)->select('id', 'solar_pv_size', 'sim_number', 'project_type_id', 'status', 'farmer_name', 'delete_flg','payment_id')->with(['district:id,name', 'getProjectType:id,name']);
//        $value = DB::table('tbl_project')->select('tbl_project.id','tbl_project.solar_pv_size','tbl_project.payment_id', 'tbl_project.pump_size', 'farmers.contact_no', 'tbl_project_type.name','farmers.credit_score' , 'tbl_project.status', 'tbl_project.farmer_name', 'tbl_project.delete_flg','tbl_district.name as district')->
//        join('farmer_project','tbl_project.id','=','farmer_project.project_id')->
//        join('farmers','farmer_project.farmer_id','=','farmers.id')->
//        join('tbl_project_type','tbl_project.project_type_id','=','tbl_project_type.id')->
//        join('tbl_district','tbl_project.district_id','=','tbl_district.id');
        $value = Project::withoutGlobalScope(ProjectScope::class);
        $role = session('role');


        if (isset($data['district'])) {
            $value = $value->orWhere('tbl_project.district_id', $data['district']);
        }
        if (isset($data['province_id'])) {
            $value = $value->orWhere('tbl_project.province_id', $data['province_id']);
        }
        if (isset($data['project_type_id'])) {
            foreach ($data['project_type_id'] as $key => $id) {
                $value = $value->orWhere('tbl_project.project_type_id', $id);
            }
        }

        if (isset($data['pump_size'])) {
            foreach ($data['pump_size'] as $key => $size) {

                $value = $value->orWhere('tbl_project.pump_size', $size);
            }
        }
        if (isset($data['status'])) {
            foreach ($data['status'] as $key => $id) {

                $value = $value->orWhere('status', $id);
            }
        }
        if (isset($data['credit_score'])) {
            foreach ($data['credit_score'] as $key => $score) {
                $value = $value->whereHas('farmers', function ($q) use ($data, $score) {

                    switch ($score) {
                        case $score == 0.2:
                            $q = $q->where(function ($query) {
                                $query->where('credit_score', '>', 0)->where('credit_score', '<=', 0.29);
                            });
                            break;
                        case $score == 0.4:
                            $q = $q->where(function ($query) {
                                $query->where('credit_score', '>', 0.29)->where('credit_score', '<=', 0.49);
                            });
//                                $q->where('credit_score', '>', 0.29)->where('credit_score', '<=', 0.49);
                            break;
                        case $score == 0.6:
                            $q = $q->where(function ($query) {
                                $query->where('credit_score', '>', 0.49)->where('credit_score', '<=', 0.69);
                            });
//                                $q->where('credit_score', '>', 0.49)->where('credit_score', '<=', 0.69);
                            break;
                        case $score == 0.8:
                            $q = $q->where('credit_score', '>', 0.69)->where('credit_score', '<=', 0.89);
                            break;
                        case $score == 1:
                            $q = $q->where('credit_score', '>', 0.89)->where('credit_score', '<=', 1);
                            break;
                    }


                    return $q;
                });
            }

        }
        $value = $value->when($searchParam, function ($query) use ($searchParam) {
            $query->where(function ($query) use ($searchParam) {
                $query->where('tbl_project.farmer_name', 'LIKE', '%' . $searchParam . '%')
                    ->orWhere('tbl_project.sim_number', 'LIKE', '%' . $searchParam . '%');
            });
        });
        $value = $value->when($orderBy, function ($query) use ($orderBy) {
            switch ($orderBy) {
                case 1:
                    return $query->orderBy('tbl_project.farmer_name', 'ASC');
                case 2:
                    return $query->orderBy('tbl_project.farmer_name', 'DESC');
                case 3:
                    return $query->orderBy('tbl_project.created_on', 'DESC');
                case 4:
                    return $query->orderBy('tbl_project.created_on', 'ASC');
                default:
                    return $query->orderBy('tbl_project.created_on', 'ASC');
            }
        });
        if (!$orderBy) {
            $value = $value->orderBy('tbl_project.created_on', 'DESC');
        }
        if ($role != Role::ADMIN) {
//            return redirect(route('admin.login'));
//            dd('hello');
//            $partner_id = Auth::user()->partner_id;
////            dd();
//            $value = $value->whereHas('user',function($q) use ($partner_id){
//                $q->where('partner_id', $partner_id);
//            });
//            dd($value->get());
            $value = $this->getQueryForPartnerUser($value);
        }
        /* if($role==Role::DEVELOPER){
            $value=$value->where('tbl_projects.created_by',Auth::user()->user_id);
        }*/
        $value = $value->paginate(10);
        return ProjectIndexResource::collection($value);
    }

    public function getProjectData($id)
    {
        $projectInformation = Project::withoutGlobalScope(ProjectScope::class)->find($id);
        return new ResourceForProject($projectInformation);
//        dd($dataToReturn);
    }

    public function updateProjectInformation(array $data, $id)
    {
        $projectToStore = Project::withoutGlobalScope(ProjectScope::class)->find($id);
//        dd($projectToStore->farmers);
        $tofill = $data['request']->except(['created_by', 'status', 'id']);
        $projectToStore->fill($tofill);
        $projectToStore->save();
        $this->farmer_model = $projectToStore->farmers()->first();
        $request = $data['request'];

        if ($request->hasFile('file_name')) {
//            dd($request->hasFile('file_name'));
            $this->uploadProfileImage($request->file('file_name'), $projectToStore->id);
        }
//        dd('this shoul not load');
        $this->farmerRepository->editFarmerInformation($data, $projectToStore, $this->farmer_model);
    }

    public function changeStatus($project)
    {
//        $result = DB::table('tbl_project')->where('id',$project)->update(['delete_flg' => $flg]);
        $projectToUpdate = Project::withoutGlobalScope(ProjectScope::class)->find($project);
//        dd($result);
        $projectToUpdate->delete_flg = !$projectToUpdate->delete_flg;
        $projectToUpdate->save();
        return true;
    }

    public function changeProjectStatus($status, $id)
    {
        try {
            $statusToStore = Status::where('name', $status)->first();
            DB::beginTransaction();
            $logToStore = new ProjectLog();
            $logToStore->project_id = $id;
            $logToStore->status_id = $statusToStore->id;
            $logToStore->user_id = isset(Auth::user()->user_id) ? Auth::user()->user_id : null;
            $logToStore->description = $this->returnLogs($status);
            $logToStore->save();
            Project::where('id', $id)->update(['status' => $status]);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function uploadFile(array $data, $id)
    {
        try {
            DB::beginTransaction();
            $request = $data['request'];

            $project = Project::withoutGlobalScope(ProjectScope::class)->find($id);

            if ($request->hasFile('file_name')) {

                $files = $request->file('file_name');

                foreach ($files as $file) {
                    $size = $file->getSize();
                    $size = number_format($size / 1048576, 2);
                    $filename = $file->getClientOriginalName() ?: null;
                    $filename_extension = $file->getClientOriginalExtension() ?: null;
                    $file_name = rand(1, 99999) . strtotime("now") . '_' . $filename;
                    $file->move($this->folder_path, $file_name);
                    $document = Document::create([
                        'project_id' => $id,
                        'file_name' => $file_name,
                        'actual_file_name' => $filename,
                        'file_type' => $filename_extension,
                        'path' => $this->folder_path,
                        'size' => $size,
                        'created_by' => Auth::user()->user_id
                    ]);
                    $category = Category::find($request->category_id);
                    $guideline = Guideline::where('attribute_key', $category->attribute_key)->first();
                    DB::table('category_document')->insert(['category_id' => $request->category_id, 'document_id' => $document->id]);
                    if ($guideline != '') {
                        $this->storeStatusTimeline($project, $guideline);
                    }
                }
//                dd('hello');
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
//            dd($e);
            DB::rollback();
//            throw $e;
            return back()->with('error', 'Category not selected');
        }
    }

    public function getFarmerImage($id)
    {
        $upload = Document::
        join('category_document', 'documents.id', '=', 'category_document.document_id')
            ->where('documents.project_id', $id)->where('category_document.category_id', '=', 1)->orderBy('documents.id', 'DESC')->first();
        return $upload;
    }

    public function deleteDocument($id)
    {
        $document_category = DB::table('category_document')->where('document_id', $id);
        if (!$document_category) {
            return redirect()->back()->with('info', "Document not found");
        } else {
            $document_category->delete();
            $document = Document::find($id);
            $document->delete();
            return $document;
        }
    }

    public function getAllProjectData($pagination_limit = null)
    {
//        $projects = DB::table('tbl_project')->select('tbl_project.id', 'tbl_project.solar_pv_size', 'tbl_project.payment_id', 'tbl_project.pump_size', 'farmers.contact_no', 'tbl_project_type.name', 'farmers.credit_score', 'tbl_project.status', 'tbl_project.farmer_name', 'tbl_project.delete_flg','tbl_district.name as district')->
//        join('farmer_project', 'tbl_project.id', '=', 'farmer_project.project_id')->
//        join('farmers', 'farmer_project.farmer_id', '=', 'farmers.id')->
//        join('tbl_project_type', 'tbl_project.project_type_id', '=', 'tbl_project_type.id')->
//        join('tbl_district','tbl_project.district_id','=','tbl_district.id')->with()
//            ->orderBy('created_on', 'DESC')->
////            join('districts','farmer_project.farmer_id','=','farmers.id')->
//            paginate($pagination_limit);
////        dd(Project::withoutGlobalScope(ProjectScope::class));
//        return $projects->toArray();

        $projects = Project::withoutGlobalScope(ProjectScope::class)->orderBy('created_on', 'DESC');
        $role = session('role');
        if ($role != Role::ADMIN) {
//            return redirect(route('admin.login'));
//            dd('hello');
//            $partner_id = Auth::user()->partner_id;
////            dd();
//           $projects = $projects->whereHas('user',function($q) use ($partner_id){
//                $q->where('partner_id', $partner_id);
//            });
            $projects = $this->getQueryForPartnerUser($projects);
        }
//        if($role==Role::DEVELOPER){
//            $projects=$projects->where('tbl_project.created_by',Auth::user()->user_id);
//        }

//        join('farmer_project', 'tbl_project.id', '=', 'farmer_project.project_id')->
//        join('farmers', 'farmer_project.farmer_id', '=', 'farmers.id')->
//        select('tbl_project.id', 'tbl_project.solar_pv_size', 'tbl_project.payment_id', 'tbl_project.pump_size', 'farmers.contact_no', 'tbl_project_type.name', 'farmers.credit_score', 'tbl_project.status', 'tbl_project.farmer_name', 'tbl_project.delete_flg','tbl_district.name as district')->
//        join('tbl_project_type', 'tbl_project.project_type_id', '=', 'tbl_project_type.id')->
//        join('tbl_district','tbl_project.district_id','=','tbl_district.id')
//            ->orderBy('created_on', 'DESC')->
//            join('districts','farmer_project.farmer_id','=','farmers.id')->
        $projects = $projects->paginate($pagination_limit);
//        dd(ProjectIndexResource::collection($projects));
//        dd(Project::withoutGlobalScope(ProjectScope::class));
        return ProjectIndexResource::collection($projects);
    }

    public function returnLogs($status)
    {
        switch ($status) {
            case 'plan':
                return 'Project saved with Farmer\'s information by an agent "' . (Auth::user()->full_name == 'root'? 'Gham Power' : Auth::user()->full_name) . '"';
            case 'new':
                return 'New Project saved by an agent "' . (Auth::user()->full_name == 'root'? 'Gham Power' : Auth::user()->full_name) . '"';
            case 'approved':
                return Auth::user()->full_name . '(Admin) change the project status to "Approved"';
            case 'funding':
                return Auth::user()->full_name . '(Admin) change the project status to "Funding"';
            case 'funded':
                return 'Project status changed to “Funded”';
            case 'installed':
                return Auth::user()->full_name . '(Admin) change the project status to "Installed"';
            case 'operational':
                return Auth::user()->full_name . '(Admin) change the project status to "Installed"';
            case 'error':
                return '"Error No data received from the meter"';
            default:
                return 'Nan';
        }
    }

    public function getProjectLogs($projectId)
    {
        try {
            $project = Project::withoutGlobalScope(ProjectScope::class)->findorfail($projectId);
            $logs = $project->projectLogs()->orderBy('created_at', 'ASC')->get();
            return $logs;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function storeStatusTimeline(Project $project, Guideline $guideline)
    {
        try {
            $delete_flag = true;
            $projectTimeline = ProjectTimeline::where('project_id', $project->id)->where('status_guidelines_id', $guideline->id)->first();

            if ($projectTimeline == '') {
                $projectTimeline = new ProjectTimeline();
                $delete_flag = false;
            } else {
                $delete_flag = !$projectTimeline->delete_flg;
            }
            $projectTimeline->project_id = $project->id;
            $projectTimeline->status_guidelines_id = $guideline->id;
            $projectTimeline->status_id = $guideline->status_id;
            $projectTimeline->created_by = Auth::id();
            $projectTimeline->delete_flg = $delete_flag;
            $projectTimeline->save();
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getUsersInvestment(Project $project)
    {
        $userInvestment = $project->projectinvestments()->orWhere('developer_id', Auth::id())->orWhere('investor_id', Auth::id())->first();
        $projectsInvestment = $project->projectinvestments()->with('user')->get()->toArray();
//        dd(collect($projectsInvestment)->sum('investment_percent'));

        $investment = new \stdClass();
        $investment->userInvestment = $userInvestment ? $userInvestment->toArray() : null;
        $investment->investment_percent = $this->getInvestmentPercentage($projectsInvestment);
        $investment->index_investment = $projectsInvestment;
        return $investment;
    }

    public function getInvestmentPercentage($projectInvestment)
    {
        $investment_percentage = round(collect($projectInvestment)->sum('investment_percent'), 2);
        return $investment_percentage;
    }

    public function uploadProfileImage($file, $id)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($id);
        $category = Category::where('attribute_key', 'farmer_photo')->first();
        $size = $file->getSize();
        $size = number_format($size / 1048576, 2);
        $filename = $file->getClientOriginalName() ?: null;
        $filename_extension = $file->getClientOriginalExtension() ?: null;
        $file_name = rand(1, 99999) . strtotime("now") . '_' . $filename;
        $file->move($this->folder_path, $file_name);
        $document = Document::create([
            'project_id' => $id,
            'file_name' => $file_name,
            'actual_file_name' => $filename,
            'file_type' => $filename_extension,
            'path' => $this->folder_path,
            'size' => $size,
            'created_by' => Auth::user()->user_id
        ]);
        $category = Category::find($category->id);
        $guideline = Guideline::where('attribute_key', $category->attribute_key)->first();
        DB::table('category_document')->insert(['category_id' => $category->id, 'document_id' => $document->id]);
        if ($guideline != '') {
            $this->storeStatusTimeline($project, $guideline);
        }
    }

    public function getDistrict($project)
    {
        return $project->province->districts;
    }

    public function getMunicipality($project)
    {
        return $project->district->municipalities;
    }

    public function getFarmers($project)
    {
        return $project->farmers;
    }

    public function updatePackage(array $data, $id)
    {

        $package = Project::where('id', $id)
            ->update(['name' => $data['name'],
                'pump_size' => $data['pump_size'],
                'solar_pv_size' => $data['solar_pv_size'],
                'daily_discharge' => $data['daily_discharge'],
                'cost' => $data['cost'],
                'pump_brand' => $data['pump_brand'],
                'controller' => $data['controller'],
                'total_emi' => $data['total_emi'],
                'advance_amount' => $data['advance_amount'],
                'emi_amount' => $data['emi_amount'],
                'emi_count' => $data['emi_count']
            ]);
        return $package;
    }

    public function getQueryForPartnerUser($query)
    {
        $role = session('role');
        switch ($role) {
            case Role::PARTNER_USER:
                $partner_id = Auth::user()->partner_id;
//            dd();
                $query = $query->whereHas('user', function ($q) use ($partner_id) {
                    $q->where('partner_id', $partner_id);
                });
                break;
            case Role::DEVELOPER:
                $query = $query->where('tbl_project.created_by', Auth::user()->user_id);
        }
        return $query;
    }

    public function getDefaultRate($project, array $defaultedArray, $emi_count)
    {
        $averageDefaultRate = (count($defaultedArray) / $emi_count) * 100;
        return round($averageDefaultRate, 2);
    }

    public function getDefaultDays($project, array $defaultedArray, $emi_count)
    {
        $uniqueDefaultArraySum = collect($defaultedArray)->unique()->values()->sum();
        $averageDefaultDays = ($uniqueDefaultArraySum / count($defaultedArray));
        return round($averageDefaultDays, 0);
    }

    public function changeProfilePicture(array $data, $id)
    {
        $file = $data['request']->file;
        $file_extension = $file->getClientOriginalExtension();
        $size = $file->getSize();
        $size = number_format($size / 1048576, 2);
        $original_file_name = $file->getClientOriginalName() ?: null;
        $original_file_name_extension = $file->getClientOriginalExtension() ?: null;
        $file_name = rand(1, 99999) . strtotime("now") . '_' . $original_file_name;
        $file->move($this->folder_path, $file_name);
        $docs = DB::table('documents')
            ->join('category_document', 'documents.id', '=', 'category_document.document_id')
            ->where('documents.project_id', $id)
            ->where('category_document.category_id', 1)->get();
        if ($docs->isNotEmpty()) {
            $document = DB::table('documents')
                ->join('category_document', 'documents.id', '=', 'category_document.document_id')
                ->where('documents.project_id', $id)
                ->where('category_document.category_id', 1)
                ->update(['file_name' => $file_name,
                    'actual_file_name' => $original_file_name,
                    'file_type' => $original_file_name_extension,
                    'path' => $this->folder_path,
                    'size' => $size]);
        } else {
            $document = Document::create([
                'project_id' => $id,
                'file_name' => $file_name,
                'actual_file_name' => $original_file_name,
                'file_type' => $original_file_name_extension,
                'path' => $this->folder_path,
                'size' => $size,
                'created_by' => Auth::user()->user_id
            ]);
            DB::table('category_document')->insert(['category_id' => 1, 'document_id' => $document->id]);
        }
        return $file_name;
    }
}
