<?php

namespace App\Repositories\Project;

interface ProjectStateUpdateRepository
{
    public function updateTransitionStateOfProject($project_id ,$status);
    public function stateUpdate($request,$project_id);
    public function updateProjectEmiState($project);
    public function updateProjectCollectedAdvanceState($project);
}