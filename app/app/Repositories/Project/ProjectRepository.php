<?php

namespace App\Repositories\Project;

use App\Models\Guideline;
use App\Models\Project;

interface ProjectRepository
{
    public function getById($id);
    public function getByPaymentId($payment_id, $related_models = null);
    public function getPaginatedProjects($pagination_limit=null);
    public function getAll();
    public function store(array $data);
    public function update(array $data,$project);
     public function updateProjectData(array $data,$project);
    public function selectPlanStore(array $data);
    public function fetchWaterRequirement($id);
    public function recommendPlanBasedOnWaterRequirement($daily_requirement_according_to_month,$plans);
    public function fetchProjectsForPayment($investments = false);
    public function getDataSetForProjectStatus($projectStatuses);
    public function getDistricts($province_id);
    public function getAllProvinces();
    public function getMunicipalities($district_id);
    public function getAllProjectTypes();
    public function storeCustom(array $data);
    public function filterProject(array $data);
    public function getProjectData($id);
    public function updateProjectInformation(array $data, $id);
    public function changeStatus($project);
    public function changeProjectStatus($status,$id);
    public function uploadFile(array $data,$id);
    public function getFarmerImage($id);
    public function getAllProjectData($pagination_limit=null);
    public function deleteDocument($id);
    public function returnLogs($status);
    public function getProjectLogs($projectId);
    public function storeStatusTimeline(Project $project,Guideline $guideline);
    public function getUsersInvestment(Project $project);
    public function getInvestmentPercentage($projectInvestment);
    public function getDistrict($project);
    public function getMunicipality($project);
    public function getFarmers($project);
    public function updatePackage(array $data, $id);
    public function getDefaultRate($project, array $defaultedArray,$emi_count);
    public function getDefaultDays($project, array $defaultedArray,$emi_count);
    public function changeProfilePicture(array $data,$id);
}
