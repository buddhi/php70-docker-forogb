<?php

namespace App\Repositories\Project;

use App\Enums\ProjectStatus;
use App\Models\Project;
use App\Models\TransitionState;
use App\OGB\Constants;
use App\Services\ProjectStateUpdateTrigger;

class ProjectStateUpdate implements ProjectStateUpdateRepository
{
    public $projectStateUpdateTrigger;

    public function __construct(ProjectStateUpdateTrigger $projectStateUpdateTrigger)
    {
        $this->projectStateUpdateTrigger = $projectStateUpdateTrigger;
    }

    public function stateUpdate($request, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $next_status = $project->getNextStatus($project->status);

        //All transition boxes must be checked
        if (!$this->isAllNextStatusChecked($request, $project,$next_status)) {
            return ['route' => 'admin.project.list', 'message_type' => 'success_message', 'message' => 'Please,make sure all the checkboxes are checked'];
        }

        //if all transition state boxes are checked.
        //Then we can update the transition states
        $this->updateTransitionStateOfProject($project->id, $next_status);

        //Special case: if meter absent then redirect to meter page before status update
        if ($next_status == ProjectStatus::OPERATIONAL && is_null($project->meter_id)) {
            return ['route' => 'admin.meter.create', 'message_type' => 'success_message', 'message' => 'Please,make sure that project has meter and make operational from admin panel'];
        }

        //Special case: for approve state emi and advance amount has to be defined for status update
        if ($next_status == ProjectStatus::APPROVED) {
            if (is_null($project->payment_id)) {
                $project->payment_id = $project->generatePaymentId();
                $project->save();
            }
            return ['route' => 'admin.project-plan.edit', 'message_type' => 'success_message', 'message' => 'Please,make sure that project has EMI details for approving project'];

        }

        $project->status = $next_status;
        $project->save();
        $this->projectStateUpdateTrigger->sendSmsEmailOnStateChange($project, $next_status);
        return ['route' => 'admin.project.list', 'message_type' => 'success_message', 'message' => "Project status for project number {$project->id} has been updated to {$project->status} state"];
    }

    public function updateProjectEmiState($project)
    {
        $first_emi_obj = TransitionState::where('title', 'first_emi_collected')->first();
        if (!$first_emi_obj) {
            return;
        }
        $sync_data = syncData(['id' => $first_emi_obj->id], Constants::ACTIVE);
        $project->transitionstate()->sync($sync_data, false);
    }

    private function isAllNextStatusChecked($request, $project,$next_status)
    {
        $input_form_array = $request->except(['_token', 'reject-update']);
        $new_transition_states = array_keys($input_form_array);
        $total_transition_states = getTransitionStates($next_status, 'id');
        $all_status_checked = !array_diff($new_transition_states,$total_transition_states) && !array_diff($total_transition_states,$new_transition_states);
        if ($all_status_checked) {
            return true;
        }
        return false;
    }

    public function updateTransitionStateOfProject($project_id, $status)
    {
        $project = Project::findOrFail($project_id);
        $transition_state_id = getTransitionStates($status, 'id');
        if (empty($project)) {
            return;
        }
        $sync_data = syncData($transition_state_id, Constants::ACTIVE);
        $project->transitionstate()->sync($sync_data, false);
    }


    //Automatic update of collected advance
    public function updateProjectCollectedAdvanceState($project)
    {
        $advance_collected_state = TransitionState::where('title','collected_advance_and_emi_for_first_month')->first();
        if(!$advance_collected_state){
            return;
        }
        $sync_data = syncData(['id'=> $advance_collected_state->id],Constants::ACTIVE);
        $project->transitionstate()->sync($sync_data, false);
    }
}