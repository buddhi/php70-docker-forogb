<?php

namespace App\Repositories\UserRegister;

use App\Enums\Role;
use App\Jobs\FireEmail;
use App\Mail\Ogbadmin\VerificationEmailToAgent;
use App\Mail\VerificationEmailForRegisterUser;
use App\Models\Token;
use App\Models\UserRegister;
use App\OGB\Constants;
use App\User;
use App\Utilities\EmailParam;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Exception;
use Webpatser\Uuid\Uuid;

class UserRegisterService implements UserRegisterRepository
{

    public function getById($user_register_id)
    {
        try {
            $userRegister = UserRegister::find($user_register_id);
        } catch (ModelNotFoundException $exception) {
            return null;
        }
        return $userRegister;
    }

    public function registerUserTemporarily($data)
    {
        $userRegister = UserRegister::create([
            'email' => $data['email'],
            'partner_id' => isset($data['partner_id']) ? $data['partner_id'] : null,
            'role' => isset($data['role']) ? $data['role'] : null,
        ]);
        if (!$userRegister) {
            return false;
        }
        return $userRegister;
    }

    public function sendUserVerificationEmail($userRegister, $token, $partner)
    {
        try {
            $subject = __('app.subject_for_email_verification_nepali') . config('messages.subject_for_email_verification');
            $title = "";
            $email_param = new EmailParam(
                $from = env('MAIL_USERNAME', null),
                $to = $userRegister->email,
                null,
                $subject,
                $title
            );
            $param_for_email = ['email' => $userRegister->email, 'token' => $token->token, 'partner' => $partner];

            dispatch(new FireEmail(VerificationEmailForRegisterUser::class, $email_param, $param_for_email));

            return;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function verifyEmail($token)
    {
        if (is_null($token)) {
            return false;
        }

        //Matching the token in emails with that in tokens table
        $token = Token::where('token', $token)->first();

        //If no valid token then email cannot verified
        if (!$token) {
            return false;
        }
        //Accessing the temp-user with corresponding token
        $userRegister = $token->tokenable;

        //checking the models and validating the valid email address
        if ($userRegister instanceof UserRegister) {
            $userRegister->verified = Constants::ACTIVE;
            $userRegister->save();
            return $userRegister;
        }
        return false;
    }

    public function sendAgentVerificationEmail($userRegister, $token, $partner)
    {
        $subject = __('app.subject_for_email_verification_nepali') . config('messages.subject_for_email_verification');
        $title = "";
        $email_param = new EmailParam(
            // $from = env('MAIL_USERNAME', null),
            $from = env('MAIL_FROM', null),
            $to = $userRegister->email,
            null,
            $subject,
            $title
        );
        $param_for_email = ['email' => $userRegister->email, 'token' => $token->token, 'partner' => $partner];
        dispatch(new FireEmail(VerificationEmailToAgent::class, $email_param, $param_for_email));
        return;
    }

    public function getPendingUsersWithRole($role, $pagination_limit = null, $partner_id = null)
    {

        $user_registers = UserRegister::where('role', $role);
        $user_role = session('role');
        if ($user_role != Role::ADMIN) {
            $id = Auth::user()->partner_id;
            $user_registers->where('partner_id', $id);
        }

        if (isset($partner_id)) {
            $user_registers->where('partner_id', $partner_id);
        }
        if (isset($pagination_limit)) {
            return $user_registers->paginate($pagination_limit);
        }
        return $user_registers->get();
    }

    public function delete($user_register_id)
    {
        $user_register = $this->getById($user_register_id);
        if ($user_register) {
            $user_register->delete();
        }
    }
}