<?php


namespace App\Repositories\UserRegister;


interface UserRegisterRepository
{
    public function getById($user_register_id);

    public function registerUserTemporarily($data);

    public function sendUserVerificationEmail($userRegister,$token,$partner);

    public function verifyEmail($token);

    public function sendAgentVerificationEmail($userRegister, $token,$partner);

    public function getPendingUsersWithRole($role , $pagination_limit = null,$partner_id = null);

    public function delete($user_register_id);
}