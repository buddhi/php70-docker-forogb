<?php


namespace App\Repositories\Otp;


interface OtpRepository
{
    public function generateOtp($userRegister);
    public function invalidateOtp($email);
    public function verifyOtp($user_id,$token);
    public function updateOtpStatus($otp);
    public function timeBeforeOtpExpire($otp);
}