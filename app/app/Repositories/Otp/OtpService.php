<?php


namespace App\Repositories\Otp;

use App\Models\Otp;
use App\OGB\Constants;
use App\User;
use Carbon\Carbon;

class OtpService implements OtpRepository
{

    private static $NO_OF_DIGITS_OTP = 6;
    private static $OTP_LIFE_SPAN = 15; //(in minutes)
    public function generateOtp($user)
    {
        $otp = Otp::create([
            'otp' => rand(pow(10, self::$NO_OF_DIGITS_OTP - 1), pow(10, self::$NO_OF_DIGITS_OTP) - 1),
            'status' => Constants::ACTIVE,
            'expires_on' => Carbon::now()->addMinutes(self::$OTP_LIFE_SPAN),
            'user_id' => $user->user_id
        ]);
        return $otp;
    }


    public function invalidateOtp($email)
    {
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->otps()->update(['status' => 0]);
        }

    }

    public function verifyOtp($user_id,$token)
    {
        $validOtp = Otp::where('user_id', $user_id)
            ->where('status', Constants::ACTIVE)
            ->whereDate('expires_on', '<=', Carbon::now()->toDateTimeString())
            ->where('validated_at', null)
            ->first();
        if (!$validOtp) {
            return false;
        }
        if ($validOtp->otp == $token) {
            return $validOtp;
        }
        return false;
    }

    public function updateOtpStatus($otp)
    {
        $otp->status = Constants::OFF;
        $otp->validated_at = Carbon::now();
        $otp->save();
        return $otp;
    }

    public function timeBeforeOtpExpire($otp)
    {
        $expire_time = 0;
        if(!$otp){
            return $expire_time;
        }
        if(Carbon::now() < Carbon::parse($otp->expires_on) && $otp->status){
            $time_lost_in_loading_page = 1; //arbitary constant; taking in account of page load time in seconds;
            $expire_time = Carbon::now()->diffInSeconds(Carbon::parse($otp->expires_on))- $time_lost_in_loading_page;
        }
        return $expire_time;
    }
}