<?php

namespace App\Repositories\RoleVerification;

use Illuminate\Support\Facades\Auth;

final class DefaultRoleVerification extends RoleVerification
{

    function verify($role)
    {
        $email_verification =Auth::user()->verified;
        if((!isset($email_verification)) ||(!$email_verification)){
            return ['isVerified' => false ,'message' => config('messages.email_not_verified_message') . Auth::user()->email];

        }
        return ['isVerified' => true ,'message' => 'Admin role is verified'];

    }
}