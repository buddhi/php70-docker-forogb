<?php

namespace App\Repositories\RoleVerification;


abstract class RoleVerification
{
    abstract function verify($role);
}