<?php

namespace App\Repositories\RoleVerification;
use App\Enums\Role;

class RoleVerifier
{
    protected $roleVerifier,$role;

    public function __construct($role)
    {
        $this->role = $role;
    }

    public function giveVerificationRole()
    {
       switch($this->role){
           case Role::ADMIN:
               return new AdminRoleVerification();
           case Role::PARTNER_USER:
               return new PartnerUserRoleVerification();
           default:
               return new DefaultRoleVerification();

       }
    }
}