<?php

namespace App\Repositories\RoleVerification;

final class AdminRoleVerification extends RoleVerification
{

    public function verify($role)
    {
        return ['isVerified' => true];
    }
}