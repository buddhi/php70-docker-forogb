<?php


namespace App\Repositories\RoleVerification;


class RoleBasedVerificationModule
{
    public static function checkUserRoleVerification($role)
    {
        $roleBasedVerifier = new RoleVerifier($role);
        $verificationRole = $roleBasedVerifier->giveVerificationRole();
        return $verificationRole->verify($role);
    }
}