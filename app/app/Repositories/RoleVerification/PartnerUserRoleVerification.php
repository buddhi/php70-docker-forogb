<?php


namespace App\Repositories\RoleVerification;

use App\OGB\Constants;
use Illuminate\Support\Facades\Auth;

final class PartnerUserRoleVerification
{

    public function verify($role)
    {
        $user = Auth::user();
        $email_verification = $user->verified;

        //checking of nullable fields as well
        if((!isset($email_verification)) ||(!$email_verification)){
            return ['isVerified' => false, 'message' => config('messages.email_not_verified_message') . Auth::user()->email ];
        }
        $partner = $user->partner;
        if ((!isset($partner)) || $partner->delete_flg == Constants::DELETE) {
            return ['isVerified' => false, 'message' => 'Please,contact with the ghampower team', 'redirect_to' => 'contact'];
        }
        return ['isVerified' => true];
    }

}