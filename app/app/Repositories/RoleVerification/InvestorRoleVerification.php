<?php

namespace App\Repositories\RoleVerification;

use Illuminate\Support\Facades\Auth;

final class InvestorRoleVerification extends RoleVerification
{

    function verify($role)
    {
        $email_verification =Auth::user()->verified;

        if((!isset($email_verification)) ||(!$email_verification)){
           return false;
        }
        return true;
    }
}