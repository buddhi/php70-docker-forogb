<?php

namespace App\Repositories\Transaction\Esewa;

use App\Models\EsewaPayment;
use App\Models\FarmerProject;
use App\Models\Loan;
use App\Models\Meter;
use App\Models\Project;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EsewaTransaction
{
    public function storeTransaction($esewa_payment,$transactionParameters,$paymentGateway)
    {
        DB::beginTransaction();
        try{
            $transaction = Transaction::create([
                'transaction_id' => $transactionParameters['transaction_id'],
                'amount' => $transactionParameters['amount'],
                'currency' => $transactionParameters['currency'],
                'transaction_type' => $transactionParameters['transaction_type'],
                'project_id' => $transactionParameters['project_id'],
                'payment_gateway' => $paymentGateway,
                'status' => $transactionParameters['status'],
                'response_data' => $transactionParameters['response_data'],
                'created_at' =>Carbon::now(),
                'investment_id' => null,
            ]);

            if(isset($transaction) && isset($esewa_payment)){
                $esewa_payment->transactional()->save($transaction);
            }
            DB::commit();
            return $transaction;
        }catch (\Exception $ex){
            DB::rollback();
            return false;
        }

    }
}