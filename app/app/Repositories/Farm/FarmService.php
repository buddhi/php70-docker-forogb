<?php


namespace App\Repositories\Farm;

use App\Models\Farm;
use App\Models\Farmer;

class FarmService implements  FarmRepository
{
    private $farm_model;
    public function __construct(Farm $farm_model)
    {
        $this->farm_model = $farm_model;

    }
    public function getLandDataInArrayFormat($data)
    {
        $land_details=[];
        $land_type_array = $data['request']->get('land_type');
        if(isset($land_type_array) && count($land_type_array)> 0){
            foreach($land_type_array as $key => $land_type){
                $land_details[$key]=[
                    'land_type' => $land_type,
                    'unit_type' => $data['request']->get('unit_type')[$key],
                    'ownership' => $data['request']->get('ownership')[$key],
                    'owner' => $data['request']->get('owner')[$key],
                    'area' => $data['request']->get('land_area')[$key],
                ];
            }
        }
        return $land_details;
    }

    public function setFarmAttributes($data,$farmer)
    {
        $this->farm_model->land_size = null;
        $this->farm_model->farmer_id = $farmer->id;
        $this->farm_model->distance = ($data['request']->get('distance'))?$data['request']->get('distance'):0;
        $this->farm_model->current_irrigation_source = $data['request']->get('current_irrigation_source');
        $this->farm_model->current_irrigation_system = $data['request']->get('current_irrigation_system');
        $this->farm_model->boring_size = ($data['request']->get('boring_size'))?$data['request']->get('boring_size'):0;
        $this->farm_model->vertical_head = ($data['request']->get('vertical_head'))?$data['request']->get('vertical_head'):0;
        $land = $this->setLandData($data);
        $this->farm_model->land = json_encode($land);
        $this->farm_model->water_requirement= ($data['request']->get('water_requirement'))?$data['request']->get('water_requirement'):0;
        $this->farm_model->boring_depth= ($data['request']->get('boring_depth'))?$data['request']->get('boring_depth'):0;
        $this->farm_model->ground_water_level= ($data['request']->get('ground_water_level'))?$data['request']->get('ground_water_level'):0;
        $this->farm_model->type_of_soil=($data['request']->get('type_of_soil'))?$data['request']->get('type_of_soil'):0;
        $this->farm_model->save();
        return $this->farm_model;
    }

    public function updateFarmAttributes($data,$farm,$project)
    {
        $this->farm_model=Farm::find($farm->id);
        $this->farm_model->land_size = null;
        $this->farm_model->farmer_id = $farm->farmer_id;
        $this->farm_model->distance = ($data['request']->get('distance'))?$data['request']->get('distance'):0;
        $this->farm_model->current_irrigation_source = $data['request']->get('current_irrigation_source');
        $this->farm_model->current_irrigation_system = $data['request']->get('current_irrigation_system');
        $this->farm_model->boring_size = ($data['request']->get('boring_size'))?$data['request']->get('boring_size'):0;
        $this->farm_model->vertical_head = ($data['request']->get('vertical_head'))?$data['request']->get('vertical_head'):0;
        $land = $this->setLandData($data);
        $this->farm_model->land = json_encode($land);
        $this->farm_model->water_requirement= ($data['request']->get('water_requirement'))?$data['request']->get('water_requirement'):0;
        $this->farm_model->boring_depth= ($data['request']->get('boring_depth'))?$data['request']->get('boring_depth'):0;
        $this->farm_model->ground_water_level= ($data['request']->get('ground_water_level'))?$data['request']->get('ground_water_level'):0;
        $this->farm_model->type_of_soil=($data['request']->get('type_of_soil'))?$data['request']->get('type_of_soil'):0;
        $this->farm_model->save();
        return $this->farm_model;
    }

    public function setLandData($data)
    {
        $land_details=[];
        $land_type_array = $data['request']->get('land_type');
        if(isset($land_type_array) && count($land_type_array)> 0){
            foreach($land_type_array as $key => $land_type){
                $land_details[$key]=[
                    'land_type' => $land_type,
                    'unit_type' => $data['request']->get('unit_type')[$key],
                    'ownership' => $data['request']->get('ownership')[$key],
                    'owner' => ($data['request']->get('owner')[$key])?$data['request']->get('owner')[$key]:0,
                    'area' => $data['request']->get('land_area')[$key],
                ];
            }
        }
        return $land_details;
    }

    public function getLandArrayFromFarm($farm)
    {
        return isset($farm->land)? json_decode($farm->land,true):null;
    }
}