<?php


namespace App\Repositories\Farm;


interface FarmRepository
{
    public function setFarmAttributes($data,$farmer);
    public function updateFarmAttributes($data,$farm,$project);
    public function getLandDataInArrayFormat($data);
    public function getLandArrayFromFarm($farm);
}