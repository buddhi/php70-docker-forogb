<?php

namespace App\Repositories\ApiPayment\Esewa;

interface EsewaPaymentRepository
{
    public function paymentInquiry($requestId);
    public function payment($requestId, $amount,$transactionCode);
}