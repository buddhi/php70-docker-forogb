<?php

namespace App\Repositories\ApiPayment\Esewa;

use App\Enums\PaymentType;
use App\Enums\ProjectStatus;
use App\Facade\DateConverter;
use App\Jobs\FireEmail;
use App\Mail\AdvancePaymentEmail;
use App\Mail\EmiPaymentEmail;
use App\Models\Payment;
use App\Models\Project;
use App\Repositories\Logger\EsewaLoggerService;
use App\Repositories\Project\ProjectStateUpdateRepository;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\TransactionModule\TransactionModuleRepository;
use App\Services\OperationalCycleTrigger;
use App\User;
use App\Utilities\EmailParam;
use App\Utilities\StringManipulator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

abstract class PaymentProcessors
{
    const ESEWA = "esewa";
}

class EsewaPaymentService implements EsewaPaymentRepository
{
    public $transactionModuleRepository,$smsRepository,$projectStateUpdateRepository;
    public $paymentGateway;
    public $esewaPaymentLogger;
    public $requestId, $amount, $transactionCode,$operationalCycleTrigger;

    public function __construct(TransactionModuleRepository $transactionModuleRepository, EsewaLoggerService $esewaPaymentLogger,SmsRepository $smsRepository,ProjectStateUpdateRepository $projectStateUpdateRepository,OperationalCycleTrigger $operationalCycleTrigger)
    {
        $this->paymentGateway = "esewa";
        $this->esewaPaymentLogger = $esewaPaymentLogger;
        $this->transactionModuleRepository = $transactionModuleRepository;
        $this->smsRepository = $smsRepository;
        $this->projectStateUpdateRepository = $projectStateUpdateRepository;
        $this->operationalCycleTrigger = $operationalCycleTrigger;
    }

    public function paymentInquiry($requestId)
    {
        //Logs every payment inquiry attempts using esewa service.
        $this->esewaPaymentLogger->paymentInquiry($requestId);

        //Getting the parameters necessary for payment
        $parameters = $this->resolveInquiryParameters($requestId);

        //Payment parameters still not defined
        if ($parameters == false) {
            return $this->errorResponse($requestId, $message = null);
        }
        //Getting payment inquiry parameters
        $response = $this->getInquiryResponse($requestId, $parameters);

        //Logging success message for inquiry
        $this->esewaPaymentLogger->logSuccessPayment($requestId, $response);
        return response()->json($response);
    }

    //Resolves the inquiry parameters for payment
    public function resolveInquiryParameters($requestId)
    {
        $project = self::getRelatedEntity($requestId);
        if ($project == false) {
            $this->logErrorResponse($requestId, 'missing project existence');
            return false;
        }

        if (!isset($project->advance_amount)) {
            $this->logErrorResponse($requestId, 'advance amount not defined');
            return false;
        }
        return $this->fetchPaymentData($project);
    }


    //Gets the related Project(Entity) for particular payment id
    private static function getRelatedEntity($requestId)
    {
        $project = Project::where('payment_id', $requestId)->first();
        if (isset($project)) {
            return $project;
        }
        return false;
    }

    //Logging failed message
    public function logErrorResponse($requestId, $message = null)
    {
        $this->esewaPaymentLogger->logFailedMessage($requestId, $message);
    }


    //Returns payment data for payment.
    //payment data contains advance/emi amount
    private function fetchPaymentData($project)
    {
        $farmer = $project->farmers->first();
        $paymentDetails = [];
        if (!$farmer) {
            $this->logErrorResponse($project->payment_id, 'Farmer details could not be obtained');
            return false;
        }
        $amount = $project->emi_amount;
        if (!$project->isAdvanceAmountPaid()) {
            $amount = $project->advance_amount;
        }
        if (isset($amount) && $amount == 0) {
            $this->logErrorResponse($project->payment_id, 'Advance or  Emi amount is not defined for project');
            return false;
        }

        $paymentDetails = array_merge($paymentDetails, [
            'address' => $farmer->address,
            'farmer_name' => $farmer->farmer_name,
            'amount' => $amount
        ]);
        return $paymentDetails;
    }

    //Returns error response on failure
    public function errorResponse($requestId, $message = null)
    {
        $error_response = [
            "response_message" => "Error due to Invalid token.",
            "response_code" => 1
        ];
        return response()->json($error_response);
    }

    //Logging error response on failure
    private function getInquiryResponse($requestId, $parameters)
    {
        $project = self::getRelatedEntity($requestId);
        $response = [
            "request_id" => (string)$requestId,
            "amount" => $parameters['amount'],
            "response_code" => 0,
            "response_message" => "SUCCESS",
            "properties" => array(
                'farmer_name' => (string)$parameters['farmer_name'],//(string)casting:esewa api requirement
                'address' => (string)$parameters['address'],
                'emi_no' => $project->emiNoToPay() ? $project->emiNoToPay() : "--",
                'emi_month' => $project->emiMonthToPay() ? $project->emiMonthToPay() : '--',
                'next_due_date' => $project->isAdvanceAmountPaid() ? $project->nextDueDateInBSFormat():'--',
                'payment_info' => $project->isAdvanceAmountPaid() ? strtoupper(PaymentType::EMI) : strtoupper(PaymentType::ADVANCE)
            )
        ];
        return $response;
    }

    //Fetches payment details to approve the transactions
    public function payment($requestId, $amount, $transactionCode)
    {
        //Logs every payment attempts using esewa service.
        $this->esewaPaymentLogger->paymentAttempt($requestId, $amount, $transactionCode);

        //Defining the parameters for logging and storing transaction
        $this->setParametersForLogging($requestId, $amount, $transactionCode);

        //Checking payment feasibility for project
        $is_payment_valid = $this->checkPaymentValidityForProject($requestId, $amount, $transactionCode);

        //Rejecting the payment if not feasible and responding error message
        if (!$is_payment_valid) {
            return $this->errorResponse($requestId);
        }

        //Getting reference code(unique) for each transaction
        $reference_code = $this->getReferenceCode($requestId, $this->paymentGateway);

        //Responding with error if unable to generate reference code
        if ($reference_code['status'] == false) {
            return $this->errorResponse($requestId);
        }

        //Resolving the parameters needed to log the payment in esewa payment table
        $paymentResponse = $this->resolvePaymentParameters($requestId, $amount, $transactionCode, $reference_code['code']);

        // Failure if payment parameters can not be resolved.
        if ($paymentResponse == null || $paymentResponse == false) {
            return $this->errorResponse($requestId);
        }

        //Processing esewa payment log
        $esewaPayment = $this->storeEsewaPayment($paymentResponse);


        //If payment fails for some reason
        if ($esewaPayment['status'] == 'error') {
            return $this->errorResponse($requestId);
        }

        //Resolving transaction parameter for successful esewa transaction
        $transactionParameters = $this->resolveTransactionParameters($esewaPayment);

        $transactionStatus = $this->transactionModuleRepository->storeTransaction($esewaPayment, $transactionParameters, 'esewa');
        if ($transactionStatus == false) {
            $this->logErrorMessageAndTransaction('error while saving transaction');
        }
        $project = self::getRelatedEntity($requestId);

        //Emi1 payment is made automatic
        $this->updateProjectState($project);

        //Convert the overdue project to operational
        $this->checkOverDueProject($project);

        $response = [
            'request_id' => $requestId,
            'response_code' => 0,
            'response_message' => "SUCCESS",
            'amount' => $amount,
            'reference_code' => $reference_code['code']
        ];
        $this->esewaPaymentLogger->paymentComplete($response, $transactionCode);
        $this->sendSmsForPayment($requestId,$esewaPayment);
        return response()->json($response);

    }


    //Resolves the payment parameters
    //Returns the advance amount first
    //Returns the emi amount after the advance amount is paid
    private function setParametersForLogging($requestId, $amount, $transactionCode)
    {
        $this->requestId = $requestId;
        $this->amount = $amount;
        $this->transactionCode = $transactionCode;
    }

    private function checkPaymentValidityForProject($requestId, $amount, $transactionCode)
    {
        // check if all parameters are available
        if ($requestId == null || $amount == null || $transactionCode == null) {
            $message = "insufficient parameters";
            $this->logErrorMessageAndTransaction($message);
            return false;
        }
        $project = self::getRelatedEntity($requestId);
        if (!$project) {
            $this->logErrorMessageAndTransaction('Existence of  project is missing');
            return false;
        }

        if ($project->status == 'plan') {
            $this->logErrorMessageAndTransaction('Project is still in plan stage');
            return false;
        }

        if ((!isset($project->emi_amount)) || $project->emi_amount == 0) {
            $this->logErrorMessageAndTransaction('Emi amount is still not defined for the project');
            return false;
        }

        if ((!isset($project->advance_amount)) || $project->advance_amount == 0) {
            $this->logErrorMessageAndTransaction('Advance amount is still not defined for the project');
            return false;
        }
        if (!isset($project->emi_start_date)) {
            $this->logErrorMessageAndTransaction('Advance amount is still not defined for the project');
            return false;
        }

        if ($project->isAllEmiAmountPaid()) {
            $this->logErrorMessageAndTransaction('All Emi amount is paid');
            return false;
        }

        //First advance amount needs to be paid
        //Then Emi amount needs to be paid

        if (!$project->isAdvanceAmountPaid()) {
            if (((float)$amount != (float)$project->advance_amount)) {
                $this->logErrorMessageAndTransaction('Entered amount does not match with advance amount');
                return false;
            }
        } else {
            if (((float)$amount != (float)$project->emi_amount)) {
                $this->logErrorMessageAndTransaction('Entered amount does not match with emi amount');
                return false;
            }
        }

        return true;
    }

    public function logErrorMessageAndTransaction($message)
    {
        $this->esewaPaymentLogger->logFailedMessage($this->getRequestId(), $message);
        $transactionParameters = $this->resolveTransactionParameters(null, $message);
        $transactionStatus = $this->transactionModuleRepository->storeTransaction(null, $transactionParameters, 'esewa');
        return;
    }

    public function getRequestId()
    {
        return $this->requestId;
    }

    //Resolving the  transaction parameter for payment
    //Stores both success or failed transaction details
    public function resolveTransactionParameters($esewaPayment, $message = null)
    {
        $project = self::getRelatedEntity($this->getRequestId());
        return [
            'transaction_id' => $this->getTransactionCode() ? $this->getTransactionCode() : 'unknown',
            'amount' => isset($esewaPayment->amount) ? $esewaPayment->amount : $this->getAmount() ? $this->getAmount() : null,
            'currency' => "NRS",
            'transaction_type' => isset($esewaPayment->payment_type) ? $esewaPayment->payment_type : PaymentType::INVALID,
            'project_id' => isset($esewaPayment->project_id) ? $esewaPayment->project_id : ($project != false) ? $project->id : null,
            'payment_gateway' => 'esewa',
            'status' => isset($esewaPayment) ? 'paid' : 'unpaid',
            'response_data' => isset($esewaPayment) ? json_encode($esewaPayment->toArray()) : $message,
        ];

    }

    public function getTransactionCode()
    {
        return $this->transactionCode;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    // Generates unique reference code so that we can used it in future
    // Reference codes are unique ids for the OGB for uniquely identifying transaction
    // and it can be used further if necessary
    public function getReferenceCode($requestId, $provider)
    {
        $referenceCode = ['status' => false];
        if ($provider === PaymentProcessors::ESEWA) {
            $referenceCode = ['status' => true, 'code' => Uuid::generate()->string];
            if ($referenceCode == null || $referenceCode == false) {
                $message = "Unique OGB transaction code missing.";
                $this->logErrorMessageAndTransaction($message);
            }
        }
        return $referenceCode;
    }

    public function resolvePaymentParameters($requestId, $amount, $transactionCode, $referenceCode)
    {
        $project = self::getRelatedEntity($requestId);
        $last_transaction = Payment::where('project_id', $project->id)->where('paid_status', 1)->where('payment_type', PaymentType::EMI)->orderBy('paid_date', 'desc')->orderBy('id', 'desc')->first();
        $code = [
            'referenceCode' => $referenceCode,
            'transactionCode' => $transactionCode
        ];
        $transactionParameter = [
            'code' => json_encode($code),
            'amount' => $project->isAdvanceAmountPaid() ? $project->emi_amount : $project->advance_amount,
            'paid_status' => 1,
            'paid_date' => Carbon::now(),
            'project_id' => $project->id,
            'payment_gateway' => 'esewa',
            'payment_type' => $project->isAdvanceAmountPaid() ? PaymentType::EMI :PaymentType::ADVANCE
        ];
        if (!$project->isAdvanceAmountPaid()) {
            return $transactionParameter;
        }
        $transactionParameter = array_merge($transactionParameter, [
            'emi_start_month' => $project->nextEmiStartDate(),
            'emi_end_month' => $project->nextEmiEndDate()
        ]);

        return $transactionParameter;
    }

    //Store each esewa payment
    public function storeEsewaPayment($transactionParameters)
    {
        DB::beginTransaction();
        try {
            $esewaTransaction = Payment::create($transactionParameters);
            DB::commit();
            return $esewaTransaction;
        } catch (\Exception $ex) {
            DB::rollback();
            return ['status' => 'error', 'message' => $ex->getMessage()];
        }
    }

    private function sendSmsForPayment($payment_id,$paymentDetails)
    {
        if(isset($paymentDetails) && ($paymentDetails!=false)){
            $project = self::getRelatedEntity($payment_id);
            $farmer = $project->farmers->first();
            try{
                $agent = User::find($project->created_by);
                $send_sms_to_agent = true;
            }catch (\Exception $exception){
                $this->esewaPaymentLogger->smsFailedLog($payment_id,$exception->getMessage());
                $send_sms_to_agent = false;
            }
            $sms_details = $this->resolveSmsParameters($farmer ,$agent,$paymentDetails,$project);
           if($paymentDetails->payment_type == PaymentType::ADVANCE){
               $this->smsRepository->sendSmsNotification($agent->phone,'ADVANCE_PAID_MESSAGE_AGENT',$sms_details);
               $this->smsRepository->sendSmsNotification($farmer->contact_no,'ADVANCE_PAID_MESSAGE_FARMER',$sms_details);
           }else{
               $this->smsRepository->sendSmsNotification($agent->phone,'EMI_PAID_MESSAGE_AGENT',$sms_details);
               $this->smsRepository->sendSmsNotification($farmer->contact_no,'EMI_PAID_MESSAGE_FARMER',$sms_details);
           }
            $this->sendPaymentEmail($agent,$project,$farmer,$paymentDetails);
            return;
        }

    }

    public static function resolveSmsParameters($farmer, $agent, $paymentDetail,$project)
    {
        $sms_detail_param = [
            'farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name),
            'agent_name' => StringManipulator::getFirstTwoWords($agent->full_name),
            'amount' => StringManipulator::getFirstTwoWords($paymentDetail->amount),
        ];
        if ($paymentDetail->payment_type == PaymentType::ADVANCE) {
            return $sms_detail_param;
        }
        return array_merge($sms_detail_param, [
            'emi_no' => $paymentDetail->emiNo(),
            'emi_month' => $paymentDetail->paidMonthInBsFormat(),
            'next_due_date' => $project->nextDueDateInBSFormat(),
        ]);
    }


    private function sendPaymentEmail($agent, $project, $farmer, $paymentDetail)
    {
        $payment_type = $paymentDetail->payment_type;
        $email_attributes = $this->resolveEmailAttributes($agent, $project, $farmer, $payment_type);
        $params = [
            'payment_detail' => $paymentDetail,
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBsFormat()
        ];
        $email_type = $payment_type == PaymentType::ADVANCE ? AdvancePaymentEmail::class : EmiPaymentEmail::class;
        dispatch(new FireEmail($email_type, $email_attributes, $params));
    }

    private function resolveEmailAttributes($agent, $project, $farmer, $payment_type)
    {
        $subject = __('app.payment_subject', ['farmer_name' => $farmer->farmer_name]);
        if ($payment_type == PaymentType::ADVANCE) {
            $title = __('app.advance_payment_email');
        }
        if ($payment_type == PaymentType::EMI) {
            $title = __('app.emi_payment_email');
        }
        return new EmailParam(
            $from = env('MAIL_ADMIN', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );
    }

    //Update the first emi and collected advance state
    private function updateProjectState($project)
    {
        //Update emi paid state only when the state isnot updated
        if ($project->firstEmiPaid() && (!$project->isEmiPaidTransitionStateUpdated())) {
            $this->projectStateUpdateRepository->updateProjectEmiState($project);
        }


        if ($project->isAdvanceAmountPaid() && (!$project->isAdvanceCollectedStateUpdated())) {
            $this->projectStateUpdateRepository->updateProjectCollectedAdvanceState($project);
        }
    }

    private function checkOverDueProject($project)
    {
        if ($project->readyToBeOperational()) {
            $project->status = ProjectStatus::OPERATIONAL;
            $project->save();
            $this->operationalCycleTrigger->sendSmsEmail($project, ProjectStatus::OVERDUE,ProjectStatus::OPERATIONAL);
        }
    }
}