<?php

namespace App\Repositories\ApiPayment;

use App\Enums\PaymentType;
use App\Enums\Role;
use App\Models\Payment;
use App\Repositories\ApiPayment\Esewa\EsewaPaymentRepository;
use App\Repositories\CashPayment\CashPaymentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

abstract class PaymentProcessors
{
    const ESEWA = "esewa";
    const CASH = "cash";
}

class ApiPayment implements ApiPaymentRepository
{
    public $esewaPaymentRepository,$cashPaymentRepository;
    private static $pagination_limit = 10;
    //Dependency inject other Services for more api-payment
    public function __construct(EsewaPaymentRepository $esewaPaymentRepository ,CashPaymentRepository $cashPaymentRepository)
    {
        $this->esewaPaymentRepository = $esewaPaymentRepository;
        $this->cashPaymentRepository = $cashPaymentRepository;
    }


    public function paymentInquiry($input_param)
    {
        //Choose payment gateway from input param
        //Default ApiPaymentGateway Esewa
        $paymentGateway = isset($input_param['paymentGateway']) ? $input_param['paymentGateway'] : PaymentProcessors::ESEWA;
        switch ($paymentGateway) {
            case PaymentProcessors::ESEWA :
                $requestId = $input_param['request_id'];
                return $this->esewaPaymentRepository->paymentInquiry($requestId);

            //Todo::future = Other payment gateway can be added here
        }


        return [];
    }

    public function payment($input_param)
    {
        //Choose payment gateway from input param
        //Default ApiPaymentGateway chosen Esewa
        $paymentGateway = isset($input_param['paymentGateway']) ? $input_param['paymentGateway'] : PaymentProcessors::ESEWA;

        switch ($paymentGateway) {
            case PaymentProcessors::ESEWA:
                $requestId = isset($input_param['request_id']) ? $input_param['request_id'] : null;
                $amount = isset($input_param['amount']) ? $input_param['amount'] : null;
                $transactionCode = isset($input_param['transaction_code']) ? $input_param['transaction_code'] : null;
                return $this->esewaPaymentRepository->payment($requestId, $amount, $transactionCode);

            case PaymentProcessors::CASH:
                return $this->cashPaymentRepository->payment($input_param);

            //Todo::future = Other payment gateway can be added here
        }

    }

    public function fetchPayments($projects,$payment_type = null)
    {
        if(is_null($payment_type)){
            $payment_type = PaymentType::EMI;
        }
        $payments = Payment::with('project.farmers','project.district')
            ->where('payment_type', $payment_type);
        if(session('role') == Role::PARTNER_USER){
            $payments = $payments->whereIn('project_id', $projects->pluck('id')->toArray());
        }
        return $payments->orderBy('id','desc')->paginate(self::$pagination_limit);
    }


    public function amountRaisedInBsMonth($projects, $start_of_bs_month, $end_of_bs_month)
    {
        $regular_emi = 0;
        $overdue_emi = 0;
        $down_payment = 0;
        $project_ids = $projects->pluck('id')->toArray();
        $regular_emi += DB::table('payments')->whereIn('project_id', $project_ids)
            ->where('payment_type', PaymentType::EMI)
            ->whereDate('paid_date', '>=', $start_of_bs_month)
            ->whereDate('paid_date', '<=', $end_of_bs_month)
            ->whereColumn('paid_date', '<=','emi_start_month')
            ->sum('amount');


        $overdue_emi += DB::table('payments')->whereIn('project_id', $project_ids)
            ->where('payment_type', PaymentType::EMI)
            ->whereDate('paid_date', '>=', $start_of_bs_month)
            ->whereDate('paid_date', '<=', $end_of_bs_month)
            ->whereColumn('paid_date', '>', 'emi_start_month')
            ->sum('amount');


        $down_payment += Payment::whereIn('project_id', $project_ids)
            ->where('payment_type', PaymentType::ADVANCE)
            ->whereDate('paid_date', '>=', $start_of_bs_month)
            ->whereDate('paid_date', '<=', $end_of_bs_month)
            ->sum('amount');

        $total_emi = $regular_emi + $overdue_emi;

        return ['total' => $total_emi, 'regular' => $regular_emi, 'overdue' => $overdue_emi, 'down_payment' => $down_payment];

    }

    public function amountToBeRaisedInBsMonth($projects, $start_of_bs_month, $end_of_bs_month)
    {
        $total_regular_emi_count = $total_overdue_emi_count = $total_overdue_emi_amount = $total_regular_emi_amount = $total_overdue_amount_= $total_overdue_amount = 0;
        foreach ($projects as $project){
            $number_of_overdue =  $this->numberOfOverdueMonths($project ,$start_of_bs_month ,$end_of_bs_month);
            $total_regular_emi_count += $number_of_overdue['regular'];
            $total_overdue_emi_count += $number_of_overdue['overdue'];
            $total_regular_emi_amount +=  $number_of_overdue['regular'] * $project->emi_amount;
            $total_overdue_emi_amount +=  $number_of_overdue['overdue'] * $project->emi_amount;
            $total_overdue_amount +=  $number_of_overdue['total'] * $project->emi_amount;
        }

        return $overdue_payments = [
            'total_regular_emi_count' => $total_regular_emi_count,
            'total_overdue_emi_count' => $total_overdue_emi_count,
            'total_regular_emi_amount' => $total_regular_emi_amount,
            'total_overdue_emi_amount' => $total_overdue_emi_amount,
            'total_overdue_amount'=> $total_overdue_amount,
        ];

    }


    //calculates the number of overdue for particular month
    private function numberOfOverdueMonths($project ,$start_of_bs_month ,$end_of_bs_month)
    {
        //Getting  payments data of the project
        $payments = Payment::where('project_id', $project->id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->whereDate('paid_date','<', $start_of_bs_month)
            ->orderBy('emi_end_month', 'desc')
            ->orderBy('id', 'desc')
            ->get();


        if(isset($payments) && $payments->count() == $project->emi_count){
            return ['total' => 0 , 'regular' => 0 ,'overdue' => 0];
        }

        $last_transaction = $payments->first();

        //Handling first transaction
        if((!$last_transaction) && Carbon::now()->lte(Carbon::parse($project->emi_start_date)->addMonth(1))){
            return ['total' => 1 , 'regular' => 1 ,'overdue' => 0];
        }

        $last_payment_date = isset($last_transaction)? Carbon::parse($last_transaction->emi_end_month) :Carbon::parse($project->emi_start_date);

        //If the user has already paid for number of months earlier  regular,
        if($last_payment_date > $end_of_bs_month){
            return ['total' => 0 , 'regular' => 0 ,'overdue' => 0];
        }

        $monthly_overdue= 0;
        for ($overdue_count = 1; $overdue_count <= $project->emi_count ; $overdue_count++){
            if($start_of_bs_month <= $last_payment_date &&  $last_payment_date <= $end_of_bs_month ) {
                break;
            }
            $last_payment_date->addMonth(1); //using carbon mutability property
            $monthly_overdue++;
        }

        $regular_count = 0;
        $overdue_count = $monthly_overdue;
        if(Carbon::now()< Carbon::parse($project->emi_start_date)->addMonths($project->emi_count)){
            $regular_count = 1;
            $overdue_count = $monthly_overdue -$regular_count;
        }
        $total_overdue_count = $regular_count+ $overdue_count;
        return ['total' => $total_overdue_count , 'regular' => $regular_count ,'overdue' => $overdue_count];
    }

    public function getPaymentForProject($project)
    {
        $payment = Payment::with('project.farmers','project.district')
            ->where('project_id', $project->id);
//        if(session('role') == Role::PARTNER_USER){
//            $payments = $payment->where('project_id', $projects->pluck('id')->toArray());
//        }
        return $payment->orderBy('id','asc')->paginate(self::$pagination_limit);
    }
}