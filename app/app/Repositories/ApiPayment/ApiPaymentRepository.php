<?php
namespace App\Repositories\ApiPayment;

interface ApiPaymentRepository
{
    public function paymentInquiry($input_param);
    public function payment($request);
    public function fetchPayments($projects, $payment_type = null);
    public function amountRaisedInBsMonth($projects, $start_of_bs_month ,$end_of_bs_month);
    public function amountToBeRaisedInBsMonth($projects, $start_of_bs_month, $end_of_bs_month);
    public function getPaymentForProject($project);
}