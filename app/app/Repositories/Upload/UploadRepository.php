<?php
namespace App\Repositories\Upload;

interface UploadRepository
{
    public function uploadProjectFile($data,$projectId);
    public function deleteProjectFile($uploadId);

}