<?php

namespace App\Repositories\Upload;

use App\Models\Document;
use App\Models\Project;
use App\Models\Upload;
use App\Models\UploadAttributes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class UploadService implements UploadRepository
{
    protected $folder_path;

    public function __construct()
    {
//        $this->folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
        $this->folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'project_details' . DIRECTORY_SEPARATOR;

    }

    //Uploads the project(farmer) files
    /*public function uploadProjectFile($request, $projectId)
    {
        $project = Project::findOrFail($projectId);
        //$path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
        $upload_attributes = config('Upload.project');
        $valid_attribute = null;
        if (count($upload_attributes) > 0) {
            foreach ($upload_attributes as $attribute_key => $attribute) {
                if ($attribute_key == $request->hasFile($attribute_key)) {
                    $valid_attribute = $attribute_key;
                    break;
                }
            }
        }
        if ($request->hasFile($valid_attribute)) {
            $file = $request->file($valid_attribute);
            $original_file_name = $file->getClientOriginalName() ?: null;
            $original_file_name_extension = $file->getClientOriginalExtension() ?: null;
            $file_name = rand(1, 99999) . strtotime("now") . '_' . $original_file_name;
            $file->move($this->folder_path, $file_name);
            $upload = Upload::create([
                'file_name' => $file_name,
                'file_type' => $original_file_name_extension,
                'file_location' => $this->folder_path,
                'actual_file_name' => $original_file_name,
                'upload_attribute_key' => $valid_attribute,
            ]);
            if (isset($upload)) {
                $project->uploadable()->save($upload);
            }
            return $upload;

        }
        return false;
    }*/

    public function uploadProjectFile($request,$projectId) {
        $project = Project::findOrFail($projectId);
        //$path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
        $upload_attributes = config('Upload.project');
        $valid_attribute = null;
        if(count($upload_attributes)>0){
            foreach ($upload_attributes as $attribute_key => $attribute) {
                if($attribute_key == $request->hasFile($attribute_key)){
                    $valid_attribute = $attribute_key;
                    $category_id=DB::table('categories')->where('attribute_key',$attribute_key)->select('id')->first()->id;
                    break;
                }
            }
        }
        if($request->hasFile($valid_attribute)) {
            $file =$request->file($valid_attribute);
            $size = $file->getSize();
            $size = number_format($size / 1048576, 2);
            $original_file_name = $file->getClientOriginalName() ?: null;
            $original_file_name_extension = $file->getClientOriginalExtension() ?: null;
            $file_name = rand(1, 99999) . strtotime("now") . '_' . $original_file_name;
            $file->move($this->folder_path, $file_name);
          /*  $upload = Upload::create([
                'file_name' => $file_name,
                'file_type' => $original_file_name_extension,
                'file_location' => $this->folder_path,
                'actual_file_name' => $original_file_name,
                'upload_attribute_key' => $valid_attribute,
            ]);*/
            $document = Document::create([
                'project_id' => $projectId,
                'file_name' => $file_name,
                'actual_file_name' => $original_file_name,
                'file_type' => $original_file_name_extension,
                'path' => $this->folder_path,
                'size' => $size,
                'created_by' => Auth::user()->user_id
            ]);
            $category=DB::table('category_document')->insert(['category_id' => $category_id, 'document_id' => $document->id]);
            if (isset($upload)) {
                $project->uploadable()->save($upload);
            }
            return $document;

        }
        return false;
    }

    //delete the project(farmer) files
  /*  public function deleteProjectFile($document_id)
    {
        $upload = Upload::findOrFail($document_id);
        $path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'project_details'.DIRECTORY_SEPARATOR;
        if (isset($upload)){
            if(file_exists($path.$upload->file_name))
                unlink($path.$upload->file_name);
            $upload->delete();
            ;
        }
        return response()->json([
            'success' => false,
        ], 500);

    }*/
    public function deleteProjectFile($document_id)
    {
        $document_category = DB::table('category_document')->where('document_id', $document_id);
        $document=Document::find($document_id);
        $path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'project_details'.DIRECTORY_SEPARATOR;
        if (isset($document)){
            if(file_exists($path.$document->file_name))
                unlink($path.$document->file_name);
            $document_category->delete();
            $document->delete();
            ;
        }
        return response()->json([
            'success' => false,
        ], 500);

    }

}




