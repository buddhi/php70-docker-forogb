<?php


namespace App\Repositories\Meter;


interface MeterRepository
{
    public function getMetersList();
    public function getMeter($request);
    public function storeMeter($request);
    public function updateMeter($request,$id);
    public function getVerticalHead($project_id);
    public function getFarmerProjectModelAssociatedWithMeter($project_id);

}