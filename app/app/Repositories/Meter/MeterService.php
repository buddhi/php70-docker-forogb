<?php

namespace App\Repositories\Meter;

use App\Enums\ProjectStatus;
use App\Models\Meter;
use App\Models\Project;
use App\Services\ProjectStateUpdateTrigger;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MeterService implements MeterRepository
{
    private $projectStateUpdateTrigger;

    public function __construct(ProjectStateUpdateTrigger $projectStateUpdateTrigger)
    {
        $this->projectStateUpdateTrigger = $projectStateUpdateTrigger;
    }

    //Getting list of meters
    public function getMetersList()
    {
        return Meter::all();
    }

    //Getting meter with certain meter_id
    public function getMeter($meter_id)
    {
        try {
            return Meter::find($meter_id);
        } catch (ModelNotFoundException $exception) {
            Log::info('Meter not found for meter id' . $meter_id);
            return [];
        }
    }

    //Storing the  new meter
    //Updating project stage to operational when creating meter
    public function storeMeter($request)
    {
        $meter = Meter::create($request->all());
        $farmer_project = DB::table('farmer_project')->select('project_id')->where('id', $request->farmer_project_id)->first();
        $project = Project::with('transitionstate')->findOrFail($farmer_project->project_id);;
        if (self::isProjectReadyToBeOperational($project)) {
            $project->status = ProjectStatus::OPERATIONAL;
            $project->save();
            $this->projectStateUpdateTrigger->sendSmsEmailOnStateChange($project, ProjectStatus::OPERATIONAL);
        }
        return $meter;
    }

    public function updateMeter($request, $id)
    {
        $meter = $this->getMeter($id);
        $meter->update($request->all());
        return $meter;
    }

    //Meter can be created in many stages of project
    //Checking if project satisfies all the operational states conditions
    //If it satisfies all condition ,project stage can be upgraded
    private static function isProjectReadyToBeOperational($project)
    {
        if ($project->status == ProjectStatus::INSTALLED) {
            $project->getNextStatus($project->status);
            $installed_transition_states = getTransitionStates($project->getNextStatus($project->status), 'id');
            $current_installed_transition_states = $project->transitionstate->where('status', 'operational')->filter(function ($state) {
                return $state->pivot->value == 1;
            })->pluck('id')->toArray();
            if ($installed_transition_states == $current_installed_transition_states) {
                return true;
            }
        }
        return false;
    }

    //Getting the vertical head from survey form
    //Vertical head needs to be pre-filled in survey meter page
    public function getVerticalHead($project_id)
    {
        $project = Project::find($project_id);
        $farmer = $project->farmers->first();
        $farm = $farmer->farm;
        $vertical_head = $farm->vertical_head;
        return $vertical_head;
    }

    public function getFarmerProjectModelAssociatedWithMeter($project_id)
    {
        $farmer_project = DB::table('farmer_project')->select('id', 'farmer_id')->where('project_id', '=', $project_id)->first();
        return $farmer_project;
    }
}