<?php

namespace App\Repositories\Token;

use App\Models\Token;
use Illuminate\Database\Eloquent\Model;

interface TokenRepository
{
    public function generateToken();
    public function bindModelWithToken(Model $model, Token $token);
    public function modifyToken(Token $token);
}