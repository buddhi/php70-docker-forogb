<?php


namespace App\Repositories\Token;


use App\Models\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class TokenService implements  TokenRepository
{

    public function generateToken()
    {
        $token = Token::create([
            'token' => Uuid::generate()->string,
        ]);
        if (!$token) {
            return false;
        }
        return $token;
    }

    public function bindModelWithToken(Model $model,Token $token)
    {
        try{
            $model->tokens()->save($token);
            return true;
        }catch (\Exception $exception){
            Log::error($exception);
        }
        return false;
    }

    public function modifyToken(Token $token)
    {
        $token->update(['token' => Uuid::generate()->string]);
        if (!$token) {
            return false;
        }
        return $token;
    }
}