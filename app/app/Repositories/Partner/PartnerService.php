<?php

namespace App\Repositories\Partner;

use App\Enums\Role;
use App\Jobs\FireEmail;
use App\Mail\Ogbadmin\VerificationEmailToPartner;
use App\Models\Partner;
use App\Models\Token;
use App\OGB\Constants;
use App\Utilities\EmailParam;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class PartnerService implements PartnerRepository
{

    public function getById($partner_id)
    {
        try {
           $partner =  Partner::find($partner_id);
        } catch (ModelNotFoundException $exception) {
            return null;
        }
        return $partner;
    }
    public function getByEmail($email)
    {
        $partner =  Partner::where('email',$email)->first();
        if(!$partner){
            return null;
        }
        return $partner;
    }

    public function getAll($pagination_limit = null)
    {

        if($pagination_limit){


            return Partner::orderBy('id','desc')->paginate($pagination_limit);
        }
        return Partner::all();
    }

    public function store($data)
    {
        try {
            $partner = Partner::create($data);
        } catch (\Exception $exception) {
            Log::error($exception);
            return false;
        }
        return $partner;
    }

    public function update($partner,$data)
    {
        $partner->update($data);
        return $partner;
    }

    public function changeStatus($partner)
    {
        $partner->disabled = $partner->disabled ? Constants::ACTIVATE : Constants::DELETE;
        $partner->save();
        return true;
    }


    public function sendVerificationEmail($partner,$token)
    {

        //concat the nepali and english subject
        $subject = __('app.subject_for_email_verification_nepali').config('messages.subject_for_email_verification');
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $partner->email,
            null,
            $subject,
            $title
        );
        $param_for_email = ['partner' => $partner, 'token' => $token];
        dispatch(new FireEmail(VerificationEmailToPartner::class, $email_param, $param_for_email));
        return;
    }

    public function verifyEmail($token)
    {
        if(is_null($token)){
            return false;
        }
        //Matching the token in emails with that in tokens table
        $token = Token::where('token', $token)->first();

        //If no valid token then email cannot verified
        if (!$token) {
            return false;
        }
        $partner = $token->tokenable;
        if($partner instanceof  Partner){
            $partner->verified = Constants::ACTIVE;
            $partner->save();
            return $partner;
        }
        return false;
    }

    public function updatePartnerLogo(Partner $partner, $image)
    {
        $folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        if (!File::isDirectory($folder_path)) {
            File::makeDirectory($folder_path, 0777, true, true);
        }
        //Removing old partner-logo
        if (isset($partner->partner_logo) && file_exists($folder_path . $partner->partner_logo))
            unlink($folder_path . $partner->partner_logo);
        $image_extension = $image->getClientOriginalName();
        $partner_logo = rand(1, 99999) . strtotime("now") . '_'. $image_extension;
        //resizing the image library to fit into profile image design
        Image::make($image)
            ->fit(150, 150, function ($constraint) {
                $constraint->upsize();
            })->save($folder_path.DIRECTORY_SEPARATOR.$partner_logo);

        $partner->update([
            'partner_logo' => $partner_logo,
        ]);
    }

    public function getDisabledPartners()
    {
        return Partner::where('disabled',1)->pluck('id')->toArray();
    }

    public function getAllVerified()
    {
        // TODO: Implement getAllVerified() method.
        return Partner::where('verified',1)->get();
    }

}