<?php


namespace App\Repositories\Partner;

use App\Models\Partner;

interface PartnerRepository
{
    public function getById($partner_id);
    public function getByEmail($email);
    public function getAll($pagination_limit = null);
    public function store($data);
    public function update($partner, $data);
    public function changeStatus($partner);
    public function sendVerificationEmail($partner,$token);
    public function verifyEmail($token);
    public function updatePartnerLogo(Partner $partner, $image);
    public function getDisabledPartners();
    public function getAllVerified();
}