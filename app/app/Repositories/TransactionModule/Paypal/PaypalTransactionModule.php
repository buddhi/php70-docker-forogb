<?php

namespace App\Repositories\TransactionModule\Paypal;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaypalTransactionModule
{
    public function storeTransaction($result, $projectInvestments, $paymentGateway)
    {
        $transactions = [];
        foreach ($projectInvestments as $projectInvestment) {
            DB::beginTransaction();
            try {
                $transaction = Transaction::create([
                    'transaction_id' => $result->id,
                    'payer_email' => $result->payer->payer_info->email,
                    'payer_name' => $result->payer->payer_info->first_name . " " . $result->payer->payer_info->last_name,
                    'payee_email' => $result->transactions[0]->payee->email,
                    'payee_name' => $result->transactions[0]->payee->merchant_id,
                    'amount' => $result->transactions[0]->amount->total,
                    'currency' => $result->transactions[0]->amount->currency,
                    'user_id' => Auth::user()->user_id,
                    'project_id' => $projectInvestment->project_id,
                    'payment_gateway' => $paymentGateway,
                    'transaction_type' => $result->intent,
                    'status' => "paid",
                    'response_data' => json_encode($result->toJson()),
                    'created_at' => Carbon::now(),
                    'investment_id' => $projectInvestment->id,
                ]);
                DB::commit();
                $projectInvestment->transactional()->save($transaction);
                $transactions[] = $transaction;
            } catch (\Exception $ex) {
                DB::rollback();
                return null;
            }

        }
        return $transactions;
    }
}