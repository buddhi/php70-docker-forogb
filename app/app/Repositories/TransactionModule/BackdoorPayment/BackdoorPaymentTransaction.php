<?php

namespace App\Repositories\TransactionModule\BackdoorPayment;

use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BackdoorPaymentTransaction
{
    public function storeTransaction($data,$projectInvestments, $paymentGateway)
    {
        $transactions = [];
        foreach ($projectInvestments as $projectInvestment) {
            DB::beginTransaction();
            try {
                $transaction = Transaction::create([
                    'transaction_id' => "none",
                    'payer_email' => Auth::user()->email,
                    'payer_name' => Auth::user()->full_name,
                    'amount' => $projectInvestment->invest_amount,
                    'currency' => "NRS",
                    'user_id' => Auth::user()->user_id,
                    'project_id' => $projectInvestment->project_id,
                    'payment_gateway' => $paymentGateway,
                    'transaction_type' => "cash",
                    'status' => "paid",
                    'response_data' => "admin panel payment",
                    'created_at' => Carbon::now(),
                    'investment_id' => $projectInvestment->id,
                ]);
                DB::commit();
                $projectInvestment->transactional()->save($transaction);
                $transactions[] = $transaction;
            } catch (\Exception $ex) {
                DB::rollback();
                return null;
            }

        }
        return $transactions;
    }

}