<?php


namespace App\Repositories\TransactionModule\Cash;


use App\Models\Payment;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CashPaymentTransaction
{

    public function storeTransaction($transactionParameters,$project_investments,$paymentGateway)
    {
        //Handling Emi/Advance Payment Cash Transactions
        if($project_investments instanceof Payment) {
            return $this->handlePaymentTransaction($transactionParameters, $project_investments, $paymentGateway);
        }
        //handling multiple project investments transactions and bindings
        return $this->handleInvestmentTransaction($transactionParameters, $project_investments, $paymentGateway);

    }

    private function handlePaymentTransaction($transactionParameters, Payment $cash_payment, $paymentGateway)
    {
        DB::beginTransaction();
        try{
            $transaction = Transaction::create([
                'transaction_id' => isset($transactionParameters['transaction_id'])?$transactionParameters['transaction_id']:null,
                'amount' => $transactionParameters['amount'],
                'currency' => $transactionParameters['currency'],
                'transaction_type' => $transactionParameters['transaction_type'],
                'project_id' => $transactionParameters['project_id'],
                'payment_gateway' => $paymentGateway,
                'status' => $transactionParameters['status'],
                'response_data' => $transactionParameters['response_data'],
                'created_at' => Carbon::now(),
                'investment_id' => null,
            ]);
            if(isset($transaction) && isset($cash_payment)){
                $cash_payment->transactional()->save($transaction);
            }
            DB::commit();
            return $transaction;
        }catch (\Exception $ex){
            dd($ex);
            DB::rollback();
            return false;
        }
    }

    private function handleInvestmentTransaction($transactionParameters, $projectInvestments, $paymentGateway)
    {
        $transactions = [];
        foreach ($projectInvestments as $projectInvestment) {
            DB::beginTransaction();
            try {
                $transaction = Transaction::create([
                    'transaction_id' => "unknown",
                    'payer_email' => Auth::user()->email,
                    'payer_name' => Auth::user()->full_name,
                    'amount' => $projectInvestment->invest_amount,
                    'currency' => "NRS",
                    'user_id' => Auth::user()->user_id,
                    'project_id' => $projectInvestment->project_id,
                    'payment_gateway' => $paymentGateway,
                    'transaction_type' => "cash",
                    'status' => "paid",
                    'response_data' => "Admin panel door payment",
                    'created_at' => Carbon::now(),
                    'investment_id' => $projectInvestment->id,
                ]);
                DB::commit();
                $projectInvestment->transactional()->save($transaction);
                $transactions[] = $transaction;
            } catch (\Exception $ex) {
                DB::rollback();
                return null;
            }
        }
        return $transactions;
    }
}