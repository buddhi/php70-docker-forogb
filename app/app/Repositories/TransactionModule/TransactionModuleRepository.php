<?php

namespace App\Repositories\TransactionModule;

interface TransactionModuleRepository
{
    public function storeTransaction($data,$projectInvestments,$paymentGateway);
}