<?php

namespace App\Repositories\TransactionModule;


use App\Repositories\Transaction\Esewa\EsewaTransaction;
use App\Repositories\TransactionModule\BackdoorPayment\BackdoorPaymentTransaction;
use App\Repositories\TransactionModule\Cash\CashPaymentTransaction;
use App\Repositories\TransactionModule\Paypal\PaypalTransactionModule;

class TransactionModule implements  TransactionModuleRepository
{
    private $payPalTransaction,$esewaTransaction,$backdoorPaymentTransaction,$cashPaymentTransaction;

    //TODO Future: Dependency injection of other gateway class to handle other type of payment.
    public function __construct(
        PaypalTransactionModule $payPalTransaction,
        EsewaTransaction $esewaTransaction,
        BackdoorPaymentTransaction $backdoorPaymentTransaction, //for backdoor payment
        CashPaymentTransaction $cashPaymentTransaction
    )
    {
        $this->payPalTransaction = $payPalTransaction;
        $this->esewaTransaction= $esewaTransaction;
        $this->backdoorPaymentTransaction= $backdoorPaymentTransaction;
        $this->cashPaymentTransaction= $cashPaymentTransaction;
    }

    public function storeTransaction($data,$projectInvestments,$paymentGateway)
    {
        //TODo Future::create a method or class that resolves the all the request from different payment gateway
        switch (strtolower($paymentGateway)) {
            case 'paypal':
                $transactionStatus = $this->payPalTransaction->storeTransaction($data,$projectInvestments,$paymentGateway);
                return $transactionStatus;
                break;
            case 'esewa':
                $transactionStatus = $this->esewaTransaction->storeTransaction($data,$projectInvestments,$paymentGateway);
                return $transactionStatus;
                break;

            case 'nopaymentgateway':
                $transactionStatus = $this->backdoorPaymentTransaction->storeTransaction($data,$projectInvestments,$paymentGateway);
                return $transactionStatus;
                break;

            case 'cash':
                $transactionStatus = $this->cashPaymentTransaction->storeTransaction($data,$projectInvestments,$paymentGateway);
                return $transactionStatus;
                break;

            default:
                return false;//or we can choose the default payment gateway later
        }
    }
}