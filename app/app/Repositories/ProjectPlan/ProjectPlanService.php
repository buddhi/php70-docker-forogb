<?php
namespace App\Repositories\ProjectPlan;

use App\Enums\ProjectStatus;
use App\Models\Plan;
use App\Models\Project;
use App\Services\ProjectStateUpdateTrigger;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

Class ProjectPlanService implements ProjectPlanRepository {

    private $projectStateUpdateTrigger;
    public function __construct(ProjectStateUpdateTrigger $projectStateUpdateTrigger)
    {
        $this->projectStateUpdateTrigger = $projectStateUpdateTrigger;
    }
    public function getAllProjectsWithComments()
    {
        return Project::with('comments')->get();
    }

    //Open edit for the project-plan
    public function getProjectPlan($plan_id)
    {
        try{
            return Plan::find($plan_id);
        }catch (ModelNotFoundException $exception){
            Log::info($exception);
            return;
        }
    }

    public function getProjectPlanData($plan_id)
    {
        try{
            return Plan::find($plan_id);
        }catch (ModelNotFoundException $exception){
            Log::info($exception);
            return;
        }
    }
    //Open edit for the project-plan
    public function getProject($id)
    {
        try{
            return Project::find($id);
        }catch (ModelNotFoundException $exception){
            Log::info($exception);
            return false;
        }
    }

    //Update ==> project-plan and status to approve stage
    public function updateProjectPlan($request,$id)
    {
        $project = $this->getProject($id);

        //Calculating total emi payment and storing in request object
        if($request->get('emi_amount') && $request->get('emi_count')){
            $total_emi = $project->emi_amount * $project->emi_count;
            $request->request->add(['total_emi' => $total_emi]);
        }
        $project->update($request->all());
        if($project->isProjectReadyForApproval()){
            $project->status = ProjectStatus::APPROVED;
            $this->projectStateUpdateTrigger->sendSmsEmailOnStateChange($project,ProjectStatus::APPROVED);
            $project->save();
        }
    }


}