<?php

namespace App\Repositories\ProjectPlan;

interface ProjectPlanRepository
{
    public function getAllProjectsWithComments();

    public function getProject($id);

    public function getProjectPlan($plan_id);

    public function getProjectPlanData($plan_id);

    public function updateProjectPlan($request, $id);


}