<?php


namespace App\Repositories\Notification;


interface NotificationRepository
{
    public function sendNotification($notification);
}
