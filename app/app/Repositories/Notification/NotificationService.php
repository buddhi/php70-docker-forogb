<?php


namespace App\Repositories\Notification;

use App\Utilities\StringManipulator;


abstract class NotificationMessage
{
    public static $MESSAGES = [
        'PARTNER_EMAIL_VERIFIED' => 'Partner :name has been verified. Click to add additional information.',
        'CASH_INVESTMENT_COMPLETE' => ':partner_name, invested Rs. :invest_amount for :gender_salutation :farmer_name’s :pump_size hp project in :district'
    ];
}


class NotificationService implements NotificationRepository
{
    private $databaseNotification;

    public function __construct(DatabaseNotification $databaseNotification)
    {
        $this->databaseNotification = $databaseNotification;
    }

    public function sendNotification($notification)
    {
        switch ($notification->channel) {
            case 'database':
                $this->sendDatabaseNotification($notification);

            //TODO::future other channel of notification like slack hook , mail ,nexmo can be added here
        }
    }

    public function resolveNotificationMessage($notification_type, $substitution)
    {
        if (!isset(NotificationMessage::$MESSAGES[strtoupper($notification_type)])) {
            return false;
        }
        $message = NotificationMessage::$MESSAGES[strtoupper($notification_type)];
        return StringManipulator::makeReplacements($message, $substitution);
    }

    private function sendDatabaseNotification($notification)
    {
        $notification_attribute = $notification->parameters;
        $substitution = $notification_attribute['substitution'];
        $data['image'] = isset($notification_attribute['image'])? $notification_attribute['image']: $default_image = asset('img/farmer_default.png');
        $data['url'] = isset($notification_attribute['url'])? $notification_attribute['url']: '#';
        $data['notification_message'] = $this->resolveNotificationMessage($notification->type, $substitution);
        $this->databaseNotification->sendNotification($notification->to, $data);
    }

}