<?php


namespace App\Repositories\Notification;

use App\Notifications\SendDatabaseNotification;
use Illuminate\Support\Facades\Notification;

class DatabaseNotification
{
    public function sendNotification($to, $message)
    {

        Notification::send($to, new SendDatabaseNotification($message));
    }

}