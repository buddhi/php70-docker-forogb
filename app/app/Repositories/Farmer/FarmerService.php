<?php


namespace App\Repositories\Farmer;


use App\Models\CattleInfo;
use App\Models\CropInfo;
use App\Models\DistrictCropRisk;
use App\Models\Farmer;
use App\Models\Project;
use App\OGB\Constants;
use App\Scope\ProjectScope;

class FarmerService implements  FarmerRepository
{
    private $farmer_model,$project_model;
    public function __construct(Farmer $farmer_model,Project $project_model)
    {
        $this->farmer_model = $farmer_model;
        $this->project_model = $project_model;
    }
    public function setFarmerAttributes($data,$project)
    {
        $project_id = $project->id;
        $annual_expense = $data['request']->get('annual_expenses');
        $annual_household_income = $data['request']->get('annual_household_income');
        $this->farmer_model->farmer_name = $data['request']->get('farmer_name');
        $this->farmer_model->address = $data['request']->get('address');
        $this->farmer_model->latitude = $data['request']->get('latitude');
        $this->farmer_model->longitude = $data['request']->get('longitude');
        $this->farmer_model->contact_no = $data['request']->get('contact_no');
        $this->farmer_model->age = $data['request']->get('age');
        $this->farmer_model->village_name = $data['request']->get('village_name');
//        $this->farmer_model->no_of_dependents = $data['request']->get('no_of_dependents');
        $this->farmer_model->no_of_dependents = $data['request']->get('non_earning_members');
        $this->farmer_model->annual_household_income = $annual_household_income;
        $this->farmer_model->annual_expenses = $annual_expense;
        $this->farmer_model->outstanding_debt = $data['request']->get('outstanding_debt');
        $this->farmer_model->email=$data['request']->get('email');
        $this->farmer_model->email=$data['request']->get('email');
        $this->farmer_model->earning_members=$data['request']->get('earning_members');
        $this->farmer_model->non_earning_members=$data['request']->get('non_earning_members');
        $this->farmer_model->educational_level=$data['request']->get('educational_level');
        $this->farmer_model->has_cattle=$data['request']->get('has_cattle');
        $this->farmer_model->sell_crops=$data['request']->get('sell_crops');

        $this->farmer_model->financial_institute = null;
        if ($this->farmer_model->has_bank) {
            $this->farmer_model->financial_institute = $data['request']->get('financial_institute');
        }
        $income_source = $this->getIncomeSourceAmountDataInArrayFormat($data);
        $expense_source = $this->getExpenseSourceAmountDataInArrayFormat($data);
        $this->farmer_model->income = json_encode($income_source);
        $this->farmer_model->expense = json_encode($expense_source);
        $this->farmer_model->has_bank = $data['request']->get('has_bank');
        $this->farmer_model->gender = $data['request']->get('gender');
//        $this->farmer_model->education = $data['request']->get('education');
        $this->farmer_model->education = 1;
        $this->farmer_model->certification = $data['request']->get('certification');
       /* $this->farmer_model->no_of_people_in_house = $data['request']->get('no_of_people_in_house');*/
        $this->farmer_model->no_of_people_in_house = $data['request']->get('earning_members')+$data['request']->get('non_earning_members');
        $this->farmer_model->crop_inventory = $data['request']->get('crop_inventory');
        $this->farmer_model->fuel_monthly_expense = $data['request']->get('fuel_monthly_expense');
        $this->farmer_model->grid_monthly_expense = $data['request']->get('grid_monthly_expense');
        $this->farmer_model->water_distribution_to_farmers = $data['request']->get('water_distribution_to_farmers');
        $this->farmer_model->recent_large_purchase_through_payment_plan = $data['request']->get('recent_large_purchase_through_payment_plan');
        $this->farmer_model->future_capital_expenditure_causing_cash_flow_interruptions = $data['request']->get('future_capital_expenditure_causing_cash_flow_interruptions');
        $this->farmer_model->additional_income_source = $data['request']->get('additional_income_source');
        $this->farmer_model->additional_salaried_occupation = $data['request']->get('additional_salaried_occupation');
        $this->farmer_model->revenues_from_water_distribution = $data['request']->get('revenues_from_water_distribution');
        $this->farmer_model->debt_service_obligation = $data['request']->get('debt_service_obligation');
        $this->farmer_model->monthly_saving = $data['request']->get('monthly_saving');
        $this->farmer_model->monthly_saving_amount = 0;
        $this->farmer_model->debt_monthly_payment = 0;
        if ($data['request']->get('debt_service_obligation')) {
            $this->farmer_model->debt_monthly_payment = $data['request']->get('debt_monthly_payment');
        }
        if ($data['request']->get('monthly_saving')) {
            $this->farmer_model->monthly_saving_amount = $data['request']->get('monthly_saving_amount');
        }
        $this->farmer_model->expected_solar_loan_monthly_payment = $data['request']->get('expected_solar_loan_monthly_payment');
        $this->farmer_model->distance_to_nearest_market = $data['request']->get('distance_to_nearest_market');
        $this->farmer_model->nearest_market = $data['request']->get('nearest_market');
        $this->farmer_model->land_owned = $data['request']->get('land_owned');
        $this->farmer_model->size_fish = $data['request']->get('size_fish');
        $this->farmer_model->neighbour_trust = $data['request']->get('neighbour_trust');
        $this->farmer_model->harvest_lost = $data['request']->get('harvest_lost');
        $this->farmer_model->village_meeting = $data['request']->get('village_meeting');
        $this->farmer_model->percentage_of_solar_loan_on_expense = $this->solarLoanPercentageOnExpenses($data['request']->get('expected_solar_loan_monthly_payment'), $annual_expense);
        $this->farmer_model->percentage_of_expenses_on_income = $this->expensePercentageOnIncome($annual_expense, $annual_household_income);
        $this->farmer_model->percentage_of_debt_on_ebitda = $this->debtPercentageOnEbitda($this->farmer_model->debt_monthly_payment, $annual_expense, $annual_household_income);
        $this->farmer_model->farming_skill = $data['request']->get('farming_skill');
        $this->farmer_model->image_recall_exercise = $data['request']->get('image_recall_exercise');
        $this->farmer_model->question_timing = $data['request']->get('question_timing');
        $this->farmer_model->answered_all_questions = $data['request']->get('answered_all_questions');
        $this->farmer_model->equivocating_question = $data['request']->get('equivocating_question');
        $this->farmer_model->save();
        $cattleInfos = CattleInfo::where('project_id', $project_id)->get();
        $cropInfos = CropInfo::where('project_id', $project_id)->get();
        $this->farmer_model->livestock_risk = $this->checkLivestockRisk($cattleInfos);
        $this->farmer_model->livestock_diversity = $this->checkLivestockDiversity($cattleInfos);
        $this->farmer_model->crop_diversity = $this->checkCropDiversity($cropInfos);
        $this->farmer_model->has_cereal_grain = $this->hasCerealsOnly($cropInfos);
        $project =Project::withoutGlobalScope(ProjectScope::class)->find($project_id);
        $this->farmer_model->crop_risk = $this->checkCropDistrictRisk($cropInfos,$project->district_id);
        $this->farmer_model->save();
        return $this->farmer_model;
    }

    public function updateFarmerAttributes($data,$farmer,$project)
    {
        $project_id=$project->id;
        $this->farmer_model=Farmer::find($farmer->id);
        $annual_expense = $data['request']->get('annual_expenses');
        $annual_household_income = $data['request']->get('annual_household_income');
        $this->farmer_model->farmer_name = $data['request']->get('farmer_name');
        $this->farmer_model->address = $data['request']->get('address');
        $this->farmer_model->latitude = $data['request']->get('latitude');
        $this->farmer_model->longitude = $data['request']->get('longitude');
        $this->farmer_model->contact_no = $data['request']->get('contact_no');
        $this->farmer_model->age = $data['request']->get('age');
        $this->farmer_model->village_name = $data['request']->get('village_name');
//        $this->farmer_model->no_of_dependents = $data['request']->get('no_of_dependents');
        $this->farmer_model->no_of_dependents = $data['request']->get('non_earning_members');
        $this->farmer_model->annual_household_income = $annual_household_income;
        $this->farmer_model->annual_expenses = $annual_expense;
        $this->farmer_model->outstanding_debt = $data['request']->get('outstanding_debt');
        $this->farmer_model->email=$data['request']->get('email');
        $this->farmer_model->email=$data['request']->get('email');
        $this->farmer_model->earning_members=$data['request']->get('earning_members');
        $this->farmer_model->non_earning_members=$data['request']->get('non_earning_members');
        $this->farmer_model->educational_level=$data['request']->get('educational_level');
        $this->farmer_model->has_cattle=$data['request']->get('has_cattle');
        $this->farmer_model->sell_crops=$data['request']->get('sell_crops');

        $this->farmer_model->financial_institute = null;
        if ($this->farmer_model->has_bank) {
            $this->farmer_model->financial_institute = $data['request']->get('financial_institute');
        }
        $income_source = $this->getIncomeSourceAmountDataInArrayFormat($data);
        $expense_source = $this->getExpenseSourceAmountDataInArrayFormat($data);
        $this->farmer_model->income = json_encode($income_source);
        $this->farmer_model->expense = json_encode($expense_source);
        $this->farmer_model->has_bank = $data['request']->get('has_bank');
        $this->farmer_model->gender = $data['request']->get('gender');
//        $this->farmer_model->education = $data['request']->get('education');
        $this->farmer_model->education = 1;
        $this->farmer_model->certification = $data['request']->get('certification');
        $this->farmer_model->no_of_people_in_house=$data['request']->get('earning_members')+$data['request']->get('non_earning_members');
//        $this->farmer_model->no_of_people_in_house = $data['request']->get('no_of_people_in_house');
        $this->farmer_model->crop_inventory = $data['request']->get('crop_inventory');
        $this->farmer_model->fuel_monthly_expense = $data['request']->get('fuel_monthly_expense');
        $this->farmer_model->grid_monthly_expense = $data['request']->get('grid_monthly_expense');
        $this->farmer_model->water_distribution_to_farmers = $data['request']->get('water_distribution_to_farmers');
        $this->farmer_model->recent_large_purchase_through_payment_plan = $data['request']->get('recent_large_purchase_through_payment_plan');
        $this->farmer_model->future_capital_expenditure_causing_cash_flow_interruptions = $data['request']->get('future_capital_expenditure_causing_cash_flow_interruptions');
        $this->farmer_model->additional_income_source = $data['request']->get('additional_income_source');
        $this->farmer_model->additional_salaried_occupation = $data['request']->get('additional_salaried_occupation');
        $this->farmer_model->revenues_from_water_distribution = $data['request']->get('revenues_from_water_distribution');
        $this->farmer_model->debt_service_obligation = $data['request']->get('debt_service_obligation');
        $this->farmer_model->monthly_saving = $data['request']->get('monthly_saving');
        $this->farmer_model->monthly_saving_amount = 0;
        $this->farmer_model->debt_monthly_payment = 0;
        if ($data['request']->get('debt_service_obligation')) {
            $this->farmer_model->debt_monthly_payment = $data['request']->get('debt_monthly_payment');
        }
        if ($data['request']->get('monthly_saving')) {
            $this->farmer_model->monthly_saving_amount = $data['request']->get('monthly_saving_amount');
        }
        $this->farmer_model->expected_solar_loan_monthly_payment = $data['request']->get('expected_solar_loan_monthly_payment');
        $this->farmer_model->distance_to_nearest_market =($data['request']['sell_crops']==1)?$data['request']->get('distance_to_nearest_market'):'';
        $this->farmer_model->nearest_market = $data['request']->get('nearest_market');
        $this->farmer_model->land_owned = $data['request']->get('land_owned');
        $this->farmer_model->size_fish = $data['request']->get('size_fish');
        $this->farmer_model->neighbour_trust = $data['request']->get('neighbour_trust');
        $this->farmer_model->harvest_lost = $data['request']->get('harvest_lost');
        $this->farmer_model->village_meeting = $data['request']->get('village_meeting');
        $this->farmer_model->percentage_of_solar_loan_on_expense = $this->solarLoanPercentageOnExpenses($data['request']->get('expected_solar_loan_monthly_payment'), $annual_expense);
        $this->farmer_model->percentage_of_expenses_on_income = $this->expensePercentageOnIncome($annual_expense, $annual_household_income);
        $this->farmer_model->percentage_of_debt_on_ebitda = $this->debtPercentageOnEbitda($this->farmer_model->debt_monthly_payment, $annual_expense, $annual_household_income);
        $this->farmer_model->farming_skill = $data['request']->get('farming_skill');
        $this->farmer_model->image_recall_exercise = $data['request']->get('image_recall_exercise');
        $this->farmer_model->question_timing = $data['request']->get('question_timing');
        $this->farmer_model->answered_all_questions = $data['request']->get('answered_all_questions');
        $this->farmer_model->equivocating_question = $data['request']->get('equivocating_question');
        $this->farmer_model->save();
        $cattleInfos = CattleInfo::where('project_id', $project_id)->get();
        $cropInfos = CropInfo::where('project_id', $project_id)->get();
        $this->farmer_model->livestock_risk = $this->checkLivestockRisk($cattleInfos);
        $this->farmer_model->livestock_diversity = $this->checkLivestockDiversity($cattleInfos);
        $this->farmer_model->crop_diversity = $this->checkCropDiversity($cropInfos);
        $this->farmer_model->has_cereal_grain = $this->hasCerealsOnly($cropInfos);
        $project =Project::withoutGlobalScope(ProjectScope::class)->find($project_id);
        $this->farmer_model->crop_risk = $this->checkCropDistrictRisk($cropInfos,$project->district_id);
        $this->farmer_model->save();
        return $this->farmer_model;
    }

       public function getIncomeSourceAmountDataInArrayFormat($data){
           $income_source_amount=[];
           $income_source_array = $data['request']->get('income_source');
//           dd($;income_source_array)

           if(isset($income_source_array) && count($income_source_array)> 0){
               foreach($income_source_array as $key => $income){
                   $income_source_amount[$key]=[
                       'income_source' => $income,
                       'income_amount' => $data['request']->get('income_amount')[$key],
                   ];
               }
           }
           return $income_source_amount;
       }

    public function getExpenseSourceAmountDataInArrayFormat($data){
        $expense_source_amount=[];
        $expense_source_array = $data['request']->get('expense_source');
        if(isset($expense_source_array) && count($expense_source_array)> 0){
            foreach($expense_source_array as $key => $expense){
                $expense_source_amount[$key]=[
                    'expense_source' => $expense,
                    'expense_amount' => $data['request']->get('expense_amount')[$key],
                ];
            }
        }
        return $expense_source_amount;
    }

    public function solarLoanPercentageOnExpenses($solar_loan_monthly_payment,$annual_expense)
    {
        if(isset($solar_loan_monthly_payment) && isset($annual_expense)){
            $monthly_expense = $annual_expense/12;
            if($monthly_expense==0) return null;
            $percentage = round(($solar_loan_monthly_payment/$monthly_expense)*100,2);
        }
        return isset($percentage)? $percentage:null;
    }

    public function debtPercentageOnEbitda($debt,$annual_expense,$annual_income)
    {
        if(isset($annual_income) && isset($annual_expense)){
            $ebitda = $annual_income-$annual_expense;
            if($ebitda==0) return 100;
            $percentage = round(($debt/$ebitda)*100,2);
        }
        return isset($percentage)? $percentage:null;
    }

    public function expensePercentageOnIncome($annual_expense,$annual_income)
    {
        if(isset($annual_expense) && isset($annual_income)){
            if($annual_income ==0) return 0;
            $percentage = round(($annual_expense/$annual_income)*100,2);
        }
        return isset($percentage)? $percentage:null;
    }


    /**
     * @param $crop_project_collection
     * @return boolean
     * Different district have different risky-crops list.
     * Return risky, if project's crops falls under the risky-crops list
     * Return risky if project crops absent
     */
    public function checkCropDistrictRisk($crop_project_collection,$district_id)
    {

        if (!isset($crop_project_collection)) {
            return Constants::Risky;
        }
        $risky_crops = DistrictCropRisk::where('risk', Constants::Risky)->where('district_id', $district_id)->pluck('crop_id')->toArray();
        $crops_in_projects = $crop_project_collection->map(function ($crop_project) {
            return $crop_project->crop_id;
        })->toArray();
        $common_risky_crops = array_intersect($crops_in_projects, $risky_crops);
        if (count($common_risky_crops) > 0) {
            return Constants::Risky;
        }
        return Constants::Not_Risky;
    }


    /**
     * @param $crop_project_collection
     * @return boolean
     * Checks if non-cereal crops exists in project's crops
     */
    public function hasCerealsOnly($crop_project_collection)
    {
        $cereal_crops = array_keys(config('Crop.cereals'));
        $non_cereals_crops_present = 1;
        $non_cereals_crops_absent = 0;
        if ((!isset($crop_project_collection) || $crop_project_collection->isEmpty())) {
            return Constants::Risky;
        }
        $crops_in_project = $crop_project_collection->map(function ($crop_project) {
            return $crop_project->crop_id;
        })->toArray();
        $non_cereals_crops = array_diff($crops_in_project, $cereal_crops);
        if (count($non_cereals_crops) > 0)
            return $non_cereals_crops_present;
        return $non_cereals_crops_absent;
    }

    /**
     *
     * @param $cattle_project_collection
     * @return boolean
     * Checks livestock if present for farmer
     * If Livestock absent,return risky else not risky
     */
    public function checkLivestockRisk($cattle_project_collection)
    {
        if ((!isset($cattle_project_collection)) || $cattle_project_collection->isEmpty())
            return Constants::Risky;
        return Constants::Not_Risky;
    }


    /**
     *
     * @param $cattle_project_collection::cattles in project
     * @return boolean
     * Checks livestock risk for a farmer
     * If the farmer has only one(Livestock_diversity_threshold) type of livestock, then  returns cattle diversity present
     */
    public function checkLivestockDiversity($cattle_project_collection)
    {

        $cattle_diversity_present = 1;
        $cattle_diversity_absent = 0;
        if (!isset($cattle_project_collection) || $cattle_project_collection->isEmpty()) {
            return Constants::Risky;
        }
        $cattles_in_project = $cattle_project_collection->map(function ($cattle_project) {
            return $cattle_project->cattle_id;
        })->toArray();
        $unique_cattle_in_project = array_unique($cattles_in_project);
        if (count($unique_cattle_in_project) > Constants::Livestock_Diversity_Threshold) {
            return $cattle_diversity_present;
        }
        return $cattle_diversity_absent;
    }


    /**
     * @param $crop_project_collection
     * @return boolean
     * If farmer has  more than two(crop_diversity_threshold) unique crops, then returns crop_diversity is present
     */
    public function checkCropDiversity($crop_project_collection)
    {
        $crop_diversity_present = 1;
        $crop_diversity_absent = 0;
        if (!isset($crop_project_collection) || $crop_project_collection->isEmpty()) {
            return Constants::Risky;
        }
        $crops_in_project = $crop_project_collection->map(function ($crop_project) {
            return $crop_project->crop_id;
        })->toArray();

        $unique_crops_in_project = array_unique($crops_in_project);
        if (count($unique_crops_in_project) > Constants::Crop_Diversity_Threshold) {
            return $crop_diversity_present;
        }
        return $crop_diversity_absent;
    }
    public function editFarmerInformation(array $data,$project,$farmer){
        $this->farmer_model=$farmer;
//        dd($data['request']);
        $this->farmer_model = $this->setFarmerAttributes($data,$project);
        $this->farmer_model->save();
//        dd(Farmer::find(1)->education);
    }

    public function getIncomeArray($farmer)
    {
//        $income_array = isset($farmer->income)?json_decode($farmer->income,true):null;
//        $expense_array = isset($farmer->expense)?json_decode($farmer->expense,true):null;

        return isset($farmer->income)?json_decode($farmer->income,true):null;

    }

    public function getExpenseArray($farmer)
    {
        return isset($farmer->expense)?json_decode($farmer->expense,true):null;
    }

    public function getFarm($farmer)
    {
        return $farmer->farm;
    }
}