<?php


namespace App\Repositories\Farmer;


interface FarmerRepository
{
    public function setFarmerAttributes($data, $project);
    public function updateFarmerAttributes($data, $farmer,$project);
    public function getIncomeSourceAmountDataInArrayFormat($data);
    public function getExpenseSourceAmountDataInArrayFormat($data);
    public function editFarmerInformation(array $data,$project,$farmer);
    public  function getIncomeArray($farmer);
    public  function getExpenseArray($farmer);
    public function getFarm($farmer);
}