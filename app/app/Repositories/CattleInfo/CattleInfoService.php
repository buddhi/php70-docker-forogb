<?php


namespace App\Repositories\CattleInfo;


use App\Models\CattleInfo;

class CattleInfoService implements CattleInfoRepository
{

    public function setCattleInfo($data,$project)
    {
        if ($data['request']->has('cattle_id')) {
            foreach ($data['request']->get('cattle_id') as $key => $cattle_id) {
                if (isset($data['request']->get('cattle_info_id')[$key])) {
                    $cattle_info = CattleInfo::findOrNew($data['request']->get('cattle_info_id')[$key]);
                } else {
                    $cattle_info = new CattleInfo();
                }
                $cattle_info->cattle_id = $data['request']->get('cattle_id')[$key];
                $cattle_info->project_id = $project->id;
                $cattle_info->number = $data['request']->get('number')[$key];
                $cattle_info->save();
            }
        }
    }


    public function deleteCattleInfo($data,$project)
    {
        if ($data['request']->get('cattle_info_id')) {
            $cattle_info_previous_id = CattleInfo::select('id')->where('project_id', $project->id)->get();
            $i = 0;
            $cattle_info_id_array = [];
            foreach ($cattle_info_previous_id as $cattle_project_id) {
                $cattle_info_id_array[$i++] = $cattle_project_id->id;
            }
            $cattle_info_current_id = $data['request']->get('cattle_info_id');
            $deleted_cattle_info = array_diff($cattle_info_id_array, $cattle_info_current_id);
            if (count($deleted_cattle_info) > 0) {
                foreach ($deleted_cattle_info as $deleted_cattle_info_id) {
                    $cattle_info = CattleInfo::findOrFail($deleted_cattle_info_id);
                    $cattle_info->delete();
                }
            }
        }
    }

    public function getCattleDetailsForProject($project)
    {
        return CattleInfo::with('cattle')->where('project_id',$project->id)->get();
    }
}