<?php


namespace App\Repositories\CattleInfo;


interface CattleInfoRepository
{
    public function setCattleInfo($data,$project);
    public function deleteCattleInfo($data,$project);
    public function getCattleDetailsForProject($project);
}