<?php


namespace App\Repositories\CropInfo;


interface CropInfoRepository
{
    public function setCropInfo($data,$project);
    public function deleteCropInfo($data,$project);
    public function getCropDetailsForProject($project);
}