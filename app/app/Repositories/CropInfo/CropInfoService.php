<?php


namespace App\Repositories\CropInfo;


use App\Enums\Role;
use App\Models\CropInfo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CropInfoService implements CropInfoRepository
{
    public function setCropInfo($data,$project)
    {

        $updated_by = null;
        $updated_on = null;
        $created_on = null;
        $created_by = null;
        if (session('role') == Role::DEVELOPER) {
            $created_by=Auth::id();
            $created_on= Carbon::now();
        }
        if (session('role') == Role::ADMIN) {
            $created_on = $project->created_on;
            $created_by = $project->created_by;
            $updated_by = Auth::id();
            $updated_on = Carbon::now();
        }
        if ($data['request']->has('crop_id')) {
            foreach ($data['request']->get('crop_id') as $key => $crop_id) {
                if (isset($data['request']->get('crop_info_id')[$key])) {
                    $crop_info = CropInfo::findOrNew($data['request']->get('crop_info_id')[$key]);
                } else {
                    $crop_info = new CropInfo();
                }
                $crop_info->crop_id = $data['request']->get('crop_id')[$key];
                $crop_info->project_id = $project->id;
                $crop_info->start_month = ($data['request']->get('start_month')[$key])?$data['request']->get('start_month')[$key]:0;
                $crop_info->crop_area_unit_type = $data['request']->get('crop_area_unit_type')[$key];
                $crop_info->area = ($data['request']->get('area')[$key])?$data['request']->get('area')[$key]:0;
                $crop_info->created_by = $created_by;
                $crop_info->created_on= $created_on;
                $crop_info->updated_by = $updated_by;
                $crop_info->updated_on = $updated_on;
                $crop_info->save();
            }
        }
    }

    public function deleteCropInfo($data,$project)
    {

        if ($data['request']->get('crop_info_id')) {
            $crop_info_previous_id = CropInfo::select('id')->where('project_id', $project->id)->get();
            $i = 0;
            $crop_info_id_array = [];
            foreach ($crop_info_previous_id as $crop_project_id) {
                $crop_info_id_array[$i++] = $crop_project_id->id;
            }
            $crop_info_current_id = $data['request']->get('crop_info_id');
            $deleted_crop_info = array_diff($crop_info_id_array, $crop_info_current_id);
            if (count($deleted_crop_info) > 0) {
                foreach ($deleted_crop_info as $delete_crop_info_id) {
                    $crop_info = CropInfo::findOrFail($delete_crop_info_id);
                    $crop_info->delete();
                }
            }
        }
    }

    public function getCropDetailsForProject($project)
    {
        return CropInfo::with('crops')->where('project_id',$project->id)->get();
    }
}