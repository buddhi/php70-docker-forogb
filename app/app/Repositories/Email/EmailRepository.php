<?php

namespace App\Repositories\Email;

interface EmailRepository
{
    /********************************
     OPERATIONAL CYCLE EMAIL TRIGGERS
    ********************************/
    public function sendEmailToAgentWhenProjectOverDue($agent, $project, $farmer);
    public function sendEmailToAgentWhenProjectTransitsOperationalToError($agent, $project, $farmer);
    public function sendEmailToAdminWhenProjectTransitsOperationalToError($agent, $project, $farmer);

    public function sendEmailToAgentWhenProjectTransitsOverDueToOperational($agent, $project, $farmer);
    public function sendEmailToAdminWhenProjectTransitsOverDueToOperational($agent, $project, $farmer);

    public function sendEmailToAgentWhenProjectTransitsErrorToOperational($agent, $project, $farmer);
    public function sendEmailToAdminWhenProjectTransitsErrorToOperational($agent, $project, $farmer);

    public function sendEmailToAgentWhenProjectTransitsOverDueToError($agent, $project, $farmer);
    public function sendEmailToAdminWhenProjectTransitsOverDueToError($agent, $project, $farmer);

    /********************************
     * STATUS UPDATE EMAIL TRIGGERS
     ********************************/
    public function sendEmailsToAgentWhenProjectIsInstalled($agent, $project,$farmer);
    public function sendEmailsToInvestorsWhenProjectIsOperational($agent, $project,$farmer);

    /********************************
     * CONTACT DEVELOPER/ADMIN EMAIL TRIGGERS
     ********************************/
    public function sendMessageToAdminAndDeveloper($text_message, $project_id);
}