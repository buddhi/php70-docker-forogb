<?php

namespace App\Repositories\Email;
use App\Facade\DateConverter;
use App\Jobs\FireEmail;
use App\Jobs\SendEmail;
use App\Mail\ProjectErrorToOperationalMailAdmin;
use App\Mail\ProjectErrorToOperationalMailAgent;
use App\Mail\ProjectOperationalMail;
use App\Mail\ProjectOperationToErrorMailAdmin;
use App\Mail\ProjectOperationToErrorMailAgent;
use App\Mail\ProjectOverDueMail;
use App\Mail\ProjectOverDueToErrorMailAdmin;
use App\Mail\ProjectOverDueToErrorMailAgent;
use App\Mail\ProjectOverDueToOperationalMailAdmin;
use App\Mail\ProjectOverDueToOperationalMailAgent;
use App\Mail\SendProjectInstalledMail;
use App\Models\District;
use App\Models\Meter;
use App\Models\Project;
use App\User;
use App\Utilities\EmailParam;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class EmailService implements EmailRepository
{

    public function sendEmailToAgentWhenProjectOverDue($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_subject', ['farmer_name' => $farmer->farmer_name]);
        $title = 'Payment is OverDue !';

        // resolving email attributes
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
            'no_of_days_since_overdue' => $project->noOfDaysSinceOverdue(),
            'no_of_emi_overdue' => $project->noOfEmiOverDue()
        ];
        dispatch(new FireEmail(ProjectOverDueMail::class, $email_param, $param_for_email));
        return;
    }

    //Operational ==> error mail:agent
    public function sendEmailToAgentWhenProjectTransitsOperationalToError($agent, $project, $farmer)
    {
        $subject = __('app.project_error_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),

        ];
        dispatch(new FireEmail(ProjectOperationToErrorMailAgent::class, $email_param, $param_for_email));
        return;
    }

    //Operational ==> error mail:admin
    public function sendEmailToAdminWhenProjectTransitsOperationalToError($agent, $project, $farmer)
    {
        $subject = __('app.project_error_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = env('MAIL_ADMIN', null),
            $ccUsers = [env('RD_MAIL_ADMIN',null)], //other RD admins can be added in array future if needed
            $subject,
            $title
        );

        $meter_phone_number = $this->getMeterPhoneNumber($project->meter_id);
        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
            'meter_phone_number' => $meter_phone_number
        ];
        dispatch(new FireEmail(ProjectOperationToErrorMailAdmin::class, $email_param, $param_for_email));
        return;

    }

    public function sendEmailToAgentWhenProjectTransitsOverDueToOperational($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_to_operational_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),

        ];
        dispatch(new FireEmail(ProjectOverDueToOperationalMailAgent::class, $email_param, $param_for_email));
        return;
    }

    public function sendEmailToAdminWhenProjectTransitsOverDueToOperational($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_to_operational_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = env('MAIL_ADMIN', null),
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
        ];
        dispatch(new FireEmail(ProjectOverDueToOperationalMailAdmin::class, $email_param, $param_for_email));
        return;

    }

    public function sendEmailToAgentWhenProjectTransitsErrorToOperational($agent, $project, $farmer)
    {
        $subject = __('app.project_error_to_operational_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
        ];
        dispatch(new FireEmail(ProjectErrorToOperationalMailAgent::class, $email_param, $param_for_email));
        return;
    }

    public function sendEmailToAdminWhenProjectTransitsErrorToOperational($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_to_operational_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = env('MAIL_ADMIN', null),
            $ccUsers = [env('RD_MAIL_ADMIN',null)], //other RD admins can be added in array future if needed
            $subject,
            $title
        );

        $meter_phone_number = $this->getMeterPhoneNumber($project->meter_id);
        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
            'meter_phone_number' => $meter_phone_number
        ];
        dispatch(new FireEmail(ProjectErrorToOperationalMailAdmin::class, $email_param, $param_for_email));
        return;
    }


    //Overdue ==> error mail:agent
    public function sendEmailToAgentWhenProjectTransitsOverDueToError($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_to_error_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );

        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
        ];
        dispatch(new FireEmail(ProjectOverDueToErrorMailAgent::class, $email_param, $param_for_email));
        return;
    }

    //OverDue ==> error mail:admin
    //TODO::future ==> find a c common method to handle the  operational triggers emails
    public function sendEmailToAdminWhenProjectTransitsOverDueToError($agent, $project, $farmer)
    {
        $subject = __('app.project_overdue_to_error_subject', ['farmer_name' => $farmer->farmer_name]);
        $title ="";
        $email_param = new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = env('MAIL_ADMIN', null),
            $ccUsers = [env('RD_MAIL_ADMIN',null)], //other RD admins can be added in array future if needed
            $subject,
            $title
        );
        $meter_phone_number = $this->getMeterPhoneNumber($project->meter_id);
        // resolving email values to be displayed in emails
        $param_for_email = [
            'agent' => $agent,
            'project' => $project,
            'farmer' => $farmer,
            'next_due_date' => $project->nextDueDateInBSFormat(),
            'meter_phone_number'=> $meter_phone_number
        ];
        dispatch(new FireEmail(ProjectOverDueToErrorMailAdmin::class, $email_param, $param_for_email));
        return;
    }


    public function sendEmailsToAgentWhenProjectIsInstalled($agent, $project, $farmer)
    {
        $subject = __('app.project_installed_subject', ['farmer_name' => $farmer->farmer_name]);
        $title = 'A project you created is installed!!';
        $email_param =  new EmailParam(
            $from = env('MAIL_USERNAME', null),
            $to = $agent->email,
            null,
            $subject,
            $title
        );
        $param_for_email = [ 'agent' => $agent ,'project' => $project,'farmer' => $agent ,'next_due_date' => $project->nextDueDateInBSFormat()];
        dispatch(new FireEmail(SendProjectInstalledMail::class,$email_param,$param_for_email));
        return;
    }

    public function sendEmailsToInvestorsWhenProjectIsOperational($agent, $project, $farmer)
    {
        $farmer_name = $farmer->farmer_name;
        $subject = "See how  {$farmer_name} is using the solar pump you funded | Gham Power";
        $district = District::find($project->district_id);
        $title = 'View your impact';
        $param_for_email = ['agent' => $agent, 'project' => $project, 'farmer' => $farmer, 'district' => $district, 'project_cost' => $project->cost/100];
        $investors = $project->getInvestors();
        foreach ($investors as $investor){
            $param_for_email = array_merge($param_for_email,['investor' => $investor]);
            $email_param =  new EmailParam(
                $from = env('MAIL_USERNAME', null),
                $to = $investor->email,
                null,
                $subject,
                $title
            );
            dispatch(new FireEmail(ProjectOperationalMail::class,$email_param,$param_for_email));
        }
        return;
    }

    public function sendMessageToAdminAndDeveloper($text_message, $project_id)
    {
        if (strlen($text_message) <= 0) {
            return response()->json(self::sendResponse(true, "Please enter some text for sending mail"));
        }
        $project = Project::findOrFail($project_id);
        $farmer = $project->farmers->first();
        $agent = User::find($project->created_by);
        $sender = Auth::user();
        $admin = env('MAIL_ADMIN', null);
        if ((!isset($admin)) && (!isset($sender))) {
            return false;
        }
        $ccUsers = [$sender];
        $email_subject = " Contact Developer – {$farmer->farmer_name} - from {$sender->full_name} to {$agent->full_name}";
        dispatch(new SendEmail($sender, $admin, $ccUsers, $text_message, $farmer, $email_subject, $agent));
        return true;
    }

    private function getMeterPhoneNumber($meter_id)
    {
        try{
            $meter = Meter::find($meter_id);
             return $meter->phoneNo;
        }catch (ModelNotFoundException $exception){
            Log::error($exception);
            return  null;
        }
    }
}