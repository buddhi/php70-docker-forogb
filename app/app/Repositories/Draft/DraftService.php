<?php

namespace App\Repositories\Draft;

use App\Models\Cattle;
use App\Models\Crop;
use App\Models\DraftProject;
use App\Models\Farm;
use App\Models\Farmer;
use App\Models\Project;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farmer\FarmerRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DraftService implements DraftRepository
{

    protected $project_model,$farmer_model,$farm_model,$farmerRepository,$farmRepository,$cropInfoRepository,$cattleInfoRepository,$draftRepository;
    public function __construct(
        Project $project_model,
        Farmer $farmer_model,
        Farm $farm_model,
        FarmerRepository $farmerRepository,
        FarmRepository $farmRepository,
        CropInfoRepository $cropInfoRepository,
        CattleInfoRepository $cattleInfoRepository
    ) {
        $this->project_model = $project_model;
        $this->farm_model = $farm_model;
        $this->farmer_model = $farmer_model;
        $this->farmerRepository = $farmerRepository;
        $this->farmRepository = $farmRepository;
        $this->cropInfoRepository = $cropInfoRepository;
        $this->cattleInfoRepository = $cattleInfoRepository;
    }

    public function getById($draft_id)
    {
        try{
            return DraftProject::find($draft_id);
        }catch (ModelNotFoundException $exception){
            Log::info($exception);
            return;
        }
    }

    //Storing data in drafts
    public function draftStore($data)
    {
        $crop_project = [];
        //Getting values from form and converting objects into json to store in draft
        $draft['project'] = $this->project_model->fill($data['request']->all())->toJson();
        $draft['farmer'] = $this->farmer_model->fill($data['request']->all())->toJson();
        $draft['farm'] = $this->farm_model->fill($data['request']->all())->toJson();

        //Sets the array of crops-project pivot table data on the basis of crop id for a project
        $crop_project = $this->setCropsProjectCombineData($data);

        //Sets the array of crops-project pivot table data on the basis of cattle id for a project
        $cattle_project = $this->setCattleProjectCombineData($data);

        $income_array = $this->farmerRepository->getIncomeSourceAmountDataInArrayFormat($data);
        $expense_array = $this->farmerRepository->getExpenseSourceAmountDataInArrayFormat($data);
        $land = $this->farmRepository->getLandDataInArrayFormat($data);

        //converting crop-project and cattle-project collection into json
        $draft['cropInfo'] = json_encode($crop_project,true);
        $draft['cattleInfo'] = json_encode($cattle_project);
        $draft['income_array'] = json_encode($income_array);
        $draft['expense_array'] = json_encode($expense_array);
        $draft['land'] = json_encode($land);
        $draftProject = new DraftProject();

        //Storing the json of different models into associative array
        $draftProject->data = json_encode($draft);
        $draftProject->user_id = Auth::id();
        $draftProject->save();
        $redirect_route = 'draft.edit';
        $return_value['redirect'] = $redirect_route;
        $return_value['id'] = $draftProject->id;
        return $return_value;
    }

    public function draftUpdate($data, $id)
    {
        $crop_project = [];
        $draftProject = DraftProject::findOrFail($id);

        //Getting values from form and converting objects into json to store in draft
        $draft['project'] = $this->project_model->fill($data['request']->all())->toJson();
        $draft['farmer'] = $this->farmer_model->fill($data['request']->all())->toJson();
        $draft['farm'] = $this->farm_model->fill($data['request']->all())->toJson();
        $crop_project = $this->setCropsProjectCombineData($data);
        $cattle_project = $this->setCattleProjectCombineData($data);
        $income_array = $this->farmerRepository->getIncomeSourceAmountDataInArrayFormat($data);
        $expense_array = $this->farmerRepository->getExpenseSourceAmountDataInArrayFormat($data);
        $land = $this->farmRepository->getLandDataInArrayFormat($data);
        //converting crop-project and cattle-project collection into json
        $draft['cropInfo'] = json_encode($crop_project);
        $draft['cattleInfo'] = json_encode($cattle_project);
        $draft['income_array'] = json_encode($income_array);
        $draft['expense_array'] = json_encode($expense_array);
        $draft['land'] = json_encode($land);
        //Storing the json of different models into associative array
        $draftProject->data = json_encode($draft);
        $draftProject->user_id = Auth::id();
        $draftProject->save();
        $redirect_route = 'draft.edit';
        $return_value['redirect'] = $redirect_route;
        $return_value['id'] = $draftProject->id;
        return $return_value;
    }


    public function delete($draftProject)
    {
        if($draftProject instanceof  DraftProject){
            $draftProject->delete();
        }
    }

    public function setCropsProjectCombineData(array $data)
    {
        $crop_project = [];
        $created_by =Auth::id();
        $i=0;
        if ($data['request']->get('crop_id')) {
            foreach ($data['request']->get('crop_id') as $key => $crop_id) {
                $crop_object = Crop::findOrFail($crop_id);
                $crop_project[$i++] = [
                    'crop_id' => $data['request']->get('crop_id')[$key],
                    'crop_name'=>$crop_object->name,
                    'start_month' => $data['request']->get('start_month')[$key],
                    'area' => $data['request']->get('area')[$key],
                    'created_by' => $created_by,
                ];
            }
        }
        return $crop_project;
    }

    public function setCattleProjectCombineData(array $data)
    {
        $cattle_project = [];
        $i=0;
        if ($data['request']->get('cattle_id')) {
            foreach ($data['request']->get('cattle_id') as $key => $cattle_id) {
                $cattle_object = Cattle::findOrFail($cattle_id);
                $cattle_project[$i++] = [
                    'cattle_id' => $data['request']->get('cattle_id')[$key],
                    'cattle_name' => $cattle_object->name,
                    'number' => $data['request']->get('number')[$key],
                    'created_by' => Auth::id()
                ];
            }
        }
        return $cattle_project;
    }

    public function getProjectDetails($array)
    {
        $project = new Project;
        $projectArray = json_decode($array['project'], true);
        $project->fill($projectArray);
        return $project;
    }

    public function getFarmerDetails($array)
    {
        $farmer = [];
        $farmerArray = json_decode($array['farmer'], true);
        $farmer[0] = new Farmer;
        $farmer[0]->fill($farmerArray);
        return $farmer;
    }

    public function getFarmDetails($array)
    {
        $farm = new Farm;
        $farmArray = json_decode($array['farm'], true);
        $farm->fill($farmArray);
        return $farm;
    }

    public function getCropDetails($array)
    {
        return isset($array['cropInfo']) ? json_decode($array['cropInfo'], true):null;
    }

    public function getCattleDetails($array)
    {
        return isset($array['cattleInfo']) ? json_decode($array['cattleInfo'], true):null;
    }

    public function getIncomeArray($array)
    {
       return  isset($array['income_array'])? json_decode($array['income_array'], true):null;
    }

    public function getExpenseArray($array)
    {
        return isset($array['expense_array'])?json_decode($array['expense_array'], true):null;
    }

    public function getLandDetails($array)
    {
        return $land_array = isset($array['land'])?json_decode($array['land'], true):null;
    }

    public function getDraftArray($draftProject)
    {
        return json_decode($draftProject->data, true);
    }
}