<?php


namespace App\Repositories\Draft;


interface DraftRepository
{
    public function getById($draft_id);
    public function delete($draft);
    public function draftStore($data);
    public function draftUpdate($data ,$id);
    public function getProjectDetails($array);
    public function getFarmerDetails($array);
    public function getFarmDetails($array);
    public function getCropDetails($array);
    public function getCattleDetails($array);
    public function getIncomeArray($array);
    public function getExpenseArray($array);
    public function getLandDetails($array);
    public function getDraftArray($draftProject);
}