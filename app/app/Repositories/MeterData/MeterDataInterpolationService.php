<?php

namespace App\Repositories\MeterData;

use App\Models\EnrichMeterData;
use App\Models\MeterData;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Exception;

class MeterDataInterpolationService
{
    public $meter;

    public function interpolateData($data, $meter, $real_time_data = 1)
    {
        $meterDataCollectionArray = collect($data);
        $meterDataCollection = new Collection();

        foreach ($meterDataCollectionArray as $meterDataArray) {
            $meterData = new MeterData();
            $meterData->fill($meterDataArray);
            $meterDataCollection->push($meterData);
        }
        $this->meter = $meter;
        $array = $meterDataCollection;
        $h = 0;
        $temp = [];
        $total = count($meterDataCollection);
        for ($key = 0; $key < ($total - 1); $key++) {
            echo "interpolating data " . $total . "\n";
            $timestamp_of_array_element1 = $this->getCarbonTimeStampAttribute($array[$key]->filename);
            if (!isset($timestamp_of_array_element1)) continue;
            $timestamp_of_array_element2 = $this->getCarbonTimeStampAttribute($array[$key + 1]->filename);
            if (!isset($timestamp_of_array_element2)) continue;
            $difference = $timestamp_of_array_element2->diffInMinutes($timestamp_of_array_element1);
            if ($difference <= 1) {
                $array[$key]->created_at = $timestamp_of_array_element1->toDateTimeString();;
                $temp[$h] = $array[$key];
                if ($real_time_data == 0) {
                    try {
                        $interpolated_meter_data = new EnrichMeterData();
                        $interpolated_meter_data->insert(collect($temp[$h])->toArray());
                        unset($interpolated_meter_data);
                    } catch (Exception $e) {
                        Log::error('Cannot write to the database');
                    }
                }
                $h++;
                continue;
            } else {
                if ($difference >= 10) {
                    $array[$key]->created_at = $this->getCarbonTimeStampAttribute($array[$key]->filename)->copy()->toDateTimeString();
                    $temp[$h] = $array[$key];
                    if ($real_time_data == 0) {
                        try {
                            $interpolated_meter_data = new EnrichMeterData();
                            $interpolated_meter_data->insert(collect($temp[$h])->toArray());
                            unset($interpolated_meter_data);
                        } catch (Exception $e) {
                            Log::error("Cannot write to the database");
                        }
                    }
                    $h++;
                    continue;
                }
                $array[$key]->created_at = $timestamp_of_array_element1->toDateTimeString();
                $temp[$h] = $array[$key];
                if ($real_time_data == 0) {
                    try {
                        $interpolated_meter_data = new EnrichMeterData();
                        $interpolated_meter_data->insert(collect($temp[$h])->toArray());
                        unset($interpolated_meter_data);
                    } catch (Exception $e) {
                        Log::error("Cannot write to the database");
                    }
                }
                $h++;
                $slope = ($array[$key + 1]->current - $array[$key]->current) / $difference;
                for ($i = 1; $i <= $difference - 1; $i++) {
                    $meterData = new MeterData();
                    $meterData->meter_id = $meter->id;
                    $meterData->current = $slope * $i + $array[$key]->current;
                    $meterData->voltage = $slope * $i + $array[$key]->voltage;
                    $meterData->temperature = $array[$key]->temperature;
                    $meterData->humidity = $array[$key]->humidity;
                    $meterData->soil_moisture = $array[$key]->soil_moisture;
                    $meterData->dc_power = $meterData->getDcPowerAttribute();
                    $meterData->ac_power = $meterData->getAcPowerAttribute();
                    $meterData->discharge = $meterData->getDischargeAttribute();
                    $meterData->power_percent = $meterData->getPowerPercentAttribute();
                    $meterData->frequency = $meterData->getFrequencyAttribute();
                    $meterData->rpm = $meterData->getRpmAttribute();
                    $meterData->runtime = $array[$key]->runtime;
                    $meterData->filename = $meter->phoneNo . '_'
                        . $this->getInterpolatedDate($array[$key]->filename, $i, 'y')
                        . $this->getInterpolatedDate($array[$key]->filename, $i, 'm')
                        . $this->getInterpolatedDate($array[$key]->filename, $i, 'd')
                        . $this->getInterpolatedDate($array[$key]->filename, $i, 'H')
                        . $this->getInterpolatedDate($array[$key]->filename, $i, 'i');
                    $meterData->created_at = $this->getInterpolatedDate($array[$key]->filename, $i);
                    $temp[$h] = $meterData;
                    if ($real_time_data == 0) {
                        try {
                            $interpolated_meter_data = new EnrichMeterData();
                            $interpolated_meter_data->insert(collect($temp[$h])->toArray());
                            unset($interpolated_meter_data);
                        } catch (Exception $e) {
                            Log::error("Cannot write to the database");
                        }
                        $h++;
                        continue;
                    }
                    $h++;
                }
            }
        }

        if (isset($array[$key]) && $array[$key] instanceof MeterData) {
            $last_element = $this->getCarbonTimeStampAttribute($array[$key]->filename);
            if (isset($last_element)) {
                $array[$key]->created_at = $this->getCarbonTimeStampAttribute($array[$key]->filename)->copy()->toDateTimeString();
                $temp[$h] = $array[$key];
                if ($real_time_data == 0) {
                    try {
                        $interpolated_meter_data = new EnrichMeterData();
                        $interpolated_meter_data->insert(collect($temp[$h])->toArray());
                        unset($interpolated_meter_data);
                    } catch (Exception $e) {
                        Log::error("Cannot write to the database");
                    }
                }
            }
        }

        if ($real_time_data == 0) {
            return;
        }
        return $temp;
    }

    public function getCarbonTimeStampAttribute($file_name)
    {
        $timestamp = explode('_', $file_name);
        if ((!isset($timestamp[1])) || $timestamp[1] == 0) {
            return null;
        }
        if (strlen($timestamp[1]) <= 8) {
            $timestamp[1] = Carbon::now()->format('y') . $timestamp[1];
        }
        $dateTime = DateTime::createFromFormat('ymdHi', $timestamp[1]);
        $carbonObject = Carbon::instance($dateTime);
        return $carbonObject;
    }

    public function getInterpolatedDate($file_name, $minute, $format = null)
    {
        $carbon_obj = $this->getCarbonTimeStampAttribute($file_name);
        $carbon_obj->addMinute($minute);
        if (isset($format)) {
            $time_stamp_format = $carbon_obj->format($format);
        } else {
            $time_stamp_format = $carbon_obj->toDateTimeString();
        }
        unset($carbon_obj);
        return $time_stamp_format;
    }

}