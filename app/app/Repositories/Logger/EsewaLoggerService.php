<?php

namespace App\Repositories\Logger;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class EsewaLoggerService
{
    public $log;
    public function __construct()
    {
        $this->log = new Logger('Esewa');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/payment/esewa.log')));
    }
    public function paymentAttempt($requestId,$amount,$transactionCode)
    {
        $this->log->info("Payment attempted for request id: " . $requestId .
            ", with amount : " . $amount .
            ", using payment gateway : Esewa ".
            ", with esewa transaction code : " . $transactionCode
        );
    }
    public function paymentComplete($response,$transactionCode)
    {
        $this->log->info("Payment finished for request id:  " . $response['request_id'].
            ", with amount : " . $response['amount'] .
            ", using payment gateway : Esewa".
            ", with esewa transaction code : " . $transactionCode.
            ", with OGB reference code :" . $response['reference_code']
        );
    }
    
    public function paymentInquiry($requestId)
    {
        $this->log->info("Payment enquired for request id: " . $requestId );
    }

    public function logFailedMessage($requestId ,$message)
    {
        $this->log->info("Payment failed for : " . $requestId ." with the message ".$message);
    }

    public function logSuccessPayment($requestId,$response)
    {
        $this->log->info("Payment inquiry succeed for : " . $requestId ." with the message ". json_encode($response,true));
    }

    public function smsFailedLog($payment_id,$message)
    {
        $this->log->info(" Sending sms failed for payment id: " .$payment_id ." with the message ". $message);

    }
    
}