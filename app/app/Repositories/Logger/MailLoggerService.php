<?php

namespace App\Repositories\Logger;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MailLoggerService
{
    public $log;
    private $message;
    private $type;
    
    public function __construct()
    {
        $this->log = new Logger('Mail');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/mail/mail.log')));
    }
    
    public function logMailInfo($sender,$reciever,$ccUsers)
    {
        $this->log->info("Mail sent from: ". $sender ." to ".$reciever." with cc to ".implode("|",$ccUsers));
    }
}