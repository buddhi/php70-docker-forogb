<?php

namespace App\Repositories\Logger;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ApplicationLogger
{
    public function logMessage($loggerName,$loggerFilePath,$logMessage)
    {
        $log = new Logger($loggerName);
        $log->pushHandler(new StreamHandler(storage_path($loggerFilePath)));
        $log->info($logMessage);
    }
}