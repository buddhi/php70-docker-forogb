<?php

namespace App\Repositories\User;

use App\Enums\Role;
use App\Jobs\SendVerificationEmail;
use App\Models\RoleUser;
use App\Models\VerifyUser;
use App\OGB\Constants;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class UserService implements UserRepository
{

    public function getUserList($request)
    {
        return User::filter($request);
    }

    public function getUser($user_id)
    {
        try {
            $user = User::find($user_id);
        } catch (ModelNotFoundException $exception) {
            Log::error($exception);
            return null;
        }
        return $user;
    }


    public function updateUser($request, $user_id)
    {
        $user = $this->getUser($user_id);
        if (isset($user) && $user->isUserUpdateAble($user->email)) {
            $user->username = $request->get('email');
            $user->email = $request->get('email');
            $user->full_name = $request->get('full_name');
            $user->phone = $request->get('phone');
            $user->address = $request->get('address');
            $user->save();
        }
        return $user;
    }

    public function changeStatus($request, $user_id)
    {
        $user = User::getAllUsers()->where('user_id', $user_id)->first();
        if (!isset($user)) {
            return false;
        }
        if ($user->isStatusChangeAble($user->email)) {
            $user->disabled = $user->disabled ? Constants::ACTIVATE : Constants::DELETE;
            $user->save();
            return true;
        }
        return false;
    }

    public function reSendVerificationEmail($request, $user_id)
    {
        $user = User::with('verifyusers')->findOrFail($user_id);
        $isVerificationTokenPresent = self::checkVerificationToken($user);
        if (isset($user) && $isVerificationTokenPresent) {
            dispatch(new SendVerificationEmail($user));
            return true;
        }
        return false;
    }

    private static function checkVerificationToken($user)
    {
        $verifyUser = $user->verifyusers;

        //If token absent then generate token
        if (!isset($verifyUser)) {
            VerifyUser::create([
                'user_id' => $user->user_id,
                'token' => Carbon::now()->timestamp . str_random(40)
            ]);
        }
        if (isset($verifyUser->token)) {
            return true;
        }
        return false;
    }

    public function updateUserOtpStatus($user_id, $otp)
    {
        $user = $this->getUser($user_id);
        $user->otp_verified = Constants::ACTIVE;
        $user->save();
    }

    public function createUser($userRegister, $data)
    {
        $user = new User;
        $user = User::create([
            'username' => $userRegister->email,
            'email' => $userRegister->email,
            'password' => bcrypt($data['password']),
            'full_name' => $data['full_name'],
            'phone' => $data['phone'],
            'address' => isset($data['address']) ? $data['address'] : null,
            'verified' => $userRegister->verified,
            'anonymous' => Constants::NOT_ANONYMOUS,
            'partner_id' => isset($userRegister->partner_id) ? $userRegister->partner_id : null,
        ]);
        return $user;
    }

    public function updateRoleForUser($user, $role)
    {
        if ($user) {
            RoleUser::create([
                'user_id' => $user->user_id,
                'role' => $role,
            ]);
        }

    }

    //TODO::create a separate service for partner users
    public function getPartnerUsers($pagination_limit = null)
    {
        $users = User::getAllUsers()->with('roles')->whereHas('roles', function ($q) {
            $q->where('role', '=', Role::PARTNER_USER);
        });

        $role = session('role');
        if ($role != Role::ADMIN) {
            $users = $this->getPartnerUser($users);
        }
        if (isset($pagination_limit)) {
            return $users->paginate($pagination_limit);
        }
        return $users->get();
    }

    public function getByEmail($email)
    {
        $user = User::where('email', $email)->first();
        if (!$user) {
            return null;
        }
        return $user;
    }


    public function getUserIncludingTrashedUser($user_id)
    {
        $user = User::getAllUsers()->where('user_id', $user_id)->first();
        if (!isset($user)) {
            return false;
        }
        return $user;
    }

    public function getAllUsersWithRole($pagination_limit = null, $include_deleted_model = false, $role = null)
    {
        $users = User::with('roles');
        if ($include_deleted_model) {
            $users = User::getAllUsers()->with('roles');
        }
        if (isset($role)) {
            $users->whereHas('roles', function ($q) use ($role) {
                $q->where('role', '=', $role);
            });
        }
        $user_role = session('role');
        if ($user_role != Role::ADMIN) {
            $partner_id = Auth::user()->partner_id;
            $users->where('partner_id', $partner_id);
        }
        if (isset($pagination_limit)) {
            return $users->orderBy('user_id', 'desc')->paginate($pagination_limit);
        }
        return $users->get();
    }

    public function uploadProfileImage($user, $request)
    {
        $folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        if (!File::isDirectory($folder_path)) {
            File::makeDirectory($folder_path, 0777, true, true);
        }
        if ($request->hasFile('file')) {
            if (isset($user->profile_image) && file_exists($folder_path . $user->profile_image) && ($user->profile_image != ''))
                unlink($folder_path . $user->profile_image);
            $image = $request->file('file');
            $image_extension = $image->getClientOriginalName();
            $profile_image = rand(1, 99999) . strtotime("now") . '_' . "social_image_" . $image_extension;
            Image::make($image)
                ->fit(150, 150, function ($constraint) {
                    $constraint->upsize();
                })->save($folder_path . DIRECTORY_SEPARATOR . $profile_image);
            $user->update([
                'profile_image' => $profile_image,
            ]);
            return true;
        }
        return false;
    }

    public function update($user, $data)
    {
        $user->update($data);
    }

    public function changeStatusOfAssociatedUser($partner)
    {
        User::getAllUsers()->where('partner_id', $partner->id)->update(['disabled' => $partner->disabled]);
    }

//    User activation code temporarily disabled

//    public function activateUserFromAdmin(Request $request)
//    {
//        $response = [];
//        $response['error'] = true;
//        $user_id = $request->get('user_id');
//        $user = User::with('roles')->findOrFail($user_id);
//        if (isset($user_id) && isset($user)) {
//            $user->admin_verification = Verification::ADMIN_VERIFIED;
//            $user->save();
//            $mail_sent = $this->sendConfirmationEmail($user);
//            if ($mail_sent) {
//                $response['error'] = false;
//            }
//        } else {
//            $response['message'] = 'Invalid request!!';
//        }
//        return response()->json(json_encode($response));
//    }
//
//    private function sendConfirmationEmail($reciever)
//    {
//        $sender = env('MAIL_ADMIN', 'noreply@ghampower.com');
//        dispatch(new SendUserActivation($sender, $reciever));
//        return true;
//    }

    public function storeCookie($user)
    {
        $user_details = [
            'user_id' => $user->user_id,
        ];
        Cookie::queue('user_details', json_encode($user_details), $minutes = 30);
    }

    public function getPartnerUser($query)
    {
        $role = session('role');
        switch ($role) {
            case Role::PARTNER_USER:
                $partner_id = Auth::user()->partner_id;
                $query = $query->where('partner_id', $partner_id);
                break;
        }
        return $query;
    }

    public function changeProfileImage($request)
    {
        $folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        if (!File::isDirectory($folder_path)) {
            File::makeDirectory($folder_path, 0777, true, true);
        }
        if ($request->hasFile('file')) {
            $user_id = $request->get('user_id');
            $user = User::find($user_id);
            if (isset($user->profile_image) && file_exists($folder_path . $user->profile_image) && ($user->profile_image != ''))
                unlink($folder_path . $user->profile_image);
            $image = $request->file('file');
            $image_extension = $image->getClientOriginalName();
            $profile_image = rand(1, 99999) . strtotime("now") . '_' . "social_image_" . $image_extension;
            $image->move($folder_path, $profile_image);
            $user = User::where('user_id', $user_id)
                ->update(['profile_image' => $profile_image]);

            if($user){
                return $profile_image;
            }
        }
    }
}
