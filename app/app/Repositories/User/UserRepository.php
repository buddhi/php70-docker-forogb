<?php

namespace App\Repositories\User;

interface UserRepository
{
    public function getUser($user_id);

    public function updateUser($request, $user_id);

    public function changeStatus($request, $user_id);

    public function reSendVerificationEmail($request, $user_id);

    public function getUserList($request);

    public function updateUserOtpStatus($user_id, $otp);

    public function createUser($userRegister, $data);

    public function updateRoleForUser($user, $role);

    public function getPartnerUsers($pagination_limit = null);

    public function getByEmail($email);

    public function getUserIncludingTrashedUser($user_id);

    public function getAllUsersWithRole($pagination_limit = null,$include_deleted_model= false, $role = null);

    public function uploadProfileImage($user,$request);

    public function update($user,$data);

    public function changeStatusOfAssociatedUser($partner);

    public function storeCookie($user_details);

    public function changeProfileImage($request);

}
