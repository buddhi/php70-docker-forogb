<?php

namespace App\Repositories\PaymentModule;

use App\Repositories\Logger\ApplicationLogger;

use App\Repositories\PaymentModule\PayPalModule\PaypalModule;
use Illuminate\Support\Facades\Auth;

class PaymentModule implements PaymentModuleRepository
{
    private $applicationLogger, $paypal, $log_file_path, $logger_name;

    public function __construct(PaypalModule $paypal, ApplicationLogger $applicationLogger)
    {
        $this->paypal = $paypal;
        $this->applicationLogger = $applicationLogger;
        $this->log_file_path = 'logs/payment/payment.log';
        $this->logger_name = 'Payment';
    }


    /**
     * @param $projectPayments
     * @param $paymentGateway
     * @return bool|mixed
     * returns: url(paypal) or payment details
     * Redirects to different modules for  different payment gateway
     */
    public function paymentHandler($projectPayments, $paymentGateway)
    {
        switch (strtolower($paymentGateway)) {
            case 'paypal':
                $paymentStatus = $this->paypal->payWithPaypal($projectPayments);
                if (array_key_exists('message_type', $paymentStatus) && $paymentStatus['message_type'] == 'error') {
                    $this->logpaymentComplete($paymentStatus);
                }
                return $paymentStatus;
            default:
                return false;//or we can choose the default payment later
        }
    }

    /**
     * @param $paymentStatus
     * Logs the payment completion
     */
    public function logPaymentComplete($paymentStatus)
    {
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
        $message_type = $paymentStatus['message_type'];
        $message = $paymentStatus['message'];
        $log_message = "Payment:" . $message_type . $message . " Payment is completed with ip" . $ip;
        $this->applicationLogger->logMessage($this->logger_name, $this->log_file_path, $log_message);
    }

    /**
     * @param $paymentGateway
     * @return array|bool|mixed
     * Redirects the payment status completion details  for different payment gateway
     */
    public function paymentStatus($paymentGateway)
    {
        switch (strtolower($paymentGateway)) {
            case 'paypal':
                return $this->paypal->getPaymentStatus();
            default:
                return false;//or we can choose the default payment later
        }
    }

    /**
     * @param $invested_amount
     * @param $id
     * @param $paymentGateway
     * Logs the payment attempt for per project from certain gateway using certain remote ip by certain user
     */
    public function logPaymentAttempt($invested_amount, $id, $paymentGateway)
    {
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
        $log_message = "Payment attempted for project id: " . $id .
            ", with amount :" . $invested_amount .
            ", using payment gateway :" . $paymentGateway .
            ", by user with id: " . Auth::id() .
            ", and ip :" . $ip;
        $this->applicationLogger->logMessage($this->logger_name, $this->log_file_path, $log_message);
    }
}