<?php

namespace App\Repositories\PaymentModule\SecurePayment;


class SecurePayment implements SecurePaymentInterface
{
    
    /**
     * SecurePayment constructor.
     */
    private $secEnv = [];
    public function __construct()
    {
        $env    = config('env');
        $crypt  = new \Illuminate\Encryption\Encrypter($env['key']);
        if(env('APP_ENV') == 'development'){
            $env = getenv();
        }else{
            $env = $_ENV;
        }
        foreach ($env as $key => $value) {
            if (!is_array($value) && strpos($value, "ENC:") === 0) {
                $this->secEnv[$key] = $crypt->decrypt(substr($value, 4));
            }
        }
    }
    
    function secEnv($name, $fallback = '') {
        $secEnv = $this->secEnv;
        return isset($secEnv[$name]) ? $secEnv[$name] : env($name, $fallback);
    }
    
}