<?php

namespace App\Repositories\PaymentModule\SecurePayment;

interface SecurePaymentInterface
{
    public function secEnv($name, $fallback);
}