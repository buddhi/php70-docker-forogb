<?php

namespace App\Repositories\PaymentModule\PayPalModule;

use App\Repositories\PaymentModule\SecurePayment\SecurePaymentInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Exception\PayPalInvalidCredentialException;
use PayPal\Rest\ApiContext;

class PaypalModule
{
    private $_api_context, $paymentGateway, $totalProjectCost;

    public function __construct(SecurePaymentInterface $securePayment)
    {
        //Encrypting the paypal payment keys from env file
        $client_id = $securePayment->secEnv('PAYPAL_CLIENT_ID', '');
        $client_secret = $securePayment->secEnv('PAYPAL_SECRET', '');

        //Get the paypal configuration from config folder
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $client_id,
            $client_secret
        ));
        $this->_api_context->setConfig($paypal_conf['settings']);
        $this->paymentGateway = "paypal";
    }

    /**
     * @param $projectPayments
     * @return mixed:
     * makes the item list for each projects
     * returns paypal-success-url if success else returns failure message
     */
    public function payWithPaypal($projectPayments)
    {
        //item_list: items_list is  project list to be funded
        $item_list = $this->generateItemList($projectPayments);
        $total_amount = $this->getProjectCost();
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($total_amount);
        $description = config('messages.paypal_description');
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($description);
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('investment.status', ['paymentGateway' => $this->paymentGateway]))
            ->setCancelUrl(route('payment.failure'));
        $payment = new Payment();
        $payment->setPayer($payer)->setIntent('Sale')
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        $returnValue = [];
        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $connectionException) {
            if (Config::get('app.debug')) {
                return $this->getMessage("error", 'Connection timeout', "payment.failure");
            } else {
                return $this->getMessage("error", 'Some error occur, sorry for inconvenient', "payment.failure");
            }
        } catch (PayPalInvalidCredentialException $credentialExceptions) {
            return $this->getMessage("error", 'Some error occur, sorry for inconvenient', "payment.failure");
        } catch (\Exception $exception) {
            return $this->getMessage("error", 'Some error occur, sorry for inconvenient', "payment.failure");
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (!isset($redirect_url)) {
            return $this->getMessage("error", 'Unknown error occurred', "payment.failure");
        }

        return $this->getMessage("success", 'Paypal Link obtained', $redirect_url);
    }

    /**
     * @param $projectPayments
     * @return ItemList:ProjectList with project id
     * Currency conversion also happens
     */
    public function generateItemList($projectPayments)
    {
        $projectPaymentList = [];
        $total_project_cost = 0;
        foreach ($projectPayments as $key => $projectPayment) {
            $project_item = new Item();

            //Past code for reference
            //$project_cost = currencyConverter($projectPayment['amount'], 'USD');
            //100 :RS to dollar conversion rate ie 1USD = 1NPR

            $project_cost = $projectPayment['amount']/currencyConverterFactor("USD");
            $project_item->setName('Project Id: ' . $projectPayment['project_id'])
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($project_cost);
            $projectPaymentList[] = $project_item;
            $total_project_cost = $total_project_cost + $project_item->getPrice();
        }
        $this->setProjectCost($total_project_cost);
        $item_list = new ItemList();
        $item_list->setItems($projectPaymentList);
        return $item_list;
    }

    public function setProjectCost($total_project_cost)
    {
        $this->totalProjectCost = $total_project_cost;
    }

    public function getProjectCost()
    {
        return $this->totalProjectCost;
    }

    public function getMessage($message_type, $message, $route = null, $result = null)
    {
        $returnValue['message_type'] = $message_type;
        $returnValue['message'] = $message;
        $returnValue['route'] = $route;
        $returnValue['result'] = $result;
        return $returnValue;
    }

    /**
     * Execute the final checkouts
     * Gets the payment details completion
     * $result:get the checkout details
     * @return array|mixed
     */
    public function getPaymentStatus()
    {
        $returnValue = [];
        $payment_id = Session::get('paypal_payment_id');
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            return $returnValue = $this->getMessage("error", 'Payment failed', "payment.failure");
        }
        try {
            $payment = Payment::get($payment_id, $this->_api_context);
            $execution = new PaymentExecution();
            $execution->setPayerId(Input::get('PayerID'));
            $result = $payment->execute($execution, $this->_api_context);
            if ($result->getState() == 'approved') {
                $returnValue = $this->getMessage("success", 'Payment success', "project.success", $result);
                return $returnValue;
            }
        } catch (PayPalConnectionException $payPalConnectionExceptions) {
            return $returnValue = $this->getMessage("error", 'Some error occur, sorry for inconvenient', "payment.failure");
        } catch (\Exception $exception) {
            return $returnValue = $this->getMessage("error", 'Some error occur, sorry for inconvenient', "payment.failure");
        }
        //Just in case the result is not processed or handling unknown error:very unlikely case
        return $this->getMessage("error", 'Payment Failed', "payment.failure");
    }
}
