<?php


namespace App\Repositories\PaymentModule;

interface PaymentModuleRepository
{
    public function paymentHandler($projectPayments, $paymentGateway);

    public function paymentStatus($paymentGateway);

    public function logPaymentAttempt($invested_amount, $id, $paymentGateway);

    public function logPaymentComplete($paymentStatus);
}