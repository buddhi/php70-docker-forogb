<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $table ='farms';
    public $timestamps =false;
    protected $fillable =['land_size','current_irrigation_source','current_irrigation_system','boring_size','vertical_head','distance','farm_image','boring_depth','ground_water_level','type_of_soil','land','water_requirement'];
    
    public function farmer(){
        return $this->belongsTo(Farmer::class,'farmer_id');
    }
}
