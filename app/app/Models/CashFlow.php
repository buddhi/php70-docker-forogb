<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model
{
    protected $table = 'cashflow_measurements';

    public function crop(){
        return $this->belongsTo(Crop::class,'crop_id');
    }

    public function district(){
        return $this->belongsTo(District::class,'district_id');
    }
}
