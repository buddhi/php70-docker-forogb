<?php

namespace App\Models;

use App\Models\District;
use Illuminate\Database\Eloquent\Model;

class AverageMonthlyTemperature extends Model
{
    protected $table = "average_monthly_temperature";

    protected $fillable =['temperature','district','month'];

    public function district()
    {
        return $this->belongsTo(District::class,'district_id');
    }
}
