<?php

namespace App\Models;

use App\Enums\PartnerStatus;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = ['contact_person', 'name', 'email', 'delete_flg', 'verified','address', 'landline', 'phone', 'partner_logo' ,'disabled'];

    public function tokens()
    {
        return $this->morphOne('App\Models\Token', 'tokenable');
    }

    public function getStatusAttribute()
    {
        if ($this->disabled) {
            return PartnerStatus::DEACTIVATED;
        }
        if (!$this->verified) {
            return PartnerStatus::VERIFICATION_PENDING;
        }
        if ($this->verified) {
            return PartnerStatus::ACTIVATED;
        }
    }
}
