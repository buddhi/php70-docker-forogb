<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    public $timestamps = false;
    protected $table='tbl_crop';
    protected $guarded=[];

    public function projects()
    {
        return $this->belongsToMany(Project::class,'tbl_crop_info')->withPivot('start_month', 'end_month','area','created_by','created_on','updated_by','updated_on','delete_flg','crop_area_unit_type');
    }

    public function cropping_factor_day(){
        return $this->hasOne(CroppingFactorDay::class,'crop_id');
    }

    public function cropping_factor(){
        return $this->hasOne(CroppingFactor::class,'crop_id');
    }

    public function cashflow_mesaurement(){
        return $this->hasMany(CashFlow::class,'crop_id');
    }

    public function cashflow_values($district_id){
        return $this->cashflow_mesaurement()->where('district_id',$district_id)->first();
    }
}
