<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CropInfo extends Model
{
    public $timestamps = false;
    protected $table='tbl_crop_info';
    protected $fillable=['crop_id','project_id','start_month','end_month','area','created_by','created_on','updated_by','updated_on','crop_area_unit_type'];
    public function crops() {
        return $this->hasOne(Crop::Class,'id','crop_id');
    }
}
