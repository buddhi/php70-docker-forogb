<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskAttribute extends Model
{
    public function categories()
    {
        return $this->hasMany(RiskAttributeCategory::class,'risk_attribute_id','id');
    }

    public function risks()
    {
        return $this->belongsTo(Risk::class,'risk_id','id');
    }
}
