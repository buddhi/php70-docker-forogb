<?php

namespace App\Models;

use App\components\ProjectUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Farmer extends Model
{
    public $timestamps = false;

    protected  $table = 'farmers';
    public $timestamp = false;
    protected $appends = ['farmer_image'];
    protected $fillable =['farmer_name','address','latitude','longitude',
                          'contact_no','age','village_name','no_of_dependents',
                          'email','earning_members','non_earning_members','educational_level','has_cattles','sell_crops',
                          'annual_household_income','annual_expenses','outstanding_debt',
                          'has_bank','financial_institute', 'gender','education','certification','no_of_people_in_house','credit_score',
                           'water_distribution_to_farmers','monthly_saving','debt_service_obligation','water_distribution_to_farmers','monthly_saving',
                            'debt_service_obligation','recent_large_purchase_through_payment_plan','future_capital_expenditure_causing_cash_flow_interruptions',
                        'additional_income_source','additional_salaried_occupation','revenues_from_water_distribution','monthly_saving_amount','debt_monthly_payment',
                        'expected_solar_loan_monthly_payment','distance_to_nearest_market','nearest_market','nearest_market','crop_inventory','land_owned',
                'fuel_monthly_expense','grid_monthly_expense','size_fish','harvest_lost','farming_skill','neighbour_trust','village_meeting'
    ];

    public function meters()
    {
        return $this->hasMany(Meter::class);
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
    
    public function farm(){
        return $this->hasOne(Farm::class,'farmer_id');
    }

    public function getFarmerImageAttribute()
    {
        if(!$farmer = Farmer::with('projects')->find($this->id)){
            return [];
        };
        $project = $farmer->projects->first();
        $upload=DB::table('documents')->join('category_document','documents.id','=','category_document.document_id')
            ->where('documents.project_id',$project->id)
            ->where('category_document.category_id',1)
            ->select('documents.file_name')
            ->first();
       /* dd($upload);
       $upload = $project->uploadable->first(function ($upload){
            return $upload->upload_attribute_key == "farmer_photo" && $this->checkFileIsImage($upload);
        });*/
        if(isset($upload->file_name)){
             return $image_path = asset('storage/project_details/'.$upload->file_name);
        }
        return null;
    }

    //Todo::future create the new class to handle this type of methods
    private function checkFileIsImage($upload)
    {
        $supported_image_type = array('jpg','jpeg','bmp','png','Jpg','Jpeg','Bmp','Png','JPG','JPEG','BMP','PNG');
        if(in_array($upload->file_type,$supported_image_type)){
            return true;
        }
        return false;
    }
}