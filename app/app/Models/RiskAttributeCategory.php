<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskAttributeCategory extends Model
{
    protected $guarded =[];

    public function riskAttributes()
    {
        return $this->belongsTo(RiskAttribute::class,'risk_attribute_id','id');
    }
}
