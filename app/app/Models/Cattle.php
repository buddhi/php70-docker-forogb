<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cattle extends Model
{
    public $timestamps = false;
    protected $table='cattles';
    protected $guarded=[];

    public function projects()
    {
        return $this->belongsToMany(Project::class,'tbl_cattle_info')->withPivot('number','created_by','created_on','updated_by','updated_on','delete_flg');
    }

    public function cattle_measurements(){
        return $this->hasMany(DistrictCattleInfo::class,'cattle_id');
    }

    public function cattle_values($district_id){
        return $this->cattle_measurements()->where('district_id',$district_id)->first();
    }
}
