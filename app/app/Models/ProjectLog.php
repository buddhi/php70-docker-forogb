<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProjectLog extends Model
{
    protected $table = 'project_logs';

    public function projects(){
        return $this->belongsTo(Project::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-M-Y');
    }
}
