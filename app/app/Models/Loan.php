<?php

namespace App\Models;

use App\Traits\Statable;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use Statable;

    const SM_CONFIG = 'loan';

    public $timestamps =false;
    protected $fillable =['next_due','over_due','status','updated_at','bank_name','loan_account_number','meter_id','last_state'];

    public function meter(){
        return $this->belongsTo(Meter::class,'meter_id');
    }
    
    public function transactional()
    {
        return $this->morphMany('App\Models\Transaction', 'transactional');
    }
}
