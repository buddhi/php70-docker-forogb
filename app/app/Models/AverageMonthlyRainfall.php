<?php

namespace App\Models;

use App\Models\District;
use Illuminate\Database\Eloquent\Model;

class AverageMonthlyRainfall extends Model
{
    protected $table = 'average_rainfall';

    protected $fillable = ['district_id','rainfall'];

    public function district()
    {
        return $this->belongsTo(District::class,'district_id');
    }
}
