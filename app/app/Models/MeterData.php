<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class MeterConstants{
    const SYSTEM_EFFICIENCY = 0.5;
    const PUMP_EFFICIENCY = 0.52;
    const CD_EFFICIENCY = 0.7;
    const MAXPOWER = 833.333;
    const HEAD = 3;
}

class MeterData extends Model
{

    public $dcPower = null;
    public $current_calc = null;
    public $voltage_calc = null;


    protected $fillable = ['id','filename','current','voltage','temperature','humidity','soil_moisture','meter_id','dc_power','power_percent','discharge','ac_power','frequency','rpm','runtime','created_at'];

    protected $appends = ['timestamp'];
    
    protected $hidden = ['id','meter','timestamp'];
    
    
    public function meter(){
        return $this->belongsTo(Meter::class);
    }
    public static function getDataByDate($start_date, $end_date,$meter_id){
       $data =  MeterData::where('meter_id',$meter_id)->whereDate('created_at','>=',$end_date)->whereDate('created_at','<=',$start_date)->get();
       return $data;
    }

    public static function aggregateDischargeAndRunTime($meter_id){
         return MeterData::select(DB::raw('sum(discharge) as total_discharge, sum(runtime) as total_runtime'))->where('meter_id',$meter_id)->first();
    }
    public function getDcPowerAttribute(){
        $this->current_calc = $this->attributes['current'];
        $this->voltage_calc = $this->attributes['voltage'];
        if($this->current_calc <= 0)
            return $this->attributes['dc_power'] = 0;
        $this->dcPower = $this->current_calc*$this->voltage_calc;
        return $this->dcPower;
    }
    public function getPowerPercentAttribute(){
        $this->getDcPowerAttribute();
        $max_power = $this->meter->max_power <= 0 ? MeterConstants::MAXPOWER : $this->meter->max_power;
        return ($this->dcPower/$max_power)*100;
    }
    public function getAcPowerAttribute(){
        $this->getDcPowerAttribute();
        $cd_efficiency = ($this->meter->cd_efficiency <= 0 or !isset($this->meter->cd_efficiency)) ? MeterConstants::CD_EFFICIENCY : $this->meter->cd_efficiency;
        if($this->current_calc <= 0)
            return $this->attributes['ac_power'] = 0;
        return $this->dcPower*$cd_efficiency;
    }
    public function getDischargeAttribute(){
        $this->getDcPowerAttribute();
        $pump_efficiency = $this->meter->pump_efficiency <= 0 ? MeterConstants::PUMP_EFFICIENCY : $this->meter->pump_efficiency;
        $head = $this->meter->head <= 0 ? MeterConstants::HEAD : $this->meter->head;
        $meter = $this->meter;
        if(!$meter->current_threshold){
            if($this->current_calc <= 0.5) return $this->attributes['discharge'] = 0;
        }else{
            if($this->current_calc <= $meter->current_threshold) return $this->attributes['discharge'] = 0;
        }
        $systemEfficiency = $this->getSystemEfficiency();
        return ($this->dcPower*$systemEfficiency*$pump_efficiency) / ($head*9.8);
    }
    public function getFrequencyAttribute(){
        if($this->getDischargeAttribute() <= 0)
            return $this->attributes['frequency'] = 0;
//        $var1 =  number_format(20.46 * (pow($this->current_calc,0.39)),2,'.','');
        $frequency =  $this->meter->frequency_multiplier * $this->getDischargeAttribute();
        if($frequency > 50){
            $frequency = 50;
        }
        return  $frequency;
    }
    public function getRpmAttribute(){
        if($this->getDischargeAttribute() <= 0)
            return $this->attributes['rpm'] = 0;
        return $this->meter->rpm_multiplier * $this->getFrequencyAttribute();
    }
    public function getTimeStampAttribute(){
        $filename = $this->attributes['filename'];
        $data = explode('_',$filename);
        $time = str_split($data[1]);
        $date = $time[0].$time[1];
        $month = $time[2].$time[3];
        $hour  = $time[4].$time[5];
        $minute = $time[6].$time[7];
        $time = $date.'/'.$month.' '.$hour.':'.$minute;

        return  $this->attributes['time_stamp'] = $time;
    }
    
    

    /**
     * @return float
     */
    private function getSystemEfficiency(): float
    {
        $this->getDcPowerAttribute();
        $systemEfficiency = MeterConstants::SYSTEM_EFFICIENCY;
        if ($this->dcPower <= 250) $systemEfficiency = $this->meter->sys_eff1;
        if ($this->dcPower > 250 && $this->dcPower <= 350) $systemEfficiency = $this->meter->sys_eff2;
        if ($this->dcPower > 350 && $this->dcPower <= 450) $systemEfficiency = $this->meter->sys_eff3;
        if ($this->dcPower > 450 && $this->dcPower <= 550) $systemEfficiency = $this->meter->sys_eff4;
        if ($this->dcPower > 550 && $this->dcPower <= 650) $systemEfficiency = $this->meter->sys_eff5;
        if ($this->dcPower > 650 && $this->dcPower <= 750) $systemEfficiency = $this->meter->sys_eff6;
        if ($this->dcPower > 750 && $this->dcPower <= 850) $systemEfficiency = $this->meter->sys_eff7;
        if ($this->dcPower > 850) $systemEfficiency = $this->meter->sys_eff8;
        return $systemEfficiency;
    }
    

}
