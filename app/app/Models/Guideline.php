<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guideline extends Model
{
    protected $table = 'status_guidelines';

    public function status(){
        return $this->hasOne(Status::class);
    }
    public function timeline(){
        return $this->hasMany(ProjectTimeline::class,'status_guidelines_id');
    }
}
