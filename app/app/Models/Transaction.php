<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable =['transaction_id','payer_email','payer_name',
        'payee_email','payee_name','amount', 'currency','payment_gateway','user_id','project_id',
        'transaction_type','status','response_data','created_at','investment_id'
        ,'transactional_id','transactional_type'
    ];
    public $timestamps =false;
    public function transactional()
    {
        return $this->morphTo();
    }
}
