<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransitionState extends Model
{
    public $guarded = [];
    public $timestamps = false;
    public function project()
    {
        return $this->belongsToMany(Project::class)->withPivot('value');
    }

}
