<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table="documents";
    protected $fillable=['project_id','file_name','actual_file_name','file_type','path','size','created_by'];


     public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
