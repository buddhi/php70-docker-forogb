<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    protected $fillable =['user_id','token'];
    public $timestamps= false;

    public function user()
    {
        return $this->belongsTo(User::class,"user_id");
    }
}
