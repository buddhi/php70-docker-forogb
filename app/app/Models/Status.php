<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';

    public function guidelines(){
        return $this->hasMany(Guideline::class);
    }

    public function category(){
        return $this->hasMany(Category::class);
    }
}
