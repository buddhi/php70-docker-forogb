<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    public $timestamps = false;
    protected $table ="tbl_municipality";
    protected $guarded =[];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
