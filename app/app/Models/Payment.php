<?php

namespace App\Models;

use App\Enums\PaymentType;
use App\Facade\DateConverter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
        'amount' => 'float',
    ];

    protected $dates = ['emi_start_month', 'emi_end_month','paid_date'];

    public function transactional()
    {
        return $this->morphMany('App\Models\Transaction', 'transactional');
    }

    public function uploadable()
    {
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public function emiNumber()
    {
        return Payment::where('project_id', $this->project_id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->where('id','<=', $this->id)
            ->count();
    }

    public function nextPaymentDueDate()
    {
        return Carbon::parse($this->emi_end_month)->toDateString();
    }


    public function paidMonth()
    {
        if (is_null($this->emi_start_month) || is_null($this->emi_end_month)) {
            return false;
        }
        $emi_start_date = Carbon::parse($this->emi_start_month);
        $emi_end_date = Carbon::parse($this->emi_end_month);
        $start_month = substr($emi_start_date->format("F"), 0, 3);
        $end_month = substr( $emi_end_date->format("F"), 0 ,3);
        $months_array = array_unique([$start_month, $end_month]);
        return implode('-', $months_array);
    }

    public function nextDueDate()
    {
        $last_transaction = Payment::where('project_id', $this->project_id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->orderBy('paid_date', 'desc')
            ->orderBy('id', 'desc')
            ->first();
        if (!$last_transaction) {
            $project = Project::find($this->project_id);
            return Carbon::parse($project->emi_start_date)->addMonth(1)->toDateString();
        }
        return Carbon::parse($last_transaction->end_month)->addMonth(1)->toDateString();
    }

    public function emiNo()
    {
        return Payment::where('project_id', $this->project_id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->count();
    }


    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id','id');
    }

    public function paidMonthInBsFormat()
    {
        $project = Project::find($this->project_id);
        if(!$project->isAdvanceAmountPaid()){
            return null;
        }
        if (is_null($this->emi_start_month) || is_null($this->emi_end_month)) {
            return false;
        }
        $emi_start_date = Carbon::parse($this->emi_start_month);
        $emi_end_date = Carbon::parse($this->emi_end_month);
        $nepali_emi_start_date = DateConverter::ad_to_bs($emi_start_date->format('Y'),$emi_start_date->format('m'),$emi_start_date->format('d'));
        $nepali_emi_end_date= DateConverter::ad_to_bs($emi_end_date->format('Y'),$emi_end_date->format('m'),$emi_end_date->format('d'));
        if(is_array($nepali_emi_start_date) && is_array($nepali_emi_end_date)){
            $nepali_start_month = $nepali_emi_start_date['nmonth'];
            $nepali_end_month = $nepali_emi_end_date['nmonth'];
            $months_array = array_unique([$nepali_start_month, $nepali_end_month]);
            return implode('-', $months_array);
        }
        return;
    }

}
