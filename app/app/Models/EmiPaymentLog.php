<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmiPaymentLog extends Model
{
    protected $fillable = ['monthly_start_emi_date','monthly_end_emi_date','paid_date','emi_amount','emi_remaining','no_of_paid_emi','project_id','status'];

}
