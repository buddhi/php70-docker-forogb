<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $timestamps = false;
    protected $table ="tbl_province";
    protected $guarded =[];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
    
    public function districts()
    {
        return $this->hasMany(District::class);
    }
}
