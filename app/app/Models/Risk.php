<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Risk extends Model
{
    protected $guarded=[];

    public function attributes()
    {
        return $this->hasMany(RiskAttribute::class);
    }
}
