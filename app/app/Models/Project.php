<?php

namespace App\Models;

use App\components\ProjectUtil;
use App\Enums\PaymentType;
use App\Enums\ProjectStatus;
use App\Enums\PumpSize;
use App\Enums\SystemName;
use App\Facade\DateConverter;
use App\Filters\Project\ProjectFilter;
use App\OGB\Constants;
use App\Scope\ProjectScope;
use App\Traits\OperationalCycle;
use App\Traits\ProjectState;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    use ProjectState,OperationalCycle;
    protected $table = 'tbl_project';
    const SM_CONFIG = 'project';
    protected $fillable = [
        'id',
        'code',
        'name',
        'project_type_id',
        'farmer_name',
        'province_id',
        'district_id',
        'municipality_id',
        'ward_no',
        'plan_id',
        'cost',
        'status',
        'latitude',
        'longitude',
        'account_number',
        'sim_number',
        'head',
        'created_by',
        'created_on',
        'updated_by',
        'updated_on',
        'is_draft',
        'delete_flg',
        'pump_size',
        'daily_discharge',
        'solar_pv_size',
        'payment_id',
        'total_emi',
        'emi_amount',
        'emi_count',
        'advance_amount',
        'is_advance_amount_paid',
        'advance_paid_date',
        'emi_start_date',
        'emi_end_date',
        'description',
        'pump_brand',
        'controller'
    ];
    public $timestamps = false;
    protected $appends = ['investment'];
    protected $casts = [
        'emi_amount' => 'float',
        'advance_amount' => 'float',
    ];

    public function getMeterIdAttribute()
    {
        $farmer_project = DB::table('farmer_project')->where('project_id', $this->id)->first();
        if (!isset($farmer_project)) return null;
        $meter = Meter::where('farmer_project_id', $farmer_project->id)->first();
        if (!$meter)
            return null;
        return $meter->id;
    }

    public function crops()
    {
        return $this->belongsToMany(Crop::class, 'tbl_crop_info')->withPivot('start_month', 'area', 'created_by',
            'created_on', 'updated_by', 'updated_on', 'delete_flg', 'crop_area_unit_type');
    }

    public function getProjectType(){
        return $this->belongsTo('App\Models\ProjectType','project_type_id');  
    }

    public function cattles()
    {
        return $this->belongsToMany(Cattle::class, 'tbl_cattle_info')->withPivot('number', 'created_by', 'created_on',
            'updated_by', 'updated_on', 'delete_flg');
    }

    public function meters()
    {
        return $this->hasMany(Meter::class, 'farmer_project_id', 'id');
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function farmers()
    {
        return $this->belongsToMany(Farmer::class)->withPivot('id');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }

    public function scopeInActive($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query->where('delete_flg', 1);
    }

    public function scopeActive($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query->where('delete_flg', 0)->where('status','!=', ProjectStatus::PLAN);
    }

    public function scopegetAllProjects($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query;
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ProjectScope());
    }

    public function uploadable()
    {
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public function getInvestmentAttribute()
    {
        return $this->attributes['investment'] = ProjectUtil::getProjectInvestmentTotal($this->id);

    }

    public function projectinvestments()
    {
        return $this->hasMany(ProjectInvestment::class, 'project_id', 'id');
    }

    public function transitionstate()
    {
        return $this->belongsToMany(TransitionState::class, 'project_transition_states', 'project_id', 'transition_state_id')->withPivot('value');
    }

    public function generatePaymentId()
    {
        if (isset($this->id) && isset($this->created_on)) {
            $payment_id = Carbon::parse($this->created_on);
            $payment_id = $payment_id->format('ym') . $this->id;
            return $payment_id;
        }
        return null;
    }

    public function isAdvanceAmountPaid()
    {
        $is_advance_amount_paid = Payment::where('project_id', $this->id)->where('paid_status', 1)->where('payment_type', PaymentType::ADVANCE)->first();
        if ($is_advance_amount_paid) {
            return true;
        }
        return false;
    }

    public function isAllEmiAmountPaid()
    {
        $emi_payment_count = Payment::where('project_id', $this->id)->where('paid_status', 1)->where('payment_type', PaymentType::EMI)->count();
        if ($emi_payment_count == $this->emi_count) {
            return true;
        }
        return false;
    }

    public function getRemainingAmount()
    {
        $investment = $this->getInvestmentAttribute();
        if (isset($investment) && isset($investment['investment_amount']) && isset($this->cost)) {
            return ($this->cost - $investment['investment_amount']);
        }
        return null;
    }

    public function scopeFilter(Builder $builder,$request)
    {
        return (new ProjectFilter($request))->filter();
    }

    public function firstEmiPaid()
    {
        $is_first_emi_paid = Payment::where('project_id', $this->id)->where('paid_status', 1)->where('payment_type', PaymentType::EMI)->first();
        if ($is_first_emi_paid) {
            return true;
        }
        return false;
    }

    public function isEmiPending()
    {
        $next_due_date = $this->nextDueDate();
        if(is_null($next_due_date)){
             return false;
        }

        if(Carbon::now()->gt($next_due_date))
            return true;
        return false;
    }

    public function readyToBeOperational()
    {
        if($this->status == ProjectStatus::OVERDUE){
            $last_emi_paid = Payment::where('project_id', $this->id)
                ->where('paid_status', 1)
                ->where('payment_type', PaymentType::EMI)
                ->orderBy('paid_date', 'desc')
                ->orderBy('id', 'desc')
                ->first();
            if (isset($last_emi_paid)) {
                if (Carbon::now()->lt(Carbon::parse($last_emi_paid->emi_end_month)->endOfDay())) {
                    return true;
                }
            }
        }
        return false;
    }

    public function nextDueDate()
    {
        $last_transaction = Payment::where('project_id', $this->id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->orderBy('paid_date', 'desc')
            ->orderBy('id', 'desc')
            ->first();

        if (!isset($this->emi_start_date)) {
            return null;
        }
        if (!$last_transaction) {
            return Carbon::parse($this->emi_start_date)->toDateString();
        }
        return Carbon::parse($last_transaction->emi_end_month)->toDateString();
    }

    public function noOfDaysSinceOverdue()
    {
        $last_transaction = Payment::where('project_id', $this->id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->orderBy('paid_date', 'desc')
            ->orderBy('id', 'desc')
            ->first();
        $no_of_days_since_overdue = 0;
        if ($last_transaction) {
            $end_date = Carbon::parse($last_transaction->emi_end_month)->endOfDay();

            //end_date - today date
            //if end date is exceeded then difference is 1
            $difference = Carbon::now()->diff($end_date);
            if ($difference->invert == 1) {
                return ($end_date->diff(Carbon::now())->days < 1) ? 'today' : $difference->days;
            }
        }
        return $no_of_days_since_overdue;
    }

    public function noOfEmiOverDue()
    {
        $number_of_overdue_emi = 0;
        $last_transaction = Payment::where('project_id', $this->id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->orderBy('paid_date', 'desc')
            ->orderBy('id', 'desc')
            ->first();
        if ($last_transaction) {
            $end_date = Carbon::parse($last_transaction->emi_end_month)->endOfDay();
            $difference = Carbon::now()->diff($end_date);
            if ($difference->invert == 1) {
                return $number_of_overdue_emi = $difference->m + 1;
            }
        }
        return $number_of_overdue_emi;
    }


    public function emiNoToPay()
    {
        $paid_emi = Payment::where('project_id', $this->id)
            ->where('paid_status', 1)
            ->where('payment_type', PaymentType::EMI)
            ->count();
        if(!$this->isAdvanceAmountPaid()){
            return null;
        }
        if(isset($paid_emi)){
            return $emi_to_pay = $paid_emi + 1;
        }
        return $first_emi = 1;
    }

    public function emiMonthToPay()
    {
        if(!isset($this->emi_start_date)){
            return null;
        }
        if(!$this->isAdvanceAmountPaid()){
            return null;
        }
        $next_emi_start_date =  $this->nextEmiStartDate();
        $next_emi_end_date = $this->nextEmiEndDate();
        $nepali_next_emi_start_date = DateConverter::ad_to_bs($next_emi_start_date->format('Y'),$next_emi_start_date->format('m'),$next_emi_start_date->format('d'));
        $nepali_next_emi_end_date= DateConverter::ad_to_bs($next_emi_end_date->format('Y'),$next_emi_end_date->format('m'),$next_emi_end_date->format('d'));
        if(is_array($nepali_next_emi_start_date) && is_array($nepali_next_emi_end_date)){
            $nepali_start_month = $nepali_next_emi_start_date['nmonth'];
            $nepali_end_month = $nepali_next_emi_end_date['nmonth'];
            $months_array = array_unique([$nepali_start_month, $nepali_end_month]);
            return implode('-', $months_array);
        }
        return;
    }


    public function nextDueDateInBSFormat()
    {
        $next_due_date_in_nepali = "";
        $next_due_date  = $this->nextDueDate();
        if (Carbon::parse($next_due_date) instanceof Carbon) {
            $next_due_date_in_nepali = DateConverter::ad_to_bs_date(Carbon::parse($this->nextDueDate()));
        }
        return $next_due_date_in_nepali;
    }

    //Simply adding a month in due date of project
    public function postPaymentDueDateInBSFormat()
    {
        $next_due_date_in_bs_format = "";
        $next_due_date  = $this->nextDueDate();
        if (!is_null($next_due_date)) {
            $next_due_date_in_bs_format = DateConverter::ad_to_bs_date(BS_addMonth(Carbon::parse($this->nextDueDate()),1));
        }
        return $next_due_date_in_bs_format;
    }

    public function isProjectReadyForApproval()
    {
        if ($this->status != ProjectStatus::NEW)
            return false;

        $next_status = $this->getNextStatus($this->status);
        $approved_transition_states = getTransitionStates($next_status, 'id');
        $current_approved_transition_states = $this->transitionstate->where('status', $next_status)->filter(function ($state) {
            return $state->pivot->value == 1;
        })->pluck('id')->toArray();
        $isAllApprovedStatusChecked = !array_diff($approved_transition_states, $current_approved_transition_states) && !array_diff($current_approved_transition_states, $approved_transition_states);
        if ($isAllApprovedStatusChecked) {
            return true;
        }
        return false;
    }



    public function getInvestors()
    {
        $investments = ProjectInvestment::with('user')->where('project_id',$this->id)->get();
        $investors_ids = $investments->filter(function ($investment) {
            return isset($investment->user->email);
        })->map(function ($investment) {
            return $investment->user->user_id;
        })->toArray();
        $investors = array_unique($investors_ids);
        return User::with('partner')->findMany($investors);
    }

    /**
     * @param $value:  name of the system
     * @return string: name of system depending upon pump_size
     *
     * System name actually depends upon the pump size
     * If pump size:absent then return custom name
     */
    public function getNameAttribute($value)
    {
        if(isset($this->pump_size)){
            switch ($this->pump_size){
                case PumpSize::SAHAJ:
                    return SystemName::SAHAJ;
                    break;
                case PumpSize::SAMPURNA:
                    return SystemName::SAMPURNA;
                    break;
                case PumpSize::SAMBRIDHI:
                    return SystemName::SAMBRIDHI;
                    break;
            }

        }

        //if pump size is absent but there is name of system
        //still returns the name of system
        if(isset($value)){
            return $value;
        }
        return SystemName::CUSTOM;
    }

    public function nextEmiStartDate()
    {
        $emi_payment_count = Payment::where('project_id', $this->id)->where('paid_status', 1)->where('payment_type', PaymentType::EMI)->count();
        if ($emi_payment_count > 0) {
            return BS_addMonth(Carbon::parse($this->emi_start_date),$emi_payment_count);
        } else {
            return Carbon::parse($this->emi_start_date);
        }
    }

    public function nextEmiEndDate()
    {
        $emi_payment_count = Payment::where('project_id', $this->id)->where('paid_status', 1)->where('payment_type', PaymentType::EMI)->count();
        if ($emi_payment_count >0) {
            return BS_addMonth(Carbon::parse($this->emi_start_date),$emi_payment_count+1);
        } else {
            return BS_addMonth(Carbon::parse($this->emi_start_date),1);
        }
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class,'project_id');
    }

  /*  public function projectcomments(){

        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }*/

  public function projectLogs(){
      return $this->hasMany(ProjectLog::class);
  }

  public function getDefaultedPaymentsArray(){
      $defaultedPayments = $this->payments()->where('payment_type','emi')->whereColumn('emi_end_month', '<', 'paid_date')
          ->get();
      $defaulted = $defaultedPayments->map(function($item,$key){
          $graseDay = $item->emi_end_month->modify('+5 day')->lt($item->paid_date);
          if($graseDay) {
              $interval = $item->emi_end_month->diffInDays($item->paid_date);
              return $interval;
          }
          return false;
      });
      return $defaulted->filter()->values()->all();
  }

  public function getDevelopedBy(){
      return $this->hasOne(User::class,'user_id','created_by');
  }

}