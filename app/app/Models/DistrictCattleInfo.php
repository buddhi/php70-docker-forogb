<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictCattleInfo extends Model
{
    protected $table = 'tbl_district_cattle_info';

    public function crop(){
        return $this->belongsTo(Cattle::class,'cattle_id');
    }

    public function district(){
        return $this->belongsTo(District::class,'district_id');
    }
}
