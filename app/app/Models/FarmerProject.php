<?php

namespace App\Models;

use App\Models\Meter;
use Illuminate\Database\Eloquent\Model;

class FarmerProject extends Model
{
    protected $table ="farmer_project";
    protected $fillable = ['farmer_id','project_id'];
    public $timestamps =false;

    public function meters()
    {
        return $this->hasMany(Meter::class,'farmer_project_id');
    }
}
