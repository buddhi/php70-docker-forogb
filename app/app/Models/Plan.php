<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public $timestamps = false;
    protected $table ='tbl_plan';
    protected $fillable =['code','name','plan_cost','pump_size','daily_discharge','solar_pv_size','delete_flg','total_emi','emi_amount','emi_count','pump_brand',
        'controller','advance_amount','no_of_year_for_payment'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}


