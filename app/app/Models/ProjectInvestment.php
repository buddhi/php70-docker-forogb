<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProjectInvestment extends Model
{
    public $timestamps = false;
    protected  $table = 'tbl_project_investment';
    protected $fillable =['project_id','investor_id','investment_type_id','funding_type',
        'investment_percent','invest_amount','expected_irr','term','interest_rate','delete_flg','developer_id','created_by'
        ];
    
    public function transactional()
    {
        return $this->morphMany('App\Models\Transaction', 'transactional');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class,'investor_id','user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class,'project_id','id');
    }

}
