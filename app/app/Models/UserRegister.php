<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRegister extends Model
{
    protected  $guarded = [];

    public function tokens()
    {
        return $this->morphOne('App\Models\Token', 'tokenable');
    }
}
