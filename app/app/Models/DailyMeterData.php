<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyMeterData extends Model
{
    protected $table ='daily_meter_data';
    protected $fillable =['daily_discharge','daily_runtime','daily_count','meter_id','date','daily_count'];
    public $timestamps = false;
}
