<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DraftProject extends Model
{
    protected $fillable =['data'];
    protected  $table ="draftproject";

}
