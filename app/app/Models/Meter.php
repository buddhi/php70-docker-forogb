<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Meter extends Model
{
    //
    public $timestamps = false;
    public $incrementing = true;
    protected  $table = 'meters';
   protected $fillable =['id','farmer_project_id','phoneNo','hardwareId',
                        'pump_efficiency','cd_efficiency','sys_eff1','sys_eff2','sys_eff3','sys_eff4','sys_eff5','sys_eff6','sys_eff7','sys_eff8',
                        'max_power','frequency_multiplier','rpm_multiplier','current_threshold',
                        'head','firmwareVersion','type','grace_ends_at','status','meter_type'];
    protected $appends = ['last_meter_data'];
    public function farmer()
    {
        return $this->belongsTo(Farmer::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function farmerproject()
    {
        return $this->belongsTo(FarmerProject::class, 'farmer_project_id');
    }

    public function loans(){
        return $this->hasMany(Loan::class,'meter_id');
    }
    public function meterdata(){
        return $this->hasMany(MeterData::class,'meter_id','id');
    }

    public function getLastMeterDataAttribute(){
        $meter_data = MeterData::where('meter_id', $this->id)->latest()->first();
        if(!$meter_data){
            return null;
        }
        return $meter_data->created_at;
    }

    public function getProjectIdAttribute()
    {
        $meter = DB::table('meters')->where('id',$this->id)->first();
        if(isset($meter)){
            $farmer_project_id =$meter->farmer_project_id;
            $farmer_project = DB::table('farmer_project')->where('id',$farmer_project_id)->first();
            if(isset($farmer_project))
                return $farmer_project->project_id;
        }
        return null;
    }

}
