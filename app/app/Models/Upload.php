<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable =['file_name','file_location','file_type','uploadable_type','uploadable_id','actual_file_name','upload_attribute_key'];
    protected $table ='uploads';
    
    public function uploadable()
    {
        return $this->morphTo();
    }
}
