<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CroppingFactorDay extends Model
{
    protected $table = 'cropping_factor_day';

    protected $fillable = ['crop_id','stage1','stage2','stage3','stage4'];

    public function crop()
    {
        return $this->belongsTo(Crop::class,'crop_id');
    }
}
