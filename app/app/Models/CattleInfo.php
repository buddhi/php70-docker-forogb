<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CattleInfo extends Model
{
    public $timestamps = false;

    protected $table='tbl_cattle_info';
    protected $fillable=['cattle_id','project_id','number','created_by','created_on','updated_by','updated_on'];
    public function cattle() {
        return $this->hasOne(Cattle::Class,'id','cattle_id');
    }
}
