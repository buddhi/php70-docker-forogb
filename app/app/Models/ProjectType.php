<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    public $timestamps = false;
    protected $table ="tbl_project_type";
    protected $guarded =[];

}
