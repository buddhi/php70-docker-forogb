<?php

namespace App\Models;

use App\AverageMonthlyRainfall;
use App\AverageMonthlyTemperature;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $timestamps = false;
    protected $table ="tbl_district";
    protected $guarded =[];
    protected $hidden = ['province_id','delete_flg'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
   
    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }

    public function averageMonthlyTemperature(){
        return $this->hasMany(AverageMonthlyTemperature::class,'district_id');
    }

    public function averageMonthlyRainFall(){
        return $this->hasMany(AverageMonthlyRainfall::class,'district_id');
    }

    public function cashflow_mesaurement(){
        return $this->hasMany(CashFlow::class,'district_id');
    }
    public function cattle_mesaurement(){
        return $this->hasMany(DistrictCattleInfo::class,'district_id');
    }
}
