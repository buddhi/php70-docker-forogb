<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable =['comment','commentable_id','commentable_type','user_id','comment_type'];
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->hasOne(User::class,'user_id','id');
    }

    public function getUsername($user_id)
    {
        return $this->hasOne(User::class,'user_id','id');
    }

    public function getCommenterNameAttribute()
    {
        if(isset($this->user_id)){
            $user = User::findOrFail($this->user_id);
            return $user->full_name;
        }
        return null;
    }


}
