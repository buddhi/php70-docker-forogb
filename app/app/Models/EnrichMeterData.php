<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnrichMeterData extends Model
{
    protected $table='enrich_meter_datas';
    
    protected $fillable = ['filename','current','voltage','temperature','humidity','soil_moisture','meter_id','dc_power','power_percent','discharge','ac_power','frequency','rpm','runtime','created_at'];
    
    protected $hidden = ['id'];
    
}
