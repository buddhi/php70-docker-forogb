<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictCropRisk extends Model
{
    public $timestamps = false;
    protected $table='crop_district_risk';
    protected $fillable =['crop_id','district_id','risk'];

}
