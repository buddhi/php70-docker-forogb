<?php


namespace App\Utilities;


class EmailParam
{

    public $from,$to,$ccUsers,$subject,$title,$message;

    public function __construct($from,$to,$ccUsers = null,$subject = null,$title = null,$message = null)
    {
        $this->from = $from;
        $this->to = $to;
        $this->ccUsers = $ccUsers;
        $this->subject = $subject;
        $this->title = $title;
        $this->message = $message;
    }
}