<?php


namespace App\Utilities;


class NotificationAttribute
{
    public $channel, $to, $type,$parameters;
    public function __construct($channel, $to, $type, $parameters=null)
    {
        $this->channel = $channel;
        $this->to = $to;
        $this->type = $type;
        $this->parameters = $parameters;
    }
}