<?php

namespace App\Utilities;


use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class StringManipulator
{
    public static function makeReplacements($line, array $replace)
    {
        if (empty($replace) || is_null($replace)) {
            return $line;
        }
        $replace = self::sortReplacements($replace);
        foreach ($replace as $key => $value) {
            $line = str_replace(
                [':' . $key, ':' . Str::upper($key), ':' . Str::ucfirst($key)],

                [$value, Str::upper($value), Str::ucfirst($value)],
                $line
            );
        }
        return $line;
    }


    public static function sortReplacements(array $replace)
    {
        return (new Collection($replace))->sortBy(function ($value, $key) {
            return mb_strlen($key) * -1;
        })->all();
    }

    public static function getFirstTwoWords($line)
    {
        if (is_null($line) || strlen($line) == 0) {
            return $line;
        }
        $words = explode(' ', $line, 3);
        if (isset($words[0]) && isset($words[1]))
            return $words[0] . " " . $words[1];
        if (isset($words[0]))
            return $words[0];
        return $line;
    }
}