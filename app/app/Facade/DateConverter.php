<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

class DateConverter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'DateConverter';
    }
}