<?php

namespace App\Policies;

use App\Enums\Role;
use App\Models\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RemarksPolicy
{
    use HandlesAuthorization;


    public function index(User $user, Project $project)
    {
        return self::checkIfProjectBelongToThePartner($project);
    }

    public function create(Project $project)
    {
        return self::checkIfProjectBelongToThePartner($project);
    }

    private static function checkIfProjectBelongToThePartner($project)
    {
        //Admin can make remarks in any project
        if (session('role') == Role::ADMIN) {
            return true;
        }
        if ((!isset(auth()->user()->partner_id))) {
            return false;
        };
        $partner_id = auth()->user()->partner_id;
        $projects = Project::with('user')->whereHas('user', function ($q) use ($partner_id) {
            $q->where('partner_id', '=', $partner_id);
        })->pluck('id')->toArray();
        if (in_array($project->id, $projects)) {
            return true;
        }
        return false;
    }
}
