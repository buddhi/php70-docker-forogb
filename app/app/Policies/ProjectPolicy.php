<?php

namespace App\Policies;

use App\Models\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Enums\Role;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        if(session('role') == Role::DEVELOPER)
            return true;
        return false;
    }

    public function store(User $user)
    {
        if(session('role')==Role::DEVELOPER)
            return true;
        return false;
    }

    public function edit(User $user,Project $project)
    {
        if(session('role') == Role::ADMIN)
            return true;


        if($user->user_id != $project->created_by)
            return false;
    
        if(strtolower($project->status) == 'new')
            return false;
        
        if(session('role')==Role::DEVELOPER)
            return true;

        return false;
    }

    public function update(User $user, Project $project)
    {
        if($user->user_id!= $project->created_by)
            return false;

        if(session('role')==Role::DEVELOPER)
            return true;
        return false;
    }

    public function selectPlan(User $user, Project $project)
    {
        if(strtolower($project->status) == 'new'||strtolower($project->status) == 'operational'||strtolower($project->status) == 'funded')
            return false;

        if($user->user_id != $project->created_by)
            return false;

        if(session('role')!=Role::DEVELOPER )
            return false;
        return true;
    }

    public function selectPlanStore(User $user, Project $project)
    {
        if($user->user_id != $project->created_by)
            return false;

        if(session('role')!=Role::DEVELOPER)
            return false;
        
        return true;
    }
}

