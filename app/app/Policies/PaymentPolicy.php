<?php

namespace App\Policies;

use App\Models\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Enums\Role;

class PaymentPolicy
{
    use HandlesAuthorization;


    public function view(User $user)
    {
        if(session('role') == Role::ADMIN  )
            return true;
        return false;
//        ||session('role') == Role::PARTNER_USER just in case required later to show investment in sidebar for partner user
    }


}

