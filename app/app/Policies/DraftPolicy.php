<?php

namespace App\Policies;

use App\Models\DraftProject;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Enums\Role;

class DraftPolicy
{
    use HandlesAuthorization;
    
    public function edit(User $user,DraftProject $draftProject)
    {
        if($user->user_id != $draftProject->user_id)
            return false;
        if(session('role')!=Role::DEVELOPER)
            return false;
        return true;
    }
}
