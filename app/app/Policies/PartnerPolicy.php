<?php

namespace App\Policies;

use App\Models\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Enums\Role;

class PartnerPolicy
{
    use HandlesAuthorization;


    public function view(User $user)
    {
        if(session('role') == Role::ADMIN  )
            return true;
        return false;
    }


}

