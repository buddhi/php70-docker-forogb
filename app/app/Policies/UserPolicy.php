<?php

namespace App\Policies;

use App\Models\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Enums\Role;

class UserPolicy
{
    use HandlesAuthorization;


    public function view(User $user)
    {
        if(session('role') == Role::ADMIN ||session('role') == Role::PARTNER_USER )
            return true;
        return false;
    }


}

