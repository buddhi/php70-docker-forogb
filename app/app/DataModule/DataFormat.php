<?php

namespace App\DataModule;

class DataFormat
{
   public $loan_account_number;
   public $payment_made;
   public $payment_date;
   public $due_amount;
}