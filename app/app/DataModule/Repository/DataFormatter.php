<?php

namespace App\DataModule\Repository;

use App\DataModule\DataFormat;

class DataFormatter implements DataIngestion
{

    public  function laxmiBankData()
    {
        $formattedData[] = null;
        // TODO: Implement LaxmiBankData() method.
        $row = 0;
        $count  = 0;
        if (($handle = fopen(storage_path('data_laxmi.csv'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
               $dataFormat = new DataFormat();
//             Gets the data from storage file located on storage/data_laxmi.csv
//             Parse the CSV data
//             logs the value of due amount with account number on console
                if ($row > 0) { //Ignoring the first row of csv which contains the information
                   $dataFormat->loan_account_number = $data[0];
                   $dataFormat->payment_made = $data[1];
                   $dataFormat->payment_date = $data[2];
                   $dataFormat->due_amount = $data[3];
                   $formattedData[$count++] = $dataFormat;
                }
                $row++;
            }
            fclose($handle);
        }
        return $formattedData;
    }
}