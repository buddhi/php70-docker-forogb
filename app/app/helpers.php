<?php

use App\OGB\Constants;
use Illuminate\Support\Facades\Cache;

if (!function_exists('include_route_files')) {
    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);
            while ($it->valid()) {
                if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }
                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}

function displayUnitFormat($field, $value = null)
{
    $systemUnits = config('Unit.units');
    if (!isset($value)) {
        return (array_key_exists($field, $systemUnits)) ? $systemUnits[$field] : "";
    }

    if (!array_key_exists($field, $systemUnits)) {
        return $value;
    }
    switch ($field) {
        case "amount":
            return $systemUnits[$field] . " " . number_format($value, 0);
        case "percent":
            return round($value, 2) . $systemUnits[$field];
        default:
            return number_format($value, 0) . " " . str_plural($systemUnits[$field], $value);
    }
}

function currencyConverter($value, $currency)
{

    // We do not want application to shut down if the scrapper fails(Extremely rare case).
    //  So default Value is set.
    //  110 is tentative average selling rate with deflation 15 percent giving 93.5.
    $approximateConversionRatio = 1 / 93.5;

    $rate = \App\Models\CurrencyRate::where('currency', '=', $currency)->latest()->first();
    if ($rate == null) {
        return $value * $approximateConversionRatio;
    }
    $conversionRatio = 1 / $rate->selling_rate;
    return $value * $conversionRatio;
}

//TODO FUTURE::Make only one currency converter helper. This one is fixed for now
function currencyConverterFactor($currency){
    switch ($currency) {
        case "RS" :
            return 1;
        case "USD" :
            return 100;
        default:
            return 1;
    }
}

function returnRole($role)
{
    switch ($role) {
        case 1 :
            return 'developer';
            break;
        case 10 :
            return 'investor';
            break;
        case 50 :
            return 'admin';
            break;

    }
}

function investmentType($value)
{
    switch ($value) {
        case 1 :
            return 'Equity';
            break;
        case 2 :
            return 'Debt';
            break;
        case 3 :
            return 'Grant';
            break;
    }
}


function currentIrrigationSource($value)
{
    switch ($value) {
        case 1 :
            return 'Pond';
            break;
        case 2 :
            return 'Rain';
            break;
        case 3 :
            return 'River';
            break;
        case 4 :
            return 'Boring';
            break;
        case 5 :
            return 'Well';
            break;
    }

}

function currentIrrigationSystem($value)
{
    switch ($value) {
        case 1 :
            return 'Diesel';
            break;
        case 2 :
            return 'Electeric';
            break;
        case 3 :
            return 'Canal';
            break;
    }

}

if (!function_exists('syncData')) {

    /**
     * @param $array: Array of ids of model
     * @param $pivot_column_value: value of pivot model
     * @return array
     */
    function syncData($array, $pivot_column_value)
    {
        $sync_data = [];
        foreach ($array as $item) {
            $sync_data[$item] = ['value' => $pivot_column_value];
        }
        return $sync_data;
    }
}

if (!function_exists('getTransitionStates')) {

    /**
     * @param null $filterByStatus
     * @param null $sortOnly :Filtering on the basis of certain states
     * @return array
     */
    function getTransitionStates($filterByStatus = null, $sortOnly = null)
    {
        if(!\Illuminate\Support\Facades\Cache::has('transition_states')){
            Cache::forever('transition_states', \App\Models\TransitionState::all()->toArray());
        };
        $transition_states = Cache::get('transition_states');
        if ((!isset($filterByStatus)) && (!isset($sortOnly)))
            return $transition_states;

        if (isset($filterByStatus) && (!isset($sortOnly)))
            return array_filter($transition_states, function ($transition_state) use ($filterByStatus) {
                return ($transition_state['status'] == $filterByStatus);
            });

        if (isset($filterByStatus) && (isset($sortOnly))) {
            $filteredArray = array_filter($transition_states, function ($transition_state) use ($filterByStatus) {
                return ($transition_state['status'] == $filterByStatus);
            });
            return array_column($filteredArray, $sortOnly);
        }

        if ((!isset($filterByStatus)) && (isset($sortOnly))) {
            return array_column($transition_states, $sortOnly);
        }
        return [];
    }
}


//Todo::future sync backend  and frontend for calculating this function
if (!function_exists('calculateEnvParams')) {
    function calculateEnvParams($systemSize)
    {
        $systemSizeInKw = $systemSize / 1000; //unit:Kw
        $dailyEnergyProduction = $systemSizeInKw * Constants::AVERAGE_DAILY_SUN_HOURS; // unit:kWh
        $annualEnergyProduction = $dailyEnergyProduction * Constants::NO_OF_DAYS_IN_YEAR; // kWh
        $annualEnergyUsage = $annualEnergyProduction * Constants::SYSTEM_UTILIZATION_RATE / 100; // kWh
        $dieselDisplacedInL = $annualEnergyUsage * Constants::DIESEL_REQUIRED_PER_KWH; // Liters / kWh
        $dieselDisplaced = $dieselDisplacedInL / 1000;
        $co2CurbedInKg = $dieselDisplacedInL * Constants::CO2_EMISSION_PER_LITER_DIESEL_CONSUMPTION;
        $co2Curbed = $co2CurbedInKg / 1000; // unit :ton , 1KG =1/1000 ton
        return ['diesel_displaced' => $dieselDisplaced, 'co2_curbed' => $co2Curbed];
    }

}

if (!function_exists('creditScoreToBankability')) {
    function creditScoreToBankability($creditScore){
        return 6.25 - ($creditScore * 5);
    }
}


function getAttributeForModel($modelName,$unique_id,$attribute){

        try{
            if (!is_subclass_of($modelName, 'Illuminate\Database\Eloquent\Model')) {
                throw new \Illuminate\Database\Eloquent\ModelNotFoundException();
            }
            $instance = $modelName::findOrFail($unique_id);
            return $instance->{$attribute};
        }catch (Exception $exception){
            return null;
        }
}

//dd months and get the nearest date in BS format
function BS_addMonth($date,$month_to_added = null){
    return \App\Facade\DateConverter::addMonth($date,$month_to_added);
}

// credit score - between 0 and 1
// 1 is risky and 0 is not risky
// bankability score - 1 -> 5
function creditScoreToBankability ($creditScore) {
    return 6.25 - ($creditScore * 5);
  }

function responseMessage($message,$error_status,$status_code)
{
    return response()->json([
        'error' => $error_status,
        'messages' => $message,
    ], $status_code);
}