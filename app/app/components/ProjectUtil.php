<?php

namespace App\components;

use Illuminate\Support\Facades\DB;

class ProjectUtil
{
    public static function getProjectInvestmentTotal($project_id)
    {

        $values = DB::table('tbl_project_investment AS pi')
            ->select(DB::raw('IFNULL(SUM(invest_amount),0) as investment_amount'),
                DB::raw('IFNULL(SUM(investment_percent),0) as investment_percent'))
            ->join('tbl_project AS p', 'p.id', '=', 'pi.project_id')
            ->where('p.id', $project_id)
            ->where('p.delete_flg', '=', 0)
            ->where('pi.delete_flg', '=', 0)
            ->first();
        $values = json_decode(json_encode($values), true);
        return $values;

    }

    public static function getProjectInvestmentDetail($project_id)
    {
        $values = DB::table('tbl_project_investment AS pi')
            ->select(DB::raw('IFNULL(SUM(investment_percent),0) as investment_percent'),
                'investment_type_id')
            ->join('tbl_project AS p', 'p.id', '=', 'pi.project_id')
            ->where('p.id', $project_id)
            ->where('p.delete_flg', '=', 0)
            ->where('pi.delete_flg', '=', 0)
            ->groupBy('investment_type_id')
            ->get();
        $result = json_decode(json_encode($values), true);

        $investmentDetail = array(
            'equity' => 0,
            'debt' => 0,
            'grant' => 0,
        );
        foreach ($result as $row) {
            if ($row['investment_type_id'] == 1) {
                $investmentDetail['equity'] = $row['investment_percent'];
            } else {
                if ($row['investment_type_id'] == 2) {
                    $investmentDetail['debt'] = $row['investment_percent'];
                } else {
                    if ($row['investment_type_id'] == 3) {
                        $investmentDetail['grant'] = $row['investment_percent'];
                    }
                }
            }
        }
        return $investmentDetail;
    }
}