<?php

namespace App\components;

abstract  class CurrentIrrigationSource
{
    const Pond = 1;
    const Rain= 2;
    const River = 3;
    const Boring= 4;
    const Well = 5;
}

