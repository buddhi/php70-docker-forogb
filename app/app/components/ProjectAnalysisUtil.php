<?php

use Illuminate\Support\Facades\DB;

class ProjectAnalysisUtil
{
    public static $_PLAN_1 = 180000;
    public static $_PLAN_2 = 250000;
    public static $_PLAN_3 = 300000;
    public static $_PLAN_4 = 350000;

    public static function analyzeWaterRequirement($project_id,$district_name){


        $sql = "DELETE FROM tbl_project_analysis
                WHERE project_id = {$project_id}";

        DB::delete($sql);


//        $cmd = Yii::app()->db->createCommand($sql);
//        $cmd->execute();

        $sql = "INSERT INTO tbl_project_analysis(project_id, water_requirement, month_id)
                SELECT
                    {$project_id} project_id, 
                    COALESCE(SUM(water_requirement),0) as water_requirement,
                    month_id

                FROM (
                    SELECT v.project_id,
                        ROUND(((((((evapotrans*30*COALESCE(f.crop_factor,0))*(c.area/260)-r.rainfall*(c.area/260))/0.5)*24)/5)*3600)*5,0) as water_requirement,
                        v.month_id
                    FROM tbl_crop_rank v
                    LEFT JOIN (SELECT * FROM tbl_master_rain WHERE district='{$district_name}') r
                        ON v.month_id=r.month_id
                    LEFT JOIN (SELECT * FROM tbl_master_evapotranspiration WHERE district='{$district_name}') e
                        ON v.month_id=e.month_id
                    LEFT JOIN tbl_crop_factor f ON v.crop_id=f.id AND v.rank=f.rank
                    LEFT JOIN tbl_crop_info c ON v.project_id=c.project_id AND v.crop_id=c.crop_id
                    WHERE v.project_id={$project_id}
                ) A
                GROUP BY month_id";

//                var_dump($sql);
//                die;
         DB::insert($sql);

    }

    public static function getWaterAnalysisChartData($project_id){
        $sql = "SELECT water_requirement, month_id
                FROM tbl_project_analysis
                WHERE project_id = {$project_id}
                ORDER BY month_id";
//        $cmd = Yii::app()->db->createCommand($sql);
//        $res = $cmd->queryAll();
        DB::select($sql);

        $waterDischargeArr = array();
        $max_water_requirement = 0;
        for($i=0;$i<12;$i++){
            $waterDischargeArr[$i] = floatval(0);
        }
        foreach ($res as $row) {
            $idx = $row['month_id'] - 1;
            $water_requirement = floatval($row['water_requirement']);
            $waterDischargeArr[$idx] = floatval($row['water_requirement']);

            if($water_requirement > $max_water_requirement){
                $max_water_requirement = $water_requirement;
            }

        }
        $count = self::getMaxCount($max_water_requirement);
        $selectedPlan = self::getChartPlans($count,$max_water_requirement);
        $selectedPlan[0]['series'] = array();
        $selectedPlan[1]['series'] = array();
        for($i = 0; $i<12;$i++){
            $selectedPlan[0]['series'][] = intval($selectedPlan[0]['plan_cost']);
            $selectedPlan[1]['series'][] = intval($selectedPlan[1]['plan_cost']);
        }
        $suggestedPlan = self::getSuggestedPlan($count);
        $res = array('selectedPlan' => $selectedPlan,
                    'suggestedPlan' => $suggestedPlan,
                    'waterDischargeArr' => $waterDischargeArr);
        return $res;
    }

    private static function getMaxCount($max_water_requirement){
        $sql = "SELECT count(*)
                FROM tbl_plan
                WHERE daily_discharge >= '{$max_water_requirement}'";
        $cmd = Yii::app()->db->createCommand($sql);
        // echo $sql;exit;
        $count  = $cmd->queryScalar();
        return $count;
    }

    private static function getChartPlans($count,$max_water_requirement){
        $sql_where = "";
        $order = " DESC ";
        if($count == 0){
            $sql_where = "daily_discharge <= '{$max_water_requirement}'";
        }
        else{
            $sql_where = "daily_discharge >= '{$max_water_requirement}'";
            if($count > 2)
                $order = " ASC ";
        }
        $sql = "SELECT name, daily_discharge, plan_cost
            FROM tbl_plan
            WHERE {$sql_where}
            ORDER BY id {$order}
            LIMIT 2";
        $cmd = Yii::app()->db->createCommand($sql);
        return $cmd->queryAll();
    }

    private static function getSuggestedPlan($count){
        if($count == 4)
            $id = 1;
        else if($count == 3)
            $id = 3;
        else
            $id = 4;
        $sql = "SELECT name, daily_discharge
                FROM tbl_plan
                WHERE id = {$id}";
        $cmd = Yii::app()->db->createCommand($sql);
        return $cmd->queryRow();
    }


}