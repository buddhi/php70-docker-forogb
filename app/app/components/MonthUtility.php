<?php
/*
    Mail Utility Class for
     Manage Mail Functions
    pb@mi 2016-04-06
 */

namespace App\components;
class MonthUtility
{
    public static $_MONTHS = array(
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'May',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Aug',
        9 => 'Sep',
        10 => 'Oct',
        11 => 'Nov',
        12 => 'Dec'
    );
}
