<?php

namespace App;

use App\Enums\Role;
use App\Enums\UserStatus;
use App\Filters\User\UserFilter;
use App\Models\Otp;
use App\Models\Partner;
use App\Models\Project;
use App\Models\ProjectInvestment;
use App\Models\RoleUser;
use App\Models\VerifyUser;
use App\Notifications\PasswordReset;
use App\OGB\Constants;
use App\Scope\UserScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table ='app_user';
    protected $primaryKey ='user_id';
    protected $fillable = [
        'name', 'email', 'password','username','user_id','phone','full_name','role','language','company_id','user_agent','ip','last_login_time','last_access_count','delete_flg','created_by'
       ,'updated_at' ,'created_on','updated_on','updated_by','investment_type','address','is_locked','text_password','admin_verification','verified','provider','provider_id',
        'profile_image','anonymous','country_code','partner_id','landline','disabled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class,'created_by');
    }

    public function roles()
    {
        return $this->hasMany(RoleUser::class,"user_id","user_id");
    }

    public function verifyusers()
    {
        return $this->hasOne(VerifyUser::class,"user_id",'user_id');
    }

    public function isStatusChangeAble($email)
    {
        if ($email == auth()->user()->email)
            return false;
        //TODO::FUTURE root admin email needs to be fixed or saved some somewhere or kept root@admin.com.
        if($email == 'root@admin.com')
            return false;
        if(session('role') == Role::PARTNER_USER)
            return true;
        if(session('role') != Role::ADMIN)
            return false;
        return true;
    }


    //TODO Future:: Make common scope using traits or BaseModel(inheritance)
    public function scopeInActive($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query->where('delete_flg', 1);
    }

    public function scopeActive($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query->where('delete_flg', 0);
    }

    public function scopegetAllUsers($query)
    {
        $query = $query->withoutGlobalScopes();
        return $query;
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UserScope());
    }

    public function isUserUpdateAble($email)
    {
        if($email == 'root@admin.com')
            return false;
        return true;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    public function projectinvestments()
    {
        return $this->hasMany(ProjectInvestment::class,'investor_id');

    }

    public function hasRole($role)
    {
        $roles = $this->roles->pluck('role')->toArray();
        if(in_array($role,$roles,true)){
            return true;
        }
        return false;
    }

    public function scopeFilter(Builder $builder,$request)
    {
        return (new UserFilter($request))->filter();
    }

    public function otps()
    {
        return $this->hasMany(Otp::class,'user_id','user_id');
    }

    public function tokens()
    {
        return $this->morphOne('App\Models\Token', 'tokenable');
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id');
    }

    public function getStatusAttribute()
    {
        if ($this->disabled) {
            return UserStatus::DEACTIVATED;
        }
        if (!$this->verified) {
            return UserStatus::VERIFICATION_PENDING;
        }
        if (!$this->otp_verified) {
            return UserStatus::VERIFIED;
        }
        if ($this->verified && $this->otp_verified) {
            return UserStatus::ACTIVATED;
        }
    }

    public function isUserSoftDeleted()
    {
        if($this->delete_flg == Constants::DELETE){
            return true;
        }
        return false;
    }

    public function isUserDisabled()
    {
        if($this->disabled == Constants::DELETE){
            return true;
        }
        return false;
    }
}
