<?php


namespace App\Services;

class CropArea
{

    public static function convertCropAreaToUnit($crop, $unit = null)
    {
        $land_area_unit = $crop->pivot->crop_area_unit_type;
        switch ($land_area_unit) {
            case 'bigaha':
                return \App\Facade\Conversion::convert($crop->pivot->area, $land_area_unit)->to($unit)->value;
                break;
            case 'ropani':
                return \App\Facade\Conversion::convert($crop->pivot->area, $land_area_unit)->to($unit)->value;
                break;
            case 'katha':
                return \App\Facade\Conversion::convert($crop->pivot->area, $land_area_unit)->to($unit)->value;
                break;
            default:
                return \App\Facade\Conversion::convert($crop->pivot->area, 'hectare')->to($unit)->value;
                break;
        }
    }

    public static function convertLandAreaToUnit($land, $unit = null)
    {
        $land_area_unit = $land['unit_type'];
        switch ($land_area_unit) {
            case 'bigaha':
                return \App\Facade\Conversion::convert($land['area'], $land_area_unit)->to($unit)->value;
                break;
            case 'ropani':
                return \App\Facade\Conversion::convert($land['area'], $land_area_unit)->to($unit)->value;
                break;
            case 'katha':
                return \App\Facade\Conversion::convert($land['area'], $land_area_unit)->to($unit)->value;
                break;
            default:
                return \App\Facade\Conversion::convert($land['area'], 'hectare')->to($unit)->value;
                break;
        }
    }
}