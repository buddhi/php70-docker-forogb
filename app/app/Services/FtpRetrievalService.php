<?php

namespace App\Services;

use App\Enums\FtpFileLocation;
use App\Models\Meter;
use App\Models\MeterData;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MeterFileAttribute
{
    public $file_name;
    public $timestamp;
    public $phoneNo;
    public $file_type;
}

abstract class MeterFileType
{
    const TIMESTAMP_FILE = 'normal';//filename ==> 'phoneNo_timestamp.txt',file with single meter data
    const TIME_DURATION_FILE = 'er';//filename ==> 'phoneNo_timestamp_err.txt',file with lots of meter data
}

class FtpRetrievalService
{
    private $log, $ftp;
    private static $FTP_DATA_LOCATION = FtpFileLocation::FTP_DATA_LOCATION;
    private static $FTP_BACKUP_DATA_LOCATION = FtpFileLocation::FTP_BACKUP_DATA_LOCATION;
    public function __construct()
    {
        $this->log = new Logger('FTP');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/ftp.log')));
    }

    //Retrieve ftp data and store in meter_data table
    public function retrieve()
    {
        //Connecting with ftp
        $this->ftp = ftp(env('FTP_HOST'), env('FTP_USER'), env('FTP_PASSWORD'), env('FTP_PORT'));
        if (!$this->ftp) {
            return 'Error while connecting to the FTP server';
        }

        //Getting files from remote ftp
        $files = $this->ftp->all(self::$FTP_DATA_LOCATION);
        if (count($files) == '0%') {
            return 0;
        }
        $phone_number_with_no_meter = null;
        foreach ($files as $file) {

            //Fetching the file attribute and meter Files in wrong format is rejected
            $meterFileAttribute = $this->getMeterFileAttribute($file);
            if (!$meterFileAttribute) {
                continue;
            }
            //Phone Number which is not associated with any meter is rejected
            if ($meterFileAttribute->phoneNo == $phone_number_with_no_meter) {
                continue;
            }
            $meter = Meter::select('id')->where('phoneNo', '=', $meterFileAttribute->phoneNo)->first();
            if ($meter == null) {
                $this->log->info("No meter with phone number " . $meterFileAttribute->phoneNo . " found");
                $phone_number_with_no_meter = $meterFileAttribute->phoneNo;
                continue;
            }

            //Empty meter file is rejected
            if ($this->ftp->size($file) <= 0) {
                echo 'Ignoring ' . $meterFileAttribute->file_name . ' The File is empty. Moving the ' . $meterFileAttribute->file_name . ' to'.self::$FTP_BACKUP_DATA_LOCATION;
                $this->log->error('Ignoring ' . $meterFileAttribute->file_name . ' The File is empty. Moving the ' . $meterFileAttribute->file_name . ' to'.self::$FTP_BACKUP_DATA_LOCATION.PHP_EOL);
                $this->backUpFiles($meterFileAttribute->file_name, $file);
                continue;
            }
            $this->processMeterFileAccordingToType($meterFileAttribute, $file, $meter);
        }

    }

    private function processMeterFileAccordingToType(MeterFileAttribute $meterFileAttribute, $file, $meter)
    {
        switch ($meterFileAttribute->file_type) {
            case MeterFileType::TIME_DURATION_FILE:
                $this->processTimeDurationFile($meterFileAttribute, $file, $meter);
                return;
            case MeterFileType::TIMESTAMP_FILE:
                $this->processTimeStampFile($meterFileAttribute, $file, $meter);
                return;
            //add another file type to be processed
            default:
                return;
        }

    }

    private function processTimeStampFile(MeterFileAttribute $meterFileAttribute, $file, $meter)
    {
        $file_data = $this->ftp->get($file);
        $new_data = explode(PHP_EOL, $file_data);
        if (count($new_data) > 5) {
            $invalidData = count($new_data) - 5;
            $this->log->error($meterFileAttribute->file_name . ' has ' . $invalidData . ' invalid data');
        } else if (count($new_data) < 5) {
            $this->log->error($meterFileAttribute->file_name . ' has less data');
            return false;
        }
        $meterData = new MeterData();
        $meterData->filename = $meterFileAttribute->file_name;
        $meterData->meter_id = $meter->id;
        $meterData->current = abs($new_data[0]);
        $meterData->voltage = $new_data[1];
        $meterData->temperature = $new_data[2];
        $meterData->humidity = $new_data[3];
        $meterData->soil_moisture = $new_data[4];
        if (strlen($meterFileAttribute->timestamp) <= 8) {
            $meterFileAttribute->timestamp = Carbon::now()->format('y') . $meterFileAttribute->timestamp;
        }
        $date = DateTime::createFromFormat('ymdHi', $meterFileAttribute->timestamp);
        $meterData->created_at = $date;
        $this->backUpFiles($meterFileAttribute->file_name, $file, $file_data);
        try {
            $meterData->save();
            $this->log->info($meterFileAttribute->file_name . " saved in the database");
        } catch (\Exception $e) {
            $this->log->error("Cannot write to the database");
        }
    }

    //Moves the file from Files to OldFiles
    //Moved to save the server space
    //It is deleted for now to save the server space.
    private function backUpFiles($file_name, $file, $file_data = null)
    {
        $back_up_file_name = $file_name . '.txt';
        echo $back_up_file_name . ' created.' . PHP_EOL;
        $file_data == null ? $this->ftp->save(self::$FTP_BACKUP_DATA_LOCATION . DIRECTORY_SEPARATOR . $back_up_file_name, '') : $this->ftp->save(self::$FTP_BACKUP_DATA_LOCATION . DIRECTORY_SEPARATOR . $back_up_file_name, $file_data);
        echo $back_up_file_name . ' populated at ' . self::$FTP_BACKUP_DATA_LOCATION . PHP_EOL;
        $this->log->info($back_up_file_name . ' populated at ' . Self::$FTP_BACKUP_DATA_LOCATION);
        $this->ftp->deleteFile($file);
        echo $back_up_file_name . ' is deleted' . PHP_EOL;
    }


    private function processTimeDurationFile(MeterFileAttribute $meterFileAttribute, $file, $meter)
    {
        $file_data = $this->ftp->get($file);
        $meter_data_collection = self::csvToArray($meterFileAttribute->phoneNo, $file_data, $meter);
        if (count($meter_data_collection) > 0) {
            foreach ($meter_data_collection as $meter_data_array){
                try {
                    MeterData::create($meter_data_array);
                    $this->log->info($meter_data_array['filename'] . " saved in the database");
                } catch (\Exception $e) {
                    $this->log->error("Cannot write to the database ".$e->getMessage());
                }
            }
        }
        $this->backUpFiles($meterFileAttribute->file_name, $file, $file_data);
        return;
    }


    //checks if the meter files has valid attributes
    private function getMeterFileAttribute($file)
    {
        $meterFileAttribute = new MeterFileAttribute();
        try {
            $file_name = explode('/', $file);            //result ==> ['Files','phoneNo_timestamp.txt']
            $file_name = explode('.', $file_name[1]);    //result ==> ['phoneNo_timestamp','txt']
        } catch (\Exception $exception) {
            $this->log->error("Invalid File name");
            return false;
        }
        try {
            $meterFileAttribute->file_name = $file_name[0];    //actually, file_name = 'phoneNo_timestamp' ie without extension
            $data = explode('_', $file_name[0]);         //result ==> ['phoneNo','timestamp'] or  ['phoneNo','timestamp','err']
            $meterFileAttribute->phoneNo = $data[0];
            $meterFileAttribute->timestamp = $data[1];
            $meterFileAttribute->file_type = isset($data[2]) &&  strtolower($data[2]) == MeterFileType::TIME_DURATION_FILE ? MeterFileType::TIME_DURATION_FILE : MeterFileType::TIMESTAMP_FILE;
        } catch (\Exception $e) {
            $this->log->error("Invalid File name");
            $this->backUpFiles($file_name, $file);
            return false;
        }
        return $meterFileAttribute;
    }


    private static function csvToArray($phoneNo, $file_data, $meter)
    {
        $data_in_csv_format = explode(PHP_EOL, $file_data);
        $delimiter = ',';
        $meter_data_collection = [];
        foreach ($data_in_csv_format as $row_value) {
            if (strlen($row_value) > 0 && is_array($row = explode($delimiter, $row_value))) {
                if (count($row) < 5) {
                    continue;
                }
                $timestamp = $row[0];
                if (strlen($timestamp) <= 8) {
                    $timestamp = Carbon::now()->format('y') . $timestamp;
                }
                $date = DateTime::createFromFormat('ymdHi', $timestamp);
                $meter_data_collection[] = [
                    'meter_id' => $meter->id,
                    'filename' => $phoneNo . '_' . $timestamp,
                    'current' => abs($row[1]),
                    'voltage' => $row[2],
                    'temperature' => $row[3],
                    'humidity' => $row[4],
                    'soil_moisture' => isset($row[5]) ? $row[5] : null,
                    'created_at' => $date,
                ];
            }
        }
        return $meter_data_collection;
    }
}