<?php

namespace App\Services;


use App\Models\CurrencyRate;
use DOMDocument;
use DOMXPath;

class PriceScrapper
{
    public function scrape(){
        $sUrl = 'https://www.nrb.org.np/fxmexchangerate.php';
        $sUrlSrc = $this->getWebsiteContent($sUrl);
        $dom = new DOMDocument();
        @$dom->loadHTML($sUrlSrc);
        $xpath = new DomXPath($dom);
        $sellingRate = '';
        $vRes = $xpath->query("/html/body/table[1]/tr[3]/td[3]/table[2]/tr[2]/td/table/tr[3]/td[4]/div/font");
        foreach ($vRes as $obj) {
            $sellingRate = $obj->nodeValue;
        }
        $this->save($sellingRate);
    }
    function getWebsiteContent($sUrl) {
        $curl = curl_init($sUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10');
        $html = curl_exec($curl);
        curl_close($curl);
        return $html;
    }

    private function save($sellingRate)
    {
        $sellingRate = $sellingRate - (0.15 * $sellingRate); //Deflation rate is set to 15%
        $currency_rate = new CurrencyRate();
        $currency_rate->currency = 'USD';
        $currency_rate->selling_rate = $sellingRate;
        $currency_rate->save();
    }
}