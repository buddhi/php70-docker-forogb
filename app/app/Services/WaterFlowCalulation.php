<?php

namespace App\Services;

use App\Models\AverageMonthlyRainfall;
use App\Models\AverageMonthlyTemperature;
use App\Models\CroppingFactor;
use App\Models\CroppingFactorDay;
use App\Models\Project;
use Carbon\Carbon;

//Refer wiki for algorithm of water flow calculation
class WaterFlowCalulation
{
    const MONTHS = [1=>'jan',2=>'feb',3=>'mar',4=>'apr',5=>'may',6=>'jun',7=>'jul',8=>'aug',9=>'sep',10=>'oct',11=>'nov',12=>'dec'];
    const EVAPOTRANSPIRATION_COEEFICIENT = ['jan'=>0.24,'feb'=>0.25,'mar'=>0.27,'apr'=>0.29,'may'=>0.31,'jun'=>0.32,'jul'=>0.31,'aug'=>0.30,'sep'=>0.28,'oct'=>0.26,'nov'=>0.24,'dec'=>0.23];
    const Maximum_crop_id_with_stage_value = 30;
    public static function calculateFlow($project_id)
    {
        $month_array = ['jan' => 0,'feb'=>0,'mar'=>0,'apr'=>0,'may'=>0,'jun'=>0,'jul'=>0,'aug'=>0,'sep'=>0,'oct'=>0,'nov'=>0,'dec'=>0];
        $daily_discharge = [];
        $project = Project::where('id',$project_id)->with('crops')->first();
        $district = $project->district_id;
        $count = 0;
        $month_value = 1;
        foreach($project->crops as $crop){
            if($crop->id > self::Maximum_crop_id_with_stage_value){
                continue;
            }
            $crop_id = $crop->pivot->crop_id;
            $month_value = $crop->pivot->start_month;
            $crop_stage_day = CroppingFactorDay::where('crop_id',$crop_id)->first();
            $area = CropArea::convertCropAreaToUnit($crop, 'hectare');
            $day = self::convertCollectionToArray($crop_stage_day);
            $crop_stage_factor = CroppingFactor::where('crop_id',$crop_id)->first();
            $factor = self::convertCollectionToArray($crop_stage_factor);
            $factoring = self::createDayArray($day,$factor);
            $minimum_harvest_months = self::findMinimumHarvestMonths($day);
            $remaining_months_day = self::findRemainingDaysInNextMonth($day);
            $kc = self::getCroppingFactor($minimum_harvest_months,$factoring,$remaining_months_day);
            $daily_discharge[$count++] =  self::getDailyDischarge($kc,$area,$month_value,$district);
        }
        $net_daily_discharge = self::calculateNetDischarge($daily_discharge);
        $net_daily_discharge = self::fillToMonthArray($net_daily_discharge,$month_array);
        return $net_daily_discharge;
    }

    public static function convertCollectionToArray($collection)
    {
        $array = [];
        $array[0] = $collection->stage1;
        $array[1] = $collection->stage2;
        $array[2] = $collection->stage3;
        $array[3] = $collection->stage4;
        return $array;
    }

    public static function resolveMonth($month_value)
    {

        return self::MONTHS[$month_value];
    }

    public static function calculateEto($month_value,$district)
    {
        $month = self::resolveMonth($month_value);
        $temperature = self::getTemperature($month,$district);
        $selected_evapotranspiration_coefficient = self::EVAPOTRANSPIRATION_COEEFICIENT[$month];
        return $selected_evapotranspiration_coefficient * (0.46 * $temperature->temperature + 8);

    }

    public static function getTemperature($month,$district)
    {

        return AverageMonthlyTemperature::where('district_id',$district)->where('month','=',$month)->first();
    }

    public static function getRainFall($month_value,$district)
    {
        $month = self::MONTHS[$month_value];
        return AverageMonthlyRainfall::where('district_id',$district)->where('month','=',$month)->first();
    }

    public static function getEffectiveRainFall($month_value,$district)
    {
        $rainfall = self::getRainFall($month_value,$district);
        $rainfall = $rainfall->rainfall;
        $rainfall_constant = $rainfall > 75 ? 0.8 : 0.6;
        $effective_rainfall = $rainfall_constant * $rainfall - 10;
        $effective_rainfall = $effective_rainfall > 0 ? $effective_rainfall : 0;
        return $effective_rainfall;
    }

    public static function fillToMonthArray($net_daily_discharge,$month_array)
    {

        foreach($net_daily_discharge as $key=>$value){
            $month = self::MONTHS[($key)];
            $month_array[$month] = $value;
        }
        return $month_array;
    }

    public static function calculateNetDischarge($daily_discharge)
    {
        $count = count($daily_discharge) + 1;
        $array1 = (isset($daily_discharge[$count - 1]) ? $daily_discharge[$count-1] : []);
        $array2 = (isset($daily_discharge[$count - 2]) ? $daily_discharge[$count-2] : []);
        $count = $count - 2;
        $sums = array();
        while($count >= 0){
            foreach (array_keys($array1 + $array2) as $key) {
                    $sums[$key] =(isset($array1[$key]) ? $array1[$key] : 0)  + (isset($array2[$key]) ? $array2[$key] : 0);
            }
            $array1 = $sums;
            $array2 = (isset($daily_discharge[$count - 1]) ? $daily_discharge[$count - 1] : []);
            $count = $count - 1;
        }
        return $sums;
    }

    public static function createDayArray($day, $factor)
    {
        $factoring = [];
        $count = 0;
        for($i = 0;$i<count($day);$i++){
            for($j = 0;$j < $day[$i];$j++){
                $factoring[$count] = $factor[$i];
                $count++;
            }
        }
        return $factoring;

    }

    public static function getDailyDischarge($kc,$area,$month_value,$district)
    {

        $etav = [];
        $inc = [];
        $sin_pump = [];
        $daily_discharge = [];
        $week_days = 7;
        $day_hours = 5;
        for($i = 0;$i < count($kc);$i++){
            $eto = self::calculateEto(($month_value+$i)%12 == 0 ? 12 : ($month_value+$i)%12,$district);
            $effective_rainfall = self::getEffectiveRainFall(($month_value+$i)%12 == 0 ? 12 : ($month_value+$i)%12,$district);
            $etav[$i] = $eto * $kc[$i] * 30;
            $inc[$i] = $etav[$i] - $effective_rainfall;
            $sin = ($inc[$i] * $area)/260;
            $sin_gross = ($sin / 50) * 100;
            $sin_pump[$i] = $sin_gross * (7/$week_days) * (24/$day_hours);
            $daily_discharge[($month_value+$i)%12 == 0 ? 12 : ($month_value+$i)%12] = $sin_pump[$i] * 3600 * 5;
        }
        return $daily_discharge;
    }

    public static function findMinimumHarvestMonths($day)
    {
        return floor(array_sum($day) / 30);
    }

    public static function findRemainingDaysInNextMonth($day)
    {
        return (array_sum($day) - (floor(array_sum($day)/30)) * 30);
    }

    public static function getCroppingFactor($minimum_harvest_months, $factoring, $remaining_months_day)
    {
        $kc = [];
        for($i=0;$i<$minimum_harvest_months;$i++){
            $sum = 0;
            for($j = (30 * $i); $j < (30 * ($i+1));$j++){
                $sum = $sum + $factoring[$j];
            }
            $kc[$i] = ($sum/30);
        }
        if($remaining_months_day > 0){
            $sum = 0;
            for($i = 30 * $minimum_harvest_months;$i < count($factoring); $i++){
                $sum = $sum + $factoring[$i];
            }
            $kc[count($kc)] = ($sum/30);
        }
        return $kc;
    }
}