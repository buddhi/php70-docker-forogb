<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use App\Models\Farmer;
use App\Models\Risk;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class CreditScore
{
    public $farmer_array,$project_array,$farm_array;
    private $log;

    public function __construct()
    {
        $this->log = new Logger('Credit Score');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/creditScore.log')));
    }

    public function creditScore($farmer_id)
    {
        $credit_score = null;
        //TODO::Enable cache at the end or in production later
//        if (!Cache::has('risk_collection')) {
//            $risk_collection = Risk::with('attributes.categories')->get();
//            Cache::forever('risk_collection', $risk_collection);
//        } else {
//            $risk_collection = Cache::get('risk_collection');
//        }
        $risk_collection = Risk::with('attributes.categories')->get();
        $risk_input = $this->riskInput($farmer_id);
        try{
            if(isset($risk_input)){
                try{
                    $credit_score = $this->totalRisk($risk_collection,$risk_input);
                }catch(\Exception $e){
                    throw new \Exception($e);
                }

            }else{
                throw new \Exception("No risk input is given");
            }
        }
        catch(\Exception $e){
            $this->log->info($e->getMessage().'Error mainly due  exception divide by zero on line number '.$e->getLine()." thus ,returning null");
            $credit_score = null;
        }
        return $credit_score;
    }

    //Calculates the total risks
    public function totalRisk($risk_collection,$risk_input)
    {
        $total_risk=$total_risk_weight = 0;
        $total_risk_component_array =[];
        if (!isset($risk_collection) || $risk_collection->isEmpty()){
            throw new \Exception("Risk collection is empty");
        }
        if(empty($risk_input)){
            throw new \Exception("Risk input is empty");
        }

        foreach ($risk_collection as $risk) {
            $component_risk_details = $this->riskComponent($risk,$risk_input) ;
            $component= $component_risk_details['component_risk']* $risk->risk_weight_factor;
            $risk_component_array[$risk->name]['component_risk'] = $component;
            $risk_component_array[$risk->name] = array_merge($risk_component_array[$risk->name],$component_risk_details);
            $total_risk_component_array = array_merge($total_risk_component_array,$risk_component_array);
            $total_risk_weight +=$risk->risk_weight_factor;
            $total_risk = $total_risk+$component;
        }
        try{
            $total_risk_component_array['credit_score'] = $total_risk/$total_risk_weight;

        }catch (\Exception $exception){
            $this->log->info($exception->getMessage().' on line number '.$exception->getLine()." Returning null");
            $total_risk_component_array = null;
        }
        return $total_risk_component_array;
    }

    //Calculates the risk of particular component
    public function riskComponent($risk,$risk_input)
    {
        $component_risk_total = 0;
        $component_risk = 0;
        $attributes = $risk->attributes;
        $attribute_risk_array =[];
        if (!isset($attributes)|| empty($risk_input)) return 1;
        $total_component_weight = $risk->attributes->sum(function ($risk_attribute) {
            return ($risk_attribute->attribute_weight_factor);
        });
        foreach ($attributes as $key => $attribute) {
            $attribute_risk = $this->attributeRisk($attribute,$risk_input);
            $attribute_risk_array[$attribute->name] = $attribute_risk;
            $component_risk_total = $component_risk_total + $attribute_risk;
        }
        try{
           $component_risk = $component_risk_total/$total_component_weight;
        }catch (\Exception $exception){
            $this->log->info($exception->getMessage().' on line number '.$exception->getLine()." Returning null");
            $component_risk_total = null;
        }
        //return $component_risk_total;
        return  array( 'component_risk'=>$component_risk,
                        'attribute_risk_array'=>$attribute_risk_array,
                        'total_component_weight'=>$total_component_weight,
                        'component_risk_weight'=>$component_risk_total
                        );
    }

    //Calculates the risk of particular attribute
    public function attributeRisk($attribute,$risk_input)
    {
        if((!isset($attribute))|| empty($risk_input)) return 1;
        $category_risk = $this->attributeCategoryRisk($attribute,$risk_input);
        $attribute_risk = $attribute->attribute_weight_factor * $category_risk;
        return $attribute_risk;
    }


    public function attributeCategoryRisk($attribute,$risk_input)
    {
        $categories = isset($attribute->categories) ? $attribute->categories : null;
        if ((!isset($categories))|| empty($risk_input)) return 1;
        $type = $attribute->type;
        if (array_key_exists($attribute->name, $risk_input)) {
            $attribute_value = $risk_input[$attribute->name];
        }
        if (!isset($attribute_value)) return 1;//if no value return risky;
        if ($type == "range") {
            $category_weight_factor = $this->attributeWeightFactorRange($categories, $attribute_value);
        } elseif($type == "fixed") {
            $category_weight_factor = $this->attributeWeightFactorFixed($categories, $attribute_value);
        }
        else{
            $category_weight_factor = $this->attributeWeightFactorBoolean($categories, $attribute_value);
        }
        return $category_weight_factor;
    }

    public function attributeWeightFactorRange($categories, $attribute_value)
    {
        $category_weight_factor = null;
        $category_weight_factor_array = [];
        foreach ($categories as $category) {
            $min_value = $category->min_value;
            $max_value = $category->max_value;
            $category_weight_factor_array[] = $category->category_weight_factor;
            if (isset($min_value) && isset($max_value)) {
                if ($attribute_value >=$min_value && $attribute_value <= $max_value) {
                    $category_weight_factor = $category->category_weight_factor;
                    break;
                }
            }
        }

        $category_weight_factor=  self::returnWeightFactor($category_weight_factor,$category_weight_factor_array);
        return $category_weight_factor;
    }

    public function attributeWeightFactorFixed($categories, $attribute_value)
    {
        $category_weight_factor_array=[];
        $category_weight_factor =null;
        foreach ($categories as $category) {
            $category_weight_factor_array[] = $category->category_weight_factor;
            if ($category->name == $attribute_value) {
                $category_weight_factor = $category->category_weight_factor;
                break;
            }
        }
        $category_weight_factor=  self::returnWeightFactor($category_weight_factor,$category_weight_factor_array);
        return $category_weight_factor;
    }

    public static function returnWeightFactor($category_weight_factor,$category_weight_factor_array){
        if(isset($category_weight_factor)) return $category_weight_factor;
        if(!empty($category_weight_factor_array)) return max($category_weight_factor_array);
        return 1;
    }

    public function attributeWeightFactorBoolean($categories, $attribute_value)
    {
        $category_weight_factor_array=array();
        $category_weight_factor=null;
        foreach ($categories as $category) {
            $category_weight_factor_array[] = $category->category_weight_factor;
            if ($category->boolean_value == $attribute_value) {
                $category_weight_factor = $category->category_weight_factor;
                break;
            }
        }
        $category_weight_factor= self::returnWeightFactor($category_weight_factor,$category_weight_factor_array);
        return $category_weight_factor;
    }

   public function riskInput($farmer_id){
        $risk_input =[];
        $farmer = Farmer::findOrFail($farmer_id);
        $project = $farmer->projects->first();
       /* $project = DB::table('farmers')->join('farmer_project','farmers.id','=','farmer_project.farmer_id')->where('farmers.id',$farmer_id)->first();
        dd($project);*/
        $farm = $farmer->farm->first();
        $this->farmer_array = $farmer->toArray();
        $this->project_array = $project->toArray();
        $this->farm_array = $farm->toArray();
        foreach (config('Risk.risk') as $item) {
            if (is_array($item)) {
                foreach ($item as $in) {
                    $risk_input[$in] = $this->riskValue($in);
                }
            }
        }
        return $risk_input;
    }

    private function riskValue($in)
    {
        if(array_key_exists($in,$this->farmer_array))
            return $this->farmer_array[$in];
        elseif(array_key_exists($in,$this->project_array))
             return $this->project_array[$in];
        elseif(array_key_exists($in,$this->farm_array))
             return $this->farm_array[$in];
        else return null;
    }

}