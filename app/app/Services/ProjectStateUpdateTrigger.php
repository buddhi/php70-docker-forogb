<?php

namespace App\Services;

use App\Enums\ProjectStatus;
use App\Jobs\SendEmailJobForApprovedProject;
use App\Jobs\SendEmailJobForFundedProject;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Sms\SmsRepository;
use App\User;
use App\Utilities\StringManipulator;

class ProjectStateUpdateTrigger
{
    private $smsRepository, $emailRepository;

    public function __construct(SmsRepository $smsRepository, EmailRepository $emailRepository)
    {
        $this->smsRepository = $smsRepository;
        $this->emailRepository = $emailRepository;
    }

    public function sendSmsEmailOnStateChange($project, $next_Status)
    {
        try {
            $agent = User::find($project->created_by);
            $farmer = $project->farmers->first();
            $send_sms_to_agent = true;
        } catch (\Exception $exception) {
            $send_sms_to_agent = false;
        }

        if (!$send_sms_to_agent) {
            return;
        }

        switch ($next_Status) {
            case ProjectStatus::APPROVED:
                $this->sendSmsAndEmailWhenProjectIsApproved($agent, $project, $farmer);
                break;

            case ProjectStatus::FUNDED:
                $this->sendSmsAndEmailWhenProjectIsFunded($agent, $project, $farmer);
                break;
            case  ProjectStatus::INSTALLED:
                $this->sendSmsAndEmailWhenProjectIsInstalled($agent, $project, $farmer);
                break;
            case  ProjectStatus::OPERATIONAL:
                $this->sendSmsAndEmailWhenProjectIsOperational($agent, $project, $farmer);

                break;
            default:
                break;

        }

    }

    private function sendSmsAndEmailWhenProjectIsApproved($agent, $project, $farmer)
    {

        $this->smsRepository->sendSmsNotification($farmer->contact_no, 'PROJECT_APPROVED_MESSAGE_FARMER', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name)]);
        $this->smsRepository->sendSmsNotification($agent->phone, 'PROJECT_APPROVED_MESSAGE_AGENT', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name), 'agent_name' => StringManipulator::getFirstTwoWords($agent->full_name)]);
        $this->sendEmailsToAgentWhenProjectIsApproved($agent, $project, $farmer);

    }

    private function sendSmsAndEmailWhenProjectIsFunded($agent, $project, $farmer)
    {
        $this->smsRepository->sendSmsNotification($farmer->contact_no, 'PROJECT_FUNDED_MESSAGE_FARMER', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name)]);
        $this->smsRepository->sendSmsNotification($agent->phone, 'PROJECT_FUNDED_MESSAGE_AGENT', ['agent_name' => StringManipulator::getFirstTwoWords($agent->full_name), 'farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name)]);
        $this->sendEmailsToInvestorsWhenProjectIsFunded($project);
    }

    private function sendSmsAndEmailWhenProjectIsInstalled($agent, $project, $farmer)
    {
        $this->smsRepository->sendSmsNotification($farmer->contact_no, 'PROJECT_INSTALLED_MESSAGE_FARMER', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name), 'next_due_date' => $project->nextDueDateInBSFormat()]);
        $this->smsRepository->sendSmsNotification($agent->phone, 'PROJECT_INSTALLED_MESSAGE_AGENT', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name), 'agent_name' => StringManipulator::getFirstTwoWords($agent->full_name), 'next_due_date' => $project->nextDueDateInBSFormat()]);
        $this->emailRepository->sendEmailsToAgentWhenProjectIsInstalled($agent, $project, $farmer);
    }

    private function sendSmsAndEmailWhenProjectIsOperational($agent, $project, $farmer)
    {
        $this->emailRepository->sendEmailsToInvestorsWhenProjectIsOperational($agent, $project, $farmer);
    }

    private function sendEmailsToInvestorsWhenProjectIsFunded($project)
    {
        $investors = $project->getInvestors();
        dispatch(new SendEmailJobForFundedProject($investors, $project->id));
    }

    private function sendEmailsToAgentWhenProjectIsApproved($agent, $project, $farmer)
    {
        dispatch(new SendEmailJobForApprovedProject($agent, $project, $farmer));
    }
}