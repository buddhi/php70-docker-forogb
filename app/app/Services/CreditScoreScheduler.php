<?php

namespace App\Services;

use App\Models\Farmer;

class CreditScoreScheduler
{
    private $creditScoreObj;

    public function __construct()
    {
        $this->creditScoreObj = new CreditScore();
    }

    public function fetchCreditScores()
    {
        $farmers = Farmer::all();
        foreach ($farmers as $farmer) {
            $farmer->credit_score = $this->getCreditScore($farmer->id);
            $farmer->save();
        }
    }

    private function getCreditScore($farmer_id)
    {
        $credit_score_array = $this->creditScoreObj->creditScore($farmer_id);
        return array_key_exists('credit_score', $credit_score_array) ? $credit_score_array['credit_score'] : null;
    }

}