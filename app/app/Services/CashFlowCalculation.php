<?php

namespace App\Services;

use App\Models\Project;

class CashFlowCalculation
{
    public static function calculate($project_id){
        $project = Project::with('district','crops')->where('id',$project_id)->first();
        $district_id = $project->district->id;
        $total_cost_for_production = [];
        $gross_revenue_anually = [];
        $crop_income = [];
        $cattle_income = [];
        $count = 0;
        $cattle_expenditure = [];
        $crop_expenditure = [];
        $projection_crops = self::findProjectionForCrops($project, $district_id, $total_cost_for_production, $gross_revenue_anually, $crop_income, $count, $crop_expenditure);
        $projection_cattles = self::findProjectionForCattles($project, $district_id, $total_cost_for_production, $gross_revenue_anually, $cattle_income, $count, $cattle_expenditure);
        $income = self::merge_arrays([$projection_cattles['income'],$projection_crops['income']]);
        $expenditure = self::merge_arrays([$projection_cattles['expenditure'],$projection_crops['expenditure']]);
        return [$income,$expenditure];


    }
    public static function total_cost_production($crop,$district_id){
        return $crop->cashflow_values($district_id)->cost_production * 1;
    }
    public static function gross_revenue($crop,$district_id){
        $value = $crop->cashflow_values($district_id);
        return $value->amount_produced_per_cycle * $value->average_market_price * 1;
    }
    public static function merge_arrays(array $array){
        $count = count($array) + 1;
        $array1 = (isset($array[$count - 1]) ? $array[$count-1] : []);
        $array2 = (isset($array[$count - 2]) ? $array[$count-2] : []);
        $count = $count - 2;
        $sums = array();
        while($count >= 0){
            foreach (array_keys($array1 + $array2) as $key) {
                $sums[$key] =(isset($array1[$key]) ? $array1[$key] : 0)  + (isset($array2[$key]) ? $array2[$key] : 0);
            }
            $array1 = $sums;
            $array2 = (isset($array[$count - 1]) ? $array[$count - 1] : []);
            $count = $count - 1;
        }

        return $sums;
    }

    public static function find_cultivating_months($crop,$district_id)
    {
        $value = $crop->cashflow_values($district_id)->cultivating_months;
        return sizeof(json_decode($value));

    }

    /**
     * @param $project
     * @param $district_id
     * @param $total_cost_for_production
     * @param $gross_revenue_anually
     * @param $crop_income
     * @param $count
     * @param $crop_expenditure
     * @return array
     */
    public static function findProjectionForCrops($project, $district_id, $total_cost_for_production, $gross_revenue_anually, $crop_income, $count, $crop_expenditure): array
    {
        foreach ($project->crops as $crop) {
            $total_cost_for_production[$crop->id] = self::total_cost_production($crop, $district_id);
            $gross_revenue_anually[$crop->id] = self::gross_revenue($crop, $district_id);
            $monthly_income = array_fill(1, 12, 0);
            $monthly_expenditure = array_fill(1, 12, 0);
            $month = $crop->pivot->start_month + self::find_cultivating_months($crop, $district_id);
            $harvesting_month = $crop->cashflow_values($district_id)->harvesting_months;
            $value = sizeof(json_decode($harvesting_month));
            $area = CropArea::convertCropAreaToUnit($crop, 'hectare');
            for($i = 0; $i < $value; $i++){
                if($crop->id == 31){
                    $monthly_income[(($month + $i) % 12)] = 0.5 * $area * $gross_revenue_anually[$crop->id];
                }else{
                    $monthly_income[(($month + $i) % 12)] = $area * $gross_revenue_anually[$crop->id];

                }
            }
            $start_month = $crop->pivot->start_month;
            $cultivation_month = self::find_cultivating_months($crop, $district_id);
            while ($cultivation_month > 0) {
                $monthly_expenditure[$start_month] = $area / 1 * $total_cost_for_production[$crop->id] / self::find_cultivating_months($crop, $district_id); //1 is the per unit cost of production it maybe 2 for goat and 4 for cow
                $start_month = ($start_month + 1) % 12;
                if($start_month == 0){
                    $start_month = 12;
                }
                $cultivation_month--;
            }
            $crop_income[$count] = $monthly_income;
            $crop_expenditure[$count] = $monthly_expenditure;
            $count++;
        }
        $crop_income = self::merge_arrays($crop_income);
        $crop_expenditure = self::merge_arrays($crop_expenditure);
        return ['income'=>$crop_income,'expenditure'=> $crop_expenditure];
    }

    private static function findProjectionForCattles($project, $district_id, $total_cost_for_production, $gross_revenue_anually, $cattle_income, $count, $cattle_expenditure)
    {
        foreach ($project->cattles as $cattle) {
            $total_cost_for_production[$cattle->id] = self::total_cost_production_cattle($cattle, $district_id);
            $gross_revenue_anually[$cattle->id] = self::gross_revenue_cattle($cattle, $district_id);
            $monthly_income = array_fill(1, 12, 0);
            $monthly_expenditure = array_fill(1, 12, 0);
            $cattle_values =  $cattle->cattle_values($district_id);
            $month = 0;
            $month = $cattle_values->start_month + ($cattle_values->total_duration - 1);

            $monthly_income[($month % 12)] =  $cattle->pivot->number / $cattle_values->unit * $gross_revenue_anually[$cattle->id]; //Check for harvest if greater than 2
            $start_month = $cattle_values->start_month;
            $cultivation_month = $cattle_values->total_duration - 1;
            while ($cultivation_month > 0) {
                $monthly_expenditure[$start_month] = $cattle->pivot->number / $cattle_values->unit * $total_cost_for_production[$cattle->id] / ($cattle_values->total_duration - 1); //1 is the per unit cost of production it maybe 2 for goat and 4 for cow
                $start_month = ($start_month + 1) % 12;
                if($start_month == 0){
                    $start_month = 12;
                }
                $cultivation_month--;
            }
            $cattle_income[$count] = $monthly_income;
            $cattle_expenditure[$count] = $monthly_expenditure;
            $count++;
        }
        $cattle_income = self::merge_arrays($cattle_income);
        $cattle_expenditure = self::merge_arrays($cattle_expenditure);
        return ['income'=>$cattle_income, 'expenditure'=>$cattle_expenditure];

    }

    private static function total_cost_production_cattle($cattle, $district_id)
    {
        $cattle_values =  $cattle->cattle_values($district_id);
        $total_cost_of_production = $cattle_values->cost_of_production * $cattle_values->number_of_cycle;
        return $total_cost_of_production;

    }

    private static function gross_revenue_cattle($cattle, $district_id)
    {
        $cattle_values =  $cattle->cattle_values($district_id);
        return $cattle_values->price_unit;
    }

}