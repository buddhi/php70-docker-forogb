<?php

namespace App\Services;

use App\Models\DailyMeterData;
use App\Models\EnrichMeterData;
use App\Models\Meter;
use App\Models\MeterData;
use App\Repositories\MeterData\MeterDataInterpolationService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MeterDataRefreshService
{
    private static $INTERPOLATION_THRESHOLD_IN_DAYS = 5;
    public function aggregateData(){
        $meters = Meter::all();
        $data = DailyMeterData::latest('date')->first();
        $date = null;
        $distinct_date_meterData = null;
        if($data){
            $date = Carbon::parse($data->date)->toDateString();
            $distinct_date_meterData = EnrichMeterData::select(DB::raw('DATE(created_at) as created_at'))->whereDate('created_at','>',$date)->distinct()->get();
        }else{
            $distinct_date_meterData = EnrichMeterData::select(DB::raw('DATE(created_at) as created_at'))->distinct()->get();
        }

        if(sizeof($distinct_date_meterData) <= 0){
            echo "No Daily data to aggregate"."\n";
            Log::info('There is no data to aggregate after date '.$date);
            return;
        }
        foreach($distinct_date_meterData as $md)
        {
            foreach($meters as $meter){
                $md_selected_date = Carbon::parse($md->created_at)->toDateString();
                $meter_data_daily = EnrichMeterData::select(
                    DB::raw('SUM(discharge) as daily_discharge'),
                    DB::raw('COUNT(id) as daily_count'),
                    DB::raw('SUM(runtime) as daily_runtime'))
                    ->where(DB::raw('DATE(created_at)'), '=', $md_selected_date)->where('meter_id', '=', $meter->id)
                    ->first();
                DailyMeterData::updateOrCreate([
                    'daily_discharge' =>$meter_data_daily->daily_discharge,
                    'daily_count' =>$meter_data_daily->daily_count,
                    'daily_runtime' =>$meter_data_daily->daily_runtime,
                    'meter_id' => $meter->id,
                    'date'=> $md_selected_date
                ]);

            }
        }
    }

    public function addData(){
        $meterdata = MeterData::where('discharge','=',null)->oldest()->first();
        if(!$meterdata){
            echo "No data to aggregate"."\n";
            Log::info('There is no data left for aggregation in meterdata table till date '.Carbon::now()->toDateString());
            return;
        }
        $distinct_date_meterData = MeterData::where('created_at','>=',$meterdata->created_at)->where('discharge',null)->get();
        $count = 0;
        foreach($distinct_date_meterData as $md)
        {
            echo 'MeterData id '.$md->id.' data aggregated'."\n";
            $md->dc_power = $md->getDcPowerAttribute();
            $md->ac_power = $md->getAcPowerAttribute();
            $md->power_percent = $md->getPowerPercentAttribute();
            $md->discharge = $md->getDischargeAttribute();
            $md->frequency = $md->getFrequencyAttribute();
            if($md->discharge > 0){
                $count ++;
            }
            $md->runtime = $count;
            $md->rpm = $md->getRpmAttribute();
            $md->save();
            $count = 0;
        }
    }

    //Interpolates the meter data
    public function interpolateMeterData()
    {
        //last interpolated meter data
        $last_interpolated_meter_data = self::getMeterDataThatWasLastEnriched();
        if (!$last_interpolated_meter_data) {
            return;
        }
        $meters = Meter::all();
        foreach ($meters as $meter) {
            // Get the earliest interpolated meter data
            $earliest_meter_data = $meter->meterdata()
                ->where('id', '>', $last_interpolated_meter_data->id)
                ->where('created_at', '>=', Carbon::now()->subDays(self::$INTERPOLATION_THRESHOLD_IN_DAYS))
                ->orderBy('created_at', 'ASC')->first();

            if((!isset($earliest_meter_data)) && (!isset($earliest_meter_data->created_at))){
                continue;
            }

            //Flush the enrich meter data that includes data from the previous dates of particular meter
            DB::table('enrich_meter_datas')->where('created_at','>=' ,$earliest_meter_data->created_at)->where('meter_id',$meter->id)->delete();

            //Fetch again the data
            $meter_data = $meter->meterdata()
                ->where('created_at', '>=', $earliest_meter_data->created_at)
                ->orderBy('created_at','asc')
                ->get()
                ->toArray();

            //Interpolate and store the data
            if ((!isset($meter_data)) && count($meter_data) <= 0) {
                continue;
            }
            $meter_interpolation_service = new MeterDataInterpolationService();
            if (isset($meter_data)) {
                $meter_interpolation_service->interpolateData($meter_data, $meter, 0);
                unset($meter_data);
            }
        }
    }

    private static function getMeterDataThatWasLastEnriched()
    {
        //Selecting the last enrich meterdata
        $last_enriched_meter_data = DB::table('enrich_meter_datas')->orderBy('id', 'DESC')->first();

        //If enrich meter data is empty return the first meter data
        if(!$last_enriched_meter_data){
            $meterDataThatWasLastEnriched = MeterData::all()->first();
            return $meterDataThatWasLastEnriched;
        }

        //Checking and getting the meter data that was last enriched
        $meterDataThatWasLastEnriched = MeterData::where('filename', $last_enriched_meter_data->filename)->first();
        if($meterDataThatWasLastEnriched){
            return $meterDataThatWasLastEnriched;
        }

        //Then looping and getting the meter data that was last enriched
        $enriched_meter_data_collection = EnrichMeterData::select('filename')->orderBy('id', 'desc')->get();
        foreach ($enriched_meter_data_collection as $enriched_meter_data) {
            $last_interpolated_file = $enriched_meter_data->filename;
            $meterDataThatWasLastEnriched = MeterData::where('filename', $last_interpolated_file)->first();
            if($meterDataThatWasLastEnriched) {
                return $meterDataThatWasLastEnriched;
            }
        }

    }
}