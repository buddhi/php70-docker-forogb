<?php
namespace App\Services;

class Conversion
{

    /**
     * @var array
     */
    protected static $conversionChart = [
        // Area
        'SQUARE_METER' => 1,
        'HECTARE' => 10000,
        'SQUARE_KILOMETER' => 1000000,
        'SQUARE_INCH' => 0.00064516,
        'SQUARE_FEET' => 0.09290304,
        'ACRE' =>  4046.86,
        'SQUARE_MILE' => 2589988.110336,
        'KATHA' => 338.63,
        'ROPANI' => 508.72,
        'BIGAHA' => 6772.6,
        'AANA' => 31.80,

        // Length
        'MILLIMETER' => 0.001,
        'CENTIMETER' => 0.01,
        'METER' => 1,
        'KILOMETER' => 1000,
        'INCH' => 0.0254,
        'FEET' => 0.3048,
        'MILE' => 1609.34,


        // Time
        'NANOSECOND' => 1e-9,
        'MICROSECOND' => 1e-6,
        'MILLISECOND' => 0.001,
        'SECOND' => 1,
        'MINUTE' => 60,
        'HOUR' => 3600,
        'DAY' => 86400,
        'WEEK' => 604800,
        'MONTH' => 2.62974e6,
        'YEAR' => 3.15569e7,
        'DECADE' => 3.15569e8,
        'CENTURY' => 3.15569e9,
        'MILLENIUM' => 3.1556926e10,

    ];
    public $value,$unit, $type;
    public $number = null;

    public function __construct($quantity = '', $unit = '')
    {
        $this->unit = $unit;
        $this->value = $quantity;
    }

    public function convert($quantity, $unit)
    {
        return new Conversion($quantity, $unit);
    }

    public function to($unit)
    {
        $this->value = $this->process($this->unit, $unit, $this->value);
        $this->unit = $unit;
        return $this;
    }

    protected function process($from, $to, $value)
    {
        $from_conversion_value =$this->getConversion($from);
        $to_conversion_value =$this->getConversion($to);
        if(!($from_conversion_value && $from_conversion_value))
            return null;
        return ($value * $from_conversion_value) / $to_conversion_value;
    }

    protected function getConversion($unit)
    {
        if (!isset(static::$conversionChart[strtoupper($unit)])) {
             return false;
        }

        return static::$conversionChart[strtoupper($unit)];
    }

    public function format($decimals = 3, $decPoint = '.', $thousandSep = ',')
    {
        return number_format($this->value, $decimals, $decPoint, $thousandSep);
    }

    public function __toString()
    {
        return $this->format();
    }

    public function value()
    {
        return $this->value;
    }


    public function withUnits()
    {   
       if(!isset($this->value))
           return null;
        return round($this->value,5)." ".strtolower( str_replace('_', ' ', $this->unit));
    }
}
