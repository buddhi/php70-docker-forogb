<?php

namespace App\Services;

use App\DataModule\Repository\DataFormatter;
use App\DataModule\Repository\DataIngestion;
use App\Models\Loan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DataIngestionService
{
    /**
     * @var DataIngestion
     */
    private $dataIngestion;

    /**
     * DataIngestionService constructor.
     * @param DataIngestion $dataIngestion
     */
    public function __construct(DataIngestion $dataIngestion)
    {
        $this->dataIngestion = $dataIngestion;
    }

    public function receive()
    {
        $formattedData = $this->dataIngestion->laxmiBankData();
        foreach ($formattedData as $data){
           if($data->due_amount > 0){
               //Condition for due amount
               $loan = new Loan();
               $loan->next_due = 0;
               $loan->over_due = $data->due_amount;
               $loan->updated_at = $data->payment_date;
               $loan->transition('isDue');
               $loan->save();
           }else{
               //Condition if no due amount
               $loan = new Loan();
               $loan->next_due = 100.20; //I think this should be given by the bank
               $loan->over_due = $data->due_amount;
               $loan->updated_at = $data->payment_date;
               $loan->transition('paid');
               $loan->save();
           }
        }
    }
}