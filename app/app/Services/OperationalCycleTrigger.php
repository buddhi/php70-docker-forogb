<?php

namespace App\Services;

use App\Enums\ProjectStatus;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Sms\SmsRepository;
use App\User;
use App\Utilities\StringManipulator;

class OperationalCycleTrigger
{
    private $smsRepository,$emailRepository;
    public function __construct(SmsRepository $smsRepository,EmailRepository $emailRepository)
    {
        $this->smsRepository = $smsRepository;
        $this->emailRepository = $emailRepository;
    }
    public function  sendSmsEmail($project,$initial_status,$final_status)
    {
        try{
            $farmer = $project->farmers->first();
            $agent = User::find($project->created_by);
        }
        catch (\Exception $exception){
            return;
        }
        switch(true)
        {
            /**  operational <==> OverDue Cycle triggers*/

            // operational==> overdue triggers
            case ($initial_status == ProjectStatus::OPERATIONAL and $final_status == ProjectStatus::OVERDUE):
                $this->triggersFromOperationalToOverdue($project, $agent, $farmer);
                break;

            // overdue ==> operational triggers
            case ($initial_status == ProjectStatus::OVERDUE and $final_status == ProjectStatus::OPERATIONAL):
                $this->triggersFromOverdueToOperational($project, $agent, $farmer);
                break;


            /**  operational <==> Error Cycle triggers*/

            //operational ==> error triggers
            case ($initial_status == ProjectStatus::OPERATIONAL and $final_status == ProjectStatus::ERROR):
                $this->triggersFromOperationalToError($project, $agent, $farmer);
                break;

            //error ==> operational triggers
            case ($initial_status == ProjectStatus::ERROR and $final_status == ProjectStatus::OPERATIONAL):
                $this->triggersFromErrorToOperational($project, $agent, $farmer);
                break;


            /**  Error <==> OverDue Cycle triggers*/

            case ($initial_status == ProjectStatus::OVERDUE and $final_status == ProjectStatus::ERROR):
                $this->triggersFromOverdueToError($project, $agent, $farmer);
                //triggers can be added here if necessary
                break;

            case ($initial_status == ProjectStatus::ERROR and $final_status == ProjectStatus::OVERDUE):
                //triggers can be added here if necessary
                break;
            default:
                break;
        }

    }


    // operational ==> overdue triggers
    public function triggersFromOperationalToOverdue($project, $agent, $farmer)
    {
        $this->smsRepository->sendSmsNotification($farmer->contact_no, 'PROJECT_OPERATIONAL_TO_OVERDUE_MESSAGE_FARMER', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name)]);
        $this->smsRepository->sendSmsNotification($agent->phone, 'PROJECT_OPERATIONAL_TO_OVERDUE_MESSAGE_AGENT', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name), 'agent_name' => StringManipulator::getFirstTwoWords($agent->full_name), 'next_due_date' => $project->nextDueDate()]);
        $this->emailRepository->sendEmailToAgentWhenProjectOverDue($agent, $project, $farmer);
    }

    // operational ==> error triggers
    private function triggersFromOperationalToError($project, $agent, $farmer)
    {
        $this->emailRepository->sendEmailToAgentWhenProjectTransitsOperationalToError($agent, $project, $farmer);
        $this->emailRepository->sendEmailToAdminWhenProjectTransitsOperationalToError($agent, $project, $farmer);
    }

    public function triggersFromOverdueToOperational($project, $agent, $farmer)
    {
        $this->smsRepository->sendSmsNotification($farmer->contact_no, 'PROJECT_OVERDUE_TO_OPERATIONAL_MESSAGE_FARMER', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name)]);
        $this->smsRepository->sendSmsNotification($agent->phone, 'PROJECT_OVERDUE_TO_OPERATIONAL_MESSAGE_AGENT', ['farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name), 'agent_name' => StringManipulator::getFirstTwoWords($agent->full_name)]);
        $this->emailRepository->sendEmailToAgentWhenProjectTransitsOverDueToOperational($agent, $project, $farmer);
        $this->emailRepository->sendEmailToAdminWhenProjectTransitsOverDueToOperational($agent, $project, $farmer);
    }

    private function triggersFromErrorToOperational($project, $agent, $farmer)
    {
        $this->emailRepository->sendEmailToAgentWhenProjectTransitsErrorToOperational($agent, $project, $farmer);
        $this->emailRepository->sendEmailToAdminWhenProjectTransitsErrorToOperational($agent, $project, $farmer);
    }

    private function triggersFromOverdueToError($project, $agent, $farmer)
    {
        $this->emailRepository->sendEmailToAgentWhenProjectTransitsOverDueToError($agent, $project, $farmer);
        $this->emailRepository->sendEmailToAdminWhenProjectTransitsOverDueToError($agent, $project, $farmer);
    }


}