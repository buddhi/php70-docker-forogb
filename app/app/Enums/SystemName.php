<?php
namespace App\Enums;

abstract Class SystemName
{
    const SAHAJ = 'sahaj';
    const SAMPURNA = 'sampurna';
    const SAMBRIDHI = 'Sambridhi';
    const CUSTOM = 'custom';

    //other custom system name are  also present
}