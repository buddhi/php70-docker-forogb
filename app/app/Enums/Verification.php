<?php

namespace App\Enums;

abstract class Verification
{
    const EMAIL_VERIFIED  =1;
    const ADMIN_VERIFIED  = 1;
    const EMAIL_NOT_VERIFIED  = 0;
    const ADMIN_NOT_VERIFIED  = 0;
}