<?php

namespace App\Enums;

abstract class EducationLevel
{
    const SLC = 'SLC';
    const Intermediate = 'Intermediate';
    const Bachelor = 'Bachelor';
    const Master = 'Master';
    const ALL = [self::SLC,self::Intermediate,self::Bachelor,self::Master];

}