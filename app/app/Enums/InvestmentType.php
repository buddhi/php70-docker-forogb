<?php

namespace App\Enums;

class InvestmentType
{
    const Equity = 1;
    const Debt = 2;
    const Grant = 3;
}