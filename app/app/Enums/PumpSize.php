<?php

namespace App\Enums;

abstract Class PumpSize
{
    const MINIPUMP = 0;
    const SAHAJ = 1;
    const SAMPURNA = 1.5;
    const SAMBRIDHI = 2;
}