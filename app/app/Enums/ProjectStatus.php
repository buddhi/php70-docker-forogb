<?php

namespace App\Enums;

abstract class ProjectStatus
{
    const PLAN = 'plan';
    const NEW = 'new';
    const APPROVED = 'approved';
    const FUNDING = 'funding';
    const FUNDED = 'funded';
    const INSTALLED = 'installed';
    const OPERATIONAL = 'operational';
    const ERROR = 'error';
    const OVERDUE = 'overdue';
    const ALL_PROJECT_STATUS = [self::PLAN,self::NEW,self::APPROVED,self::FUNDING,self::FUNDED,self::INSTALLED,self::OPERATIONAL,self::OVERDUE,self::ERROR];
}