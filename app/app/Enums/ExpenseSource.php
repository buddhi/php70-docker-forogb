<?php

namespace App\Enums;

abstract class ExpenseSource
{
    const HOUSEHOLD_EXPENSE = 'household_expense';
    const EDUCATION_EXPENSE = 'education_expense';
    const HEALTH = 'health';
    const INSTALLMENTS = 'installments';
    const LOAN = 'loan';
    const OTHER = 'other';
    const ALL = [self::HOUSEHOLD_EXPENSE,self::EDUCATION_EXPENSE,self::HEALTH,self::INSTALLMENTS,self::LOAN,self::OTHER];

}