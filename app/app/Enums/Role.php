<?php
namespace App\Enums;

abstract class Role
{
    const DEVELOPER = 1;
    const INVESTOR = 10;
    const ADMIN = 50;
    const PARTNER_USER = 200;
}