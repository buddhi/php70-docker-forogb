<?php

namespace App\Enums;

abstract class OtpVerificationType
{
    const AGENT_VERIFICATION  = 'agent';
    const PARTNER_USER_VERIFICATION  = 'partner-user';
    const ACCOUNT_VERIFICATION  = 'account';

}