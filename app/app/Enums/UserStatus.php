<?php

namespace App\Enums;

 abstract class UserStatus
{
  const VERIFIED = 'verified';
  const VERIFICATION_PENDING = 'verification-pending';
  const DEACTIVATED = 'deactivated';
  const ACTIVATED = 'activated';
}