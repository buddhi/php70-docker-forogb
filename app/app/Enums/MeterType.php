<?php

namespace App\Enums;

abstract class MeterType
{
    const SMART_METER_MEASURE_ATTRIBUTES = ['current', 'voltage'];
    const AGRICULTURE_METER_ATTRIBUTES = ['current', 'voltage', 'temperature', 'humidity', 'soil_moisture'];
    const FISH_METER_ATTRIBUTES = ['water_temperature', 'oxygen_level'];
    const SMART_METER = 'smart_meter';
    const AGRICULTURE_METER = 'agriculture_meter';
    const FISH_METER = 'fish_meter';
    const ALL_METER_TYPES = [self::SMART_METER,self::AGRICULTURE_METER,self::FISH_METER];

}