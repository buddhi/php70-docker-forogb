<?php

namespace App\Enums;

abstract class FtpFileLocation
{
    const FTP_DATA_LOCATION = 'Files';              //for development:TestFiles
    const FTP_BACKUP_DATA_LOCATION = 'OldFiles';    //for development:TestOldFiles
}