<?php
namespace App\Enums;

abstract class PaymentType
{
    const ADVANCE = 'advance';
    const EMI = 'emi';
    const INVALID = 'invalid'; //recognises invalid payment
    const Cash = 'cash';
}