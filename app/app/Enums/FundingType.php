<?php

namespace App\Enums;

class FundingType
{
    const Percent = 0;
    const Amount = 1;
    const Remaining = 2;
}