<?php

namespace App\Enums;

abstract class IncomeSource
{
    const BUSINESS = 'business';
    const JOB = 'job';
    const REMITTANCE = 'remittance';
    const OTHER = 'other';
    const ALL = [self::BUSINESS,self::JOB,self::REMITTANCE,self::OTHER];

}