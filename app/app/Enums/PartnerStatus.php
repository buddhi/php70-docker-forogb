<?php

namespace App\Enums;

 abstract class PartnerStatus
{
  const VERIFICATION_PENDING = 'verification-pending';
  const DEACTIVATED = 'deactivated';
  const ACTIVATED = 'activated';
}