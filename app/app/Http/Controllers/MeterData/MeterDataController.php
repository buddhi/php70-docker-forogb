<?php

namespace App\Http\Controllers\MeterData;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExportValidation;
use App\Models\DailyMeterData;
use App\Models\EnrichMeterData;
use App\Models\Farm;
use App\Models\FarmerProject;
use App\Models\Meter;
use App\Models\MeterData;
use App\Models\Project;
use App\Repositories\MeterData\MeterDataInterpolationService;
use App\Repositories\Project\ProjectRepository;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use JavaScript;
use function MongoDB\BSON\toJSON;

class MeterDataController extends Controller
{
    
    /**
     * MeterDataController constructor.
     */
    public $meterDataInterpolationService;
    public function __construct(MeterDataInterpolationService $meterDataInterpolationService)
    {
        $this->meterDataInterpolationService=$meterDataInterpolationService;
    }
    public function listMeters($project_id){
       $farmer_project_id = DB::table('farmer_project')->where('project_id',$project_id)->first();
       $farmer_project_id = $farmer_project_id->id;
       $meters = Meter::where('farmer_project_id',$farmer_project_id)->get();
       return view('meterdata.meterlist',compact('meters'));
   }
   //This method sends the loads the meterdata page
   public function index($project_id){
       $farmer_project_id = DB::table('farmer_project')->where('project_id',$project_id)->first();
       $farmer_project_id = $farmer_project_id->id;
       $meter = Meter::where('farmer_project_id',$farmer_project_id)->first();
       $meter_id = $meter->id;
       $farm= Farm::select('id','farm_image')->where('id','=',$farmer_project_id )->first();
       JavaScript::put([
           'meter_id' => $meter_id
       ]);
       return view('meterdata.meterdata_demo',compact('meter_id','farm','project_id','meter'));
   }
   //This function sends the data to highchart view
    public function sendData(Request $request){
        $meter_id = $request->meter_id;
        $lastDischarge = MeterData::where('meter_id','=',$meter_id)->where('discharge','>',0)->latest()->first();
        $lastData = MeterData::where('meter_id','=',$meter_id)->latest()->first();
        $meter = Meter::where('id',$meter_id)->first();
        $farmer_project_id = $meter->farmer_project_id;
        $project_id = DB::table('farmer_project')->find($farmer_project_id);
        $project = Project::with(['province','municipality','district','user'])->where('id',$project_id->project_id)->first()->toArray();
        $aggregate = MeterData::aggregateDischargeAndRunTime($meter_id);
        $aggregate_flow_runtime = ['discharge'=>$aggregate->total_discharge,'runtime'=>$aggregate->total_runtime];
        if(App::environment('production', 'staging')){
            $data = MeterData::where('meter_id','=',$meter_id)->whereDate('created_at','=',Carbon::today()->toDateString())->where('current','>=',0)->orderBy('created_at','ASC')->get()->toArray();
        }else{
            $date = Carbon::today();
            $date = $date->subDays(30);
            $data = MeterData::where('meter_id','=',$meter_id)->whereDate('created_at','=',$date)->where('current','>=',0)->get()->toArray();
            $data =$this->meterDataInterpolationService->interpolateData($data,$meter,1);
        }

        if($lastDischarge){
            $created_at = Carbon::parse($lastDischarge->created_at)->toDateTimeString();
        }else{
            $created_at = null;
        }

        if($lastData){
            $lastDataDate = Carbon::parse($lastData->created_at)->toDateTimeString();
        }else{
            $lastDataDate = null;
        }
        return [$data,$created_at,$project,$aggregate_flow_runtime,$lastDataDate];
        
   }
    
    public function generateCsv(ExportValidation $request){
       $meterData = MeterData::getDataByDate(Carbon::parse($request->end)->toDateString(),Carbon::parse($request->start)->toDateString(),$request->meter_id);
       $data []= null;
       $data[0] = array('Time','Current','Voltage','DC Power','AC Power','Discharge (I/s)','Motor Rpm','Frequency','Cumulative Flow (Liters)','Cumulative Runtime(Minutes)');
       $count = 1;
       $previous_discharge = 0;
       $minute = 0;
       foreach($meterData as $md){
           if($md->current < 0){
               continue;
           }
           $dcpower = $md->dc_power;
           if($md->discharge > 0) {
               $minute = $minute + 1;
           }
           $data[$count++] = array(
               $md->created_at,
               $md->current,
               $md->voltage,
               $dcpower,
               $md->ac_power,
               $md->discharge,
               $md->rpm,
               $md->frequency,
               $previous_discharge = ($md->discharge * 60) + $previous_discharge,
               $minute

           );
       }
       $file = fopen(storage_path('meterdata.csv'), 'w');
       foreach ($data as $row)
       {
           fputcsv($file, $row);
       }

       fclose($file);

       return response()->download(storage_path('meterdata.csv'))->deleteFileAfterSend(true);
   }

   public function filterData($meter_id,Request $request){
       $date = $request->date;
       $indicator = $request->indicator;
       $year = Carbon::parse($date)->format('Y');
       $month = Carbon::parse($date)->format('m');
       $fulldate = Carbon::parse($date);
       $fulldate = $fulldate->addDay(1);
       $fulldate = $fulldate->toDateString();
       $today = Carbon::now()->toDateString();
       $data = [];
       $lastDischarge = MeterData::where('meter_id','=',$meter_id)->where('discharge','>',0)->latest()->first();
       if($lastDischarge){
           $created_at = Carbon::parse($lastDischarge->created_at)->toDateTimeString();
       }else{
           $created_at = null;
       }
       if($indicator == 'daily'){
           if($today == $fulldate){
               $dailyData = MeterData::where('meter_id','=',$meter_id)->whereDate('created_at','=',$fulldate)->where('current','>=',0)->orderBy('created_at','ASC')->get()->toArray();
           }else{
               $dailyData = EnrichMeterData::where('meter_id','=',$meter_id)->whereDate('created_at','=',$fulldate)->where('current','>=',0)->orderBy('created_at','ASC')->get()->toArray();

           }
           return [$dailyData,$created_at];
       } elseif($indicator == 'yearly'){
           $dailyData = DailyMeterData::select(DB::raw('SUM(daily_discharge) as daily_discharge, SUM(daily_runtime) as daily_runtime,  DATE_FORMAT(date, "%Y-%m") as date'))->where('meter_id',$meter_id)->where(DB::raw('YEAR(date)'), '=', $year)->groupBy(DB::raw(' DATE_FORMAT(date, "%Y-%m")'))->get();
       }else{
           $dailyData = DailyMeterData::where('meter_id',$meter_id)->where(DB::raw('YEAR(date)'), '=', $year)->where(DB::raw('MONTH(date)'), '=', $month)->get()->toArray();
       }


       $data[0] = $dailyData;

       return [$data,$created_at];
   }
}
