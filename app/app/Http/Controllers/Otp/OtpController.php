<?php

namespace App\Http\Controllers\Otp;

use App\Http\Requests\ChangeMobileValidation;
use App\Http\Requests\Ogbadmin\Otp\OtpFormValidation;
use App\Repositories\Otp\OtpRepository;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserRegister\UserRegisterRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class OtpController extends Controller
{
    private $userRepository, $userRegisterRepository, $smsRepository, $otpRepository;
    public function __construct(
        UserRepository $userRepository,
        UserRegisterRepository $userRegisterRepository,
        SmsRepository $smsRepository,
        OtpRepository $otpRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userRegisterRepository = $userRegisterRepository;
        $this->smsRepository = $smsRepository;
        $this->otpRepository = $otpRepository;
    }

    public function showOtpForm($user_id)
    {
        $isUserValid = $this->checkIfTheUserIsValid($user_id);
        if(!$isUserValid){
            return redirect()->route('error');
        }
        $user = $this->userRepository->getUser($user_id);
        $otp =  $user->otps()->latest()->first();
        $expire_time = $this->otpRepository->timeBeforeOtpExpire($otp);
        return view('otp.verify-otp',compact('user','expire_time'));
    }

    public function verifyOtp(OtpFormValidation $request,$user_id)
    {
        $isUserValid = $this->checkIfTheUserIsValid($user_id);
        if(!$isUserValid){
            return redirect()->route('error');
        }
        $token = $request->get('digit1') . $request->get('digit2') . $request->get('digit3') . $request->get('digit4') . $request->get('digit5') . $request->get('digit6');
        $validOtp = $this->otpRepository->verifyOtp($user_id, $token);
        if (!$validOtp) {
            $request->session()->flash('error', 'That code is invalid. Please try again.');
            return redirect()->route('verify-otp.form',['user_id' => $user_id] );
        }
        $updatedOtp = $this->otpRepository->updateOtpStatus($validOtp);
        $this->userRepository->updateUserOtpStatus($user_id, $updatedOtp);
        $request->session()->flash('success',  config('messages.user_created'));
        $redirect_route = $this->getRedirectRouteAfterVerification();
        $request->session()->forget('otp_verification_type');
        return redirect()->to($redirect_route);
    }


    public function resendOtp($user_id)
    {
        $isUserValid = $this->checkIfTheUserIsValid($user_id);
        if(!$isUserValid){
            return redirect()->route('error');
        }
        $user = $this->userRepository->getUser($user_id);
        $this->otpRepository->invalidateOtp($user->email);
        $otp = $this->otpRepository->generateOtp($user);
        if($otp){
            $this->smsRepository->sendSmsNotification($user->phone, 'OTP_MESSAGE', ['otp_code' => $otp->otp]);
        }
        return redirect()->route('verify-otp.form', ['user_id' => $user->user_id ]);
    }

    public function changeMobileNumber(ChangeMobileValidation $request,$user_id)
    {
        $phoneNumber = $request->get('phone');
        $user = $this->userRepository->getUser($user_id);
        if($phoneNumber == $user->phone){
            return redirect()->back()->with('error','Do not enter same phone number');
        }
        $user->phone = $phoneNumber;
        $user->save();
        //Invalidate otp if any
        $this->otpRepository->invalidateOtp($user->email);
        //Generate new otp
        $otp = $this->otpRepository->generateOtp($user);
        if($otp){
            $this->smsRepository->sendSmsNotification($user->phone, 'OTP_MESSAGE', ['otp_code' => $otp->otp]);
        }
        $request->session()->flash('success', 'Number changed successfully');
        return redirect()->route('verify-otp.form',$user->user_id);
    }

    private function getRedirectRouteAfterVerification()
    {
        $otp_verification_type = session('otp_verification_type');
        switch ($otp_verification_type){
            case 'partner-user':
                return route('partner-user.login');
            case 'account':
                return route('account.edit', Auth::user()->user_id);
            default:
                return route('login');

        }
    }

    public function checkIfTheUserIsValid($user_id)
    {
        try{
            $user_details = Cookie::get('user_details');
            $user_details = json_decode($user_details,true);
            $validated_user_id = $user_details['user_id'];
        }catch (\Exception $exception){
            return false;
        }
        if($validated_user_id != $user_id){
            return false;
        }
        return true;
    }
}
