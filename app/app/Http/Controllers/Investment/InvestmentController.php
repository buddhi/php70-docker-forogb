<?php

namespace App\Http\Controllers\Investment;

use App\components\ProjectUtil;
use App\Http\Requests\Investment\CheckoutFormValidation;
use App\Jobs\SendInvestmentEmailsToInvestor;
use App\Models\Farm;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Investment\InvestmentRepository;
use App\Repositories\PaymentModule\PaymentModuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvestmentController extends Controller
{
    private $investmentRepository, $paymentModuleRepository,$emailRepository;

    public function __construct(InvestmentRepository $investmentRepository, PaymentModuleRepository $paymentModuleRepository,EmailRepository $emailRepository)
    {
        $this->investmentRepository = $investmentRepository;
        $this->paymentModuleRepository = $paymentModuleRepository;
        $this->emailRepository = $emailRepository;
    }

    //Handles the investment payment
    public function submitCheckout(CheckoutFormValidation $request)
    {
        //Default payment gateway is Paypal.
        //For BackDoor Payment we have assumed paymentgateway as "noPaymentGateway"
        $paymentGateway = $request->get('payment_gateway')? $request->get('payment_gateway'):'paypal';

        $checkoutStatus = true;
        $data = ['request' => $request];
        //Checking the investment is possible for projects.
        //If it is not possible for any project, then entire payment is failed
        $projectPayments = $this->investmentRepository->checkProjectPaymentCheckout($data,$paymentGateway);
        foreach ($projectPayments as $key => $projectPayment) {
            if (in_array('error', $projectPayment, true)) {
                $paymentErrorMessage[$key] = $projectPayment;
                $checkoutStatus = false;
            }
        }

        // redirecting to failure page on payment failure
        if (!$checkoutStatus) {
            return view('investor.payment-failure', compact('paymentErrorMessage'));
        }

        //For payment without payment gateway ie for BackDoor payment
        // Storing investment details and transaction only
        if ($paymentGateway == 'noPaymentGateway') {
            $projectInvestments = $this->investmentRepository->investmentDataStore(['request' => $request->all()], $projectPayments);
            $investmentTransactions = $this->investmentRepository->investmentTransactionStore(null, $projectInvestments, $paymentGateway);
            $request->session()->flash('success','Payment succeeded successfully');
            return redirect()->route('index');
        }

        //For Paypal payment we are fetching the payment routes for project
        $paymentStatus = $this->investmentRepository->submitCheckout($projectPayments, $paymentGateway);

        //If payment fails for some reason
        if (array_key_exists('message_type',$paymentStatus) && $paymentStatus['message_type'] == "error") {
            $request->session()->flash($paymentStatus['message_type'], $paymentStatus['message']);
            return redirect()->route('payment.failure');
        }

        //Storing the investment details in session so that we can save details after the payment
        $request->session()->put('data', ['request' => $request->all()]);
        $request->session()->put('payment', $projectPayments);

        //redirecting to paypal for payment
        return redirect()->away($paymentStatus['route']);
    }

    //Function that stores the invested amount
    //For Paypal payment
    public function getPaymentStatus(Request $request, $paymentGateway)
    {
        /**
         * Http is stateless.
         * We need to store the investment details in session when there is communication between payment gateway  and system.
         * Information about the investment is restored from session.
         */
        $data = $request->session()->pull('data');
        $projectPayments = $request->session()->pull('payment');

        //Getting payment status from paypal
        $paymentStatus = $this->investmentRepository->getPaymentStatus($data, $paymentGateway);

        //If the message type is error, redirecting to failure page
        if ($paymentStatus['message_type'] == 'error' && (!isset($data))) {
            $paymentMessage['message_type'] = $paymentStatus['message_type'];
            $paymentMessage['message'] = $paymentStatus['message'];
            return view('investor.payment-failure', compact('paymentMessage'));
        }

        //If the message type is success , storing investments and  transaction details
        if ($paymentStatus['message_type'] == 'success' && isset($data)) {
            $this->paymentModuleRepository->logPaymentComplete($paymentStatus);
            DB::beginTransaction();
            try {
                $projectInvestments = $this->investmentRepository->investmentDataStore($data, $projectPayments);
                if (!isset($projectInvestments)) {
                    $paymentStatus['message'] = "Error while storing investments";
                    $paymentStatus['message_type'] = "error";
                    DB::rollback();
                }

                $investmentTransactions = $this->investmentRepository->investmentTransactionStore($paymentStatus['result'], $projectInvestments, $paymentGateway);
                if (!isset($investmentTransactions)) {
                    $paymentStatus['message'] = "Error while storing the transactions";
                    $paymentStatus['message_type'] = "error";
                    DB::rollback();
                }

                if(isset($projectInvestments) && isset($investmentTransactions)){
                    $this->sendThankYouEmailToInvestor($projectInvestments);
                    $paymentStatus['message'] = "Transactions successfully completed";
                    $paymentStatus['message_type'] = "success";
                }
            } catch (\Exception $exception) {
                DB::rollback();
            }
            DB::commit();
        }
        $request->session()->flash($paymentStatus['message_type'], $paymentStatus['message']);
        return redirect()->route('payment.success');
    }

    public function getMessage($message_type, $message, $route = null)
    {
        $returnValue['message_type'] = $message_type;
        $returnValue['message'] = $message;
        return $returnValue;
    }

    //Loads the page in success in payment
    public function paymentSuccess()
    {
        return view('investor.payment-success');
    }

    //Loads page on failure in payment
    public function paymentFailure()
    {
        return view('investor.payment-failure');
    }

    //loads the project that has invested by the user
    public function myInvestment()
    {
        return view('investor.my-investment');
    }

    private function sendThankYouEmailToInvestor($projectInvestments)
    {
        dispatch(new SendInvestmentEmailsToInvestor(Auth::user(),$projectInvestments));
    }

    //shows the details investment of particular project
    public function investmentDetail(Request $request, $id)
    {
        //Getting project eager loading other relations
        $project = $this->investmentRepository->getByIdWithRelations($id,['municipality','district','province','user']);
        if(!isset($project)){
            return redirect()->route('error');
        }
        //Getting investment details according to type
        $investmentTotalData = ProjectUtil::getProjectInvestmentTotal($id);
        $projectInvestmentDetail = ProjectUtil::getProjectInvestmentDetail($id);

        //Getting project related details
        $crops = $project->crops;
        $cattles = $project->cattles;
        $farmer = $project->farmers->first();
        $farm = Farm::where('farmer_id',$farmer->id)->first();
        $investor_list = $project->getInvestors();
        $land_array = isset($farm->land)?json_decode($farm->land, true):[];

        //loading new page as always
        if ($request->has('new')) {
            return view('investor/project-fund-new', compact('project', 'investmentTotalData', 'projectInvestmentDetail', 'crops','cattles','farmer','investor_list','farm','land_array'));
        }
        //loading old page for backdoor. This was thought to be needed
        return view('investor/project-fund', compact('project', 'investmentTotalData', 'projectInvestmentDetail', 'crops','cattles','farmer','investor_list'));
    }

    //Checkout page
    public function checkout()
    {
        return view('investor/checkout');
    }

    //Contact developer and admin by investor
    public function contactDeveloper(Request $request, $project_id)
    {
        $text_message = $request->get('message');
        if(strlen($text_message) <= 0){
            return response()->json(self::sendResponse(true,"Please enter some text for sending mail"));
        }
        $isMessageSendSuccessfully = $this->emailRepository->sendMessageToAdminAndDeveloper($text_message,$project_id);
        if(!$isMessageSendSuccessfully){
            return response()->json(self::sendResponse(true,"Mail cannot be sent due to unknown reasons"));
        }
        return response()->json(self::sendResponse(false,'Mail sent successfully. Please check the email'));
    }

    //returns the error response
    private static function sendResponse($error,$message)
    {
        $response['error'] = $error;
        $response['message'] = $message;
        return $response;
    }

}
