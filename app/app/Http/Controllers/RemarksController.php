<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Models\Comment;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class RemarksController extends Controller
{
    //Remarks discussion page
    public function index($project_id)
    {
        $project = Project::with('user')->findOrFail($project_id);
        $remarks = $project->comments->filter(function ($comment) {
            return $comment->comment_type == "remark";
        });

        if (Gate::denies('index', $project)) {
            return redirect()->route('error');
        }
        $default_page = $this->getDefaultPageTitle();
        return view('remark.index', compact('remarks', 'project', 'default_page'));
    }

    //Creates a new remark of project for authenticated user

    public function create(Request $request, $project_id)
    {
        $project = Project::findOrFail($project_id);
        if (Gate::denies('index', $project)) {
            return redirect()->back();
        }
        $request->validate([
            'remark' => 'required|min:5',
        ]);
        $remark = $request->get('remark');
        $project->comments()->create(['comment' => $remark, 'comment_type' => 'remark', 'user_id' => Auth::user()->user_id]);
        $request->session()->flash('success_message', config('messages.remark_success'));
        return redirect()->route('remark.index', $project_id);
    }

    private function getDefaultPageTitle()
    {
        return 'admin.default';

    }
}
