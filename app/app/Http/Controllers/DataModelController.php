<?php

namespace App\Http\Controllers;
use App\Models\Farmer;
use App\Models\Meter;
use App\Models\Project;


class DataModelController extends Controller
{
    /**
     * @param $id
     * @return  Meter models having certain ProjectId in Json Format
     */
    public function getMetersFromProjectId($id)
    {
        $project = Project::findOrFail($id);
        $data['meters'] = $project->meters;
        return response()->json(["meters"=>$data['meters']]);
    }

    /**
     * @param $id
     * @return Project and Farmer model having certain MeterId in Json Format
     */
    public function getProjectAndFarmerFromMeterId($id)
    {
        $meter = Meter::findorFail($id);
        $data['project'] =$meter->Project;
        $data['farmer'] = $meter->farmer;
        return response()->json(["project" => $data['project'], "farmer" => $data['farmer']]);
    }

    /**
     * @param $id
     * @return  Meter models having certain ProjectId in Json Format
     */
    public function getMetersFromFarmerId($id)
    {
        $farmer = Farmer::findOrFail($id);
        $data['meters'] =$farmer->meters;
        return response()->json(["meters"=>$data['meters']]);

    }
}
