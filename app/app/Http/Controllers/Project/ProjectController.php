<?php

namespace App\Http\Controllers\Project;

use App\Enums\ProjectStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\project\SelectPlanValidation;
use App\Http\Requests\project\ProjectFormValidation;
use App\Models\Plan;
use App\Models\Project;
use App\Models\Upload;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\Draft\DraftRepository;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farmer\FarmerRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Upload\UploadRepository;
use Illuminate\Http\Request;


class ProjectController extends Controller
{
    private $projectRepository,$uploadRepository,$farmerRepository,$farmRepository,$cropInfoRepository,$cattleInfoRepository,$draftRepository;
    private static $pagination_limit = 10;
    private $projectStatuses = ProjectStatus::ALL_PROJECT_STATUS;
    private $projectStatusLabelForFrontend = ["Approved", "Installed", "Operational", "Error"];

    public function __construct(
        ProjectRepository $projectRepository,
        UploadRepository $uploadRepository,
        FarmerRepository $farmerRepository,
        FarmRepository $farmRepository,
        CropInfoRepository $cropInfoRepository,
        CattleInfoRepository $cattleInfoRepository,
        DraftRepository $draftRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->uploadRepository = $uploadRepository;
        $this->farmerRepository = $farmerRepository;
        $this->farmRepository = $farmRepository;
        $this->cropInfoRepository  = $cropInfoRepository;
        $this->cattleInfoRepository = $cattleInfoRepository;
        $this->draftRepository = $draftRepository;
    }
    
    //Shows the project survey form
    //Using composer to load other variables
    public function create()
    {
        $this->authorize('create', Project::class); //Using project policy for access controll
        return view('project.create');
    }

    //Stores the project
    public function projectStore(ProjectFormValidation $request)
    {
        $this->authorize('store', Project::class);
        $data = ['request' => $request];
        $form = $data['request']->all();
        //Button: Submit-analysis is saving the project
        //Button: Submit is for saving the project in draft
        if (is_array($form) && in_array('submit-analysis', $form)) {
            $return_value = $this->projectRepository->store($data);
        } else {
            if (!$data['request']->id) {
                $return_value = $this->draftRepository->draftStore($data);
            } else {
                $return_value = $this->draftRepository->draftUpdate($data, $data['request']->id);
            }
        }
        return redirect()->route($return_value['redirect'], ['id' => $return_value['id']]);
    }

    // Edits the project
    public function edit($id)
    {
        //Fetching project
        $project = $this->projectRepository->getById($id);

        //Checking policy to edit the project
        $this->authorize('edit', $project);

        //fetching district for project
        $selected_districts = $this->projectRepository->getDistrict($project);

        //fetching municipality for project
        $selected_municipalities =$this->projectRepository->getMunicipality($project);

        //fetching farmers for project.For now we assume one project-one farmer.
        $farmer = $this->projectRepository->getFarmers($project);

        //Get income and expense details of farmer
        $income_array = $this->farmerRepository->getIncomeArray($farmer->first());
        $expense_array = $this->farmerRepository->getExpenseArray($farmer->first());
        $farm = $this->farmerRepository->getFarm($farmer->first());

        //Getting land details of farm.
        $land_array = $this->farmRepository->getLandArrayFromFarm($farm);

        //Getting crop and cattle details for the project.
        $cropInfoArr = $this->cropInfoRepository->getCropDetailsForProject($project);
        $cattleInfoArray = $this->cattleInfoRepository->getCattleDetailsForProject($project);
        return view('project.create', compact('project', 'cropInfoArr', 'farmer', 'farm',
            'cattleInfoArray','selected_districts','selected_municipalities','income_array','expense_array','land_array'));

    }

    public function draftEdit($id)
    {
        $draftProject = $this->draftRepository->getById($id);
        $this->authorize('edit', $draftProject);

        //Getting draft array
        $array = $this->draftRepository->getDraftArray($draftProject);

        //Getting details from json format value for other models and array
        $project = $this->draftRepository->getProjectDetails($array);
        $farmer = $this->draftRepository->getFarmerDetails($array);
        $farm = $this->draftRepository->getFarmDetails($array);
        $cropInfoArr = $this->draftRepository->getCropDetails($array);
        $cattleInfoArray = $this->draftRepository->getCattleDetails($array);
        $income_array = $this->draftRepository->getIncomeArray($array);
        $expense_array = $this->draftRepository->getExpenseArray($array);
        $land_array = $this->draftRepository->getLandDetails($array);
        if(isset($project->district_id) && isset($project->province_id)) $selected_districts = $project->province->districts;
        if(isset($project->district_id) && isset($project->municipality_id)) $selected_municipalities = $project->district->municipalities;
        return view('draft.create', compact('project', 'cropInfoArr', 'farmer', 'draftProject', 'farm','cattleInfoArray','selected_districts','selected_municipalities','income_array','expense_array','land_array'));
    }
    
    //Updates the project
    public function update(ProjectFormValidation $request, $id)
    {
        $project = $this->projectRepository->getById($id);
        $this->authorize('update', $project);
        $data = ['request' => $request];
        $return_value = $this->projectRepository->update($data, $project);
        return redirect()->route($return_value['route'],['id' => $project->id]);
    }
    
    //Selects the plan
    public function selectPlan($id)
    {
        // upload  file types
        $upload_attributes = config('Upload.project');
        $project = $this->projectRepository->getById($id);
        $this->authorize('selectPlan', $project);

        //getting all plans
        $plans = Plan::all();

        //Getting all the uploaded files
        $uploads = $project->uploadable;

        //fetching monthly water requirement
        $daily_requirement_according_to_month = $this->projectRepository->fetchWaterRequirement($id);

        //selecting project based on closest median
        $selected_plan = $daily_requirement_according_to_month = $this->projectRepository->recommendPlanBasedOnWaterRequirement($daily_requirement_according_to_month,$plans);

        //calculating the credit score of the farmer
        $credit_score = $project->farmers->first()->credit_score;
        return view('project.select-plan', compact('plans', 'project','upload_attributes','selected_plan','credit_score','uploads'));
    }

    //Getting all the files for project
    public function getFiles($project_id){
        $project = $this->projectRepository->getById($project_id);
        $uploads = $project->uploadable;
        return json_encode(['uploads' => $uploads]);
    }

    //Stores the chosen plan in project
    public function selectPlanStore(SelectPlanValidation $request)
    {
        $id = $request->get('project_id');
        $project = $this->projectRepository->getById($id);
        $this->authorize('selectPlan', $project);

        //All types of files mentioned in config  must be uploaded for next phase of project
        $uploaded_attributes = $project->uploadable->pluck('upload_attribute_key')->toArray();
        $upload_categories= config('Upload.project');
        $upload_attributes = array_keys($upload_categories);
        $allFilesUploaded = count(array_unique(array_intersect($uploaded_attributes, $upload_attributes))) == count($upload_attributes);
        if(!$allFilesUploaded){
            $request->session()->flash('error','Please,make sure that you have uploaded all type of fields');
            return redirect()->back();
        }
        //Passing the request to update the plan of project.
        $data = [
            'request' => $request,
        ];
        $this->projectRepository->selectPlanStore($data,$project);
        return redirect()->route('project.list');
    }

    //Uploading file for project
    public function uploadFile(Request $request,$projectId) {
        $upload = $this->uploadRepository->uploadProjectFile($request,$projectId);
        if(isset($upload) && $upload instanceof Upload){
            return response()->json([
                'success' => true,
                'id' => $upload->id
            ], 200);
        }
        return response()->json([
            'success' => false
        ], 500);
    }

    //deletes individual file in select plan page
    public function deleteFile($uploadId)
    {
        $isFileDeleted = $this->uploadRepository->deleteProjectFile($uploadId);
        if($isFileDeleted){
            return response()->json([
                'success' => true,
            ], 200);
        }
        return response()->json([
            'success' => false,
        ], 500);

    }

    //fetching water requirement for project
    public function fetchWaterRequirement($id)
    {
        $water_requirement = $this->projectRepository->fetchWaterRequirement($id);
        return $water_requirement;
    }
   /* public function getAgentProject(){
        $projects = $this->projectRepository->getPaginatedProjects(self::$pagination_limit);
        return $this->returnFromIndexAndFilter($projects);
    }
    public function returnFromIndexAndFilter($projects)
    {
        $province = $this->projectRepository->getAllProvinces();
        $project_type = $this->projectRepository->getAllProjectTypes();
        $project_status = ProjectStatus::ALL_PROJECT_STATUS;
        $plans=Plan::all();
//        $district=$this->projectRepository->getAllDistrict();
//        $value='Baara';
        $dataSets = $this->projectRepository->getDataSetForProjectStatus($this->projectStatusLabelForFrontend);
        return view('ogbadmin.project.index', [
                'projects' => $projects,
                'dataSets' => $dataSets,
                'province' => $province,
                'project_type' => $project_type,
                'project_status' => $project_status,
                'plans'=>$plans,
//                'district'=>$district,
                'projectStatuses' => $this->projectStatusLabelForFrontend
            ]
        );
    }*/

}
