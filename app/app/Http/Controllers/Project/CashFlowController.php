<?php

namespace App\Http\Controllers\Project;

use App\Services\CashFlowCalculation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CashFlowController extends Controller
{
    public function calculateCashFlow($project_id)
    {
        $data = CashFlowCalculation::calculate($project_id);
        return $data;
    }
}
