<?php

namespace App\Http\Controllers\Project;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Mail\AdvancePaymentEmail;
use App\Models\District;
use App\Models\DraftProject;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Models\ProjectInvestment;

class SiteController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('project.list');
        }
        return view('index');
    }

    public function error()
    {
        return view('layouts/errorpage');
    }

    public function loadData()
    {
        $role = session('role');
        $projects = null;
        if (isset($role) && auth()->check()) {
            $projects = $this->getProjectsAccordingToRole($role);
        } else {
            $projects = $this->getProjectsWithFarmerAndDistrict()->where('status', '=', 'new')->get();
        }
        $projects = json_encode($projects, true);
        return $projects;
    }

    //var $project_threshold::Maximum number of projects in index page
    //Priority is set for getting list of projects as mentioned below:
    //Get the Latest partially funded projects
    //Get the non funded projects ready for funding
    //Get any projects in funded stage but not in plan stage.
    public function latestFundedProjects()
    {
        $project_threshold = 4;
        //Getting the latest non-funded projects which are ready for funding
        $latest_invested_project_ids = ProjectInvestment::orderBy('id', 'desc')
            ->whereHas('project', function ($q) {
                $q->where('status', '!=', 'operational')
                    ->where('status', '!=', 'plan')
                    ->where('status', '!=', 'funded');
            })->pluck('project_id')->toArray();
        $latest_invested_project_ids = array_unique($latest_invested_project_ids);
        if (count($latest_invested_project_ids) >= $project_threshold) {
            $placeholders = implode(',', array_fill(0, count($latest_invested_project_ids), '?'));
            $latest_invested_projects = $this->getProjectsWithFarmerAndDistrict()->whereIn('id', $latest_invested_project_ids)->orderByRaw("field(id,{$placeholders})", $latest_invested_project_ids)->take($project_threshold)->get();
            return $latest_invested_projects;
        }

        //Getting the latest projects which are ready for funding + new projects
        $new_project_ids = Project::orderBy('id', 'desc')
            ->whereNotIn('id', $latest_invested_project_ids)
            ->where('status', '!=', 'operational')
            ->where('status', '!=', 'plan')
            ->where('status', '!=', 'funded')
            ->pluck('id')->toArray();
        $latest_invested_new_project_ids = array_merge($latest_invested_project_ids, $new_project_ids);

        if (count($latest_invested_new_project_ids) >= $project_threshold) {
            $placeholders = implode(',', array_fill(0, count($latest_invested_new_project_ids), '?'));
            $latest_invested__new_projects = $this->getProjectsWithFarmerAndDistrict()->whereIn('id', $latest_invested_new_project_ids)->orderByRaw("field(id,{$placeholders})", $latest_invested_new_project_ids)->take($project_threshold)->get();
            return $latest_invested__new_projects;
        }

        //Getting the latest projects which are ready for funding + new projects + funded projects
        $funded_ids = Project::orderBy('id', 'desc')->where('status', 'funded')->whereNotIn('id', $latest_invested_new_project_ids)->pluck('id')->toArray();
        $latest_invested__new_funded_project_ids = array_merge($latest_invested_new_project_ids, $funded_ids);
        if (count($latest_invested__new_funded_project_ids) >= $project_threshold) {
            $placeholders = implode(',', array_fill(0, count($latest_invested__new_funded_project_ids), '?'));
            $latest_invested__new_funded_projects = $this->getProjectsWithFarmerAndDistrict()->whereIn('id', $latest_invested__new_funded_project_ids)->orderByRaw("field(id,{$placeholders})", $latest_invested__new_funded_project_ids)->take($project_threshold)->get();
            return $latest_invested__new_funded_projects;
        }

        //Getting any projects except in plan stage
        return $this->getProjectsWithFarmerAndDistrict()->orderBy('id', 'desc')->where('staus', '!=', 'plan')->take($project_threshold)->get();
    }

    public function loadMapData()
    {
        $projects = Project::select('district_id')->where('status', '!=', 'plan')->get();
        $district_list = $projects->map(function ($project) {
            return $project->district_id;
        });
        $districts_ids = $district_list->toArray();
        $districts = new Collection();
        foreach ($districts_ids as $districts_id){
            $districts = $districts->push(District::find($districts_id));
        }
        return $districts->toJson();
    }

    private function getProjectsAccordingToRole($role, $my_investment_flag = false)
    {
        switch ($role) {
            case Role::DEVELOPER:
                $projects = Project::with('district', 'farmers')->where('created_by', Auth::id())->orderBy('id', 'desc')->paginate(8);
                break;

            case Role::INVESTOR && $my_investment_flag:
                $projects = $this->getProjectsWithFarmerAndDistrict()->whereHas('projectinvestments', function ($query) {
                    $query->where('investor_id', Auth::user()->user_id);
                })->get();
                break;
            case Role::INVESTOR :
                $projects = $this->getProjectsWithFarmerAndDistrict()
                    ->where('status', '=', 'funding')
                    ->orwhereHas('projectinvestments', function ($query) {
                        $query->where('investor_id', Auth::user()->user_id);
                    })
                    ->where('status', '!=', 'error')
                    ->get();
                break;
        }
        return $projects;
    }

    private function getProjectsWithFarmerAndDistrict()
    {
        return Project::select('id', 'status', 'farmer_name', 'district_id', 'cost', 'solar_pv_size')
            ->with(
                [
                    'district' => function ($q) {
                        $q->select('name', 'id');
                    },
                    'farmers' => function ($q) {
                        $q->select('age', 'gender', 'address', 'farmers.id', 'no_of_dependents', 'education', 'credit_score');
                    }
                ]);

    }

    public function loadMyInvestmentProjects()
    {
        $projects = null;
        if (session('role') && auth()->check()) {
            $role = session('role');
            $my_investment_flag = true;
            $projects = $this->getProjectsAccordingToRole($role, $my_investment_flag);
            $projects = json_encode($projects, true);
            return $projects;
        }
        return null;
    }

    public function filterProjects(Request $request)
    {
        $projects = null;
        $draft_project = null;
        $user_id = Auth::user()->user_id;
        $status = $request->status;
        if ($status == 'funded') {
            $projects = Project::where('status', '=', 'funded')->orWhere('status', 'operational')->orderBy('id', 'desc')->paginate(8);

        } elseif ($status == 'draft') {
            $drafts = DraftProject::where('user_id', $user_id)->get();
            $drafts = json_decode($drafts);
            $count = 0;
            foreach ($drafts as $draft) {
                $draft_project[$count] = json_decode($draft->data, true);
                $draft_project[$count]['project'] = json_encode(self::array_push_assoc($draft_project[$count]['project'], 'id', $draft->id));
                $projects[$count] = json_decode($draft_project[$count]['project'], true);
                $count++;
            }
        } else {
            $projects = Project::where('status', '=', $status)->orderBy('id', 'desc')->paginate(8);
        }
        $projects = json_encode($projects, true);

        return $projects;
    }

    public static function array_push_assoc($array, $key, $value)
    {
        $array = json_decode($array, true);
        if (!isset($array['key'])) {
            $array[$key] = $value;
        }
        return $array;
    }

    public function projectList()
    {
        if (!session('role')) {
            return;
        }
        switch (session('role')) {
            case Role::DEVELOPER:
                return view('project.project-list');
            case Role::INVESTOR:
                return view('project.project-list-investor');
            case Role::ADMIN:
                return redirect()->route('admin.project.list');
            default:
                return;
        }
    }

    public function verificationStep(Request $request)
    {
        $admin_verification = $request->get('admin_verification') ? $request->get('admin_verification') : false;
        $email_verification = $request->get('email_verification') ? $request->get('email_verification') : false;
        return view('auth.verification_step', compact('admin_verification', 'email_verification'));
    }

    public function newIndex()
    {
        return view("newIndex");
    }

    public function socialRegister()
    {
        return view("auth.social-register");

    }

    public function socialRegisterWaiting()
    {
        return view("auth.verification-step-social");
    }

    public function verifyEmail()
    {
        return view("auth.email-verification-waiting");
    }
}
