<?php

namespace App\Http\Controllers\Sms;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckSMS;
use App\Http\Requests\SMS\CheckFormValidataion;
use App\Models\Meter;
use App\Repositories\Sms\SmsRepository;

class SmsController extends Controller
{
    private $smsRepository;

    public function __construct(SmsRepository $smsRepository)
    {
        $this->smsRepository = $smsRepository;
    }

    public function changeMeterStatus(CheckSMS $request)
    {
        $value = $request->get('value');
        $meter_id = $request->get('meter_id');
        try {
            $meter = Meter::find($meter_id);
            $phone = $meter->phoneNo;
            $this->smsRepository->sendSmsNotification($phone, 'SMS_CHANGE_STATUS',$substitution = [] , $value);
            if ($value == '4951111') {
                $meter->status = 1;
                $meter->save();
            } elseif ($value = '4951000') {
                $meter->status = 0;
                $meter->save();
            }
            $response = "User notified successfully";
        } catch (\Exception $e) {
            $response = 'SMS Gateway unresponsive';
        }
        return $response;
    }

}
