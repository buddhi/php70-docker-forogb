<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    public function setPasswordForm()
    {
        $user = Auth::user();
        $isPasswordSet = self::checkUserPassword($user);
        if ($isPasswordSet) {
            return redirect()->route('profile.edit');
        }
        return view('auth.passwords.set', compact('user'));
    }

    public function setPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = Auth::user();
        $isPasswordSet = self::checkUserPassword($user);
        if ($isPasswordSet) {
            return redirect()->route('profile.edit');
        }
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->route('profile.edit')->with('success', 'Password is successfully Set');
    }

    private static function checkUserPassword($user)
    {
        if (is_null($user->password) || (mb_strlen($user->password) == 0)) {
            return false;
        }
        return true;
    }
}
