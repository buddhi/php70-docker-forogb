<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Role;
use App\Events\PasswordChange;
use App\Http\Controllers\Controller;
use App\Mail\SendPasswordChangeEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ChangePasswordController extends Controller
{
    public function showChangePasswordForm()
    {
        if(session('role') == Role::PARTNER_USER){
            return view('ogbadmin.passwords.changepassword');
        }
        return view('auth.passwords.changepassword');

    }

    public function changePassword(Request $request)
    {
        // Redirect back if correct old password is not entered
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error", config('messages.old_password_not_match_error'));
        }

        // Redirect back if current password and new password are same
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            return redirect()->back()->with("error", config('messages.same_new_and_old_password_error'));
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        $user = User::findOrFail(Auth::id());
        $user->fill([
            'password' => bcrypt($request->get('new-password'))
        ])->save();
        $request->session()->flash("success", config('messages.password_change'));
        if(session('role') == Role::PARTNER_USER){
            return redirect()->route('account.edit', Auth::user()->user_id);
        }
        Mail::to(Auth::user()->email)->send(new SendPasswordChangeEmail(Auth::user()));
        return redirect()->route('changePassword.form');
    }

}

