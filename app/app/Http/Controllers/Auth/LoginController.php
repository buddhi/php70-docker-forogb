<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\OGB\Constants;
use App\Repositories\RoleVerification\RoleBasedVerificationModule;
use App\Services\LinkedInProvider;
use App\Traits\AuthenticationUtils;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoginController extends Controller
{
    use AuthenticatesUsers, AuthenticationUtils;

    protected $redirectTo = 'project/list';
    private $log;

    public function __construct()
    {
        $this->log = new Logger('Social Login');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/SocialLogin.log')));
        $this->middleware(['guest'])->except('logout');
    }

    //Overriding the default laravel auth login(function) system to handle the soft deleted user
    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        //using getAllUsers to override the user scope
        $user = User::getAllUsers()->where('email', '=' ,$request->get('email'))->first();
        if (isset($user) && $user->isUserDisabled())
        {
            return redirect()->route('contact');

        }

        if ($this->attemptLogin($request)) {

            return $this->sendLoginResponse($request);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }


    //Post Authentication:authentication based on role
    public function authenticated(Request $request)
    {
        //Getting authenticated user roles in an array
        $userRoles = Auth::user()->roles->pluck('role')->toArray();

        // For developer and investor,role value(1,10) is obtained from form page
        // For admin system gets admin roles
        // If the user is both admin and organisation admin ,admin login super seeds organisation admin login
        $role = $request->has('role') ? $request->role : self::checkAndGetValidRoleForAdmin($userRoles);
        $admin_type = $request->get('admin_type') ? $request->get('admin_type') : null;
        //TODO::future ::add another role
        // admin type is added in session for now to separate the admin types
        if (isset($admin_type) && $admin_type == 'ogbadmin') {
            $request->session()->put('admin_type', 'ogbadmin');
        }
        //Checking that user has valid role and logging out user if user doesnt have valid role
        $doesUserHasValidRole = self::roleExists($role, $userRoles);
        if (!$doesUserHasValidRole) {
            $message = 'Error due to Unauthorised access';
            return $this->logoutUserBasedOnRole($request, $role, $message);
        }
        // Developer,investor organisation admin can be verified differently based on their role
        // Checking role based verification(email verification,admin verification or some check based on role)
        $user_verification_based_on_role = RoleBasedVerificationModule::checkUserRoleVerification($role);

        if (!$user_verification_based_on_role['isVerified']) {
            $message = $user_verification_based_on_role['message'];
            if (isset($user_verification_based_on_role['redirect_to'])) {
                return $this->logoutUserBasedOnRole($request, $role, $message, $user_verification_based_on_role['redirect_to']);
            }
            return $this->logoutUserBasedOnRole($request, $role, $message);
        }

        //User with valid roles are allowed to login
        $request->session()->put('role', $role);
        $request->session()->flash('role_message', "You are logged in as " . str_replace('_', ' ', returnRole($role)));

    }

    //Logout function that logs out user during authentication
    public function logoutUserBasedOnRole($request, $role, $flash_message = null,$redirect_to= null)
    {
        $logoutRoute = self::getLogOutRoute($role);
        $this->guard()->logout();
        self::flushPreviousSession();
        $request->session()->invalidate();
        if ($redirect_to){
            return redirect()->route($redirect_to);
        }
        return redirect($logoutRoute)->with('error', $flash_message);
    }

    //Logout function that helps in logout from any page
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $role = session('role');
        $admin_type = session('admin_type');
        self::flushPreviousSession();
        if (isset($admin_type) && $admin_type == 'ogbadmin' && $role == Role::ADMIN) {
            return redirect()->route('ogbadmin.login');
        }
        if ($role == Role::ADMIN) {
            return redirect()->route('admin.login');
        }
        if ($role == Role::INVESTOR) {
            return redirect()->route('index');
        }
        if ($role == Role::PARTNER_USER) {
            return redirect()->route('partner-user.login');
        }
        $request->session()->invalidate();
        return redirect()->route('login');
    }


    /**
     * function:Visits the Social site and gets the details
     * @param Request $request
     * @param $provider :Social sites: facebook,google,linkedin and twitter
     * @return mixed: redirects to url path with user details
     */
    public function redirectToProvider(Request $request ,$provider)
    {
        if($provider == 'linkedin'){
            return $this->fetchUpdatedLinkedInProvider($request)->redirect();;
        }
        return Socialite::driver($provider)->redirect();;
    }


    //Todo::future Buid a separate service for social login
    //Think of more fail cases,forget and reset password
    /**
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     *
     *This function gets the user details from the social accounts
     * If the user has already signed in, then user can log in as investor.
     * If the social account has missing email,user is rejected,
     * If the social account is not registered then user is redirect to social-sign up page with email
     * After social registration the user is redirected to admin verification page.
     * @throws \Exception
     */
    public function Callback(Request $request, $provider)
    {
        //get the user detail from social login provider
        try {
            $userSocial = $this->getSocialDetails($provider,$request);
        } catch (\Exception $exception) {
            $this->log->info($exception->getMessage());
            return redirect()->route('index')->with('error', 'Sorry you can not log in due to some reasons.Please try again');
        }

        //users are redirected to index page if the user are not fetched from provider service
        if (!isset($userSocial)) {
            return redirect()->route('index')->with('error', 'Sorry you can not log in due to some reasons.Please try again');;
        }

        //User with no email in social account(registered with phone no) are rejected and redirected.
        if (!$this->checkIfSocialAccountHasEmail($userSocial)) {
            return redirect()->route('login')->with('error', 'You need to have emails in your social account to register or login into the system');
        }

        //Fetch the user in database
        $user = User::where(['email' => $userSocial->getEmail($userSocial)])
            ->where('provider', $provider)
            ->where('provider_id', $userSocial->id)
            ->first();

        //If there no such user ,then redirect user to user registration page
        if (!$user) {
            $user_mail_existence = $this->checkUserEmailExistence($userSocial);
            if ($user_mail_existence['status']) {
                $request->session()->flash('error', 'User is already registered. ' . $user_mail_existence['message']);
                return redirect()->route('index');
            }
            $request->session()->put('user-email', $this->getEmail($userSocial));
            return view('auth.social-register', compact('userSocial', 'provider'));
        }

        //For valid user,they are allowed to login
        $role = Role::INVESTOR;
        Session::put('role', $role);
        $userRoles = $user->roles->pluck('role')->toArray();

        //Checking that user has valid role and logging out user if user doesnt have valid role
        $doesUserHasValidRole = self::roleExists($role, $userRoles);
        if (!$doesUserHasValidRole) {
            $message = 'Error due to Unauthorised access';
            return $this->logoutUserBasedOnRole($request, $role, $message);
        }

        //Users are allowed to login
        Auth::login($user);

        //If the password is not set ,then user are prompt to set password after login
        if (is_null($user->password) || (mb_strlen($user->password) == 0)) {
            return redirect()->route('password.set.form');
        }
        return redirect()->route('project.list');
    }

    private function getSocialDetails($provider,$request)
    {
        /**
         *Since the twitter still implements oAuth1.0
         * so twitter doesnt support stateless
         */

        if ($provider == 'twitter')
            return Socialite::driver($provider)->user();
        if($provider == 'linkedin'){
            return $this->fetchUpdatedLinkedInProvider($request)->stateless()->user();
        }
        return Socialite::driver($provider)->stateless()->user();
    }

    private function checkIfSocialAccountHasEmail($userSocial)
    {
        if ($this->getEmail($userSocial)) {
            return true;
        }
        return false;
    }

    public function getEmail($userSocial)
    {
        return $userSocial->email;
    }


    //Checking email detail obtained from social account with that in database
    //But if the user has already registered with email that exist in another social account
    //Then user is redirected to index page instead of user registration page
    public function checkUserEmailExistence($userSocial)
    {
        $mail_existence = [];
        $mail_existence['status'] = false;
        $email = $this->getEmail($userSocial);
        $user = User::getAllUsers()->where('email', $email)->first();
        if (isset($user)) {
            $mail_existence['status'] = true;
            $mail_existence['message'] = "Please, login using email and password ";
            if (isset($user->provider)) {
                $mail_existence['message'] = "Please, login via " . $user->provider;
            }
        }
        return $mail_existence;
    }

    public function redirectTo()
    {
        $role = session('role');
        $admin_type = session('admin_type');
        if (auth::check()) {
            if (isset($admin_type) && $admin_type == 'ogbadmin' && $role == Role::ADMIN) {
                return 'ogbadmin/project';
            }
            if ($role == Role::ADMIN) {
                return 'admin/project';
            }
            if ($role == Role::PARTNER_USER) {
//                return 'partner-user/agent/' . Auth::user()->partner_id;
                return 'partner-user/project';
            }
            return 'ogbadmin/project';

        }
        return '/';
    }

    private static function flushPreviousSession()
    {
        Session::forget('role');
        Session::flush();
    }

    public function buildProvider($provider, $config,$request)
    {
        $app = app();
        return new $provider(
            $app['request'], $config['client_id'],
            $config['client_secret'], $this->formatRedirectUrl($config),
            Arr::get($config, 'guzzle', [])
        );
    }

    protected function formatRedirectUrl(array $config)
    {
        $redirect = value($config['redirect']);
        return Str::startsWith($redirect, '/')
            ? $this->app['url']->to($redirect)
            : $redirect;
    }

    private function fetchUpdatedLinkedInProvider($request)
    {
        $config = config('services.linkedin');
        return $this->buildProvider(
            LinkedInProvider::class, $config,$request
        );

    }
}