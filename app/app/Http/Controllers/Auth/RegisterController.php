<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Role;
use App\Enums\Verification;
use App\Http\Requests\User\SocialRegistrationValidation;
use App\Http\Requests\User\UserRegistrationValidation;
use App\Jobs\NotifyAdminsToActivateUser;
use App\Jobs\SendActivateUserEmails;
use App\Jobs\SendUserActivation;
use App\Jobs\SendVerificationEmail;
use App\Mail\SendActivateUserEmailsToAdmin;
use App\Models\RoleUser;
use App\Models\VerifyUser;
use App\OGB\Constants;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/project/list';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function verifyUser(Request $request, $token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        $user_id = $verifyUser->user_id;
        $user = User::findOrFail($user_id);
        if (isset($verifyUser) && isset($user)) {
            if ((!isset($user->verified) || (!$user->verified))) {
                $user->verified = Verification::EMAIL_VERIFIED;
                $user->save();
            }
            $message = config('messages.verification_email_success_developer');
            if ($user->hasRole(Role::INVESTOR)) {
                $message = config('messages.verification_email_success_investor');
            }
            $request->session()->flash('success', $message);
            $sender = env('MAIL_ADMIN', 'noreply@ghampower.com');
            dispatch(new SendUserActivation($sender, $user));
            return redirect()->route('index');

        } else {
            $request->session()->flash('error', 'Invalid request');
        }
        return redirect('/login');
    }

    public function register(UserRegistrationValidation $request)
    {
        event(new Registered($user = $this->create($request->all())));
        dispatch(new SendVerificationEmail($user));
        session()->flash('error', config('messages.email_not_verified_message'). $user->email);
        return redirect()->route('verify.email');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User;
        $user = User::create([
            'username' => $data['email'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'full_name' => $data['full_name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'verified' => array_key_exists('verified', $data) ? $data['verified'] : Verification::EMAIL_NOT_VERIFIED,
            'admin_verification' => array_key_exists('admin_verification', $data) ? $data['admin_verification'] : Verification::ADMIN_VERIFIED,
            'investment_type' => null,
            'anonymous' => Constants::NOT_ANONYMOUS,
            'delete_flg' => array_key_exists('delete_flg', $data) ? $data['delete_flg'] : Constants::OFF,
            'disabled' => array_key_exists('disabled', $data) ? $data['disabled'] : Constants::OFF,
            'partner_id' => array_key_exists('partner_id', $data) ? $data['partner_id'] : null
        ]);
        foreach ($data['role'] as $role) {
            $userRole = new RoleUser();
            $userRole->user_id = $user->user_id;
            $userRole->role = $role;
            $userRole->save();
        }

        VerifyUser::create([
            'user_id' => $user->user_id,
            'token' => Carbon::now()->timestamp . str_random(40)
        ]);
        return $user;
    }

    public function registerSocialUser(SocialRegistrationValidation $request)
    {
        $image_url = $request->get('image_url');
        if (!isset($image_url)) {
            return redirect()->route('login');
        }
        $fileContents = file_get_contents($image_url);
        $image_extension = pathinfo(parse_url($image_url, PHP_URL_PATH), PATHINFO_EXTENSION);
        $profile_image = rand(1, 99999) . strtotime("now") . '_'."social_image_".$request->get('provider_id').".".$image_extension;
        Storage::disk('local')->put('public/upload/'.$profile_image,$fileContents);
        $user_email = $request->session()->pull('user-email');
        if (!isset($user_email)) {
            return redirect()->route('login');
        }
        $user = User::create([
            'full_name' => $request->full_name,
            'username' => $user_email,
            'email' => $user_email,
            'phone' => $request->phone ? $request->phone : null,
            'verified' => Verification::EMAIL_VERIFIED,
            'admin_verification' => Verification::ADMIN_VERIFIED,
            'provider_id' => $request->provider_id,
            'provider' => $request->provider,
            'profile_image' => $profile_image,
            'anonymous' => Constants::NOT_ANONYMOUS,
            'country_code' => $request->get('country_code')? $request->get('country_code'): Constants::DEFAULT_COUNTRY_CODE
        ]);

        $userRole = new RoleUser();
        $userRole->user_id = $user->user_id;
        $userRole->role = Role::INVESTOR;
        $userRole->save();
        $sender = env('MAIL_ADMIN', 'noreply@ghampower.com');
        dispatch(new SendUserActivation($sender, $user));

        /**This feature is temporarily disabled,Feature::sends emails and redirects to admin verification template
         * dispatch(new NotifyAdminsToActivateUser($user));
         * return redirect()->route('social.register.waiting', $user);
         **/
        $request->session()->flash('success', "You are successfully registered with {$request->provider} . Please login with your {$request->provider} account.");
        return redirect()->route('index');
    }

}