<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\ApiPayment\ApiPaymentRepository;
use Illuminate\Http\Request;

class ApiPaymentController extends Controller
{
    public $transactionModuleRepository;
    public $paymentGateway;
    public $esewaPaymentLogger;
    public $apiPaymentRepository;

    public function __construct(ApiPaymentRepository $apiPaymentRepository)
    {
        $this->apiPaymentRepository = $apiPaymentRepository;
    }

    // Inquiry API
    public function inquiry(Request $request, $request_id = null)
    {
        $input_param = $request->all();

        if (isset($request_id)) {
            $input_param = array_merge(
                ['request_id' => $request_id],
                $request->all()
            );
        }

        return $this->apiPaymentRepository->paymentInquiry($input_param);
    }

    //Payment API
    public function payment(Request $request)
    {
        $input_param = $request->all();
        return $this->apiPaymentRepository->payment($input_param);
    }

}
