<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IngestController extends Controller
{

    public function meterLog(Request $request)
    {
      Log::info("Log from meter", ['request' => $request]);
      return response()->json($request);
    }

    public function ingestHttpMeterData(Request $request){
        $meterdatas = $request->all();
        $data = fopen(storage_path("http_meter_data.txt"), "a");
        fwrite($data,Carbon::now()->toDateString());
        foreach($meterdatas as $meterdata){
            fwrite($data,', ');
            fwrite($data,$meterdata);
            fwrite($data," ");
        }
        fwrite($data,"\n");
        fclose($data);
        return response('200');
    }

    public function showMeterData()
    {
        $data = fopen(storage_path("http_meter_data.txt"),'r');
        while(!feof($data)) {
            echo fgets($data) . "<br>";
        }
    }

    public function downloadMeterData(){
        return response()->download(storage_path('http_meter_data.txt'));

    }
}
