<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function admin()
    {

        return view('admin.login');
    }

    public function ogbAdminLogin()
    {
        return view('ogbadmin.login');
    }

    public function partnerUserLogin()
    {
        return view('ogbadmin.user.login');
    }

}