<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Image\ImageFormValidation;
use App\Models\Farm;
use App\Models\FarmerProject;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    protected $folder_path;
    
    public function __construct()
    {
        $this->folder_path = storage_path() .DIRECTORY_SEPARATOR.'app'. DIRECTORY_SEPARATOR . 'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
    }
    
    public function store(ImageFormValidation $request, $id)
    {
        $project_id =$id;
        $farmProject = FarmerProject::select('id')->where('id','=',$project_id)->first();
        $farm= Farm::select('id','farm_image')->where('id','=',$farmProject->id)->first();
        $file = $request->file('farm_image');
        if ($request->hasFile('farm_image')) {
            if ($farm->farm_image){
                if(file_exists($this->folder_path.$farm->farm_image))
                    unlink($this->folder_path.$farm->farm_image);
            }
            self::createFolderIfNotExist($this->folder_path);
            $file_name = rand(1, 99999) .strtotime("now").'_' . $file->getClientOriginalName();
            $file->move($this->folder_path, $file_name);
            
            $farm->update([
                'farm_image' =>$file_name,
            ]);
        }
        return redirect()->route('meterdata_demo.show',['id'=>$id]);
    }
    
    protected function createFolderIfNotExist($path)
    {
        if (!file_exists($path)) {
            mkdir($path);
        }
    }
}
