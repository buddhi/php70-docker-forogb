<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjectPlan\ProjectPlanFormValidation;
use App\Models\Project;
use App\Repositories\ProjectPlan\ProjectPlanRepository;
use App\Http\Controllers\Controller;

class ProjectPlanController extends Controller
{
    private $projectPlanRepository;

    public function __construct(ProjectPlanRepository $projectPlanRepository)
    {
        $this->projectPlanRepository = $projectPlanRepository;
    }

    //Project-plan list
    public function listProjectsWithPlans()
    {
        $projects = $this->projectPlanRepository->getAllProjectsWithComments();
        return view('admin.plan.index', compact('projects'));
    }

    //Open edit for the project-plan
    public function editProjectPlan($id)
    {
        $project = $this->projectPlanRepository->getProject($id);
        return view('admin.plan.edit', compact('project'));
    }

    //Updates the project-plan
    public function updateProjectPlan(ProjectPlanFormValidation $request, $id)
    {
        $this->projectPlanRepository->updateProjectPlan($request,$id);
        $request->session()->flash('success_message', 'Plan is updated successfully!!');
        return redirect()->route('admin.project.list');
    }
}
