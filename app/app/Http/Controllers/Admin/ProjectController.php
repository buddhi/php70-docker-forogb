<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\project\ProjectFormValidation;

use App\Models\Project;
use App\Http\Controllers\Controller;
use App\Models\Upload;
use App\Models\ProjectInvestment;
use App\ProjectFilter;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farmer\FarmerRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Project\ProjectStateUpdateRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ZipArchive;

class ProjectController extends Controller
{
    protected $project_model, $projectRepository, $projectStateUpdateRepository, $farmerRepository,$farmRepository,$cropInfoRepository,$cattleInfoRepository;

    public function __construct(
        ProjectRepository $projectRepository,
        ProjectStateUpdateRepository $projectStateUpdateRepository,
        FarmerRepository $farmerRepository,
        FarmRepository $farmRepository,
        CropInfoRepository $cropInfoRepository,
        CattleInfoRepository $cattleInfoRepository
    ) {
        $this->projectRepository = $projectRepository;
        $this->projectStateUpdateRepository = $projectStateUpdateRepository;
        $this->farmerRepository = $farmerRepository;
        $this->farmRepository = $farmRepository;
        $this->cropInfoRepository  = $cropInfoRepository;
        $this->cattleInfoRepository = $cattleInfoRepository;
    }

    public function index(Request $request)
    {
        $filterStatus = $request->get('filter')? $request->get('filter'):'active';
        $data['row'] =  Project::filter($request);
        return view('admin.project.index', compact('data', 'filterStatus'));
    }


    //Opens the edit form for project
    public function edit($id)
    {
        //Fetching project
        $project = $this->projectRepository->getById($id);

        //Using policy to edit the project
        $this->authorize('edit', $project);

        //fetching district for project
        $selected_districts = $this->projectRepository->getDistrict($project);

        //fetching municipality for project
        $selected_municipalities =$this->projectRepository->getMunicipality($project);

        //fetching farmers for project.For now we assume one project-one farmer.
        $farmer = $this->projectRepository->getFarmers($project);

        //Get income and expense details of farmer
        $income_array = $this->farmerRepository->getIncomeArray($farmer->first());
        $expense_array = $this->farmerRepository->getExpenseArray($farmer->first());
        $farm = $this->farmerRepository->getFarm($farmer->first());

        //Getting land details of farm.
        $land_array = $this->farmRepository->getLandArrayFromFarm($farm);

        //Getting crop and cattle details for the project.
        $cropInfoArr = $this->cropInfoRepository->getCropDetailsForProject($project);
        $cattleInfoArray = $this->cattleInfoRepository->getCattleDetailsForProject($project);
        return view('admin.project.edit', compact('project', 'cropInfoArr', 'farmer', 'farm',
            'cattleInfoArray','selected_districts','selected_municipalities','income_array','expense_array','land_array'));

    }

    public function update(ProjectFormValidation $request, $id)
    {
        $data['request'] = $request;
        $project = $this->projectRepository->getById($id);
        $this->projectRepository->update($data, $project);
        $request->session()->flash('success_message', 'project is updated successfully!!');
        return redirect()->route('admin.project.list');
    }


    //Making the project operational
    public function projectMakeOperational($id)
    {
        $project = Project::find($id);
        $this->authorize('projectMakeOperational', $project);
        $project->status = "operational";
        $project->save();
        return redirect()->route('project.detail', ['id' => $project->id]);
    }

    public function downloadFile($upload_id)
    {
        $upload = Upload::findOrFail($upload_id);
        $base_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        if (isset($upload)) {
            $path_to_file = $base_path . $upload->file_name;
            if (file_exists($path_to_file)) {
                return response()->download($path_to_file);
            }
        }
        return redirect()->route('admin.project-plan.list');
    }


    public function downloadAllFiles($id)
    {
        $project = Project::findOrFail($id);
        $farmer = $project->farmers->first();
        $uploaded_files = $project->uploadable;
        $temporary_location = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'download' . DIRECTORY_SEPARATOR;
        self::createFolderIfNotExist($temporary_location);
        $zipFileName = $farmer->farmer_name . '.zip';
        $zip = new ZipArchive;
        $base_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        if ($zip->open($temporary_location . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            foreach ($uploaded_files as $file) {
                if (isset($file)) {
                    $file_path = $base_path . $file->file_name;
                    if (file_exists($file_path)) {
                        $zip->addFile($file_path, $file->file_name);
                    }
                }
            }
            $zip->close();
        }
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $zip_file_path = $temporary_location . $zipFileName;
        if (file_exists($zip_file_path)) {
            return Response::download($zip_file_path, $zipFileName, $headers);
        }
        return Redirect::back();
    }

    protected function createFolderIfNotExist($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, $mode = 0755, true, true);
        }
    }

    public function disable($id)
    {
        $project = Project::getAllProjects()->where('id', $id)->first();
        if (!isset($project)) {
            return redirect()->route('admin.project.list');
        }
        if ($project->delete_flg)
            $project->delete_flg = 0;
        else
            $project->delete_flg = 1;
        $project->save();
        return redirect()->route('admin.project.list');
    }

    public function investorDetail($id)
    {
        $project = Project::findOrFail($id);
        $projectInvestments = ProjectInvestment::with('user')->where('project_id', $id)->orderBy('created_on', 'desc')->get();
        return view('admin.investor.detail', compact('projectInvestments', 'project'));
    }

    public function stateUpdate(Request $request, $project_id)
    {
        $updated_state = $this->projectStateUpdateRepository->stateUpdate($request, $project_id);
        if ($updated_state['route'] == 'admin.meter.create' || $updated_state['route'] == 'admin.project-plan.edit')
            return redirect()->route($updated_state['route'], $project_id);
        return redirect()->route($updated_state['route'], $project_id)->with($updated_state['message_type'], $updated_state['message']);
    }

    public function transitionStateUpdate(Request $request)
    {
        $project_id = $request->get('project_id');
        $response['error'] = true;
        $project = Project::find($project_id);
        $state_id = $request->get('state_id');
        $state_id_array []= $state_id;
        $value = $request->get('value');
        if(isset($project) && isset($value) && isset($state_id)){
            $response['error'] = false;
            $response['message'] = 'Status cannot be updated';
        }
        $sync_data = syncData($state_id_array,$value);
        $project->transitionstate()->sync($sync_data, false);
        return response()->json(json_encode($response));
    }

}