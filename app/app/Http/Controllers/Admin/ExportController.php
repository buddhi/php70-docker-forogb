<?php

namespace App\Http\Controllers\Admin;

use App\Models\CattleInfo;
use App\Models\Crop;
use App\Models\CropInfo;
use App\Models\Farm;
use App\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ExportController extends Controller
{
    public function projectCsvExport()
    {
        $array =[];
        $array[0] = array(
            'FarmerId','Farmer Name', 'Age',
            'Gender','District','Municipality',
            'ward_no','Village Name', 'gps',
            '# of People in Household','No of dependents', 'Contact no',
            'occupation','Education', 'Certification',
            'Farming income1','Non farming income1','Non farming income2','Non farming income3',
            'Total Monthly Income (Rs)',
            'Total Monthly Expenses (Rs)',
            'Monthly expense1','Monthly expense2','Monthly Expense3',
            'Additional Income Source (Y/N)',
            'Plan to provide water to farmers','Revenues from water distribution','Additional Household Member w/Salaried Occupation? (Y/N)',
            'Monthly Savings (Y/N)', 'Monthly Savings (Rs)','Access to Financial Institution (Y/N)', 'Access to Financial Institution (Name)',
            'Monthly Debt Servicing Obligation (Rs)','Recent Payment Plan Purchase? (Y/N)','Planned Interuptions to Cash Flow due to capex? (Y/N)',
            'Nearest Goods Selling Point (Name)','Distance to Goods Selling Point (km)','Land Ownership (Y/N)',
            'Expected Monthly Solar Loan Payment (Rs)'
        );

        //Heading set lands
        array_push($array[0] ,
            'Wet Land-ropani','Wet Land-katha','Wet Land-bigaha','Wet Land-own/lease','Wet Land-owner','Wet Land-relation',
            'Dry Land-ropani','Dry Land-katha','Dry Land-bigaha','Dry Land-own/lease','Dry Land-owner','Dry Land-relation',
            'Total-land-ropani','Total-land-katha','Total-land-bigaha','Total-land-own/lease','Total-land-owner','Total-land-relation'
        );

        // Heading for crops
        $excel_sheets_crop_id = [30,14,19,13,32,15,34,35,36];
        $excel_sheets_crops = Crop::whereIn('id',$excel_sheets_crop_id)->orderBy(DB::raw('FIELD(`id`, '.implode(',', $excel_sheets_crop_id).')'))->get();
        foreach ($excel_sheets_crops as $crop){
            array_push($array[0],$crop->name.'-Farming Area(Hectares))',$crop->name.'-Cultivate Period',$crop->name.'-Profitability',$crop->name.'-Plantation Time');
        }
        array_push($array[0],'Other-Farming Area(Hectares))','other-Cultivate Period','other-Profitability','other-Plantation Time','Grain-Other-Farming Area(Hectares))','Grain-Other-Cultivate Period','Grain-Other-Profitability');


        //Heading for cropstorage and livestock
        array_push($array[0],'Is this farmer only growing grains? (Y/N)','Cow/Buffalo','Goat','Fish','Chicken/Duck');

        //Heading for Farms
        array_push($array[0],'Way to Store Excess Crop Inventory (Y/N)?',
            'Current Water Source (Boring, Pond, River, Well)','Existing Irrigation System (Diesel Pump, Electric Pump, Canal)', 'Water Requirements (liters per day)',
            'Ground Water Level (ft)','Distance from Source to Farm (meters)', 'Boring Size (inch)',
            'Vertical Head (ft)','Depth of Boring (ft)','Type of Soil','Fuel');

        $projects = Project::with('district','municipality','province','farmers')->get();
        $count=1;

        foreach($projects as $key => $project) {
            $farmer = $project->farmers->first();
            $farm= Farm::where('id','=',$farmer->id )->first();
            $cattleInfoArray= CattleInfo::with('cattle')->where('project_id',$project->id)->get();
            $cropInfoArray= CropInfo::with('crops')->where('project_id',$project->id)->get();
            $cattles = $this->getCattle($cattleInfoArray);
            $land_array = isset($farm->land)?json_decode($farm->land,true):null;
            $land_array =$this->getLand($land_array);
            $crop_array =$this->getCrops($cropInfoArray,$excel_sheets_crops);
            $array[$count] =
                array(
                    isset($farmer->id)?$farmer->id:null,
                    isset($project->farmer_name)?$project->farmer_name:null,
                    isset($farmer->age)? $farmer->age:null,

                    isset($farmer->gender)? $farmer->gender:null,
                    isset($project->district->name)? $project->district->name:null,
                    isset($project->municipality->name)? $project->municipality->name:null,

                    isset($project->ward_no)? $project->ward_no:null,
                    isset($farmer->village_name)? $farmer->village_name:null,
                    //TODO::Add gps field
                    isset($project->gps)? $project->gps:null,//

                    isset($farmer->no_of_people_in_house)? $farmer->no_of_people_in_house:null,
                    isset($farmer->no_of_dependents)? $farmer->no_of_dependents:null,
                    isset($farmer->contact_no)? $farmer->contact_no:null,

                    //TODO::make occupation field
                    isset($farmer->occupation)? $farmer->occupation:'farmer',//
                    isset($farmer->education)? $farmer->education? 'Y':'N':null,
                    isset($farmer->certification)? $farmer->education? 'Y':'N':null,


                    isset($farmer->annual_household_income)? $farmer->annual_household_income/12:null,

                    //Non farming incomes
                    null,null,null,

                    isset($farmer->annual_household_income)? $farmer->annual_household_income/12:null,

                    //TODO::Expense details
                    isset($farmer->annual_expenses)? $farmer->annual_expenses/12:null,
                    isset($farmer->annual_expenses)? $farmer->annual_expenses/12:null,

                    //Non farming expenses
                    null,null,

                    isset($farmer->additional_income_source)? $farmer->additional_income_source? 'Y':'N':null,
                    isset($farmer->water_distribution_to_farmers)? $farmer->water_distribution_to_farmers? 'Y':'N':null,
                    isset($farmer->revenues_from_water_distribution)?$farmer->revenues_from_water_distribution:null,
                    isset($farmer->additional_salaried_occupation)?$farmer->additional_salaried_occupation?'Y':'N':null,

                    isset($farmer->monthly_saving)?$farmer->monthly_saving? 'Y':'N':null,
                    isset($farmer->monthly_saving_amount)? $farmer->monthly_saving_amount:null,
                    isset($farmer->has_bank)?$farmer->has_bank? 'Y':'N':null,
                    isset($farmer->financial_institute)? $farmer->financial_institute:"none",

                    isset($farmer->debt_monthly_payment)? $farmer->debt_monthly_payment:0,
                    isset($farmer->recent_large_purchase_through_payment_plan)?$farmer->recent_large_purchase_through_payment_plan?'Y':'N':null,
                    isset($farmer->future_capital_expenditure_causing_cash_flow_interruptions)? 'Y':'N',

                    isset($farmer->nearest_market)? $farmer->nearest_market:null,
                    isset($farmer->distance_to_nearest_market)? $farmer->distance_to_nearest_market:null,
                    isset($farmer->land_owned)? $farmer->land_owned? 'Y':'N':null,
                    isset($farmer->expected_solar_loan_monthly_payment)? $farmer->expected_solar_loan_monthly_payment:null,
                );

            foreach ($land_array as $land){
                $array[$count] =array_merge($array[$count],$land);
            }

            $array[$count] =array_merge($array[$count],$crop_array);
            array_push($array[$count],
                isset($farmer->has_cereal_grain)?$farmer->has_cereal_grain? 'Y':'N':null,
                array_key_exists('cow_buffalo_count',$cattles)? $cattles['cow_buffalo_count']:null,
                array_key_exists('goat_count',$cattles)? $cattles['goat_count']:null,
                array_key_exists('fish_count',$cattles)? $cattles['fish_count']:null,
                array_key_exists('chicken_duck_count',$cattles)? $cattles['chicken_duck_count']:null,
                isset($farmer->crop_inventory)?$farmer->crop_inventory? 'Y':'N':null,
                isset($farm->current_irrigation_source)? array_search($farm->current_irrigation_source,config('SystemVariable.CurrentIrrigationSource')):null,
                isset($farm->current_irrigation_system)? array_search($farm->current_irrigation_system,config('SystemVariable.CurrentIrrigationSystem')):null,
                isset($farm->water_requirement)? $farm->water_requirement:null,
                isset($farm->ground_water_level)? $farm->ground_water_level:null,
                isset($farm->distance)? $farm->distance:null,
                isset($farm->boring_size)? $farm->boring_size:null,
                isset($farm->vertical_head)? $farm->vertical_head:null,
                isset($farm->boring_depth)? $farm->boring_depth:null,
                isset($farm->type_of_soil)? $farm->type_of_soil:null,
                isset($farmer->fuel_monthly_expense)? $farmer->fuel_monthly_expense:null
            );
            $count++;
        }

        $file = fopen(storage_path('project.csv'), 'w');
        foreach ($array as $row)
        {
            fputcsv($file, $row);

        }
        fclose($file);
        return response()->download(storage_path('project.csv'))->deleteFileAfterSend(true);
    }

    public function getCattle($cattleInfoArray)
    {
        $cow_buffalo_count=$goat_count=$fish_count = $chicken_duck_count =0;
        foreach ($cattleInfoArray as $cattle){
            switch($cattle->cattle_id){
                case 1:
                    $cow_buffalo_count+= $cattle->number;
                    break;
                case 2:
                    $goat_count+= $cattle->number;
                    break;

            }
        }
        return array('cow_buffalo_count'=>$cow_buffalo_count,'goat_count'=>$goat_count,'fish_count'=>$fish_count,'chicken_duck_count'=>$chicken_duck_count);
    }

    public function getTotalAndGrainCrops()
    {
        $total_and_grains= [0,0,0,0,0,0,0];
        return $total_and_grains;
    }

    public function getCrops($cropInfos,$excel_sheets_crops)
    {
        $crop_details =[];
        $area = $total_area = $other_vegetable_area= 0;
        $excel_sheets_crops_area = 0;
        if(isset($excel_sheets_crops) && count($excel_sheets_crops)>0){
            foreach ($excel_sheets_crops as $excel_crop){
                $area= 0;
                if(is_array($cropInfos) && count($cropInfos)>0){
                    foreach ($cropInfos as $crop_info){
                        if($excel_crop->id==$crop_info->crop_id)
                            $area +=floatval($crop_info->area);

                    }
                }
                $excel_sheets_crops_area += $area;
                array_push($crop_details,$area,0,0,0);
            }
        }
        $total_area_array = $cropInfos->map(function($crop_info){
            return $crop_info->area;
        })->toArray();
        $total_area = is_array($total_area_array)? array_sum($total_area_array):0;
        $other_vegetable_area =$total_area-$excel_sheets_crops_area;
        array_push($crop_details,$other_vegetable_area,0,0,0,0,0,0);
        return $crop_details;
    }

    public function getLand($land_array)
    {
        $dry_land_array =$wet_land_array=[];
        if(isset($land_array) && is_array($land_array) && count($land_array)>0){
            foreach ($land_array as $land){
                if($land['land_type'] =="dry")
                    $dry_land_array [] = $land;
                else
                    $wet_land_array[]=$land;
            }
        }
        $wet_land =$this->landFormatAccordingToUnit('wet_land',$wet_land_array);
        $dry_land =$this->landFormatAccordingToUnit('dry_land',$dry_land_array);
        $total_land=$this->landFormatAccordingToUnit('total_land',$land_array);
        return array($wet_land,$dry_land,$total_land);
    }

    public function landFormatAccordingToUnit($land_type,$land_array)
    {
        $ropani=$bigaha=$katha=0;
        if(isset($land_array) && is_array($land_array) && count($land_array)>0){
            foreach ($land_array as $land){
                $unit_type =$land['unit_type'];
                switch ($unit_type) {
                    case 'ropani':
                        $ropani += $land['area'];
                        break;
                    case 'katha':
                        $katha += $land['area'];
                        break;
                    case 'bigaha':
                        $bigaha += $land['area'];
                        break;
                }

            }
        }
        return array($land_type.'_ropani'=>$ropani, $land_type.'_katha'=>$katha, $land_type.'_bigaha'=>$bigaha, $land_type.'_ownership'=>null, $land_type.'_owner'=> null, $land_type.'_relation'=>null);

    }
}
