<?php

namespace App\Http\Controllers\Admin;

use App\Models\Project;
use App\Models\Risk;
use App\Models\RiskAttribute;
use App\Services\CreditScore;
use App\Http\Controllers\Controller;

class CreditScoreController extends Controller
{
    public function creditScore($project_id)
    {
        $credit_score_array = [];
        $project = Project::findOrFail($project_id);
        $farmer = $project->farmers->first();
        $creditScore = new CreditScore();
        $credit_score_array = $creditScore->creditScore($farmer->id);
        return view('admin.creditscore.index',compact('credit_score_array','project'));
    }

    public function creditScoreWeight()
    {
        $risk_collection = Risk::with('attributes.categories')->get();
        return view('admin.creditscore.weight',compact('risk_collection'));
    }

    public function creditScoreAttributeWeight($attribute_id)
    {
        $attribute = RiskAttribute::with('categories')->findOrFail($attribute_id);
        return view('admin.creditscore.attribute.weight',compact('attribute'));
    }
}
