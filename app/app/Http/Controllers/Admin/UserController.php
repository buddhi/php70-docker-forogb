<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserEditValidation;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function userList(Request $request)
    {
        $filterStatus = $request->get('filter')? $request->get('filter'): 'active';
        $users = $this->userRepository->getUserList($request);
        return view('admin.user.index', compact('users','filterStatus'));
    }

    public function editUser($user_id)
    {
        $user = $this->userRepository->getUser($user_id);
        return view('admin.user.edit', compact( 'user'));
    }

    public function updateUser(UserEditValidation $request, $user_id)
    {
        $user = $this->userRepository->updateUser($request,$user_id);
        if($user){
            $request->session()->flash('success_message', 'User is updated');
        }
        return redirect()->route('admin.user.list');
    }

    public function changeStatus(Request $request,$user_id)
    {
        $isStatusChanged = $this->userRepository->changeStatus($request,$user_id);
        if($isStatusChanged){
            $request->session()->flash('success_message', 'User status is updated');
        }
        return redirect()->back();
    }

    public function resendVerificationEmail(Request $request ,$user_id)
    {
        $isVerificationEmailSent  = $this->userRepository->reSendVerificationEmail($request,$user_id);
        if($isVerificationEmailSent){
            $user = $this->userRepository->getUser($user_id);
            $request->session()->flash('success_message', config('messages.verification_email_success').$user->full_name);
        }
        return redirect()->back();
    }
}
