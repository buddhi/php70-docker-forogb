<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MeterType;
use App\Http\Requests\Meter\AddMeterFormValidation;
use App\Http\Requests\Meter\EditMeterFormValidation;
use App\Http\Controllers\Controller;
use App\Repositories\Meter\MeterRepository;

class MeterController extends Controller
{
    protected  $meterRepository;
    public function __construct(MeterRepository $meterRepository)
    {
        $this->meterRepository = $meterRepository;
    }
    public function meterList()
    {
        $meters = $this->meterRepository->getMetersList();
        return view('admin.meter.index', compact('meters'));
    }

    public function create($project_id)
    {
        $farmer_project = $this->meterRepository->getFarmerProjectModelAssociatedWithMeter($project_id);
        $farmer_project_id = $farmer_project->id;
        $head = $this->meterRepository->getVerticalHead($project_id);
        return view('admin.meter.create', compact('farmer_project_id', 'head'));
    }
    
    public function store(AddMeterFormValidation $request)
    {
        $meter = $this->meterRepository->storeMeter($request);
        if($meter){
            $request->session()->flash('success_message', 'Meter is created successfully');
        }
        return redirect()->route('admin.project.list');
    }

    
    public function edit($id)
    {
        $meter = $this->meterRepository->getMeter($id);
        return view('admin.meter.edit',compact('meter'));
    }
    
    public function update(EditMeterFormValidation $request,$id)
    {
        $meter = $this->meterRepository->updateMeter($request,$id);
        if($meter){
            $request->session()->flash('success_message', 'Meter is updated successfully');
        }
        return redirect()->route('admin.meter.list');
    }
}
