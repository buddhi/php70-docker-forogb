<?php
namespace App\Http\Controllers;

use App\Enums\PaymentType;
use App\Models\District;
use App\Models\Municipality;
use App\Models\Project;
use App\Models\Province;

class DataController extends Controller
{

    public function districtsForProvince($province_id)
    {
      $districts = District::select(['id', 'name'])
        ->where('province_id', $province_id)
        ->get();
      return response()->json($districts);
    }

    public function municipalityForDistrict($district_id)
    {
      $municipals = Municipality::select(['id', 'name'])
        ->where('district_id', $district_id)
        ->get();
      return response()->json($municipals);
    }

    public function projectDetailsForPayment($payment_id)
    {
        $project = Project::where('payment_id', $payment_id)->first();
        if(!$project->isAdvanceAmountPaid()){
            return response()->json([
                'amount' => $project->advance_amount,
                'payment_type' => PaymentType::ADVANCE
            ]);
        }
        return response()->json([
            'amount' => $project->emi_amount,
            'payment_type' => PaymentType::EMI,

        ]);

    }
}
