<?php

namespace App\Http\Controllers\Profile;

use App\OGB\Constants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        $user = Auth::user();
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'full_name' => 'required|string|max:255',
        ]);
        $user = Auth::user();
        $user->full_name = $request->get('full_name');
        $user->phone = $request->get('phone') ? $request->get('phone') : null;
        $user->address = $request->get('address') ? $request->get('address') : null;
        $user->country_code = $request->get('country_code') ? $request->get('country_code') : null;
        $user->anonymous = $request->get('anonymous') ? Constants::NOT_ANONYMOUS : Constants::ANONYMOUS;
        $user->save();
        return redirect()->route('profile.edit');
    }

    public function uploadProfileImage(Request $request)
    {
        $user = Auth::user();
        if ($request->hasFile('file')) {
            if (isset($user->profile_image) && file_exists($this->folder_path . $user->profile_image) && ($user->profile_image != ''))
                unlink($this->folder_path . $user->profile_image);
            $image = $request->file('file');
            $image_extension = $image->getClientOriginalName();
            self::createFolderIfNotExist($this->folder_path);
            $profile_image = rand(1, 99999) . strtotime("now") . '_' . "social_image_" . $image_extension;
            Image::make($image)
            ->fit(150, 150, function ($constraint) {
                $constraint->upsize();
            })->save($this->folder_path.DIRECTORY_SEPARATOR.$profile_image);
            $user->update([
                'profile_image' => $profile_image,
            ]);
            $response ['message'] = 'Profile Image uploaded successfully';
            $response ['profile_image'] = $user->profile_image;
            return response()->json($response);
        }
        $response ['message'] = 'Sorry ,the image can  not be uploaded.Please try again';
        return response()->json($response);
    }

    protected function createFolderIfNotExist($path)
    {
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
    }

    public function deleteProfileImage()
    {
        $user = Auth::user();
        if (isset($user) && isset($user->profile_image)){
            if(file_exists($this->folder_path.$user->profile_image))
                unlink($this->folder_path.$user->profile_image);
            $user->profile_image = null;
            $user->save();
            return response()->json([
                'success' => true,
            ], 200);
        }
        return response()->json([
            'success' => false,
        ], 500);

    }

}
