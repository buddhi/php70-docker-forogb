<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Enums\OtpVerificationType;
use App\Enums\Role;
use App\Http\Requests\User\UserCreationValidation;
use App\Models\UserRegister;
use App\Repositories\Otp\OtpRepository;
use App\Repositories\Partner\PartnerRepository;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\Token\TokenRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserRegister\UserRegisterRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class PartnerUserController extends Controller
{
    private $userRepository, $userRegisterRepository, $smsRepository, $partnerRepository, $otpRepository, $tokenRepository;
    private static $pagination_limit = 10;

    public function __construct(
        UserRepository $userRepository,
        UserRegisterRepository $userRegisterRepository,
        SmsRepository $smsRepository,
        PartnerRepository $partnerRepository,
        OtpRepository $otpRepository,
        TokenRepository $tokenRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userRegisterRepository = $userRegisterRepository;
        $this->smsRepository = $smsRepository;
        $this->partnerRepository = $partnerRepository;
        $this->otpRepository = $otpRepository;
        $this->tokenRepository = $tokenRepository;
    }

    public function index(Request $request)
    {
        $this->authorize('view',User::class);
        $partners = $this->partnerRepository->getAllVerified();
        $filterStatus = $request->get('filter')? $request->get('filter'): 'valid';
        if($filterStatus == 'valid'){
            $users = $this->userRepository->getPartnerUsers(self::$pagination_limit);
        } else {
            $users = $this->userRegisterRepository->getPendingUsersWithRole($role = Role::PARTNER_USER, self::$pagination_limit);
        }
        $disabled_partners = $this->partnerRepository->getDisabledPartners();
        $partner_id = Auth::user()->partner_id;
        return view('ogbadmin.user.index', compact('users', 'partners', 'filterStatus', 'disabled_partners','partner_id'));
    }

    //form to create a new user
    public function create()
    {
        return view('ogbadmin.user.create');
    }

    //store a new user
    public function storeUser(UserCreationValidation $request)
    {
        $user_register_id = $request->get('user_register_id');

        $userRegister = $this->userRegisterRepository->getById($user_register_id);
        //fetch user details from userRegister ie temporary user details
        if (!$userRegister) {
            $request->session()->flash('error', config('messages.user_cannot_be_created'));
            return redirect()->route('ogbadmin.partner-user.index');
        }

        //create user
        $user = $this->userRepository->createUser($userRegister, $request->all());
        if (!$user) {
            $request->session()->flash('error', config('messages.user_cannot_be_created'));
            return redirect()->route('partner-user.create');
        }

        //update role for user
        $this->userRepository->updateRoleForUser($user, $userRegister->role);

        //Invalidate old otp if any
        $this->otpRepository->invalidateOtp($userRegister->email);

        //Generate new otp
        $otp = $this->otpRepository->generateOtp($user);

        //hard delete the old pending user
        $this->userRegisterRepository->delete($userRegister->id);

        //send otp to users
        $this->smsRepository->sendSmsNotification($user->phone, 'OTP_MESSAGE', ['otp_code' => $otp->otp]);
        if ($otp) {
            $this->userRepository->storeCookie($user);
            $request->session()->put('otp_verification_type', OtpVerificationType::PARTNER_USER_VERIFICATION);
            return redirect()->route('verify-otp.form', $user->user_id);
        }
        return redirect()->route('ogbadmin.user.index');
    }

    //Stores the temp user
    public function store(Request $request)
    {
        //validating the email and partner
        $validator = Validator::make($request->input(), array(
            'email' => 'bail|required|email|unique:app_user,email',
            'partner_id' => 'required|exists:partners,id'
        ));

        //Validation fails: send error response
        if ($validator->fails()) {
            return $this->responseMessage($validator->errors(), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //TODO::future use transaction
        //Generate a random token
        $token = $this->tokenRepository->generateToken();
        if (!$token) {
            return $this->responseMessage(config('messages.invalid_token'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //adding a role
        $input_data = array_merge($request->all(), ['role' => Role::PARTNER_USER]);

        //create a temp-user
        $userRegister = $this->userRegisterRepository->registerUserTemporarily($input_data);
        if (!$userRegister) {
            return $this->responseMessage(config('messages.user_cannot_be_created'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //bind temp-user with token
        $isTokenBindedWithModel = $this->tokenRepository->bindModelWithToken($userRegister, $token);
        if (!$isTokenBindedWithModel) {
            return $this->responseMessage('Error', true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $partner = $this->partnerRepository->getById($userRegister->partner_id);
        if (!$partner) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->userRegisterRepository->sendUserVerificationEmail($userRegister, $token, $partner);
        return $this->responseMessage(config('messages.email_success'), false, Response::HTTP_OK);
    }

    public function verifyEmail(Request $request, $token)
    {
        if (is_null($token)) {
            $request->session()->flash('error', 'Invalid token!!');
            return redirect()->route('ogbadmin.user.index');
        }
        //verify email
        $userRegister = $this->userRegisterRepository->verifyEmail($token);
        //Redirecting to create the actual user
        if ($userRegister instanceof UserRegister) {
            $request->session()->flash('success', 'Email verified successfully.');
            return view('ogbadmin.user.create', compact('userRegister'));
        }
        return redirect()->route('error');
    }

    //TODO::create a helper to be globally available
    public function responseMessage($message, $error_status, $status_code)
    {
        return response()->json([
            'error' => $error_status,
            'messages' => $message,
        ], $status_code);
    }

    public function dashboard()
    {
        return redirect()->route('ogbadmin.project.index');
//        return view('ogbadmin.user.dashboard');
    }

    public function changeStatus(Request $request, $partner_user_id)
    {
        $isStatusUpdated = $this->userRepository->changeStatus($request, $partner_user_id);
        if (!$isStatusUpdated) {
            return $this->responseMessage('User status cannot be changed', true, Response::HTTP_OK);
        }
        $partner_user = $this->userRepository->getUserIncludingTrashedUser($partner_user_id);
        return response()->json([
            'error' => false,
            'messages' => "{$partner_user->full_name} is  changed to {$partner_user->status}",
            'user_status' => ucfirst($partner_user->status)
        ], Response::HTTP_OK);
    }

    public function editUser($partner_user_id)
    {
        $user = $this->userRepository->getUser($partner_user_id);
        return view('ogbadmin.user.edit-user', compact('user'));

    }

    public function updateUser(Request $request,$id){
        $user = $this->userRepository->getUser($id);
        if (isset($user) && $user->isUserUpdateAble($user->email)) {
            $user->update($request->all());
        }
        return redirect()->route('ogbadmin.partner-user.index');

    }
    
    public function resendVerificationEmail(Request $request,UserRegister $userRegister){
//        $userRegister = $this->userRegisterRepository->registerUserTemporarily($input_data);
        if (!$userRegister) {
            return $this->responseMessage(config('messages.user_cannot_be_created'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $token = $userRegister->tokens;
        if (!$token) {
            return $this->responseMessage(config('messages.invalid_token'), true, Response::HTTP_BAD_REQUEST);
        }
        //bind temp-user with token
//        $isTokenBindedWithModel = $this->tokenRepository->bindModelWithToken($userRegister, $token);
        $token = $this->tokenRepository->modifyToken($token);
        if (!$token) {
            return $this->responseMessage('Error', true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $partner = $this->partnerRepository->getById($userRegister->partner_id);
        if (!$partner) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->userRegisterRepository->sendUserVerificationEmail($userRegister, $token, $partner);
        return $this->responseMessage(config('messages.email_success'), false, Response::HTTP_OK);
    }

    public function changeImage(Request $request)
    {
        $image=$this->userRepository->changeProfileImage($request);
      if($image){
          return response()->json([
              'error' => false,
              'messages' => 'User image is  updated successfully',
              'image' => $image
          ], Response::HTTP_OK);
      }

    }
}