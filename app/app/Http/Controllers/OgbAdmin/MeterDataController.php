<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Models\Farm;
use App\Models\Meter;
use App\Repositories\MeterData\MeterDataInterpolationService;
use App\Repositories\Project\ProjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MeterDataController extends Controller
{
    public $meterDataInterpolationService, $projectRepository;
    public function __construct(MeterDataInterpolationService $meterDataInterpolationService, ProjectRepository $projectRepository)
    {
        $this->meterDataInterpolationService=$meterDataInterpolationService;
        $this->projectRepository=$projectRepository;
    }

    public function index($project_id){
        $farmer_project_id = DB::table('farmer_project')->where('project_id',$project_id)->first();
        $farmer_project_id = $farmer_project_id->id;
        $meter = Meter::where('farmer_project_id',$farmer_project_id)->first();
        $meter_id = $meter->id;
        $farm= Farm::select('id','farm_image')->where('id','=',$farmer_project_id )->first();
        \JavaScript::put([
            'meter_id' => $meter_id
        ]);
        $image = $this->projectRepository->getFarmerImage($project_id);
        return view('ogbadmin.meterdata.meterdata_demo',compact('meter_id','farm','project_id','meter','image','project_id'));
    }
}
