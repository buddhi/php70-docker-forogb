<?php

namespace App\Http\Controllers\OgbAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function markReadForNotification()
    {
        auth()->user()->unreadNotifications->markAsRead();
    }
}
