<?php
namespace App\Http\Controllers\OgbAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Meter\AddMeterFormValidation;
use App\Http\Requests\Meter\EditMeterFormValidation;
use App\Models\Project;
use App\Repositories\Meter\MeterRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MeterController extends Controller
{
    protected  $meterRepository;
    public function __construct(MeterRepository $meterRepository)
    {
        $this->meterRepository = $meterRepository;
    }
    public function create($project_id){
        $farmer_project = $this->meterRepository->getFarmerProjectModelAssociatedWithMeter($project_id);
        $farmer_project_id = $farmer_project->id;
        $head = $this->meterRepository->getVerticalHead($project_id);
        return view('ogbadmin.meter.create-meter',compact('farmer_project_id','head','project_id'));
    }

    public function store(AddMeterFormValidation $request){
        $meter = $this->meterRepository->storeMeter($request);
        if($meter){
            $request->session()->flash('success_message', 'Meter is created successfully');
        }
        return redirect()->route('ogbadmin.project.details',['id' => $meter->project->id ]);
    }

    public function edit($id)
    {
        $meter = $this->meterRepository->getMeter($id);
        return view('ogbadmin.meter.edit-meter',compact('meter'));
    }

    public function update(EditMeterFormValidation $request,$id)
    {
        $meter = $this->meterRepository->updateMeter($request,$id);
        if($meter){
            $request->session()->flash('success_message', 'Meter is updated successfully');
        }
        return redirect()->route('ogbadmin.project.details',['id' => $meter->project->id]);
    }
}