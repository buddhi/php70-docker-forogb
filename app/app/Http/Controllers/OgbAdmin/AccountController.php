<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Enums\OtpVerificationType;
use App\Http\Requests\ChangeMobileValidation;
use App\Http\Requests\Ogbadmin\Account\AccountFormValidation;
use App\Http\Requests\Ogbadmin\Otp\OtpFormValidation;
use App\Repositories\Otp\OtpRepository;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Webpatser\Uuid\Uuid;

class AccountController extends Controller
{
    public $userRepository ,$otpRepository ,$smsRepository;
    public function __construct( UserRepository $userRepository , OtpRepository $otpRepository ,SmsRepository $smsRepository)
    {
        $this->userRepository = $userRepository;
        $this->otpRepository = $otpRepository;
        $this->smsRepository = $smsRepository;

    }
    public function edit($user_id)
    {
        $user = Auth::user();
        return view('ogbadmin.user.edit',compact('user'));
    }

    public function update(AccountFormValidation $request,$user_id)
    {
        $user = $this->userRepository->getUser($user_id);
        if (isset($user) && $user->isUserUpdateAble($user->email)) {
            $user->update($request->all());
        }
        return redirect()->route('account.edit',$user->user_id);
    }

    public function uploadImage(Request $request)
    {
        //Validating the partner-logo is image
        $validator = Validator::make($request->all(), ['file' => 'required | image', 'user_id'=> 'required']);
        if ($validator->fails())
            return response()->json([
                'error' => true,
                'messages' => 'Please, enter the valid image',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        $user_id = $request->get('user_id');
        $user = Auth::user();
        $isImageUpdated = $this->userRepository->uploadProfileImage($user,$request);
        if($isImageUpdated){
            return response()->json([
                'error' => false,
                'messages' => 'User image is  updated successfully',
                'image' => $user->profile_image
            ], Response::HTTP_OK);
        }
        return response()->json([
            'error' => true,
            'messages' => 'Error while updating profile image',
            'image' => $user->profile_image
        ], Response::HTTP_INTERNAL_SERVER_ERROR);

    }

    public function showChangePasswordForm()
    {
        return view('auth.passwords.changepassword');
    }

    public function changeMobile(Request $request)
    {
        $user = Auth::user();
        return view('partner-user.account.change-mobile', compact('user'));
    }

    public function storeNumber(ChangeMobileValidation $request)
    {
        $user = Auth::user();
        $phoneNumber = $request->get('phone');
        if($phoneNumber == $user->phone){
            return redirect()->back()->with('error','Do not enter same phone number');
        }
        //invalid any otp if there
        $this->otpRepository->invalidateOtp($user->email);
        //Generate a new otp
        $otp = $this->otpRepository->generateOtp($user);
        //send sms
        $this->smsRepository->sendSmsNotification($request->get('phone'), 'OTP_MESSAGE', ['otp_code' => $otp->otp]);
        $this->userRepository->update($user,['phone' => $request->get('phone')]);
        $this->userRepository->storeCookie($user);
        $request->session()->put('otp_verification_type', OtpVerificationType::ACCOUNT_VERIFICATION);
        return redirect()->route('verify-otp.form', ['user_id' => $user->user_id ]);
    }
}
