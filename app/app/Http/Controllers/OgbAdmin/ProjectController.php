<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Enums\EducationLevel;
use App\Enums\ExpenseSource;
use App\Enums\IncomeSource;
use App\Enums\PaymentType;
use App\Enums\Role;
use App\Http\Requests\Ogbadmin\Investment\InvestmentFormValidation;
use App\Http\Requests\Ogbadmin\Project\PlanValidation;
use App\Http\Requests\Payment\PaymentFormValidation;
use App\Models\Category;
use App\Models\Cattle;
use App\Models\Crop;
use App\Models\Document;
use App\Models\Guideline;
use App\Models\Plan;
use App\Models\Project;
use App\Models\Projectcomment;
use App\Models\ProjectTimeline;
use App\Models\Status;
use App\Models\Upload;
use App\OGB\Constants;
use App\Repositories\ApiPayment\ApiPaymentRepository;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farmer\FarmerRepository;
use App\Repositories\Investment\InvestmentRepository;
use App\Repositories\Project\GuideLineRepository;
use App\Repositories\Upload\UploadRepository;
use App\Repositories\User\UserRepository;
use App\Scope\ProjectScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Project\ProjectRepository;
use App\Enums\ProjectStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Ogbadmin\Project\FormValidation;
use Exception;


class ProjectController extends Controller
{
    private $projectRepository, $guideLineRepository, $uploadRepository, $investmentRepository, $apiPaymentRepository, $userRepository, $farmerRepository, $farmRepository, $cropInfoRepository, $cattleInfoRepository;
    private static $pagination_limit = 10;
    private $projectStatuses = ProjectStatus::ALL_PROJECT_STATUS;
    private $projectStatusLabelForFrontend = ["Approved", "Installed", "Operational", "Error"];
    protected $folder_path;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ProjectRepository $projectRepository, UploadRepository $uploadRepository, GuideLineRepository $guideLineRepository, InvestmentRepository $investmentRepository, ApiPaymentRepository $apiPaymentRepository, UserRepository $userRepository, FarmerRepository $farmerRepository, FarmRepository $farmRepository, CattleInfoRepository $cattleInfoRepository, CropInfoRepository $cropInfoRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->uploadRepository = $uploadRepository;
        $this->guideLineRepository = $guideLineRepository;
        $this->investmentRepository = $investmentRepository;
        $this->apiPaymentRepository = $apiPaymentRepository;
        $this->userRepository = $userRepository;
        $this->farmerRepository = $farmerRepository;
        $this->farmRepository = $farmRepository;
        $this->cropInfoRepository = $cropInfoRepository;
        $this->cattleInfoRepository = $cattleInfoRepository;
        $this->folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'project_details' . DIRECTORY_SEPARATOR;

    }

    public function index()
    {

        $projects = $this->projectRepository->getPaginatedProjects(self::$pagination_limit);
        return $this->returnFromIndexAndFilter($projects);
    }

    public function returnFromIndexAndFilter($projects)
    {
        $province = $this->projectRepository->getAllProvinces();
        $project_type = $this->projectRepository->getAllProjectTypes();
        $project_status = ProjectStatus::ALL_PROJECT_STATUS;
        $plans = Plan::all();
//        $district=$this->projectRepository->getAllDistrict();
//        $value='Baara';
        $dataSets = $this->projectRepository->getDataSetForProjectStatus($this->projectStatusLabelForFrontend);
        return view('ogbadmin.project.index', [
                'projects' => $projects,
                'dataSets' => $dataSets,
                'province' => $province,
                'project_type' => $project_type,
                'project_status' => $project_status,
                'plans' => $plans,
//                'district'=>$district,
                'projectStatuses' => $this->projectStatusLabelForFrontend
            ]
        );
    }

    public function getProjectInformation($id)
    {
        return $this->projectRepository->getProjectData($id);
    }

    public function filter(Request $request)
    {
        // $total_project =$this->projectRepository->totalProject();
        $projects = $this->projectRepository->filterProject($request->all());
        // return view('ogbadmin.project.index',compact('projects'));
        return $this->projectRepository->filterProject($request->all());;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = $this->projectRepository->getAllProvinces();
        $projectTypes = $this->projectRepository->getAllProjectTypes();
        $education_level = EducationLevel::ALL;
        $income_sources = IncomeSource::ALL;
        $expense_sources = ExpenseSource::ALL;
        $crops = Crop::orderBy('name')->get();
        $cattles = Cattle::all();
//        return view('ogbadmin.project.create', compact('provinces', 'projectTypes'));
        return view('ogbadmin.project.project', compact('provinces', 'projectTypes', 'crops', 'cattles', 'education_level','income_sources','expense_sources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormValidation $request)
    {
        $data = ['request' => $request];
        $result = $this->projectRepository->store($data);
        if ($result) {
            return redirect()->route($result['redirect'], ['id' => $result['id']]);
        } else {
            return back()->with('error', 'Something wrong');
        }
    }

    public function selectPlan($id)
    {
        $waterQuantity = [];
        $upload_attributes = config('Upload.project');
        $project = $this->projectRepository->getById($id);
//        $this->authorize('selectPlan', $project);
        $plans = Plan::all();
        $uploads = $project->uploadable;
        $daily_requirement_according_to_month = $this->projectRepository->fetchWaterRequirement($id);
        foreach ($daily_requirement_according_to_month as $key => $req) {
            $waterQuantity[] = $req;
        }

        $selected_plan = $daily_requirement_according_to_month = $this->projectRepository->recommendPlanBasedOnWaterRequirement($daily_requirement_according_to_month, $plans->where('name','!=', 'Mini Pump'));
        $credit_score = $project->farmers->first()->credit_score;

        return view('ogbadmin.project.selectplan', compact('plans', 'upload_attributes', 'project', 'selected_plan', 'daily_requirement_according_to_month', 'waterQuantity'));
    }

    public function selectPlanStore(Request $request)
    {
        $id = $request->get('project_id');
        $project = $this->projectRepository->getById($id);
        $uploaded_attributes = DB::table('documents')
            ->join('category_document', 'documents.id', '=', 'category_document.document_id')
            ->join('categories', 'category_document.category_id', '=', 'categories.id')
            ->where('documents.project_id', $request->project_id)
            ->pluck('categories.attribute_key')->toArray();
//        dd($uploaded_attributes);
        $upload_categories = config('Upload.project');
        $upload_attributes = array_keys($upload_categories);
        $allFilesUploaded = count(array_unique(array_intersect($uploaded_attributes, $upload_attributes))) == count($upload_attributes);

        if (!$allFilesUploaded) {
            $request->session()->flash('error', 'Please,make sure that you have uploaded all type of fields');
            return redirect()->back();
        }
        $data = [
            'request' => $request,
            'project' => $project,
        ];
        $this->projectRepository->selectPlanStore($data);
        return redirect()->route('ogbadmin.project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
    }

    public function editProjectData($id)
    {
        //Fetching project

        $project = $this->projectRepository->getById($id);
        //Checking policy to edit the project
//        $this->authorize('edit', $project);

        //fetching district for project
        $selected_districts = $this->projectRepository->getDistrict($project);
        //fetching municipality for project
        $selected_municipalities = $this->projectRepository->getMunicipality($project);

        //fetching farmers for project.For now we assume one project-one farmer.
        $farmer = $this->projectRepository->getFarmers($project);
        //Get income and expense details of farmer
        $income_array = $this->farmerRepository->getIncomeArray($farmer->first());

        $expense_array = $this->farmerRepository->getExpenseArray($farmer->first());
        $farm = $this->farmerRepository->getFarm($farmer->first());

        //Getting land details of farm.
        $land_array = $this->farmRepository->getLandArrayFromFarm($farm);
        //Getting crop and cattle details for the project.
        $cropInfoArr = $this->cropInfoRepository->getCropDetailsForProject($project);

        $cattleInfoArray = $this->cattleInfoRepository->getCattleDetailsForProject($project);
//        dd($cattleInfoArray);
        $provinces = $this->projectRepository->getAllProvinces();

        $projectTypes = $this->projectRepository->getAllProjectTypes();
        $education_level = EducationLevel::ALL;
        $income_sources = IncomeSource::ALL;
        $expense_sources = ExpenseSource::ALL;
        $crops = Crop::all();
        $cattles = Cattle::all();
        return view('ogbadmin.project.edit-project', compact('project', 'cropInfoArr', 'farmer', 'farm',
            'cattleInfoArray', 'selected_districts', 'selected_municipalities', 'income_array', 'expense_array', 'land_array',
            'provinces', 'projectTypes', 'education_level', 'crops', 'cattles','income_sources','expense_sources'));

    }


    public function updateProjectData(Request $request, $id)
    {
        $project = $this->projectRepository->getById($id);
        // $this->authorize('update', $project);
        $data = ['request' => $request];
        $return_value = $this->projectRepository->updateProjectData($data, $project);
//        return redirect()->route($return_value['route'],['id' => $project->id]);
        return redirect()->route($return_value['route'], ['id' => $project->id])->with('success_message', 'Project Information Updated Successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $data = ['request' => $request];
        $this->projectRepository->updateProjectInformation($data, $id);
        return response()->json([
            'status' => 1,
            'message' => 'successful updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllDistricts($province_id)
    {
        $districts = $this->projectRepository->getDistricts($province_id);
        return $districts;
    }

    public function getAllMunicipalities($municipality_id)
    {
        $municipalities = $this->projectRepository->getMunicipalities($municipality_id);
        return $municipalities;
    }


    public function changeStatus($id)
    {
        $project_id = $id;
//        $project = $this->projectRepository->getById($project_id);
//        dd($project);/
        $isStatusChanged = $this->projectRepository->changeStatus($id);
        if (!$isStatusChanged) {
            return 'Failure';
        } else {
            return 'Success';
        }
    }

    public function projectDetails($id)
    {
        $project = $this->projectRepository->getById($id);
        $farmer = $project->farmers->first();
        $project_status = ProjectStatus::ALL_PROJECT_STATUS;
        $image = $this->projectRepository->getFarmerImage($id);
        $documents = $project->documents;
        $getUsersInvestment = $this->projectRepository->getUsersInvestment($project);
        $payments = $this->apiPaymentRepository->getPaymentForProject($project);

        $paymentData = [];
        if (!$project->isAdvanceAmountPaid()) {
            $paymentData = [
                'amount' => $project->advance_amount,
                'payment_type' => PaymentType::ADVANCE
            ];
        } else {
            $paymentData = [
                'amount' => $project->emi_amount,
                'payment_type' => PaymentType::EMI,

            ];
        }
//        dd($getUsersInvestment);
        return view('ogbadmin.project.project_details', compact('project', 'farmer', 'project_status', 'image', 'documents', 'getUsersInvestment', 'paymentData', 'payments'));
    }

    public function getPaymentHistory(Project $project)
    {
        $payments = $this->apiPaymentRepository->getPaymentForProject($project);

        $paymentData = [];
        if (!$project->isAdvanceAmountPaid()) {
            $paymentData = [
                'amount' => $project->advance_amount,
                'payment_type' => PaymentType::ADVANCE
            ];
        } else {
            $paymentData = [
                'amount' => $project->emi_amount,
                'payment_type' => PaymentType::EMI,

            ];
        }
        return response()->json([
            'paymentData' => $paymentData,
            'payments' => $payments->toArray(),
            'dueDate' => $project->nextDueDate()
        ]);
    }

    public function changeProjectStatus(Request $request)
    {
//        DB::beginTransaction();
        $status = $request->value;
        $id = $request->id;
        $this->projectRepository->changeProjectStatus($status, $id);
        return response()->json([
            'status' => 1,
            'message' => 'successful'
        ]);
    }

    public function uploadFile(Request $request, $id)
    {
        $uploaded_files = $request->files;
        foreach ($uploaded_files as $uploaded_file) {

            $file_extension = $uploaded_file[0]->getClientOriginalExtension();
            $allowed_image_extension = array(
                "png",
                "jpg",
                "jpeg",
                "pdf"
            );

            if (!in_array($file_extension, $allowed_image_extension)) {
                return redirect()->back()->with('error', "Please upload files files with extension jpg,jpeg,png,pdf.");
            }
            if ($uploaded_file[0]->getSize() > 5242880) {
                return redirect()->back()->with('error', "Please upload files with size less than 5 MB");
            }
        }

        try {
            $data = ['request' => $request];
//        dd($data);
            $upload = $this->projectRepository->uploadFile($data, $id);
            if ($upload) {
                return redirect()->back()->with('success', "Uploaded Successfully");
            } else {
                return redirect()->back()->with('success', "Error while uploading file");
            }
        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function getProjectData()
    {
        $projects = $this->projectRepository->getAllProjectData(self::$pagination_limit);
        return $projects;
    }

    public function deleteDocument($id)
    {
        $document = $this->projectRepository->deleteDocument($id);
        if ($document) {
            return "success";

        } else {
            return "failure";
        }
    }

    /*
        public function storeComment(Request $request){
            $comment = new Projectcomment();
            $comment->body = $request->get('comment_body');
            $comment->user_id=Auth::user()->user_id;
            $project = Project::find($request->get('project_id'));
            $project->projectcomments()->save($comment);
            return back();


        }*/
    public function getGuidelines($project)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        return response()->json($this->guideLineRepository->getGuidelines($project));
    }

    public function updateTimelineForNewStatusComplete(Request $request, Project $project)
    {
        try {
            DB::beginTransaction();
            foreach ($request->get('guidelines') as $guideline) {
                $guideline = Guideline::findorfail($guideline);
                $response = $this->storeTimeline($project, $guideline);
            }
            DB::commit();
            return response()->json([
                'status' => 1,
                'message' => 'saved tasks'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function storeTimeline(Project $project, Guideline $guideline)
    {
        try {
//            $delete_flag = true;
//            $projectTimeline = ProjectTimeline::where('project_id', $project->id)->where('status_guidelines_id', $guideline->id)->first();
//
//            if ($projectTimeline == '') {
////                dd('hello');
//                $projectTimeline = new ProjectTimeline();
//                $delete_flag = false;
//            }
//            else{
//                $delete_flag = !$projectTimeline->delete_flg;
//            }
////            dd($projectTimeline);
////            $projectTimeline = new ProjectTimeline();
//            $projectTimeline->project_id = $project->id;
//            $projectTimeline->status_guidelines_id = $guideline->id;
//            $projectTimeline->status_id = $guideline->status_id;
////        dd(Auth::id());
//            $projectTimeline->created_by = Auth::id();
//            $projectTimeline->delete_flg = $delete_flag;
//            $projectTimeline->save();
            $this->projectRepository->storeStatusTimeline($project, $guideline);
            return response()->json([
                'status' => 1,
                'message' => 'updated'
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getStatusAvailable($project)
    {
        try {
            $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
            $data = [];
            $projectStatus = $project->getNextStatus($project->status);
//            dd('hello',$projectStatus);
            $currentProjectStatus = Status::where('name', $project->status)->first();

//            dd($currentProjectStatus);
            $nextProject = Status::where('name', $projectStatus)->first();
//            dd($nextProject);
            $nextStatusGuidelines = $currentProjectStatus->guidelines()->count();
//            dd($nextProject->id,$project->id);
            $currentFulfilledGuidelines = ProjectTimeline::where('status_id', $currentProjectStatus->id)->where('project_id', $project->id)->where('delete_flg', 0)->count();
//            dd($nextStatusGuidelines,$currentFulfilledGuidelines);
            if ($nextStatusGuidelines == $currentFulfilledGuidelines && $projectStatus) {
                $data = [
                    $currentProjectStatus,
                    $nextProject
                ];
            } else {
                $data = [$currentProjectStatus];
            }
            return response()->json($data);

        } catch (\Exception $e) {

        }
    }

    public function updateTimelineForFundingComplete($project)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        try {
            $fundingStatus = Status::where('name', 'funding')->first();
            $fundingCompleteGuideLine = Guideline::where('status_id', $fundingStatus->id)->first();
            $alreadyExist = ProjectTimeline::where('status_id', $fundingStatus->id)->where('project_id', $project->id)->where('status_guidelines_id', $fundingCompleteGuideLine->id)->count();
            if ($alreadyExist != 0) {
                throw new Exception('Already registered', 500);
            }
            $timeline = new ProjectTimeline();
            $timeline->status_id = $fundingStatus->id;
            $timeline->project_id = $project->id;
            $timeline->status_guidelines_id = $fundingCompleteGuideLine->id;
            $timeline->created_by = null;
            $timeline->save();
            return response()->json([
                'status' => 1,
                'message' => 'successful'
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function projectLogs($project)
    {
//
        try {
//            dd('hello');
            $projectLogs = $this->projectRepository->getProjectLogs($project);
            return $projectLogs;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateTimelineOnDocumentUpload($project, Category $category)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        $guideline = Guideline::where('attribute_key', $category->attribute_key)->first();
        return $this->storeTimeline($project, $guideline);

    }

    public function getAvailableDocumentCategoryForProject($project)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        $nextStatus = $projectStatus = $project->getNextStatus($project->status);
        $currentProjectStatus = Status::where('name', $nextStatus)->first();
//        dd($currentProjectStatus)
//        dd($currentProjectStatus);
        $documentCategory = Category::orWhere('status_id', $currentProjectStatus->id)->orWhere('status_id', null)->get();
        return $documentCategory;
    }

    public function storeProjectInvestment(InvestmentFormValidation $request)
    {
        $project = $this->projectRepository->getByPaymentId($request->get('payment_id'), $related_models = ['farmers', 'district']);

        if (!$project) {
            return redirect()->back()->with('error', 'Payment Id is invalid for system');
        }

        //formatting request data to integrate with investment module to handle multiple project investments
        $formattedRequestData = self::formatDataRequestForInvestment($request, $project);

        $paymentGateway = $request->get('payment_gateway');

        //Checks the investment is valid for project
        $checkoutStatus = true;
        $projectPayments = $this->investmentRepository->checkProjectPaymentCheckout($formattedRequestData, $paymentGateway);
        foreach ($projectPayments as $key => $projectPayment) {
            if (in_array('error', $projectPayment, true)) {
                $paymentErrorMessage[$key] = $projectPayment;
                $checkoutStatus = false;
            }
        }
        if (!$checkoutStatus) {
//            return redirect()->back()->with('error', 'Investment amount exceeds than project cost');
            return response()->json([
                'status' => 0,
                'message' => 'investment amount exceeeds than project cost'
            ]);
        }

        if ($paymentGateway == 'cash') {
            $projectInvestments = $this->investmentRepository->investmentDataStore($formattedRequestData, $projectPayments);
            if ($projectInvestments) {
                $admin = $this->userRepository->getByEmail(env('ROOT_ADMIN'));
                $farmer = $project->farmers->first();
                $parameters = [
                    'substitution' => [
                        'partner_name' => $this->getPartnerName(),
                        'invest_amount' => $request->get('invest_amount'),
                        'gender_salutation' => $farmer->gender == 'female' ? 'Mrs' : 'Mr',
                        'farmer_name' => $farmer->farmer_name,
                        'pump_size' => $project->pump_size && $project->pump_size != 0 ? $project->pump_size : Constants::DEFAULT_PUMP_SIZE,
                        'district' => $project->district->name,
                    ],
                    'url' => '#'
                ];
//                $this->notificationRepository->sendNotification(new NotificationAttribute($channel ='database',$admin,'CASH_INVESTMENT_COMPLETE',$parameters));
            }
            $this->investmentRepository->investmentTransactionStore($formattedRequestData, $projectInvestments, $paymentGateway);
            $request->session()->flash('success_message', 'Investment is successfully completed');
        }
        return response()->json(['status' => 'success', 'message' => 'successful']);

    }

    public function storeProjectPayment(PaymentFormValidation $request)
    {
//        DB::beginTransaction();
        $data = $request->all();
//        dd($request->has('files'));
        $payment = $this->apiPaymentRepository->payment($data);
//        dd($payment);
        $folder_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
        //TODO::future Move upload logic to service
        if ($payment && $request->file('files')) {
//            dd($request->file('files'));
            foreach ($request->file('files') as $file) {
//            $file = $request->file('file');
                $original_file_name = $file->getClientOriginalName() ?: null;

                $original_file_name_extension = $file->getClientOriginalExtension() ?: null;
                $file_name = rand(1, 99999) . strtotime("now") . '_' . $original_file_name;
                $file->move($folder_path, $file_name);
                $upload = Upload::create([
                    'file_name' => $file_name,
                    'file_type' => $original_file_name_extension,
                    'file_location' => $folder_path,
                    'actual_file_name' => $original_file_name,
                ]);
                if (isset($upload)) {
                    $payment->uploadable()->save($upload);
                }
            }
        }
        $request->session()->flash('success_message', 'cash payment registered successfully');
//        return redirect()->route('ogbadmin.payment.index');
        return response()->json([
            'status' => 1,
            'message' => 'Successful'
        ]);
    }

    private function formatDataRequestForInvestment($request, $project)
    {
        $modified_request = new Request();
        $modified_request->request->add(
            [
                'project_id' => [$project->id],
                'invest_amount' => [$request->get('invest_amount')],
                'invest_percent' => [($request->get('invest_amount') / $project->cost) * 100],
                'interest_rate' => [$request->get('interest_rate')],
                'term' => [$request->get('term')],
            ]);
        return ['request' => $modified_request];
    }

    private function getPartnerName()
    {
        if (session('role') == Role::ADMIN) {
            return 'OGB ADMIN';
        }
        if (isset(Auth::user()->partner) && isset(Auth::user()->partner->name)) {
            return Auth::user()->partner->name;
        }
        return;
    }

    public function getInvestmentPercentage($project)
    {
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        $projectInvestment = $project->projectinvestments;
        $totalInvestmentDone = collect($projectInvestment)->sum('invest_amount');
        return response()->json([
            'percentage' => $this->projectRepository->getInvestmentPercentage($projectInvestment),
            'investmentRemaining' => number_format($project->cost - $totalInvestmentDone),
            'totalInvestment' => $totalInvestmentDone
        ]);
    }

    public function uploadProjectDocument(Request $request, $projectId)
    {
        $upload = $this->uploadRepository->uploadProjectFile($request, $projectId);
        if (isset($upload)) {
            return response()->json([
                'success' => true,
                'id' => $upload->id
            ], 200);
        }
        return response()->json([
            'success' => false
        ], 500);
    }

    //deletes individual file in select plan page
    public function deleteProjectDocument($uploadId)
    {
        $isFileDeleted = $this->uploadRepository->deleteProjectFile($uploadId);
        if ($isFileDeleted) {
            return response()->json([
                'success' => true,
            ], 200);
        }
        return response()->json([
            'success' => false,
        ], 500);

    }

    public function getProjectDocument($project_id)
    {
        $project = $this->projectRepository->getById($project_id);
        $uploads = $project->documents;
        return json_encode(['uploads' => $uploads]);
    }

    public function changeProfilePicture(Request $request, $id)
    {
        $data = ['request' => $request];
        $filename = $this->projectRepository->changeProfilePicture($data, $id);
        return $filename;
    }

    public function editPackage($id)
    {
        $project = Project::find($id);
        $plans = Plan::all();
        return view('ogbadmin.project.edit-package', compact('project', 'plans'));
    }


    public function updatePackage(Request $request, $id)
    {
        $data = $request->all();
        $result = $this->projectRepository->updatePackage($data, $id);
        if ($result) {
            return redirect()->back()->with('success_message', "Package Updated Successfully");

        } else {
            return redirect()->back()->with('error', "Nothing To Update");
        }
    }

    public function getPackage($package)
    {
        $details = Plan::where('name', $package)->first();
        return $details;
    }

    public function getInvestmentChartData($project)
    {
//        $project->id = 46;
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        $investmentDataSets = DB::select(DB::raw(
            'select \'Gham Power\' as investor,CAST(ROUND(ifnull(sum(b.amount),0),0) as int) as amount from app_user a
join transactions b on a.user_id = b.user_id
join tbl_project_investment c on b.investment_id = c.id
where admin_verification = 1
and partner_id is null
and c.Project_id = '.$project->id.'
UNION ALL
select c.name as investor,CAST(ROUND(ifnull(sum(a.amount),0),0) as int) from transactions a
join app_user b on a.user_id = b.user_id
join partners c on b.partner_id = c.id
join tbl_project_investment d on b.user_id = d.investor_id
where c.verified = 1 and d.Project_id = '.$project->id.' group by c.name
UNION ALL
SELECT investor, amount from (
select "Funding Gap" as investor,CAST(ROUND(ifnull(('.$project->cost.' - sum(a.amount)),0),0) as int) as amount from transactions a
join app_user b on a.user_id = b.user_id
join tbl_project_investment c on a.investment_id = c.id
where c.Project_id = '.$project->id.') e 
            Having e.amount > 0'
        ));
//        Union ALL
//select \'Grants\' as Partner, ifnull(sum(b.amount),0) as amount from app_user a
//join transactions b on a.user_id = b.user_id
//join tbl_project_investment c on b.investment_id = c.id
//where admin_verification = 0
//and partner_id is null and c.Project_id = '.$project->id
//        dd($project->id);
        $investmentDataSetsInvestors = collect($investmentDataSets)->pluck('investor');
        $investmentDataSetsAmount = collect($investmentDataSets)->pluck('amount');
        $investmentEmiBreakDown = DB::select(DB::raw("select investor, sum(emi) as emi from
            (select 'Gham Power' as investor,ifnull(b.amount,0) as amount, c.interest_rate, c.term, ROUND((ifnull(b.amount,0) * (c.interest_rate/100)/12 * POWER((1 + (c.interest_rate/100)/12), c.term)) / (POWER((1 + (c.interest_rate/100)/12), c.term) - 1),3) as emi from app_user a 
            join transactions b on a.user_id = b.user_id 
            join tbl_project_investment c on b.investment_id = c.id
            where admin_verification = 1
                and partner_id is null
                and c.Project_id = " . $project->id . "
            UNION ALL 
            select c.name as investor,ifnull(a.amount,0) as amount, d.interest_rate, d.term, ROUND((ifnull(a.amount,0) * (d.interest_rate/100)/12 * POWER((1 + (d.interest_rate/100)/12), d.term)) / (POWER((1 + (d.interest_rate/100)/12), d.term) - 1),3) as emi from transactions  a 
            join app_user b on a.user_id = b.user_id 
            join partners c on b.partner_id = c.id
            join tbl_project_investment d on b.user_id = d.investor_id
            where c.verified = 1 and d.Project_id = " . $project->id . " 
            Union ALL
            select 'Grants' as investor, ifnull(b.amount,0) as amount, c.interest_rate, c.term, ROUND((ifnull(b.amount,0) * (c.interest_rate/100)/12 * POWER((1 + (c.interest_rate/100)/12), c.term)) / (POWER((1 + (c.interest_rate/100)/12), c.term) - 1),3) as emi from app_user a 
            join transactions b on a.user_id = b.user_id 
            join tbl_project_investment c on b.investment_id = c.id
            where admin_verification = 0
                and partner_id is null and c.Project_id = " . $project->id . ") d group by investor"));
//        dd($invetmentEM)
        $total = collect($investmentEmiBreakDown)->sum('emi');
        $projectInvestment = $project->projectinvestments;

        return response()->json([
            'dataSetsInvestor' => $investmentDataSetsInvestors,
            'dataSetsAmount' => $investmentDataSetsAmount,
            'contribution' => $investmentEmiBreakDown,
            'total' => $total,
            'percentage' => $this->projectRepository->getInvestmentPercentage($projectInvestment),
        ]);

    }

    public function getProjectPaymentDefaultedDetails(Project $project)
    {
        $averageDefaultArray = $project->getDefaultedPaymentsArray();
        $emiCount = $project->payments()->where('payment_type', 'emi')->count();
        $averageDefaultRate = $this->projectRepository->getDefaultRate($project, $averageDefaultArray, $emiCount);
        $averageDefaultDays = $this->projectRepository->getDefaultDays($project, $averageDefaultArray, $emiCount);
        return response()->json([
            'averageDefaultRate' => $averageDefaultRate,
            'averageDefaultDays' => $averageDefaultDays
        ]);
    }
}
