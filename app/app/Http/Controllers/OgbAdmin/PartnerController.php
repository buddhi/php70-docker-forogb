<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Http\Requests\Ogbadmin\Partner\PartnerFormValidation;
use App\Models\Partner;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Partner\PartnerRepository;
use App\Http\Controllers\Controller;
use App\Repositories\Token\TokenRepository;
use App\Repositories\User\UserRepository;
use App\User;
use App\Utilities\NotificationAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class PartnerController extends Controller
{
    private $partnerRepository, $tokenRepository ,$notificationRepository,$userRepository;
    private static $pagination_limit = 10;

    public function __construct(
        PartnerRepository $partnerRepository,
        TokenRepository $tokenRepository,
        NotificationRepository $notificationRepository,
        UserRepository $useRepository
    )
    {
        $this->partnerRepository = $partnerRepository;
        $this->tokenRepository = $tokenRepository;
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $useRepository;

    }

    public function index()
    {
        $this->authorize('view',User::class);
        $partners = $this->partnerRepository->getAll(self::$pagination_limit);
        return view('ogbadmin.partner.index', compact('partners'));
    }

    public function create()
    {
        return view('ogbadmin.partner.create');
    }

    public function store(Request $request)
    {

        //validation
        $validator = Validator::make($request->input(), array(
            'name' => 'required',
            'email' => 'required|email|unique:partners',
            'contact_person' => 'required'
        ));

        //Validation fails: send error response
        if ($validator->fails()) {
            return $this->responseMessage($validator->errors(), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //create unique random token
        $token = $this->tokenRepository->generateToken();


        if (!$token) {
            return $this->responseMessage(config('messages.token_creation_problem'), true, Response::HTTP_SERVICE_UNAVAILABLE);
        }
        //Create partner
        $partner = $this->partnerRepository->store($request->all());
        if (!$partner) {
            return $this->responseMessage(config('messages.spartner_cannot_be_created'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //associate partner with token
        $associateTokenWithPartner = $this->tokenRepository->bindModelWithToken($partner, $token);
        if (!$associateTokenWithPartner) {
            return $this->responseMessage(config('messages.unknown_server_error'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //send email if there is valid partner and associated token
        if ($partner) {

            /*$param = [
                'to' => $partner->email,
                'subject' => __('app.subject_for_email_verification_nepali').config('messages.subject_for_email_verification'),
                'from' =>'noreply@ghampower.com',
                'token'=>$token,
                'title'=>'',
                'for_investor' => false
            ];
            Mail::send('emails.verifypartner', $param, function ($message) use ($param) {
                $message->to($param['to'])
                    ->from($param['from'])
                    ->subject($param['subject']);
            });*/
            $this->partnerRepository->sendVerificationEmail($partner, $token);
        }
        return $this->responseMessage(config('messages.partner_created'), false, Response::HTTP_OK);
    }

    public function edit(Request $request, $partner_id)
    {
        $partner = $this->partnerRepository->getById($partner_id);
        if (is_null($partner)) {
            $request->session()->flash('error', config('messages.partner_not_found'));
            return redirect()->route('ogbadmin.partner.index');
        }
        return view('ogbadmin.partner.edit', compact('partner'));
    }

    public function update(PartnerFormValidation $request, $partner_id)
    {
        $partner = $this->partnerRepository->getById($partner_id);
        if (is_null($partner)) {
            $request->session()->flash('error', config('messages.partner_not_found'));
            return redirect()->route('ogbadmin.partner.index');
        }
        $request->session()->flash('success', config('messages.partner_update'));
        $this->partnerRepository->update($partner, $request->all());
        return redirect()->route('ogbadmin.partner.index');
    }

    public function changeStatus(Request $request)
    {
        $partner_id = $request->get('partner_id');
        $partner = $this->partnerRepository->getById($partner_id);
        if (is_null($partner)) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $isStatusChanged = $this->partnerRepository->changeStatus($partner);
        if (!$isStatusChanged) {
            return $this->responseMessage(config('messages.partner_status_error'), true, Response::HTTP_OK);
        }
        $this->userRepository->changeStatusOfAssociatedUser($partner);
        return response()->json([
            'error' => false,
            'messages' => "{$partner->name} is changed to {$partner->status}",
            'partner_status' => ucfirst($partner->status)
        ], Response::HTTP_OK);

    }

    public function resendVerificationEmail(Request $request)
    {
        $email = $request->get('email');
        if (!isset($email)) {
            return $this->responseMessage(config('messages.missing_email'), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $partner = $this->partnerRepository->getByEmail($email);
        if (!$partner) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_BAD_REQUEST);
        }
        $token = $partner->tokens;
        if (!$token) {
            return $this->responseMessage(config('messages.invalid_token'), true, Response::HTTP_BAD_REQUEST);
        }
        $this->tokenRepository->modifyToken($token);
        $this->partnerRepository->sendVerificationEmail($partner, $token);
        return $this->responseMessage(config('messages.email_success'), false, Response::HTTP_OK);
    }

    public function verifyEmail(Request $request, $token)
    {
        if (is_null($token)) {
            $request->session()->flash('error', 'Invalid token.');
            return redirect()->route('ogbadmin.partner.index');
        }
        $partner = $this->partnerRepository->verifyEmail($token);
        if (!$partner instanceof  Partner) {
            $request->session()->flash('error','Invalid token.');
            return redirect()->route('ogbadmin.login');
        }
        $admin = $this->userRepository->getByEmail(env('ROOT_ADMIN'));
        if ($admin){
            $parameters = [
                'substitution'=>['name'=> $partner->name],
                'url'=> route('ogbadmin.partner.edit',$partner->id),
                'image'=> asset('storage/upload/'.$partner->partner_logo)
            ];
            $this->notificationRepository->sendNotification(new NotificationAttribute($channel ='database',$admin,'PARTNER_EMAIL_VERIFIED',$parameters));
        }
        $request->session()->flash('success','Your email is verified successfully');
        return view('ogbadmin.verification-success',compact('partner'));
    }


    public function uploadPartnerLogo(Request $request)
    {
        //Validating the partner-logo is image
        $validator = Validator::make($request->all(), ['file' => 'required | image', 'partner_id'=> 'required']);
        if ($validator->fails())
            return $this->responseMessage($validator->fails(), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        $partner_id = $request->get('partner_id');
        $image = $request->file('file');
        $partner = $this->partnerRepository->getById($partner_id);
        $this->partnerRepository->updatePartnerLogo($partner,$image);
        return response()->json([
            'error' => false,
            'messages' => 'Partner Logo updated successfully',
            'image' => $partner->partner_logo
        ], Response::HTTP_OK);

    }

    //TODO::create a helper to be globally available
    public function responseMessage($message, $error_status, $status_code)
    {
        return response()->json([
            'error' => $error_status,
            'messages' => $message,
        ], $status_code);
    }
}