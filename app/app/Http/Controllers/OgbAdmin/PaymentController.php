<?php
namespace App\Http\Controllers\OgbAdmin;

use App\Enums\PaymentType;
use App\Enums\Role;
use App\Facade\DateConverter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PaymentFormValidation;
use App\Models\Payment;
use App\Models\Project;
use App\Models\Upload;
use App\Repositories\ApiPayment\ApiPaymentRepository;
use App\Repositories\Project\ProjectRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends  Controller
{
    private $apiPaymentRepository, $projectRepository;
    private static $pagination_limit =10;
    public function __construct(ApiPaymentRepository $apiPaymentRepository,ProjectRepository $projectRepository)
    {
        $this->apiPaymentRepository = $apiPaymentRepository;
        $this->projectRepository = $projectRepository;
    }

    public function payment()
    {
        $this->authorize('view',Payment::class);
        $projects = $this->projectRepository->fetchProjectsForPayment();
        $payments = $this->apiPaymentRepository->fetchPayments($projects);
        $start_of_bs_month = DateConverter::getStartOfMonthAccordingToBsDate($today_date = Carbon::now());
        $end_of_bs_month = DateConverter::getEndOfMonthAccordingToBsDate($today_date = Carbon::now());
        $total_monthly_amount_raised = $this->apiPaymentRepository->amountRaisedInBsMonth($projects, $start_of_bs_month, $end_of_bs_month);
        $overdue_payments = $this->apiPaymentRepository->amountToBeRaisedInBsMonth($projects, $start_of_bs_month, $end_of_bs_month);
        return view('ogbadmin.payment.index', compact('payments', 'total_monthly_amount_raised', 'overdue_payments'));
    }

    public function create(Request $request)
    {
        $projects = $this->projectRepository->fetchProjectsForPayment();
        return view('ogbadmin.payment.create' ,compact('projects'));
    }

    public function store(PaymentFormValidation $request)
    {
        $data = $request->all();
        $payment = $this->apiPaymentRepository->payment($data);
        $folder_path =  storage_path() . DIRECTORY_SEPARATOR . 'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
        //TODO::future Move upload logic to service
        if($payment && $request->hasFile('file')){
            $file = $request->file('file');
            $original_file_name = $file->getClientOriginalName() ?: null;
            $original_file_name_extension = $file->getClientOriginalExtension() ?: null;
            $file_name = rand(1, 99999) . strtotime("now") . '_' . $original_file_name;
            $file->move($folder_path, $file_name);
            $upload = Upload::create([
                'file_name' => $file_name,
                'file_type' => $original_file_name_extension,
                'file_location' => $folder_path,
                'actual_file_name' => $original_file_name,
            ]);
            if (isset($upload)) {
                $payment->uploadable()->save($upload);
            }
        }
        $request->session()->flash('success_message', 'cash payment registered successfully');
        return redirect()->route('ogbadmin.payment.index');
    }

}