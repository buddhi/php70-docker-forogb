<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Enums\Role;
use App\Http\Requests\Ogbadmin\Investment\InvestmentFormValidation;
use App\Models\ProjectInvestment;
use App\OGB\Constants;
use App\Repositories\Investment\InvestmentRepository;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\User\UserRepository;
use App\Utilities\NotificationAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InvestmentController extends Controller
{
    private $investmentRepository, $projectRepository,$notificationRepository,$userRepository;

    public function __construct(
        InvestmentRepository $investmentRepository,
        ProjectRepository $projectRepository,
        NotificationRepository $notificationRepository,
        UserRepository $userRepository
    )
    {
        $this->investmentRepository = $investmentRepository;
        $this->projectRepository = $projectRepository;
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $this->authorize('view',ProjectInvestment::class);
        $projects = $this->projectRepository->fetchProjectsForPayment($for_investment = true);
        return view('ogbadmin.investment.index' ,compact('projects'));
    }

    public function store(InvestmentFormValidation $request)
    {
        $project = $this->projectRepository->getByPaymentId($request->get('payment_id'),$related_models= ['farmers','district']);
        if (!$project) {
            return redirect()->back()->with('error', 'Payment Id is invalid for system');
        }
        //formatting request data to integrate with investment module to handle multiple project investments
        $formattedRequestData = self::formatDataRequestForInvestment($request, $project);
        $paymentGateway = $request->get('payment_gateway');

        //Checks the investment is valid for project
        $checkoutStatus = true;
        $projectPayments = $this->investmentRepository->checkProjectPaymentCheckout($formattedRequestData ,$paymentGateway);
        foreach ($projectPayments as $key => $projectPayment) {
            if (in_array('error', $projectPayment, true)) {
                $paymentErrorMessage[$key] = $projectPayment;
                $checkoutStatus = false;
            }
        }

        if (!$checkoutStatus) {
            return redirect()->back()->with('error', 'Investment amount exceeds than project cost');
        }
        
        if ($paymentGateway == 'cash') {
            //Storing investment details
            $projectInvestments = $this->investmentRepository->investmentDataStore($formattedRequestData, $projectPayments);

            //Sending notification to admin
            if($projectInvestments){
                $admin = $this->userRepository->getByEmail(env('ROOT_ADMIN'));
                $farmer = $project->farmers->first();
                $parameters = [
                    'substitution'=>[
                        'partner_name'=> $this->getPartnerName(),
                        'invest_amount'=> $request->get('invest_amount'),
                        'gender_salutation'=> $farmer->gender == 'female'? 'Mrs':'Mr',
                        'farmer_name'=> $farmer->farmer_name,
                        'pump_size'=> $project->pump_size && $project->pump_size != 0? $project->pump_size:Constants::DEFAULT_PUMP_SIZE,
                        'district'=> $project->district->name,
                    ],
                    'url'=> '#'
                ];

                $this->notificationRepository->sendNotification(new NotificationAttribute($channel ='database',$admin,'CASH_INVESTMENT_COMPLETE',$parameters));
                //Storing transactions for investment
                $this->investmentRepository->investmentTransactionStore($formattedRequestData, $projectInvestments, $paymentGateway);
                $request->session()->flash('success_message', 'Investment is successfully completed');

            }
        }
        return redirect()->route('ogbadmin.investment.index');
    }


    private function formatDataRequestForInvestment($request, $project)
    {
        $modified_request = new Request();
        $modified_request->request->add(
            [
                'project_id' => [$project->id],
                'invest_amount' => [$request->get('invest_amount')],
                'invest_percent' => [($request->get('invest_amount') / $project->cost) * 100],
                'interest_rate' => [$request->get('interest_rate')],
                'term' => [$request->get('term')],
            ]);
        return ['request' => $modified_request];
    }

    private function getPartnerName()
    {
        if(session('role') == Role::ADMIN){
            return 'OGB ADMIN';
        }
        if(isset(Auth::user()->partner) && isset(Auth::user()->partner->name)){
            return Auth::user()->partner->name;
        }
        return;
    }
}
