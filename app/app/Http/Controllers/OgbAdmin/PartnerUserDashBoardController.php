<?php
namespace App\Http\Controllers\OgbAdmin;

use App\Enums\Role;
use App\Http\Requests\Payment\PaymentFormValidation;
use App\Models\Project;
use App\Models\Upload;
use App\Repositories\ApiPayment\ApiPaymentRepository;
use App\Repositories\Partner\PartnerRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserRegister\UserRegisterRepository;
use Illuminate\Http\Request;

class PartnerUserDashBoardController
{
    private static $pagination_limit = 10;
    private $userRepository,$userRegisterRepository, $partnerRepository;
    public function __construct(
        UserRepository $userRepository ,
        UserRegisterRepository $userRegisterRepository,
        PartnerRepository $partnerRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userRegisterRepository = $userRegisterRepository;
        $this->partnerRepository = $partnerRepository;
    }

    public function agentList(Request $request, $partner_id)
    {
        $filterStatus = $request->get('filter')? $request->get('filter'): 'valid';
        if($filterStatus == 'valid'){
            $agents = $this->userRepository->getAllUsersWithRole($pagination_limit = 10,$include_deleted_user = true, $role = Role::DEVELOPER);
        }else{
            $agents= $this->userRegisterRepository->getPendingUsersWithRole($role = Role::DEVELOPER,self::$pagination_limit,$partner_id);
        }
        $selected_partner = $this->partnerRepository->getById($partner_id);
        return view('partner-user.agent', compact('agents', 'selected_partner', 'filterStatus'));
    }
}
