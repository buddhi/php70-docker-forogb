<?php

namespace App\Http\Controllers\OgbAdmin;

use App\Enums\OtpVerificationType;
use App\Enums\Role;
use App\Http\Requests\User\UserCreationValidation;
use App\Models\UserRegister;
use App\Repositories\Otp\OtpRepository;
use App\Repositories\Partner\PartnerRepository;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\Token\TokenRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\UserRegister\UserRegisterRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class AgentController extends Controller
{
    private static $pagination_limit = 10;
    private $userRepository, $tokenRepository, $userRegisterRepository, $otpRepository, $smsRepository, $partnerRepository;
    public function __construct(
        UserRepository $userRepository ,
        TokenRepository $tokenRepository ,
        UserRegisterRepository $userRegisterRepository,
        OtpRepository $otpRepository,
        SmsRepository $smsRepository,
        PartnerRepository $partnerRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
        $this->userRegisterRepository = $userRegisterRepository;
        $this->otpRepository = $otpRepository;
        $this->smsRepository = $smsRepository;
        $this->partnerRepository = $partnerRepository;
    }
    public function index(Request $request)
    {
        $this->authorize('view',User::class);
        $filterStatus = $request->get('filter')? $request->get('filter'): 'valid';
        if($filterStatus == 'valid'){
            $agents = $this->userRepository->getAllUsersWithRole($pagination_limit = 10,$include_deleted_user = true, $role = Role::DEVELOPER);
        }else{
            $agents= $this->userRegisterRepository->getPendingUsersWithRole($role = Role::DEVELOPER,self::$pagination_limit);
        }
        $partners = $this->partnerRepository->getAllVerified();
        $disabled_partners = $this->partnerRepository->getDisabledPartners();
        return view('ogbadmin.agent.index' ,compact('agents', 'partners', 'filterStatus', 'disabled_partners'));
    }

    public function store( Request $request)
    {

        //validating the email and partner
        $validator = Validator::make($request->input(), array(
            'email' => 'bail|required|email|unique:app_user,email',
            'partner_id' => 'required|exists:partners,id'
        ));

        //Validation fails: send error response
        if ($validator->fails()) {
            return $this->responseMessage($validator->errors(), true, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $partner = $this->partnerRepository->getById($request->get('partner_id'));
        if (!$partner) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //make temp user
        $token = $this->tokenRepository->generateToken();
        if (!$token) {
            return $this->responseMessage(config('messages.invalid_token'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //adding a role
        $input_data = array_merge($request->all(),['role' => Role::DEVELOPER]);
        //create a temp-user
        $userRegister = $this->userRegisterRepository->registerUserTemporarily($input_data);
        if (!$userRegister) {
            return $this->responseMessage(config('messages.user_cannot_be_created'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //bind temp-user with token
        $isTokenBindedWithModel = $this->tokenRepository->bindModelWithToken($userRegister, $token);
        if (!$isTokenBindedWithModel) {
            return $this->responseMessage('Error', true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->userRegisterRepository->sendAgentVerificationEmail($userRegister, $token ,$partner);
        return $this->responseMessage(config('messages.email_success'), false, Response::HTTP_OK);

    }

    public function verifyEmail(Request $request, $token)
    {
        if (is_null($token)) {
            $request->session()->flash('error', 'Invalid token.');
            return redirect()->route('ogbadmin.partner.index');
        }
        $agent= $this->userRegisterRepository->verifyEmail($token);
        if (!$agent) {
            $request->session()->flash('error','Invalid token.');
            return redirect()->route('login');
        }
        $request->session()->flash('success','Your email is verified successfully');
        return redirect()->route('agent.create', $agent->id);
    }

    //TODO::create a helper to be globally available
    public function responseMessage($message, $error_status, $status_code)
    {
        return response()->json([
            'error' => $error_status,
            'messages' => $message,
        ], $status_code);
    }

    public function create($user_register_id)
    {
        $userRegister = $this->userRegisterRepository->getById($user_register_id);
        return view('ogbadmin.agent.create' ,compact('userRegister'));
    }

    public function storeAgent(UserCreationValidation $request)
    {
        $user_register_id = $request->get('user_register_id');
        $userRegister = $this->userRegisterRepository->getById($user_register_id);

        //fetch user details from userRegister ie temporary user details
        if (!$userRegister) {
            $request->session()->flash('error', config('messages.user_cannot_be_created'));
            return redirect()->route('ogbadmin.agent.index');
        }
        //create user
        $user = $this->userRepository->createUser($userRegister, $request->all());
        if (!$user) {
            $request->session()->flash('error', config('messages.user_cannot_be_created'));
            return redirect()->route('agent.create');
        }

        //update role for user
        $this->userRepository->updateRoleForUser($user,Role::DEVELOPER);

        //Invalidate otp if any
        $this->otpRepository->invalidateOtp($userRegister->email);

        //Generate new otp
        $otp = $this->otpRepository->generateOtp($user);

        //hard delete the old pending user
        $this->userRegisterRepository->delete($userRegister->id);

        //send otp to users
        $this->smsRepository->sendSmsNotification($user->phone, 'OTP_MESSAGE', ['otp_code' => $otp->otp]);
        if ($otp) {
            $this->userRepository->storeCookie($user);
            $request->session()->put('otp_verification_type', OtpVerificationType::AGENT_VERIFICATION);
            return redirect()->route('verify-otp.form',$user->user_id);
        }
        return redirect()->route('ogbadmin.user.index');
    }

    public function changeStatus(Request $request, $partner_user_id)
    {
        $isStatusUpdated = $this->userRepository->changeStatus($request, $partner_user_id);
        if (!$isStatusUpdated) {
            return $this->responseMessage('User status cannot be changed', true, Response::HTTP_OK);
        }
        $partner_user = $this->userRepository->getUserIncludingTrashedUser($partner_user_id);
        return response()->json([
            'error' => false,
            'messages' => "{$partner_user->full_name} is  changed to {$partner_user->status}",
            'user_status' => ucfirst($partner_user->status)
        ], Response::HTTP_OK);
    }

    public function edit($agent_id){
        $agent = $this->userRepository->getUser($agent_id);
        return view('ogbadmin.agent.edit', compact('agent'));
    }

    public function update(Request $request,$agent_id){
        $user = $this->userRepository->getUser($agent_id);
        if (isset($user) && $user->isUserUpdateAble($user->email)) {
            $user->update($request->all());
        }
        return redirect()->route('ogbadmin.agent.index');
    }

    public function changeImage(Request $request){
        $image=$this->userRepository->changeProfileImage($request);
        if($image){
            return response()->json([
                'error' => false,
                'messages' => 'User image is  updated successfully',
                'image' => $image
            ], Response::HTTP_OK);
        }
    }

    public function resendEmailVerification(UserRegister $userRegister){
        if (!$userRegister) {
            return $this->responseMessage(config('messages.user_cannot_be_created'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $token = $userRegister->tokens;
        if (!$token) {
            return $this->responseMessage(config('messages.invalid_token'), true, Response::HTTP_BAD_REQUEST);
        }
        //bind temp-user with token
//        $isTokenBindedWithModel = $this->tokenRepository->bindModelWithToken($userRegister, $token);
        $token = $this->tokenRepository->modifyToken($token);
        if (!$token) {
            return $this->responseMessage('Error', true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $partner = $this->partnerRepository->getById($userRegister->partner_id);
        if (!$partner) {
            return $this->responseMessage(config('messages.partner_not_found'), true, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->userRegisterRepository->sendAgentVerificationEmail($userRegister, $token, $partner);
        return $this->responseMessage(config('messages.email_success'), false, Response::HTTP_OK);
    }

}
