<?php

namespace App\Http\Middleware;

use Closure;

class ApiPaymentToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization') ? $request->header('Authorization') : null;
        if (is_null($token)) {
            return self::returnUnAuthorisedError();
        }
        $actual_token = env('API_PAYMENT_TOKEN');
        if ($actual_token != $token) {
            return self::returnUnAuthorisedError();
        }
        return $next($request);
    }

    private static function returnUnAuthorisedError()
    {
        return response()->json([
            'response_message' => 'Error due to Invalid token.',
            'response_code' => 1
        ],401);
    }
}
