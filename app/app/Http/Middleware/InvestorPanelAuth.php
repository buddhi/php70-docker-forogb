<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use Closure;

class InvestorPanelAuth
{

    public function handle($request, Closure $next)
    {
        if (auth()->check() && session('role') == Role::INVESTOR)
            return $next($request);
        return redirect()->route('index');
    }
}
