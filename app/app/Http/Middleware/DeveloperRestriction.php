<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use Closure;

class DeveloperRestriction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = session('role');
        if($role == Role::DEVELOPER)
            return response('Unauthorized', 302);
        return $next($request);
    }
}
