<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            $role = session('role');
            $admin_type = session('admin_type');
            if (isset($admin_type) && $admin_type == 'ogbadmin' && $role == Role::ADMIN) {
                return redirect()->route('ogbadmin.partner.index');
            }
            if ($role == Role::ADMIN) {
                return redirect()->route('admin.project.list');
            }
            if ($role == Role::DEVELOPER || $role == Role::INVESTOR) {
                return redirect()->route('project.list');
            }

            if ($role == Role::PARTNER_USER) {
                return redirect()->route('partner-user.dashboard');
            }
            return redirect()->route('project.list');
        }
        return $next($request);
    }
}
