<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminPanelAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next3
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = session('role');

        if($role != Role::PARTNER_USER && $role != Role::ADMIN && $role!=Role::DEVELOPER){

            return redirect(route('admin.login'));
        }
        return $next($request);
    }
    
}
