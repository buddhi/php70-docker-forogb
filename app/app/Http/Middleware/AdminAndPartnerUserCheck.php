<?php

namespace App\Http\Middleware;

use App\Enums\Role;
use Closure;

class AdminAndPartnerUserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin_roles = [Role::ADMIN, Role::PARTNER_USER];

        if (!auth()->check()) {
            return redirect()->back();
        }
        if (!in_array(session('role'), $admin_roles)) {
            return redirect()->route('admin.login');
        }
        return $next($request);
    }
}
