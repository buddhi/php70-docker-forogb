<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ExportValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customRules();
        $rules = [];
        if($this->request->has('export')){
            $rules = array_merge($rules,[
                'start' => 'required | compare_date',
                'end' => 'required',
            ]);
        }
        return $rules;
    }

    /**
     *Custom validation rules
     */
    public function customRules()
    {
        /*Checking that user selects start date earlier than end date*/
        Validator::extend('compare_date', function ($attribute, $value, $parameters, $validator) {
            $start = Carbon::parse($value);
            $end = $this->request->get('end');
            $end = Carbon::parse($end);
            if ($value > $end) {
                    return false;
            }
            return true;
        });

    }

    /**
     * @return array with custom message if validation fails
     */
    public function messages()
    {
        return [
            'start.required' => 'Start Date is required',
            'end.required' => 'End Date is required',
            'start.compare_date' => 'Start Date needs to be earlier than enddate',
        ];
    }
}
