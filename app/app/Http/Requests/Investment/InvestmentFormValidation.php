<?php

namespace App\Http\Requests\Investment;

use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class InvestmentFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =[
            'project_id'=>'required',
            'investor_id'=>'required',
            'developer_id'=>'required',
            'funding_type'=>'required',
            'investment_type_id'=>'required',
        ];

        if ($this->request->get('funding_type') == 0) {
            $rules = array_merge($rules, [
                'investment_percent' => 'required',
            ]);
        }elseif($this->request->get('funding_type') == 1){
            $rules = array_merge($rules, [
                'invest_amount' => 'required',
            ]);
        }



        if($this->request->get('investment_type_id') == 1) {
            $rules = array_merge($rules, [
                'expected_irr'=>'required'
            ]);
        }
        elseif($this->request->get('investment_type_id')==2)
        {
            $rules = array_merge($rules, [
                'term' => 'required',
                'interest_rate' => 'required'
            ]);
        }

        return $rules;
    }

}
