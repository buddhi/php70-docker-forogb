<?php

namespace App\Http\Requests\Investment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CheckoutFormValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'project_id'=>'required|array|numeric_array',
            'invest_amount'=>'required|array|numeric_array',
            'invest_percent'=>'required|array',
        ];
    }

    public function messages()
    {
        return[
          '*.numeric_array'=>'Please, do not enter the amount in decimals.'
        ];
    }

    public function customValidation()
    {
        Validator::extend('numeric_array', function($attribute, $value, $parameters)
        {
            foreach($value as $val) {
                if(is_numeric($val) && floor($val)!=$val){
                    return false;
                }
            }
            return true;
        });
    }
}
