<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;

class PaymentFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_id' => 'required',
            'payment_date' => 'required',
            'paymentGateway' => 'required',
            'amount' => 'required',
        ];
    }

    public function messages()
    {
        return ['amount.required' => 'Payment amount must be defined for valid payment'];
    }
}
