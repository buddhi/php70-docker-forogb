<?php

namespace App\Http\Requests\SMS;

//use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CheckFormValidataion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'phone'=>'required',
            'value'=>'required|min_value'
        ];
    }

    public function messages()
    {    $this->customRules();

        return [
           'phone'=>'Phone field value is required',
           'value.min_value'=>'Value field must be greater than 5'

        ];
    }

    public function customRules()
    {
        Validator::extend('min_value', function ($attribute, $value, $parameters, $validator) {

            return $value > 5 ? true : false;

        });
    }
}
