<?php

namespace App\Http\Requests\project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ProjectFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     * @return array $rules
     */
    public function rules()
    {
        $rules =[];
        if ($this->request->has('submit-analysis')) {
            $this->customRules();
            $rules = [
                'farmer_name' => 'required',
                'address' => 'required',
                'project_type_id' => 'required',
                'province_id' => 'required|numeric',
                'district_id' => 'required|numeric',
                'municipality_id' => 'required|numeric',
                'ward_no' => 'required|numeric',
                'age' => 'nullable|numeric',
                'contact_no' => 'required',
                'no_of_dependents' => 'required|numeric|min:0',
                'outstanding_debt' => 'nullable|numeric|min:0',
                'annual_household_income' => 'required|numeric|min:0',
                'has_bank' => 'nullable|in: 0,1',
                'financial_institute' => 'check_institute',
                'annual_expenses' => 'required|numeric|min:0',
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
                'crop_id' => 'bail|required|array|min:1|check_array',
                'cattle_id' => 'bail|required|array|min:1',
                'number' => 'bail|required|array|min:1',
                'crop_area_unit_type' => 'bail|required|array|min:1',
                'start_month' => 'bail|required|array|min:1',
                'area' => 'bail|required|array|min:1',


                'current_irrigation_source' => 'required',
                'current_irrigation_system' => 'required',
                'boring_size' => 'required',
                'distance' => 'required',
                'vertical_head' => 'required',
                'gender'=>'required',
                'education'=>'required| in:1,0',
                'certification'=>'required| in:1,0',
                'no_of_people_in_house'=>'required',
                'income_source'=>'required',
                'income_amount'=>'required',
                'expense_source'=>'required',
                'expense_amount'=>'required',

                //financial score validation
                'water_distribution_to_farmers'=>'required|in: 0,1',
                'revenues_from_water_distribution'=>'check_water_distribution',
                'monthly_saving'=>'required|in: 0,1',
                'monthly_saving_amount'=>'check_monthly_saving_amount',
                'debt_service_obligation'=>'required|in: 0,1',
                'debt_monthly_payment'=>'check_debt_monthly_payment',
                'additional_income_source'=>'required|in: 0,1',
                'additional_salaried_occupation'=>'required|in: 0,1',
                'recent_large_purchase_through_payment_plan'=>'required|in: 0,1',
                'future_capital_expenditure_causing_cash_flow_interruptions'=>'required|in: 0,1',
                'expected_solar_loan_monthly_payment'=>'required|numeric|min:0',
                'distance_to_nearest_market'=>'required|numeric|min:0',
                'nearest_market'=>'required',

                //Agricultural score validation
                'boring_depth'=>'required|numeric|min:0',
                'ground_water_level'=>'required|numeric|min:0',
                'crop_inventory'=>'required|in: 0,1',
                'fuel_monthly_expense'=>'required|numeric|min:0',
                'grid_monthly_expense'=>'required|numeric|min:0',

//              Psycometric score validation commented out for now
//              'size_fish'=>'required|in: 0,1',
//              'neighbour_trust'=>'required|between: 0,100',
//              'harvest_lost'=>'required|between: 0,10',
//              'village_meeting'=>'required|in: 0,1',
//              'farming_skill'=>'required|between: 0,100',


                'land_type'=>'bail|required|array|min:1',
                'ownership'=>'bail|required|array|min:1',
                'owner'=>'bail|required|array|min:1',
                'land_area'=>'bail|required|array|min:1',
                'unit_type'=>'bail|required|array|min:1',
            ];
    }
        return $rules;
    }
    
    /**
     *Custom validation rules
     */
    public function customRules()
    {
        //If user has bank account then he must enter the bank or financial institution name
        Validator::extend('check_institute', function ($attribute, $value, $parameters, $validator) {
            if($this->get('has_bank') && (!isset($value))){
                return false;
            }
            return true;
        });

        Validator::extend('check_array', function ($attribute, $value, $parameters, $validator) {

            $array_form =
                [
                    $crop_data = [
                        $this->request->get('crop_id'),
                        $this->request->get('start_month'),
                        $this->request->get('area'),
                    ],

                    $land_data = [
                        $this->request->get('land_type'),
                        $this->request->get('ownership'),
                        $this->request->get('owner'),
                        $this->request->get('unit_type'),
                    ],

                    $cattle_data = [
                        $this->request->get('cattle_id'),
                        $this->request->get('number'),
                    ],
                    $income_data = [
                        $this->request->get('income_source'),
                        $this->request->get('income_amount'),
                    ],

                    $expense_data = [
                        $this->request->get('expense_source'),
                        $this->request->get('expense_amount'),

                    ]
                ];

            foreach ($array_form as $array){
                $status = $this->checkArrayLength($array);
                if($status == false){
                    return false;
                    break;
                }
            }
            return true;
        });

        Validator::extend('check_water_distribution', function ($attribute, $value, $parameters, $validator) {
            if($this->get('water_distribution_to_farmers') && (!isset($value)) ){
                return false;
            }
            return true;
        });

        Validator::extend('check_monthly_saving_amount', function ($attribute, $value, $parameters, $validator) {
            if($this->get('monthly_saving') && (!isset($value))){
                return false;
            }
            return true;
        });

        Validator::extend('check_debt_monthly_payment', function ($attribute, $value, $parameters, $validator) {
            if($this->get('debt_service_obligation') && (!isset($value))){
                return false;
            }
            return true;
        });
    }
    
    /**
     * @return array with custom message if validation fails
     */
    public function messages()
    {
        return [
            'farmer_name.required' => 'Farmer  name is required',
            'address.required' => 'Address field is required',
            'project_type_id.required' => 'ProjectType field is required',
            'province_id.required' => 'Province is required',
            'district_id.required' => 'District field is required',
            'municipality_id.required' => 'Municipality field is required',
            'ward_no.required' => 'Ward field is required',
            'crop_id.required' => 'Atleast one Crop field is required',
            'start_month.required' => 'Start month field is required',
            'contact_no.required' => 'Contact field field is required',
            'latitude.required' => 'Latitude field field is required',
            'longitude.required' => 'Longitude field field is required',
            'land_size.required' => 'Land Size field field is required',
            'current_irrigation_source.required' => 'Current irrigation  field is required',
            'current_irrigation_system.required' => ' Current irrigation system field field is required',
            'boring_size.required' => 'Boring size field field is required',
            'vertical_head.required' => 'Vertical head field field is required',
            'distance.required' => 'Distance field field is required',
            'financial_institute.check_institution' => 'Enter the bank name',
            'cattle_id.check_cattle' => 'Please dont enter duplicate cattle',
            'revenues_from_water_distribution.check_water_distribution' => 'Please enter the revenue generated from water distribution',
            'monthly_saving_amount.check_monthly_saving_amount' => 'Please enter the monthly saving amount',
            'debt_monthly_payment.check_debt_monthly_payment' => 'Please enter the monthly debt amount',
            'financial_institute.check_institute' => 'Please enter the name of bank ',
            'land_type.required'=>'Choose type of land',
            'ownership.required'=>'select ownership',
            'unit_type.required'=>'Unit field is required',
            'crop_area_unit_type.required'=>'Please choose at-least one unit',
           '*.check_array'=> 'All the data in row must be filled'
        ];
    }

    private function checkArrayLength($array)
    {
        for ($i = 0; $i < (count($array,0) - 1); $i++) {

            if(!is_array($array[$i])){
                return false;
            }
            if (count($array[$i]) != count($array[$i+1])) {
                return false;
            }
        }
        return true;
    }
}
