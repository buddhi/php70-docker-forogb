<?php

namespace App\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;

class ImageFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'farm_image' => 'required | mimes:jpeg,bmp,png,jpg,gif,svg',
        ];
        
    }
    
    public function messages()
    {
        $messages = [
            'farm_image.required'=>'This field is required',
            'farm_image.mimes'=>'Please upload image with extension jpeg,bmp,png,jpg,gif,svg'
        ];
        return $messages;
    }
}
