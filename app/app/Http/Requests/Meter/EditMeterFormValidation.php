<?php

namespace App\Http\Requests\Meter;

use Illuminate\Foundation\Http\FormRequest;

class EditMeterFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            //laravel helps in maintaining unique fields except its own
            'phoneNo' => 'required | max:10 | unique:meters,phoneNo,'.$this->id,
            
            'hardwareId' => 'required',
            'firmwareVersion' => 'required',
            'type' => 'required',
            'sys_eff1' => 'required',
            'sys_eff2' => 'required',
            'sys_eff3' => 'required',
            'sys_eff4' => 'required',
            'sys_eff5' => 'required',
            'sys_eff6' => 'required',
            'sys_eff7' => 'required',
            'sys_eff8' => 'required',
            'current_threshold' => 'required',
            'pump_efficiency' => 'required',
            'cd_efficiency' => 'required',
            'max_power' => 'required',
            'frequency_multiplier' => 'required',
            'rpm_multiplier' => 'required',
            'status' => 'required | in:1,0',
        ];
        
        return $rules;
    }
    
    public function messages()
    {
        return [
            'phoneNo.required' => 'Phone no is required',
            'hardwareId.required' => 'Hardware Id field is required',
            'firmwareVersion.required' => 'Enter the Firmware version of meter',
            'type.required' => 'Meter type field is required',
            'sys_eff1.required' => 'System efficiency is required',
            'sys_eff2.required' => 'System efficiency is required',
            'sys_eff3.required' => 'System efficiency is required',
            'sys_eff4.required' => 'System efficiency is required',
            'sys_eff5.required' => 'System efficiency is required',
            'sys_eff6.required' => 'System efficiency is required',
            'sys_eff7.required' => 'System efficiency is required',
            'sys_eff8.required' => 'System efficiency is required',
            'pump_efficiency.required' => 'Pump efficiency is required',
            'cd_efficiency.required' => 'CD efficiency field is required',
            'max_power.required' => 'Max Power is required',
            'frequency_multiplier.required' => 'Frequency multiplier is required',
            'rpm_multiplier.required' => 'RPM multiplier is required',
            'status.required' => 'status is required',
        ];
    }
}
