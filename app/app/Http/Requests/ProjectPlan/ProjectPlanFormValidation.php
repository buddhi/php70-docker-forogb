<?php

namespace App\Http\Requests\ProjectPlan;

use App\Models\Project;
use Illuminate\Foundation\Http\FormRequest;

class ProjectPlanFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'code' => 'required',
            'name' => 'required',
            'pump_size' => 'required',
            'cost' => 'required',
            'daily_discharge' => 'required',
            'solar_pv_size' => 'required',
        ];

        //Extra rules  are added if the advance amount is not paid.
        $extra_rules = [
            'emi_count' => 'required | numeric',
            'emi_amount' => 'required | numeric',
            'advance_amount' => 'required | numeric',
            'emi_start_date' => 'nullable|date',
        ];
        if($this->id){
            $project = Project::findOrFail($this->id);
            if(!$project->isAdvanceAmountPaid()){
                $rules = array_merge($rules,$extra_rules);
            }
        }
        return $rules;
    }
}

