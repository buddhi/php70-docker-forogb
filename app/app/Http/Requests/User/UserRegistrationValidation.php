<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRegistrationValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'email' => 'required|string|email|max:255|unique:app_user',
            'full_name' => 'required',
            'password' => 'required|string|min:4|confirmed',
            'address' => 'required'
        ];
        if($this->request->has('user-form')) {
            $rules = array_merge($rules,['role'=>'required']);
        }
        return $rules;
    }
}
