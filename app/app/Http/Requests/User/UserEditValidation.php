<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserEditValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $rules = [
            'email' => 'required|string|email|max:255|unique:app_user,email,'.$this->request->get('user_id').',user_id',
            'full_name' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ];
        return $rules;
    }
}
