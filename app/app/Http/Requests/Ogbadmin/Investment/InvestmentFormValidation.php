<?php

namespace App\Http\Requests\Ogbadmin\Investment;

use Illuminate\Foundation\Http\FormRequest;

class InvestmentFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invest_amount' => 'required',
            'payment_id' => 'required',
            'payment_gateway' => 'required',
            'term' => 'required|numeric|between:0,100',
            'interest_rate' => 'required|numeric|between:0,300',
        ];
    }

}
