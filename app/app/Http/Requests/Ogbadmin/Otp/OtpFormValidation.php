<?php

namespace App\Http\Requests\Ogbadmin\Otp;

use Illuminate\Foundation\Http\FormRequest;

class OtpFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'digit1' => 'required',
            'digit2' => 'required',
            'digit3' => 'required',
            'digit4' => 'required',
            'digit5' => 'required',
            'digit6' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'digit*.required' => 'That code is invalid.&nbsp;Please try again.',
        ];
    }
}
