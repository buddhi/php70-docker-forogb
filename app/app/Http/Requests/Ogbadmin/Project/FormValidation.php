<?php

namespace App\Http\Requests\Ogbadmin\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;


class FormValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $rules = [];
        if ($this->request->has('submit-analysis')) {
            $this->customRules();


            $rules = [
                'project_type_id' => 'required',
                'farmer_name' => 'required',
                'earning_members' => 'required|numeric',
                'non_earning_members' => 'required|numeric',
                'income_source' => 'required',
                'income_amount' => 'required',
                'expense_source' => 'required',
                'expense_amount' => 'required',
                'contact_no' => 'required|numeric',
                'educational_level' => 'required',
                'age' => 'nullable|numeric',
                'province_id' => 'required|numeric',
                'district_id' => 'required|numeric',
                'municipality_id' => 'required|numeric',
                'ward_no' => 'required|numeric',
                'address' => 'required',
                'village_name' => 'required',
                'gender' => 'required',
                'current_irrigation_source' => 'required',
                'current_irrigation_system' => 'required',
                'grid_monthly_expense' => 'required|numeric|min:0',
                'boring_size' => 'required|numeric|min:0',
                'land_type' => 'required|array|min:1',
                'ownership' => 'required|array|min:1',
                'unit_type' => 'required|array|min:1',
                'land_area' => 'required|array|min:1',

                'crop_id' => 'required|array|min:1|check_array',
                'crop_area_unit_type' => 'required',
                'area' => 'required|array|min:1',
                'cattle_id' => 'nullable',
                'number' => 'nullable',
                'distance_to_nearest_market' => 'nullable'


            ];

            return $rules;

        }
    }

    /**
     *Custom validation rules
     */
    public function customRules()
    {
        Validator::extend('check_array', function ($attribute, $value, $parameters, $validator) {


        $array_form =
            [
                $crop_data = [
                    $this->request->get('crop_id'),
                    $this->request->get('area'),

                ],

                $land_data = [
                    $this->request->get('land_type'),
                    $this->request->get('ownership'),
                    $this->request->get('land_area'),
                    $this->request->get('unit_type'),
                ],

                $income_data = [
                    $this->request->get('income_source'),
                    $this->request->get('income_amount'),
                ],

                $expense_data = [
                    $this->request->get('expense_source'),
                    $this->request->get('expense_amount'),

                ]
            ];
        foreach ($array_form as $array){
            $status = $this->checkArrayLength($array);
            if($status == false){
                return false;
                break;
            }
        }
        return true;
    });



    }

    /**
     * @return array with custom message if validation fails
     */
    public function messages()
    {
        return [
            'project_type_id.required' => 'Please Select Project Type',
            'account_name'=>'Enter a valid account number',
            'farmer_name.required' => 'Please Input Name and Surname',
            'sim_number.requird'=>'Sim number is required',
            'province_id.required' => 'Please Select Province',
            'district_id.required' => 'Please Select District',
            'municipality_id.required' => 'Please Select Municipality',
            'ward_no.required' => 'Please Input Ward Number',
            'address.required' => 'Please Input Address',
            'village_name.required'=>'Please Input Village Name',
            'educational_level'=>'Please Select Education Level',
            'income_source.required' => 'Please Input Income Source',
            'income_amount.required' => 'Please Input Income Amount',
            'expense_source.required' => 'Please Input Expense Source',
            'expense_amount.required' => 'Please Input Expense Amount',
            'earning_members.required' => 'Please Input Number Of Earning Members',
            'non_earning_members.required' => 'Please Input Number Of Non Earning Members',
            'contact_no.required' => 'Please Enter Your Contact Number',
            'current_irrigation_source.required' => 'Please Select Current Irrigation Source',
            'current_irrigation_system.required' => ' Please Select Current Irrigation System',
            'land_type.required'=>'Please Select Type Of Land',
            'ownership.required'=>'Please Select Type Of Ownership',
            'unit_type.required'=>'Please Select Unit Type',
            'land_size.required' => 'Land Size field field is required',
            'land_area.required'=>'Please Input Land Area',
            'area.required'=>'Please Input Area',
            'crop_id.required' => 'Please Select Crop',
            'crop_area_unit_type.required'=>'Please Select Crop Unit Type',
            'grid_monthly_expense.required'=>'Please Input Monthly Expense Amount',
            'boring_size.required'=>'Please Input Boring Size'


        ];
    }
    private function checkArrayLength($array)
    {
        $equals = true;
        for ($i = 0; $i < (count($array,0) - 1); $i++) {
            if (count($array[$i]) != count($array[$i+1])) {
                $equals = false;
                break;
            }
        }
        return $equals;
    }


}


