<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ChangeMobileValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customRules();
        return [
            'phone' =>'required|check_number'
        ];
    }

    public function messages()
    {
        return [
            'phone.check_number' => 'Please enter 10 digits only.'
        ];
    }

    public function customRules()
    {
        //If user has bank account then he must enter the bank or financial institution name
        Validator::extend('check_number', function ($attribute, $value, $parameters, $validator) {
            if(preg_match('/^[0-9]{10}+$/', $value)){
                return true;
            }
            return false;
        });
    }
}
