<?php

namespace App\Http\Resources;

use App\Traits\FundingMix;
use Illuminate\Http\Resources\Json\Resource;

class ProjectIndexResource extends Resource
{
    use FundingMix;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'payment_id' => $this->payment_id,
            'pump_size' => $this->pump_size,
            'farmer_name' => $this->farmer_name,
            'district' => $this->district->name,
            'contact_no' => $this->farmers[0]->contact_no,
//            'name' => $this->project_type_id->name,
            'credit_score' => $this->farmers[0]->credit_score,
            'name' => $this->getProjectType->name,
            'funding_mix' => $this->fundingMixForProjectIndex($this->id),
            'status' => $this->status,
            'delete_flg' => $this->delete_flg,
            'cost' => $this->cost,
            'created_by'=>$this->created_by
        ];
    }
}
