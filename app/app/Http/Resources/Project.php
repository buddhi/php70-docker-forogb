<?php

namespace App\Http\Resources;

use App\Models\ProjectType;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Storage;

class Project extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $file_name = ($this->documents()->join('category_document','documents.id','=','category_document.document_id')
            ->where('documents.project_id',$this->id)->where('category_document.category_id','=',1)->orderBy('documents.id','DESC')->first());
        return [
            'id' => $this->id,
            'payment_id' => $this->payment_id,
            'farmer_name' => $this->farmer_name,
//            'address' => $this->province->name.','.$this->district->name.','.$this->municipality->name.','.$this->ward_no,
            'description' => isset($this->description)? $this->description : null,
            'address' => $this->farmers[0]->address,
            'contact_no' => $this->farmers[0]->contact_no,
            'payment_id' => $this->payment_id,
            'project_type_id' => $this->project_type_id,
            'no_of_people_in_house' => $this->farmers[0]->no_of_people_in_house,
            'no_of_dependents' => $this->farmers[0]->no_of_dependents,
            'age' => $this->farmers[0]->age,
            'education' => $this->farmers[0]->education,
            'status' => $this->status,
            'created_by' => $this->user->full_name,
            'created_on' => \Carbon\Carbon::parse($this->created_on)->format('Y-m-d'),
            'image' => ($file_name!='')?url('/').'/storage/project_details/'.$file_name->file_name:url('/').'/img/avatar.png',
            'project_types' => ProjectType::all(),
            'meter_id' => isset($this->meters[0]) ? $this->meters[0]->id : 0,
            'meters' => $this->meters
        ];
    }
}
