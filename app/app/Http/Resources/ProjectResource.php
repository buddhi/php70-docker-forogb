<?php

namespace App\Http\Resources;

//use Illuminate\Http\Resources\Json\JsonResource;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'farmer_name' => $this->farmer_name,
            'province' => $this->province->name,
            'district' => $this->province->name,
            'municipality' => $this->province->name,
            'ward_no' => $this->ward_no,
            'contact_no' => $this->sim_number,
            'payment_id' => $this->payment_id,
            'category' => $this->project_type->name,
            'no_of_people_in_house' => $this->farmer->no_of_people_in_house,
            'no_of_dependents' => $this->farmer->no_of_dependents,
            'age' => $this->farmer->age,
            'education' => $this->farmer->education,
            'status' => $this->status,
            'created_by' => $this->user->full_name
        ];
    }
}
