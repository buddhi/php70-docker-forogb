<?php

namespace App\OGB;

abstract class Constants
{
    const route_admin_project_list_columns = [0, 1, 2, 3];
    const route_admin_meter_list_columns = [0, 1, 2, 3, 4, 5, 6];
    const route_admin_user_list_columns = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const default_admin_list_columns =  [0,1,2,3];
    const Risky = 1;
    const Not_Risky = 0;
    const Livestock_Diversity_Threshold = 1;
    const Crop_Diversity_Threshold = 2;
    const NOT_ANONYMOUS = 0;
    const ANONYMOUS = 1;
    const DELETE = 1;
    const ACTIVATE = 0;
    const ACTIVE = 1;
    const INACTIVE = 0;
    const AVERAGE_DAILY_SUN_HOURS = 5; // Hours
    const NO_OF_DAYS_IN_YEAR = 365;
    const SYSTEM_UTILIZATION_RATE = 70; // percent
    const DIESEL_REQUIRED_PER_KWH = 0.51; // L/KWh
    const CO2_EMISSION_PER_LITER_DIESEL_CONSUMPTION = 2.68; // KG/L
    const DEFAULT_COUNTRY_CODE = 0;
    const OFF = 0;
    const ON = 1;
    const DEFAULT_PUMP_SIZE = 1;
}