<?php
namespace App\Traits;

use App\Models\Project;
use App\Models\TransitionState;
use App\OGB\Constants;

trait ProjectState{

    public function getNextStatus($status)
    {
        $states = array_keys(config('transition.states'));
        $current_key = array_search($status, $states);
        if(isset($states[$current_key+1])){
            return  $states[$current_key+1];
        }
        return false;
    }

    public function getNextStatusCondition($status)
    {
        $next_states = [];
        $next_status = $this->getNextStatus($status);
        $next_transition_states =  getTransitionStates($next_status);
        $current_transition_states =  $this->transitionstate;
        $current_transition_states_ids = $current_transition_states->filter(function ($current_transition_states_id) {
            return $current_transition_states_id->pivot->value ==1;
        })->pluck('id')->toArray();
        foreach ($next_transition_states as $next_transition_state){
            if(in_array($next_transition_state['id'], $current_transition_states_ids))
            {
                $next_transition_state = array_merge($next_transition_state, ['value'=> 1]);
            }
            $next_states[] = $next_transition_state;
        }

        return $next_states;
    }

    public function totalTransitionStateCount($status)
    {
        $next_status = $this->getNextStatus($status);
        $next_transition_states =  getTransitionStates($next_status);
        $array_count = count($next_transition_states);
        return  $array_count;
    }

    public function currentTransitionStateCount($status)
    {
        $next_status = $this->getNextStatus($status);
        $current_transition_states =  $this->transitionstate;
        $current_condition = $current_transition_states->filter(function ($current_transition_states_id) use ($next_status) {
            return $current_transition_states_id->pivot->value == Constants::ACTIVE  && $current_transition_states_id->status == $next_status;
        })->pluck('id')->toArray();
        return count($current_condition);
    }

    public function isEmiPaidTransitionStateUpdated()
    {
        $first_emi_obj = TransitionState::where('title','first_emi_collected')->first();
        if(!$first_emi_obj){
            return false;
        }
        $isFirstEmiStateUpdated = $this->transitionstate->where('id', $first_emi_obj->id)->first(function ($state){
            return $state->pivot->value == Constants::ACTIVE;
        });

        //If emi paid state is already updated then we dont need to re-update it
        if ($isFirstEmiStateUpdated) {
            return true;
        }

        return false;
    }

    public function isAdvanceCollectedStateUpdated()
    {
        $advance_collected_state = TransitionState::where('title','collected_advance_and_emi_for_first_month')->first();
        if(!$advance_collected_state){
            return false;
        }
        $isTransitionStateUpdated = $this->transitionstate->where('id', $advance_collected_state->id)->first(function ($state){
            return $state->pivot->value == Constants::ACTIVE;
        });

        //If advance collected state is already updated then we dont need to re-update it
        if ($isTransitionStateUpdated) {
            return true;
        }
        return false;
    }
}