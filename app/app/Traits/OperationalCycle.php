<?php

namespace App\Traits;

use App\Enums\ProjectStatus;
use App\Models\Meter;
use App\Models\MeterData;
use Carbon\Carbon;

trait OperationalCycle
{
    private static $METER_ERROR_THRESHOLD = 15; //Unit:minute

    //Will use this class to handle all payment related stuff
    public function getStatusFromOperationalCycle()
    {

        $project_status = $this->status;

        //Returns all status if not in operational cycle
        if($project_status != ProjectStatus::OPERATIONAL && $project_status != ProjectStatus::ERROR && $project_status != ProjectStatus::OVERDUE){
            return $project_status;
        }

        //Checking whether the project(meter) is in error state
        //Error state has higher priority so,returning early
        $is_project_in_error = $this->checkError();
        if($is_project_in_error)
            return ProjectStatus::ERROR;

        //Checking whether the project(meter) is in overdue
        //Overdue state has higher priority than operational
        $is_project_in_overdue = $this->isEmiPending();
        if($is_project_in_overdue)
            return ProjectStatus::OVERDUE;

        return ProjectStatus::OPERATIONAL;
    }

    //Checking whether the project(meter) is in error state
    private function checkError()
    {
        $meter_id = $this->getMeterIdAttribute();
        if(!isset($meter_id)){
            return true;
        }
        $meter = Meter::find($meter_id);
        if(!$meter) {
            return true;
        }
        $last_meter_data  = MeterData::where('meter_id','=',$meter->id)->latest()->first();

        if(!$last_meter_data){
            return true;
        }
        $difference = Carbon::now()->diffInMinutes(Carbon::parse($last_meter_data->created_at));
        if ($difference <= self::$METER_ERROR_THRESHOLD) {
            return false;
        }
        return true;
    }


}