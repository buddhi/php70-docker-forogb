<?php
namespace App\Traits;


use App\Models\Project;
use App\Scope\ProjectScope;
use Illuminate\Support\Facades\DB;

trait FundingMix{

    public function fundingMixForProjectIndex($project){
//        return 'hello';
        $project = Project::withoutGlobalScope(ProjectScope::class)->find($project);
        $investments = $this->getInvestmentQuery($project);
        return $investments;
    }

    public function getInvestmentQuery($project){
        $investmentDataSets = DB::select(DB::raw('SELECT f.investor,f.amount from (
select \'Gham Power\' as investor,ifnull(sum(b.amount),0) as amount from app_user a
join transactions b on a.user_id = b.user_id
join tbl_project_investment c on b.investment_id = c.id
where admin_verification = 1
and partner_id is null
and c.Project_id = '.$project->id.'
UNION ALL
select c.name as investor,ifnull(sum(a.amount),0) from transactions a
join app_user b on a.user_id = b.user_id
join partners c on b.partner_id = c.id
join tbl_project_investment d on b.user_id = d.investor_id
where c.verified = 1 and d.Project_id = '.$project->id.' group by c.name
Union ALL
select \'Other\' as Partner, ifnull(sum(b.amount),0) as amount from app_user a
join transactions b on a.user_id = b.user_id
join tbl_project_investment c on b.investment_id = c.id
where admin_verification = 0
and partner_id is null and c.Project_id = '.$project->id.'
UNION ALL
SELECT investor, amount from (
select "Funding Gap" as investor,ifnull(('.$project->cost.' - sum(a.amount)),0) as amount from transactions a
join app_user b on a.user_id = b.user_id
join tbl_project_investment c on a.investment_id = c.id
where c.Project_id = '.$project->id.') e 
            Having e.amount > 0) f HAVING f.amount > 0'
));
//        $investmentDataSets = DB::select(DB::raw('
//select investor, amount from (
//select \'GhamPower\' as investor,ifnull(sum(b.amount),0) as amount from app_user a
//join transactions b on a.user_id = b.user_id
//join tbl_project_investment c on b.investment_id = c.id
//where admin_verification = 1
//and partner_id is null
//and c.Project_id = '.$project->id.'
//UNION ALL
//select c.name as investor,ifnull(sum(a.amount),0) from transactions a
//join app_user b on a.user_id = b.user_id
//join partners c on b.partner_id = c.id
//join tbl_project_investment d on b.user_id = d.investor_id
//where c.verified = 1 and d.Project_id = '.$project->id.' group by c.name
//UNION ALL
//select "Funding Gap" as investor,ifnull(('.$project->cost.' - sum(a.amount)),0) as amount from transactions a
//join app_user b on a.user_id = b.user_id
//join tbl_project_investment c on a.investment_id = c.id
//where c.Project_id = '.$project->id.'
//            Having sum(a.amount) > 0) e Having e.amount>0'
//        ));
        return $investmentDataSets;
    }
}