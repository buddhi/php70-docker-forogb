<?php
namespace App\Traits;

use App\Enums\Role;
use Illuminate\Support\Facades\Auth;

trait AuthenticationUtils{

    private static function checkAndGetValidRoleForAdmin($userRoles)
    {
        if (in_array(Role::ADMIN, $userRoles)) {
            return Role::ADMIN;
        }
        return false;
    }

    private static function roleExists($role, $userRoles)
    {
        if(!$role){
            return false;
        }

        if (in_array($role, $userRoles)) {
            return true;
        }
        return false;
    }


    private function getLoginRoute($role, $admin_redirect_to = null)
    {

        if ($role == Role::ADMIN) {
            if ($admin_redirect_to == 'ogbadmin') {
                return route('ogbadmin.partner.index');
            }
            return route('admin.project.list');
        }
        return redirect()->intended($this->redirectPath())->getTargetUrl();
    }

    private static function getLogOutRoute($role)
    {
        //switch statement returns the first false otherwise so check is made before switch statement
        if($role == false){
            return route('admin.login');
        }

        switch ($role) {
            case $role == Role::DEVELOPER:
                return route('login');
                break;
            case $role == Role::INVESTOR:
                return route('index');
                break;
            case ($role == Role::PARTNER_USER):
                return route('partner-user.login');
                break;
            case ($role == Role::ADMIN):
                return route('admin.login');
                break;
            default:
                return route('admin.login');
        }

    }
}