<?php

namespace App\Traits;

use Illuminate\Container\EntryNotFoundException;
use SM\Factory\FactoryInterface;

trait Statable
{
    protected $stateMachine;

    public function stateMachine()
    {
        if(!$this->stateMachine){
            try {
                $this->stateMachine = app(FactoryInterface::class)->get($this, self::SM_CONFIG);
            } catch (EntryNotFoundException $e) {
                return abort(500, $e->getMessage());
            }
        }
        return $this->stateMachine;
    }

    public function stateIs()
    {
        return $this->stateMachine()->getState();
    }
    public function transition($transition)
    {
        return $this->stateMachine()->apply($transition);
    }
    public function transitionAllowed($transition)
    {
        return $this->stateMachine()->can($transition);
    }
}