<?php

namespace App\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserScope implements Scope
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ProjectScope());
    }

    public function apply(Builder $builder, Model $model)
    {
        $builder->where('delete_flg', '=', 0);
    }
}