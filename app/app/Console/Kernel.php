<?php

namespace App\Console;

use App\IngestTest;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\IngestData::class,
        Commands\RetreiveFtpData::class,
        Commands\CurrencyRateScrapper::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire') ->hourly();
        //$schedule->command('ingest:data')->everyMinute();
        $schedule->command('ftp:retrieve')->everyFiveMinutes();
        $schedule->command('meterdata:refresh')->dailyAt('00:00');
        $schedule->command('get:currency_rate')->dailyAt('11:00');

        //At minute 0 past every hour from 8 through 17 is time to check meter.
        $schedule->command('check:operational-cycle')->cron('0 8-17 * * *');

        //Send sms and emails at 8:30 am
        $schedule->command('send:overdue-message')->dailyAt('8:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
