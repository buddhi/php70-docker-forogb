<?php

namespace App\Console\Commands;

use App\Models\Project;
use App\Repositories\Sms\SmsRepository;
use App\Services\OperationalCycleTrigger;
use Illuminate\Console\Command;

class ProjectOperationalCycle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:operational-cycle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the projects in Operational ,error or overdue stage and sends necessary sms and emails if state changes';

    /**
     * Create a new command instance.
     *
     * @param SmsRepository $smsRepository
     */
    private $operationalCycleTrigger;
    public function __construct(OperationalCycleTrigger $operationalCycleTrigger)
    {
        parent::__construct();
        $this->operationalCycleTrigger = $operationalCycleTrigger;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::with('user', 'farmers')->get();
        foreach ($projects as $project) {
            $initial_status = $project->status;
            $final_status = $project->getStatusFromOperationalCycle();
            if($final_status != $initial_status){
                $project->status = $final_status;
                $this->operationalCycleTrigger->sendSmsEmail($project,$initial_status,$final_status);
                $project->save();
            }
        }
    }

}
