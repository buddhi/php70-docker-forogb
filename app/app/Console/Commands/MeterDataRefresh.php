<?php

namespace App\Console\Commands;

use App\Services\MeterDataRefreshService;
use Illuminate\Console\Command;

class MeterDataRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meterdata:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command aggregates the discharge and runtime in meterdata in a new column and updates the daily discharge and daily runtime in daily_meter_data table';
    /**
     * @var MeterDataRefreshService
     */
    private $meterDataRefreshService;

    /**
     * Create a new command instance.
     *
     * @param MeterDataRefreshService $meterDataRefreshService
     */
    public function __construct(MeterDataRefreshService $meterDataRefreshService)
    {
        parent::__construct();
        $this->meterDataRefreshService = $meterDataRefreshService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->meterDataRefreshService->addData();
        $this->meterDataRefreshService->interpolateMeterData();
        $this->meterDataRefreshService->aggregateData();
    }
}
