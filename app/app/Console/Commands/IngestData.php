<?php

namespace App\Console\Commands;

use App\Services\DataIngestionService;
use App\Services\IngestTest;
use Illuminate\Console\Command;

class IngestData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This ingests the data from CSV File for now can be modified later';
    /**
     * @var DataIngestionService
     */
    private $ingest;

    /**
     * Create a new command instance.
     *
     * @param DataIngestionService $ingest
     */
    public function __construct(DataIngestionService $ingest)
    {
        parent::__construct();
        $this->ingest = $ingest;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->ingest->receive();
    }
}
