<?php

namespace App\Console\Commands;

use App\Models\Project;
use Illuminate\Console\Command;

class GeneratePaymentId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:payment-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::all();
        foreach ($projects as $project){
            $project->payment_id = $project->generatePaymentId();
            $project->save();
        }
    }
}
