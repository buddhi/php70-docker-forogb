<?php

namespace App\Console\Commands;

use App\Services\CreditScoreScheduler;
use Illuminate\Console\Command;

class CalculateCreditScore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:credit-score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the credit score at night for every farmer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $creditScoreScheduler;

    public function __construct()
    {
        parent::__construct();
        $this->creditScoreScheduler = new CreditScoreScheduler();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start fetching credit-score..');
        $this->creditScoreScheduler->fetchCreditScores();
        $this->info('Credit-score calculation completed..');
    }
}
