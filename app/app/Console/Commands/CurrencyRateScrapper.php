<?php

namespace App\Console\Commands;

use App\Services\PriceScrapper;
use Illuminate\Console\Command;

class CurrencyRateScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:currency_rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command scrapes the USD selling price from the NRB website and inflates it by 1%';
    /**
     * @var PriceScrapper
     */
    private $priceScrapper;

    /**
     * Create a new command instance.
     *
     * @param PriceScrapper $priceScrapper
     */
    public function __construct(PriceScrapper $priceScrapper)
    {
        parent::__construct();
        $this->priceScrapper = $priceScrapper;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->priceScrapper->scrape();
    }
}
