<?php

namespace App\Console\Commands;

use App\Enums\PaymentType;
use App\Enums\ProjectStatus;
use App\Models\Payment;
use App\Models\Project;
use App\Repositories\Sms\SmsRepository;
use App\Services\OperationalCycleTrigger;
use App\Utilities\StringManipulator;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendOverDueMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:overdue-message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send SMS and email when project EMI payment deadline is exceeded';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $operationalCycleTrigger, $smsRepository;
    private static $Expiration_Threshold_Days = 5;

    public function __construct(OperationalCycleTrigger $operationalCycleTrigger, SmsRepository $smsRepository)
    {
        parent::__construct();
        $this->operationalCycleTrigger = $operationalCycleTrigger;
        $this->smsRepository = $smsRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::with('user', 'farmers')->get();
        foreach ($projects as $project) {

            $payment_deadline = $project->nextDueDate();

            //Stop sending sms when payment deadline is not defined
            if(is_null($payment_deadline)){
                continue;
            }

            //Send sms + emails for overdue days
            if (Carbon::now() > $payment_deadline) {
                $this->operationalCycleTrigger->sendSmsEmail($project, ProjectStatus::OPERATIONAL, ProjectStatus::OVERDUE);
                continue;
            }

            //send sms for expiration threshold days
            $difference_in_days = Carbon::now()->diffInDays($payment_deadline);
            if (Carbon::now()->lt($payment_deadline) && $difference_in_days <= self::$Expiration_Threshold_Days) {
                $farmer = $project->farmers()->first();
                $this->smsRepository->sendSmsNotification(
                    $farmer->contact_no,
                    'PROJECT_IN_GRACE_DAYS_MESSAGE_FARMER',
                    [
                    'farmer_name' => StringManipulator::getFirstTwoWords($farmer->farmer_name),
                    'number_of_days' => $difference_in_days
                    ]
                );
            }
        }

    }
}
