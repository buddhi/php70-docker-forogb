<?php

namespace App\Console\Commands;

use App\Services\FtpRetrievalService;
use Illuminate\Console\Command;

class RetreiveFtpData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ftp:retrieve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve data from the remote FTP server and stores in the database';
    /**
     * @var FtpRetrievalService
     */
    private $ftpRetrievalService;

    /**
     * Create a new command instance.
     *
     * @param FtpRetrievalService $ftpRetrievalService
     */
    public function __construct(FtpRetrievalService $ftpRetrievalService)
    {
        parent::__construct();
        $this->ftpRetrievalService = $ftpRetrievalService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->ftpRetrievalService->retrieve();
    }
}
