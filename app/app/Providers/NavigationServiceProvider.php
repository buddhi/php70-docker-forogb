<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NavigationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigation();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composeNavigation()
    {
        view()->composer(
            [   'layouts.headsection',
                'newIndex',
                'index',
                'admin.headsection',
                'investor.project-fund',
                'project.project-list-investor',
                'investor.my-investment'
            ],
            'App\Composers\NavigationProvider');
    }
}
