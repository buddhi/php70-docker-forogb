<?php

namespace App\Providers;

use App\DataModule\Repository\DataFormatter;
use App\DataModule\Repository\DataIngestion;
use App\Repositories\ApiPayment\ApiPayment;
use App\Repositories\ApiPayment\ApiPaymentRepository;
use App\Repositories\ApiPayment\Esewa\EsewaPaymentRepository;
use App\Repositories\ApiPayment\Esewa\EsewaPaymentService;
use App\Repositories\CashPayment\CashPaymentRepository;
use App\Repositories\CashPayment\CashPaymentService;
use App\Repositories\CattleInfo\CattleInfoRepository;
use App\Repositories\CattleInfo\CattleInfoService;
use App\Repositories\CropInfo\CropInfoRepository;
use App\Repositories\CropInfo\CropInfoService;
use App\Repositories\Draft\DraftRepository;
use App\Repositories\Draft\DraftService;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Email\EmailService;
use App\Repositories\Farm\FarmRepository;
use App\Repositories\Farm\FarmService;
use App\Repositories\Farmer\FarmerRepository;
use App\Repositories\Farmer\FarmerService;
use App\Repositories\Investment\InvestmentRepository;
use App\Repositories\Investment\InvestmentService;
use App\Repositories\Meter\MeterRepository;
use App\Repositories\Meter\MeterService;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Notification\NotificationService;
use App\Repositories\Otp\OtpRepository;
use App\Repositories\Otp\OtpService;
use App\Repositories\Partner\PartnerRepository;
use App\Repositories\Partner\PartnerService;
use App\Repositories\PaymentModule\PaymentModule;
use App\Repositories\PaymentModule\PaymentModuleRepository;
use App\Repositories\PaymentModule\SecurePayment\SecurePayment;
use App\Repositories\PaymentModule\SecurePayment\SecurePaymentInterface;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Project\ProjectService;
use App\Repositories\Project\ProjectStateUpdate;
use App\Repositories\Project\ProjectStateUpdateRepository;
use App\Repositories\ProjectPlan\ProjectPlanRepository;
use App\Repositories\ProjectPlan\ProjectPlanService;
use App\Repositories\Sms\SmsRepository;
use App\Repositories\Sms\SmsService;
use App\Repositories\Token\TokenRepository;
use App\Repositories\Token\TokenService;
use App\Repositories\TransactionModule\Cash\CashPaymentTransaction;
use App\Repositories\TransactionModule\TransactionModule;
use App\Repositories\TransactionModule\TransactionModuleRepository;
use App\Repositories\Upload\UploadRepository;
use App\Repositories\Upload\UploadService;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserService;
use App\Repositories\UserRegister\UserRegisterRepository;
use App\Repositories\UserRegister\UserRegisterService;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\Project\GuideLineRepository;
use App\Repositories\Project\GuidelineService;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(DataIngestion::class,DataFormatter::class);
        $this->app->singleton(ProjectRepository::class,ProjectService::class);
        $this->app->singleton(SecurePaymentInterface::class,SecurePayment::class);
        $this->app->singleton(UploadRepository::class,UploadService::class);
        $this->app->singleton(InvestmentRepository::class,InvestmentService::class);
        $this->app->singleton(PaymentModuleRepository::class,PaymentModule::class);
        $this->app->singleton(TransactionModuleRepository::class,TransactionModule::class);
        $this->app->singleton(ProjectStateUpdateRepository::class,ProjectStateUpdate::class);
        $this->app->singleton(SmsRepository::class,SmsService::class);
        $this->app->singleton(ApiPaymentRepository::class,ApiPayment::class);
        $this->app->singleton(EsewaPaymentRepository::class,EsewaPaymentService::class);
        $this->app->singleton(EmailRepository::class,EmailService::class);
        $this->app->singleton(FarmerRepository::class,FarmerService::class);
        $this->app->singleton(FarmRepository::class,FarmService::class);
        $this->app->singleton(CropInfoRepository::class,CropInfoService::class);
        $this->app->singleton(CattleInfoRepository::class,CattleInfoService::class);
        $this->app->singleton(DraftRepository::class,DraftService::class);
        $this->app->singleton(CashPaymentRepository::class, CashPaymentService::class);

        //admin section
        $this->app->singleton(ProjectPlanRepository::class,ProjectPlanService::class);
        $this->app->singleton(MeterRepository::class,MeterService::class);
        $this->app->singleton(UserRepository::class,UserService::class);

        //New OGB admin
        $this->app->singleton(PartnerRepository::class,PartnerService::class);
        $this->app->singleton(UserRegisterRepository::class,UserRegisterService::class);
        $this->app->singleton(OtpRepository::class,OtpService::class);
        $this->app->singleton(TokenRepository::class,TokenService::class);
        $this->app->singleton(NotificationRepository::class,NotificationService::class);
        $this->app->singleton(GuideLineRepository::class, GuidelineService::class);
    }
}
