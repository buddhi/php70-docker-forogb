<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OgbAdminServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->composeNavigation();
    }


    private function composeNavigation()
    {
        view()->composer(
            [   'ogbadmin.header',

            ],
            'App\Composers\OgbAdminViewComposer');
    }
}
