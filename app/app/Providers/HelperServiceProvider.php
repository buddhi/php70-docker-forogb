<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        App::bind('appHelper', function()
        {
            return new \App\HelperClass\AppHelper;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    
    }
}
