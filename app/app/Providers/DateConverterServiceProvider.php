<?php

namespace App\Providers;

use App\Services\DateConverter;
use Illuminate\Support\ServiceProvider;

class DateConverterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DateConverter', function ($app) {
            return new DateConverter();
        });
    }
}
