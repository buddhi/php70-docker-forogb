<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProjectDependenciesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeProjectForm();
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    
    private function composeProjectForm()
    {
        view()->composer('includes.form','App\Composers\FormProjectDependency');
        
    }
}
