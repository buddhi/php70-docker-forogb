<?php

namespace App\Providers;

use App\Models\DraftProject;
use App\Models\Partner;
use App\Models\Payment;
use App\Models\Project;
use App\Models\ProjectInvestment;
use App\Policies\DraftPolicy;
use App\Policies\InvestmentPolicy;
use App\Policies\PartnerPolicy;
use App\Policies\PaymentPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Project::class => ProjectPolicy::class,
        DraftProject::class => DraftPolicy::class,
        ProjectInvestment::class=>InvestmentPolicy::class,
        Payment::class => PaymentPolicy::class,
        User::class=>UserPolicy::class,
        Partner::class => PartnerPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('index', 'App\Policies\RemarksPolicy@index');
        Gate::define('create', 'App\Policies\RemarksPolicy@create');
    }
}
