<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectOverDueMail extends Mailable
{
    use Queueable, SerializesModels;

    private  $email_param,$param;
    public function __construct($email_param,$param)
    {
        $this->email_param = $email_param;
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->email_param->subject;
        $title = $this->email_param->title;
        $view_vars = array_merge($this->param,[
            'for_investor' => false,
            'title' =>  'Payment is Overdue!',
        ]);
        return $this->subject($subject)
            ->view('emails.project_overdue_email',$view_vars);
    }
}
