<?php

namespace App\Mail\Ogbadmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerificationEmailToAgent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $email_param,$param;
    public function __construct($email_param,$param)
    {
        $this->email_param = $email_param;
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->email_param->title;
        $for_investor = false;
        $view_vars = array_merge($this->param,[
            'for_investor' => $for_investor,
            'title' => $title,
        ]);
        return $this->subject($this->email_param->subject)
            ->from(env('MAIL_FROM',null))
            ->view('emails.verifyagent',$view_vars);
    }
}
