<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordChangeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $confirmation_message = config('messages.password_change_confirmation_message');
        $subject = config('messages.password_change_confirmation_subject');
        $user = $this->user;
        $for_investor = false;
        $title = 'Password changed!!';
        return $this->subject($subject)
            ->view('emails.change-password-email',
                compact('user','confirmation_message','for_investor','title'));
    }
}
