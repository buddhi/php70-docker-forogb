<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectErrorToOperationalMailAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $email_param,$param;
    public function __construct($email_param,$param)
    {
        $this->email_param = $email_param;
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->email_param->subject;
        $title = $this->email_param->title;
        $for_investor = false;
        $view_vars = array_merge($this->param,[
            'for_investor' => $for_investor,
            'for_admin' => true,
            'title' => $title,
        ]);
        return $this->subject($subject)
            ->cc($this->email_param->ccUsers)
            ->view('emails.project_error_to_operational_admin',$view_vars);
    }
}
