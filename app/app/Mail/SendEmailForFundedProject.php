<?php

namespace App\Mail;

use App\Models\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailForFundedProject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $project,$agent,$farmer,$plan;
    public function __construct($project,$agent,$farmer,$plan)
    {
        $this->project = $project;
        $this->agent   = $agent;
        $this->farmer = $farmer;
        $this->plan = $plan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   $farmer_name = $this->project->farmer_name;
        $subject = $farmer_name." ". __('app.about_project'). "| About " ." ".$farmer_name." Project | Gham Power";
        $title = "A project you created is taking off!";
        return $this->subject($subject)->cc(env('MAIL_ADMIN',null))
            ->view('emails.funded-project-email',[
                'project' => $this->project,
                'farmer' => $this->farmer,
                'plan' => $this->plan,
                'agent' => $this->agent,
                'title' => $title,
                'for_investor' => false
            ]);
    }
}
