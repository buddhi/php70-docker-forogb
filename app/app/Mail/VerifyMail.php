<?php

namespace App\Mail;

use App\Enums\Role;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $verify_email_subject = config('messages.verify_email_subject');
        $for_investor  = $this->user->hasRole(Role::INVESTOR);
        return $this->from(env('MAIL_FROM',null))->subject($verify_email_subject)
            ->view('emails.verifyuser',
                [   'user'=> $this->user,
                    'verify_email_subject' => $verify_email_subject,
                    'for_investor' => $for_investor,
                    'title' => config('messages.default_email_title')
                    ]);
    }
}
