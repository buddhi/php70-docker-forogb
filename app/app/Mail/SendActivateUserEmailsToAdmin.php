<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendActivateUserEmailsToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    protected  $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        $ccUsers = [env('BIZ_MAIL_ADMIN1',null),env('BIZ_MAIL_ADMIN2',null),env('BIZ_MAIL_ADMIN3',null)];
        return $this->from(env('MAIL_FROM',null))->subject('Active new user in OGB')->cc($ccUsers)->view('emails.activate-user-admin',['user'=> $this->user]);
    }
}

