<?php

namespace App\Mail;

use App\Models\District;
use App\Models\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailToInvestorForFundedProject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $project, $agent, $amount_raised, $amount_required, $farmer, $investor;

    public function __construct($investor, $project, $farmer, $amount_raised, $amount_required)
    {
        $this->investor = $investor;
        $this->project = $project;
        $this->farmer = $farmer;
        $this->amount_raised = $amount_raised;
        $this->amount_required = $amount_required;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $district = District::find($this->project->district_id);
        return $this
            ->subject('Your project is ready to take off at Off Grid Bazaar | Gham Power')
            ->cc(env('MAIL_ADMIN', null))
            ->view('emails.funded-project-email-investor',
                [
                    'for_investor' => true,
                    'district' => $district->name,
                    'title' => 'A project you helped is taking off!',
                    'investor' => $this->investor,
                    'project' => $this->project,
                    'farmer' => $this->farmer,
                    'project_cost' => $this->project->cost/100,
                ]);
    }
}
