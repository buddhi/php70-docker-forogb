<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactDeveloper extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $text_message;
    public $farmer_name;
    public $subject, $investor_name, $developer_name;

    public function __construct($farmer_name, $text_message, $subject, $investor_name, $developer_name)
    {
        $this->farmer_name = $farmer_name;
        $this->text_message = $text_message;
        $this->subject = $subject;
        $this->investor_name = $investor_name;
        $this->developer_name = $developer_name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_subject = $this->subject;
        $title = "Contact Developer – {$this->farmer_name} - from {$this->investor_name} to {$this->developer_name}";
        $for_investor = false;
        return $this->from(env('MAIL_FROM', null))->subject($email_subject)->view('emails.contact_developer')->with([
            'text_message' => $this->text_message,
            'for_investor' => $for_investor,
            'title' => $title
        ]);
    }
}
