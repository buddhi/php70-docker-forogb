<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectOperationalMail extends Mailable
{
    use Queueable, SerializesModels;

    private  $email_param,$param;
    public function __construct($email_param,$param)
    {
        $this->email_param = $email_param;
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->email_param->subject;
        $title = $this->email_param->title;
        $for_investor = true;
        $view_vars = array_merge($this->param,[
            'for_investor' => $for_investor,
            'title' => $title,
        ]);
        return $this->subject($subject)
            ->view('emails.project_operational_email',$view_vars);
    }
}
