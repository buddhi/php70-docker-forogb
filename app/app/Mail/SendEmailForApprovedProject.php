<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailForApprovedProject extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected  $agent,$project,$farmer;
    public function __construct($agent,$project,$farmer)
    {
        $this->agent  = $agent;
        $this->project = $project;
        $this->farmer = $farmer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = __('app.subject_for_approved_project',['farmer_name' =>$this->farmer->farmer_name]);
        $title = __('app.approved_project_title');
        return $this->subject($subject)->cc(env('MAIL_ADMIN',null))
            ->view('emails.approved-project-email',[
                'title' => $title,
                'project' => $this->project,
                'farmer' => $this->farmer,
                'agent' => $this->agent,
                'for_investor' => true,
            ]);

    }
}
