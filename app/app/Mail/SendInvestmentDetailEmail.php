<?php

namespace App\Mail;

use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvestmentDetailEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $user;
    private $projectPayments;
    public function __construct($user, $projectInvestments)
    {
        $this->user = $user;
        $this->projectPayments = $projectInvestments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $funded_project_count = count($this->projectPayments);
        $farmer_name = null;
        if($funded_project_count == 1){
            $project = Project::findOrFail($this->projectPayments[0]['project_id']);
            $farmer_name = $project->farmers->first()->farmer_name;
        }
        $title = 'Thank you for your generosity!';
        $send_investment_detail_email_subject =  config('messages.send_investment_detail_email_subject');
        return $this->subject($send_investment_detail_email_subject)
            ->view('emails.thank-investor-email',
                [   'user' => $this->user,
                    'funded_project_count'=>$funded_project_count,
                    'farmer_name'=>$farmer_name,
                    'for_investor'=> true,
                    'title'=> $title
                ]);
    }
}
