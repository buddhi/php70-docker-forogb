<?php

namespace App\Mail;

use App\Enums\Role;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class SendUserActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $text_message, $sender, $recipient;
    public function __construct($sender, $recipient)
    {
        $this->sender = $sender;
        $this->recipient = $recipient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user_activation_subject = config('messages.user_activation_subject');
        $email_template =  'emails.user-admin-activation';
        $title = 'Welcome to Off Grid bazaar!';
        $for_investor = false;
        if($this->recipient->hasRole(Role::INVESTOR)){
            $email_template =  'emails.user-admin-activation-investor';
            $for_investor = true;
            $title = 'Congratulations, you are ready to make an impact!';
        }
        return $this->from($this->sender)
            ->subject($user_activation_subject)
            ->view($email_template)->with([
                'recipient' => $this->recipient,
                'user_activation_subject' => $user_activation_subject,
                'title'  => $title,
                'for_investor'  => $for_investor
            ]);
    }
}